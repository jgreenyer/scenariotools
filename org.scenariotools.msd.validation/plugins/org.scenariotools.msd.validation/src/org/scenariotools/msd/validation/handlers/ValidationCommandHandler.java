package org.scenariotools.msd.validation.handlers;

import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.scenariotools.msd.validation.ocl.OclDiagramValidator;

public class ValidationCommandHandler extends AbstractHandler {
	private static final String COMPLETEOCLPATH 
		= "org.scenariotools.msd.validation/constraints/msd_constraints.ocl";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		URI oclURI = URI.createPlatformPluginURI(COMPLETEOCLPATH, true);
		OclDiagramValidator validator = new OclDiagramValidator();
		try {
			validator.validateSelection(oclURI);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return null;
	}

}
