package org.scenariotools.msd.validation.ocl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.ocl.pivot.utilities.OCL;
import org.eclipse.ocl.xtext.completeocl.validation.CompleteOCLEObjectValidator;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.core.sasheditor.editor.ISashWindowsContainer;
import org.eclipse.papyrus.infra.core.sashwindows.di.PageRef;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;

public class OclDiagramValidator {
	private static OCL ocl;

	public OclDiagramValidator() {
		this.loadOCL();
	}

	private void loadOCL() {
		if (ocl == null) {
			ocl = OCL.newInstance();
		}
	}
	
	private PapyrusMultiDiagramEditor getPapyrusEditor(){
		IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.getActiveEditor();

		if (!(activeEditor instanceof PapyrusMultiDiagramEditor))
			return null;

		PapyrusMultiDiagramEditor papyrusEditor = (PapyrusMultiDiagramEditor) activeEditor;

		return papyrusEditor;
	}
	
	private IFile getSelectedFile(){
		PapyrusMultiDiagramEditor papyrusEditor = this.getPapyrusEditor();
		
		if (papyrusEditor == null){
			return null;
		}
		IEditorInput editorInput = papyrusEditor.getEditorInput();
		
		if (!(editorInput instanceof FileEditorInput))
			return null;

		FileEditorInput fileEditorInput = (FileEditorInput) editorInput;
		
		return fileEditorInput.getFile();
	}

	private Model getModel() throws IllegalArgumentException {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IEditorPart editorPart = page.getActiveEditor(); // The Papyrus editor,
															// not a nested
															// editorPart
		ISashWindowsContainer container = (ISashWindowsContainer) editorPart.getAdapter(ISashWindowsContainer.class);
		Object model = container.getActiveSashWindowsPage().getRawModel();
		if (model instanceof PageRef) {
			EObject diagramEObj = ((PageRef) model).getEmfPageIdentifier();
			if (diagramEObj instanceof Diagram && ((Diagram) diagramEObj).getElement() != null) {
				Diagram diagram = (Diagram) diagramEObj;
				EObject parentContainer = diagram.getElement().eContainer();

				while (parentContainer != null) {
					if (parentContainer instanceof Model) {
						return (Model) parentContainer;
					}
					parentContainer = parentContainer.eContainer();
				}
			}
		}
		throw new IllegalArgumentException("Invalid model");
	}

	public void validateSelection(URI oclURI) throws IOException, CoreException {
		Model model = this.getModel();
		IFile markerFile = this.getSelectedFile();
		markerFile.deleteMarkers(IMarker.PROBLEM, false, IResource.DEPTH_INFINITE); // delete old markers
		
		CompleteOCLEObjectValidator myValidator 
			= new CompleteOCLEObjectValidator(
				UMLPackage.eINSTANCE,
				oclURI, 
				ocl.getEnvironmentFactory());

		ArrayList<EObject> objectsToValidate = new ArrayList<EObject>();
		objectsToValidate.addAll(this.findPackagedElements(model));
		
		System.out.println("###### " + new Date().toString() + " - Validating "+objectsToValidate.size()+" objects... ######");

		for (EObject eObj : objectsToValidate) {
			this.validateEObject(eObj, myValidator);
		}
	}
	
	private void placeMarker(int severity, String description) throws CoreException{
		IFile markerFile = this.getSelectedFile();
		
		// Add markers to marker file
		org.eclipse.core.resources.IMarker eclipseMarker = markerFile
				.createMarker(IMarker.PROBLEM);
		eclipseMarker.setAttribute(org.eclipse.core.resources.IMarker.SEVERITY, severity);

		eclipseMarker.setAttribute(org.eclipse.core.resources.IMarker.MESSAGE, description);

		String location = markerFile.getFullPath().toString();
		if (location != null)
			eclipseMarker.setAttribute(org.eclipse.core.resources.IMarker.LOCATION, location);
	}

	private ArrayList<EObject> findPackagedElements(EObject root) {
		ArrayList<EObject> contents = new ArrayList<EObject>();
		for (EObject element : root.eContents()) {
			contents.addAll(this.findPackagedElements(element));
		}
		contents.add(root);
		return contents;
	}

	private void validateEObject(EObject eObj, EValidator validator) throws CoreException {
		BasicDiagnostic diagnostics = Diagnostician.INSTANCE.createDefaultDiagnostic(eObj);
		Map<Object, Object> context = Diagnostician.INSTANCE.createDefaultContext();
		
		validator.validate(eObj, diagnostics, context);
		for (Diagnostic constraintResult : diagnostics.getChildren())
		{
			int severity = IMarker.SEVERITY_INFO;
			String description = constraintResult.getMessage();
			if (constraintResult.getSeverity() == Diagnostic.ERROR){
				System.err.println(
						"Result: " + description
							+ ", Severity: " + constraintResult.getSeverity());
				severity = IMarker.SEVERITY_ERROR;
			}
			else{
				System.out.println(
						"Result: " + description 
							+ ", Severity: " + constraintResult.getSeverity());
				severity = IMarker.SEVERITY_WARNING;
			}
			this.placeMarker(severity, description);
		}
	}
}
