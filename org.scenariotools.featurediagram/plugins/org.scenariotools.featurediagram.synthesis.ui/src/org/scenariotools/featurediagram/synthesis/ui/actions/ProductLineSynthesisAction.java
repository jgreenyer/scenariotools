package org.scenariotools.featurediagram.synthesis.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.featurediagram.synthesis.ui.jobs.ProductLineSynthesisJob;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;

public class ProductLineSynthesisAction implements IObjectActionDelegate {

	protected void postProblem(final String problem) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"A problem occurred",
						problem);
			}
		});
	}

	@Override
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		if (!"scenariorunconfiguration".equals(((IFile)structuredSelection.getFirstElement()).getFileExtension())){
			postProblem("You must select a *.scenariorunconfiguration file.");
		}
		
		IFile scenarioRunConfigurationResourceFile = (IFile) structuredSelection.getFirstElement();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);

		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

		ProductLineSynthesisJob job=new ProductLineSynthesisJob("Productline Synthesis",scenarioRunConfiguration);
		job.setUser(true);
		job.schedule();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

}
