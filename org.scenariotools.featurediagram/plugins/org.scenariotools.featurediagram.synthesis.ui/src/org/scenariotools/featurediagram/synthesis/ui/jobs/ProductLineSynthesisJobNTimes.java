package org.scenariotools.featurediagram.synthesis.ui.jobs;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.scenariotools.featurediagram.synthesis.ProductLineTopDownSynthesis;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;

public class ProductLineSynthesisJobNTimes extends Job {
	ScenarioRunConfiguration scenarioRunConfiguration;

	public static final int N = 20;
	
	public ProductLineSynthesisJobNTimes(String name, ScenarioRunConfiguration scenarioRunConfiguration) {
		super(name);
		this.scenarioRunConfiguration = scenarioRunConfiguration;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		org.eclipse.uml2.uml.Package umlPackage=scenarioRunConfiguration.getUml2EcoreMapping().getUmlPackage();
		//search BaseFeature (assuming there is just one)
		Component baseFeature=null;
		for(Element element:umlPackage.getOwnedElements())
			if(element instanceof org.eclipse.uml2.uml.Component){
				Component component=(Component)element;
				if(component.getAppliedStereotype("FeatureDiagram::BaseFeature")!=null){
					baseFeature=component;
					break;
				}
			}
		
		int totalNumberOfProducts = -1; 
		long[] totalSynthesisTimes = new long[N];
		int noOfRealizableProducts = -1;
		int noOfUnrealizableProducts = -1;
		int[] totalNumerOfStatesExplored = new int[N];
		int[] totalNumerOfNonWinningStatesExplored = new int[N];
		
		
		for(int i = 0; i < N; i++){
			long totalSynthesisTime = System.currentTimeMillis();
			
			ProductLineTopDownSynthesis productLineTopDownSynthesis = new ProductLineTopDownSynthesis(baseFeature, scenarioRunConfiguration);
			productLineTopDownSynthesis.synthesize();

			totalSynthesisTimes[i] = System.currentTimeMillis() - totalSynthesisTime;

			if (noOfRealizableProducts < 0){
				noOfRealizableProducts = productLineTopDownSynthesis.getRealizableProducts().size();
			}else{
				Assert.isTrue(noOfRealizableProducts == productLineTopDownSynthesis.getRealizableProducts().size());
			}
			if (noOfUnrealizableProducts < 0){
				noOfUnrealizableProducts = productLineTopDownSynthesis.getUnrealizableProducts().size();
			}else{
				Assert.isTrue(noOfUnrealizableProducts == productLineTopDownSynthesis.getUnrealizableProducts().size());
			}
			if (totalNumberOfProducts < 0){
				totalNumberOfProducts = productLineTopDownSynthesis.getRealizableProducts().size() + productLineTopDownSynthesis.getUnrealizableProducts().size();
			}else{
				Assert.isTrue(totalNumberOfProducts == productLineTopDownSynthesis.getRealizableProducts().size() + productLineTopDownSynthesis.getUnrealizableProducts().size());
			}
			totalNumerOfStatesExplored[i] = productLineTopDownSynthesis.getTotalNumberOfStatesExplored();
			totalNumerOfNonWinningStatesExplored[i] = productLineTopDownSynthesis.getTotalNumberOfStatesExplored() - productLineTopDownSynthesis.getTotalNumberOfWinningStatesExplored();
		}
		

		
		postFinished(totalNumberOfProducts, 
				noOfRealizableProducts, 
				noOfUnrealizableProducts, 
				average(totalNumerOfStatesExplored),
				roundToTwoDecimals(stdDeviation(totalNumerOfStatesExplored)),
				lowest(totalNumerOfStatesExplored),
				highest(totalNumerOfStatesExplored),
				average(totalNumerOfNonWinningStatesExplored),
				roundToTwoDecimals(stdDeviation(totalNumerOfNonWinningStatesExplored)),
				lowest(totalNumerOfNonWinningStatesExplored),
				highest(totalNumerOfNonWinningStatesExplored),
				average(totalSynthesisTimes),
				minmaxdiff(totalSynthesisTimes));
		
		
		return Status.OK_STATUS;
	}

	private double average(int[] a){
		int sum = 0;
		for(int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum/a.length;
	}
	
	private double roundToTwoDecimals(double d){
		return Math.round(d*100)/100d;
	}

    private double stdDeviation(int[] a)
    {
        return Math.sqrt(variance(a));
    }

    private double variance(int[] a)
    {
        double average = average(a);
        double temp = 0;
        for(double x : a)
            temp += (average-x)*(average-x);
        return temp/a.length;
    }
    
    private int highest(int[] a){
    	int h = a[0];
        for(int x : a)
        	if (h < x) h = x;
    	return h;
    }

    private int lowest(int[] a){
    	int l = a[0];
        for(int x : a)
        	if (l > x) l = x;
    	return l;
    }
    
	private int minmaxdiff(int[] a){
		int min = a[0];
		int max = a[0];
		
		for(int i = 1; i < a.length; i++){
			if (a[i] > max)
				max = a[i];
			else if (a[i] < min)
				min = a[i];
		}
		return max-min;
	} 

	private long minmaxdiff(long[] a){
		long min = a[0];
		long max = a[0];
		
		for(int i = 1; i < a.length; i++){
			if (a[i] > max)
				max = a[i];
			else if (a[i] < min)
				min = a[i];
		}
		return max-min;
	} 

	
	private double average(long[] a){
		long sum = 0;
		for(int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum/a.length;
	} 

	private void postFinished(
			final int totalNumberOfProducts,
			final int noOfRealizableProducts,
			final int noOfUnrealizableProducts,
			final double averageNumerOfStatesExplored,
			final double stdDeviationOfStatesExplored,
			final int lowestNumerOfStatesExplored,
			final int highestNumerOfStatesExplored,
			final double averageNumerOfNonWinningStatesExplored,
			final double stdDeviationOfNonWinningStatesExplored,
			final int lowestNumberOfNonWinningStatesExplored,
			final int highestNumberOfNonWinningStatesExplored,
			final double averageSynthesisTime,
			final long maxDiffSynthesisTime) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"Product line synthesis: ",
						"Number of products: "
						+  totalNumberOfProducts + "\n"
						+ "Number of realizable products: "
						+  noOfRealizableProducts + "\n"
						+ "Number of unrealizable products: "
						+  noOfUnrealizableProducts + "\n"
						+ "Average no. states explored: "
						+  averageNumerOfStatesExplored  + "\n"
						+ "            std.deviation : " 
						+ stdDeviationOfStatesExplored + " "
						+ " - lowest: " 
						+ lowestNumerOfStatesExplored + " "
						+ " - highest: " 
						+ highestNumerOfStatesExplored + "\n"
						+ "Average no. non-winning states explored: "
						+  averageNumerOfNonWinningStatesExplored + "\n"
						+ "            std.deviation : " 
						+ stdDeviationOfNonWinningStatesExplored + " "
						+ " - lowest: " 
						+ lowestNumberOfNonWinningStatesExplored + " "
						+ " - highest: " 
						+ highestNumberOfNonWinningStatesExplored + "\n"
						+ "Average time required (ms): "
						+  averageSynthesisTime 
						+ " - max diff: " 
						+ maxDiffSynthesisTime + "\n"
						+ "\n"
						+ "Number of measurements performed: "
						+  N + "\n"
					);
			}
		});
	}

}
