package org.scenariotools.featurediagram.synthesis.ui.jobs;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.scenariotools.featurediagram.synthesis.ProductLineTopDownSynthesis;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.RuntimeStateGraph;

public class ProductLineSynthesisJob extends Job {
	ScenarioRunConfiguration scenarioRunConfiguration;

	public ProductLineSynthesisJob(String name, ScenarioRunConfiguration scenarioRunConfiguration) {
		super(name);
		this.scenarioRunConfiguration = scenarioRunConfiguration;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		org.eclipse.uml2.uml.Package umlPackage=scenarioRunConfiguration.getUml2EcoreMapping().getUmlPackage();
		//search BaseFeature (assuming there is just one)
		Component baseFeature=null;
		for(Element element:umlPackage.getOwnedElements())
			if(element instanceof org.eclipse.uml2.uml.Component){
				Component component=(Component)element;
				if(component.getAppliedStereotype("FeatureDiagram::BaseFeature")!=null){
					baseFeature=component;
					break;
				}
			}
		
		long totalSynthesisTime = System.currentTimeMillis();
		
		ProductLineTopDownSynthesis productLineTopDownSynthesis = new ProductLineTopDownSynthesis(baseFeature, scenarioRunConfiguration);
		productLineTopDownSynthesis.synthesize();

		totalSynthesisTime = System.currentTimeMillis() - totalSynthesisTime;

		for (Map.Entry<Set<Component>, RuntimeStateGraph> productContollersMapEntry : productLineTopDownSynthesis.getControls().entrySet()) {
			// TODO: Serialize results
		}
		
		
		postFinished(productLineTopDownSynthesis, totalSynthesisTime);
		
		
		return Status.OK_STATUS;
	}

	private void postFinished(
			final ProductLineTopDownSynthesis p,
			final long totalSynthesisTime) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"Product line synthesis: ",
						"Total number of products: "
						+  (p.getRealizableProducts().size() + p.getUnrealizableProducts().size()) + "\n"
						+ "Number of realizable products: "
						+  p.getRealizableProducts().size() + "\n"
						+ "Number of unrealizable products: "
						+  p.getUnrealizableProducts().size() + "\n"
						+ "Total number of states explored: "
						+  p.getTotalNumberOfStatesExplored() + "\n"
						+ "Total number of non-winning states explored: "
						+  (p.getTotalNumberOfStatesExplored() - p.getTotalNumberOfWinningStatesExplored()) + "\n"
						+ "Total time required (ms): "
						+  totalSynthesisTime + "\n"
					);
			}
		});
	}

}
