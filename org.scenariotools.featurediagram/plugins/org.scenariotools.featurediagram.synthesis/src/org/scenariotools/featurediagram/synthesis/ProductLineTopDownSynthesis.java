package org.scenariotools.featurediagram.synthesis;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Stereotype;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.synthesis.otfb.incremental.OTFBAlgorithmIncremental;
import org.scenariotools.synthesis.otfb.incremental.OTFSynthesisIncremental;



public class ProductLineTopDownSynthesis {
	private Component root;
	private Map<Set<Component>, RuntimeStateGraph> controls;

	private ScenarioRunConfiguration scenarioRunConfiguration;
	
	private Set<Set<Component>> realizableProducts;
	private Set<Set<Component>> unrealizableProducts;

	private int totalNumberOfStatesExplored;
	private int totalNumberOfWinningStatesExplored;
	
	public ProductLineTopDownSynthesis(Component root, ScenarioRunConfiguration scenarioRunConfiguration){
		this.root = root;
		this.scenarioRunConfiguration = scenarioRunConfiguration;
	}

	//corresponds to the algorithm in the paper
	public void synthesize(){
		//init
		totalNumberOfStatesExplored = 0;
		totalNumberOfWinningStatesExplored = 0;
		controls = new HashMap<Set<Component>, RuntimeStateGraph>();
		realizableProducts = new HashSet<Set<Component>>();
		unrealizableProducts = new HashSet<Set<Component>>();
		
		
		//pseudocode line 1:
		Stack<StackEntry> stack = new Stack<StackEntry>();
		Set<Component> rootUnionRootRequired = requires(root);
		rootUnionRootRequired.add(root);
		Set<Component> rootExcluded = excludes(root);
		stack.push(new StackEntry(rootUnionRootRequired, rootExcluded,
				rootUnionRootRequired));
		
		//pseudocode lines 2,3:
		RuntimeStateGraph previous = null;
		while(!stack.isEmpty()){
			//pseudocode line 4:
			StackEntry entry = stack.pop();
			//pseudocode line 5:
			Set<Component> incIntersectionExc = new HashSet<Component>(
					entry.inc);
			incIntersectionExc.retainAll(entry.exc);
			if (incIntersectionExc.isEmpty()) {
				//pseudocode lines 6,7:
				Set<Choice> choices = computeChoices(entry.lastInc);
				if (!choices.isEmpty()) {
					//pseudocode line 8:
					for(Choice choice:choices){
						//pseudocode line 9:
						Set<Component> incUnionToInc = new HashSet<Component>(
								entry.inc);
						incUnionToInc.addAll(choice.toInc);
						Set<Component> excUnionToExc = new HashSet<Component>(
								entry.exc);
						excUnionToExc.addAll(choice.toExc);
						stack.push(new StackEntry(incUnionToInc, excUnionToExc,
								choice.toInc));
					//(pseudocode line 10:)
					}
				}
				//pseudocode line 11:
				else{
					//pseudocode line 12:
					RuntimeStateGraph controllerForInc = synthesize(entry.inc, previous);
					if (controllerForInc != null){
						controls.put(entry.inc, controllerForInc);
						//pseudocode line 13:
						previous = controllerForInc;
					}
				//(pseudocode lines 14,15,16:)
				}
			}
		}
	}
	
	private RuntimeStateGraph synthesize(Set<Component> inc, RuntimeStateGraph previous){

		List<Package> productPackages = new BasicEList<Package>();
		
		System.out.println("Synthesizing product with features:");
		for(Component feature:inc)
			System.out.println("\t"+feature.getName());
		System.out.println("packages:");
		for(Component feature:inc){
			Package featurePackage=getPackageForFeature(feature);
			if(featurePackage!=null){
				productPackages.add(featurePackage);
				System.out.println("\t"+featurePackage.getName());
			}
		}
		
		

		ScenarioRunConfiguration productScenarioRunConfiguration = EcoreUtil.copy(scenarioRunConfiguration);
		
		Assert.isTrue(productScenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered().isEmpty());
		
		productScenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered().addAll(productPackages);
		
		OTFSynthesisIncremental otfSynthesisIncremental = new OTFSynthesisIncremental();
		otfSynthesisIncremental.initializeOTFBAlgorithm();
		((OTFBAlgorithmIncremental)otfSynthesisIncremental.getOTFBAlgorithm()).setInitialBaseController(previous);

		MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();
		MSDRuntimeState msdRuntimeState = msdRuntimeStateGraph.init(productScenarioRunConfiguration);
		
		boolean strategyExists = otfSynthesisIncremental.otfSynthesizeController(msdRuntimeStateGraph);

		totalNumberOfStatesExplored += msdRuntimeStateGraph.getStates().size();
		totalNumberOfWinningStatesExplored += otfSynthesisIncremental.getOTFBAlgorithm().getWinAndNotLoseBuechiStates().size();
		
		
		System.out.println("strategyExists: " + strategyExists);
		if (strategyExists){
			realizableProducts.add(inc);
			return otfSynthesisIncremental.getNewController();
//			return null;
		}else{
			unrealizableProducts.add(inc);
			return null;
		}
	}
	
	private Package getPackageForFeature(Component feature){
		//TODO: currently supports only one package/feature
		for(Dependency dependency:feature.getClientDependencies())
			for(NamedElement dependencyTarget:dependency.getSuppliers())
				if(dependencyTarget instanceof Package)
					return (Package)dependencyTarget;
		return null;
	}
	
	private Set<Component> requires(Component feature){
		List<Component> todo=new LinkedList<Component>();
		Set<Component> result = new HashSet<Component>();
		todo.add(feature);
		while(todo.size()>0){
			Component currentFeature = todo.remove(0);
			if(currentFeature!=null){
				for(Component requiredFeature:getDirectlyRequired(currentFeature)){
					if(!result.contains(requiredFeature)){
						todo.add(requiredFeature);
						result.add(requiredFeature);
					}
				}
			}
		}
		
		return result;
	}
	
	private Set<Component> excludes(Component feature){
		Set<Component> result = new HashSet<Component>();
		//TODO: support explicit exclusions by root feature
		return result;
	}
	
	private Set<Choice> computeChoices(Set<Component> lastInc){
		Set<Choice> result = new HashSet<Choice>();
		for(Component incFeature:lastInc){
			Set<Choice> newChoices=computeChoicesForFeature(incFeature);
			
			if(newChoices.size()>0){
				//create empty choice to combine with if no choice is in the set yet
				if(result.size()==0)
					result.add(new Choice(new HashSet<Component>(),new HashSet<Component>()));

				Set<Choice> newResult = new HashSet<Choice>();
				//combine new Choices with all choices of previous features
				for(Choice newChoice:newChoices)
					for(Choice oldChoice:result){
						Choice combinedChoice = new Choice(
								new HashSet<Component>(oldChoice.toInc),
								new HashSet<Component>(oldChoice.toExc));
						combinedChoice.toExc.addAll(newChoice.toExc);
						combinedChoice.toInc.addAll(newChoice.toInc);
						newResult.add(combinedChoice);
					}
				//replace result set with new version
				result = newResult;
			}
		}
		//System.out.println("computeChoices("+lastInc+"):\n"+result);
		return result;
	}
	
	private Set<Choice> computeChoicesForFeature(Component feature){
		Port relationshipGroup=getChildPort(feature);
		if(relationshipGroup!=null){
			String relationshipGroupKind=getRelationshipGroupKind(feature);  
			if(relationshipGroupKind.equals("AND")){
				//System.out.println("choices for AND group: "+computeChoicesForAndGroup(relationshipGroup));
				return computeChoicesForAndGroup(relationshipGroup);
			}
			else if(relationshipGroupKind.equals("OR")){
				return computeChoicesForOrGroup(relationshipGroup);
			}
			else if(relationshipGroupKind.equals("XOR")){
				//System.out.println("choices for XOR group: "+computeChoicesForXorGroup(relationshipGroup));
				return computeChoicesForXorGroup(relationshipGroup);
			}
		}
		//TODO: support explicitly required/excluded features via cross-edges
		return new HashSet<Choice>();
	}

	private Set<Choice> computeChoicesForAndGroup(Port group){
		Set<Choice> result = new HashSet<Choice>();
		//create empty choice to start with
		if(group.getClientDependencies().size()>0)
			result.add(new Choice(new HashSet<Component>(),new HashSet<Component>()));
		for(Dependency dependency:group.getClientDependencies()){
			Component childFeature=(Component)dependency.getSuppliers().get(0);
			for(Choice choice:new LinkedList<Choice>(result)){
				//alternative choice for optional features is to exclude them
				//(copies ALL other choices and adds alternative choice to each)
				if(getFeatureRelationshipKind(dependency).equals("Optional")){
					//copy existing choice
					Choice alternativeChoice=new Choice(new HashSet<Component>(choice.toInc), new HashSet<Component>(choice.toExc));
					alternativeChoice.toExc.add(childFeature);
					result.add(alternativeChoice);
				}
				//"default" choice (and only one for mandatory child features) is to include the child feature
				//(added to ALL other choices)
				choice.toInc.add(childFeature);
			}
		}
		return result;
	}

	private Set<Choice> computeChoicesForOrGroup(Port group){
		Set<Choice> result = new HashSet<Choice>();
		//create empty choice to start with
		if(group.getClientDependencies().size()>0)
			result.add(new Choice(new HashSet<Component>(),new HashSet<Component>()));
		for(Dependency dependency:group.getClientDependencies()){
			Component childFeature=(Component)dependency.getSuppliers().get(0);
			for(Choice choice:new LinkedList<Choice>(result)){
				//we can either include or exclude the next child feature in the existing choices
				//this creates two new choices
				//alternative choice excluding the child feature (duplicate the choice):
				Choice alternativeChoice=new Choice(new HashSet<Component>(choice.toInc), new HashSet<Component>(choice.toExc));
				alternativeChoice.toExc.add(childFeature);
				result.add(alternativeChoice);
				//"default" choice including the child feature:
				choice.toInc.add(childFeature);
			}
		}
		for(Choice choice:new LinkedList<Choice>(result)){
			if(choice.toInc.isEmpty())
				result.remove(choice);
		}
		return result;
	}

	private Set<Choice> computeChoicesForXorGroup(Port group){
		Set<Choice> result = new HashSet<Choice>();
		
		Set<Component> allChildFeatures=new HashSet<Component>();
		for(Dependency dependency:group.getClientDependencies())
			allChildFeatures.add((Component)dependency.getSuppliers().get(0));
		
		boolean optionalChild=false;
		for(Dependency dependency:group.getClientDependencies()){
			Component childFeature=(Component)dependency.getSuppliers().get(0);
			Set<Component> toInc=new HashSet<Component>();
			Set<Component> toExc=new HashSet<Component>();
			if(getFeatureRelationshipKind(dependency).equals("Mandatory")){
				toInc.add(childFeature);
				//add all others to toExc
				toExc.addAll(allChildFeatures);
				toExc.remove(childFeature);
			}
			else{
				//special case: at least one child feature is optional
				//-> one choice is not to include anything, but to exclude everything
				optionalChild=true;
			}
			result.add(new Choice(toInc, toExc));
		}
		if(optionalChild)
			result.add(new Choice(allChildFeatures, new HashSet<Component>()));
		return result;
	}

	private Set<Component> getDirectlyRequired(Component feature){
		Set<Component> result = new HashSet<Component>();
		
		Port relationshipGroup=getChildPort(feature);
		if(relationshipGroup!=null){
			//System.out.println(getRelationshipGroupKind(feature));
			if(getRelationshipGroupKind(feature).equals("AND")){
				for(Dependency dependency:relationshipGroup.getClientDependencies()){
					//System.out.println(getFeatureRelationshipKind(dependency));
					if(getFeatureRelationshipKind(dependency).equals("Mandatory")){
						result.add((Component)dependency.getSuppliers().get(0));
					}
				}
			}
		}
		//TODO: support explicitly required features via cross-edges
		return result;
	}
		
	private String getRelationshipGroupKind(Component feature){
		Port relationshipGroup=getChildPort(feature);
		Stereotype stereotype=relationshipGroup.getAppliedStereotype("FeatureDiagram::RelationshipGroup");
		EnumerationLiteral groupKind=(EnumerationLiteral)relationshipGroup.getValue(stereotype, "relationshipGroupKind");
		return groupKind.getName();
	}

	private String getFeatureRelationshipKind(Dependency dependency){
		Stereotype stereotype=dependency.getAppliedStereotype("FeatureDiagram::ChildFeatureRelationship");
		EnumerationLiteral relationshipKind=(EnumerationLiteral)dependency.getValue(stereotype, "featureRelationshipKind");
		return relationshipKind.getName();
	}

	private Port getChildPort(Component feature){
		EList<Port> ownedPorts=feature.getOwnedPorts();
		if(ownedPorts!=null && ownedPorts.size()>0)
			return ownedPorts.get(0);
		return null;
	}

	class StackEntry{
		
		public StackEntry(Set<Component> inc,
				Set<Component> exc,
				Set<Component> lastInc) {
			super();
			this.lastInc = lastInc;
			this.inc = inc;
			this.exc = exc;
		}
		public Set<Component> lastInc;
		public Set<Component> inc;
		public Set<Component> exc;
	}

	class Choice{
		
		public Choice(Set<Component> toInc, Set<Component> toExc) {
			super();
			this.toInc = toInc;
			this.toExc = toExc;
		}
		public Set<Component> toInc;
		public Set<Component> toExc;
		
		//for debug output
		public String toString(){
			String result="\nchoice:\n\ttoInc:\n\t";
			for(Component feature:toInc)
				result+=feature.getName()+"+";
			if(result.endsWith("+"))
				result=result.substring(0, result.length()-1);
			result+="\n\ttoExc:\n\t";
			for(Component feature:toExc)
				result+=feature.getName()+"+";
			if(result.endsWith("+"))
				result=result.substring(0, result.length()-1);
			return result+"\n";
		}
	}

	public Map<Set<Component>, RuntimeStateGraph> getControls() {
		return controls;
	}
	
	public Set<Set<Component>> getRealizableProducts() {
		return realizableProducts;
	}

	public Set<Set<Component>> getUnrealizableProducts() {
		return unrealizableProducts;
	}

	public int getTotalNumberOfStatesExplored() {
		return totalNumberOfStatesExplored;
	}

	public int getTotalNumberOfWinningStatesExplored() {
		return totalNumberOfWinningStatesExplored;
	}

}
