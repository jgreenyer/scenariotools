/**
 */
package plruntime;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see plruntime.PlruntimePackage
 * @generated
 */
public interface PlruntimeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlruntimeFactory eINSTANCE = plruntime.impl.PlruntimeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Featured MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Featured MSD Runtime State Graph</em>'.
	 * @generated
	 */
	FeaturedMSDRuntimeStateGraph createFeaturedMSDRuntimeStateGraph();

	/**
	 * Returns a new object of class '<em>Featured MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Featured MSD Runtime State</em>'.
	 * @generated
	 */
	FeaturedMSDRuntimeState createFeaturedMSDRuntimeState();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PlruntimePackage getPlruntimePackage();

} //PlruntimeFactory
