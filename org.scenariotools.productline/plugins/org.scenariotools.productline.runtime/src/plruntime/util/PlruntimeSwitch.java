/**
 */
package plruntime.util;

import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interaction;

import org.scenariotools.events.Event;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

import org.scenariotools.stategraph.Transition;
import plruntime.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see plruntime.PlruntimePackage
 * @generated
 */
public class PlruntimeSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PlruntimePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlruntimeSwitch() {
		if (modelPackage == null) {
			modelPackage = PlruntimePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH: {
				FeaturedMSDRuntimeStateGraph featuredMSDRuntimeStateGraph = (FeaturedMSDRuntimeStateGraph)theEObject;
				T result = caseFeaturedMSDRuntimeStateGraph(featuredMSDRuntimeStateGraph);
				if (result == null) result = caseMSDRuntimeStateGraph(featuredMSDRuntimeStateGraph);
				if (result == null) result = caseRuntimeStateGraph(featuredMSDRuntimeStateGraph);
				if (result == null) result = caseStateGraph(featuredMSDRuntimeStateGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE: {
				FeaturedMSDRuntimeState featuredMSDRuntimeState = (FeaturedMSDRuntimeState)theEObject;
				T result = caseFeaturedMSDRuntimeState(featuredMSDRuntimeState);
				if (result == null) result = caseMSDRuntimeState(featuredMSDRuntimeState);
				if (result == null) result = caseRuntimeState(featuredMSDRuntimeState);
				if (result == null) result = caseState(featuredMSDRuntimeState);
				if (result == null) result = caseAnnotatableElement(featuredMSDRuntimeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<org.eclipse.uml2.uml.Package, EList<Interaction>> packageToInteractionListMapEntry = (Map.Entry<org.eclipse.uml2.uml.Package, EList<Interaction>>)theEObject;
				T result = casePackageToInteractionListMapEntry(packageToInteractionListMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Event, EMap<Transition, Integer>> eventToFeaturedTransitionListMapEntry = (Map.Entry<Event, EMap<Transition, Integer>>)theEObject;
				T result = caseEventToFeaturedTransitionListMapEntry(eventToFeaturedTransitionListMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Transition, Integer> transitionToFeatureExpressionMapEntry = (Map.Entry<Transition, Integer>)theEObject;
				T result = caseTransitionToFeatureExpressionMapEntry(transitionToFeatureExpressionMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.PACKAGE_TO_FEATURE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<org.eclipse.uml2.uml.Package, Component> packageToFeatureMapEntry = (Map.Entry<org.eclipse.uml2.uml.Package, Component>)theEObject;
				T result = casePackageToFeatureMapEntry(packageToFeatureMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.FEATURE_TO_BDD_VARIABLE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Component, Integer> featureToBddVariableMapEntry = (Map.Entry<Component, Integer>)theEObject;
				T result = caseFeatureToBddVariableMapEntry(featureToBddVariableMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PlruntimePackage.TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Transition, String> transitionToFeatureExpressionStringMapEntry = (Map.Entry<Transition, String>)theEObject;
				T result = caseTransitionToFeatureExpressionStringMapEntry(transitionToFeatureExpressionStringMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Featured MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Featured MSD Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeaturedMSDRuntimeStateGraph(FeaturedMSDRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Featured MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Featured MSD Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeaturedMSDRuntimeState(FeaturedMSDRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package To Interaction List Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package To Interaction List Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageToInteractionListMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, EList<Interaction>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event To Featured Transition List Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event To Featured Transition List Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventToFeaturedTransitionListMapEntry(Map.Entry<Event, EMap<Transition, Integer>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition To Feature Expression Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition To Feature Expression Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionToFeatureExpressionMapEntry(Map.Entry<Transition, Integer> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package To Feature Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package To Feature Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageToFeatureMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, Component> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature To Bdd Variable Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature To Bdd Variable Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureToBddVariableMapEntry(Map.Entry<Component, Integer> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition To Feature Expression String Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition To Feature Expression String Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionToFeatureExpressionStringMapEntry(Map.Entry<Transition, String> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateGraph(StateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeStateGraph(RuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeStateGraph(MSDRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatableElement(AnnotatableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeState(RuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeState(MSDRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //PlruntimeSwitch
