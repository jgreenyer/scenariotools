/**
 */
package plruntime.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interaction;

import org.scenariotools.events.Event;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

import org.scenariotools.stategraph.Transition;
import plruntime.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see plruntime.PlruntimePackage
 * @generated
 */
public class PlruntimeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PlruntimePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlruntimeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = PlruntimePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlruntimeSwitch<Adapter> modelSwitch =
		new PlruntimeSwitch<Adapter>() {
			@Override
			public Adapter caseFeaturedMSDRuntimeStateGraph(FeaturedMSDRuntimeStateGraph object) {
				return createFeaturedMSDRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseFeaturedMSDRuntimeState(FeaturedMSDRuntimeState object) {
				return createFeaturedMSDRuntimeStateAdapter();
			}
			@Override
			public Adapter casePackageToInteractionListMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, EList<Interaction>> object) {
				return createPackageToInteractionListMapEntryAdapter();
			}
			@Override
			public Adapter caseEventToFeaturedTransitionListMapEntry(Map.Entry<Event, EMap<Transition, Integer>> object) {
				return createEventToFeaturedTransitionListMapEntryAdapter();
			}
			@Override
			public Adapter caseTransitionToFeatureExpressionMapEntry(Map.Entry<Transition, Integer> object) {
				return createTransitionToFeatureExpressionMapEntryAdapter();
			}
			@Override
			public Adapter casePackageToFeatureMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, Component> object) {
				return createPackageToFeatureMapEntryAdapter();
			}
			@Override
			public Adapter caseFeatureToBddVariableMapEntry(Map.Entry<Component, Integer> object) {
				return createFeatureToBddVariableMapEntryAdapter();
			}
			@Override
			public Adapter caseTransitionToFeatureExpressionStringMapEntry(Map.Entry<Transition, String> object) {
				return createTransitionToFeatureExpressionStringMapEntryAdapter();
			}
			@Override
			public Adapter caseStateGraph(StateGraph object) {
				return createStateGraphAdapter();
			}
			@Override
			public Adapter caseRuntimeStateGraph(RuntimeStateGraph object) {
				return createRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseMSDRuntimeStateGraph(MSDRuntimeStateGraph object) {
				return createMSDRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseAnnotatableElement(AnnotatableElement object) {
				return createAnnotatableElementAdapter();
			}
			@Override
			public Adapter caseState(State object) {
				return createStateAdapter();
			}
			@Override
			public Adapter caseRuntimeState(RuntimeState object) {
				return createRuntimeStateAdapter();
			}
			@Override
			public Adapter caseMSDRuntimeState(MSDRuntimeState object) {
				return createMSDRuntimeStateAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link plruntime.FeaturedMSDRuntimeStateGraph <em>Featured MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph
	 * @generated
	 */
	public Adapter createFeaturedMSDRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link plruntime.FeaturedMSDRuntimeState <em>Featured MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see plruntime.FeaturedMSDRuntimeState
	 * @generated
	 */
	public Adapter createFeaturedMSDRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Package To Interaction List Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createPackageToInteractionListMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Event To Featured Transition List Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEventToFeaturedTransitionListMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Transition To Feature Expression Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createTransitionToFeatureExpressionMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Package To Feature Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createPackageToFeatureMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Feature To Bdd Variable Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createFeatureToBddVariableMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Transition To Feature Expression String Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createTransitionToFeatureExpressionStringMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.StateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.StateGraph
	 * @generated
	 */
	public Adapter createStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeStateGraph
	 * @generated
	 */
	public Adapter createRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph <em>MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph
	 * @generated
	 */
	public Adapter createMSDRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.AnnotatableElement <em>Annotatable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.AnnotatableElement
	 * @generated
	 */
	public Adapter createAnnotatableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	public Adapter createRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDRuntimeState <em>MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState
	 * @generated
	 */
	public Adapter createMSDRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //PlruntimeAdapterFactory
