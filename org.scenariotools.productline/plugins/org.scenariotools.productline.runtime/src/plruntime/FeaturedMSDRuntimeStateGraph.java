/**
 */
package plruntime;

import java.util.List;
import java.util.Map;
import java.util.Set;

import jdd.bdd.BDD;


import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;

import org.scenariotools.events.Event;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;

import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Featured MSD Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link plruntime.FeaturedMSDRuntimeStateGraph#getPackageToInteractionListMap <em>Package To Interaction List Map</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeStateGraph#getFeatureToBddVariableMap <em>Feature To Bdd Variable Map</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeStateGraph#getPackageToFeatureMap <em>Package To Feature Map</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeStateGraph#getBdd <em>Bdd</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeStateGraph#getFd <em>Fd</em>}</li>
 * </ul>
 * </p>
 *
 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph()
 * @model
 * @generated
 */
public interface FeaturedMSDRuntimeStateGraph extends MSDRuntimeStateGraph {
	/**
	 * Returns the value of the '<em><b>Package To Interaction List Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Package},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Interaction},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package To Interaction List Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package To Interaction List Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap()
	 * @model mapType="plruntime.PackageToInteractionListMapEntry<org.eclipse.uml2.uml.Package, org.eclipse.uml2.uml.Interaction>"
	 *        extendedMetaData="name='packageToInteractionListMap'"
	 * @generated
	 */
	EMap<org.eclipse.uml2.uml.Package, EList<Interaction>> getPackageToInteractionListMap();

	/**
	 * Returns the value of the '<em><b>Feature To Bdd Variable Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Component},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature To Bdd Variable Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature To Bdd Variable Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph_FeatureToBddVariableMap()
	 * @model mapType="plruntime.FeatureToBddVariableMapEntry<org.eclipse.uml2.uml.Component, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Component, Integer> getFeatureToBddVariableMap();

	/**
	 * Returns the value of the '<em><b>Package To Feature Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Package},
	 * and the value is of type {@link org.eclipse.uml2.uml.Component},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package To Feature Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package To Feature Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph_PackageToFeatureMap()
	 * @model mapType="plruntime.PackageToFeatureMapEntry<org.eclipse.uml2.uml.Package, org.eclipse.uml2.uml.Component>"
	 * @generated
	 */
	EMap<org.eclipse.uml2.uml.Package, Component> getPackageToFeatureMap();

	/**
	 * Returns the value of the '<em><b>Bdd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bdd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bdd</em>' attribute.
	 * @see #setBdd(BDD)
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph_Bdd()
	 * @model dataType="plruntime.bdd"
	 * @generated
	 */
	BDD getBdd();

	/**
	 * Sets the value of the '{@link plruntime.FeaturedMSDRuntimeStateGraph#getBdd <em>Bdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bdd</em>' attribute.
	 * @see #getBdd()
	 * @generated
	 */
	void setBdd(BDD value);

	/**
	 * Returns the value of the '<em><b>Fd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fd</em>' attribute.
	 * @see #setFd(int)
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeStateGraph_Fd()
	 * @model dataType="org.eclipse.uml2.types.Integer"
	 * @generated
	 */
	int getFd();

	/**
	 * Sets the value of the '{@link plruntime.FeaturedMSDRuntimeStateGraph#getFd <em>Fd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fd</em>' attribute.
	 * @see #getFd()
	 * @generated
	 */
	void setFd(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * initializes the FeaturedMSDRuntimeStateGraph and returns the start FeaturedMSDRuntimeState.
	 * (Leads to the creation of an ElementContainer and the creation+initialization of a RuntimeUtil)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	FeaturedMSDRuntimeState init(ScenarioRunConfiguration scenarioRunConfiguration);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="plruntime.TransitionToFeatureExpressionMapEntry<org.scenariotools.stategraph.Transition, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Transition, Integer> generateAllFeaturedSuccessors(RuntimeState runtimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="plruntime.TransitionToFeatureExpressionMapEntry<org.scenariotools.stategraph.Transition, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Transition, Integer> generateAllFeaturedSuccessorsForTheSameEvent(RuntimeState runtimeState, Event event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.eclipse.uml2.types.Integer" bddDataType="plruntime.bdd"
	 * @generated
	 */
	int computeAnd(BDD bdd);
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.eclipse.uml2.types.Integer" 
	 * @generated NOT
	 */
	int computeAnd();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	Set<Integer> computePowerSet(List<Package> productPackages);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	Integer createVar(Component childFeature);

	
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @model
//	 * @generated NOT
//	 */
//	Integer computeAnd(BDD bdd);
} // FeaturedMSDRuntimeStateGraph
