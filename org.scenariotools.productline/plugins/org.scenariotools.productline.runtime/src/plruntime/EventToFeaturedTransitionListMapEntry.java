/**
 */
package plruntime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.events.Event;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event To Featured Transition List Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link plruntime.EventToFeaturedTransitionListMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link plruntime.EventToFeaturedTransitionListMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see plruntime.PlruntimePackage#getEventToFeaturedTransitionListMapEntry()
 * @model
 * @generated
 */
public interface EventToFeaturedTransitionListMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Event)
	 * @see plruntime.PlruntimePackage#getEventToFeaturedTransitionListMapEntry_Key()
	 * @model
	 * @generated
	 */
	Event getKey();

	/**
	 * Sets the value of the '{@link plruntime.EventToFeaturedTransitionListMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Event value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link plruntime.TransitionToFeatureExpressionMapEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see plruntime.PlruntimePackage#getEventToFeaturedTransitionListMapEntry_Value()
	 * @model
	 * @generated
	 */
	EList<TransitionToFeatureExpressionMapEntry> getValue();

} // EventToFeaturedTransitionListMapEntry
