/**
 */
package plruntime;

import java.util.Map;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.uml2.uml.Interaction;

import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;

import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Featured MSD Runtime State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionMap <em>Transition To Feature Expression Map</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeState#getEventToFeaturedTransitionListMap <em>Event To Featured Transition List Map</em>}</li>
 *   <li>{@link plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionStringMap <em>Transition To Feature Expression String Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeState()
 * @model
 * @generated
 */
public interface FeaturedMSDRuntimeState extends MSDRuntimeState {
	/**
	 * Returns the value of the '<em><b>Transition To Feature Expression Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.stategraph.Transition},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition To Feature Expression Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition To Feature Expression Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeState_TransitionToFeatureExpressionMap()
	 * @model mapType="plruntime.TransitionToFeatureExpressionMapEntry<org.scenariotools.stategraph.Transition, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Transition, Integer> getTransitionToFeatureExpressionMap();

	/**
	 * Returns the value of the '<em><b>Event To Featured Transition List Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.events.Event},
	 * and the value is of type list of {@link java.util.Map.Entry<org.scenariotools.stategraph.Transition, java.lang.Integer>},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event To Featured Transition List Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event To Featured Transition List Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeState_EventToFeaturedTransitionListMap()
	 * @model mapType="plruntime.EventToFeaturedTransitionListMapEntry<org.scenariotools.events.Event, plruntime.TransitionToFeatureExpressionMapEntry>"
	 * @generated
	 */
	EMap<Event, EMap<Transition, Integer>> getEventToFeaturedTransitionListMap();

	/**
	 * Returns the value of the '<em><b>Transition To Feature Expression String Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.stategraph.Transition},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition To Feature Expression String Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition To Feature Expression String Map</em>' map.
	 * @see plruntime.PlruntimePackage#getFeaturedMSDRuntimeState_TransitionToFeatureExpressionStringMap()
	 * @model mapType="plruntime.TransitionToFeatureExpressionStringMapEntry<org.scenariotools.stategraph.Transition, org.eclipse.uml2.types.String>"
	 * @generated
	 */
	EMap<Transition, String> getTransitionToFeatureExpressionStringMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<ActiveMSD> computeActiveMSDs(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model interactionListMany="true"
	 * @generated
	 */
	void performStep(MessageEvent messageEvent, EList<Interaction> interactionList);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void printInfoAboutState();
} // FeaturedMSDRuntimeState
