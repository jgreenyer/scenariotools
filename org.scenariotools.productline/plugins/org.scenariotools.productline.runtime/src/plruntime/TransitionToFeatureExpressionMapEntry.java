/**
 */
package plruntime;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition To Feature Expression Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link plruntime.TransitionToFeatureExpressionMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link plruntime.TransitionToFeatureExpressionMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see plruntime.PlruntimePackage#getTransitionToFeatureExpressionMapEntry()
 * @model
 * @generated
 */
public interface TransitionToFeatureExpressionMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Transition)
	 * @see plruntime.PlruntimePackage#getTransitionToFeatureExpressionMapEntry_Key()
	 * @model
	 * @generated
	 */
	Transition getKey();

	/**
	 * Sets the value of the '{@link plruntime.TransitionToFeatureExpressionMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Transition value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Integer)
	 * @see plruntime.PlruntimePackage#getTransitionToFeatureExpressionMapEntry_Value()
	 * @model
	 * @generated
	 */
	Integer getValue();

	/**
	 * Sets the value of the '{@link plruntime.TransitionToFeatureExpressionMapEntry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Integer value);

} // TransitionToFeatureExpressionMapEntry
