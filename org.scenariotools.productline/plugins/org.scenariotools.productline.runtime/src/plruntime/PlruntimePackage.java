/**
 */
package plruntime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see plruntime.PlruntimeFactory
 * @model kind="package"
 * @generated
 */
public interface PlruntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "plruntime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://plruntime.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "plruntime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlruntimePackage eINSTANCE = plruntime.impl.PlruntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl <em>Featured MSD Runtime State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.FeaturedMSDRuntimeStateGraphImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__STATES = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__START_STATE = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__START_STATE;

	/**
	 * The feature id for the '<em><b>Strategy Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__STRATEGY_KIND = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__STRATEGY_KIND;

	/**
	 * The feature id for the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Key Wrapper To MSD Runtime State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP;

	/**
	 * The feature id for the '<em><b>Package To Interaction List Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature To Bdd Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Package To Feature Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Bdd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Fd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH__FD = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Featured MSD Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link plruntime.impl.FeaturedMSDRuntimeStateImpl <em>Featured MSD Runtime State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.FeaturedMSDRuntimeStateImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getFeaturedMSDRuntimeState()
	 * @generated
	 */
	int FEATURED_MSD_RUNTIME_STATE = 1;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__OUTGOING_TRANSITION = RuntimePackage.MSD_RUNTIME_STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__INCOMING_TRANSITION = RuntimePackage.MSD_RUNTIME_STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__STATE_GRAPH = RuntimePackage.MSD_RUNTIME_STATE__STATE_GRAPH;


	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__LABEL = RuntimePackage.MSD_RUNTIME_STATE__LABEL;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__OBJECT_SYSTEM = RuntimePackage.MSD_RUNTIME_STATE__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = RuntimePackage.MSD_RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = RuntimePackage.MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = RuntimePackage.MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = RuntimePackage.MSD_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP;

	/**
	 * The feature id for the '<em><b>Active Processes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__ACTIVE_PROCESSES = RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__RUNTIME_UTIL = RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Active MSD Changed During Perform Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP = RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP;

	/**
	 * The feature id for the '<em><b>Transition To Feature Expression Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event To Featured Transition List Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transition To Feature Expression String Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Featured MSD Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURED_MSD_RUNTIME_STATE_FEATURE_COUNT = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link plruntime.impl.PackageToInteractionListMapEntryImpl <em>Package To Interaction List Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.PackageToInteractionListMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getPackageToInteractionListMapEntry()
	 * @generated
	 */
	int PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Package To Interaction List Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link plruntime.impl.EventToFeaturedTransitionListMapEntryImpl <em>Event To Featured Transition List Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.EventToFeaturedTransitionListMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getEventToFeaturedTransitionListMapEntry()
	 * @generated
	 */
	int EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Event To Featured Transition List Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link plruntime.impl.TransitionToFeatureExpressionMapEntryImpl <em>Transition To Feature Expression Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.TransitionToFeatureExpressionMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getTransitionToFeatureExpressionMapEntry()
	 * @generated
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Transition To Feature Expression Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link plruntime.impl.PackageToFeatureMapEntryImpl <em>Package To Feature Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.PackageToFeatureMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getPackageToFeatureMapEntry()
	 * @generated
	 */
	int PACKAGE_TO_FEATURE_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_FEATURE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_FEATURE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Package To Feature Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_FEATURE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link plruntime.impl.FeatureToBddVariableMapEntryImpl <em>Feature To Bdd Variable Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.FeatureToBddVariableMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getFeatureToBddVariableMapEntry()
	 * @generated
	 */
	int FEATURE_TO_BDD_VARIABLE_MAP_ENTRY = 6;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Feature To Bdd Variable Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_BDD_VARIABLE_MAP_ENTRY_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link plruntime.impl.TransitionToFeatureExpressionStringMapEntryImpl <em>Transition To Feature Expression String Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plruntime.impl.TransitionToFeatureExpressionStringMapEntryImpl
	 * @see plruntime.impl.PlruntimePackageImpl#getTransitionToFeatureExpressionStringMapEntry()
	 * @generated
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY = 7;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Transition To Feature Expression String Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '<em>bdd</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jdd.bdd.BDD
	 * @see plruntime.impl.PlruntimePackageImpl#getbdd()
	 * @generated
	 */
	int BDD = 8;


	/**
	 * Returns the meta object for class '{@link plruntime.FeaturedMSDRuntimeStateGraph <em>Featured MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Featured MSD Runtime State Graph</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph
	 * @generated
	 */
	EClass getFeaturedMSDRuntimeStateGraph();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeStateGraph#getPackageToInteractionListMap <em>Package To Interaction List Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Package To Interaction List Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph#getPackageToInteractionListMap()
	 * @see #getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeStateGraph#getFeatureToBddVariableMap <em>Feature To Bdd Variable Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Feature To Bdd Variable Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph#getFeatureToBddVariableMap()
	 * @see #getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeStateGraph_FeatureToBddVariableMap();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeStateGraph#getPackageToFeatureMap <em>Package To Feature Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Package To Feature Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph#getPackageToFeatureMap()
	 * @see #getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeStateGraph_PackageToFeatureMap();

	/**
	 * Returns the meta object for the attribute '{@link plruntime.FeaturedMSDRuntimeStateGraph#getBdd <em>Bdd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bdd</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph#getBdd()
	 * @see #getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	EAttribute getFeaturedMSDRuntimeStateGraph_Bdd();

	/**
	 * Returns the meta object for the attribute '{@link plruntime.FeaturedMSDRuntimeStateGraph#getFd <em>Fd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fd</em>'.
	 * @see plruntime.FeaturedMSDRuntimeStateGraph#getFd()
	 * @see #getFeaturedMSDRuntimeStateGraph()
	 * @generated
	 */
	EAttribute getFeaturedMSDRuntimeStateGraph_Fd();

	/**
	 * Returns the meta object for class '{@link plruntime.FeaturedMSDRuntimeState <em>Featured MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Featured MSD Runtime State</em>'.
	 * @see plruntime.FeaturedMSDRuntimeState
	 * @generated
	 */
	EClass getFeaturedMSDRuntimeState();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionMap <em>Transition To Feature Expression Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Transition To Feature Expression Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionMap()
	 * @see #getFeaturedMSDRuntimeState()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeState_TransitionToFeatureExpressionMap();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeState#getEventToFeaturedTransitionListMap <em>Event To Featured Transition List Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Event To Featured Transition List Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeState#getEventToFeaturedTransitionListMap()
	 * @see #getFeaturedMSDRuntimeState()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeState_EventToFeaturedTransitionListMap();

	/**
	 * Returns the meta object for the map '{@link plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionStringMap <em>Transition To Feature Expression String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Transition To Feature Expression String Map</em>'.
	 * @see plruntime.FeaturedMSDRuntimeState#getTransitionToFeatureExpressionStringMap()
	 * @see #getFeaturedMSDRuntimeState()
	 * @generated
	 */
	EReference getFeaturedMSDRuntimeState_TransitionToFeatureExpressionStringMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Package To Interaction List Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package To Interaction List Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Package"
	 *        valueType="org.eclipse.uml2.uml.Interaction" valueMany="true"
	 * @generated
	 */
	EClass getPackageToInteractionListMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToInteractionListMapEntry()
	 * @generated
	 */
	EReference getPackageToInteractionListMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToInteractionListMapEntry()
	 * @generated
	 */
	EReference getPackageToInteractionListMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Event To Featured Transition List Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event To Featured Transition List Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.events.Event"
	 *        valueMapType="plruntime.TransitionToFeatureExpressionMapEntry<org.scenariotools.stategraph.Transition, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EClass getEventToFeaturedTransitionListMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToFeaturedTransitionListMapEntry()
	 * @generated
	 */
	EReference getEventToFeaturedTransitionListMapEntry_Key();

	/**
	 * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToFeaturedTransitionListMapEntry()
	 * @generated
	 */
	EReference getEventToFeaturedTransitionListMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Transition To Feature Expression Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition To Feature Expression Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.stategraph.Transition"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getTransitionToFeatureExpressionMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTransitionToFeatureExpressionMapEntry()
	 * @generated
	 */
	EReference getTransitionToFeatureExpressionMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTransitionToFeatureExpressionMapEntry()
	 * @generated
	 */
	EAttribute getTransitionToFeatureExpressionMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Package To Feature Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package To Feature Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Package"
	 *        valueType="org.eclipse.uml2.uml.Component"
	 * @generated
	 */
	EClass getPackageToFeatureMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToFeatureMapEntry()
	 * @generated
	 */
	EReference getPackageToFeatureMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToFeatureMapEntry()
	 * @generated
	 */
	EReference getPackageToFeatureMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Feature To Bdd Variable Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature To Bdd Variable Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Component"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getFeatureToBddVariableMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFeatureToBddVariableMapEntry()
	 * @generated
	 */
	EReference getFeatureToBddVariableMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFeatureToBddVariableMapEntry()
	 * @generated
	 */
	EAttribute getFeatureToBddVariableMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Transition To Feature Expression String Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition To Feature Expression String Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.stategraph.Transition"
	 *        valueDataType="org.eclipse.uml2.types.String"
	 * @generated
	 */
	EClass getTransitionToFeatureExpressionStringMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTransitionToFeatureExpressionStringMapEntry()
	 * @generated
	 */
	EReference getTransitionToFeatureExpressionStringMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getTransitionToFeatureExpressionStringMapEntry()
	 * @generated
	 */
	EAttribute getTransitionToFeatureExpressionStringMapEntry_Value();

	/**
	 * Returns the meta object for data type '{@link jdd.bdd.BDD <em>bdd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>bdd</em>'.
	 * @see jdd.bdd.BDD
	 * @model instanceClass="jdd.bdd.BDD"
	 * @generated
	 */
	EDataType getbdd();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PlruntimeFactory getPlruntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl <em>Featured MSD Runtime State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.FeaturedMSDRuntimeStateGraphImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getFeaturedMSDRuntimeStateGraph()
		 * @generated
		 */
		EClass FEATURED_MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getFeaturedMSDRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Package To Interaction List Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP = eINSTANCE.getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap();

		/**
		 * The meta object literal for the '<em><b>Feature To Bdd Variable Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP = eINSTANCE.getFeaturedMSDRuntimeStateGraph_FeatureToBddVariableMap();

		/**
		 * The meta object literal for the '<em><b>Package To Feature Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP = eINSTANCE.getFeaturedMSDRuntimeStateGraph_PackageToFeatureMap();

		/**
		 * The meta object literal for the '<em><b>Bdd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD = eINSTANCE.getFeaturedMSDRuntimeStateGraph_Bdd();

		/**
		 * The meta object literal for the '<em><b>Fd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURED_MSD_RUNTIME_STATE_GRAPH__FD = eINSTANCE.getFeaturedMSDRuntimeStateGraph_Fd();

		/**
		 * The meta object literal for the '{@link plruntime.impl.FeaturedMSDRuntimeStateImpl <em>Featured MSD Runtime State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.FeaturedMSDRuntimeStateImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getFeaturedMSDRuntimeState()
		 * @generated
		 */
		EClass FEATURED_MSD_RUNTIME_STATE = eINSTANCE.getFeaturedMSDRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Transition To Feature Expression Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP = eINSTANCE.getFeaturedMSDRuntimeState_TransitionToFeatureExpressionMap();

		/**
		 * The meta object literal for the '<em><b>Event To Featured Transition List Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP = eINSTANCE.getFeaturedMSDRuntimeState_EventToFeaturedTransitionListMap();

		/**
		 * The meta object literal for the '<em><b>Transition To Feature Expression String Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP = eINSTANCE.getFeaturedMSDRuntimeState_TransitionToFeatureExpressionStringMap();

		/**
		 * The meta object literal for the '{@link plruntime.impl.PackageToInteractionListMapEntryImpl <em>Package To Interaction List Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.PackageToInteractionListMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getPackageToInteractionListMapEntry()
		 * @generated
		 */
		EClass PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY = eINSTANCE.getPackageToInteractionListMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__KEY = eINSTANCE.getPackageToInteractionListMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__VALUE = eINSTANCE.getPackageToInteractionListMapEntry_Value();

		/**
		 * The meta object literal for the '{@link plruntime.impl.EventToFeaturedTransitionListMapEntryImpl <em>Event To Featured Transition List Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.EventToFeaturedTransitionListMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getEventToFeaturedTransitionListMapEntry()
		 * @generated
		 */
		EClass EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY = eINSTANCE.getEventToFeaturedTransitionListMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__KEY = eINSTANCE.getEventToFeaturedTransitionListMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__VALUE = eINSTANCE.getEventToFeaturedTransitionListMapEntry_Value();

		/**
		 * The meta object literal for the '{@link plruntime.impl.TransitionToFeatureExpressionMapEntryImpl <em>Transition To Feature Expression Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.TransitionToFeatureExpressionMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getTransitionToFeatureExpressionMapEntry()
		 * @generated
		 */
		EClass TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY = eINSTANCE.getTransitionToFeatureExpressionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__KEY = eINSTANCE.getTransitionToFeatureExpressionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__VALUE = eINSTANCE.getTransitionToFeatureExpressionMapEntry_Value();

		/**
		 * The meta object literal for the '{@link plruntime.impl.PackageToFeatureMapEntryImpl <em>Package To Feature Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.PackageToFeatureMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getPackageToFeatureMapEntry()
		 * @generated
		 */
		EClass PACKAGE_TO_FEATURE_MAP_ENTRY = eINSTANCE.getPackageToFeatureMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_FEATURE_MAP_ENTRY__KEY = eINSTANCE.getPackageToFeatureMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_FEATURE_MAP_ENTRY__VALUE = eINSTANCE.getPackageToFeatureMapEntry_Value();

		/**
		 * The meta object literal for the '{@link plruntime.impl.FeatureToBddVariableMapEntryImpl <em>Feature To Bdd Variable Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.FeatureToBddVariableMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getFeatureToBddVariableMapEntry()
		 * @generated
		 */
		EClass FEATURE_TO_BDD_VARIABLE_MAP_ENTRY = eINSTANCE.getFeatureToBddVariableMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__KEY = eINSTANCE.getFeatureToBddVariableMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__VALUE = eINSTANCE.getFeatureToBddVariableMapEntry_Value();

		/**
		 * The meta object literal for the '{@link plruntime.impl.TransitionToFeatureExpressionStringMapEntryImpl <em>Transition To Feature Expression String Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plruntime.impl.TransitionToFeatureExpressionStringMapEntryImpl
		 * @see plruntime.impl.PlruntimePackageImpl#getTransitionToFeatureExpressionStringMapEntry()
		 * @generated
		 */
		EClass TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY = eINSTANCE.getTransitionToFeatureExpressionStringMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__KEY = eINSTANCE.getTransitionToFeatureExpressionStringMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__VALUE = eINSTANCE.getTransitionToFeatureExpressionStringMapEntry_Value();

		/**
		 * The meta object literal for the '<em>bdd</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jdd.bdd.BDD
		 * @see plruntime.impl.PlruntimePackageImpl#getbdd()
		 * @generated
		 */
		EDataType BDD = eINSTANCE.getbdd();

	}

} //PlruntimePackage
