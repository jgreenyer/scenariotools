/**
 */
package plruntime.impl;

import java.util.Map;

import jdd.bdd.BDD;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interaction;

import org.scenariotools.events.Event;
import org.scenariotools.stategraph.Transition;
import plruntime.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlruntimeFactoryImpl extends EFactoryImpl implements PlruntimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PlruntimeFactory init() {
		try {
			PlruntimeFactory thePlruntimeFactory = (PlruntimeFactory)EPackage.Registry.INSTANCE.getEFactory(PlruntimePackage.eNS_URI);
			if (thePlruntimeFactory != null) {
				return thePlruntimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PlruntimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlruntimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH: return createFeaturedMSDRuntimeStateGraph();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE: return createFeaturedMSDRuntimeState();
			case PlruntimePackage.PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY: return (EObject)createPackageToInteractionListMapEntry();
			case PlruntimePackage.EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY: return (EObject)createEventToFeaturedTransitionListMapEntry();
			case PlruntimePackage.TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY: return (EObject)createTransitionToFeatureExpressionMapEntry();
			case PlruntimePackage.PACKAGE_TO_FEATURE_MAP_ENTRY: return (EObject)createPackageToFeatureMapEntry();
			case PlruntimePackage.FEATURE_TO_BDD_VARIABLE_MAP_ENTRY: return (EObject)createFeatureToBddVariableMapEntry();
			case PlruntimePackage.TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY: return (EObject)createTransitionToFeatureExpressionStringMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case PlruntimePackage.BDD:
				//return createbddFromString(eDataType, initialValue);
				System.out.println("createFromString case BDD initialValue "+initialValue);
				return null;
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case PlruntimePackage.BDD:
				return convertbddToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeaturedMSDRuntimeStateGraph createFeaturedMSDRuntimeStateGraph() {
		FeaturedMSDRuntimeStateGraphImpl featuredMSDRuntimeStateGraph = new FeaturedMSDRuntimeStateGraphImpl();
		return featuredMSDRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeaturedMSDRuntimeState createFeaturedMSDRuntimeState() {
		FeaturedMSDRuntimeStateImpl featuredMSDRuntimeState = new FeaturedMSDRuntimeStateImpl();
		return featuredMSDRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<org.eclipse.uml2.uml.Package, EList<Interaction>> createPackageToInteractionListMapEntry() {
		PackageToInteractionListMapEntryImpl packageToInteractionListMapEntry = new PackageToInteractionListMapEntryImpl();
		return packageToInteractionListMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Event, EMap<Transition, Integer>> createEventToFeaturedTransitionListMapEntry() {
		EventToFeaturedTransitionListMapEntryImpl eventToFeaturedTransitionListMapEntry = new EventToFeaturedTransitionListMapEntryImpl();
		return eventToFeaturedTransitionListMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Transition, Integer> createTransitionToFeatureExpressionMapEntry() {
		TransitionToFeatureExpressionMapEntryImpl transitionToFeatureExpressionMapEntry = new TransitionToFeatureExpressionMapEntryImpl();
		return transitionToFeatureExpressionMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<org.eclipse.uml2.uml.Package, Component> createPackageToFeatureMapEntry() {
		PackageToFeatureMapEntryImpl packageToFeatureMapEntry = new PackageToFeatureMapEntryImpl();
		return packageToFeatureMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Component, Integer> createFeatureToBddVariableMapEntry() {
		FeatureToBddVariableMapEntryImpl featureToBddVariableMapEntry = new FeatureToBddVariableMapEntryImpl();
		return featureToBddVariableMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Transition, String> createTransitionToFeatureExpressionStringMapEntry() {
		TransitionToFeatureExpressionStringMapEntryImpl transitionToFeatureExpressionStringMapEntry = new TransitionToFeatureExpressionStringMapEntryImpl();
		return transitionToFeatureExpressionStringMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BDD createbddFromString(EDataType eDataType, String initialValue) {
		return (BDD)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertbddToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlruntimePackage getPlruntimePackage() {
		return (PlruntimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PlruntimePackage getPackage() {
		return PlruntimePackage.eINSTANCE;
	}

} //PlruntimeFactoryImpl
