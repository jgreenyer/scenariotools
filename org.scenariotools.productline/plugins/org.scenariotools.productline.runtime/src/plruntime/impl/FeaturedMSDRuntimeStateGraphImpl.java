/**
 */
package plruntime.impl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import jdd.bdd.BDD;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.util.RuntimeUtil;
//import org.scenariotools.msd.util.RuntimeUtil.MSDRuntimeStateCopier;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.StategraphPackage;
import org.scenariotools.stategraph.Transition;

import plruntime.FeaturedMSDRuntimeState;
import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimeFactory;
import plruntime.PlruntimePackage;
//import org.scenariotools.msd.runtime.plugin.Activator;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Featured MSD Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl#getPackageToInteractionListMap <em>Package To Interaction List Map</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl#getFeatureToBddVariableMap <em>Feature To Bdd Variable Map</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl#getPackageToFeatureMap <em>Package To Feature Map</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl#getBdd <em>Bdd</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateGraphImpl#getFd <em>Fd</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FeaturedMSDRuntimeStateGraphImpl extends MSDRuntimeStateGraphImpl implements FeaturedMSDRuntimeStateGraph {
	/**
	 * The cached value of the '{@link #getPackageToInteractionListMap() <em>Package To Interaction List Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageToInteractionListMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<org.eclipse.uml2.uml.Package, EList<Interaction>> packageToInteractionListMap;

	/**
	 * The cached value of the '{@link #getFeatureToBddVariableMap() <em>Feature To Bdd Variable Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureToBddVariableMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Component, Integer> featureToBddVariableMap;

	/**
	 * The cached value of the '{@link #getPackageToFeatureMap() <em>Package To Feature Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageToFeatureMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<org.eclipse.uml2.uml.Package, Component> packageToFeatureMap;

	/**
	 * The default value of the '{@link #getBdd() <em>Bdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBdd()
	 * @generated
	 * @ordered
	 */
	protected static final BDD BDD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBdd() <em>Bdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBdd()
	 * @generated
	 * @ordered
	 */
	protected BDD bdd = BDD_EDEFAULT;

	/**
	 * The default value of the '{@link #getFd() <em>Fd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFd()
	 * @generated
	 * @ordered
	 */
	protected static final int FD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFd() <em>Fd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFd()
	 * @generated
	 * @ordered
	 */
	protected int fd = FD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeaturedMSDRuntimeStateGraphImpl() {
		super();
	}

	//	/**
	//	 * <!-- begin-user-doc -->
	//	 * <!-- end-user-doc -->
	//	 * @generated NOT
	//	 */
	//	protected BDD bdd =  new BDD(10000,10000);



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlruntimePackage.Literals.FEATURED_MSD_RUNTIME_STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<org.eclipse.uml2.uml.Package, EList<Interaction>> getPackageToInteractionListMap() {
		if (packageToInteractionListMap == null) {
			packageToInteractionListMap = new EcoreEMap<org.eclipse.uml2.uml.Package,EList<Interaction>>(PlruntimePackage.Literals.PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY, PackageToInteractionListMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP);
		}
		return packageToInteractionListMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Component, Integer> getFeatureToBddVariableMap() {
		if (featureToBddVariableMap == null) {
			featureToBddVariableMap = new EcoreEMap<Component,Integer>(PlruntimePackage.Literals.FEATURE_TO_BDD_VARIABLE_MAP_ENTRY, FeatureToBddVariableMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP);
		}
		return featureToBddVariableMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<org.eclipse.uml2.uml.Package, Component> getPackageToFeatureMap() {
		if (packageToFeatureMap == null) {
			packageToFeatureMap = new EcoreEMap<org.eclipse.uml2.uml.Package,Component>(PlruntimePackage.Literals.PACKAGE_TO_FEATURE_MAP_ENTRY, PackageToFeatureMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP);
		}
		return packageToFeatureMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BDD getBdd() {
		return bdd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Integer createVar(Component childFeature){
		int child = bdd.createVar();
		getFeatureToBddVariableMap().put(childFeature, child);
		return child;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBdd(BDD newBdd) {
		BDD oldBdd = bdd;
		bdd = newBdd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD, oldBdd, bdd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FeaturedMSDRuntimeState init(ScenarioRunConfiguration scenarioRunConfiguration) {
		setScenarioRunConfiguration(scenarioRunConfiguration);
		setElementContainer(RuntimeFactory.eINSTANCE.createElementContainer());

		setRuntimeUtil(UtilFactory.eINSTANCE.createRuntimeUtil());

		InteractionFragment finalFragment = UMLFactory.eINSTANCE
				.createOccurrenceSpecification();
		getElementContainer().setFinalFragment(finalFragment);
		getRuntimeUtil().setFinalFragment(finalFragment);

		MSDObjectSystem msdObjectSystem = RuntimeFactory.eINSTANCE
				.createMSDObjectSystem();
//		msdObjectSystem.setRuntimeUtil(getRuntimeUtil());

//		msdObjectSystem.getRootObjects().addAll(
//				getScenarioRunConfiguration().getSimulationRootObjects());

		FeaturedMSDRuntimeState initialMSDRuntimeState = PlruntimeFactory.eINSTANCE
				.createFeaturedMSDRuntimeState();
		initialMSDRuntimeState.setRuntimeUtil(getRuntimeUtil());
		initialMSDRuntimeState.setObjectSystem(msdObjectSystem);

		setStartState(initialMSDRuntimeState);

		getRuntimeUtil().init();
		
		getRuntimeUtil().init(scenarioRunConfiguration.getUml2EcoreMapping(), 
				scenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered());

		msdObjectSystem.init(scenarioRunConfiguration.getSimulationRootObjects(),
				scenarioRunConfiguration.getRoleToEObjectMappingContainer(),
				getRuntimeUtil());

		initializeInitialMSDRuntimeState(initialMSDRuntimeState, scenarioRunConfiguration.getExecutionSemantics());

		// "register" start state.
		getMSDRuntimeState(initialMSDRuntimeState, new BasicEList<ActiveProcess>());

		//CREATE BDD VAR
		bdd= new BDD(1000,1000);

		return initialMSDRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<Transition, Integer> generateAllFeaturedSuccessors(RuntimeState runtimeState) {
				
		EMap<Transition, Integer> allFeaturedTransitions = new BasicEMap<Transition, Integer>();
		Set<MessageEvent> nextEvents = new HashSet<>(runtimeState.getMessageEventToModalMessageEventMap().keySet());
		//eliminate redundancy for self-transitions
		nextEvents = eliminateRedundancy(nextEvents);

		if(runtimeState.equals(startState)){
			for (MessageEvent messageEvent : nextEvents) {
				allFeaturedTransitions.putAll(generateAllFeaturedSuccessorsForTheSameEvent(runtimeState, messageEvent));
			}
		}
		else{
			//Generate successors for each event
			for (MessageEvent messageEvent : nextEvents) {

				if(!((FeaturedMSDRuntimeState)startState).getMessageEventToModalMessageEventMap().keySet().contains(messageEvent))
					allFeaturedTransitions.putAll(generateAllFeaturedSuccessorsForTheSameEvent(runtimeState, messageEvent));
			}
		}

		//System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		return allFeaturedTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Set<MessageEvent> eliminateRedundancy(Set<MessageEvent> nextEvents) {
		Iterator<MessageEvent> iter = nextEvents.iterator();

		while (iter.hasNext()) {
			MessageEvent me = iter.next();

			if(!(me.isConcrete())){
				iter.remove();
			}
		}
		return nextEvents;
	}

	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState,
			boolean forAssumptions) {
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();
			if (forAssumptions) {
				if (modalMessageEvent.getAssumptionsModality().isMandatory()){
					System.out.println("Active message "+ messageEventToModalMessageEventMapEntry.getKey().getMessageName());
					return true;
				}
			} else {
				if (modalMessageEvent.getRequirementsModality().isMandatory())
					return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<Transition, Integer> generateAllFeaturedSuccessorsForTheSameEvent(RuntimeState runtimeState, Event event) {

		MessageEvent messageEvent = (MessageEvent) event;
		Assert.isTrue((event instanceof MessageEvent) && ((MessageEvent) event).isConcrete());

		getPackageToInteractionListMap().clear();

		int fe;
		int feTemp;

		if (((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().get(messageEvent) != null){
			//System.out.println("EventToFeaturedTransitionListMap already exists ("+((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().get(messageEvent).size()+" elements)");			

			EMap<Transition, Integer> transitionToFEMap = ((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().get(messageEvent);

			System.out.println("*****exited**********************************************************************************");

			return transitionToFEMap;
		}


		EMap<Transition,Integer> featuredTransitionList = new BasicEMap<Transition,Integer>();
		EMap<Transition,String> featuredTransitionStringList = new BasicEMap<Transition,String>();


		EList<Interaction> interactionList = new BasicEList<Interaction>();
		//(1) compute MSDs activated by the event
		EList<ActiveMSD> amsdList= ((FeaturedMSDRuntimeState) runtimeState).computeActiveMSDs(messageEvent);

		//(2) build a PackageToInteractionListMap (or FeatureToInteractionListMap)
		for(ActiveMSD amsd : amsdList){
			//System.out.println("\t-" + amsd.getInteraction().getName().toString());
			Package p = amsd.getInteraction().allOwningPackages().get(0);
			Interaction i = amsd.getInteraction();

			if(!getPackageToInteractionListMap().containsKey(p)){
				EList<Interaction> interactionListForPackagep = new BasicEList<Interaction>();
				interactionListForPackagep.add(i);
				getPackageToInteractionListMap().put(p, interactionListForPackagep);
			}
			else{
				if(!getPackageToInteractionListMap().get(p).contains(i)){
					getPackageToInteractionListMap().get(p).add(i);
				}
			}
			interactionList.add(amsd.getInteraction());
		}

		//(3) Compute Powerset of packages
		Set<Set<Package>> powerSet = powerSet(getPackageToInteractionListMap().keySet());
		//System.out.println("powerset size: " + powerSet.size());

		//(4) Retrieve Interaction List corresponding to a combination of packages in the powerset
		for(Set<Package> combination: powerSet){
			EList<Interaction> interactionListForACombination = new BasicEList<Interaction>();
			//System.out.println("\nCombination: ");
			if(combination.size()==0){
				//System.out.println("\tno package activated");
			}
			//TODO case in which there is a second interaction in the same package
			//			if(combination.size()==0 && powerSet.size()>1){
			//				continue;
			//			}
//			else{
//				for(Package p: combination){
//					//System.out.println("\t"+p.getName());
//
//				}
//			}
			fe=1;
			bdd.ref(fe);
			for(Package p: getPackageToInteractionListMap().keySet()){
				//				System.out.println("pkg: " + p.getName().toString());
				if(combination.contains(p)){
					//					System.out.println("pkg IN combination");
					interactionListForACombination.addAll(getPackageToInteractionListMap().get(p));
					if(fe==0){
						if(getPackageToFeatureMap().keySet().contains(p)){
							Component feature = getPackageToFeatureMap().get(p);
							//System.out.println("feature: " + feature.getName().toString());

							fe=getFeatureToBddVariableMap().get(feature);
							bdd.ref(fe);

						}
					}
					else{
						feTemp = bdd.and(fe, getFeatureToBddVariableMap().get(getPackageToFeatureMap().get(p)));
						bdd.ref(feTemp);
						fe = feTemp;

					}
				}
				else{
					//					System.out.println("pkg OUT combination");
					if(fe==0){
						if(getPackageToFeatureMap().keySet().contains(p)){
							Component feature = getPackageToFeatureMap().get(p);
							fe=getFeatureToBddVariableMap().get(feature);
							feTemp = bdd.not(fe);
							bdd.ref(feTemp);
							fe = feTemp;

						}
					}
					else{
						feTemp = bdd.and(fe, bdd.not(getFeatureToBddVariableMap().get(getPackageToFeatureMap().get(p))));
						bdd.ref(feTemp);
						fe = feTemp;

					}
				}
			}

			//if the combination is not a valid product do nothing and continue with the next combination in the powerset
			if(bdd.and(fd, fe)==0)
				continue;

			FeaturedMSDRuntimeStateCopier msdRuntimeStateCopier = new FeaturedMSDRuntimeStateCopier();
			FeaturedMSDRuntimeState msdRuntimeStateCopy = (FeaturedMSDRuntimeState) msdRuntimeStateCopier.copy((FeaturedMSDRuntimeState) runtimeState, messageEvent);
			
			msdRuntimeStateCopy.setStateGraph(this);

			//msdRuntimeStateCopy.updateMSDModalMessageEvents(getScenarioRunConfiguration().getExecutionSemantics());

			msdRuntimeStateCopy.performStep((MessageEvent) event);

			EList<ActiveProcess> newActiveMSD = new BasicEList<ActiveProcess>();

			boolean inAmsd=false;

			//System.out.println("Activated msds: ");
			//for(ActiveMSD amsd : amsdList){
			//System.out.println("\tinteraction: "+((ActiveMSD) amsd).getInteraction().getName().toString());
			//}

			//System.out.println("Pkg related to msd changed during perform step (on copy)");
			for(ActiveProcess ap: msdRuntimeStateCopy.getActiveMSDChangedDuringPerformStep()){
				inAmsd=false;
				//System.out.println("\tinteraction: "+((ActiveMSD) ap).getInteraction().getName().toString());
				//System.out.println("is it in combination?");
				if(combination.contains(((ActiveMSD) ap).getInteraction().allOwningPackages().get(0))){
					//System.out.println("\tYES --> KEEP "+((ActiveMSD) ap).getInteraction().allOwningPackages().get(0).getLabel());
					newActiveMSD.add(ap);
				}
				else{
					for(ActiveMSD amsd : amsdList){
						if(((ActiveMSD) ap).getInteraction().equals(amsd.getInteraction()))
							inAmsd=true;
					}
					if(inAmsd){
						//System.out.println("\t\tYES --> DON'T KEEP "+((ActiveMSD) ap).getInteraction().allOwningPackages().get(0).getLabel());
					}
					else{
						newActiveMSD.add(ap);
					}
				}

			}

			//remove duplicates
			newActiveMSD = new BasicEList<ActiveProcess>(new HashSet<ActiveProcess>(newActiveMSD));

			//modify newly activated msds in the copy
			msdRuntimeStateCopy.getActiveMSDChangedDuringPerformStep().clear();
			msdRuntimeStateCopy.getActiveMSDChangedDuringPerformStep().addAll(newActiveMSD);
			msdRuntimeStateCopy.getActiveProcesses().clear();
			msdRuntimeStateCopy.getActiveProcesses().addAll(newActiveMSD);


			//retrieve succ
			FeaturedMSDRuntimeState successorMSDRuntimeState = (FeaturedMSDRuntimeState) getMSDRuntimeState(
					msdRuntimeStateCopy,
					msdRuntimeStateCopier.getUnchangedActiveMSDsEList());
			
			if(successorMSDRuntimeState!=msdRuntimeStateCopy){
				disposeCopyOfMSDRuntimeState(msdRuntimeStateCopy);
			}

			//build transition
			Transition transition = StategraphFactory.eINSTANCE.createTransition();
			transition.setEvent(messageEvent);
			transition.setSourceState(runtimeState);
			transition.setTargetState(successorMSDRuntimeState);

			//build featuredTransitionList
			//System.out.println("Event: "+transition.getEvent());
			featuredTransitionList.put(transition, fe);

			//feature expression to string
			// Create a stream to hold the output
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			// IMPORTANT: Save the old System.out!
			PrintStream old = System.out;
			// Tell Java to use your special stream
			System.setOut(ps);
			// Print some output: goes to your special stream
			bdd.printSet(bdd.ref((Integer) fe));
			// Put things back
			System.out.flush();
			System.setOut(old);
			// Show what happened
			String feString = baos.toString();
			//System.out.println("FE: "+ feString);
			featuredTransitionStringList.put(transition, feString);

		}

		((FeaturedMSDRuntimeState) runtimeState).getTransitionToFeatureExpressionStringMap().putAll(featuredTransitionStringList);
		((FeaturedMSDRuntimeState) runtimeState).getTransitionToFeatureExpressionMap().putAll(featuredTransitionList);	

		((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().put(messageEvent, new BasicEMap<Transition, Integer>());
		((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().get(messageEvent).putAll(featuredTransitionList);

		//System.out.println("*****************************************************************");

		return featuredTransitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int computeAnd(BDD bdd) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int computeAnd() {

		int a= featureToBddVariableMap.get(0).getValue();
		int b= featureToBddVariableMap.get(1).getValue();
		System.out.println("a "+a);
		System.out.println("b "+b);

		int c = bdd.and(a, b);
		bdd.ref(c);
		bdd.printSet(c);
		return c;

	}

	private void printPackageInfoAboutTransition(Transition t, Integer fe, Set<Package> combination){

		System.out.println("\n\ntransition event: " + t.getEvent().toString());

		System.out.println("feature expression: ");
		bdd.printSet(fe);

		System.out.println("sourceState: " + t.getSourceState().toString());

		System.out.println("targetState: " + t.getTargetState().toString());


		if(combination!=null){
			System.out.println("combination size: "+ combination.size());
			for(Package p: combination){
				System.out.println("Package: " + p.getName().toString());
			}
		}
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public static <Package> Set<Set<Package>> powerSet(Set<Package> originalSet) {
		Set<Set<Package>> sets = new HashSet<Set<Package>>();
		if (originalSet.isEmpty()) {
			sets.add(new HashSet<Package>());
			return sets;
		}
		List<Package> list = new ArrayList<Package>(originalSet);
		Package head = list.get(0);
		Set<Package> rest = new HashSet<Package>(list.subList(1, list.size())); 
		for (Set<Package> set : powerSet(rest)) {
			Set<Package> newSet = new HashSet<Package>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}		
		return sets;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Set<Integer> computePowerSet(List<Package> originalSet) {

		Set<Set<Package>> sets = new HashSet<Set<Package>>();
		Set<Integer> feSet = new HashSet<Integer>();
		Integer fe;
		Integer feTemp;

		if (originalSet.isEmpty()) {
			fe = 1;
			bdd.ref(fe);
			feSet.add(fe);
			for(Integer f: feSet){
				bdd.ref(f);
				bdd.printSet(f);
			}
			return feSet;
		}
		List<Package> list = new ArrayList<Package>(originalSet);
		Package head = list.get(0);
		Set<Package> rest = new HashSet<Package>(list.subList(1, list.size())); 
		for (Set<Package> set : powerSet(rest)) {
			Set<Package> newSet = new HashSet<Package>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}		
		System.out.println("sets size: " + sets.size());

		for(Set<Package> combination: sets){
			System.out.println("combination size: " + combination.size());
			fe=1;
			bdd.ref(fe);
			for(Package p: originalSet){
				if(combination.contains(p)){
					if(fe==0){
						Component feature = getPackageToFeatureMap().get(p);
						fe=getFeatureToBddVariableMap().get(feature);
						bdd.ref(fe);
					}
					else{
						feTemp = bdd.and(fe, getFeatureToBddVariableMap().get(getPackageToFeatureMap().get(p)));
						bdd.ref(feTemp);
						fe = feTemp;
					}
				}
				else{
					if(fe==0){
						if(getPackageToFeatureMap().keySet().contains(p)){
							Component feature = getPackageToFeatureMap().get(p);
							fe=getFeatureToBddVariableMap().get(feature);
							feTemp = bdd.not(fe);
							bdd.ref(feTemp);
							fe = feTemp;

						}
					}
					else{
						feTemp = bdd.and(fe, bdd.not(getFeatureToBddVariableMap().get(getPackageToFeatureMap().get(p))));
						bdd.ref(feTemp);
						fe = feTemp;

					}
				}

			}

			feSet.add(fe);
		}

		for(Integer f: feSet){
			bdd.ref(f);
			bdd.printSet(f);
		}
		return feSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP:
				return ((InternalEList<?>)getPackageToInteractionListMap()).basicRemove(otherEnd, msgs);
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP:
				return ((InternalEList<?>)getFeatureToBddVariableMap()).basicRemove(otherEnd, msgs);
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP:
				return ((InternalEList<?>)getPackageToFeatureMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP:
				if (coreType) return getPackageToInteractionListMap();
				else return getPackageToInteractionListMap().map();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP:
				if (coreType) return getFeatureToBddVariableMap();
				else return getFeatureToBddVariableMap().map();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP:
				if (coreType) return getPackageToFeatureMap();
				else return getPackageToFeatureMap().map();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD:
				return getBdd();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FD:
				return getFd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP:
				((EStructuralFeature.Setting)getPackageToInteractionListMap()).set(newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP:
				((EStructuralFeature.Setting)getFeatureToBddVariableMap()).set(newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP:
				((EStructuralFeature.Setting)getPackageToFeatureMap()).set(newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD:
				setBdd((BDD)newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FD:
				setFd((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP:
				getPackageToInteractionListMap().clear();
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP:
				getFeatureToBddVariableMap().clear();
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP:
				getPackageToFeatureMap().clear();
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD:
				setBdd(BDD_EDEFAULT);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FD:
				setFd(FD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP:
				return packageToInteractionListMap != null && !packageToInteractionListMap.isEmpty();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP:
				return featureToBddVariableMap != null && !featureToBddVariableMap.isEmpty();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP:
				return packageToFeatureMap != null && !packageToFeatureMap.isEmpty();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD:
				return BDD_EDEFAULT == null ? bdd != null : !BDD_EDEFAULT.equals(bdd);
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE_GRAPH__FD:
				return fd != FD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bdd: ");
		result.append(bdd);
		result.append(", fd: ");
		result.append(fd);
		result.append(')');
		return result.toString();
	}


	public class FeaturedMSDRuntimeStateCopier {

		EList<ActiveProcess> unchangedActiveProcessesEList;

		public EList<ActiveProcess> getUnchangedActiveMSDsEList() {
			return unchangedActiveProcessesEList;
		}

		FeaturedMSDRuntimeState copiedMSDRuntimeState;
		MessageEvent copiedMessageEvent;
		RuntimeMessage copiedRuntimeMessage;

		public FeaturedMSDRuntimeState copy(FeaturedMSDRuntimeState featuredMSDRuntimeState, MessageEvent messageEvent){

			EList<EObject> objectsToCopy = new BasicEList<EObject>();

			objectsToCopy.add(featuredMSDRuntimeState);

			boolean supportDynamicObjectSystem = ((MSDRuntimeStateGraph) featuredMSDRuntimeState.getStateGraph()).getScenarioRunConfiguration().isDynamicObjectSystem();

			if (supportDynamicObjectSystem){
				objectsToCopy.add(featuredMSDRuntimeState.getObjectSystem());
				for (EObject rootEObject : featuredMSDRuntimeState.getObjectSystem()
						.getRootObjects()) {
					if (!featuredMSDRuntimeState.getObjectSystem().getRootObjectsContained().contains(rootEObject))
						objectsToCopy.add(rootEObject);
				}
			}

			unchangedActiveProcessesEList = new BasicEList<ActiveProcess>();

			for (ActiveProcess activeProcess : featuredMSDRuntimeState
					.getActiveProcesses()) {
				if(supportDynamicObjectSystem
						|| activeProcess.isRelavant(messageEvent)){
					objectsToCopy.add(activeProcess);
					objectsToCopy.add(activeProcess.getCurrentState());
					if (activeProcess instanceof ActiveMSD)
						objectsToCopy.add(((ActiveMSD) activeProcess)
								.getLifelineBindings());
					if (activeProcess instanceof ActiveMSS){
						objectsToCopy.add(((ActiveMSS) activeProcess)
								.getRoleBindings());
					}
					objectsToCopy.add(activeProcess.getVariableValuations());
				} else {
					unchangedActiveProcessesEList.add(activeProcess);
				}
			}

			// should be already a registered message event that will be copied anyways.
			//		objectsToCopy.add(0, messageEvent);
			//		objectsToCopy.add(1, messageEvent.getRuntimeMessage());


			//Collection<EObject> copiedObjects = EcoreUtil.copyAll(objectsToCopy);
			// instead do this to avoid outgoing transitions to get copied.
			Copier copier = new Copier() {
				@Override
				protected void copyContainment(EReference eReference,
						EObject eObject, EObject copyEObject) {
					if (eReference != StategraphPackage.eINSTANCE.getState_OutgoingTransition())
						super.copyContainment(eReference, eObject, copyEObject);
				}
			};
			Collection<EObject> copiedObjects = copier.copyAll(objectsToCopy);
			copier.copyReferences();

			copiedMessageEvent = (MessageEvent)copier.get(messageEvent);
			copiedMSDRuntimeState = (FeaturedMSDRuntimeState) copier.get(featuredMSDRuntimeState);

			if (supportDynamicObjectSystem){
				for (EObject rootEObject : copiedMSDRuntimeState.getObjectSystem().getRootObjects()) {
					if (!copiedMSDRuntimeState.getObjectSystem().getRootObjectsContained().contains(rootEObject))
						copiedMSDRuntimeState.getObjectSystem().getRootObjectsContained().add(rootEObject);
				}

				// update synch message event registry.
				Collection<MessageEvent> registeredMessageEvents = new BasicEList<MessageEvent>(((MSDObjectSystem)copiedMSDRuntimeState.getObjectSystem()).getMessageEventKeyWrapperToMessageEventMap().values());
				((MSDObjectSystem)copiedMSDRuntimeState.getObjectSystem()).getMessageEventKeyWrapperToMessageEventMap().clear();
				for (MessageEvent registeredMessageEvent : registeredMessageEvents) {
					((MSDObjectSystem)copiedMSDRuntimeState.getObjectSystem()).getMessageEventKeyWrapperToMessageEventMap().put(
							new MessageEventKeyWrapper(
									registeredMessageEvent.getSendingObject(), 
									registeredMessageEvent.getOperation(), 
									registeredMessageEvent.getReceivingObject(), 
									RuntimeUtil.Helper.getParameterValue(registeredMessageEvent), 
									registeredMessageEvent.eClass()), 
							registeredMessageEvent);
				}

			}

			copiedMSDRuntimeState.getEventToTransitionMap().clear();
			copiedMSDRuntimeState.getTransitionToFeatureExpressionMap().clear();
			copiedMSDRuntimeState.getEventToFeaturedTransitionListMap().clear();

			return copiedMSDRuntimeState;
		}
	}

	@Override
	public int getFd() {
		return fd;
	}

	@Override
	public void setFd(int value) {
		fd = value;
	}
}