/**
 */
package plruntime.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

import org.scenariotools.events.EventsPackage;

import org.scenariotools.msd.runtime.RuntimePackage;

import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;

import org.scenariotools.msd.util.UtilPackage;

import org.scenariotools.stategraph.StategraphPackage;

import plruntime.EventToFeaturedTransitionListMapEntry;
import plruntime.FeaturedMSDRuntimeState;
import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimeFactory;
import plruntime.PlruntimePackage;
import plruntime.TransitionToFeatureExpressionStringMapEntry;
import plruntime.TransitionToFeatureExpressionMapEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlruntimePackageImpl extends EPackageImpl implements PlruntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featuredMSDRuntimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featuredMSDRuntimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageToInteractionListMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventToFeaturedTransitionListMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionToFeatureExpressionMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageToFeatureMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureToBddVariableMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionToFeatureExpressionStringMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType bddEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see plruntime.PlruntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PlruntimePackageImpl() {
		super(eNS_URI, PlruntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PlruntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PlruntimePackage init() {
		if (isInited) return (PlruntimePackage)EPackage.Registry.INSTANCE.getEPackage(PlruntimePackage.eNS_URI);

		// Obtain or create and register package
		PlruntimePackageImpl thePlruntimePackage = (PlruntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PlruntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PlruntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();
		ScenariorunconfigurationPackage.eINSTANCE.eClass();
		UtilPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		thePlruntimePackage.createPackageContents();

		// Initialize created meta-data
		thePlruntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePlruntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PlruntimePackage.eNS_URI, thePlruntimePackage);
		return thePlruntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeaturedMSDRuntimeStateGraph() {
		return featuredMSDRuntimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap() {
		return (EReference)featuredMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeStateGraph_FeatureToBddVariableMap() {
		return (EReference)featuredMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeStateGraph_PackageToFeatureMap() {
		return (EReference)featuredMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeaturedMSDRuntimeStateGraph_Bdd() {
		return (EAttribute)featuredMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeaturedMSDRuntimeStateGraph_Fd() {
		return (EAttribute)featuredMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeaturedMSDRuntimeState() {
		return featuredMSDRuntimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeState_TransitionToFeatureExpressionMap() {
		return (EReference)featuredMSDRuntimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeState_EventToFeaturedTransitionListMap() {
		return (EReference)featuredMSDRuntimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturedMSDRuntimeState_TransitionToFeatureExpressionStringMap() {
		return (EReference)featuredMSDRuntimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageToInteractionListMapEntry() {
		return packageToInteractionListMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToInteractionListMapEntry_Key() {
		return (EReference)packageToInteractionListMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToInteractionListMapEntry_Value() {
		return (EReference)packageToInteractionListMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventToFeaturedTransitionListMapEntry() {
		return eventToFeaturedTransitionListMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToFeaturedTransitionListMapEntry_Key() {
		return (EReference)eventToFeaturedTransitionListMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToFeaturedTransitionListMapEntry_Value() {
		return (EReference)eventToFeaturedTransitionListMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransitionToFeatureExpressionMapEntry() {
		return transitionToFeatureExpressionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransitionToFeatureExpressionMapEntry_Key() {
		return (EReference)transitionToFeatureExpressionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransitionToFeatureExpressionMapEntry_Value() {
		return (EAttribute)transitionToFeatureExpressionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageToFeatureMapEntry() {
		return packageToFeatureMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToFeatureMapEntry_Key() {
		return (EReference)packageToFeatureMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToFeatureMapEntry_Value() {
		return (EReference)packageToFeatureMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureToBddVariableMapEntry() {
		return featureToBddVariableMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToBddVariableMapEntry_Key() {
		return (EReference)featureToBddVariableMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureToBddVariableMapEntry_Value() {
		return (EAttribute)featureToBddVariableMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransitionToFeatureExpressionStringMapEntry() {
		return transitionToFeatureExpressionStringMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransitionToFeatureExpressionStringMapEntry_Key() {
		return (EReference)transitionToFeatureExpressionStringMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransitionToFeatureExpressionStringMapEntry_Value() {
		return (EAttribute)transitionToFeatureExpressionStringMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getbdd() {
		return bddEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlruntimeFactory getPlruntimeFactory() {
		return (PlruntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		featuredMSDRuntimeStateGraphEClass = createEClass(FEATURED_MSD_RUNTIME_STATE_GRAPH);
		createEReference(featuredMSDRuntimeStateGraphEClass, FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_INTERACTION_LIST_MAP);
		createEReference(featuredMSDRuntimeStateGraphEClass, FEATURED_MSD_RUNTIME_STATE_GRAPH__FEATURE_TO_BDD_VARIABLE_MAP);
		createEReference(featuredMSDRuntimeStateGraphEClass, FEATURED_MSD_RUNTIME_STATE_GRAPH__PACKAGE_TO_FEATURE_MAP);
		createEAttribute(featuredMSDRuntimeStateGraphEClass, FEATURED_MSD_RUNTIME_STATE_GRAPH__BDD);
		createEAttribute(featuredMSDRuntimeStateGraphEClass, FEATURED_MSD_RUNTIME_STATE_GRAPH__FD);

		featuredMSDRuntimeStateEClass = createEClass(FEATURED_MSD_RUNTIME_STATE);
		createEReference(featuredMSDRuntimeStateEClass, FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP);
		createEReference(featuredMSDRuntimeStateEClass, FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP);
		createEReference(featuredMSDRuntimeStateEClass, FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP);

		packageToInteractionListMapEntryEClass = createEClass(PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY);
		createEReference(packageToInteractionListMapEntryEClass, PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__KEY);
		createEReference(packageToInteractionListMapEntryEClass, PACKAGE_TO_INTERACTION_LIST_MAP_ENTRY__VALUE);

		eventToFeaturedTransitionListMapEntryEClass = createEClass(EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY);
		createEReference(eventToFeaturedTransitionListMapEntryEClass, EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__KEY);
		createEReference(eventToFeaturedTransitionListMapEntryEClass, EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY__VALUE);

		transitionToFeatureExpressionMapEntryEClass = createEClass(TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY);
		createEReference(transitionToFeatureExpressionMapEntryEClass, TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__KEY);
		createEAttribute(transitionToFeatureExpressionMapEntryEClass, TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY__VALUE);

		packageToFeatureMapEntryEClass = createEClass(PACKAGE_TO_FEATURE_MAP_ENTRY);
		createEReference(packageToFeatureMapEntryEClass, PACKAGE_TO_FEATURE_MAP_ENTRY__KEY);
		createEReference(packageToFeatureMapEntryEClass, PACKAGE_TO_FEATURE_MAP_ENTRY__VALUE);

		featureToBddVariableMapEntryEClass = createEClass(FEATURE_TO_BDD_VARIABLE_MAP_ENTRY);
		createEReference(featureToBddVariableMapEntryEClass, FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__KEY);
		createEAttribute(featureToBddVariableMapEntryEClass, FEATURE_TO_BDD_VARIABLE_MAP_ENTRY__VALUE);

		transitionToFeatureExpressionStringMapEntryEClass = createEClass(TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY);
		createEReference(transitionToFeatureExpressionStringMapEntryEClass, TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__KEY);
		createEAttribute(transitionToFeatureExpressionStringMapEntryEClass, TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY__VALUE);

		// Create data types
		bddEDataType = createEDataType(BDD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		ScenariorunconfigurationPackage theScenariorunconfigurationPackage = (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);
		org.scenariotools.runtime.RuntimePackage theRuntimePackage_1 = (org.scenariotools.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.runtime.RuntimePackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		StategraphPackage theStategraphPackage = (StategraphPackage)EPackage.Registry.INSTANCE.getEPackage(StategraphPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		featuredMSDRuntimeStateGraphEClass.getESuperTypes().add(theRuntimePackage.getMSDRuntimeStateGraph());
		featuredMSDRuntimeStateEClass.getESuperTypes().add(theRuntimePackage.getMSDRuntimeState());

		// Initialize classes and features; add operations and parameters
		initEClass(featuredMSDRuntimeStateGraphEClass, FeaturedMSDRuntimeStateGraph.class, "FeaturedMSDRuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap(), this.getPackageToInteractionListMapEntry(), null, "packageToInteractionListMap", null, 0, -1, FeaturedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeaturedMSDRuntimeStateGraph_FeatureToBddVariableMap(), this.getFeatureToBddVariableMapEntry(), null, "featureToBddVariableMap", null, 0, -1, FeaturedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeaturedMSDRuntimeStateGraph_PackageToFeatureMap(), this.getPackageToFeatureMapEntry(), null, "packageToFeatureMap", null, 0, -1, FeaturedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeaturedMSDRuntimeStateGraph_Bdd(), this.getbdd(), "bdd", null, 0, 1, FeaturedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeaturedMSDRuntimeStateGraph_Fd(), theTypesPackage.getInteger(), "fd", null, 0, 1, FeaturedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(featuredMSDRuntimeStateGraphEClass, this.getFeaturedMSDRuntimeState(), "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theScenariorunconfigurationPackage.getScenarioRunConfiguration(), "scenarioRunConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(featuredMSDRuntimeStateGraphEClass, this.getTransitionToFeatureExpressionMapEntry(), "generateAllFeaturedSuccessors", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRuntimePackage_1.getRuntimeState(), "runtimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(featuredMSDRuntimeStateGraphEClass, this.getTransitionToFeatureExpressionMapEntry(), "generateAllFeaturedSuccessorsForTheSameEvent", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRuntimePackage_1.getRuntimeState(), "runtimeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(featuredMSDRuntimeStateGraphEClass, theTypesPackage.getInteger(), "computeAnd", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getbdd(), "bdd", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(featuredMSDRuntimeStateEClass, FeaturedMSDRuntimeState.class, "FeaturedMSDRuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeaturedMSDRuntimeState_TransitionToFeatureExpressionMap(), this.getTransitionToFeatureExpressionMapEntry(), null, "transitionToFeatureExpressionMap", null, 0, -1, FeaturedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeaturedMSDRuntimeState_EventToFeaturedTransitionListMap(), this.getEventToFeaturedTransitionListMapEntry(), null, "eventToFeaturedTransitionListMap", null, 0, -1, FeaturedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeaturedMSDRuntimeState_TransitionToFeatureExpressionStringMap(), this.getTransitionToFeatureExpressionStringMapEntry(), null, "transitionToFeatureExpressionStringMap", null, 0, -1, FeaturedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(featuredMSDRuntimeStateEClass, theRuntimePackage.getActiveMSD(), "computeActiveMSDs", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(featuredMSDRuntimeStateEClass, null, "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getInteraction(), "interactionList", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(packageToInteractionListMapEntryEClass, Map.Entry.class, "PackageToInteractionListMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPackageToInteractionListMapEntry_Key(), theUMLPackage.getPackage(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPackageToInteractionListMapEntry_Value(), theUMLPackage.getInteraction(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventToFeaturedTransitionListMapEntryEClass, Map.Entry.class, "EventToFeaturedTransitionListMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventToFeaturedTransitionListMapEntry_Key(), theEventsPackage.getEvent(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventToFeaturedTransitionListMapEntry_Value(), this.getTransitionToFeatureExpressionMapEntry(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionToFeatureExpressionMapEntryEClass, Map.Entry.class, "TransitionToFeatureExpressionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransitionToFeatureExpressionMapEntry_Key(), theStategraphPackage.getTransition(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransitionToFeatureExpressionMapEntry_Value(), theEcorePackage.getEIntegerObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(packageToFeatureMapEntryEClass, Map.Entry.class, "PackageToFeatureMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPackageToFeatureMapEntry_Key(), theUMLPackage.getPackage(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPackageToFeatureMapEntry_Value(), theUMLPackage.getComponent(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureToBddVariableMapEntryEClass, Map.Entry.class, "FeatureToBddVariableMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureToBddVariableMapEntry_Key(), theUMLPackage.getComponent(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureToBddVariableMapEntry_Value(), theEcorePackage.getEIntegerObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionToFeatureExpressionStringMapEntryEClass, Map.Entry.class, "TransitionToFeatureExpressionStringMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransitionToFeatureExpressionStringMapEntry_Key(), theStategraphPackage.getTransition(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransitionToFeatureExpressionStringMapEntry_Value(), theTypesPackage.getString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(bddEDataType, jdd.bdd.BDD.class, "bdd", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";			
		addAnnotation
		  (getFeaturedMSDRuntimeStateGraph_PackageToInteractionListMap(), 
		   source, 
		   new String[] {
			 "name", "packageToInteractionListMap"
		   });
	}

} //PlruntimePackageImpl
