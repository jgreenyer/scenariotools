/**
 */
package plruntime.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import jdd.bdd.BDD;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Interaction;
import org.scenariotools.events.Event;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;

import org.scenariotools.events.MessageEvent;

import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.RuntimeFactory;

import org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl;
import org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.msd.util.RuntimeUtil;

import plruntime.EventToFeaturedTransitionListMapEntry;
import plruntime.FeaturedMSDRuntimeState;
import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimePackage;
import plruntime.TransitionToFeatureExpressionStringMapEntry;
import plruntime.TransitionToFeatureExpressionMapEntry;
import plugin.Activator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Featured MSD Runtime State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateImpl#getTransitionToFeatureExpressionMap <em>Transition To Feature Expression Map</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateImpl#getEventToFeaturedTransitionListMap <em>Event To Featured Transition List Map</em>}</li>
 *   <li>{@link plruntime.impl.FeaturedMSDRuntimeStateImpl#getTransitionToFeatureExpressionStringMap <em>Transition To Feature Expression String Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FeaturedMSDRuntimeStateImpl extends MSDRuntimeStateImpl implements FeaturedMSDRuntimeState {

	//	/**
	//	 * <!-- begin-user-doc -->
	//	 * <!-- end-user-doc -->
	//	 * @generated NOT
	//	 */
	//	private static Logger logger = Activator.getLogManager().getLogger(
	//			MSDRuntimeStateGraphImpl.class.getName());

	/**
	 * The cached value of the '{@link #getTransitionToFeatureExpressionMap() <em>Transition To Feature Expression Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionToFeatureExpressionMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Transition, Integer> transitionToFeatureExpressionMap;

	/**
	 * The cached value of the '{@link #getEventToFeaturedTransitionListMap() <em>Event To Featured Transition List Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventToFeaturedTransitionListMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Event, EMap<Transition, Integer>> eventToFeaturedTransitionListMap;

	/**
	 * The cached value of the '{@link #getTransitionToFeatureExpressionStringMap() <em>Transition To Feature Expression String Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionToFeatureExpressionStringMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Transition, String> transitionToFeatureExpressionStringMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeaturedMSDRuntimeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlruntimePackage.Literals.FEATURED_MSD_RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Transition, Integer> getTransitionToFeatureExpressionMap() {
		if (transitionToFeatureExpressionMap == null) {
			transitionToFeatureExpressionMap = new EcoreEMap<Transition,Integer>(PlruntimePackage.Literals.TRANSITION_TO_FEATURE_EXPRESSION_MAP_ENTRY, TransitionToFeatureExpressionMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP);
		}
		return transitionToFeatureExpressionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<Event, EMap<Transition, Integer>> getEventToFeaturedTransitionListMap() {
		if (eventToFeaturedTransitionListMap == null) {
			eventToFeaturedTransitionListMap = new EcoreEMap<Event,EMap<Transition, Integer>>(PlruntimePackage.Literals.EVENT_TO_FEATURED_TRANSITION_LIST_MAP_ENTRY, EventToFeaturedTransitionListMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP);
		}
		return eventToFeaturedTransitionListMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Transition, String> getTransitionToFeatureExpressionStringMap() {
		if (transitionToFeatureExpressionStringMap == null) {
			transitionToFeatureExpressionStringMap = new EcoreEMap<Transition,String>(PlruntimePackage.Literals.TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP_ENTRY, TransitionToFeatureExpressionStringMapEntryImpl.class, this, PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP);
		}
		return transitionToFeatureExpressionStringMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ActiveMSD> computeActiveMSDs(MessageEvent messageEvent) {
		//		if (logger.isDebugEnabled()) {
		//			logger.debug("Creating active MSDs for message event: "
		//					+ messageEvent);
		//			Assert.isTrue(getObjectSystem().containsEObject(messageEvent.getSendingObject()));
		//			Assert.isTrue(getObjectSystem().containsEObject(messageEvent.getReceivingObject()));
		//		}

		EList<ActiveMSD> createdActiveMSDs = new BasicEList<ActiveMSD>();
		//		System.out.println("Computing active MSDs for message event: " + messageEvent);

		for (Interaction interaction : getRuntimeUtil().getInteractions()) {

			//			Message firstMessage = RuntimeUtil.Helper.getFirstMessageForMSD(interaction);
			//			System.out.println("Interacion: " + interaction.getName().toString());
			//			System.out.println("Message activating interaction: " + getRuntimeUtil().getOperation(firstMessage).toString());
			//			System.out.println("Message event: " + messageEvent.getOperation().toString());

			// do not create new active assumption or requirement MSDs if
			// already a safety violation occurred in the requirements resp.
			// assumptions.
			boolean isAssumptionMSD = getRuntimeUtil()
					.getInteractionToMSDUtilMap().get(interaction)
					.isEnvironmentAssumption();
			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			Message firstMessage = RuntimeUtil.Helper
					.getFirstMessageForMSD(interaction);

			// if firstMessage and messageEvent has the same operation, sending
			// class and receiving class
			if (getRuntimeUtil().getOperation(firstMessage).equals(messageEvent.getOperation())
					&& ((MSDObjectSystem)getObjectSystem()).lifelineMayBindToEObject(RuntimeUtil.Helper.getSendingLifeline(firstMessage), messageEvent.getSendingObject())
					&& ((MSDObjectSystem)getObjectSystem()).lifelineMayBindToEObject(RuntimeUtil.Helper.getReceivingLifeline(firstMessage), messageEvent.getReceivingObject())
					&& RuntimeUtil.Helper
						.isDiagramMessageParameterUnifiableWithValue(
							messageEvent, getRuntimeUtil()
									.getParameterValue(firstMessage))
					){

				// create the cut and progress it
				ActiveMSD activeMSD = RuntimeFactory.eINSTANCE
						.createActiveMSD();
				activeMSD.setRuntimeUtil(getRuntimeUtil());
				activeMSD.setMsdUtil(getRuntimeUtil().getInteractionToMSDUtilMap().get(interaction));
				activeMSD.setObjectSystem((MSDObjectSystem) getObjectSystem());
				ActiveMSDCut cut = RuntimeFactory.eINSTANCE.createActiveMSDCut();
				cut.setRuntimeUtil(getRuntimeUtil());
				ActiveMSDLifelineBindings lifelineBindings = RuntimeFactory.eINSTANCE
						.createActiveMSDLifelineBindings();
				ActiveMSDVariableValuations variableValuations = RuntimeFactory.eINSTANCE
						.createActiveMSDVariableValuations();
				activeMSD.setCurrentState(cut);
				activeMSD.setLifelineBindings(lifelineBindings);
				activeMSD.setVariableValuations(variableValuations);

				Lifeline sendingLifeline = RuntimeUtil.Helper
						.getSendingLifeline(firstMessage);
				Lifeline receivingLifeline = RuntimeUtil.Helper
						.getReceivingLifeline(firstMessage);

				activeMSD.addLifelineBinding(sendingLifeline,
						messageEvent.getSendingObject());
				activeMSD.addLifelineBinding(receivingLifeline,
						messageEvent.getReceivingObject());

				// progress the active MSD beyond the first message.
				activeMSD
				.getCurrentState()
				.progressCutLocationOnLifeline(
						sendingLifeline,
						RuntimeUtil.Helper
						.getSendingMessageOccurrenceSpecification(firstMessage));
				activeMSD
				.getCurrentState()
				.progressCutLocationOnLifeline(
						receivingLifeline,
						RuntimeUtil.Helper
						.getReceivingMessageOccurrenceSpecification(firstMessage));

				// init OCL for active MSD:

				OCL ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(
						activeMSD);

				for (Map.Entry<Lifeline, EObject> lifelineToEObjectMapEntry : activeMSD
						.getLifelineBindings().getLifelineToEObjectMap()
						.entrySet()) {
					activeMSD.assignValue(ocl,
							RuntimeUtil.Helper
							.getLifelineName(lifelineToEObjectMapEntry
									.getKey()),
									lifelineToEObjectMapEntry.getValue());
				}

				for (Map.Entry<String, EObject> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToEObjectValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, String> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToStringValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Integer> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToIntegerValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Boolean> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToBooleanValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}

				//				if (logger.isDebugEnabled()) {
				//					logger.debug("Active MSD created: " + activeMSD);
				//				}
				createdActiveMSDs.add(activeMSD);

			}
		}

		return createdActiveMSDs;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void performStep(MessageEvent messageEvent, EList<Interaction> interactionList) {
		//		if (logger.isDebugEnabled()) {
		//			logger.debug("performStep called with messageEvent: "
		//					+ RuntimeUtil.Helper.getMessageEventString(messageEvent));
		//		}

		// in this cases it makes no sense to progress at all because the system
		// or the environment has lost for sure.
		if (isSafetyViolationOccurredInAssumptions()
				|| isSafetyViolationOccurredInRequirements()
				&& !hasActiveAssumptionProcesses()) { // this may be a bit too
			// optimistic
			getActiveProcesses().clear();
			return;
		}

		boolean messageEventHadSideEffect = handleSideEffectOnObjectSystem(messageEvent);

		performStepOnActiveProcesses(messageEvent, interactionList);


		//if (messageEventHadSideEffect) // then the object system may have changed
		getActiveMSDChangedDuringPerformStep().addAll(getActiveProcesses());

	}
	/**
	 * Performs the activeMSD-specific changes.
	 * 
	 * @param messageEvent
	 */
	protected void performStepOnActiveProcesses(MessageEvent messageEvent, EList<Interaction> interactionList) {
		// 1) Check if the event violates an active Process in the current state
		// using an iterator to avoid a ConcurrentModificationException if
		// handleColdViolations removes a cut from
		// getCoreScenarioLogic().getCurrentState().getCuts().

		//		logger.debug("1) Check if the event violates an active Process in the current cut: ");

		Iterator<ActiveProcess> activeProcessesIterator = getActiveProcesses()
				.iterator();
		while (activeProcessesIterator.hasNext()) {
			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			// do not bother if it's an assumption MSD, but a safety violation
			// already occurred in the assumptions
			// or if it's a requirement MSD, but a safety violation already
			// occurred in the requirements
			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
					|| !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			if (activeProcess.isSafetyViolating(messageEvent)) {
				if (isAssumptionMSD) {
					//					if (logger.isDebugEnabled()) {
					//						logger.debug("Safety violation occurred in assumption MSD "
					//								+ activeProcess);
					//					}
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					//					if (logger.isDebugEnabled()) {
					//						logger.debug("Safety violation occurred in requirements MSD "
					//								+ activeProcess);
					//					}
					setSafetyViolationOccurredInRequirements(true);
				}
				// we do not return (stop) already here, because we want to
				// check if the safety violation also occurred in other MSDs
				// (to see whether it is a violation in the assumptions or
				// requirements or both)
			}
			if (activeProcess.isColdViolating(messageEvent)) {
				handleColdViolations(messageEvent, activeProcess,
						activeProcessesIterator);
			}
		}

		// if a safety violation occurred in the assumptions or requirements,
		// remove all the assumption resp. requirement MSDs.
		activeProcessesIterator = getActiveProcesses().iterator();
		while (activeProcessesIterator.hasNext()) {
			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				disposeState(activeProcess, activeProcessesIterator);
		}

		Set<ActiveProcess> activeProcessesNeedingFurtherProcessing = new HashSet<ActiveProcess>();

		// 2) Progress cut according to the message event (if possible)

		//		logger.debug("2) Progress cut according to the message event (if possible)");

		activeProcessesIterator = getActiveProcesses().iterator();
		while (activeProcessesIterator.hasNext()) {

			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);
			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
					|| !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			if (activeProcess instanceof ActiveMSD) {
				if (progressActiveMSD((ActiveMSD) activeProcess, messageEvent,
						activeProcessesIterator)) {
					activeProcessesNeedingFurtherProcessing.add(activeProcess);
				}
			} else if (activeProcess instanceof ActiveMSS) {
				if (progressActiveMSS((ActiveMSS) activeProcess, messageEvent,
						activeProcessesIterator)) {
					activeProcessesNeedingFurtherProcessing.add(activeProcess);
				}
			}
		}

		// 3) Create a new active MSD for all MSDs where this event is
		// represented by the first message

		//		logger.debug("3a) Create a new active MSD for all MSDs where this event is represented by the first message.");

		activeProcessesNeedingFurtherProcessing
		.addAll(createActiveMSDs(messageEvent, interactionList));

		//		logger.debug("3b) Create a new active MSS for all MSSs where this event is represented by the first transition.");

		activeProcessesNeedingFurtherProcessing
		.addAll(createActiveMSSs(messageEvent));

		EList<ActiveProcess> activeProcessesWhereRevelantEventsNeedRecalculation = new BasicEList<ActiveProcess>(
				activeProcessesNeedingFurtherProcessing);

		// 4) Progress hidden events and evaluate lifeline bindings

		//		logger.debug("4) Progress hidden events and evaluate lifeline bindings");

		while (!activeProcessesNeedingFurtherProcessing.isEmpty()) {
			ActiveProcess activeProcess = activeProcessesNeedingFurtherProcessing
					.iterator().next();
			boolean nextActiveProcessNeedsFurtherProcessing = false;

			// progress a hidden event. If this changed the cut (the method then
			// returns true), the cut needs to be considered in another
			// iteration of this loop.
			ActiveMSDProgress activeMSDProgress = activeProcess
					.progressEnabledHiddenEvents();
			if (activeMSDProgress == ActiveMSDProgress.PROGRESS) {
				nextActiveProcessNeedsFurtherProcessing = true;
			} else if (activeMSDProgress == ActiveMSDProgress.COLD_VIOLATION) {
				handleColdViolations(null, activeProcess, null);
				activeProcessesWhereRevelantEventsNeedRecalculation
				.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.SAFETY_VIOLATION) {
				if (RuntimeUtil.Helper
						.isEnvironmentAssumptionProcess(activeProcess)) {
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					setSafetyViolationOccurredInRequirements(true);
				}
				activeProcessesWhereRevelantEventsNeedRecalculation
				.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.NO_PROGRESS) {
				// do nothing.
			}

			if (activeProcess instanceof ActiveMSD) {
				// returns a list, because binding lifelines may lead to a
				// multiplication of cuts if multiple candidate objects for a
				// lifeline binding exist.
				EList<ActiveMSD> activeMSDsWhereAdditionalLifelinesWereBound = evaluateNextLifelineBindingExpression((ActiveMSD) activeProcess);

				// if the returned list is empty...
				if (activeMSDsWhereAdditionalLifelinesWereBound == null
						|| activeMSDsWhereAdditionalLifelinesWereBound
						.isEmpty())
					nextActiveProcessNeedsFurtherProcessing = false;
				else {
					activeProcessesNeedingFurtherProcessing
					.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
					activeProcessesWhereRevelantEventsNeedRecalculation
					.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
					nextActiveProcessNeedsFurtherProcessing = true;
				}
			} else if (activeProcess instanceof ActiveMSS) {
				EList<ActiveMSS> activeMSSsWhereAdditionalRolesWereBound = evaluateNextRoleBindingExpression((ActiveMSS) activeProcess);

				// if the returned list is empty...
				if (activeMSSsWhereAdditionalRolesWereBound == null
						|| activeMSSsWhereAdditionalRolesWereBound.isEmpty())
					nextActiveProcessNeedsFurtherProcessing = false;
				else {
					activeProcessesNeedingFurtherProcessing
					.addAll(activeMSSsWhereAdditionalRolesWereBound);
					activeProcessesWhereRevelantEventsNeedRecalculation
					.addAll(activeMSSsWhereAdditionalRolesWereBound);
					nextActiveProcessNeedsFurtherProcessing = true;
				}
			}

			if (!nextActiveProcessNeedsFurtherProcessing)
				activeProcessesNeedingFurtherProcessing.remove(activeProcess);
		}

		// 5) for each cut update relevant events
		// ---> moved to updateMSDModalMessageEvents
		//		logger.debug("5) for each cut update relevant events");
		//		for (ActiveMSD activeMSD: activeMSDsWhereRevelantEventsNeedRecalculation){
		//			boolean isAssumptionMSD = activeMSD.getMsdUtil().isEnvironmentAssumption();
		//			
		//			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
		//					|| !isAssumptionMSD && isSafetyViolationOccurredInRequirements())
		//				continue;
		//
		//			activeMSD.calculateRelevantEvents();
		//		}

		getActiveMSDChangedDuringPerformStep().clear();
		getActiveMSDChangedDuringPerformStep().addAll(activeProcessesWhereRevelantEventsNeedRecalculation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */

	protected EList<ActiveMSD> createActiveMSDs(MessageEvent messageEvent, EList<Interaction> interactionList) {
		//		if (logger.isDebugEnabled()) {
		//			logger.debug("Creating active MSDs for message event: "
		//					+ messageEvent);
		//			Assert.isTrue(getObjectSystem().containsEObject(messageEvent.getSendingObject()));
		//			Assert.isTrue(getObjectSystem().containsEObject(messageEvent.getReceivingObject()));
		//		}

		EList<ActiveMSD> createdActiveMSDs = new BasicEList<ActiveMSD>();
		for (Interaction interaction : interactionList) {

			//			for (Interaction interaction : getRuntimeUtil().getInteractions()) {

			/*
			 * TODO ask confirmation: concerning instructions checking violations (first block)
			 * is it the same to iterate only on a smaller set of interactions?
			 * */
			// do not create new active assumption or requirement MSDs if
			// already a safety violation occurred in the requirements resp.
			// assumptions.
			boolean isAssumptionMSD = getRuntimeUtil()
					.getInteractionToMSDUtilMap().get(interaction)
					.isEnvironmentAssumption();
			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			Message firstMessage = RuntimeUtil.Helper
					.getFirstMessageForMSD(interaction);
			// if firstMessage and messageEvent has the same operation, sending
			// class and receiving class
			if (getRuntimeUtil().getOperation(firstMessage).equals(messageEvent.getOperation())
					&& ((MSDObjectSystem)getObjectSystem()).lifelineMayBindToEObject(RuntimeUtil.Helper.getSendingLifeline(firstMessage), messageEvent.getSendingObject())
					&& ((MSDObjectSystem)getObjectSystem()).lifelineMayBindToEObject(RuntimeUtil.Helper.getReceivingLifeline(firstMessage), messageEvent.getReceivingObject())
					//&& RuntimeUtil.Helper.isParameterEqual(messageEvent, getRuntimeUtil().getParameterValue(firstMessage))
					&& RuntimeUtil.Helper
						.isDiagramMessageParameterUnifiableWithValue(
							messageEvent, getRuntimeUtil()
									.getParameterValue(firstMessage))
					){
				// create the cut and progress it
				ActiveMSD activeMSD = RuntimeFactory.eINSTANCE
						.createActiveMSD();
				activeMSD.setRuntimeUtil(getRuntimeUtil());
				activeMSD.setMsdUtil(getRuntimeUtil().getInteractionToMSDUtilMap().get(interaction));
				activeMSD.setObjectSystem((MSDObjectSystem) getObjectSystem());
				ActiveMSDCut cut = RuntimeFactory.eINSTANCE.createActiveMSDCut();
				cut.setRuntimeUtil(getRuntimeUtil());
				ActiveMSDLifelineBindings lifelineBindings = RuntimeFactory.eINSTANCE
						.createActiveMSDLifelineBindings();
				ActiveMSDVariableValuations variableValuations = RuntimeFactory.eINSTANCE
						.createActiveMSDVariableValuations();
				activeMSD.setCurrentState(cut);
				activeMSD.setLifelineBindings(lifelineBindings);
				activeMSD.setVariableValuations(variableValuations);

				Lifeline sendingLifeline = RuntimeUtil.Helper
						.getSendingLifeline(firstMessage);
				Lifeline receivingLifeline = RuntimeUtil.Helper
						.getReceivingLifeline(firstMessage);

				activeMSD.addLifelineBinding(sendingLifeline,
						messageEvent.getSendingObject());
				activeMSD.addLifelineBinding(receivingLifeline,
						messageEvent.getReceivingObject());

				// progress the active MSD beyond the first message.
				activeMSD
				.getCurrentState()
				.progressCutLocationOnLifeline(
						sendingLifeline,
						RuntimeUtil.Helper
						.getSendingMessageOccurrenceSpecification(firstMessage));
				activeMSD
				.getCurrentState()
				.progressCutLocationOnLifeline(
						receivingLifeline,
						RuntimeUtil.Helper
						.getReceivingMessageOccurrenceSpecification(firstMessage));

				// init OCL for active MSD:

				OCL ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(
						activeMSD);

				for (Map.Entry<Lifeline, EObject> lifelineToEObjectMapEntry : activeMSD
						.getLifelineBindings().getLifelineToEObjectMap()
						.entrySet()) {
					activeMSD.assignValue(ocl,
							RuntimeUtil.Helper
							.getLifelineName(lifelineToEObjectMapEntry
									.getKey()),
									lifelineToEObjectMapEntry.getValue());
				}

				for (Map.Entry<String, EObject> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToEObjectValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, String> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToStringValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Integer> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToIntegerValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Boolean> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToBooleanValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}

				//					if (logger.isDebugEnabled()) {
				//						logger.debug("Active MSD created: " + activeMSD);
				//					}

				// add the cut to the current state
				getActiveProcesses().add(activeMSD);
				createdActiveMSDs.add(activeMSD);
			}
		}


		//		System.out.println("messageName: " + messageEvent.getMessageName());
		//		for(ActiveMSD amsd : createdActiveMSDs){
		//			System.out.println(amsd.getInteraction().getName());
		//		}
		return createdActiveMSDs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP:
				return ((InternalEList<?>)getTransitionToFeatureExpressionMap()).basicRemove(otherEnd, msgs);
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP:
				return ((InternalEList<?>)getEventToFeaturedTransitionListMap()).basicRemove(otherEnd, msgs);
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP:
				return ((InternalEList<?>)getTransitionToFeatureExpressionStringMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP:
				if (coreType) return getTransitionToFeatureExpressionMap();
				else return getTransitionToFeatureExpressionMap().map();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP:
				if (coreType) return getEventToFeaturedTransitionListMap();
				else return getEventToFeaturedTransitionListMap().map();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP:
				if (coreType) return getTransitionToFeatureExpressionStringMap();
				else return getTransitionToFeatureExpressionStringMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP:
				((EStructuralFeature.Setting)getTransitionToFeatureExpressionMap()).set(newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP:
				((EStructuralFeature.Setting)getEventToFeaturedTransitionListMap()).set(newValue);
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP:
				((EStructuralFeature.Setting)getTransitionToFeatureExpressionStringMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP:
				getTransitionToFeatureExpressionMap().clear();
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP:
				getEventToFeaturedTransitionListMap().clear();
				return;
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP:
				getTransitionToFeatureExpressionStringMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_MAP:
				return transitionToFeatureExpressionMap != null && !transitionToFeatureExpressionMap.isEmpty();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__EVENT_TO_FEATURED_TRANSITION_LIST_MAP:
				return eventToFeaturedTransitionListMap != null && !eventToFeaturedTransitionListMap.isEmpty();
			case PlruntimePackage.FEATURED_MSD_RUNTIME_STATE__TRANSITION_TO_FEATURE_EXPRESSION_STRING_MAP:
				return transitionToFeatureExpressionStringMap != null && !transitionToFeatureExpressionStringMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void printInfoAboutState() {
		//incomingTransitions
		System.out.println("incoming transitions");
		for(Transition t: getIncomingTransition()){
			System.out.println(t.getLabel());
		}
		//outgoingTransitions
		System.out.println("outgoing transitions");
		for(Transition t: getOutgoingTransition()){
			System.out.println(t.getLabel());
		}
		//activeMSDchanged
		System.out.println("active MSDs changed");
		for(ActiveProcess ap:  getActiveMSDChangedDuringPerformStep()){
			System.out.println("\tMSD Package: "+((ActiveMSD) ap).getInteraction().getName().toString());
		}
		//allActive
		System.out.println("\nall ActiveMSDs");
		for(ActiveProcess ap:  getActiveProcesses()){
			System.out.println("\tMSD Package: "+((ActiveMSD) ap).getInteraction().getName().toString());
		}
		
		//eventToFeaturedTransition
		System.out.println("eventToFeaturedTransitionListMap");
		BDD bdd=((FeaturedMSDRuntimeStateGraph) getStateGraph()).getBdd();
		for(Event e: getEventToFeaturedTransitionListMap().keySet()){
			EMap<Transition, Integer> transitionToFEMap = getEventToFeaturedTransitionListMap().get(e);
			Iterator it = transitionToFEMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				System.out.println("{");
				System.out.println("\tevent: "+((Transition) pairs.getKey()).getLabel() + " ;" );
				System.out.println("\ttransition: "+((Transition) pairs.getKey()) + " ;" );
				System.out.println("\tfeature exp: ");
				bdd.printSet(bdd.ref((Integer) pairs.getValue()));
				System.out.println("}");
			}
		}
		
	}
} //FeaturedMSDRuntimeStateImpl
