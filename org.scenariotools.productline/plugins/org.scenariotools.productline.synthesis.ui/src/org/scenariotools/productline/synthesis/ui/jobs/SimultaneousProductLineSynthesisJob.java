package org.scenariotools.productline.synthesis.ui.jobs;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.productline.synthesis.SimultaneousProductLineSynthesis;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.State;

import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimeFactory;


public class SimultaneousProductLineSynthesisJob extends Job {
	ScenarioRunConfiguration scenarioRunConfiguration;
	ResourceSet resourceSet;
	IFile scenarioRunConfigurationResourceFile;
	Set<RuntimeState> exploredStates = new HashSet<RuntimeState>();

	public SimultaneousProductLineSynthesisJob(String name, ScenarioRunConfiguration scenarioRunConfiguration, ResourceSet resourceSet, IFile scenarioRunConfigurationResourceFile) {
		super(name);
		this.scenarioRunConfiguration = scenarioRunConfiguration;
		this.resourceSet = resourceSet;
		this.scenarioRunConfigurationResourceFile = scenarioRunConfigurationResourceFile;

	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {



		Resource msdSpecificationStateSpaceResource;
		Resource controllerResource = null;

		URI msdSpecificationStateSpaceFileURI = URI
				.createPlatformResourceURI(
						scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true).trimFileExtension()
						.appendFileExtension("msdruntime");
		URI controllerFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
				.removeFileExtension().toString()
				+ "Controller", true).appendFileExtension("msdruntime");

		try {
			msdSpecificationStateSpaceResource = resourceSet.getResource(
					msdSpecificationStateSpaceFileURI, true);
			controllerResource = resourceSet.getResource(controllerFileURI,
					true);
			if (msdSpecificationStateSpaceResource != null) {
				MessageDialog
				.openError(
						new Shell(),
						"An error occurred while creating the interpreter configuration",
						"There already exists a MSD runtime file at the default location:\n"
								+ msdSpecificationStateSpaceFileURI
								+ "\n\n"
								+ "Remove it first to create a new one.");
			}
			if (controllerResource != null) {
				MessageDialog
				.openError(
						new Shell(),
						"An error occurred while creating the interpreter configuration",
						"There already exists a controller file at the default location:\n"
								+ controllerFileURI
								+ "\n\n"
								+ "Remove it first to create a new one.");
			}

			return Status.CANCEL_STATUS;
		} catch (Exception e) {
			// that's good. Continue.
		}


		final FeaturedMSDRuntimeStateGraph featuredMSDRuntimeStateGraph = PlruntimeFactory.eINSTANCE.createFeaturedMSDRuntimeStateGraph();

		org.eclipse.uml2.uml.Package umlPackage=scenarioRunConfiguration.getUml2EcoreMapping().getUmlPackage();
		//search BaseFeature (assuming there is just one)
		Component baseFeature=null;
		for(Element element:umlPackage.getOwnedElements())
			if(element instanceof org.eclipse.uml2.uml.Component){
				Component component=(Component)element;
				if(component.getAppliedStereotype("FeatureDiagram::BaseFeature")!=null){
					baseFeature=component;
					break;
				}
			}
		

		long currentTimeMillis = System.currentTimeMillis();

		SimultaneousProductLineSynthesis simultaneousProductLineSynthesis = new SimultaneousProductLineSynthesis(baseFeature, scenarioRunConfiguration, featuredMSDRuntimeStateGraph);

		boolean buechiStrategyExists = simultaneousProductLineSynthesis.synthesize();

		long currentTimeMillisDelta = System.currentTimeMillis() - currentTimeMillis;					

		// remove reference to RuntimeUtil:
		featuredMSDRuntimeStateGraph.setRuntimeUtil(null);
		for (State state : featuredMSDRuntimeStateGraph.getStates()) {
			MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
			msdRuntimeState.setRuntimeUtil(null);
		}
		for (ActiveProcess activeProcess : featuredMSDRuntimeStateGraph
				.getElementContainer().getActiveProcesses()) {
			activeProcess.setRuntimeUtil(null);
			if(activeProcess instanceof ActiveMSD)
				((ActiveMSD)activeProcess).setMsdUtil(null);
		}
		for (ActiveState activeMSDCut : featuredMSDRuntimeStateGraph
				.getElementContainer().getActiveStates()) {
			activeMSDCut.setRuntimeUtil(null);
		}
		for (MSDObjectSystem msdObjectSystem : featuredMSDRuntimeStateGraph
				.getElementContainer().getObjectSystems()) {
			msdObjectSystem.setRuntimeUtil(null);
		}
		
		
		
		msdSpecificationStateSpaceResource = resourceSet
				.createResource(msdSpecificationStateSpaceFileURI);
		msdSpecificationStateSpaceResource.getContents().add(
				featuredMSDRuntimeStateGraph);

		Map<String, Boolean> options = new HashMap<String, Boolean>();

		try {
			msdSpecificationStateSpaceResource.save(options);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		postExplorationFinished(simultaneousProductLineSynthesis, buechiStrategyExists, currentTimeMillisDelta);

		return Status.OK_STATUS;
	}	


	//public static void postExplorationFinished(final SimultaneousProductLineSynthesis simultaneousProductLineSynthesis, final boolean buechiStrategyExists, final long currentTimeMillisDelta){
	public static void postExplorationFinished(final SimultaneousProductLineSynthesis simultaneousProductLineSynthesis, final boolean buechiStrategyExists, final long currentTimeMillisDelta){
		Display.getDefault().asyncExec (new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"NOTF Synthesis was executed, " 
										+ simultaneousProductLineSynthesis.getNumberOfExploredStates()
										+ " states were explored, "
										+ simultaneousProductLineSynthesis.getNumberOfExploredTransitions()
										+ " transitions were created.\n"
										+ "Time taken: " + (currentTimeMillisDelta)
										+ " milliseconds.\n"
										+ "Number of goal states: "
										+ simultaneousProductLineSynthesis.getGoal().size()
										+ "\nNumber of winning states: "
										+ simultaneousProductLineSynthesis.getWinning().size()
										+ "\n\n Number of valid products: "
										+ simultaneousProductLineSynthesis.getNumberOfValidProducts()
										+ "\n\n Number of realizable products: "
										+ simultaneousProductLineSynthesis.getNumberOfRealizableProducts()
										+ "\n Combination: "
										+ simultaneousProductLineSynthesis.getProducts());

			}
		});
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}

