package org.scenariotools.productline.synthesis.ui.jobs;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.productline.synthesis.SimultaneousProductLineSynthesisPERFORMANCE;
import org.scenariotools.runtime.RuntimeState;

import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimeFactory;


public class SimultaneousProductLineSynthesisJobPERFORMANCE extends Job {
	ScenarioRunConfiguration scenarioRunConfiguration;
	
	public static final int N = 20;

	ResourceSet resourceSet;
	IFile scenarioRunConfigurationResourceFile;
	Set<RuntimeState> exploredStates = new HashSet<RuntimeState>();

	public SimultaneousProductLineSynthesisJobPERFORMANCE(String name, ScenarioRunConfiguration scenarioRunConfiguration, ResourceSet resourceSet, IFile scenarioRunConfigurationResourceFile) {
		super(name);
		this.scenarioRunConfiguration = scenarioRunConfiguration;
		this.resourceSet = resourceSet;
		this.scenarioRunConfigurationResourceFile = scenarioRunConfigurationResourceFile;

	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {



		Resource msdSpecificationStateSpaceResource;
		Resource controllerResource = null;

		URI msdSpecificationStateSpaceFileURI = URI
				.createPlatformResourceURI(
						scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true).trimFileExtension()
						.appendFileExtension("msdruntime");
		URI controllerFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
				.removeFileExtension().toString()
				+ "Controller", true).appendFileExtension("msdruntime");


		

		org.eclipse.uml2.uml.Package umlPackage=scenarioRunConfiguration.getUml2EcoreMapping().getUmlPackage();
		//search BaseFeature (assuming there is just one)
		Component baseFeature=null;
		for(Element element:umlPackage.getOwnedElements())
			if(element instanceof org.eclipse.uml2.uml.Component){
				Component component=(Component)element;
				if(component.getAppliedStereotype("FeatureDiagram::BaseFeature")!=null){
					baseFeature=component;
					break;
				}
			}
		
		
		boolean buechiStrategyExists = false;
		int totalNumberOfProducts = -1; 
		long[] totalSynthesisTimes = new long[N];
		int noOfRealizableProducts = -1;
		//int noOfUnrealizableProducts = -1;
		int[] totalNumerOfStatesExplored = new int[N];
		//int[] totalNumerOfNonWinningStatesExplored = new int[N];
		
		
		for(int i = 0; i < N; i++){
			
			long totalSynthesisTime = System.currentTimeMillis();
			
			final FeaturedMSDRuntimeStateGraph featuredMSDRuntimeStateGraph = PlruntimeFactory.eINSTANCE.createFeaturedMSDRuntimeStateGraph();
			SimultaneousProductLineSynthesisPERFORMANCE simultaneousProductLineSynthesis = new SimultaneousProductLineSynthesisPERFORMANCE(baseFeature, scenarioRunConfiguration, featuredMSDRuntimeStateGraph);

			buechiStrategyExists = simultaneousProductLineSynthesis.synthesize();

			totalSynthesisTimes[i] = System.currentTimeMillis() - totalSynthesisTime;

			if (noOfRealizableProducts < 0){
				noOfRealizableProducts = simultaneousProductLineSynthesis.getNumberOfRealizableProducts();
			}else{
				Assert.isTrue(noOfRealizableProducts == simultaneousProductLineSynthesis.getNumberOfRealizableProducts());
			}
			
			if (totalNumberOfProducts < 0){
				totalNumberOfProducts = simultaneousProductLineSynthesis.getNumberOfValidProducts();
			}else{
				Assert.isTrue(totalNumberOfProducts == simultaneousProductLineSynthesis.getNumberOfValidProducts());
			}
			totalNumerOfStatesExplored[i] = simultaneousProductLineSynthesis.getNumberOfExploredStates();
			
		}			

		postFinished(
				buechiStrategyExists,
				totalNumberOfProducts, 
				noOfRealizableProducts, 
				
				average(totalNumerOfStatesExplored),
				roundToTwoDecimals(stdDeviation(totalNumerOfStatesExplored)),
				lowest(totalNumerOfStatesExplored),
				highest(totalNumerOfStatesExplored),

				average(totalSynthesisTimes),
				minmaxdiff(totalSynthesisTimes));

		return Status.OK_STATUS;
	}	

	private double average(int[] a){
		int sum = 0;
		for(int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum/a.length;
	}
	
	private double roundToTwoDecimals(double d){
		return Math.round(d*100)/100d;
	}

    private double stdDeviation(int[] a)
    {
        return Math.sqrt(variance(a));
    }

    private double variance(int[] a)
    {
        double average = average(a);
        double temp = 0;
        for(double x : a)
            temp += (average-x)*(average-x);
        return temp/a.length;
    }
    
    private int highest(int[] a){
    	int h = a[0];
        for(int x : a)
        	if (h < x) h = x;
    	return h;
    }

    private int lowest(int[] a){
    	int l = a[0];
        for(int x : a)
        	if (l > x) l = x;
    	return l;
    }

	private long minmaxdiff(long[] a){
		long min = a[0];
		long max = a[0];
		
		for(int i = 1; i < a.length; i++){
			if (a[i] > max)
				max = a[i];
			else if (a[i] < min)
				min = a[i];
		}
		return max-min;
	} 

	
	private double average(long[] a){
		long sum = 0;
		for(int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum/a.length;
	} 

	private void postFinished(
			final boolean buechiStrategyExists,
			final int totalNumberOfProducts,
			final int noOfRealizableProducts,
			
			final double averageNumerOfStatesExplored,
			final double stdDeviationOfStatesExplored,
			final int lowestNumerOfStatesExplored,
			final int highestNumerOfStatesExplored,
			
			final double averageSynthesisTime,
			final long maxDiffSynthesisTime) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"Featured Synthesis was executed, " + "\n"
						+"Number of products: "
						+  totalNumberOfProducts + "\n"
						+ "Number of realizable products: "
						+  noOfRealizableProducts + "\n"
						+ "Average no. states explored: "
						+  averageNumerOfStatesExplored  + "\n"
						+ "            std.deviation : " 
						+ stdDeviationOfStatesExplored + " "
						+ " - lowest: " 
						+ lowestNumerOfStatesExplored + " "
						+ " - highest: " 
						+ highestNumerOfStatesExplored + "\n"
						
						+ "Average time required (ms): "
						+  averageSynthesisTime 
						+ " - max diff: " 
						+ maxDiffSynthesisTime + "\n"
						+ "\n"
						+ "Number of measurements performed: "
						+  N + "\n"
					);
			}
		});
	}
	
	public static void postExplorationFinished(final SimultaneousProductLineSynthesisPERFORMANCE simultaneousProductLineSynthesis, final boolean buechiStrategyExists, final long currentTimeMillisDelta){
		Display.getDefault().asyncExec (new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"NOTF Synthesis was executed, " 
										+ simultaneousProductLineSynthesis.getNumberOfExploredStates()
										+ " states were explored, "
										+ simultaneousProductLineSynthesis.getNumberOfExploredTransitions()
										+ " transitions were created.\n"
										+ "Time taken: " + (currentTimeMillisDelta)
										+ " milliseconds.\n"
										+ "Number of goal states: "
										+ simultaneousProductLineSynthesis.getGoal().size()
										+ "\nNumber of winning states: "
										+ simultaneousProductLineSynthesis.getWinning().size()
										+ "\n\n Number of valid products: "
										+ simultaneousProductLineSynthesis.getNumberOfRealizableProducts()
										//+ "\n Combination: "
										//+ simultaneousProductLineSynthesis.getProducts()
						);

			}
		});
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}

