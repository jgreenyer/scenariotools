   package org.scenariotools.productline.synthesis.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.productline.synthesis.ui.jobs.SimultaneousProductLineSynthesisJob;
import org.scenariotools.productline.synthesis.ui.jobs.SimultaneousProductLineSynthesisJobPERFORMANCE;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;


import org.eclipse.jface.action.Action;
import org.eclipse.ui.IActionDelegate;


public class SimultaneousProductLineSynthesisAction implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public SimultaneousProductLineSynthesisAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	protected void postProblem(final String problem) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"A problem occurred",
						problem);
			}
		});
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		if (!"scenariorunconfiguration".equals(((IFile)structuredSelection.getFirstElement()).getFileExtension())){
			postProblem("You must select a *.scenariorunconfiguration file.");
		}
		
		IFile scenarioRunConfigurationResourceFile = (IFile) structuredSelection.getFirstElement();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);

		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

	
		SimultaneousProductLineSynthesisJob job = new SimultaneousProductLineSynthesisJob("MSD state space exploration.",scenarioRunConfiguration, resourceSet, scenarioRunConfigurationResourceFile);
		//SimultaneousProductLineSynthesisJobPERFORMANCE job = new SimultaneousProductLineSynthesisJobPERFORMANCE("MSD state space exploration.",scenarioRunConfiguration, resourceSet, scenarioRunConfigurationResourceFile);

		job.setUser(true);
		job.schedule();
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
