package org.scenariotools.productline.synthesis;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import jdd.bdd.BDD;


import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

import plruntime.FeaturedMSDRuntimeState;
import plruntime.FeaturedMSDRuntimeStateGraph;
import plruntime.PlruntimeFactory;


public class SimultaneousProductLineSynthesisPERFORMANCE {

	private Component root;
	private RuntimeStateGraph newController;
	RuntimeState startState;
	private ScenarioRunConfiguration scenarioRunConfiguration;
	private FeaturedMSDRuntimeStateGraph featuredMSDRuntimeStateGraph;
	private FeaturedMSDRuntimeState featuredMSDRuntimeStartState;
	private int numberOfExploredStates;
	private int numberOfExploredTransitions;
	private Map<RuntimeState, Integer> goal;
	private Map<RuntimeState, Integer> goalToEvaluate;
	private Map<RuntimeState, Integer> winning;
	int countNOTFR;
	private BDD bdd;
	private Integer fd;
	private ArrayList<String> featureList;
	private String products;
	private int numberOfRealizableProducts;
	private int numberOfValidProducts;

	public SimultaneousProductLineSynthesisPERFORMANCE(Component root, ScenarioRunConfiguration scenarioRunConfiguration, FeaturedMSDRuntimeStateGraph featuredMSDRuntimeStateGraph) {
		this.root = root;
		this.scenarioRunConfiguration = scenarioRunConfiguration;
		this.featuredMSDRuntimeStateGraph = featuredMSDRuntimeStateGraph;

	}

	public boolean synthesize() {

		//INIT
		
		newController = PlruntimeFactory.eINSTANCE.createFeaturedMSDRuntimeStateGraph();
		boolean buechiStrategyExists = false;
		numberOfExploredStates = 0;
		numberOfExploredTransitions = 0;
		goal = new HashMap<RuntimeState, Integer>();
		goalToEvaluate = new HashMap<RuntimeState, Integer>();
		winning = new HashMap<RuntimeState, Integer>();
		featureList = new ArrayList<String>();
		countNOTFR = 0;
		products="none";
		numberOfRealizableProducts = 0;
		numberOfValidProducts = 0;

		//creates BDD
		featuredMSDRuntimeStartState = featuredMSDRuntimeStateGraph.init(scenarioRunConfiguration);

		//PACKAGETOFEATUREMAP: gets all features and all packages. maps them
		//all ft
		Set<Component> features = allFeatures(root);
		features.add(root);
		//all pkg
		List<Package> productPackages = new BasicEList<Package>();
		for(Component feature:features){
			Package featurePackage=getPackageForFeature(feature);
			if(featurePackage!=null){
				productPackages.add(featurePackage);
				featuredMSDRuntimeStateGraph.getPackageToFeatureMap().put(featurePackage, feature);
			}
		}
		//adds all packages to the scenarioRunConfiguration
		scenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered().addAll(productPackages);

		//retrieve bdd
		bdd=featuredMSDRuntimeStateGraph.getBdd();

		//SYNTHESIZE
		winning.putAll(FB(featuredMSDRuntimeStartState));
		buechiStrategyExists = getStrategy();
		
		//count explored stated and transitions in the FTS
		countNumberOfStatesAndTransitions(featuredMSDRuntimeStateGraph);

		return buechiStrategyExists;
	}

	private boolean getStrategy() {
		/**
		 * return true if the winning expression for the start state is not false.
		 */
		boolean buechiStrategyExists = false;
		if(winning.keySet().contains(featuredMSDRuntimeStartState)){
			//retrieve the winning expression for the start state (i.e. for which combination of features the start state is winning)
			int winInt = winning.get(featuredMSDRuntimeStartState);
			if(winInt!=0){
				buechiStrategyExists = true;
				System.out.println("\n\nREALIZABLE PRODUCTS:");
				bdd.printSet(winInt);
				//winInt to winString
				String winString = getString(winInt);
				//winString to winArray
				String[] winArray = winString.split("\\r?\\n");
				//retrieve the number of realizable products
				buildNumberOfRealizableProducts(winArray);
			}
			//System.out.println("Products: "+ products);
		}
		return buechiStrategyExists;
	}

	private void buildNumberOfValidProducts(Integer fdInt) {
		/**
		 * return the number of realizable products
		 */

		String fdString = getString(fdInt);
		//winString to winArray
		String[] fdArray = fdString.split("\\r?\\n");
		//retrieve the number of valid products

		System.out.println("\n\nVALID PRODUCTS:");
		bdd.printSet(fdInt);

		int dashes = 0;
		for(int j=0; j<fdArray.length; j++){
			dashes = 0;
			if(fdArray[j].charAt(0)=='-'){
				dashes++;
			}
			for(int i=1; i<fdArray[0].length(); i++){

				if(fdArray[j].charAt(i)=='-'){
					dashes++;
				}
			}
			dashes=(int) Math.pow(2, dashes);
			numberOfValidProducts=numberOfValidProducts+dashes;
		}
		System.out.println("numberOfValidProducts: "+ numberOfValidProducts);
	}


	private void buildNumberOfRealizableProducts(String[] winArray) {
		/**
		 * return the number of realizable products
		 */
		
		int dashes = 0;
		for(int j=0; j<winArray.length; j++){
			dashes = 0;
			if(winArray[j].charAt(0)=='-'){
				dashes++;
			}
			for(int i=1; i<winArray[0].length(); i++){

				if(winArray[j].charAt(i)=='-'){
					dashes++;
				}
			}
			dashes=(int) Math.pow(2, dashes);
			numberOfRealizableProducts=numberOfRealizableProducts+dashes;
		}
		System.out.println("numberOfProducts: "+ numberOfRealizableProducts);
	}

	private String getString(int bddInt) {
		/*
		 * translate from bdd to string using stdout
		 */
		//feature expression to string
		// Create a stream to hold the output
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		// IMPORTANT: Save the old System.out!
		PrintStream old = System.out;
		// Tell Java to use your special stream
		System.setOut(ps);
		// Print some output: goes to your special stream
		bdd.printSet(bdd.ref((Integer) bddInt));
		// Put things back
		System.out.flush();
		System.setOut(old);
		// Show what happened
		String winString = baos.toString();
		return winString;
	}

	private Set<Component> allFeatures(Component feature){
		/*
		 * retrieve valid products from the FD in the form of a bdd
		 */
		List<Component> todo=new LinkedList<Component>();
		Set<Component> result = new HashSet<Component>();
		todo.add(feature);
		//(1) root is mandatory. fd=true&&root
		fd = 1;
		int father = featuredMSDRuntimeStateGraph.createVar(feature);
		fd = featuredMSDRuntimeStateGraph.getBdd().and(fd, father);
		featuredMSDRuntimeStateGraph.getBdd().ref(fd);
		featureList.add(feature.getLabel());
		while(todo.size()>0){
			Component currentFeature = todo.remove(0);
			if(currentFeature!=null){
				for(Component requiredFeature:getChildren(currentFeature)){
					if(!result.contains(requiredFeature)){
						todo.add(requiredFeature);
						result.add(requiredFeature);
					}
				}
			}
		}
		//PRINT fd
		//System.out.println("VALID PRODUCTS: fd");
		//featuredMSDRuntimeStateGraph.getBdd().printSet(fd);

		featuredMSDRuntimeStateGraph.setFd(fd);
		return result;
	}

	private Set<Component> getChildren(Component feature){

		Set<Component> children = new HashSet<Component>();
		int childDecomposition = 0;
		Port relationshipGroup=getChildPort(feature);
		if(relationshipGroup!=null){
			//for and decomposition zero is 1 (x&&0=0; x&&1=x)
			if(getRelationshipGroupKind(feature).equals("AND")){
				childDecomposition  = 1;
			}
			for(Dependency dependency:relationshipGroup.getClientDependencies()){
				Component childFeature=(Component)dependency.getSuppliers().get(0);
				featureList.add(childFeature.getLabel());
				//create bdd var for each feature
				int child = featuredMSDRuntimeStateGraph.createVar(childFeature);
				//add child feature to returning set
				children.add(childFeature);
				//(2) father is mandatory for children. fd=fd&&( child --> father)
				int cond1 = featuredMSDRuntimeStateGraph.getBdd().imp(child, featuredMSDRuntimeStateGraph.getFeatureToBddVariableMap().get(feature));
				featuredMSDRuntimeStateGraph.getBdd().ref(cond1);
				fd  = featuredMSDRuntimeStateGraph.getBdd().and(fd, cond1);
				//build childDecomposition
				if(getRelationshipGroupKind(feature).equals("AND")){
					//AND childDecomposition=x1&&x2&&...&&xn for xi mandatory
					if(getFeatureRelationshipKind(dependency).equals("Mandatory")){
						childDecomposition = featuredMSDRuntimeStateGraph.getBdd().and(childDecomposition, child);
					}
				}
				if(getRelationshipGroupKind(feature).equals("OR")){
					//OR childDecomposition=x1||x2||...||xn
					childDecomposition = featuredMSDRuntimeStateGraph.getBdd().or(childDecomposition, child);
				}
				if(getRelationshipGroupKind(feature).equals("XOR")){
					//XOR childDecomposition=x1 x2 ... xn
					childDecomposition = featuredMSDRuntimeStateGraph.getBdd().xor(childDecomposition, child);
				}
			}

			//(3) constraint condition. fd=fd&&(father --> childDecomposition)
			int cond2 = featuredMSDRuntimeStateGraph.getBdd().imp(featuredMSDRuntimeStateGraph.getFeatureToBddVariableMap().get(feature), childDecomposition);
			featuredMSDRuntimeStateGraph.getBdd().ref(cond2);
			fd  = featuredMSDRuntimeStateGraph.getBdd().and(fd, cond2);
		}
		return children;
	}

	private String getRelationshipGroupKind(Component feature){
		Port relationshipGroup=getChildPort(feature);
		Stereotype stereotype=relationshipGroup.getAppliedStereotype("FeatureDiagram::RelationshipGroup");
		EnumerationLiteral groupKind=(EnumerationLiteral)relationshipGroup.getValue(stereotype, "relationshipGroupKind");
		return groupKind.getName();
	}

	private String getFeatureRelationshipKind(Dependency dependency){
		Stereotype stereotype=dependency.getAppliedStereotype("FeatureDiagram::ChildFeatureRelationship");
		EnumerationLiteral relationshipKind=(EnumerationLiteral)dependency.getValue(stereotype, "featureRelationshipKind");
		return relationshipKind.getName();
	}

	private Port getChildPort(Component feature){
		EList<Port> ownedPorts=feature.getOwnedPorts();
		if(ownedPorts!=null && ownedPorts.size()>0)
			return ownedPorts.get(0);
		return null;
	}

	private Package getPackageForFeature(Component feature){

		for(Dependency dependency:feature.getClientDependencies()){
			for(NamedElement dependencyTarget:dependency.getClients())
				if(dependencyTarget instanceof Component){
				}
			for(NamedElement dependencyTarget:dependency.getSuppliers())
				if(dependencyTarget instanceof Package)
					return (Package)dependencyTarget;
		}
		return null;
	}


	private boolean isGoal(RuntimeState q) {
		if (goal.keySet().contains(q)){
			//System.out.println("goal state "+q);						
			return true;
		}
		else {
			// 1. no safety violation must have occurred in the requirements and there must not be any active events in active req. MSDs
			// OR 2. there was a safety violation of the assumptions
			// OR 3. if a safety violation occurred in the requirements, there are active messages left in the assumptions,
			if (!q.isSafetyViolationOccurredInRequirements() && !hasMandatoryMessageEvents(q, false)
					|| q.isSafetyViolationOccurredInAssumptions()
					|| q.isSafetyViolationOccurredInRequirements() && hasMandatoryMessageEvents(q, true)
					) {
				//initialize the feature expression corresponding to each runtimeStare in the goal map as true;
				goal.put(q, 1);
				//System.out.println("goal state "+q);						
				return true;
			}
		}
		//System.out.println("not a goal state "+q);	
		if(goal.containsKey(q)){
			goal.remove(q);
		}
		return false;
	}

	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState,
			boolean forAssumptions) {
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();
			if (forAssumptions) {
				if (modalMessageEvent.getAssumptionsModality().isMandatory()){
					//System.out.println("Active message "+ messageEventToModalMessageEventMapEntry.getKey().getMessageName());
					return true;
				}
			} else {
				if (modalMessageEvent.getRequirementsModality().isMandatory())
					return true;
			}
		}
		return false;
	}

	public Map<RuntimeState, Integer> FB(MSDRuntimeState msdRuntimeState){

		/*
		 * Featured Buechi
		 */
		Map<RuntimeState, Integer> goalToEvaluateTemp = new HashMap<RuntimeState, Integer>();
		Map<RuntimeState, Integer> win = new HashMap<RuntimeState, Integer>();

		//lines 1,2 algorithm
		win.putAll(firstFOTFR(msdRuntimeState));	

		goalToEvaluate.clear();
		goalToEvaluate.putAll(goal);
		goalToEvaluateTemp.putAll(goalToEvaluate);

		//line 3
		while(existsAnUpdatedGoalState(win, goalToEvaluateTemp)){

			goalToEvaluateTemp.clear();
			goalToEvaluateTemp.putAll(goalToEvaluate);

			updateGoalStates(win, goalToEvaluateTemp);

			win.clear();
			win.putAll(FOTFR(msdRuntimeState));

		}

		setStateStatusFlags((RuntimeStateGraph) msdRuntimeState.eContainer());
		System.out.println("\n\nReachability algorithm (FOTFR) called " + countNOTFR + " time(s)");

		buildNumberOfValidProducts(featuredMSDRuntimeStateGraph.getFd());
		
		return win;
	}

	private boolean existsAnUpdatedGoalState(Map<RuntimeState, Integer> win, Map<RuntimeState, Integer> goalToEvaluateTemp) {

		Integer cond = 0;
		for(Map.Entry<RuntimeState, Integer> g: goalToEvaluateTemp.entrySet()){
			Integer winFe = 0;
			if(win.keySet().contains(g.getKey())){
				winFe = win.get(g.getKey());
			}

			/*
			 * CHECK if winFe is a subset of goalFe 
			 * i.e. if at least a valid product of goalFe is not in winFe
			 * i.e. if (fd&&goalFe&&!winFe != 0)
			 */
			cond = bdd.and(bdd.not(winFe), bdd.and(fd, g.getValue()));
			bdd.ref(cond);
			if(cond!=0){
				return true;
			}	
		}
		return false;
	}

	private void updateGoalStates(Map<RuntimeState, Integer> win, Map<RuntimeState, Integer> goalToEvaluateTemp){
		for(Map.Entry<RuntimeState, Integer> g: goalToEvaluateTemp.entrySet()){
			Integer winFe = 0;
			if(win.keySet().contains(g.getKey())){
				winFe = win.get(g.getKey());
			}
			goalToEvaluate.remove(g.getKey());
			goalToEvaluate.put(g.getKey(), bdd.and(fd, winFe));
		}
	}

	public Map<RuntimeState, Integer> firstFOTFR(RuntimeState runtimeState){
		/*
		 * the first time we call FOTFR in FB we also retrieve the set of goal states in the meantime
		 * (cfr line 498).
		 */

		countNOTFR++;
		//		System.out.println("------------------------------FOTFR-------------------------- #"+ countNOTFR);
		this.startState = runtimeState;
		Set<RuntimeState> visited;
		Map<Transition, Integer>  waiting;
		Map<RuntimeState, Map<Transition, Integer>> depend;
		Map<RuntimeState, Integer> winTot;
		visited = new HashSet<RuntimeState>();
		waiting = new LinkedHashMap<Transition, Integer>();
		depend  = new HashMap<RuntimeState, Map<Transition, Integer>>();
		winTot = new HashMap<RuntimeState, Integer>();

		initStateAnnotations(runtimeState);

		RuntimeState targetState;
		RuntimeState sourceState;
		Transition transition = null;
		Integer featureExpression = null;
		Integer winStar;
		boolean targetStateIsGoal;

		//Initialization
		visited.add(runtimeState);
		addWaitingFirstFOTFR(waiting, runtimeState);
		winTot.put(runtimeState, 0);
		targetStateIsGoal=false;

		//Main
		while(!waiting.isEmpty()) {

			//retrieve and remove the last
			List<Entry<Transition, Integer>> waitingEntryList =
					new ArrayList<Map.Entry<Transition, Integer>>(waiting.entrySet());
			Entry<Transition, Integer> lastEntry =
					waitingEntryList.get(waitingEntryList.size()-1);
			transition = lastEntry.getKey();
			featureExpression = lastEntry.getValue();
			waiting.remove(lastEntry.getKey());

			targetState = (RuntimeState) transition.getTargetState();
			sourceState = (RuntimeState) transition.getSourceState();

			if (isGoal(targetState)){
				targetStateIsGoal=true;
			}

			if (!visited.contains(targetState)){
				//forward
				//System.out.println("forward");
				initStateAnnotations(targetState);

				visited.add(targetState);
				addDepend(depend, targetState, transition, featureExpression);
				addWaitingFirstFOTFR(waiting, targetState);

				if (targetStateIsGoal){
					waiting.put(lastEntry.getKey(), lastEntry.getValue());			
				}
			}
			else{
				//backward
				//System.out.println("backward");
				winStar = canGuaranteeToReachWinningOrGoalSuccessor(sourceState, winTot, goal);
				Integer winFe = 0;
				if(winTot.keySet().contains(sourceState)){
					winFe = winTot.get(sourceState);
				}
				/*
				 * CHECK if winFe is a subset of win* 
				 * i.e. if at least a valid product of win* is not in winFe
				 * i.e. if (fd&&win*&&!winFe != 0)
				 */
				int cond = bdd.and(bdd.not(winFe), bdd.and(fd, winStar));
				bdd.ref(cond);
			
				if(cond!=0){
					if (depend.get(sourceState) != null){
						Map<Transition, Integer> dependentFromSourceState = depend.get(sourceState);
						for (Map.Entry<Transition, Integer> ft : dependentFromSourceState.entrySet()) {
							if(!waiting.containsKey(ft.getKey())){
								waiting.put(ft.getKey(), ft.getValue());
							}
						}
					}
					//substitute win[s] with win*
					winTot.remove(sourceState);
					winTot.put(sourceState,bdd.and(fd, winStar));
					//System.out.println("\nADDING STATE "+sourceState+"TO WIN SET ");
				}
			}
		}

		//		System.out.println("--------------------------------------------------------------------------");

		return winTot;
	}

	public Map<RuntimeState, Integer> FOTFR(RuntimeState runtimeState){

		countNOTFR++;
		//		System.out.println("------------------------------FOTFR-------------------------- #"+ countNOTFR);

		Set<RuntimeState> visited;
		Map<Transition, Integer>  waiting;
		Map<RuntimeState, Map<Transition, Integer>> depend;
		Map<RuntimeState, Integer> winTot;

		visited = new HashSet<RuntimeState>();
		waiting = new LinkedHashMap<Transition, Integer>();
		depend  = new HashMap<RuntimeState, Map<Transition, Integer>>();
		winTot = new HashMap<RuntimeState, Integer>();

		RuntimeState targetState;
		RuntimeState sourceState;
		Transition transition;
		Integer featureExpression = null;
		Integer winStar;
		boolean targetStateIsGoal;

		//Initialization
		visited.add(runtimeState);
		addWaitingFOTFR(waiting, runtimeState);
		targetStateIsGoal=false;

		//Main
		while(!waiting.isEmpty()) {

			//retrieve and remove the last entry
			List<Entry<Transition, Integer>> entryList =
					new ArrayList<Map.Entry<Transition, Integer>>(waiting.entrySet());
			Entry<Transition, Integer> lastEntry =
					entryList.get(entryList.size()-1);
			transition = lastEntry.getKey();
			featureExpression = lastEntry.getValue();
			waiting.remove(lastEntry.getKey());

			targetState = (RuntimeState) transition.getTargetState();
			sourceState = (RuntimeState) transition.getSourceState();


			if (goalToEvaluate.keySet().contains(targetState)){
				if(goalToEvaluate.get(targetState)!=0){
					targetStateIsGoal=true;
				}
			}

			if (!visited.contains(targetState)){

				//Forward

				visited.add(targetState);
				addDepend(depend, targetState, transition, featureExpression);
				addWaitingFOTFR(waiting, targetState);

				if (targetStateIsGoal){
					waiting.put(lastEntry.getKey(), lastEntry.getValue());			
				}

			}
			else{

				//Backward

				winStar = canGuaranteeToReachWinningOrGoalSuccessor(sourceState, winTot, goalToEvaluate);

				Integer winFe = 0;
				if(winTot.keySet().contains(sourceState)){
					winFe = winTot.get(sourceState);
				}

				/*
				 * CHECK if winFe is a subset of win* 
				 * i.e. if at least a valid product of win* is not in winFe
				 * i.e. if (fd&&win*&&!winFe != 0)
				 */
				int cond = bdd.and(bdd.not(winFe), bdd.and(fd, winStar));
				bdd.ref(cond);

				if(cond!=0){
					if (depend.get(sourceState) != null){
						Map<Transition, Integer> dependentFromSourceState = depend.get(sourceState);
						for (Map.Entry<Transition, Integer> ft : dependentFromSourceState.entrySet()) {

							if(!waiting.containsKey(ft.getKey())){
								waiting.put(ft.getKey(), ft.getValue());
							}
						}
					}
					//substitute win[s] with win*
					winTot.remove(sourceState);
					winTot.put(sourceState,bdd.and(fd, winStar));

				}
			}
		}

		return winTot;
	}

	private void addWaitingFirstFOTFR(Map<Transition, Integer> waiting, RuntimeState q) {

		//System.out.println("\nGENERATE SUCCESSORS*******************************************");
		EMap<Transition, Integer> allOutgoingFeaturedTransitions = featuredMSDRuntimeStateGraph.generateAllFeaturedSuccessors(q);
		//System.out.println("\nSUCCESSORS GENERATED\n");

		for (Entry<Transition, Integer> t : allOutgoingFeaturedTransitions) {
			//ADD TO WAITING
			waiting.put(t.getKey(),t.getValue());
		}
	}

	private void addWaitingFOTFR(Map<Transition, Integer> waiting, RuntimeState q) {

		/*
		 * add q to the waiting set (a set of featured transitions is represented by a map)
		 */
		EMap<Transition, Integer> allOutgoingFeaturedTransitions = new BasicEMap<Transition, Integer>();
		allOutgoingFeaturedTransitions.putAll(((FeaturedMSDRuntimeState) q).getTransitionToFeatureExpressionMap());

		for (Entry<Transition, Integer> t : allOutgoingFeaturedTransitions) {
			waiting.put(t.getKey(),t.getValue());
		}
	}

	private void addDepend(Map<RuntimeState, Map<Transition, Integer>> depend, RuntimeState q, Transition t, Integer fe) {
		
		/*
		 * state q mapped to a featured transition <t, fe> (represented by a map)
		 */
		Map<Transition, Integer> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new HashMap<Transition, Integer>();
			depend.put(q, dependingTransitions); 
		}
		dependingTransitions.put(t, fe);
	}


	private boolean canReachSomething(RuntimeState runtimeState){
		boolean stateReachesSomething = false;
		if(!((FeaturedMSDRuntimeState) runtimeState).getEventToFeaturedTransitionListMap().isEmpty()){
			stateReachesSomething = true;
		}
		return stateReachesSomething;
	}

	private Integer canGuaranteeToReachWinningOrGoalSuccessor(RuntimeState runtimeState, Map<RuntimeState, Integer> win, Map<RuntimeState, Integer> goal){

		Integer winStar=0;

		//first check whether the state has outGoing transitions (of course it does, it is a sourceState!).
		if(canReachSomething(runtimeState)){
			//We are supposing to always have transitions of the same kind (either all controllable or all uncontrollable)
			if (allTransitionsAreControllable(runtimeState)){
				//winStar = win.keySet().contains(runtimeState) || atLeastOneControllableTransitionLeadsToAWinningOrAGoalSuccessor(runtimeState, win, goal);
				if(win.keySet().contains(runtimeState)){
					winStar = bdd.or(win.get(runtimeState), atLeastOneControllableTransitionLeadsToAWinningOrAGoalSuccessor(runtimeState, win, goal));
				}
				else{
					winStar = atLeastOneControllableTransitionLeadsToAWinningOrAGoalSuccessor(runtimeState, win, goal);
				}

			}
			else {
				//winStar = win.keySet().contains(runtimeState) || everyUncontrollableTransitionLeadsToAWinningSuccessor(runtimeState, win, goal);
				if(win.keySet().contains(runtimeState)){
					winStar = everyUncontrollableTransitionLeadsToAWinningSuccessor(runtimeState, win, goal);
					
					winStar = bdd.or(win.get(runtimeState), everyUncontrollableTransitionLeadsToAWinningSuccessor(runtimeState, win, goal));
					
				}
				else{
					winStar = everyUncontrollableTransitionLeadsToAWinningSuccessor(runtimeState, win, goal);
				
				}

			}
		}
		return winStar;
	}


	private boolean allTransitionsAreControllable(RuntimeState runtimeState){

		boolean areControllable = false;

		if (isControllable(((FeaturedMSDRuntimeState) runtimeState).getOutgoingTransition().get(0)))
			areControllable = true;

		return areControllable;
	}

	
	private Integer atLeastOneControllableTransitionLeadsToAWinningOrAGoalSuccessor(RuntimeState runtimeState, Map<RuntimeState, Integer> win, Map<RuntimeState, Integer> goal){

		Integer returnExp=1;
		Integer orGamma=0;
		Integer orGammaAndWinOrGoal=0;

		EMap<Transition, Integer> allOutgoingFeaturedTransitions = new BasicEMap<Transition, Integer>();
		allOutgoingFeaturedTransitions.putAll(((FeaturedMSDRuntimeState) runtimeState).getTransitionToFeatureExpressionMap());


		for (Entry<Transition, Integer> t : allOutgoingFeaturedTransitions) {
			Integer goalExp;
			Integer winExp;
			Integer gamma;
			Integer partialExp;
			if (win.keySet().contains(t.getKey().getTargetState())){
				winExp=win.get(t.getKey().getTargetState());
			}else{
				winExp=0;
			}
			if (goal.keySet().contains(t.getKey().getTargetState())){
				goalExp=goal.get(t.getKey().getTargetState());
			}else{
				goalExp=0;
			}
			gamma=t.getValue();

			partialExp = bdd.and(gamma, bdd.or(winExp, goalExp));
			orGammaAndWinOrGoal = bdd.or(partialExp, orGammaAndWinOrGoal);
			bdd.ref(orGammaAndWinOrGoal);
			orGamma = bdd.or(gamma, orGamma);
			bdd.ref(orGamma);
		}
		returnExp = orGammaAndWinOrGoal;
		bdd.ref(returnExp);
		//bdd.printSet(returnExp);
		return returnExp;
	}

	
	private Integer everyUncontrollableTransitionLeadsToAWinningSuccessor(RuntimeState runtimeState,  Map<RuntimeState, Integer> win, Map<RuntimeState, Integer> goalLocal){

		Integer returnExp=1;
		Integer orGamma=0;

		EMap<Transition, Integer> allOutgoingFeaturedTransitions = new BasicEMap<Transition, Integer>();
		allOutgoingFeaturedTransitions.putAll(((FeaturedMSDRuntimeState) runtimeState).getTransitionToFeatureExpressionMap());

		for (Entry<Transition, Integer> t : allOutgoingFeaturedTransitions) {
			Integer goalExp;
			Integer winExp;
			Integer gamma;
			Integer partialExp;
			if (win.keySet().contains(t.getKey().getTargetState())){
				winExp=win.get(t.getKey().getTargetState());
			}else{
				winExp=0;
			}
			if (goalLocal.keySet().contains(t.getKey().getTargetState())){
				goalExp=goalLocal.get(t.getKey().getTargetState());
			}else{
				goalExp=0;
			}
			gamma=t.getValue();
			partialExp=bdd.imp(gamma, bdd.or(goalExp, winExp));
			returnExp=bdd.and(partialExp, returnExp);
			orGamma = bdd.or(gamma, orGamma);
			bdd.ref(returnExp);
		}
		bdd.ref(returnExp);
		returnExp = bdd.and(returnExp, orGamma);
		//bdd.printSet(returnExp);

		return returnExp;
	}

	private boolean isControllable(Transition transition){
		return ((RuntimeState)transition.getSourceState()).getObjectSystem().isControllable(((MessageEvent)transition.getEvent()).getSendingObject());
	}

	public void countNumberOfStatesAndTransitions(FeaturedMSDRuntimeStateGraph msdRuntimeStateGraph){

		for (State state : msdRuntimeStateGraph.getStates()) {
			numberOfExploredStates++;
			for (Transition transition : state.getOutgoingTransition()) {
				numberOfExploredTransitions++;
			}
		}

	}

	public Set<RuntimeState> getGoal() {
		return goal.keySet();
	}

	public void setGoal(Map<RuntimeState, Integer> goal) {
		this.goal = goal;
	}

	public Set<RuntimeState> getWinning() {
		return winning.keySet();
	}
	public  Map<RuntimeState, Integer> getWinningMap() {
		return winning;
	}
	public String getProducts() {
		return products;
	}

	public Integer getNumberOfRealizableProducts() {
		return numberOfRealizableProducts;
	}

	public Integer getNumberOfValidProducts() {
		return numberOfValidProducts;
	}

	public int getNumberOfExploredStates() {
		return numberOfExploredStates;
	}

	public void setNumberOfExploredStates(int numberOfExploredStates) {
		this.numberOfExploredStates = numberOfExploredStates;
	}

	public int getNumberOfExploredTransitions() {
		return numberOfExploredTransitions;
	}

	public void setNumberOfExploredTransitions(int numberOfExploredTransitions) {
		this.numberOfExploredTransitions = numberOfExploredTransitions;
	}

	public RuntimeStateGraph getNewController() {
		return newController;
	}


	private int passedStatesCounter = 1;
	private Set<RuntimeState> globalPassed=new HashSet<RuntimeState>();
	private void initStateAnnotations(RuntimeState state){
		/*
		 * this is for the graph
		 */
		if (!globalPassed.contains(state)) {
			state.getStringToStringAnnotationMap().put("passedIndex",
					String.valueOf(passedStatesCounter++));
			state.getStringToStringAnnotationMap().removeKey("stateLog");
			state.getStringToBooleanAnnotationMap().removeKey("win");
			state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
			state.getStringToBooleanAnnotationMap().removeKey("goal");
			globalPassed.add(state);
		}
	}


	private void setStateStatusFlags(RuntimeStateGraph graph){
		/*
		 * this is for the graph
		 */
		for(State state:graph.getStates()){
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("goal", goalToEvaluate.keySet().contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("loseBuechi", winning.keySet().contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("win", winning.keySet().contains(state));
		}
	}
}

