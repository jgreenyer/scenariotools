/**
 */
package org.scenariotools.timed.timedcontrollersystem.provider;

import org.eclipse.emf.common.EMFPlugin;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.provider.EcoreEditPlugin;

import org.eclipse.uml2.uml.edit.UMLEditPlugin;

import org.scenariotools.controllersystem.provider.ControllersystemEditPlugin;

import org.scenariotools.events.provider.EventsEditPlugin;

import org.scenariotools.msd.roles2eobjects.provider.Roles2eobjectsEditPlugin;

import org.scenariotools.msd.runtime.provider.RuntimeEditPlugin;

import org.scenariotools.msd.timed.timedruntime.provider.TimedruntimeEditPlugin;

import org.scenariotools.msd.uml2ecore.provider.Uml2ecoreEditPlugin;

import org.scenariotools.stategraph.provider.StategraphEditPlugin;

/**
 * This is the central singleton for the Timedcontrollersystem edit plugin.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public final class TimedcontrollersystemEditPlugin extends EMFPlugin {
	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final TimedcontrollersystemEditPlugin INSTANCE = new TimedcontrollersystemEditPlugin();

	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static Implementation plugin;

	/**
	 * Create the instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemEditPlugin() {
		super
		  (new ResourceLocator [] {
		     ControllersystemEditPlugin.INSTANCE,
		     EcoreEditPlugin.INSTANCE,
		     EventsEditPlugin.INSTANCE,
		     Roles2eobjectsEditPlugin.INSTANCE,
		     RuntimeEditPlugin.INSTANCE,
		     org.scenariotools.runtime.provider.RuntimeEditPlugin.INSTANCE,
		     StategraphEditPlugin.INSTANCE,
		     TimedruntimeEditPlugin.INSTANCE,
		     UMLEditPlugin.INSTANCE,
		     Uml2ecoreEditPlugin.INSTANCE,
		   });
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	public static Implementation getPlugin() {
		return plugin;
	}

	/**
	 * The actual implementation of the Eclipse <b>Plugin</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static class Implementation extends EclipsePlugin {
		/**
		 * Creates an instance.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Implementation() {
			super();

			// Remember the static instance.
			//
			plugin = this;
		}
	}

}
