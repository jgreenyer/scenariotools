package org.scenariotools.msd.timed.simulation.ui.launcher;

import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.scenariotools.msd.simulation.ui.launcher.ScenarioSimulationTabGroup;
import org.scenariotools.msd.simulation.ui.launcher.SimulationAgentConfigurationTab;

public class TimedScenarioSimulationTabGroup extends ScenarioSimulationTabGroup {

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
				new TimedScenarioRunConfigurationTab(),
				new SimulationAgentConfigurationTab(),
				new CommonTab()
				
		};
		setTabs(tabs);
	}
}
