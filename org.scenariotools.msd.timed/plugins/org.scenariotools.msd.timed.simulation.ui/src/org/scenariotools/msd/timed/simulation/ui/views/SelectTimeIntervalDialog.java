package org.scenariotools.msd.timed.simulation.ui.views;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.muml.udbm.ClockConstraint;
import org.muml.udbm.Federation;
import org.muml.udbm.SimpleClockConstraint;
import org.muml.udbm.UDBMClock;
import org.scenariotools.msd.timed.simulation.ui.Activator;

public class SelectTimeIntervalDialog extends Dialog {
	private List<String> clockNames;

	private Federation federation;

	private String selectedClockName;

	private Combo clockList;
	private Text lbText;
	private Text ubText;
	private Button lbExcludeButton;
	private Button ubExcludeButton;
	private Button noLbButton;
	private Button noUbButton;

	public SelectTimeIntervalDialog(Shell parent, List<String> clockNames,
			Federation federation) {
		super(parent);
		this.federation = federation;
		this.clockNames = clockNames;
	}

	private void updateClockInputElements(String selectedClockName) {
		this.selectedClockName = selectedClockName;
		UDBMClock clock = federation.getFromClock(selectedClockName);
		ClockConstraint lb = federation.getLowerBound(clock);
		ClockConstraint ub = federation.getUpperBound(clock);
		if (lb instanceof SimpleClockConstraint) {
			SimpleClockConstraint slb = (SimpleClockConstraint) lb;
			noLbButton.setSelection(false);
			lbExcludeButton.setSelection(!slb.getRelationalOperator()
					.toString().contains("="));
			lbText.setText(slb.getValue() + "");
		} else {
			noLbButton.setSelection(true);
			lbExcludeButton.setSelection(false);
			lbText.setText("");
		}
		if (ub instanceof SimpleClockConstraint) {
			SimpleClockConstraint sub = (SimpleClockConstraint) ub;
			noUbButton.setSelection(false);
			ubExcludeButton.setSelection(!sub.getRelationalOperator()
					.toString().contains("="));
			ubText.setText(sub.getValue() + "");
		} else {
			noUbButton.setSelection(true);
			ubExcludeButton.setSelection(false);
			ubText.setText("");
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(4, false);
		container.setLayout(layout);

		Label clockLabel = new Label(container, SWT.NONE);
		clockLabel.setText("Clock");
		clockList = new Combo(container, SWT.DROP_DOWN | SWT.BORDER);
		clockList.setItems(clockNames.toArray(new String[] {}));
		clockList.select(0);
		clockList.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateClockInputElements(clockList.getItem(clockList.getSelectionIndex()));
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lbLabel = new Label(container, SWT.NONE);
		lbLabel.setText("Lower Bound");
		lbText = new Text(container, SWT.SINGLE | SWT.BORDER);
		lbText.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		lbExcludeButton = new Button(container, SWT.CHECK);
		lbExcludeButton.setText("strict");
		noLbButton = new Button(container, SWT.CHECK);
		noLbButton.setText("unbounded");

		Label ubLabel = new Label(container, SWT.NONE);
		ubLabel.setText("Upper Bound");
		ubText = new Text(container, SWT.SINGLE | SWT.BORDER);
		ubText.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		ubExcludeButton = new Button(container, SWT.CHECK);
		ubExcludeButton.setText("strict");
		noUbButton = new Button(container, SWT.CHECK);
		noUbButton.setText("unbounded");
		updateClockInputElements(clockNames.get(0));
		return container;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select Sending Time Interval");
		newShell.setImage(Activator.getImageDescriptor(
				"img/play-out-event-interval.png").createImage());
	}

	private int minValue;
	private int maxValue;
	private boolean excludeLB;
	private boolean excludeUB;
	private boolean noLB;
	private boolean noUB;

	@Override
	protected void okPressed() {
		if (noLbButton.getSelection())
			minValue = 0;
		try {
			minValue = Integer.parseInt(lbText.getText());
		} catch (Exception e) {
			minValue = 0;
		}
		if (noUbButton.getSelection())
			maxValue = 0;
		try {
			maxValue = Integer.parseInt(ubText.getText());
		} catch (Exception e) {
			maxValue = 0;
		}
		excludeLB = lbExcludeButton.getSelection();
		excludeUB = ubExcludeButton.getSelection();
		noLB = noLbButton.getSelection();
		noUB = noUbButton.getSelection();

		super.okPressed();
	}

	public int getMinValue() {
		return minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public boolean isExcludeMinBound() {
		return excludeLB;
	}

	public boolean isExcludeMaxBound() {
		return excludeUB;
	}

	public String getSelectedClockName() {
		return selectedClockName;
	}

	public boolean isNoMinBound() {
		return noLB;
	}

	public boolean isNoMaxBound() {
		return noUB;
	}

}
