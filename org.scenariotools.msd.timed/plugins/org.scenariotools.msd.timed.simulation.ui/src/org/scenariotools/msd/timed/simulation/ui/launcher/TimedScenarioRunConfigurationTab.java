package org.scenariotools.msd.timed.simulation.ui.launcher;

import org.scenariotools.msd.simulation.ui.launcher.ScenarioRunConfigurationTab;

public class TimedScenarioRunConfigurationTab extends ScenarioRunConfigurationTab {
	protected boolean isFileExtensionOK(String fileExtension){
		return fileExtension.equals("scenariorunconfiguration") || fileExtension.equals("timedruntime");
	}
	
	protected String getFilenameHintText(){
		return "Please select a scenario run configuration file (*.scenariorunconfiguration) or a timed runtime file (*.timedruntime)";
	}
}
