package org.scenariotools.msd.timed.simulation.ui.views;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;
import org.muml.udbm.Federation;
import org.muml.udbm.UDBMClock;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.util.EventsUtil;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.simulation.ui.icons.ImageHelper;
import org.scenariotools.msd.simulation.ui.views.MSDModalMessageEventSelectionView;
import org.scenariotools.msd.timed.simulation.ui.Activator;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.util.RuntimeUtil;

public class TimedMSDModalMessageEventSelectionView extends
		MSDModalMessageEventSelectionView {
	protected boolean restrictEventIntervals = false;
	public static String MODAL_MESSAGE_EVENT_SELECTION_VIEW = "org.scenariotools.msd.timed.simulation.ui.views.TimedMessageEventSelectionView";

	@Override
	public void setInput(Collection<MSDModalMessageEvent> msdModalMessageEvents) {
		super.setInput(msdModalMessageEvents);
		Collection<EObject> viewerContent = new LinkedList<>();
		@SuppressWarnings("unchecked")
		Collection<MSDModalMessageEvent> modalEvents = ((Collection<MSDModalMessageEvent>) getViewer()
				.getInput());
		for (MSDModalMessageEvent modalEvent : modalEvents) {
			viewerContent.add(modalEvent);
		}
		if (getCurrentUserInteractingSimulationAgent().getSimulationManager()
				.getCurrentMSDRuntimeState() instanceof TimedMSDRuntimeState) {
			TimedMSDRuntimeState currentState = (TimedMSDRuntimeState) getCurrentUserInteractingSimulationAgent()
					.getSimulationManager().getCurrentMSDRuntimeState();
			for (TimeConditionEvent event : currentState
					.getCurrentTimeConditionEvents()) {
				viewerContent.add(event);
			}
		}
		getViewer().setInput(viewerContent);
		MSDRuntimeState msdRuntimeState = getCurrentSimulationManager()
				.getCurrentMSDRuntimeState();
		if (msdRuntimeState instanceof TimedMSDRuntimeState) {
			((TimedMSDRuntimeState) msdRuntimeState).getRuntimeUtil()
					.setEnforceEarliestDelaysFirst(!restrictEventIntervals);
		}
	}

	class TimedViewContentProvider extends ViewContentProvider {

		@Override
		public Object[] getElements(Object parent) {
			LinkedList<Object> result = new LinkedList<Object>();
			for (Object obj : super.getElements(parent))
				result.add(obj);
			if (parent instanceof Collection<?>) {
				for (Object obj : (Collection<?>) parent) {
					if (obj instanceof TimeConditionEvent)
						result.add(obj);
				}
			}
			return result.toArray();
		}

	}

	@Override
	protected FirstColumnLabelProvider getFirstColumnLabelProvider() {
		return new TimedFirstColumnLabelProvider();
	}

	@Override
	protected ViewContentProvider getViewContentProvider() {
		return new TimedViewContentProvider();
	}

	@Override
	protected MSDColumnLabelProvider getMSDColumnLabelProvider(int column) {
		return new TimedMSDColumnLabelProvider(column);
	}

	class TimedMSDColumnLabelProvider extends MSDColumnLabelProvider {

		public TimedMSDColumnLabelProvider(int column) {
			super(column);
		}

		@Override
		public String getText(Object element) {
			if (element instanceof TimeConditionEvent)
				return "";
			else
				return super.getText(element);
		}
	}

	public class TimedFirstColumnLabelProvider extends FirstColumnLabelProvider {

		@Override
		public String getText(Object element) {
			if (element instanceof TimeConditionEvent) {
				TimeConditionEvent timedEvent = (TimeConditionEvent) element;
				return "Timed Condition "
						+ timedEvent.getClockName()
						+ timedEvent.getOperatorString()
						+ timedEvent.getCompareValue()
						+ (timedEvent.isConstraintFulfilled() ? " fulfilled"
								: " NOT fulfilled");
			} else if (element instanceof MSDModalMessageEvent) {

				MessageEvent event = ((MSDModalMessageEvent) element)
						.getRepresentedMessageEvent();
				if (event instanceof AsynchronousSendEvent)
					return super.getText(element) + " (sending)";
				else if (event instanceof AsynchronousReceiveEvent) {
					int minDelay = ((AsynchronousReceiveEvent) event)
							.getMinDelay();
					int maxDelay = ((AsynchronousReceiveEvent) event)
							.getMaxDelay();
					return super.getText(element)
							+ " (receiving)"
							+ (minDelay != 0 || maxDelay != 0 ? "[" + minDelay
									+ "," + maxDelay + "]" : "");
				}
			}
			return super.getText(element);
		}

		@Override
		public Color getForeground(Object element) {
			if (element instanceof TimeConditionEvent) {
				TimeConditionEvent event = (TimeConditionEvent) element;
				if (event.isHot())
					return new Color(Display.getCurrent(), 255, 0, 0);
				else
					return new Color(Display.getCurrent(), 0, 0, 255);
			}
			return super.getForeground(element);
		}

	}

	@Override
	protected void objectDoubleClicked(Object object) {
		if (object instanceof TimeConditionEvent) {
			getCurrentUserInteractingSimulationAgent().setNextEvent(
					(Event) object);
		}
		if (restrictEventIntervals
				&& object instanceof MSDModalMessageEvent
				&& ((MSDModalMessageEvent) object).getRepresentedMessageEvent() instanceof TimeRestrictedMessageEvent) {
			TimeRestrictedMessageEvent event = (TimeRestrictedMessageEvent) ((MSDModalMessageEvent) object)
					.getRepresentedMessageEvent();

			TimedMSDRuntimeState currentState = (TimedMSDRuntimeState) getCurrentUserInteractingSimulationAgent()
					.getSimulationManager().getCurrentMSDRuntimeState();
			Federation fed = currentState.getFederation();
			Iterator<? extends UDBMClock> clockIt = fed.iteratorOfClock();
			List<String> clockNames = new LinkedList<String>();
			while (clockIt.hasNext()) {
				String clockName = clockIt.next().getName();
				if (!clockName.equals("zeroclock"))
					clockNames.add(clockName);
			}
			if (clockNames.size() > 0) {

				SelectTimeIntervalDialog dialog = new SelectTimeIntervalDialog(
						null, clockNames, fed);
				int ok = dialog.open();
				if (ok == Window.OK) {
					event.setClockName(dialog.getSelectedClockName());
					event.setMinBound(dialog.getMinValue());
					event.setMaxBound(dialog.getMaxValue());
					event.setExcludeMinBound(dialog.isExcludeMinBound());
					event.setExcludeMaxBound(dialog.isExcludeMaxBound());
				}
			}
		}
		super.objectDoubleClicked(object);
	}

	protected void toggleRestrictEventIntervals() {
		if (getCurrentSimulationManager() == null)
			return;

		restrictEventIntervals = toggleRestrictEventIntervalsAction.isChecked();
		((TimedMSDRuntimeState) getCurrentSimulationManager()
				.getCurrentMSDRuntimeState()).getRuntimeUtil()
				.setEnforceEarliestDelaysFirst(!restrictEventIntervals);
		refreshCurrentModelMessageEventsList();
	}

	Action toggleRestrictEventIntervalsAction;
	Action togglePlayEventSequenceAction;

	// FIXME: Needs refactoring. One part should be in the non-timed version,
	// method should be refactored into multiple methods
	protected void togglePlaySequenceMode() {

		// Open file dialog to get a sequence file
		// File Format: plain text. Every line contains exactly on message in
		// the form: "sender,receiver,messageName,timestamp,[int,string,boolean]=param" (e.g.
		// ego,ego,isEmcyBrakePossible,1,boolean=true)
		// If int,string, or boolean is not in the parameter part, then the
		// parameter is considered to be empty ("_")
		FileDialog dlg = new FileDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), SWT.OPEN);
		String fileName = dlg.open();
		// super.log(fileName);

		// Read in the file and do csv seperation
		BufferedReader br = null;
		String line = "";
		String cvsSeparator = ",";
		LinkedList<String> events = new LinkedList<String>();

		boolean success = true;

		try {
			br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				for (String current : line.split(cvsSeparator)) {
					events.add(current);
				}
			}
		} catch (FileNotFoundException e) {
			MessageDialog.openInformation(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					"MSDModalMessageEventSelectionView",
					"Event sequence not found. Stopping message playback!");
		} catch (IOException e) {
			MessageDialog
					.openInformation(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(),
							"MSDModalMessageEventSelectionView",
							"Event sequence could not be read. Stopping message playback!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// super.log(e.getMessage());
				}
			}
		}

		// Try simulating the message list.
		while (!events.isEmpty()) {
			String sendingObject = events.removeFirst();
			String receivingObject = events.removeFirst();
			String currentEvent = events.removeFirst();
			int currentTime = Integer.parseInt(events.removeFirst());
			String eventParam = events.removeFirst().toLowerCase();

			int parameterCode = -1;
			Object parameterValue = null;
			if (eventParam.indexOf("int=") != -1) {
				// Integer param supplied
				parameterValue = new Integer(Integer.parseInt(eventParam
						.substring(4)));
				parameterCode = 1;
			} else if (eventParam.indexOf("boolean=") != -1) {
				// Boolean param supplied
				parameterValue = new Boolean(Boolean.parseBoolean(eventParam
						.substring(8)));
				parameterCode = 2;
			} else if (eventParam.indexOf("string=") != -1) {
				// String param supplied
				parameterValue = new String(eventParam.substring(7));
				parameterCode = 3;
			}
			boolean executed = false;

			refreshCurrentModelMessageEventsList();

			Collection<MSDModalMessageEvent> msdEvents = (Collection<MSDModalMessageEvent>) (Collection<?>) getCurrentSimulationManager()
					.getCurrentMSDRuntimeState()
					.getMessageEventToModalMessageEventMap().values();

			// Find right event with matching or arbitrary parameters
			MSDModalMessageEvent parametrizedEvent = null;

			for (MSDModalMessageEvent current : msdEvents) {
				MessageEvent messageEvent = current
						.getRepresentedMessageEvent();
				
				String sender = RuntimeUtil.Helper.getEObjectName(current.getRepresentedMessageEvent().getSendingObject());
				String receiver = RuntimeUtil.Helper.getEObjectName(current.getRepresentedMessageEvent().getReceivingObject());
				
				if (current.getRepresentedMessageEvent().getMessageName()
						.equals(currentEvent) && sender.equals(sendingObject) && receiver.equals(receivingObject)) {
					if (messageEvent.isParameterized()) {
						switch (parameterCode) {
						case 1:
							if (parametrizedEvent == null
									&& !messageEvent
											.isSetIntegerParameterValue()) {
								parametrizedEvent = current;
							} else if (Integer.parseInt(EventsUtil
									.getParameterValueString(messageEvent)) == (Integer) parameterValue) {
								parametrizedEvent = current;
								executed = true;
							}
							break;
						case 2:
							if (parametrizedEvent == null
									&& !messageEvent
											.isSetBooleanParameterValue()) {
								parametrizedEvent = current;
							} else if (Boolean.parseBoolean(EventsUtil
									.getParameterValueString(messageEvent)) == (Boolean) parameterValue) {
								parametrizedEvent = current;
								executed = true;
							}
							break;
						case 3:
							if (parametrizedEvent == null
									&& !messageEvent
											.isSetStringParameterValue()) {
								parametrizedEvent = current;
							} else if (EventsUtil.getParameterValueString(
									messageEvent).equals(parameterValue)) {
								parametrizedEvent = current;
								executed = true;
							}
							break;
						default:
						}
					} else {
						parametrizedEvent = current;
						executed = true;
						break;
					}
					if (executed) {
						break;
					}
				}
			}

			// Set parameter if needed and execute event
			if (parametrizedEvent != null) {
				switch (parameterCode) {
				case 1:
					if (!parametrizedEvent.getRepresentedMessageEvent()
							.isSetIntegerParameterValue()) {
						parametrizedEvent
								.getRepresentedMessageEvent()
								.getRuntimeMessage()
								.setIntegerParameterValue(
										(Integer) parameterValue);
					}
					break;
				case 2:
					if (!parametrizedEvent.getRepresentedMessageEvent()
							.isSetBooleanParameterValue()) {
						parametrizedEvent
								.getRepresentedMessageEvent()
								.getRuntimeMessage()
								.setBooleanParameterValue(
										(Boolean) parameterValue);
					}
					break;
				case 3:
					if (!parametrizedEvent.getRepresentedMessageEvent()
							.isSetStringParameterValue()) {
						parametrizedEvent
								.getRepresentedMessageEvent()
								.getRuntimeMessage()
								.setStringParameterValue(
										(String) parameterValue);
					}
					break;
				default:
				}
				
				// executing event
				getCurrentUserInteractingSimulationAgent().setNextEvent(
						parametrizedEvent.getRepresentedMessageEvent());

				// Try to set the global clock, if present
				if (parametrizedEvent.getRepresentedMessageEvent() instanceof TimeRestrictedMessageEvent) {

					TimeRestrictedMessageEvent event = (TimeRestrictedMessageEvent) parametrizedEvent
							.getRepresentedMessageEvent();

					TimedMSDRuntimeState currentState = (TimedMSDRuntimeState) getCurrentUserInteractingSimulationAgent()
							.getSimulationManager().getCurrentMSDRuntimeState();

					Federation fed = currentState.getFederation();

					Iterator<? extends UDBMClock> clockIt = fed
							.iteratorOfClock();
					while (clockIt.hasNext()) {
						String clockName = clockIt.next().getName();
						if (clockName.equals("global.clock_0")) {
							event.setClockName(clockName);
							event.setMinBound(currentTime);
							event.setMaxBound(currentTime);
							event.setExcludeMinBound(false);
							event.setExcludeMaxBound(false);
							// super.log("Setting global clock to: " +
							// currentTime);
						}
					}
				}

				getCurrentUserInteractingSimulationAgent()
						.getSimulationManager()
						.performNextStepFromSimulationAgent(
								getCurrentUserInteractingSimulationAgent());

				// Check for occured safety violations
				if (getCurrentUserInteractingSimulationAgent()
						.getSimulationManager().getCurrentMSDRuntimeState()
						.isSafetyViolationOccurredInAssumptions()
						|| getCurrentUserInteractingSimulationAgent()
								.getSimulationManager()
								.getCurrentMSDRuntimeState()
								.isSafetyViolationOccurredInRequirements()) {
					// for (ActiveProcess test :
					// super.getCurrentUserInteractingSimulationAgent()
					// .getSimulationManager()
					// .getCurrentMSDRuntimeState().getActiveProcesses()) {
					// test.getCurrentState().
					// }
//					MessageDialog.openInformation(PlatformUI.getWorkbench()
//							.getActiveWorkbenchWindow().getShell(),
//							"MSDModalMessageEventSelectionView",
//							"Safety Violation!");
					success = false;
					break;
				}
			} else {
				MessageDialog.openInformation(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(),
						"MSDModalMessageEventSelectionView", "Event "
								+ sendingObject + ": " + receivingObject + "." + currentEvent
								+ " not found. Stopping message playback!");
				success = false;
				break;
			}
		}
		if (success) {
			MessageDialog.openInformation(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					"MSDModalMessageEventSelectionView",
					"Event sequence successfully played back!");
		}
	}

	@Override
	protected void makeActions() {
		super.makeActions();

		toggleRestrictEventIntervalsAction = new Action(
				"Toggle restriction of event intervals", Action.AS_CHECK_BOX) {
			public void run() {
				toggleRestrictEventIntervals();
			}
		};
		toggleRestrictEventIntervalsAction
				.setToolTipText("Ask user for event time intervals");
		toggleRestrictEventIntervalsAction.setImageDescriptor(Activator
				.getImageDescriptor("img/play-out-event-interval.png"));

		togglePlayEventSequenceAction = new Action(
				"Play Event Sequence", Action.AS_PUSH_BUTTON) {
			public void run() {
				togglePlaySequenceMode();
			}
		};
		togglePlayEventSequenceAction
				.setToolTipText("Automatically run message sequence!");
		togglePlayEventSequenceAction.setImageDescriptor(ImageHelper
				.getImageDescriptor("play.png"));
	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		super.fillLocalPullDown(manager);
		manager.add(toggleRestrictEventIntervalsAction);
		manager.add(togglePlayEventSequenceAction);
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		super.fillLocalToolBar(manager);
		manager.add(toggleRestrictEventIntervalsAction);
		manager.add(togglePlayEventSequenceAction);
	}
}
