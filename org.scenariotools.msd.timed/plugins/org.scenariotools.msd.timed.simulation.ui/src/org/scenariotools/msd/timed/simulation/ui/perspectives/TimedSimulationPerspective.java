package org.scenariotools.msd.timed.simulation.ui.perspectives;

import org.scenariotools.msd.simulation.ui.perspectives.SimulationPerspective;
import org.scenariotools.msd.timed.simulation.ui.views.TimedMSDModalMessageEventSelectionView;

public class TimedSimulationPerspective extends SimulationPerspective {
	protected String getMessageEventSelectionViewString(){
		return TimedMSDModalMessageEventSelectionView.MODAL_MESSAGE_EVENT_SELECTION_VIEW;
	}
}
