/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clock Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getOperatorString <em>Operator String</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getCompareValue <em>Compare Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockConstraint()
 * @model
 * @generated
 */
public interface ClockConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Operator String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator String</em>' attribute.
	 * @see #setOperatorString(String)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockConstraint_OperatorString()
	 * @model
	 * @generated
	 */
	String getOperatorString();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getOperatorString <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator String</em>' attribute.
	 * @see #getOperatorString()
	 * @generated
	 */
	void setOperatorString(String value);

	/**
	 * Returns the value of the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name</em>' attribute.
	 * @see #setClockName(String)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockConstraint_ClockName()
	 * @model
	 * @generated
	 */
	String getClockName();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getClockName <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Name</em>' attribute.
	 * @see #getClockName()
	 * @generated
	 */
	void setClockName(String value);

	/**
	 * Returns the value of the '<em><b>Compare Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compare Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compare Value</em>' attribute.
	 * @see #setCompareValue(int)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockConstraint_CompareValue()
	 * @model
	 * @generated
	 */
	int getCompareValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getCompareValue <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compare Value</em>' attribute.
	 * @see #getCompareValue()
	 * @generated
	 */
	void setCompareValue(int value);

} // ClockConstraint
