/**
 */
package org.scenariotools.timed.timedcontrollersystem.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedcontrollersystemXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		TimedcontrollersystemPackage.eINSTANCE.eClass();
	}
	
	/**
	 * Register for "*" and "xml" file extensions the TimedcontrollersystemResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new TimedcontrollersystemResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new TimedcontrollersystemResourceFactoryImpl());
		}
		return registrations;
	}

} //TimedcontrollersystemXMLProcessor
