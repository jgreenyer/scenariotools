/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.scenariotools.controllersystem.ControllerDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Controller Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedControllerDefinition()
 * @model
 * @generated
 */
public interface TimedControllerDefinition extends ControllerDefinition {
} // TimedControllerDefinition
