/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage
 * @generated
 */
public interface TimedcontrollersystemFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedcontrollersystemFactory eINSTANCE = org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Timed Controller Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Controller Definition</em>'.
	 * @generated
	 */
	TimedControllerDefinition createTimedControllerDefinition();

	/**
	 * Returns a new object of class '<em>Timed Controller State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Controller State</em>'.
	 * @generated
	 */
	TimedControllerState createTimedControllerState();

	/**
	 * Returns a new object of class '<em>Timed Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Transition</em>'.
	 * @generated
	 */
	TimedTransition createTimedTransition();

	/**
	 * Returns a new object of class '<em>Clock Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clock Constraint</em>'.
	 * @generated
	 */
	ClockConstraint createClockConstraint();

	/**
	 * Returns a new object of class '<em>Clock Reset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clock Reset</em>'.
	 * @generated
	 */
	ClockReset createClockReset();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TimedcontrollersystemPackage getTimedcontrollersystemPackage();

} //TimedcontrollersystemFactory
