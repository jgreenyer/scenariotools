/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.common.util.EList;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Controller State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getTimeInvariant <em>Time Invariant</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedControllerState()
 * @model
 * @generated
 */
public interface TimedControllerState extends RuntimeState {
	/**
	 * Returns the value of the '<em><b>Time Invariant</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Invariant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Invariant</em>' containment reference list.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedControllerState_TimeInvariant()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClockConstraint> getTimeInvariant();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedControllerState_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * @Generated NOT
	 */
	@Override
	public TimedMSDRuntimeStateGraph getStateGraph();

} // TimedControllerState
