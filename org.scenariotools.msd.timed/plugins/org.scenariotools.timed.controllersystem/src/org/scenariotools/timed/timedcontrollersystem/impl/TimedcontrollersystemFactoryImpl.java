/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.timed.timedcontrollersystem.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedcontrollersystemFactoryImpl extends EFactoryImpl implements TimedcontrollersystemFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TimedcontrollersystemFactory init() {
		try {
			TimedcontrollersystemFactory theTimedcontrollersystemFactory = (TimedcontrollersystemFactory)EPackage.Registry.INSTANCE.getEFactory("http://org.scenariotools.timed.controllersystem/1.0"); 
			if (theTimedcontrollersystemFactory != null) {
				return theTimedcontrollersystemFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TimedcontrollersystemFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_DEFINITION: return createTimedControllerDefinition();
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE: return createTimedControllerState();
			case TimedcontrollersystemPackage.TIMED_TRANSITION: return createTimedTransition();
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT: return createClockConstraint();
			case TimedcontrollersystemPackage.CLOCK_RESET: return createClockReset();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedControllerDefinition createTimedControllerDefinition() {
		TimedControllerDefinitionImpl timedControllerDefinition = new TimedControllerDefinitionImpl();
		return timedControllerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedControllerState createTimedControllerState() {
		TimedControllerStateImpl timedControllerState = new TimedControllerStateImpl();
		return timedControllerState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedTransition createTimedTransition() {
		TimedTransitionImpl timedTransition = new TimedTransitionImpl();
		return timedTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockConstraint createClockConstraint() {
		ClockConstraintImpl clockConstraint = new ClockConstraintImpl();
		return clockConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockReset createClockReset() {
		ClockResetImpl clockReset = new ClockResetImpl();
		return clockReset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemPackage getTimedcontrollersystemPackage() {
		return (TimedcontrollersystemPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TimedcontrollersystemPackage getPackage() {
		return TimedcontrollersystemPackage.eINSTANCE;
	}

} //TimedcontrollersystemFactoryImpl
