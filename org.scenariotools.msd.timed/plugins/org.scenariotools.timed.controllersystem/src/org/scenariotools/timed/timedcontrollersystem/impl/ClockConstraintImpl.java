/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clock Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl#getOperatorString <em>Operator String</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl#getCompareValue <em>Compare Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClockConstraintImpl extends EObjectImpl implements ClockConstraint {
	/**
	 * The default value of the '{@link #getOperatorString() <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorString()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATOR_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperatorString() <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorString()
	 * @generated
	 * @ordered
	 */
	protected String operatorString = OPERATOR_STRING_EDEFAULT;

	/**
	 * The default value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected static final String CLOCK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected String clockName = CLOCK_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCompareValue() <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareValue()
	 * @generated
	 * @ordered
	 */
	protected static final int COMPARE_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCompareValue() <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareValue()
	 * @generated
	 * @ordered
	 */
	protected int compareValue = COMPARE_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClockConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedcontrollersystemPackage.Literals.CLOCK_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperatorString() {
		return operatorString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatorString(String newOperatorString) {
		String oldOperatorString = operatorString;
		operatorString = newOperatorString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.CLOCK_CONSTRAINT__OPERATOR_STRING, oldOperatorString, operatorString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClockName() {
		return clockName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClockName(String newClockName) {
		String oldClockName = clockName;
		clockName = newClockName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.CLOCK_CONSTRAINT__CLOCK_NAME, oldClockName, clockName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCompareValue() {
		return compareValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompareValue(int newCompareValue) {
		int oldCompareValue = compareValue;
		compareValue = newCompareValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.CLOCK_CONSTRAINT__COMPARE_VALUE, oldCompareValue, compareValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__OPERATOR_STRING:
				return getOperatorString();
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__CLOCK_NAME:
				return getClockName();
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__COMPARE_VALUE:
				return getCompareValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__OPERATOR_STRING:
				setOperatorString((String)newValue);
				return;
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__CLOCK_NAME:
				setClockName((String)newValue);
				return;
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__COMPARE_VALUE:
				setCompareValue((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__OPERATOR_STRING:
				setOperatorString(OPERATOR_STRING_EDEFAULT);
				return;
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__CLOCK_NAME:
				setClockName(CLOCK_NAME_EDEFAULT);
				return;
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__COMPARE_VALUE:
				setCompareValue(COMPARE_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__OPERATOR_STRING:
				return OPERATOR_STRING_EDEFAULT == null ? operatorString != null : !OPERATOR_STRING_EDEFAULT.equals(operatorString);
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__CLOCK_NAME:
				return CLOCK_NAME_EDEFAULT == null ? clockName != null : !CLOCK_NAME_EDEFAULT.equals(clockName);
			case TimedcontrollersystemPackage.CLOCK_CONSTRAINT__COMPARE_VALUE:
				return compareValue != COMPARE_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operatorString: ");
		result.append(operatorString);
		result.append(", clockName: ");
		result.append(clockName);
		result.append(", compareValue: ");
		result.append(compareValue);
		result.append(')');
		return result.toString();
	}

} //ClockConstraintImpl
