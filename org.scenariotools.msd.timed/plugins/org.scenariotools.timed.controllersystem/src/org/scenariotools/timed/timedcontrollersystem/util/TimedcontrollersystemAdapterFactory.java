/**
 */
package org.scenariotools.timed.timedcontrollersystem.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.timed.timedcontrollersystem.*;
import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerState;
import org.scenariotools.timed.timedcontrollersystem.TimedTransition;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage
 * @generated
 */
public class TimedcontrollersystemAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TimedcontrollersystemPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TimedcontrollersystemPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedcontrollersystemSwitch<Adapter> modelSwitch =
		new TimedcontrollersystemSwitch<Adapter>() {
			@Override
			public Adapter caseTimedControllerDefinition(TimedControllerDefinition object) {
				return createTimedControllerDefinitionAdapter();
			}
			@Override
			public Adapter caseTimedControllerState(TimedControllerState object) {
				return createTimedControllerStateAdapter();
			}
			@Override
			public Adapter caseTimedTransition(TimedTransition object) {
				return createTimedTransitionAdapter();
			}
			@Override
			public Adapter caseClockConstraint(ClockConstraint object) {
				return createClockConstraintAdapter();
			}
			@Override
			public Adapter caseClockReset(ClockReset object) {
				return createClockResetAdapter();
			}
			@Override
			public Adapter caseControllerDefinition(ControllerDefinition object) {
				return createControllerDefinitionAdapter();
			}
			@Override
			public Adapter caseAnnotatableElement(AnnotatableElement object) {
				return createAnnotatableElementAdapter();
			}
			@Override
			public Adapter caseState(State object) {
				return createStateAdapter();
			}
			@Override
			public Adapter caseRuntimeState(RuntimeState object) {
				return createRuntimeStateAdapter();
			}
			@Override
			public Adapter caseTransition(Transition object) {
				return createTransitionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition <em>Timed Controller Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition
	 * @generated
	 */
	public Adapter createTimedControllerDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState <em>Timed Controller State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerState
	 * @generated
	 */
	public Adapter createTimedControllerStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition <em>Timed Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition
	 * @generated
	 */
	public Adapter createTimedTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint <em>Clock Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockConstraint
	 * @generated
	 */
	public Adapter createClockConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.timed.timedcontrollersystem.ClockReset <em>Clock Reset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockReset
	 * @generated
	 */
	public Adapter createClockResetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.controllersystem.ControllerDefinition <em>Controller Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.controllersystem.ControllerDefinition
	 * @generated
	 */
	public Adapter createControllerDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.AnnotatableElement <em>Annotatable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.AnnotatableElement
	 * @generated
	 */
	public Adapter createAnnotatableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	public Adapter createRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.Transition
	 * @generated
	 */
	public Adapter createTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TimedcontrollersystemAdapterFactory
