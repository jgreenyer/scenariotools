/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clock Reset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.ClockReset#getClockName <em>Clock Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockReset()
 * @model
 * @generated
 */
public interface ClockReset extends EObject {
	/**
	 * Returns the value of the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name</em>' attribute.
	 * @see #setClockName(String)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getClockReset_ClockName()
	 * @model
	 * @generated
	 */
	String getClockName();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.ClockReset#getClockName <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Name</em>' attribute.
	 * @see #getClockName()
	 * @generated
	 */
	void setClockName(String value);

} // ClockReset
