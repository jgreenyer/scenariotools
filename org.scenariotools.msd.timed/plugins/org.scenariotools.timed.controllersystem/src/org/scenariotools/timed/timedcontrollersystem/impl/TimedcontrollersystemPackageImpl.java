/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.stategraph.StategraphPackage;
import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.ClockReset;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerState;
import org.scenariotools.timed.timedcontrollersystem.TimedTransition;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemFactory;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedcontrollersystemPackageImpl extends EPackageImpl implements TimedcontrollersystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedControllerDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedControllerStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clockConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clockResetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TimedcontrollersystemPackageImpl() {
		super(eNS_URI, TimedcontrollersystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TimedcontrollersystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TimedcontrollersystemPackage init() {
		if (isInited) return (TimedcontrollersystemPackage)EPackage.Registry.INSTANCE.getEPackage(TimedcontrollersystemPackage.eNS_URI);

		// Obtain or create and register package
		TimedcontrollersystemPackageImpl theTimedcontrollersystemPackage = (TimedcontrollersystemPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TimedcontrollersystemPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TimedcontrollersystemPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ControllersystemPackage.eINSTANCE.eClass();
		TimedRuntimePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTimedcontrollersystemPackage.createPackageContents();

		// Initialize created meta-data
		theTimedcontrollersystemPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTimedcontrollersystemPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TimedcontrollersystemPackage.eNS_URI, theTimedcontrollersystemPackage);
		return theTimedcontrollersystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedControllerDefinition() {
		return timedControllerDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedControllerState() {
		return timedControllerStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedControllerState_TimeInvariant() {
		return (EReference)timedControllerStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedControllerState_Name() {
		return (EAttribute)timedControllerStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedTransition() {
		return timedTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedTransition_TimeGuard() {
		return (EReference)timedTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedTransition_Resets() {
		return (EReference)timedTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedTransition_Sending() {
		return (EAttribute)timedTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedTransition_Asynchronous() {
		return (EAttribute)timedTransitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClockConstraint() {
		return clockConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClockConstraint_OperatorString() {
		return (EAttribute)clockConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClockConstraint_ClockName() {
		return (EAttribute)clockConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClockConstraint_CompareValue() {
		return (EAttribute)clockConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClockReset() {
		return clockResetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClockReset_ClockName() {
		return (EAttribute)clockResetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedcontrollersystemFactory getTimedcontrollersystemFactory() {
		return (TimedcontrollersystemFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timedControllerDefinitionEClass = createEClass(TIMED_CONTROLLER_DEFINITION);

		timedControllerStateEClass = createEClass(TIMED_CONTROLLER_STATE);
		createEReference(timedControllerStateEClass, TIMED_CONTROLLER_STATE__TIME_INVARIANT);
		createEAttribute(timedControllerStateEClass, TIMED_CONTROLLER_STATE__NAME);

		timedTransitionEClass = createEClass(TIMED_TRANSITION);
		createEReference(timedTransitionEClass, TIMED_TRANSITION__TIME_GUARD);
		createEReference(timedTransitionEClass, TIMED_TRANSITION__RESETS);
		createEAttribute(timedTransitionEClass, TIMED_TRANSITION__SENDING);
		createEAttribute(timedTransitionEClass, TIMED_TRANSITION__ASYNCHRONOUS);

		clockConstraintEClass = createEClass(CLOCK_CONSTRAINT);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__OPERATOR_STRING);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__CLOCK_NAME);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__COMPARE_VALUE);

		clockResetEClass = createEClass(CLOCK_RESET);
		createEAttribute(clockResetEClass, CLOCK_RESET__CLOCK_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ControllersystemPackage theControllersystemPackage = (ControllersystemPackage)EPackage.Registry.INSTANCE.getEPackage(ControllersystemPackage.eNS_URI);
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);
		StategraphPackage theStategraphPackage = (StategraphPackage)EPackage.Registry.INSTANCE.getEPackage(StategraphPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timedControllerDefinitionEClass.getESuperTypes().add(theControllersystemPackage.getControllerDefinition());
		timedControllerStateEClass.getESuperTypes().add(theRuntimePackage.getRuntimeState());
		timedTransitionEClass.getESuperTypes().add(theStategraphPackage.getTransition());

		// Initialize classes and features; add operations and parameters
		initEClass(timedControllerDefinitionEClass, TimedControllerDefinition.class, "TimedControllerDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timedControllerStateEClass, TimedControllerState.class, "TimedControllerState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimedControllerState_TimeInvariant(), this.getClockConstraint(), null, "timeInvariant", null, 0, -1, TimedControllerState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedControllerState_Name(), ecorePackage.getEString(), "name", null, 0, 1, TimedControllerState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedTransitionEClass, TimedTransition.class, "TimedTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimedTransition_TimeGuard(), this.getClockConstraint(), null, "timeGuard", null, 0, -1, TimedTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimedTransition_Resets(), this.getClockReset(), null, "resets", null, 0, -1, TimedTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedTransition_Sending(), theEcorePackage.getEBoolean(), "sending", null, 0, 1, TimedTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedTransition_Asynchronous(), theEcorePackage.getEBoolean(), "asynchronous", null, 0, 1, TimedTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clockConstraintEClass, ClockConstraint.class, "ClockConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClockConstraint_OperatorString(), theEcorePackage.getEString(), "operatorString", null, 0, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClockConstraint_ClockName(), theEcorePackage.getEString(), "clockName", null, 0, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClockConstraint_CompareValue(), theEcorePackage.getEInt(), "compareValue", null, 0, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clockResetEClass, ClockReset.class, "ClockReset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClockReset_ClockName(), theEcorePackage.getEString(), "clockName", null, 0, 1, ClockReset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TimedcontrollersystemPackageImpl
