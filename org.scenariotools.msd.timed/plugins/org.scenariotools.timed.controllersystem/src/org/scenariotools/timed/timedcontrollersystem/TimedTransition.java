/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#getTimeGuard <em>Time Guard</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#getResets <em>Resets</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isSending <em>Sending</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isAsynchronous <em>Asynchronous</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedTransition()
 * @model
 * @generated
 */
public interface TimedTransition extends Transition {
	/**
	 * Returns the value of the '<em><b>Time Guard</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Guard</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Guard</em>' containment reference list.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedTransition_TimeGuard()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClockConstraint> getTimeGuard();
	
	/**
	 * Returns the value of the '<em><b>Resets</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.timed.timedcontrollersystem.ClockReset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resets</em>' containment reference list.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedTransition_Resets()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClockReset> getResets();

	/**
	 * Returns the value of the '<em><b>Sending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sending</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sending</em>' attribute.
	 * @see #setSending(boolean)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedTransition_Sending()
	 * @model
	 * @generated
	 */
	boolean isSending();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isSending <em>Sending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sending</em>' attribute.
	 * @see #isSending()
	 * @generated
	 */
	void setSending(boolean value);

	/**
	 * Returns the value of the '<em><b>Asynchronous</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous</em>' attribute.
	 * @see #setAsynchronous(boolean)
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage#getTimedTransition_Asynchronous()
	 * @model
	 * @generated
	 */
	boolean isAsynchronous();

	/**
	 * Sets the value of the '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isAsynchronous <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous</em>' attribute.
	 * @see #isAsynchronous()
	 * @generated
	 */
	void setAsynchronous(boolean value);

	/**
	 * @Generated NOT
	 */
	@Override
	TimedControllerState getSourceState();

	/**
	 * @Generated NOT
	 */
	@Override
	TimedControllerState getTargetState();
} // TimedTransition
