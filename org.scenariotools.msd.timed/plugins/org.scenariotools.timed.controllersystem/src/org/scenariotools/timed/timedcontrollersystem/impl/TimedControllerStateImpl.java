/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.runtime.impl.RuntimeStateImpl;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl;
import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerState;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Controller State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl#getTimeInvariant <em>Time Invariant</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TimedControllerStateImpl extends RuntimeStateImpl implements TimedControllerState {
	/**
	 * The cached value of the '{@link #getTimeInvariant() <em>Time Invariant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeInvariant()
	 * @generated
	 * @ordered
	 */
	protected EList<ClockConstraint> timeInvariant;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedControllerStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedcontrollersystemPackage.Literals.TIMED_CONTROLLER_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClockConstraint> getTimeInvariant() {
		if (timeInvariant == null) {
			timeInvariant = new EObjectContainmentEList<ClockConstraint>(ClockConstraint.class, this, TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT);
		}
		return timeInvariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT:
				return ((InternalEList<?>)getTimeInvariant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT:
				return getTimeInvariant();
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT:
				getTimeInvariant().clear();
				getTimeInvariant().addAll((Collection<? extends ClockConstraint>)newValue);
				return;
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT:
				getTimeInvariant().clear();
				return;
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__TIME_INVARIANT:
				return timeInvariant != null && !timeInvariant.isEmpty();
			case TimedcontrollersystemPackage.TIMED_CONTROLLER_STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

	/**
	 * @Generated NOT
	 */
	@Override
	public TimedMSDRuntimeStateGraph getStateGraph() {
		return (TimedMSDRuntimeStateGraph)super.getStateGraph();
	}

} //TimedControllerStateImpl
