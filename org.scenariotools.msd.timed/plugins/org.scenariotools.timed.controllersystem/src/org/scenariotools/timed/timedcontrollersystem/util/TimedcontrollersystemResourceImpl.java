/**
 */
package org.scenariotools.timed.timedcontrollersystem.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.scenariotools.timed.timedcontrollersystem.util.TimedcontrollersystemResourceFactoryImpl
 * @generated
 */
public class TimedcontrollersystemResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public TimedcontrollersystemResourceImpl(URI uri) {
		super(uri);
	}

} //TimedcontrollersystemResourceImpl
