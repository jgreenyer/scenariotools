/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.controllersystem.impl.ControllerDefinitionImpl;

import org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Controller Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TimedControllerDefinitionImpl extends ControllerDefinitionImpl implements TimedControllerDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedControllerDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedcontrollersystemPackage.Literals.TIMED_CONTROLLER_DEFINITION;
	}

} //TimedControllerDefinitionImpl
