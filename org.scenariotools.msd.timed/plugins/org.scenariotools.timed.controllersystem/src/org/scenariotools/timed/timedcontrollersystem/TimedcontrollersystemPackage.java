/**
 */
package org.scenariotools.timed.timedcontrollersystem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.scenariotools.controllersystem.ControllersystemPackage;

import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemFactory
 * @model kind="package"
 * @generated
 */
public interface TimedcontrollersystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "timedcontrollersystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.timed.controllersystem/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "timedcontrollersystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedcontrollersystemPackage eINSTANCE = org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerDefinitionImpl <em>Timed Controller Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerDefinitionImpl
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedControllerDefinition()
	 * @generated
	 */
	int TIMED_CONTROLLER_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Controlled Objects Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP = ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_DEFINITION__GRAPH = ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH;

	/**
	 * The number of structural features of the '<em>Timed Controller Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_DEFINITION_FEATURE_COUNT = ControllersystemPackage.CONTROLLER_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl <em>Timed Controller State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedControllerState()
	 * @generated
	 */
	int TIMED_CONTROLLER_STATE = 1;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = RuntimePackage.RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__STRING_TO_STRING_ANNOTATION_MAP = RuntimePackage.RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = RuntimePackage.RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__OUTGOING_TRANSITION = RuntimePackage.RUNTIME_STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__INCOMING_TRANSITION = RuntimePackage.RUNTIME_STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__STATE_GRAPH = RuntimePackage.RUNTIME_STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__OBJECT_SYSTEM = RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__EVENT_TO_TRANSITION_MAP = RuntimePackage.RUNTIME_STATE__EVENT_TO_TRANSITION_MAP;

	/**
	 * The feature id for the '<em><b>Time Invariant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__TIME_INVARIANT = RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE__NAME = RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Timed Controller State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_CONTROLLER_STATE_FEATURE_COUNT = RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl <em>Timed Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedTransition()
	 * @generated
	 */
	int TIMED_TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__STRING_TO_BOOLEAN_ANNOTATION_MAP = StategraphPackage.TRANSITION__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__STRING_TO_STRING_ANNOTATION_MAP = StategraphPackage.TRANSITION__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__STRING_TO_EOBJECT_ANNOTATION_MAP = StategraphPackage.TRANSITION__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Source State</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__SOURCE_STATE = StategraphPackage.TRANSITION__SOURCE_STATE;

	/**
	 * The feature id for the '<em><b>Target State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__TARGET_STATE = StategraphPackage.TRANSITION__TARGET_STATE;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__EVENT = StategraphPackage.TRANSITION__EVENT;

	/**
	 * The feature id for the '<em><b>Time Guard</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__TIME_GUARD = StategraphPackage.TRANSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__RESETS = StategraphPackage.TRANSITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__SENDING = StategraphPackage.TRANSITION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Asynchronous</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION__ASYNCHRONOUS = StategraphPackage.TRANSITION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Timed Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_TRANSITION_FEATURE_COUNT = StategraphPackage.TRANSITION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl <em>Clock Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getClockConstraint()
	 * @generated
	 */
	int CLOCK_CONSTRAINT = 3;

	/**
	 * The feature id for the '<em><b>Operator String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT__OPERATOR_STRING = 0;

	/**
	 * The feature id for the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT__CLOCK_NAME = 1;

	/**
	 * The feature id for the '<em><b>Compare Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT__COMPARE_VALUE = 2;

	/**
	 * The number of structural features of the '<em>Clock Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT_FEATURE_COUNT = 3;


	/**
	 * The meta object id for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockResetImpl <em>Clock Reset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.ClockResetImpl
	 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getClockReset()
	 * @generated
	 */
	int CLOCK_RESET = 4;

	/**
	 * The feature id for the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_RESET__CLOCK_NAME = 0;

	/**
	 * The number of structural features of the '<em>Clock Reset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_RESET_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition <em>Timed Controller Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Controller Definition</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerDefinition
	 * @generated
	 */
	EClass getTimedControllerDefinition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState <em>Timed Controller State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Controller State</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerState
	 * @generated
	 */
	EClass getTimedControllerState();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getTimeInvariant <em>Time Invariant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Time Invariant</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getTimeInvariant()
	 * @see #getTimedControllerState()
	 * @generated
	 */
	EReference getTimedControllerState_TimeInvariant();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedControllerState#getName()
	 * @see #getTimedControllerState()
	 * @generated
	 */
	EAttribute getTimedControllerState_Name();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition <em>Timed Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Transition</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition
	 * @generated
	 */
	EClass getTimedTransition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#getTimeGuard <em>Time Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Time Guard</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition#getTimeGuard()
	 * @see #getTimedTransition()
	 * @generated
	 */
	EReference getTimedTransition_TimeGuard();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#getResets <em>Resets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resets</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition#getResets()
	 * @see #getTimedTransition()
	 * @generated
	 */
	EReference getTimedTransition_Resets();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isSending <em>Sending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sending</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition#isSending()
	 * @see #getTimedTransition()
	 * @generated
	 */
	EAttribute getTimedTransition_Sending();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.TimedTransition#isAsynchronous <em>Asynchronous</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Asynchronous</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.TimedTransition#isAsynchronous()
	 * @see #getTimedTransition()
	 * @generated
	 */
	EAttribute getTimedTransition_Asynchronous();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint <em>Clock Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clock Constraint</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockConstraint
	 * @generated
	 */
	EClass getClockConstraint();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getOperatorString <em>Operator String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator String</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getOperatorString()
	 * @see #getClockConstraint()
	 * @generated
	 */
	EAttribute getClockConstraint_OperatorString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getClockName <em>Clock Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock Name</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getClockName()
	 * @see #getClockConstraint()
	 * @generated
	 */
	EAttribute getClockConstraint_ClockName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getCompareValue <em>Compare Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compare Value</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockConstraint#getCompareValue()
	 * @see #getClockConstraint()
	 * @generated
	 */
	EAttribute getClockConstraint_CompareValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.timed.timedcontrollersystem.ClockReset <em>Clock Reset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clock Reset</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockReset
	 * @generated
	 */
	EClass getClockReset();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.timed.timedcontrollersystem.ClockReset#getClockName <em>Clock Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock Name</em>'.
	 * @see org.scenariotools.timed.timedcontrollersystem.ClockReset#getClockName()
	 * @see #getClockReset()
	 * @generated
	 */
	EAttribute getClockReset_ClockName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TimedcontrollersystemFactory getTimedcontrollersystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerDefinitionImpl <em>Timed Controller Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerDefinitionImpl
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedControllerDefinition()
		 * @generated
		 */
		EClass TIMED_CONTROLLER_DEFINITION = eINSTANCE.getTimedControllerDefinition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl <em>Timed Controller State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedControllerStateImpl
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedControllerState()
		 * @generated
		 */
		EClass TIMED_CONTROLLER_STATE = eINSTANCE.getTimedControllerState();

		/**
		 * The meta object literal for the '<em><b>Time Invariant</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_CONTROLLER_STATE__TIME_INVARIANT = eINSTANCE.getTimedControllerState_TimeInvariant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_CONTROLLER_STATE__NAME = eINSTANCE.getTimedControllerState_Name();

		/**
		 * The meta object literal for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl <em>Timed Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getTimedTransition()
		 * @generated
		 */
		EClass TIMED_TRANSITION = eINSTANCE.getTimedTransition();

		/**
		 * The meta object literal for the '<em><b>Time Guard</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_TRANSITION__TIME_GUARD = eINSTANCE.getTimedTransition_TimeGuard();

		/**
		 * The meta object literal for the '<em><b>Resets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_TRANSITION__RESETS = eINSTANCE.getTimedTransition_Resets();

		/**
		 * The meta object literal for the '<em><b>Sending</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_TRANSITION__SENDING = eINSTANCE.getTimedTransition_Sending();

		/**
		 * The meta object literal for the '<em><b>Asynchronous</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_TRANSITION__ASYNCHRONOUS = eINSTANCE.getTimedTransition_Asynchronous();

		/**
		 * The meta object literal for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl <em>Clock Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.ClockConstraintImpl
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getClockConstraint()
		 * @generated
		 */
		EClass CLOCK_CONSTRAINT = eINSTANCE.getClockConstraint();

		/**
		 * The meta object literal for the '<em><b>Operator String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOCK_CONSTRAINT__OPERATOR_STRING = eINSTANCE.getClockConstraint_OperatorString();

		/**
		 * The meta object literal for the '<em><b>Clock Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOCK_CONSTRAINT__CLOCK_NAME = eINSTANCE.getClockConstraint_ClockName();

		/**
		 * The meta object literal for the '<em><b>Compare Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOCK_CONSTRAINT__COMPARE_VALUE = eINSTANCE.getClockConstraint_CompareValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.timed.timedcontrollersystem.impl.ClockResetImpl <em>Clock Reset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.ClockResetImpl
		 * @see org.scenariotools.timed.timedcontrollersystem.impl.TimedcontrollersystemPackageImpl#getClockReset()
		 * @generated
		 */
		EClass CLOCK_RESET = eINSTANCE.getClockReset();

		/**
		 * The meta object literal for the '<em><b>Clock Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOCK_RESET__CLOCK_NAME = eINSTANCE.getClockReset_ClockName();

	}

} //TimedcontrollersystemPackage
