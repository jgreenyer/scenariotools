/**
 */
package org.scenariotools.timed.timedcontrollersystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.stategraph.impl.TransitionImpl;
import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.ClockReset;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerState;
import org.scenariotools.timed.timedcontrollersystem.TimedTransition;
import org.scenariotools.timed.timedcontrollersystem.TimedcontrollersystemPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl#getTimeGuard <em>Time Guard</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl#getResets <em>Resets</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl#isSending <em>Sending</em>}</li>
 *   <li>{@link org.scenariotools.timed.timedcontrollersystem.impl.TimedTransitionImpl#isAsynchronous <em>Asynchronous</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TimedTransitionImpl extends TransitionImpl implements TimedTransition {
	/**
	 * The cached value of the '{@link #getTimeGuard() <em>Time Guard</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeGuard()
	 * @generated
	 * @ordered
	 */
	protected EList<ClockConstraint> timeGuard;

	/**
	 * The cached value of the '{@link #getResets() <em>Resets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResets()
	 * @generated
	 * @ordered
	 */
	protected EList<ClockReset> resets;

	/**
	 * The default value of the '{@link #isSending() <em>Sending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSending()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SENDING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSending() <em>Sending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSending()
	 * @generated
	 * @ordered
	 */
	protected boolean sending = SENDING_EDEFAULT;

	/**
	 * The default value of the '{@link #isAsynchronous() <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ASYNCHRONOUS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAsynchronous() <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected boolean asynchronous = ASYNCHRONOUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedcontrollersystemPackage.Literals.TIMED_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClockConstraint> getTimeGuard() {
		if (timeGuard == null) {
			timeGuard = new EObjectContainmentEList<ClockConstraint>(ClockConstraint.class, this, TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD);
		}
		return timeGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClockReset> getResets() {
		if (resets == null) {
			resets = new EObjectContainmentEList<ClockReset>(ClockReset.class, this, TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS);
		}
		return resets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSending() {
		return sending;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSending(boolean newSending) {
		boolean oldSending = sending;
		sending = newSending;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.TIMED_TRANSITION__SENDING, oldSending, sending));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAsynchronous() {
		return asynchronous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronous(boolean newAsynchronous) {
		boolean oldAsynchronous = asynchronous;
		asynchronous = newAsynchronous;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedcontrollersystemPackage.TIMED_TRANSITION__ASYNCHRONOUS, oldAsynchronous, asynchronous));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD:
				return ((InternalEList<?>)getTimeGuard()).basicRemove(otherEnd, msgs);
			case TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS:
				return ((InternalEList<?>)getResets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD:
				return getTimeGuard();
			case TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS:
				return getResets();
			case TimedcontrollersystemPackage.TIMED_TRANSITION__SENDING:
				return isSending();
			case TimedcontrollersystemPackage.TIMED_TRANSITION__ASYNCHRONOUS:
				return isAsynchronous();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD:
				getTimeGuard().clear();
				getTimeGuard().addAll((Collection<? extends ClockConstraint>)newValue);
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS:
				getResets().clear();
				getResets().addAll((Collection<? extends ClockReset>)newValue);
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__SENDING:
				setSending((Boolean)newValue);
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__ASYNCHRONOUS:
				setAsynchronous((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD:
				getTimeGuard().clear();
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS:
				getResets().clear();
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__SENDING:
				setSending(SENDING_EDEFAULT);
				return;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__ASYNCHRONOUS:
				setAsynchronous(ASYNCHRONOUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedcontrollersystemPackage.TIMED_TRANSITION__TIME_GUARD:
				return timeGuard != null && !timeGuard.isEmpty();
			case TimedcontrollersystemPackage.TIMED_TRANSITION__RESETS:
				return resets != null && !resets.isEmpty();
			case TimedcontrollersystemPackage.TIMED_TRANSITION__SENDING:
				return sending != SENDING_EDEFAULT;
			case TimedcontrollersystemPackage.TIMED_TRANSITION__ASYNCHRONOUS:
				return asynchronous != ASYNCHRONOUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sending: ");
		result.append(sending);
		result.append(", asynchronous: ");
		result.append(asynchronous);
		result.append(')');
		return result.toString();
	}

	/**
	 * @Generated NOT
	 */
	@Override
	public TimedControllerState getSourceState() {
		return (TimedControllerState)super.getSourceState();
	}

	/**
	 * @Generated NOT
	 */
	@Override
	public TimedControllerState getTargetState() {
		return (TimedControllerState)super.getTargetState();
	}

} //TimedTransitionImpl
