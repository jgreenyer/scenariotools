/**
 */
package org.scenariotools.msd.timed.timedsimulation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.timed.timedsimulation.TimedSimulationPackage
 * @generated
 */
public interface TimedSimulationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedSimulationFactory eINSTANCE = org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Manager</em>'.
	 * @generated
	 */
	TimedSimulationManager createTimedSimulationManager();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TimedSimulationPackage getTimedSimulationPackage();

} //TimedSimulationFactory
