package org.scenariotools.msd.timed.timedsimulation.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.scenariotools.msd.simulation.debug.AbstractScenarioThread;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;

public class ClockZoneThread extends AbstractScenarioThread {
	protected TimedMSDRuntimeState timedMSDRuntimeState;

	public ClockZoneThread(IDebugTarget target,
			TimedMSDRuntimeState timedMSDRuntimeState) {
		super(target);
		this.timedMSDRuntimeState = timedMSDRuntimeState;
	}

	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		if (timedMSDRuntimeState.getMessageQueuesForLinks().size() > 0)
			return new IStackFrame[] {
					new ClockZoneFrame(getDebugTarget(), timedMSDRuntimeState,
							this),
					new BuffersFrame(getDebugTarget(), timedMSDRuntimeState,
							this) };
		else
			return new IStackFrame[] { new ClockZoneFrame(getDebugTarget(),
					timedMSDRuntimeState, this) };
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}

	@Override
	public String getName() throws DebugException {
		return "Timed State Information";
	}

}
