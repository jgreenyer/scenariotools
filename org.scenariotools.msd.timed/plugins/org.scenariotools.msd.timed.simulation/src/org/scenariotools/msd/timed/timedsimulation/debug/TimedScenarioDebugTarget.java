package org.scenariotools.msd.timed.timedsimulation.debug;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IThread;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.debug.ScenarioDebugTarget;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedsimulation.TimedSimulationFactory;

public class TimedScenarioDebugTarget extends ScenarioDebugTarget {

	public TimedScenarioDebugTarget(ILaunch launch,
			ScenarioRunConfiguration scenarioRunConfiguration,
			String configuredEnvironmentSimulationAgentNsURI,
			String configuredEnvironmentSimulationAgentEClassName,
			String configuredSystemSimulationAgentNsURI,
			String configuredSystemSimulationAgentEClassName,
			String delayMilliseconds, boolean startInPauseMode) {
		super(launch, scenarioRunConfiguration,
				configuredEnvironmentSimulationAgentNsURI,
				configuredEnvironmentSimulationAgentEClassName,
				configuredSystemSimulationAgentNsURI,
				configuredSystemSimulationAgentEClassName, delayMilliseconds,
				startInPauseMode);
	}

	@Override
	protected SimulationManager createSimulationManager() {
		return TimedSimulationFactory.eINSTANCE.createTimedSimulationManager();
	}

	@Override
	protected MSDRuntimeStateGraph createMSDRuntimeStateGraph() {
		return TimedRuntimeFactory.eINSTANCE.createTimedMSDRuntimeStateGraph();
	}

	@Override
	public IThread[] getThreads() throws DebugException {
		List<IThread> threads = new LinkedList<>(Arrays.asList(super.getThreads()));
		threads.add(new ClockZoneThread(getDebugTarget(), ((TimedMSDRuntimeState) this
				.getSimulationManager().getCurrentMSDRuntimeState())));
		return threads.toArray(new IThread[] {});
	}

}
