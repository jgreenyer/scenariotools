package org.scenariotools.msd.timed.timedsimulation.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.scenariotools.msd.simulation.debug.ObjectValue;
import org.scenariotools.msd.simulation.debug.ScenarioDebugElement;

public class ClockZoneVariable extends ScenarioDebugElement implements
		IVariable {
	private String value;

	public ClockZoneVariable(IDebugTarget target, String value) {
		super(target);
		this.value = value;
	}

	@Override
	public void setValue(String expression) throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean supportsValueModification() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(String expression) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IValue getValue() throws DebugException {
		return new ObjectValue(getDebugTarget(), value);
	}

	@Override
	public String getName() throws DebugException {
		return "Clock Condition";
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasValueChanged() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

}
