/**
 */
package org.scenariotools.msd.timed.timedsimulation;

import org.scenariotools.msd.simulation.SimulationManager;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.timed.timedsimulation.TimedSimulationPackage#getTimedSimulationManager()
 * @model
 * @generated
 */
public interface TimedSimulationManager extends SimulationManager {
} // TimedSimulationManager
