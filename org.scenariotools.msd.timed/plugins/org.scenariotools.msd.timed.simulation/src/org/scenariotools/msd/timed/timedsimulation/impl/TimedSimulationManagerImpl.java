/**
 */
package org.scenariotools.msd.timed.timedsimulation.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.msd.simulation.impl.SimulationManagerImpl;

import org.scenariotools.msd.timed.timedsimulation.TimedSimulationManager;
import org.scenariotools.msd.timed.timedsimulation.TimedSimulationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TimedSimulationManagerImpl extends SimulationManagerImpl implements TimedSimulationManager {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedSimulationManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedSimulationPackage.Literals.TIMED_SIMULATION_MANAGER;
	}

} //TimedSimulationManagerImpl
