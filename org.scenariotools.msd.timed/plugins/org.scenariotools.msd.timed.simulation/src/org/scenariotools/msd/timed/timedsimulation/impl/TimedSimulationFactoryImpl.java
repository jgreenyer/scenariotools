/**
 */
package org.scenariotools.msd.timed.timedsimulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.msd.timed.timedsimulation.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedSimulationFactoryImpl extends EFactoryImpl implements TimedSimulationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TimedSimulationFactory init() {
		try {
			TimedSimulationFactory theTimedSimulationFactory = (TimedSimulationFactory)EPackage.Registry.INSTANCE.getEFactory("http://org.scenariotools.msd.timed.simulation/1.0"); 
			if (theTimedSimulationFactory != null) {
				return theTimedSimulationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TimedSimulationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedSimulationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TimedSimulationPackage.TIMED_SIMULATION_MANAGER: return createTimedSimulationManager();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedSimulationManager createTimedSimulationManager() {
		TimedSimulationManagerImpl timedSimulationManager = new TimedSimulationManagerImpl();
		return timedSimulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedSimulationPackage getTimedSimulationPackage() {
		return (TimedSimulationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TimedSimulationPackage getPackage() {
		return TimedSimulationPackage.eINSTANCE;
	}

} //TimedSimulationFactoryImpl
