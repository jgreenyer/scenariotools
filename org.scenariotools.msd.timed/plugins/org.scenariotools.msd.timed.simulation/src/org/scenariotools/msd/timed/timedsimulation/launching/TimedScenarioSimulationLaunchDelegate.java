package org.scenariotools.msd.timed.timedsimulation.launching;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.simulation.launching.ScenarioSimulationLaunchConfigurationKeys;
import org.scenariotools.msd.simulation.launching.ScenarioSimulationLaunchDelegate;
import org.scenariotools.msd.timed.timedsimulation.debug.TimedScenarioDebugTarget;

public class TimedScenarioSimulationLaunchDelegate extends
		ScenarioSimulationLaunchDelegate {

	@Override
	protected IDebugTarget createDebugTarget(ILaunch launch,
			ScenarioRunConfiguration scenarioRunConfiguration,
			ILaunchConfiguration configuration, String delayMilliseconds,
			boolean startInPauseMode) throws CoreException {
		return new TimedScenarioDebugTarget(launch, scenarioRunConfiguration, 
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, ""),
				delayMilliseconds,
				startInPauseMode);	
	}

}
