/**
 */
package org.scenariotools.msd.timed.timedsimulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.scenariotools.msd.simulation.SimulationPackage;

import org.scenariotools.msd.timed.timedsimulation.TimedSimulationFactory;
import org.scenariotools.msd.timed.timedsimulation.TimedSimulationManager;
import org.scenariotools.msd.timed.timedsimulation.TimedSimulationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedSimulationPackageImpl extends EPackageImpl implements TimedSimulationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedSimulationManagerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.timed.timedsimulation.TimedSimulationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TimedSimulationPackageImpl() {
		super(eNS_URI, TimedSimulationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TimedSimulationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TimedSimulationPackage init() {
		if (isInited) return (TimedSimulationPackage)EPackage.Registry.INSTANCE.getEPackage(TimedSimulationPackage.eNS_URI);

		// Obtain or create and register package
		TimedSimulationPackageImpl theTimedSimulationPackage = (TimedSimulationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TimedSimulationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TimedSimulationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SimulationPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTimedSimulationPackage.createPackageContents();

		// Initialize created meta-data
		theTimedSimulationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTimedSimulationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TimedSimulationPackage.eNS_URI, theTimedSimulationPackage);
		return theTimedSimulationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedSimulationManager() {
		return timedSimulationManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedSimulationFactory getTimedSimulationFactory() {
		return (TimedSimulationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timedSimulationManagerEClass = createEClass(TIMED_SIMULATION_MANAGER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SimulationPackage theSimulationPackage = (SimulationPackage)EPackage.Registry.INSTANCE.getEPackage(SimulationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timedSimulationManagerEClass.getESuperTypes().add(theSimulationPackage.getSimulationManager());

		// Initialize classes and features; add operations and parameters
		initEClass(timedSimulationManagerEClass, TimedSimulationManager.class, "TimedSimulationManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //TimedSimulationPackageImpl
