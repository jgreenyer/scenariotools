package org.scenariotools.msd.timed.timedsimulation.debug;

import org.eclipse.debug.core.model.IDebugTarget;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.simulation.debug.ActiveMSDThread;

public class TimedActiveMSDThread extends ActiveMSDThread {

	public TimedActiveMSDThread(IDebugTarget target, ActiveMSD activeMSD) {
		super(target, activeMSD);
	}

}
