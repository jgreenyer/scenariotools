/**
 */
package org.scenariotools.msd.timed.timedsimulation;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.scenariotools.msd.simulation.SimulationPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.timed.timedsimulation.TimedSimulationFactory
 * @model kind="package"
 * @generated
 */
public interface TimedSimulationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "timedsimulation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.timed.simulation/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "timedsimulation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedSimulationPackage eINSTANCE = org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationManagerImpl <em>Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationManagerImpl
	 * @see org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationPackageImpl#getTimedSimulationManager()
	 * @generated
	 */
	int TIMED_SIMULATION_MANAGER = 0;

	/**
	 * The feature id for the '<em><b>Pause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__PAUSE = SimulationPackage.SIMULATION_MANAGER__PAUSE;

	/**
	 * The feature id for the '<em><b>Terminated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__TERMINATED = SimulationPackage.SIMULATION_MANAGER__TERMINATED;

	/**
	 * The feature id for the '<em><b>Step Delay Milliseconds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS = SimulationPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS;

	/**
	 * The feature id for the '<em><b>Current System Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT = SimulationPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT;

	/**
	 * The feature id for the '<em><b>Registered System Simulation Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT = SimulationPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT;

	/**
	 * The feature id for the '<em><b>Current Environment Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT = SimulationPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT;

	/**
	 * The feature id for the '<em><b>Registered Environment Simulation Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT = SimulationPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT;

	/**
	 * The feature id for the '<em><b>Registered Active Simulation Agent Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER = SimulationPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER;

	/**
	 * The feature id for the '<em><b>Active Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT = SimulationPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT;

	/**
	 * The feature id for the '<em><b>Registered Step Performed Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER = SimulationPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__MSD_RUNTIME_STATE_GRAPH = SimulationPackage.SIMULATION_MANAGER__MSD_RUNTIME_STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Current MSD Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__CURRENT_MSD_RUNTIME_STATE = SimulationPackage.SIMULATION_MANAGER__CURRENT_MSD_RUNTIME_STATE;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION = SimulationPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Current Execution Semantics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER__CURRENT_EXECUTION_SEMANTICS = SimulationPackage.SIMULATION_MANAGER__CURRENT_EXECUTION_SEMANTICS;

	/**
	 * The number of structural features of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SIMULATION_MANAGER_FEATURE_COUNT = SimulationPackage.SIMULATION_MANAGER_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedsimulation.TimedSimulationManager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manager</em>'.
	 * @see org.scenariotools.msd.timed.timedsimulation.TimedSimulationManager
	 * @generated
	 */
	EClass getTimedSimulationManager();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TimedSimulationFactory getTimedSimulationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationManagerImpl <em>Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationManagerImpl
		 * @see org.scenariotools.msd.timed.timedsimulation.impl.TimedSimulationPackageImpl#getTimedSimulationManager()
		 * @generated
		 */
		EClass TIMED_SIMULATION_MANAGER = eINSTANCE.getTimedSimulationManager();

	}

} //TimedSimulationPackage
