/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.PortDefinition#getContainer <em>Container</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.PortDefinition#getController <em>Controller</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.PortDefinition#getCommunicationPartnerObject <em>Communication Partner Object</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.PortDefinition#getComponentObject <em>Component Object</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.PortDefinition#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition()
 * @model
 * @generated
 */
public interface PortDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.controllersystem.SynthesisConfiguration#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(SynthesisConfiguration)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition_Container()
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#getPorts
	 * @model opposite="ports" transient="false"
	 * @generated
	 */
	SynthesisConfiguration getContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.PortDefinition#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(SynthesisConfiguration value);

	/**
	 * Returns the value of the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' reference.
	 * @see #setController(ControllerDefinition)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition_Controller()
	 * @model
	 * @generated
	 */
	ControllerDefinition getController();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.PortDefinition#getController <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(ControllerDefinition value);

	/**
	 * Returns the value of the '<em><b>Communication Partner Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Partner Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Partner Object</em>' reference.
	 * @see #setCommunicationPartnerObject(EObject)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition_CommunicationPartnerObject()
	 * @model
	 * @generated
	 */
	EObject getCommunicationPartnerObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.PortDefinition#getCommunicationPartnerObject <em>Communication Partner Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Communication Partner Object</em>' reference.
	 * @see #getCommunicationPartnerObject()
	 * @generated
	 */
	void setCommunicationPartnerObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Component Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Object</em>' reference.
	 * @see #setComponentObject(EObject)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition_ComponentObject()
	 * @model
	 * @generated
	 */
	EObject getComponentObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.PortDefinition#getComponentObject <em>Component Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Object</em>' reference.
	 * @see #getComponentObject()
	 * @generated
	 */
	void setComponentObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getPortDefinition_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.PortDefinition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // PortDefinition
