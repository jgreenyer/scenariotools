/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.controllersystem.ControllersystemPackage
 * @generated
 */
public interface ControllersystemFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ControllersystemFactory eINSTANCE = org.scenariotools.controllersystem.impl.ControllersystemFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Controller Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controller Definition</em>'.
	 * @generated
	 */
	ControllerDefinition createControllerDefinition();

	/**
	 * Returns a new object of class '<em>Controller System Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controller System Definition</em>'.
	 * @generated
	 */
	ControllerSystemDefinition createControllerSystemDefinition();

	/**
	 * Returns a new object of class '<em>Synthesis Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synthesis Configuration</em>'.
	 * @generated
	 */
	SynthesisConfiguration createSynthesisConfiguration();

	/**
	 * Returns a new object of class '<em>Controlled Objects Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controlled Objects Group</em>'.
	 * @generated
	 */
	ControlledObjectsGroup createControlledObjectsGroup();

	/**
	 * Returns a new object of class '<em>Port Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Definition</em>'.
	 * @generated
	 */
	PortDefinition createPortDefinition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ControllersystemPackage getControllersystemPackage();

} //ControllersystemFactory
