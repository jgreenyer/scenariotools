/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.runtime.RuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup <em>Controlled Objects Group</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.ControllerDefinition#getGraph <em>Graph</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerDefinition()
 * @model
 * @generated
 */
public interface ControllerDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Controlled Objects Group</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Objects Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Objects Group</em>' reference.
	 * @see #setControlledObjectsGroup(ControlledObjectsGroup)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerDefinition_ControlledObjectsGroup()
	 * @see org.scenariotools.controllersystem.ControlledObjectsGroup#getController
	 * @model opposite="controller"
	 * @generated
	 */
	ControlledObjectsGroup getControlledObjectsGroup();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup <em>Controlled Objects Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Objects Group</em>' reference.
	 * @see #getControlledObjectsGroup()
	 * @generated
	 */
	void setControlledObjectsGroup(ControlledObjectsGroup value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' reference.
	 * @see #setGraph(RuntimeStateGraph)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerDefinition_Graph()
	 * @model
	 * @generated
	 */
	RuntimeStateGraph getGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.ControllerDefinition#getGraph <em>Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(RuntimeStateGraph value);

} // ControllerDefinition
