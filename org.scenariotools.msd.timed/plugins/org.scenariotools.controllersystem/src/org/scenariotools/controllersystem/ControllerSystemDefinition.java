/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller System Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getHas <em>Has</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getControllers <em>Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getConfiguration <em>Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerSystemDefinition()
 * @model
 * @generated
 */
public interface ControllerSystemDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Has</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.controllersystem.ControllerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' containment reference list.
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerSystemDefinition_Has()
	 * @model containment="true"
	 * @generated
	 */
	EList<ControllerDefinition> getHas();

	/**
	 * Returns the value of the '<em><b>Controllers</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.controllersystem.ControllerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controllers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controllers</em>' containment reference list.
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerSystemDefinition_Controllers()
	 * @model containment="true"
	 * @generated
	 */
	EList<ControllerDefinition> getControllers();

	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' containment reference.
	 * @see #setConfiguration(SynthesisConfiguration)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControllerSystemDefinition_Configuration()
	 * @model containment="true"
	 * @generated
	 */
	SynthesisConfiguration getConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getConfiguration <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' containment reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(SynthesisConfiguration value);

} // ControllerSystemDefinition
