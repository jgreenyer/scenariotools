/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controlled Objects Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getObjects <em>Objects</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getController <em>Controller</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControlledObjectsGroup()
 * @model
 * @generated
 */
public interface ControlledObjectsGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects</em>' reference list.
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControlledObjectsGroup_Objects()
	 * @model
	 * @generated
	 */
	EList<EObject> getObjects();

	/**
	 * Returns the value of the '<em><b>Controller</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup <em>Controlled Objects Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' reference.
	 * @see #setController(ControllerDefinition)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getControlledObjectsGroup_Controller()
	 * @see org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup
	 * @model opposite="controlledObjectsGroup"
	 * @generated
	 */
	ControllerDefinition getController();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getController <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(ControllerDefinition value);

} // ControlledObjectsGroup
