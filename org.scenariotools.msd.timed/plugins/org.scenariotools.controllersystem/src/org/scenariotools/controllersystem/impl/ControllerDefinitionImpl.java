/**
 */
package org.scenariotools.controllersystem.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.scenariotools.controllersystem.ControlledObjectsGroup;
import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.runtime.RuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControllerDefinitionImpl#getControlledObjectsGroup <em>Controlled Objects Group</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControllerDefinitionImpl#getGraph <em>Graph</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControllerDefinitionImpl extends EObjectImpl implements ControllerDefinition {
	/**
	 * The cached value of the '{@link #getControlledObjectsGroup() <em>Controlled Objects Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledObjectsGroup()
	 * @generated
	 * @ordered
	 */
	protected ControlledObjectsGroup controlledObjectsGroup;

	/**
	 * The cached value of the '{@link #getGraph() <em>Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraph()
	 * @generated
	 * @ordered
	 */
	protected RuntimeStateGraph graph;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControllerDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ControllersystemPackage.Literals.CONTROLLER_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledObjectsGroup getControlledObjectsGroup() {
		if (controlledObjectsGroup != null && controlledObjectsGroup.eIsProxy()) {
			InternalEObject oldControlledObjectsGroup = (InternalEObject)controlledObjectsGroup;
			controlledObjectsGroup = (ControlledObjectsGroup)eResolveProxy(oldControlledObjectsGroup);
			if (controlledObjectsGroup != oldControlledObjectsGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, oldControlledObjectsGroup, controlledObjectsGroup));
			}
		}
		return controlledObjectsGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledObjectsGroup basicGetControlledObjectsGroup() {
		return controlledObjectsGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledObjectsGroup(ControlledObjectsGroup newControlledObjectsGroup, NotificationChain msgs) {
		ControlledObjectsGroup oldControlledObjectsGroup = controlledObjectsGroup;
		controlledObjectsGroup = newControlledObjectsGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, oldControlledObjectsGroup, newControlledObjectsGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledObjectsGroup(ControlledObjectsGroup newControlledObjectsGroup) {
		if (newControlledObjectsGroup != controlledObjectsGroup) {
			NotificationChain msgs = null;
			if (controlledObjectsGroup != null)
				msgs = ((InternalEObject)controlledObjectsGroup).eInverseRemove(this, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, ControlledObjectsGroup.class, msgs);
			if (newControlledObjectsGroup != null)
				msgs = ((InternalEObject)newControlledObjectsGroup).eInverseAdd(this, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, ControlledObjectsGroup.class, msgs);
			msgs = basicSetControlledObjectsGroup(newControlledObjectsGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, newControlledObjectsGroup, newControlledObjectsGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeStateGraph getGraph() {
		if (graph != null && graph.eIsProxy()) {
			InternalEObject oldGraph = (InternalEObject)graph;
			graph = (RuntimeStateGraph)eResolveProxy(oldGraph);
			if (graph != oldGraph) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH, oldGraph, graph));
			}
		}
		return graph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeStateGraph basicGetGraph() {
		return graph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraph(RuntimeStateGraph newGraph) {
		RuntimeStateGraph oldGraph = graph;
		graph = newGraph;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH, oldGraph, graph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				if (controlledObjectsGroup != null)
					msgs = ((InternalEObject)controlledObjectsGroup).eInverseRemove(this, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, ControlledObjectsGroup.class, msgs);
				return basicSetControlledObjectsGroup((ControlledObjectsGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				return basicSetControlledObjectsGroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				if (resolve) return getControlledObjectsGroup();
				return basicGetControlledObjectsGroup();
			case ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH:
				if (resolve) return getGraph();
				return basicGetGraph();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				setControlledObjectsGroup((ControlledObjectsGroup)newValue);
				return;
			case ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH:
				setGraph((RuntimeStateGraph)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				setControlledObjectsGroup((ControlledObjectsGroup)null);
				return;
			case ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH:
				setGraph((RuntimeStateGraph)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP:
				return controlledObjectsGroup != null;
			case ControllersystemPackage.CONTROLLER_DEFINITION__GRAPH:
				return graph != null;
		}
		return super.eIsSet(featureID);
	}

} //ControllerDefinitionImpl
