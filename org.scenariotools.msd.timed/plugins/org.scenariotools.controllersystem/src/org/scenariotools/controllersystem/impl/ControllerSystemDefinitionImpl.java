/**
 */
package org.scenariotools.controllersystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.controllersystem.ControllerSystemDefinition;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.controllersystem.SynthesisConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller System Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl#getHas <em>Has</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl#getControllers <em>Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl#getConfiguration <em>Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControllerSystemDefinitionImpl extends EObjectImpl implements ControllerSystemDefinition {
	/**
	 * The cached value of the '{@link #getHas() <em>Has</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHas()
	 * @generated
	 * @ordered
	 */
	protected EList<ControllerDefinition> has;

	/**
	 * The cached value of the '{@link #getControllers() <em>Controllers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControllers()
	 * @generated
	 * @ordered
	 */
	protected EList<ControllerDefinition> controllers;

	/**
	 * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfiguration()
	 * @generated
	 * @ordered
	 */
	protected SynthesisConfiguration configuration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControllerSystemDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ControllersystemPackage.Literals.CONTROLLER_SYSTEM_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControllerDefinition> getHas() {
		if (has == null) {
			has = new EObjectContainmentEList<ControllerDefinition>(ControllerDefinition.class, this, ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS);
		}
		return has;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControllerDefinition> getControllers() {
		if (controllers == null) {
			controllers = new EObjectContainmentEList<ControllerDefinition>(ControllerDefinition.class, this, ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS);
		}
		return controllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynthesisConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfiguration(SynthesisConfiguration newConfiguration, NotificationChain msgs) {
		SynthesisConfiguration oldConfiguration = configuration;
		configuration = newConfiguration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION, oldConfiguration, newConfiguration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(SynthesisConfiguration newConfiguration) {
		if (newConfiguration != configuration) {
			NotificationChain msgs = null;
			if (configuration != null)
				msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION, null, msgs);
			if (newConfiguration != null)
				msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION, null, msgs);
			msgs = basicSetConfiguration(newConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION, newConfiguration, newConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS:
				return ((InternalEList<?>)getHas()).basicRemove(otherEnd, msgs);
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS:
				return ((InternalEList<?>)getControllers()).basicRemove(otherEnd, msgs);
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION:
				return basicSetConfiguration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS:
				return getHas();
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS:
				return getControllers();
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION:
				return getConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS:
				getHas().clear();
				getHas().addAll((Collection<? extends ControllerDefinition>)newValue);
				return;
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS:
				getControllers().clear();
				getControllers().addAll((Collection<? extends ControllerDefinition>)newValue);
				return;
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION:
				setConfiguration((SynthesisConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS:
				getHas().clear();
				return;
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS:
				getControllers().clear();
				return;
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION:
				setConfiguration((SynthesisConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__HAS:
				return has != null && !has.isEmpty();
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS:
				return controllers != null && !controllers.isEmpty();
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION:
				return configuration != null;
		}
		return super.eIsSet(featureID);
	}

} //ControllerSystemDefinitionImpl
