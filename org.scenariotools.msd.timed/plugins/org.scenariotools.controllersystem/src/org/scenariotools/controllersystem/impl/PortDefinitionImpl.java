/**
 */
package org.scenariotools.controllersystem.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.scenariotools.controllersystem.ControlledObjectsGroup;
import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.controllersystem.PortDefinition;
import org.scenariotools.controllersystem.SynthesisConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl#getContainer <em>Container</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl#getController <em>Controller</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl#getCommunicationPartnerObject <em>Communication Partner Object</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl#getComponentObject <em>Component Object</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortDefinitionImpl extends EObjectImpl implements PortDefinition {
	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected ControllerDefinition controller;

	/**
	 * The cached value of the '{@link #getCommunicationPartnerObject() <em>Communication Partner Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationPartnerObject()
	 * @generated
	 * @ordered
	 */
	protected EObject communicationPartnerObject;

	/**
	 * The cached value of the '{@link #getComponentObject() <em>Component Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentObject()
	 * @generated
	 * @ordered
	 */
	protected EObject componentObject;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ControllersystemPackage.Literals.PORT_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynthesisConfiguration getContainer() {
		if (eContainerFeatureID() != ControllersystemPackage.PORT_DEFINITION__CONTAINER) return null;
		return (SynthesisConfiguration)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(SynthesisConfiguration newContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainer, ControllersystemPackage.PORT_DEFINITION__CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(SynthesisConfiguration newContainer) {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != ControllersystemPackage.PORT_DEFINITION__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS, SynthesisConfiguration.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.PORT_DEFINITION__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerDefinition getController() {
		if (controller != null && controller.eIsProxy()) {
			InternalEObject oldController = (InternalEObject)controller;
			controller = (ControllerDefinition)eResolveProxy(oldController);
			if (controller != oldController) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.PORT_DEFINITION__CONTROLLER, oldController, controller));
			}
		}
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerDefinition basicGetController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(ControllerDefinition newController) {
		ControllerDefinition oldController = controller;
		controller = newController;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.PORT_DEFINITION__CONTROLLER, oldController, controller));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getCommunicationPartnerObject() {
		if (communicationPartnerObject != null && communicationPartnerObject.eIsProxy()) {
			InternalEObject oldCommunicationPartnerObject = (InternalEObject)communicationPartnerObject;
			communicationPartnerObject = eResolveProxy(oldCommunicationPartnerObject);
			if (communicationPartnerObject != oldCommunicationPartnerObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT, oldCommunicationPartnerObject, communicationPartnerObject));
			}
		}
		return communicationPartnerObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetCommunicationPartnerObject() {
		return communicationPartnerObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommunicationPartnerObject(EObject newCommunicationPartnerObject) {
		EObject oldCommunicationPartnerObject = communicationPartnerObject;
		communicationPartnerObject = newCommunicationPartnerObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT, oldCommunicationPartnerObject, communicationPartnerObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getComponentObject() {
		if (componentObject != null && componentObject.eIsProxy()) {
			InternalEObject oldComponentObject = (InternalEObject)componentObject;
			componentObject = eResolveProxy(oldComponentObject);
			if (componentObject != oldComponentObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT, oldComponentObject, componentObject));
			}
		}
		return componentObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetComponentObject() {
		return componentObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentObject(EObject newComponentObject) {
		EObject oldComponentObject = componentObject;
		componentObject = newComponentObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT, oldComponentObject, componentObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.PORT_DEFINITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((SynthesisConfiguration)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				return basicSetContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				return eInternalContainer().eInverseRemove(this, ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS, SynthesisConfiguration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				return getContainer();
			case ControllersystemPackage.PORT_DEFINITION__CONTROLLER:
				if (resolve) return getController();
				return basicGetController();
			case ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT:
				if (resolve) return getCommunicationPartnerObject();
				return basicGetCommunicationPartnerObject();
			case ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT:
				if (resolve) return getComponentObject();
				return basicGetComponentObject();
			case ControllersystemPackage.PORT_DEFINITION__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				setContainer((SynthesisConfiguration)newValue);
				return;
			case ControllersystemPackage.PORT_DEFINITION__CONTROLLER:
				setController((ControllerDefinition)newValue);
				return;
			case ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT:
				setCommunicationPartnerObject((EObject)newValue);
				return;
			case ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT:
				setComponentObject((EObject)newValue);
				return;
			case ControllersystemPackage.PORT_DEFINITION__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				setContainer((SynthesisConfiguration)null);
				return;
			case ControllersystemPackage.PORT_DEFINITION__CONTROLLER:
				setController((ControllerDefinition)null);
				return;
			case ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT:
				setCommunicationPartnerObject((EObject)null);
				return;
			case ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT:
				setComponentObject((EObject)null);
				return;
			case ControllersystemPackage.PORT_DEFINITION__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.PORT_DEFINITION__CONTAINER:
				return getContainer() != null;
			case ControllersystemPackage.PORT_DEFINITION__CONTROLLER:
				return controller != null;
			case ControllersystemPackage.PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT:
				return communicationPartnerObject != null;
			case ControllersystemPackage.PORT_DEFINITION__COMPONENT_OBJECT:
				return componentObject != null;
			case ControllersystemPackage.PORT_DEFINITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PortDefinitionImpl
