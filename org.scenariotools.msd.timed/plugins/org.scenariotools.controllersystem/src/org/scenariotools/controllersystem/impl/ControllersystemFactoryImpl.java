/**
 */
package org.scenariotools.controllersystem.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.controllersystem.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ControllersystemFactoryImpl extends EFactoryImpl implements ControllersystemFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ControllersystemFactory init() {
		try {
			ControllersystemFactory theControllersystemFactory = (ControllersystemFactory)EPackage.Registry.INSTANCE.getEFactory(ControllersystemPackage.eNS_URI);
			if (theControllersystemFactory != null) {
				return theControllersystemFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ControllersystemFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllersystemFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ControllersystemPackage.CONTROLLER_DEFINITION: return createControllerDefinition();
			case ControllersystemPackage.CONTROLLER_SYSTEM_DEFINITION: return createControllerSystemDefinition();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION: return createSynthesisConfiguration();
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP: return createControlledObjectsGroup();
			case ControllersystemPackage.PORT_DEFINITION: return createPortDefinition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerDefinition createControllerDefinition() {
		ControllerDefinitionImpl controllerDefinition = new ControllerDefinitionImpl();
		return controllerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerSystemDefinition createControllerSystemDefinition() {
		ControllerSystemDefinitionImpl controllerSystemDefinition = new ControllerSystemDefinitionImpl();
		return controllerSystemDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynthesisConfiguration createSynthesisConfiguration() {
		SynthesisConfigurationImpl synthesisConfiguration = new SynthesisConfigurationImpl();
		return synthesisConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledObjectsGroup createControlledObjectsGroup() {
		ControlledObjectsGroupImpl controlledObjectsGroup = new ControlledObjectsGroupImpl();
		return controlledObjectsGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDefinition createPortDefinition() {
		PortDefinitionImpl portDefinition = new PortDefinitionImpl();
		return portDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllersystemPackage getControllersystemPackage() {
		return (ControllersystemPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ControllersystemPackage getPackage() {
		return ControllersystemPackage.eINSTANCE;
	}

} //ControllersystemFactoryImpl
