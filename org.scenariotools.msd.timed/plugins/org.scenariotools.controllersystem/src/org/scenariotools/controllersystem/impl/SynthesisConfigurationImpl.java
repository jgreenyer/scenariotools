/**
 */
package org.scenariotools.controllersystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.controllersystem.ControlledObjectsGroup;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.controllersystem.PortDefinition;
import org.scenariotools.controllersystem.SynthesisConfiguration;

import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synthesis Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl#getControlGroups <em>Control Groups</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl#isCreatePortControllers <em>Create Port Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl#isCreateClassLevelControllers <em>Create Class Level Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl#getPorts <em>Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SynthesisConfigurationImpl extends EObjectImpl implements SynthesisConfiguration {
	/**
	 * The cached value of the '{@link #getControlGroups() <em>Control Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlledObjectsGroup> controlGroups;

	/**
	 * The cached value of the '{@link #getScenarioRunConfiguration() <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 * @ordered
	 */
	protected ScenarioRunConfiguration scenarioRunConfiguration;

	/**
	 * The default value of the '{@link #isCreatePortControllers() <em>Create Port Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreatePortControllers()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CREATE_PORT_CONTROLLERS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCreatePortControllers() <em>Create Port Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreatePortControllers()
	 * @generated
	 * @ordered
	 */
	protected boolean createPortControllers = CREATE_PORT_CONTROLLERS_EDEFAULT;

	/**
	 * The default value of the '{@link #isCreateClassLevelControllers() <em>Create Class Level Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreateClassLevelControllers()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CREATE_CLASS_LEVEL_CONTROLLERS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCreateClassLevelControllers() <em>Create Class Level Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreateClassLevelControllers()
	 * @generated
	 * @ordered
	 */
	protected boolean createClassLevelControllers = CREATE_CLASS_LEVEL_CONTROLLERS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<PortDefinition> ports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SynthesisConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ControllersystemPackage.Literals.SYNTHESIS_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledObjectsGroup> getControlGroups() {
		if (controlGroups == null) {
			controlGroups = new EObjectContainmentEList<ControlledObjectsGroup>(ControlledObjectsGroup.class, this, ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS);
		}
		return controlGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfiguration getScenarioRunConfiguration() {
		if (scenarioRunConfiguration != null && scenarioRunConfiguration.eIsProxy()) {
			InternalEObject oldScenarioRunConfiguration = (InternalEObject)scenarioRunConfiguration;
			scenarioRunConfiguration = (ScenarioRunConfiguration)eResolveProxy(oldScenarioRunConfiguration);
			if (scenarioRunConfiguration != oldScenarioRunConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
			}
		}
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfiguration basicGetScenarioRunConfiguration() {
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenarioRunConfiguration(ScenarioRunConfiguration newScenarioRunConfiguration) {
		ScenarioRunConfiguration oldScenarioRunConfiguration = scenarioRunConfiguration;
		scenarioRunConfiguration = newScenarioRunConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCreatePortControllers() {
		return createPortControllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreatePortControllers(boolean newCreatePortControllers) {
		boolean oldCreatePortControllers = createPortControllers;
		createPortControllers = newCreatePortControllers;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS, oldCreatePortControllers, createPortControllers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCreateClassLevelControllers() {
		return createClassLevelControllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreateClassLevelControllers(boolean newCreateClassLevelControllers) {
		boolean oldCreateClassLevelControllers = createClassLevelControllers;
		createClassLevelControllers = newCreateClassLevelControllers;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS, oldCreateClassLevelControllers, createClassLevelControllers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortDefinition> getPorts() {
		if (ports == null) {
			ports = new EObjectContainmentWithInverseEList<PortDefinition>(PortDefinition.class, this, ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS, ControllersystemPackage.PORT_DEFINITION__CONTAINER);
		}
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPorts()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS:
				return ((InternalEList<?>)getControlGroups()).basicRemove(otherEnd, msgs);
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				return ((InternalEList<?>)getPorts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS:
				return getControlGroups();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION:
				if (resolve) return getScenarioRunConfiguration();
				return basicGetScenarioRunConfiguration();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS:
				return isCreatePortControllers();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS:
				return isCreateClassLevelControllers();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				return getPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS:
				getControlGroups().clear();
				getControlGroups().addAll((Collection<? extends ControlledObjectsGroup>)newValue);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((ScenarioRunConfiguration)newValue);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS:
				setCreatePortControllers((Boolean)newValue);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS:
				setCreateClassLevelControllers((Boolean)newValue);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				getPorts().clear();
				getPorts().addAll((Collection<? extends PortDefinition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS:
				getControlGroups().clear();
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((ScenarioRunConfiguration)null);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS:
				setCreatePortControllers(CREATE_PORT_CONTROLLERS_EDEFAULT);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS:
				setCreateClassLevelControllers(CREATE_CLASS_LEVEL_CONTROLLERS_EDEFAULT);
				return;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				getPorts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CONTROL_GROUPS:
				return controlGroups != null && !controlGroups.isEmpty();
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION:
				return scenarioRunConfiguration != null;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS:
				return createPortControllers != CREATE_PORT_CONTROLLERS_EDEFAULT;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS:
				return createClassLevelControllers != CREATE_CLASS_LEVEL_CONTROLLERS_EDEFAULT;
			case ControllersystemPackage.SYNTHESIS_CONFIGURATION__PORTS:
				return ports != null && !ports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (createPortControllers: ");
		result.append(createPortControllers);
		result.append(", createClassLevelControllers: ");
		result.append(createClassLevelControllers);
		result.append(')');
		return result.toString();
	}

} //SynthesisConfigurationImpl
