/**
 */
package org.scenariotools.controllersystem.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.controllersystem.ControlledObjectsGroup;
import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.controllersystem.ControllerSystemDefinition;
import org.scenariotools.controllersystem.ControllersystemFactory;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.controllersystem.PortDefinition;
import org.scenariotools.controllersystem.SynthesisConfiguration;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ControllersystemPackageImpl extends EPackageImpl implements ControllersystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controllerDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controllerSystemDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass synthesisConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlledObjectsGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portDefinitionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ControllersystemPackageImpl() {
		super(eNS_URI, ControllersystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ControllersystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ControllersystemPackage init() {
		if (isInited) return (ControllersystemPackage)EPackage.Registry.INSTANCE.getEPackage(ControllersystemPackage.eNS_URI);

		// Obtain or create and register package
		ControllersystemPackageImpl theControllersystemPackage = (ControllersystemPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ControllersystemPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ControllersystemPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();
		ScenariorunconfigurationPackage.eINSTANCE.eClass();
		UtilPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theControllersystemPackage.createPackageContents();

		// Initialize created meta-data
		theControllersystemPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theControllersystemPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ControllersystemPackage.eNS_URI, theControllersystemPackage);
		return theControllersystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControllerDefinition() {
		return controllerDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerDefinition_ControlledObjectsGroup() {
		return (EReference)controllerDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerDefinition_Graph() {
		return (EReference)controllerDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControllerSystemDefinition() {
		return controllerSystemDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerSystemDefinition_Has() {
		return (EReference)controllerSystemDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerSystemDefinition_Controllers() {
		return (EReference)controllerSystemDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerSystemDefinition_Configuration() {
		return (EReference)controllerSystemDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSynthesisConfiguration() {
		return synthesisConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynthesisConfiguration_ControlGroups() {
		return (EReference)synthesisConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynthesisConfiguration_ScenarioRunConfiguration() {
		return (EReference)synthesisConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSynthesisConfiguration_CreatePortControllers() {
		return (EAttribute)synthesisConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSynthesisConfiguration_CreateClassLevelControllers() {
		return (EAttribute)synthesisConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynthesisConfiguration_Ports() {
		return (EReference)synthesisConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlledObjectsGroup() {
		return controlledObjectsGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlledObjectsGroup_Objects() {
		return (EReference)controlledObjectsGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlledObjectsGroup_Controller() {
		return (EReference)controlledObjectsGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortDefinition() {
		return portDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortDefinition_Container() {
		return (EReference)portDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortDefinition_Controller() {
		return (EReference)portDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortDefinition_CommunicationPartnerObject() {
		return (EReference)portDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortDefinition_ComponentObject() {
		return (EReference)portDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDefinition_Name() {
		return (EAttribute)portDefinitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllersystemFactory getControllersystemFactory() {
		return (ControllersystemFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		controllerDefinitionEClass = createEClass(CONTROLLER_DEFINITION);
		createEReference(controllerDefinitionEClass, CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP);
		createEReference(controllerDefinitionEClass, CONTROLLER_DEFINITION__GRAPH);

		controllerSystemDefinitionEClass = createEClass(CONTROLLER_SYSTEM_DEFINITION);
		createEReference(controllerSystemDefinitionEClass, CONTROLLER_SYSTEM_DEFINITION__HAS);
		createEReference(controllerSystemDefinitionEClass, CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS);
		createEReference(controllerSystemDefinitionEClass, CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION);

		synthesisConfigurationEClass = createEClass(SYNTHESIS_CONFIGURATION);
		createEReference(synthesisConfigurationEClass, SYNTHESIS_CONFIGURATION__CONTROL_GROUPS);
		createEReference(synthesisConfigurationEClass, SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION);
		createEAttribute(synthesisConfigurationEClass, SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS);
		createEAttribute(synthesisConfigurationEClass, SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS);
		createEReference(synthesisConfigurationEClass, SYNTHESIS_CONFIGURATION__PORTS);

		controlledObjectsGroupEClass = createEClass(CONTROLLED_OBJECTS_GROUP);
		createEReference(controlledObjectsGroupEClass, CONTROLLED_OBJECTS_GROUP__OBJECTS);
		createEReference(controlledObjectsGroupEClass, CONTROLLED_OBJECTS_GROUP__CONTROLLER);

		portDefinitionEClass = createEClass(PORT_DEFINITION);
		createEReference(portDefinitionEClass, PORT_DEFINITION__CONTAINER);
		createEReference(portDefinitionEClass, PORT_DEFINITION__CONTROLLER);
		createEReference(portDefinitionEClass, PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT);
		createEReference(portDefinitionEClass, PORT_DEFINITION__COMPONENT_OBJECT);
		createEAttribute(portDefinitionEClass, PORT_DEFINITION__NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		org.scenariotools.runtime.RuntimePackage theRuntimePackage_1 = (org.scenariotools.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.runtime.RuntimePackage.eNS_URI);
		ScenariorunconfigurationPackage theScenariorunconfigurationPackage = (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(controllerDefinitionEClass, ControllerDefinition.class, "ControllerDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControllerDefinition_ControlledObjectsGroup(), this.getControlledObjectsGroup(), this.getControlledObjectsGroup_Controller(), "controlledObjectsGroup", null, 0, 1, ControllerDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControllerDefinition_Graph(), theRuntimePackage_1.getRuntimeStateGraph(), null, "graph", null, 0, 1, ControllerDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controllerSystemDefinitionEClass, ControllerSystemDefinition.class, "ControllerSystemDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControllerSystemDefinition_Has(), this.getControllerDefinition(), null, "has", null, 0, -1, ControllerSystemDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControllerSystemDefinition_Controllers(), this.getControllerDefinition(), null, "controllers", null, 0, -1, ControllerSystemDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControllerSystemDefinition_Configuration(), this.getSynthesisConfiguration(), null, "configuration", null, 0, 1, ControllerSystemDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(synthesisConfigurationEClass, SynthesisConfiguration.class, "SynthesisConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSynthesisConfiguration_ControlGroups(), this.getControlledObjectsGroup(), null, "controlGroups", null, 0, -1, SynthesisConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSynthesisConfiguration_ScenarioRunConfiguration(), theScenariorunconfigurationPackage.getScenarioRunConfiguration(), null, "scenarioRunConfiguration", null, 0, 1, SynthesisConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSynthesisConfiguration_CreatePortControllers(), theEcorePackage.getEBoolean(), "createPortControllers", null, 0, 1, SynthesisConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSynthesisConfiguration_CreateClassLevelControllers(), theEcorePackage.getEBoolean(), "createClassLevelControllers", null, 0, 1, SynthesisConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSynthesisConfiguration_Ports(), this.getPortDefinition(), this.getPortDefinition_Container(), "ports", null, 0, -1, SynthesisConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlledObjectsGroupEClass, ControlledObjectsGroup.class, "ControlledObjectsGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControlledObjectsGroup_Objects(), theEcorePackage.getEObject(), null, "objects", null, 0, -1, ControlledObjectsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlledObjectsGroup_Controller(), this.getControllerDefinition(), this.getControllerDefinition_ControlledObjectsGroup(), "controller", null, 0, 1, ControlledObjectsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portDefinitionEClass, PortDefinition.class, "PortDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortDefinition_Container(), this.getSynthesisConfiguration(), this.getSynthesisConfiguration_Ports(), "container", null, 0, 1, PortDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortDefinition_Controller(), this.getControllerDefinition(), null, "controller", null, 0, 1, PortDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortDefinition_CommunicationPartnerObject(), theEcorePackage.getEObject(), null, "communicationPartnerObject", null, 0, 1, PortDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortDefinition_ComponentObject(), theEcorePackage.getEObject(), null, "componentObject", null, 0, 1, PortDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortDefinition_Name(), theEcorePackage.getEString(), "name", null, 0, 1, PortDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ControllersystemPackageImpl
