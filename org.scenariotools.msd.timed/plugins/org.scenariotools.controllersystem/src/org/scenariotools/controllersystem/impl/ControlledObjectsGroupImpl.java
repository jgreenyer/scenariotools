/**
 */
package org.scenariotools.controllersystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.controllersystem.ControlledObjectsGroup;
import org.scenariotools.controllersystem.ControllerDefinition;
import org.scenariotools.controllersystem.ControllersystemPackage;
import org.scenariotools.controllersystem.PortDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controlled Objects Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl#getObjects <em>Objects</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl#getController <em>Controller</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControlledObjectsGroupImpl extends EObjectImpl implements ControlledObjectsGroup {
	/**
	 * The cached value of the '{@link #getObjects() <em>Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> objects;

	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected ControllerDefinition controller;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlledObjectsGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ControllersystemPackage.Literals.CONTROLLED_OBJECTS_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getObjects() {
		if (objects == null) {
			objects = new EObjectResolvingEList<EObject>(EObject.class, this, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__OBJECTS);
		}
		return objects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerDefinition getController() {
		if (controller != null && controller.eIsProxy()) {
			InternalEObject oldController = (InternalEObject)controller;
			controller = (ControllerDefinition)eResolveProxy(oldController);
			if (controller != oldController) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, oldController, controller));
			}
		}
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerDefinition basicGetController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetController(ControllerDefinition newController, NotificationChain msgs) {
		ControllerDefinition oldController = controller;
		controller = newController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, oldController, newController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(ControllerDefinition newController) {
		if (newController != controller) {
			NotificationChain msgs = null;
			if (controller != null)
				msgs = ((InternalEObject)controller).eInverseRemove(this, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, ControllerDefinition.class, msgs);
			if (newController != null)
				msgs = ((InternalEObject)newController).eInverseAdd(this, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, ControllerDefinition.class, msgs);
			msgs = basicSetController(newController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER, newController, newController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				if (controller != null)
					msgs = ((InternalEObject)controller).eInverseRemove(this, ControllersystemPackage.CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP, ControllerDefinition.class, msgs);
				return basicSetController((ControllerDefinition)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				return basicSetController(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__OBJECTS:
				return getObjects();
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				if (resolve) return getController();
				return basicGetController();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__OBJECTS:
				getObjects().clear();
				getObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				setController((ControllerDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__OBJECTS:
				getObjects().clear();
				return;
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				setController((ControllerDefinition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__OBJECTS:
				return objects != null && !objects.isEmpty();
			case ControllersystemPackage.CONTROLLED_OBJECTS_GROUP__CONTROLLER:
				return controller != null;
		}
		return super.eIsSet(featureID);
	}

} //ControlledObjectsGroupImpl
