/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.controllersystem.ControllersystemFactory
 * @model kind="package"
 * @generated
 */
public interface ControllersystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "controllersystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.controllersystem/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "controllersystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ControllersystemPackage eINSTANCE = org.scenariotools.controllersystem.impl.ControllersystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.controllersystem.impl.ControllerDefinitionImpl <em>Controller Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.controllersystem.impl.ControllerDefinitionImpl
	 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControllerDefinition()
	 * @generated
	 */
	int CONTROLLER_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Controlled Objects Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP = 0;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_DEFINITION__GRAPH = 1;

	/**
	 * The number of structural features of the '<em>Controller Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_DEFINITION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl <em>Controller System Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl
	 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControllerSystemDefinition()
	 * @generated
	 */
	int CONTROLLER_SYSTEM_DEFINITION = 1;

	/**
	 * The feature id for the '<em><b>Has</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_SYSTEM_DEFINITION__HAS = 0;

	/**
	 * The feature id for the '<em><b>Controllers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS = 1;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION = 2;

	/**
	 * The number of structural features of the '<em>Controller System Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_SYSTEM_DEFINITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl <em>Synthesis Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl
	 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getSynthesisConfiguration()
	 * @generated
	 */
	int SYNTHESIS_CONFIGURATION = 2;

	/**
	 * The feature id for the '<em><b>Control Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION__CONTROL_GROUPS = 0;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION = 1;

	/**
	 * The feature id for the '<em><b>Create Port Controllers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS = 2;

	/**
	 * The feature id for the '<em><b>Create Class Level Controllers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS = 3;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION__PORTS = 4;

	/**
	 * The number of structural features of the '<em>Synthesis Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHESIS_CONFIGURATION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl <em>Controlled Objects Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl
	 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControlledObjectsGroup()
	 * @generated
	 */
	int CONTROLLED_OBJECTS_GROUP = 3;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLED_OBJECTS_GROUP__OBJECTS = 0;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLED_OBJECTS_GROUP__CONTROLLER = 1;

	/**
	 * The number of structural features of the '<em>Controlled Objects Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLED_OBJECTS_GROUP_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl <em>Port Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.controllersystem.impl.PortDefinitionImpl
	 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getPortDefinition()
	 * @generated
	 */
	int PORT_DEFINITION = 4;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION__CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION__CONTROLLER = 1;

	/**
	 * The feature id for the '<em><b>Communication Partner Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT = 2;

	/**
	 * The feature id for the '<em><b>Component Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION__COMPONENT_OBJECT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION__NAME = 4;

	/**
	 * The number of structural features of the '<em>Port Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DEFINITION_FEATURE_COUNT = 5;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.controllersystem.ControllerDefinition <em>Controller Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller Definition</em>'.
	 * @see org.scenariotools.controllersystem.ControllerDefinition
	 * @generated
	 */
	EClass getControllerDefinition();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup <em>Controlled Objects Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controlled Objects Group</em>'.
	 * @see org.scenariotools.controllersystem.ControllerDefinition#getControlledObjectsGroup()
	 * @see #getControllerDefinition()
	 * @generated
	 */
	EReference getControllerDefinition_ControlledObjectsGroup();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.ControllerDefinition#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph</em>'.
	 * @see org.scenariotools.controllersystem.ControllerDefinition#getGraph()
	 * @see #getControllerDefinition()
	 * @generated
	 */
	EReference getControllerDefinition_Graph();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.controllersystem.ControllerSystemDefinition <em>Controller System Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller System Definition</em>'.
	 * @see org.scenariotools.controllersystem.ControllerSystemDefinition
	 * @generated
	 */
	EClass getControllerSystemDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has</em>'.
	 * @see org.scenariotools.controllersystem.ControllerSystemDefinition#getHas()
	 * @see #getControllerSystemDefinition()
	 * @generated
	 */
	EReference getControllerSystemDefinition_Has();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getControllers <em>Controllers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Controllers</em>'.
	 * @see org.scenariotools.controllersystem.ControllerSystemDefinition#getControllers()
	 * @see #getControllerSystemDefinition()
	 * @generated
	 */
	EReference getControllerSystemDefinition_Controllers();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.controllersystem.ControllerSystemDefinition#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see org.scenariotools.controllersystem.ControllerSystemDefinition#getConfiguration()
	 * @see #getControllerSystemDefinition()
	 * @generated
	 */
	EReference getControllerSystemDefinition_Configuration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.controllersystem.SynthesisConfiguration <em>Synthesis Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synthesis Configuration</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration
	 * @generated
	 */
	EClass getSynthesisConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.controllersystem.SynthesisConfiguration#getControlGroups <em>Control Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control Groups</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#getControlGroups()
	 * @see #getSynthesisConfiguration()
	 * @generated
	 */
	EReference getSynthesisConfiguration_ControlGroups();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.SynthesisConfiguration#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario Run Configuration</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#getScenarioRunConfiguration()
	 * @see #getSynthesisConfiguration()
	 * @generated
	 */
	EReference getSynthesisConfiguration_ScenarioRunConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreatePortControllers <em>Create Port Controllers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Create Port Controllers</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#isCreatePortControllers()
	 * @see #getSynthesisConfiguration()
	 * @generated
	 */
	EAttribute getSynthesisConfiguration_CreatePortControllers();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreateClassLevelControllers <em>Create Class Level Controllers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Create Class Level Controllers</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#isCreateClassLevelControllers()
	 * @see #getSynthesisConfiguration()
	 * @generated
	 */
	EAttribute getSynthesisConfiguration_CreateClassLevelControllers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.controllersystem.SynthesisConfiguration#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ports</em>'.
	 * @see org.scenariotools.controllersystem.SynthesisConfiguration#getPorts()
	 * @see #getSynthesisConfiguration()
	 * @generated
	 */
	EReference getSynthesisConfiguration_Ports();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.controllersystem.ControlledObjectsGroup <em>Controlled Objects Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controlled Objects Group</em>'.
	 * @see org.scenariotools.controllersystem.ControlledObjectsGroup
	 * @generated
	 */
	EClass getControlledObjectsGroup();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Objects</em>'.
	 * @see org.scenariotools.controllersystem.ControlledObjectsGroup#getObjects()
	 * @see #getControlledObjectsGroup()
	 * @generated
	 */
	EReference getControlledObjectsGroup_Objects();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.ControlledObjectsGroup#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controller</em>'.
	 * @see org.scenariotools.controllersystem.ControlledObjectsGroup#getController()
	 * @see #getControlledObjectsGroup()
	 * @generated
	 */
	EReference getControlledObjectsGroup_Controller();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.controllersystem.PortDefinition <em>Port Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Definition</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition
	 * @generated
	 */
	EClass getPortDefinition();

	/**
	 * Returns the meta object for the container reference '{@link org.scenariotools.controllersystem.PortDefinition#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition#getContainer()
	 * @see #getPortDefinition()
	 * @generated
	 */
	EReference getPortDefinition_Container();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.PortDefinition#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controller</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition#getController()
	 * @see #getPortDefinition()
	 * @generated
	 */
	EReference getPortDefinition_Controller();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.PortDefinition#getCommunicationPartnerObject <em>Communication Partner Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Communication Partner Object</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition#getCommunicationPartnerObject()
	 * @see #getPortDefinition()
	 * @generated
	 */
	EReference getPortDefinition_CommunicationPartnerObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.controllersystem.PortDefinition#getComponentObject <em>Component Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Object</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition#getComponentObject()
	 * @see #getPortDefinition()
	 * @generated
	 */
	EReference getPortDefinition_ComponentObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.controllersystem.PortDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.controllersystem.PortDefinition#getName()
	 * @see #getPortDefinition()
	 * @generated
	 */
	EAttribute getPortDefinition_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ControllersystemFactory getControllersystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.controllersystem.impl.ControllerDefinitionImpl <em>Controller Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.controllersystem.impl.ControllerDefinitionImpl
		 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControllerDefinition()
		 * @generated
		 */
		EClass CONTROLLER_DEFINITION = eINSTANCE.getControllerDefinition();

		/**
		 * The meta object literal for the '<em><b>Controlled Objects Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_DEFINITION__CONTROLLED_OBJECTS_GROUP = eINSTANCE.getControllerDefinition_ControlledObjectsGroup();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_DEFINITION__GRAPH = eINSTANCE.getControllerDefinition_Graph();

		/**
		 * The meta object literal for the '{@link org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl <em>Controller System Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.controllersystem.impl.ControllerSystemDefinitionImpl
		 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControllerSystemDefinition()
		 * @generated
		 */
		EClass CONTROLLER_SYSTEM_DEFINITION = eINSTANCE.getControllerSystemDefinition();

		/**
		 * The meta object literal for the '<em><b>Has</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_SYSTEM_DEFINITION__HAS = eINSTANCE.getControllerSystemDefinition_Has();

		/**
		 * The meta object literal for the '<em><b>Controllers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_SYSTEM_DEFINITION__CONTROLLERS = eINSTANCE.getControllerSystemDefinition_Controllers();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_SYSTEM_DEFINITION__CONFIGURATION = eINSTANCE.getControllerSystemDefinition_Configuration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl <em>Synthesis Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.controllersystem.impl.SynthesisConfigurationImpl
		 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getSynthesisConfiguration()
		 * @generated
		 */
		EClass SYNTHESIS_CONFIGURATION = eINSTANCE.getSynthesisConfiguration();

		/**
		 * The meta object literal for the '<em><b>Control Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNTHESIS_CONFIGURATION__CONTROL_GROUPS = eINSTANCE.getSynthesisConfiguration_ControlGroups();

		/**
		 * The meta object literal for the '<em><b>Scenario Run Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNTHESIS_CONFIGURATION__SCENARIO_RUN_CONFIGURATION = eINSTANCE.getSynthesisConfiguration_ScenarioRunConfiguration();

		/**
		 * The meta object literal for the '<em><b>Create Port Controllers</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNTHESIS_CONFIGURATION__CREATE_PORT_CONTROLLERS = eINSTANCE.getSynthesisConfiguration_CreatePortControllers();

		/**
		 * The meta object literal for the '<em><b>Create Class Level Controllers</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNTHESIS_CONFIGURATION__CREATE_CLASS_LEVEL_CONTROLLERS = eINSTANCE.getSynthesisConfiguration_CreateClassLevelControllers();

		/**
		 * The meta object literal for the '<em><b>Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNTHESIS_CONFIGURATION__PORTS = eINSTANCE.getSynthesisConfiguration_Ports();

		/**
		 * The meta object literal for the '{@link org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl <em>Controlled Objects Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.controllersystem.impl.ControlledObjectsGroupImpl
		 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getControlledObjectsGroup()
		 * @generated
		 */
		EClass CONTROLLED_OBJECTS_GROUP = eINSTANCE.getControlledObjectsGroup();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLED_OBJECTS_GROUP__OBJECTS = eINSTANCE.getControlledObjectsGroup_Objects();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLED_OBJECTS_GROUP__CONTROLLER = eINSTANCE.getControlledObjectsGroup_Controller();

		/**
		 * The meta object literal for the '{@link org.scenariotools.controllersystem.impl.PortDefinitionImpl <em>Port Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.controllersystem.impl.PortDefinitionImpl
		 * @see org.scenariotools.controllersystem.impl.ControllersystemPackageImpl#getPortDefinition()
		 * @generated
		 */
		EClass PORT_DEFINITION = eINSTANCE.getPortDefinition();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DEFINITION__CONTAINER = eINSTANCE.getPortDefinition_Container();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DEFINITION__CONTROLLER = eINSTANCE.getPortDefinition_Controller();

		/**
		 * The meta object literal for the '<em><b>Communication Partner Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DEFINITION__COMMUNICATION_PARTNER_OBJECT = eINSTANCE.getPortDefinition_CommunicationPartnerObject();

		/**
		 * The meta object literal for the '<em><b>Component Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DEFINITION__COMPONENT_OBJECT = eINSTANCE.getPortDefinition_ComponentObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DEFINITION__NAME = eINSTANCE.getPortDefinition_Name();

	}

} //ControllersystemPackage
