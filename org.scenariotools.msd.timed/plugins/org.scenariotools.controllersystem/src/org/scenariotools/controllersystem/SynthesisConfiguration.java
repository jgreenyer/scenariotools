/**
 */
package org.scenariotools.controllersystem;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synthesis Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.controllersystem.SynthesisConfiguration#getControlGroups <em>Control Groups</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.SynthesisConfiguration#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreatePortControllers <em>Create Port Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreateClassLevelControllers <em>Create Class Level Controllers</em>}</li>
 *   <li>{@link org.scenariotools.controllersystem.SynthesisConfiguration#getPorts <em>Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration()
 * @model
 * @generated
 */
public interface SynthesisConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Control Groups</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.controllersystem.ControlledObjectsGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Control Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control Groups</em>' containment reference list.
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration_ControlGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<ControlledObjectsGroup> getControlGroups();

	/**
	 * Returns the value of the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario Run Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #setScenarioRunConfiguration(ScenarioRunConfiguration)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration_ScenarioRunConfiguration()
	 * @model
	 * @generated
	 */
	ScenarioRunConfiguration getScenarioRunConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.SynthesisConfiguration#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	void setScenarioRunConfiguration(ScenarioRunConfiguration value);

	/**
	 * Returns the value of the '<em><b>Create Port Controllers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Port Controllers</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Port Controllers</em>' attribute.
	 * @see #setCreatePortControllers(boolean)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration_CreatePortControllers()
	 * @model
	 * @generated
	 */
	boolean isCreatePortControllers();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreatePortControllers <em>Create Port Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Create Port Controllers</em>' attribute.
	 * @see #isCreatePortControllers()
	 * @generated
	 */
	void setCreatePortControllers(boolean value);

	/**
	 * Returns the value of the '<em><b>Create Class Level Controllers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Class Level Controllers</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Class Level Controllers</em>' attribute.
	 * @see #setCreateClassLevelControllers(boolean)
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration_CreateClassLevelControllers()
	 * @model
	 * @generated
	 */
	boolean isCreateClassLevelControllers();

	/**
	 * Sets the value of the '{@link org.scenariotools.controllersystem.SynthesisConfiguration#isCreateClassLevelControllers <em>Create Class Level Controllers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Create Class Level Controllers</em>' attribute.
	 * @see #isCreateClassLevelControllers()
	 * @generated
	 */
	void setCreateClassLevelControllers(boolean value);

	/**
	 * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.controllersystem.PortDefinition}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.controllersystem.PortDefinition#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' containment reference list.
	 * @see org.scenariotools.controllersystem.ControllersystemPackage#getSynthesisConfiguration_Ports()
	 * @see org.scenariotools.controllersystem.PortDefinition#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<PortDefinition> getPorts();

} // SynthesisConfiguration
