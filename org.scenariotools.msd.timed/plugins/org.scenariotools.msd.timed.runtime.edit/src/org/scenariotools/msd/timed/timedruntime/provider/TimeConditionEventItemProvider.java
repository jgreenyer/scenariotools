/**
 */
package org.scenariotools.msd.timed.timedruntime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.events.provider.EventItemProvider;

import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TimeConditionEventItemProvider
	extends EventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeConditionEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClockNamePropertyDescriptor(object);
			addOperatorStringPropertyDescriptor(object);
			addCompareValuePropertyDescriptor(object);
			addClockConditionPropertyDescriptor(object);
			addActiveMSDCutPropertyDescriptor(object);
			addConstraintFulfilledPropertyDescriptor(object);
			addHotPropertyDescriptor(object);
			addClockObserversPropertyDescriptor(object);
			addTimedControllablePropertyDescriptor(object);
			addTimedControllableSetManuallyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Clock Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClockNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_clockName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_clockName_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__CLOCK_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Operator String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperatorStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_operatorString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_operatorString_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__OPERATOR_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Compare Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCompareValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_compareValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_compareValue_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__COMPARE_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Clock Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClockConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_clockCondition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_clockCondition_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__CLOCK_CONDITION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Active MSD Cut feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addActiveMSDCutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_activeMSDCut_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_activeMSDCut_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Constraint Fulfilled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstraintFulfilledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_constraintFulfilled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_constraintFulfilled_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_hot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_hot_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__HOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Clock Observers feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClockObserversPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_clockObservers_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_clockObservers_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__CLOCK_OBSERVERS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timed Controllable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimedControllablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_timedControllable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_timedControllable_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timed Controllable Set Manually feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimedControllableSetManuallyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeConditionEvent_timedControllableSetManually_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeConditionEvent_timedControllableSetManually_feature", "_UI_TimeConditionEvent_type"),
				 TimedRuntimePackage.Literals.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns TimeConditionEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TimeConditionEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TimeConditionEvent)object).getClockName();
		return label == null || label.length() == 0 ?
			getString("_UI_TimeConditionEvent_type") :
			getString("_UI_TimeConditionEvent_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TimeConditionEvent.class)) {
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__HOT:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE:
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimedruntimeEditPlugin.INSTANCE;
	}

}
