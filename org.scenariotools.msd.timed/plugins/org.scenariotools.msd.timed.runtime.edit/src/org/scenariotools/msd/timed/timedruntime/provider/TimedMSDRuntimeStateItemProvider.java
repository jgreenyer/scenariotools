/**
 */
package org.scenariotools.msd.timed.timedruntime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.runtime.provider.MSDRuntimeStateItemProvider;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedMSDRuntimeStateItemProvider
	extends MSDRuntimeStateItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedMSDRuntimeStateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFederationStringPropertyDescriptor(object);
			addBufferStringPropertyDescriptor(object);
			addTimingConditionsInRequirementsSatisfiablePropertyDescriptor(object);
			addTimingConditionsInAssumptionsSatisfiablePropertyDescriptor(object);
			addTimedSafetyViolationOccurredInRequirementsPropertyDescriptor(object);
			addTimedSafetyViolationOccurredInAssumptionsPropertyDescriptor(object);
			addGoodFederationStringPropertyDescriptor(object);
			addBadFederationStringPropertyDescriptor(object);
			addWinningFederationStringPropertyDescriptor(object);
			addTimeInconsistencyOccurredInRequirementsPropertyDescriptor(object);
			addTimeInconsistencyOccurredInAssumptionsPropertyDescriptor(object);
			addUpperBoundRealTimeInconsistencyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Federation String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFederationStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_federationString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_federationString_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Buffer String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBufferStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_bufferString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_bufferString_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timing Conditions In Requirements Satisfiable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimingConditionsInRequirementsSatisfiablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timingConditionsInRequirementsSatisfiable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timingConditionsInRequirementsSatisfiable_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timing Conditions In Assumptions Satisfiable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimingConditionsInAssumptionsSatisfiablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timingConditionsInAssumptionsSatisfiable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timingConditionsInAssumptionsSatisfiable_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timed Safety Violation Occurred In Requirements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimedSafetyViolationOccurredInRequirementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timedSafetyViolationOccurredInRequirements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timedSafetyViolationOccurredInRequirements_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Timed Safety Violation Occurred In Assumptions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimedSafetyViolationOccurredInAssumptionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timedSafetyViolationOccurredInAssumptions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timedSafetyViolationOccurredInAssumptions_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Good Federation String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGoodFederationStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_goodFederationString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_goodFederationString_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bad Federation String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBadFederationStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_badFederationString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_badFederationString_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Winning Federation String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWinningFederationStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_winningFederationString_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_winningFederationString_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Time Inconsistency Occurred In Requirements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimeInconsistencyOccurredInRequirementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timeInconsistencyOccurredInRequirements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timeInconsistencyOccurredInRequirements_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Time Inconsistency Occurred In Assumptions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimeInconsistencyOccurredInAssumptionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_timeInconsistencyOccurredInAssumptions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_timeInconsistencyOccurredInAssumptions_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Upper Bound Real Time Inconsistency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpperBoundRealTimeInconsistencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimedMSDRuntimeState_upperBoundRealTimeInconsistency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimedMSDRuntimeState_upperBoundRealTimeInconsistency_feature", "_UI_TimedMSDRuntimeState_type"),
				 TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns TimedMSDRuntimeState.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TimedMSDRuntimeState"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TimedMSDRuntimeState)object).getLabel();
		return label == null || label.length() == 0 ?
			getString("_UI_TimedMSDRuntimeState_type") :
			getString("_UI_TimedMSDRuntimeState_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TimedMSDRuntimeState.class)) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS:
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimedruntimeEditPlugin.INSTANCE;
	}

}
