/**
 */
package org.scenariotools.msd.timed.timedruntime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.provider.MSDRuntimeStateGraphItemProvider;

import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;

import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.stategraph.StategraphPackage;
import org.scenariotools.stategraph.StrategyKind;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedMSDRuntimeStateGraphItemProvider
	extends MSDRuntimeStateGraphItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedMSDRuntimeStateGraphItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TimedMSDRuntimeStateGraph.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TimedMSDRuntimeStateGraph"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		StrategyKind labelValue = ((TimedMSDRuntimeStateGraph)object).getStrategyKind();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_TimedMSDRuntimeStateGraph_type") :
			getString("_UI_TimedMSDRuntimeStateGraph_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TimedMSDRuntimeStateGraph.class)) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(StategraphPackage.Literals.STATE_GRAPH__STATES,
				 TimedRuntimeFactory.eINSTANCE.createTimedMSDRuntimeState()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL,
				 TimedRuntimeFactory.eINSTANCE.createTimedRuntimeUtil()));

		newChildDescriptors.add
			(createChildParameter
				(TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS,
				 TimedRuntimeFactory.eINSTANCE.createTimeConditionEvent()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimedruntimeEditPlugin.INSTANCE;
	}

}
