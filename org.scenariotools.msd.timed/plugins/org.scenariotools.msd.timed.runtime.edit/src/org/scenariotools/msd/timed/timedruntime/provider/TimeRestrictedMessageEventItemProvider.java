/**
 */
package org.scenariotools.msd.timed.timedruntime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.events.provider.MessageEventItemProvider;

import org.scenariotools.events.provider.SynchronousMessageEventItemProvider;
import org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TimeRestrictedMessageEventItemProvider
	extends SynchronousMessageEventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeRestrictedMessageEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClockNamePropertyDescriptor(object);
			addMinBoundPropertyDescriptor(object);
			addMaxBoundPropertyDescriptor(object);
			addExcludeMinBoundPropertyDescriptor(object);
			addExcludeMaxBoundPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Clock Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClockNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeRestrictedMessageEvent_clockName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeRestrictedMessageEvent_clockName_feature", "_UI_TimeRestrictedMessageEvent_type"),
				 TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Min Bound feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinBoundPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeRestrictedMessageEvent_minBound_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeRestrictedMessageEvent_minBound_feature", "_UI_TimeRestrictedMessageEvent_type"),
				 TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Bound feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxBoundPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeRestrictedMessageEvent_maxBound_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeRestrictedMessageEvent_maxBound_feature", "_UI_TimeRestrictedMessageEvent_type"),
				 TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Exclude Min Bound feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExcludeMinBoundPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeRestrictedMessageEvent_excludeMinBound_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeRestrictedMessageEvent_excludeMinBound_feature", "_UI_TimeRestrictedMessageEvent_type"),
				 TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Exclude Max Bound feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExcludeMaxBoundPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeRestrictedMessageEvent_excludeMaxBound_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeRestrictedMessageEvent_excludeMaxBound_feature", "_UI_TimeRestrictedMessageEvent_type"),
				 TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns TimeRestrictedMessageEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TimeRestrictedMessageEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TimeRestrictedMessageEvent)object).getMessageName();
		return label == null || label.length() == 0 ?
			getString("_UI_TimeRestrictedMessageEvent_type") :
			getString("_UI_TimeRestrictedMessageEvent_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TimeRestrictedMessageEvent.class)) {
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME:
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND:
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND:
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND:
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimedruntimeEditPlugin.INSTANCE;
	}

}
