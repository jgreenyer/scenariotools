package org.scenariotools.msd.timed.statespaceexploration.popup.actions;

import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.statespaceexploration.popup.actions.ExploreStateSpaceAction;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

public class ExploreTimedStateSpaceAction extends ExploreStateSpaceAction {

	protected String jobName = "Timed MSD state space exploration.";
	protected String infoTitle = "Timed State Space Exploration";

	@Override
	protected MSDRuntimeStateGraph createMSDRuntimeStateGraph() {
		return TimedRuntimeFactory.eINSTANCE.createTimedMSDRuntimeStateGraph();
	}

	
}
