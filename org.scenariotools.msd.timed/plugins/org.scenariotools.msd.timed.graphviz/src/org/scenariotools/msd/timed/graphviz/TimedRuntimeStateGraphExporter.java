package org.scenariotools.msd.timed.graphviz;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.synthesis.graphviz.RuntimeStateGraphExporter;
import org.scenariotools.timed.timedcontrollersystem.ClockConstraint;
import org.scenariotools.timed.timedcontrollersystem.ClockReset;
import org.scenariotools.timed.timedcontrollersystem.TimedControllerState;
import org.scenariotools.timed.timedcontrollersystem.TimedTransition;


public class TimedRuntimeStateGraphExporter extends RuntimeStateGraphExporter {

	@Override
	protected String getMsdStateString(MSDRuntimeState state) {
		String result = super.getMsdStateString(state);
		if (state instanceof TimedMSDRuntimeState) {
			TimedMSDRuntimeState timedState = (TimedMSDRuntimeState) state;
			result += maskString("\\n*** State Federation ***\\n" + timedState.getFederationString()).replace(", ", "<br align=\"left\"/>");
//			result += maskString("\\n*** State Federation ***\\n" + timedState.getStringToStringAnnotationMap().get("stateFederation")).replace(", ", "<br align=\"left\"/>");
			String goodFedStr = timedState.getGoodFederationString();
			if (goodFedStr != null && !goodFedStr.equals("")){
				result += maskString("\\n*** Good Federation ***\\n" + goodFedStr).replace(", ", "<br align=\"left\"/>");
			}
			String badFedStr = timedState.getBadFederationString();
			if (badFedStr != null && !badFedStr.equals("")){
				result += maskString("\\n*** Losing Federation ***\\n" + badFedStr).replace(", ", "<br align=\"left\"/>");
			}
			String winFedStr = timedState.getWinningFederationString();
			if (winFedStr != null && !winFedStr.equals("")){
				result += maskString("\\n\\n*** Winning Federation ***\\n" + winFedStr + "\\n").replace(", ", "<br align=\"left\"/>");
			}
			else {
				result += maskString("\\n").replace(", ", "<br align=\"left\"/>");
			}
			result += maskString(timedState.getBufferString()).replace(", ", "<br align=\"left\"/>");;
		}
		return result;
	}

	@Override
	protected String getEventLabel(Event event) {
		if (event instanceof TimeConditionEvent)
			return getTimeConditionEventLabel((TimeConditionEvent) event);
		else
			return super.getEventLabel(event);
	}

	@Override
	protected boolean isControllable(Transition transition) {
		Event event = transition.getEvent();
		if(event instanceof AsynchronousReceiveEvent)
			return false;
		else if (event instanceof TimeConditionEvent)
			return ((TimeConditionEvent) event).isTimedControllable();
		else
			return super.isControllable(transition);
	}

	public void export(Object object) {
		this.graph = (RuntimeStateGraph) object;
		RuntimeUtil.Helper.resetObjectNumbers();
		graphvizData.append("digraph G {\n");
		graphvizData
				.append("node[shape=Mrecord, style=\"rounded, filled\", fillcolor=\"lightgray:white\", gradientangle=90]\n");
		graphvizData.append("edge[arrowhead=\""
				+ (outputFormat == OutputFormat.PLAIN ? "none" : "vee")
				+ "\", minlen=2]\n");
		graphvizData
				.append("startstate[shape=circle, label=\"\", width=\"0.2\", height=\"0.2\", fillcolor=black]\n");
		for (State state : graph.getStates()){
			exportState((RuntimeState) state);
		}

		graphvizData.append("startstate -> "
				+ graph.getStartState().hashCode() + "[minlen=1]\n");
		graphvizData.append("}\n");
		outputGraph();
		makeImage();
	}

	private static String maskString(String string) {
		if (string == null){ return "";}
		return string.replace("&", "&amp;").replace(">", "&gt;")
				.replace("<", "&lt;").replace("\\n", "<br align=\"left\"/>");
	}

	private String getTimeConditionEventLabel(TimeConditionEvent event) {
		String observersString = "";
		for (EObject obj : event.getClockObservers()) {
			observersString += RuntimeUtil.Helper.getEObjectName(obj) + ", ";
		}
		int len = observersString.length();
		if (len != 0)
			observersString = observersString.substring(0, len - 2);
		return "<font color=\""
				+ (event.isConstraintFulfilled() ? "green" : "red") + "\">"
				+ (event.isConstraintFulfilled() ? "" : "not ")
				+ event.getClockName() + maskString(event.getOperatorString())
				+ event.getCompareValue() + "[" + observersString + "]"
				+ "</font>";
	}

	@Override
	protected void exportTransition(Transition transition) {
		if (transition.isExcluded()) return;
		if (transition instanceof TimedTransition) {
			TimedTransition timedTransition = (TimedTransition) transition;
			graphvizData.append(transition.getSourceState().hashCode() + "->"
					+ transition.getTargetState().hashCode() + "[");
			graphvizData.append(outputFormat == OutputFormat.PLAIN ? "label="
					: "xlabel=");
			graphvizData.append(createTimedTransitionLabel(timedTransition));
			graphvizData.append("]\n");
		} 
		else {
			super.exportTransition(transition);
		}
	}

	@Override
	protected String getMessageEventLabel(MessageEvent event) {
		if (event instanceof AsynchronousSendEvent) {
			return "s: " + super.getMessageEventLabel(event);
		} else if (event instanceof AsynchronousReceiveEvent) {
			AsynchronousReceiveEvent recEvent = (AsynchronousReceiveEvent) event;
			return "<font color=\"grey\">r: "
					+ super.getMessageEventLabel(event)
					+ (recEvent.getMinDelay() != 0
							|| recEvent.getMaxDelay() != 0 ? " ["
							+ recEvent.getMinDelay() + ","
							+ recEvent.getMaxDelay() + "]" : "") + "</font>";
		} else
			return super.getMessageEventLabel(event);
	}

	@Override
	protected void exportTransitionAttributeString(Transition transition) {
		super.exportTransitionAttributeString(transition);
		if (transition.getEvent() instanceof AsynchronousReceiveEvent) {
			graphvizData.append(", color=\"grey\"");
		}
	}

	public static String createTimedTransitionLabel(TimedTransition transition) {
		TimedTransition timedTransition = (TimedTransition) transition;
		// TODO: refactor to remove duplicate code!
		String guardString = "";
		for (ClockConstraint guard : timedTransition.getTimeGuard())
			guardString += guard.getClockName() + " "
					+ guard.getOperatorString() + " " + guard.getCompareValue()
					+ ", ";
		if (!guardString.isEmpty()) {
			guardString = guardString.substring(0, guardString.length() - 2);
			guardString = "[" + maskString(guardString) + "]";
		}

		String resetsString = "";
		for (ClockReset reset : timedTransition.getResets())
			resetsString += reset.getClockName() + ", ";
		if (!resetsString.isEmpty()) {
			resetsString = resetsString.substring(0, resetsString.length() - 2);
			resetsString = "{reset: " + maskString(resetsString) + "}";
		}
		String conditionString = guardString;
		String actionString = resetsString;
		if (timedTransition.getEvent() != null
				&& timedTransition.getEvent() instanceof MessageEvent) {
			MessageEvent messageEvent = (MessageEvent) timedTransition
					.getEvent();
			// TODO: support parameters
			if (!timedTransition.isAsynchronous()) {
				conditionString += " " + messageEvent.getMessageName()
						+ (timedTransition.isSending() ? "!" : "?");
			} else {
				if (timedTransition.isSending()) {
					actionString = messageEvent.getMessageName() + " "
							+ actionString;
				} else {
					conditionString += " " + messageEvent.getMessageName();
				}
			}
		}
		return "<" + conditionString
				+ (!actionString.isEmpty() ? " / <br align=\"left\"/>" + actionString : "")
				+ ">";
	}

	@Override
	protected void exportState(RuntimeState state) {
		if (state.isExcluded()) return;
		if (state instanceof TimedControllerState) {
			String invString = "";
			for (ClockConstraint inv : ((TimedControllerState) state)
					.getTimeInvariant()) {
				invString += inv.getClockName() + " " + inv.getOperatorString()
						+ " " + inv.getCompareValue() + "\\n";
			}
			if (!invString.isEmpty())
				invString = maskString(invString.substring(0,
						invString.length() - 2));
			TimedControllerState timedState = (TimedControllerState) state;
			graphvizData.append(state.hashCode() + "[label=<{");
			graphvizData.append("<b>" + timedState.getName() + "</b><br align=\"left\"/>");
			if (!invString.equals(""))
				graphvizData.append("|<b>invariant</b> " + invString
						+ "<br align=\"left\"/><br align=\"left\"/>");
			else
				graphvizData.append("<br align=\"left\"/>");
			graphvizData.append("}>];\n");
			for (Transition transition : state.getOutgoingTransition()) {
				exportTransition(transition);
			}
		} else{
			super.exportState(state);
		}
	}

	@Override
	protected void outputNodeShape(boolean plain) {
		graphvizData.append(", shape=box");
	}

}
