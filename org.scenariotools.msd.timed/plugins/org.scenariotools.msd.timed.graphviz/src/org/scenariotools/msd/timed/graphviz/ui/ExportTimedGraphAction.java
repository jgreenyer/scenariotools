package org.scenariotools.msd.timed.graphviz.ui;

import org.scenariotools.msd.timed.graphviz.TimedRuntimeStateGraphExporter;
import org.scenariotools.synthesis.graphviz.GraphvizExporter;
import org.scenariotools.synthesis.graphviz.ui.ExportControllerAction;

public class ExportTimedGraphAction extends ExportControllerAction {
	protected String jobName="Timed Graph Export.";

	@Override
	protected GraphvizExporter createExporter(){
		return new TimedRuntimeStateGraphExporter();
	}
}
