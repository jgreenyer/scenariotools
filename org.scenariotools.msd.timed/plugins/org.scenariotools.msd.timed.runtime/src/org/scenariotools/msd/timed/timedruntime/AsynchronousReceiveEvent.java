/**
 */
package org.scenariotools.msd.timed.timedruntime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asynchronous Receive Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getClockName <em>Clock Name</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousReceiveEvent()
 * @model
 * @generated
 */
public interface AsynchronousReceiveEvent extends AsynchronousMessageEvent {

	/**
	 * Returns the value of the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Delay</em>' attribute.
	 * @see #setMinDelay(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousReceiveEvent_MinDelay()
	 * @model
	 * @generated
	 */
	int getMinDelay();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMinDelay <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Delay</em>' attribute.
	 * @see #getMinDelay()
	 * @generated
	 */
	void setMinDelay(int value);

	/**
	 * Returns the value of the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Delay</em>' attribute.
	 * @see #setMaxDelay(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousReceiveEvent_MaxDelay()
	 * @model
	 * @generated
	 */
	int getMaxDelay();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMaxDelay <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Delay</em>' attribute.
	 * @see #getMaxDelay()
	 * @generated
	 */
	void setMaxDelay(int value);

	/**
	 * Returns the value of the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name</em>' attribute.
	 * @see #setClockName(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousReceiveEvent_ClockName()
	 * @model
	 * @generated
	 */
	String getClockName();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getClockName <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Name</em>' attribute.
	 * @see #getClockName()
	 * @generated
	 */
	void setClockName(String value);
} // AsynchronousReceiveEvent
