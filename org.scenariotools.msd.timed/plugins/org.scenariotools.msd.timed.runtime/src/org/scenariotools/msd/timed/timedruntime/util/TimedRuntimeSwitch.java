/**
 */
package org.scenariotools.msd.timed.timedruntime.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.SynchronousMessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.timed.timedruntime.*;
import org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage
 * @generated
 */
public class TimedRuntimeSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TimedRuntimePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedRuntimeSwitch() {
		if (modelPackage == null) {
			modelPackage = TimedRuntimePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE: {
				TimedMSDRuntimeState timedMSDRuntimeState = (TimedMSDRuntimeState)theEObject;
				T result = caseTimedMSDRuntimeState(timedMSDRuntimeState);
				if (result == null) result = caseMSDRuntimeState(timedMSDRuntimeState);
				if (result == null) result = caseRuntimeState(timedMSDRuntimeState);
				if (result == null) result = caseState(timedMSDRuntimeState);
				if (result == null) result = caseAnnotatableElement(timedMSDRuntimeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIMED_ACTIVE_MSD: {
				TimedActiveMSD timedActiveMSD = (TimedActiveMSD)theEObject;
				T result = caseTimedActiveMSD(timedActiveMSD);
				if (result == null) result = caseActiveMSD(timedActiveMSD);
				if (result == null) result = caseActiveProcess(timedActiveMSD);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH: {
				TimedMSDRuntimeStateGraph timedMSDRuntimeStateGraph = (TimedMSDRuntimeStateGraph)theEObject;
				T result = caseTimedMSDRuntimeStateGraph(timedMSDRuntimeStateGraph);
				if (result == null) result = caseMSDRuntimeStateGraph(timedMSDRuntimeStateGraph);
				if (result == null) result = caseRuntimeStateGraph(timedMSDRuntimeStateGraph);
				if (result == null) result = caseStateGraph(timedMSDRuntimeStateGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIMED_RUNTIME_UTIL: {
				TimedRuntimeUtil timedRuntimeUtil = (TimedRuntimeUtil)theEObject;
				T result = caseTimedRuntimeUtil(timedRuntimeUtil);
				if (result == null) result = caseRuntimeUtil(timedRuntimeUtil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIME_CONDITION_EVENT: {
				TimeConditionEvent timeConditionEvent = (TimeConditionEvent)theEObject;
				T result = caseTimeConditionEvent(timeConditionEvent);
				if (result == null) result = caseEvent(timeConditionEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK: {
				TimedCommunicationLink timedCommunicationLink = (TimedCommunicationLink)theEObject;
				T result = caseTimedCommunicationLink(timedCommunicationLink);
				if (result == null) result = caseCommunicationLink(timedCommunicationLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT: {
				AsynchronousMessageEvent asynchronousMessageEvent = (AsynchronousMessageEvent)theEObject;
				T result = caseAsynchronousMessageEvent(asynchronousMessageEvent);
				if (result == null) result = caseMessageEvent(asynchronousMessageEvent);
				if (result == null) result = caseEvent(asynchronousMessageEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.ASYNCHRONOUS_SEND_EVENT: {
				AsynchronousSendEvent asynchronousSendEvent = (AsynchronousSendEvent)theEObject;
				T result = caseAsynchronousSendEvent(asynchronousSendEvent);
				if (result == null) result = caseAsynchronousMessageEvent(asynchronousSendEvent);
				if (result == null) result = caseMessageEvent(asynchronousSendEvent);
				if (result == null) result = caseEvent(asynchronousSendEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT: {
				AsynchronousReceiveEvent asynchronousReceiveEvent = (AsynchronousReceiveEvent)theEObject;
				T result = caseAsynchronousReceiveEvent(asynchronousReceiveEvent);
				if (result == null) result = caseAsynchronousMessageEvent(asynchronousReceiveEvent);
				if (result == null) result = caseMessageEvent(asynchronousReceiveEvent);
				if (result == null) result = caseEvent(asynchronousReceiveEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIMED_SCENARIO_RUN_CONFIGURATION: {
				TimedScenarioRunConfiguration timedScenarioRunConfiguration = (TimedScenarioRunConfiguration)theEObject;
				T result = caseTimedScenarioRunConfiguration(timedScenarioRunConfiguration);
				if (result == null) result = caseScenarioRunConfiguration(timedScenarioRunConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT: {
				TimeRestrictedMessageEvent timeRestrictedMessageEvent = (TimeRestrictedMessageEvent)theEObject;
				T result = caseTimeRestrictedMessageEvent(timeRestrictedMessageEvent);
				if (result == null) result = caseSynchronousMessageEvent(timeRestrictedMessageEvent);
				if (result == null) result = caseMessageEvent(timeRestrictedMessageEvent);
				if (result == null) result = caseEvent(timeRestrictedMessageEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed MSD Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedMSDRuntimeState(TimedMSDRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Active MSD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Active MSD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedActiveMSD(TimedActiveMSD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed MSD Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedMSDRuntimeStateGraph(TimedMSDRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Util</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Util</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedRuntimeUtil(TimedRuntimeUtil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Condition Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Condition Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeConditionEvent(TimeConditionEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Communication Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Communication Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedCommunicationLink(TimedCommunicationLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asynchronous Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asynchronous Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsynchronousMessageEvent(AsynchronousMessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asynchronous Send Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asynchronous Send Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsynchronousSendEvent(AsynchronousSendEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asynchronous Receive Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asynchronous Receive Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsynchronousReceiveEvent(AsynchronousReceiveEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Scenario Run Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Scenario Run Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedScenarioRunConfiguration(TimedScenarioRunConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Restricted Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Restricted Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeRestrictedMessageEvent(TimeRestrictedMessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatableElement(AnnotatableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeState(RuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeState(MSDRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Process</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Process</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveProcess(ActiveProcess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSD(ActiveMSD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateGraph(StateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeStateGraph(RuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeStateGraph(MSDRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Runtime Util</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Runtime Util</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeUtil(RuntimeUtil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationLink(CommunicationLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEvent(MessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scenario Run Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scenario Run Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScenarioRunConfiguration(ScenarioRunConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronous Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronous Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronousMessageEvent(SynchronousMessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TimedRuntimeSwitch
