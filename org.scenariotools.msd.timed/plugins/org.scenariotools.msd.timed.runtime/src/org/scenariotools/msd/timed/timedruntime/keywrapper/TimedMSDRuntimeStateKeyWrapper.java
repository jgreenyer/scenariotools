package org.scenariotools.msd.timed.timedruntime.keywrapper;

import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;

public class TimedMSDRuntimeStateKeyWrapper extends MSDRuntimeStateKeyWrapper {

	public TimedMSDRuntimeStateKeyWrapper(MSDRuntimeState msdRuntimeState) {
		super(msdRuntimeState);
		if(msdRuntimeState instanceof TimedMSDRuntimeState){
			TimedMSDRuntimeState timedState=((TimedMSDRuntimeState) msdRuntimeState);
			addSubObject(timedState.getFederation());
			addSubObject(timedState.getControllableClocks());
			addSubObject(timedState.getClockToLastResetObservers());
			addSubObject(timedState.getMessageQueuesForLinks());
		}
	}

}
