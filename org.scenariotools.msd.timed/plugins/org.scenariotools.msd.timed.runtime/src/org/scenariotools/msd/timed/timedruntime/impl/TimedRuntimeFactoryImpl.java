/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.msd.timed.timedruntime.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedRuntimeFactoryImpl extends EFactoryImpl implements TimedRuntimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TimedRuntimeFactory init() {
		try {
			TimedRuntimeFactory theTimedRuntimeFactory = (TimedRuntimeFactory)EPackage.Registry.INSTANCE.getEFactory(TimedRuntimePackage.eNS_URI);
			if (theTimedRuntimeFactory != null) {
				return theTimedRuntimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TimedRuntimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedRuntimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE: return createTimedMSDRuntimeState();
			case TimedRuntimePackage.TIMED_ACTIVE_MSD: return createTimedActiveMSD();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH: return createTimedMSDRuntimeStateGraph();
			case TimedRuntimePackage.TIMED_RUNTIME_UTIL: return createTimedRuntimeUtil();
			case TimedRuntimePackage.TIME_CONDITION_EVENT: return createTimeConditionEvent();
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK: return createTimedCommunicationLink();
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT: return createAsynchronousMessageEvent();
			case TimedRuntimePackage.ASYNCHRONOUS_SEND_EVENT: return createAsynchronousSendEvent();
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT: return createAsynchronousReceiveEvent();
			case TimedRuntimePackage.TIMED_SCENARIO_RUN_CONFIGURATION: return createTimedScenarioRunConfiguration();
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT: return createTimeRestrictedMessageEvent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedMSDRuntimeState createTimedMSDRuntimeState() {
		TimedMSDRuntimeStateImpl timedMSDRuntimeState = new TimedMSDRuntimeStateImpl();
		return timedMSDRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedActiveMSD createTimedActiveMSD() {
		TimedActiveMSDImpl timedActiveMSD = new TimedActiveMSDImpl();
		return timedActiveMSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedMSDRuntimeStateGraph createTimedMSDRuntimeStateGraph() {
		TimedMSDRuntimeStateGraphImpl timedMSDRuntimeStateGraph = new TimedMSDRuntimeStateGraphImpl();
		return timedMSDRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedRuntimeUtil createTimedRuntimeUtil() {
		TimedRuntimeUtilImpl timedRuntimeUtil = new TimedRuntimeUtilImpl();
		return timedRuntimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeConditionEvent createTimeConditionEvent() {
		TimeConditionEventImpl timeConditionEvent = new TimeConditionEventImpl();
		return timeConditionEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedCommunicationLink createTimedCommunicationLink() {
		TimedCommunicationLinkImpl timedCommunicationLink = new TimedCommunicationLinkImpl();
		return timedCommunicationLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousMessageEvent createAsynchronousMessageEvent() {
		AsynchronousMessageEventImpl asynchronousMessageEvent = new AsynchronousMessageEventImpl();
		return asynchronousMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousSendEvent createAsynchronousSendEvent() {
		AsynchronousSendEventImpl asynchronousSendEvent = new AsynchronousSendEventImpl();
		return asynchronousSendEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousReceiveEvent createAsynchronousReceiveEvent() {
		AsynchronousReceiveEventImpl asynchronousReceiveEvent = new AsynchronousReceiveEventImpl();
		return asynchronousReceiveEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedScenarioRunConfiguration createTimedScenarioRunConfiguration() {
		TimedScenarioRunConfigurationImpl timedScenarioRunConfiguration = new TimedScenarioRunConfigurationImpl();
		return timedScenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeRestrictedMessageEvent createTimeRestrictedMessageEvent() {
		TimeRestrictedMessageEventImpl timeRestrictedMessageEvent = new TimeRestrictedMessageEventImpl();
		return timeRestrictedMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedRuntimePackage getTimedRuntimePackage() {
		return (TimedRuntimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TimedRuntimePackage getPackage() {
		return TimedRuntimePackage.eINSTANCE;
	}

} //TimedRuntimeFactoryImpl
