/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.impl.ActiveMSDImpl;
import org.scenariotools.msd.runtime.impl.StringToStringMapEntryImpl;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.timed.timedruntime.keywrapper.TimedActiveMSDKeyWrapper;
import org.scenariotools.msd.util.RuntimeUtil;

import org.muml.udbm.ClockConstraint;
import org.muml.udbm.ClockZone;
import org.muml.udbm.DifferenceClockConstraint;
import org.muml.udbm.Federation;
import org.muml.udbm.SimpleClockConstraint;
import org.muml.udbm.UDBMClock;
import org.muml.udbm.clockconstraint.TrueClockConstraint;

/**
 * <!-- begin-user-doc --> An implementation of the model object ' <em><b>Timed
 * Active MSD</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl#getCurrentRuntimeState <em>Current Runtime State</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl#getClockNameToClockInstanceMap <em>Clock Name To Clock Instance Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedActiveMSDImpl extends ActiveMSDImpl implements TimedActiveMSD {
	/**
	 * The cached value of the '{@link #getCurrentRuntimeState() <em>Current
	 * Runtime State</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCurrentRuntimeState()
	 * @generated
	 * @ordered
	 */
	protected MSDRuntimeState currentRuntimeState;

	/**
	 * The cached value of the '{@link #getClockNameToClockInstanceMap() <em>Clock Name To Clock Instance Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockNameToClockInstanceMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> clockNameToClockInstanceMap;

	/**
	 * @generated NOT
	 */
	public boolean isInHotCutDueToTimeConditionWithLowerBound() {
		// TODO: refactor (duplicate code with ActiveMSDImpl)
		for (InteractionFragment currentFragment : getCurrentState().getLifelineToInteractionFragmentMap().values()) {
			if (currentFragment instanceof CombinedFragment && ((CombinedFragment) currentFragment)
					.getInteractionOperator() != InteractionOperatorKind.ALT_LITERAL) {
				boolean isEnabled;

				CombinedFragment combinedFragment = (CombinedFragment) currentFragment;
				isEnabled = true;
				Iterator<Lifeline> byCombinedFragmentCoveredLifelinesIterator = combinedFragment.getCovereds()
						.iterator();
				while (byCombinedFragmentCoveredLifelinesIterator.hasNext() && isEnabled) {
					if (getCurrentState().getLifelineToInteractionFragmentMap()
							.get(byCombinedFragmentCoveredLifelinesIterator.next()) != combinedFragment)
						isEnabled = false;
				}
				if (isEnabled) {
					TimedMSDRuntimeState state = (TimedMSDRuntimeState) getCurrentRuntimeState();
					Federation federation = (Federation) state.getFederation();
					// Federation waitFederation=(Federation)
					// federation.clone();
					// waitFederation.up();
					if (getRuntimeUtil().isTimeCondition(combinedFragment)) {
						SimpleClockConstraint constraint = getRuntimeUtil()
								.parseClockConstraintForFragment(combinedFragment, federation, this);
						Federation boundFulfilledFed = (Federation) federation.clone();
						boundFulfilledFed.and(constraint);
						federation.and(new TrueClockConstraint()); // TODO:
																	// remove
																	// this
																	// workaround
																	// when
																	// possible
						boolean constraintAlwaysFulfilled = federation.equals(boundFulfilledFed);
						if (RuntimeUtil.Helper.isHot(combinedFragment)
								&& constraint.getRelationalOperator().toString().contains(">")
								&& !constraintAlwaysFulfilled)
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @generated NOT
	 */
	public ActiveMSDProgress progressCombinedFragment(CombinedFragment combinedFragment) {
		// System.out.println("processing fragment, federation=" +
		// getCurrentRuntimeState().getFederation());
		TimedMSDRuntimeState state = (TimedMSDRuntimeState) getCurrentRuntimeState();
		Federation federation = (Federation) state.getFederation();
		// Federation waitFederation=(Federation) federation.clone();
		// waitFederation.up();
		if (getRuntimeUtil().isTimeCondition(combinedFragment)) {
			SimpleClockConstraint constraint = getRuntimeUtil().parseClockConstraintForFragment(combinedFragment,
					federation, this);
			Federation boundFulfilledFed = (Federation) federation.clone();
			try {
				boundFulfilledFed.and(constraint);
			} catch (Exception e) {
				e.printStackTrace();
			}
			federation.and(new TrueClockConstraint()); // TODO: remove this
														// workaround when
														// possible
			boolean constraintAlwaysFulfilled = federation.equals(boundFulfilledFed);
			boolean constraintNeverFulfilled = boundFulfilledFed.isEmpty();
			if (RuntimeUtil.Helper.isHot(combinedFragment)) {
				if (constraint.getRelationalOperator().toString().contains(">")) {
					if (constraintAlwaysFulfilled) { // always fulfilled->step
														// beyond condition
						progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
						if (constraint.getClock() != null) {
							// System.out.println("hot clock constraint " +
							// constraint + " is always fulfilled");
						}
						return ActiveMSDProgress.PROGRESS;
					} else { // consider case where bound is not yet
								// fulfilled->delay transition handles other
								// case
						if (getRuntimeUtil().isEnforceEarliestDelaysFirst()){
							getCurrentState().setHot(true);
						}
						getCurrentState().setExecuted(true);
						// System.out.println("hot clock constraint " +
						// constraint + " is not always fulfilled");
						return ActiveMSDProgress.NO_PROGRESS;
					}
				} else {
					if (constraintNeverFulfilled) {
						// System.out.println("hot clock constraint " +
						// constraint + " is never fulfilled");
						return ActiveMSDProgress.TIMED_SAFETY_VIOLATION;
					} else if (constraintAlwaysFulfilled) {
						// System.out.println("hot clock constraint " +
						// constraint + " is always fulfilled");
						progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
						return ActiveMSDProgress.PROGRESS;
					} else
						return ActiveMSDProgress.NO_PROGRESS;
				}
			} else {
				if (constraintNeverFulfilled) {
					// System.out.println("cold clock constraint " + constraint
					// + " is never fulfilled");
					return ActiveMSDProgress.TIMED_COLD_VIOLATION;
				} else if (constraintAlwaysFulfilled) {
					progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
					// System.out.println("cold clock constraint " + constraint
					// + " is always fulfilled");
					return ActiveMSDProgress.PROGRESS;
				} else
					return ActiveMSDProgress.NO_PROGRESS;
			}
		} else if (getRuntimeUtil().isClockReset(combinedFragment)) {
			String resetString = getCombinedFragmentGuardString(combinedFragment);
			String clockName = resetString.split("=")[0].trim();
			// System.out.println("resetting clock " + clockName);
			UDBMClock clock = federation.getFromClock(getClockInstanceNameForClockName(clockName));
			federation = (Federation) ((TimedMSDRuntimeState) getCurrentRuntimeState()).getFederation();

			HashSet<UDBMClock> resets = new HashSet<>();
			resets.add(clock);
			federation.applyResets(resets);

			progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
			((TimedMSDRuntimeState) getCurrentRuntimeState()).addResetClock(clock);
		} else {
			return super.progressCombinedFragment(combinedFragment);
		}

		return ActiveMSDProgress.NO_PROGRESS;
	}

	/**
	 * @Generated NOT
	 */
	public String getClockInstanceNameForClockName(String name) {
		// System.out.println(getClockNameToClockInstanceMap());
		return getClockNameToClockInstanceMap().get(name);
	}

	protected void removeClockForMSD(String removedClockInstanceName) {
		// System.out.println("removing clock " + removedClockInstanceName);
		TimedMSDRuntimeState state = (TimedMSDRuntimeState) getCurrentRuntimeState();
		Federation federation = (Federation) state.getFederation();
		UDBMClock removedClock = federation.getFromClock(removedClockInstanceName);

		HashSet<UDBMClock> clocks = new HashSet<UDBMClock>();
		Iterator<? extends UDBMClock> it = federation.iteratorOfClock();
		while (it.hasNext()) {
			UDBMClock clock = it.next();
			if (!"zeroclock".equals(clock.getName()) && clock != removedClock)
				clocks.add(clock);
		}

		HashSet<ClockConstraint> constraints = new HashSet<ClockConstraint>();

		for (ClockZone zone : federation.getClockZone()) {
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext()) {
				ClockConstraint constraint = (ClockConstraint) ccIt.next().clone();
				if (constraint instanceof SimpleClockConstraint) {
					if (((SimpleClockConstraint) constraint).getClock() != removedClock) {
						constraints.add(constraint);
					}

				} else if (constraint instanceof DifferenceClockConstraint) {
					DifferenceClockConstraint diffConstraint = ((DifferenceClockConstraint) constraint);
					if (diffConstraint.getClockMinuend() != removedClock
							&& diffConstraint.getClockSubtrahend() != removedClock) {
						constraints.add(constraint);
					}
				}
			}
		}

		// System.out.println("fed before removal: " + federation);
		federation = getRuntimeUtil().getFederationFactory().createFederation(clocks, constraints);
		state.setFederation(federation);
		// System.out.println("fed after removal: " + federation);
	}

	public void removeClocksForMSD() {
//		System.out.println("removing clocks");
//		System.out.println(getClockNameToClockInstanceMap());
		if (getClockNameToClockInstanceMap().size() == 0){
//			System.out.println("no clocks to remove");
		}
		for (String clockInstanceName : getClockNameToClockInstanceMap().values()) {
//			System.out.println("removing clock " + clockInstanceName);
			removeClockForMSD(clockInstanceName);
		}
	}

	public void addClocksForMSD() {
		Set<String> clockNames = ((TimedRuntimeUtil) getRuntimeUtil()).getClockNamesForMSD(this.getInteraction());
		for (String clockName : clockNames)
			addClockForMSD(clockName);
	}

	protected int getFirstFreeInstanceNumberForClock(String clockQualifiedName) {
		TimedMSDRuntimeState state = (TimedMSDRuntimeState) getCurrentRuntimeState();
		Federation federation = (Federation) state.getFederation();

		Set<Integer> usedNumbers = new HashSet<>();
		@SuppressWarnings("unchecked")
		Iterator<UDBMClock> clockIt = (Iterator<UDBMClock>) federation.iteratorOfClock();
		while (clockIt.hasNext()) {
			UDBMClock clock = clockIt.next();
			String[] clockNameSegments = clock.getName().split("_");
			if (clockNameSegments[0].equals(clockQualifiedName)) {
				int usedNum = Integer.parseInt(clockNameSegments[1]);
				usedNumbers.add(usedNum);
			}
		}

		int result = 0;
		while (true) {
			if (!usedNumbers.contains(result))
				return result;
			result++;
		}
	}

	public void addClockForMSD(String clockName) {
		// System.out.println("adding clock " + clockName);
		String clockQualifiedName = getInteraction().getName() + "I" + clockName;
		int instanceNumber = getFirstFreeInstanceNumberForClock(clockQualifiedName);
		String clockInstanceName = clockQualifiedName + "_" + instanceNumber;

		UDBMClock newClock = getRuntimeUtil().getClockInstanceForName(clockInstanceName);
		HashSet<UDBMClock> newFederationClocks = new HashSet<UDBMClock>();
		TimedMSDRuntimeState state = (TimedMSDRuntimeState) getCurrentRuntimeState();
		Federation federation = (Federation) state.getFederation();
		Iterator<? extends UDBMClock> clockIt = federation.iteratorOfClock();
		while (clockIt.hasNext()) {
			UDBMClock clock = clockIt.next();
			if (!"zeroclock".equals(clock.getName()))
				newFederationClocks.add(clock);
		}
		newFederationClocks.add(newClock);
		this.getClockNameToClockInstanceMap().put(clockName, clockInstanceName);

		HashSet<ClockConstraint> constraints = new HashSet<ClockConstraint>();
		for (ClockZone zone : federation.getClockZone()) {
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext())
				constraints.add((ClockConstraint) ccIt.next().clone());
		}

		federation = getRuntimeUtil().getFederationFactory().createFederation(newFederationClocks, constraints);
		HashSet<UDBMClock> clocksToReset = new HashSet<UDBMClock>();
		clocksToReset.add(newClock);
		federation.applyResets(clocksToReset);
		state.setFederation(federation);

		// System.out.println("constraints of federation after adding clock " +
		// clockInstanceName + ": " + constraints);
	}

	public TimedRuntimeUtil getRuntimeUtil() {
		return (TimedRuntimeUtil) super.getRuntimeUtil();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected TimedActiveMSDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_ACTIVE_MSD;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public TimedMSDRuntimeState getCurrentRuntimeState() {
		if (currentRuntimeState != null && currentRuntimeState.eIsProxy()) {
			InternalEObject oldCurrentRuntimeState = (InternalEObject) currentRuntimeState;
			currentRuntimeState = (MSDRuntimeState) eResolveProxy(oldCurrentRuntimeState);
			if (currentRuntimeState != oldCurrentRuntimeState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE, oldCurrentRuntimeState,
							currentRuntimeState));
			}
		}
		return (TimedMSDRuntimeState) currentRuntimeState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeState basicGetCurrentRuntimeState() {
		return currentRuntimeState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentRuntimeState(MSDRuntimeState newCurrentRuntimeState) {
		MSDRuntimeState oldCurrentRuntimeState = currentRuntimeState;
		currentRuntimeState = newCurrentRuntimeState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE, oldCurrentRuntimeState, currentRuntimeState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getClockNameToClockInstanceMap() {
		if (clockNameToClockInstanceMap == null) {
			clockNameToClockInstanceMap = new EcoreEMap<String,String>(RuntimePackage.Literals.STRING_TO_STRING_MAP_ENTRY, StringToStringMapEntryImpl.class, this, TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP);
		}
		return clockNameToClockInstanceMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP:
				return ((InternalEList<?>)getClockNameToClockInstanceMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE:
				if (resolve) return getCurrentRuntimeState();
				return basicGetCurrentRuntimeState();
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP:
				if (coreType) return getClockNameToClockInstanceMap();
				else return getClockNameToClockInstanceMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE:
				setCurrentRuntimeState((MSDRuntimeState)newValue);
				return;
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP:
				((EStructuralFeature.Setting)getClockNameToClockInstanceMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE:
				setCurrentRuntimeState((MSDRuntimeState)null);
				return;
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP:
				getClockNameToClockInstanceMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE:
				return currentRuntimeState != null;
			case TimedRuntimePackage.TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP:
				return clockNameToClockInstanceMap != null && !clockNameToClockInstanceMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void initializeRelevantEventsForActiveProcess() {
		super.initializeRelevantEventsForActiveProcess();
		if (getRuntimeUtil().isEnforceEarliestDelaysFirst()) {
			boolean hotTimeEventEnabled = false;
			for (TimeConditionEvent event : timeConditionEvents)
				if (event.isHot()) {
					hotTimeEventEnabled = true;
					break;
				}
			if (hotTimeEventEnabled) {
				getCurrentState().setHot(true);
				Interaction msd = getInteraction();
				for (Message message : msd.getMessages()) {
					if (!getViolatingEvents().containsKey(message) && !getEnabledEvents().containsKey(message)) {
						getViolatingEvents().put(message, getMessageEvent(message, false));
					}
				}
			}
		}
	}

	protected boolean isFragmentEnabled(CombinedFragment fragment) {
		for (Lifeline coveredLifeline : fragment.getCovereds())
			if (!getCurrentState().getLifelineToInteractionFragmentMap().get(coveredLifeline).equals(fragment))
				return false;
		return true;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void calculateRelevantTimeConditionEvents() {
		timeConditionEvents.clear();
		Set<CombinedFragment> timeConditionFragments = new HashSet<>();
		HashSet<TimeConditionEvent> relevantTimeConditionEvents = new HashSet<>();
		ActiveMSDCut currentCut = getCurrentState();
		for (InteractionFragment fragment : currentCut.getLifelineToInteractionFragmentMap().values()) {
			if (fragment instanceof CombinedFragment) {
				if (isFragmentEnabled((CombinedFragment) fragment)) {
					CombinedFragment combinedFragment = (CombinedFragment) fragment;
					if (getRuntimeUtil().isTimeCondition(combinedFragment)) {
						timeConditionFragments.add(combinedFragment);
					}
				}
			}
		}
		for (CombinedFragment fragment : timeConditionFragments) {
			TimeConditionEvent trueEvent = createTimeConditionEventForFragment(fragment, true);
			if (trueEvent != null)
				relevantTimeConditionEvents.add(trueEvent);

			TimeConditionEvent falseEvent = createTimeConditionEventForFragment(fragment, false);
			if (falseEvent != null)
				relevantTimeConditionEvents.add(falseEvent);
		}
		timeConditionEvents.addAll(relevantTimeConditionEvents);
	}

	private TimeConditionEvent createTimeConditionEventForFragment(CombinedFragment fragment, boolean fulfilled) {
		TimeConditionEvent timeConditionEvent;
		Federation federation = (Federation) ((TimedMSDRuntimeState) getCurrentRuntimeState()).getFederation();
		SimpleClockConstraint constraint = getRuntimeUtil().parseClockConstraintForFragment(fragment, federation, this);
		// do not create events for unfulfilled hot conditions with lower bound
		if (RuntimeUtil.Helper.isHot(fragment) && !fulfilled
				&& constraint.getRelationalOperator().toString().contains(">"))
			return null;
		Federation waitFederation = (Federation) federation.clone();
		if (constraint.getRelationalOperator().toString().contains(">") && RuntimeUtil.Helper.isHot(fragment))
			waitFederation.up();
		waitFederation.and(new TrueClockConstraint());
		Federation constraintFulfilledFederation = (Federation) waitFederation.clone();
		// System.out.println("fed:" + federation);
		if (constraint.getClock() != null) {
			// System.out.println("constraint:" + constraint);
		}
		try {
			constraintFulfilledFederation.and(constraint);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println("fulfilled fed:" + constraintFulfilledFederation);
		boolean constraintAlwaysFulfilled = constraintFulfilledFederation.equals(waitFederation);
		boolean constraintNeverFulfilled = constraintFulfilledFederation.isEmpty();
		if (fulfilled && constraintNeverFulfilled || !fulfilled && constraintAlwaysFulfilled)
			return null;

		timeConditionEvent = TimedRuntimeFactory.eINSTANCE.createTimeConditionEvent();
		timeConditionEvent.setConstraintFulfilled(fulfilled);
		timeConditionEvent.setClockCondition(fragment);
		timeConditionEvent.setClockName(constraint.getClock().getName());
		timeConditionEvent.setCompareValue(constraint.getValue());
		timeConditionEvent.setOperatorString(constraint.getRelationalOperator().toString());
		ActiveMSDCut currentCut = (ActiveMSDCut) ((TimedMSDRuntimeStateGraph) getCurrentRuntimeState().getStateGraph())
				.getElementContainer().getActiveState(this, getCurrentState());

		timeConditionEvent.setActiveMSDCut(currentCut);
		timeConditionEvent.setHot(RuntimeUtil.Helper.isHot(fragment));

		// modify federation of runtime state for hot condition with lower bound
		// if (RuntimeUtil.Helper.isHot(fragment)
		// && constraint.getRelationalOperator().toString().contains(">")) {
		// getCurrentRuntimeState().applyNegatedDelay(constraint);
		// }

		return timeConditionEvent;
	}

	private Set<TimeConditionEvent> timeConditionEvents = new HashSet<>();

	/**
	 * @generated NOT
	 */
	@Override
	public Set<TimeConditionEvent> getCurrentTimeConditionEvents() {
		return timeConditionEvents;
	}

	/**
	 * @generated NOT
	 */
	public void copyClocksFromTimedActiveMSD(TimedActiveMSD otherActiveMSD) {
		// System.out.println("old clocks:" +
		// this.getClockNameToClockInstanceMap());
		if (otherActiveMSD != this) {
			this.getClockNameToClockInstanceMap().clear();
			this.getClockNameToClockInstanceMap()
					.putAll(((TimedActiveMSDImpl) otherActiveMSD).getClockNameToClockInstanceMap());
		}
		// System.out.println("new clocks:" +
		// this.getClockNameToClockInstanceMap());
	}

	@Override
	public Collection<String> getClockInstanceNames() {
		return getClockNameToClockInstanceMap().values();
	}

	@Override
	protected MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature,
			EObject sendingObject, EObject receivingObject, Object parameterValue) {
		if (getRuntimeUtil().getBufferSize(sendingObject, receivingObject) > 0) {
			AsynchronousSendEvent newEvent = (AsynchronousSendEvent) getObjectSystem().getMessageEvent(eOperation,
					eStructuralFeature, sendingObject, receivingObject, parameterValue,
					TimedRuntimeFactory.eINSTANCE.createAsynchronousSendEvent());
			newEvent.getProgressedActiveMSDs().add(this);
			return newEvent;
		} else
			return super.getMessageEvent(eOperation, eStructuralFeature, sendingObject, receivingObject,
					parameterValue);
	}

	@Override
	protected MessageEvent getMessageEvent(Message message, boolean ignoreParameter) {
		MessageEvent event = super.getMessageEvent(message, ignoreParameter);
		if (event instanceof AsynchronousMessageEvent)
			((AsynchronousMessageEvent) event).getProgressedMessages().add(message);
		return event;
	}

	@Override
	public boolean isRelavant(MessageEvent messageEvent) {
		return messageEvent instanceof AsynchronousReceiveEvent || super.isRelavant(messageEvent);
	}

	protected boolean isMessageEnabled(Message message, MessageEvent event) {
		if (getRuntimeUtil().getBufferSize(event) > 0) {
			if (event instanceof AsynchronousSendEvent) {
				// if (getCurrentRuntimeState().getQueue(event).size() >=
				// getRuntimeUtil()
				// .getBufferSize(event))
				// return false;
				MessageOccurrenceSpecification sendingMOS = RuntimeUtil.Helper
						.getSendingMessageOccurrenceSpecification(message);
				return getCurrentState().isMOSEnabled(sendingMOS, RuntimeUtil.Helper.getSendingLifeline(message));
			} else if (event instanceof AsynchronousReceiveEvent) {
				if (getCurrentRuntimeState().getQueue(event).peek() != event)
					return false;
				MessageOccurrenceSpecification receivingMOS = RuntimeUtil.Helper
						.getReceivingMessageOccurrenceSpecification(message);
				return getCurrentState().isMOSEnabled(receivingMOS, RuntimeUtil.Helper.getReceivingLifeline(message));
			}
			return false;
		} else
			return super.isMessageEnabled(message, event);
	}

	/**
	 * @generated NOT
	 */
	public ActiveProcessKeyWrapper getKeyWrapper() {
		return new TimedActiveMSDKeyWrapper(this);
	}

} // TimedActiveMSDImpl
