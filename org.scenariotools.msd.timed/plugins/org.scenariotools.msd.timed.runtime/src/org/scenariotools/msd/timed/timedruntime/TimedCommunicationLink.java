/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Communication Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#isAsynchronous <em>Asynchronous</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getBufferSize <em>Buffer Size</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedCommunicationLink()
 * @model
 * @generated
 */
public interface TimedCommunicationLink extends CommunicationLink {
	/**
	 * Returns the value of the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Delay</em>' attribute.
	 * @see #setMinDelay(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedCommunicationLink_MinDelay()
	 * @model
	 * @generated
	 */
	int getMinDelay();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMinDelay <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Delay</em>' attribute.
	 * @see #getMinDelay()
	 * @generated
	 */
	void setMinDelay(int value);

	/**
	 * Returns the value of the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Delay</em>' attribute.
	 * @see #setMaxDelay(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedCommunicationLink_MaxDelay()
	 * @model
	 * @generated
	 */
	int getMaxDelay();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMaxDelay <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Delay</em>' attribute.
	 * @see #getMaxDelay()
	 * @generated
	 */
	void setMaxDelay(int value);

	/**
	 * Returns the value of the '<em><b>Asynchronous</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous</em>' attribute.
	 * @see #setAsynchronous(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedCommunicationLink_Asynchronous()
	 * @model
	 * @generated
	 */
	boolean isAsynchronous();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#isAsynchronous <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous</em>' attribute.
	 * @see #isAsynchronous()
	 * @generated
	 */
	void setAsynchronous(boolean value);

	/**
	 * Returns the value of the '<em><b>Buffer Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Buffer Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Buffer Size</em>' attribute.
	 * @see #setBufferSize(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedCommunicationLink_BufferSize()
	 * @model
	 * @generated
	 */
	int getBufferSize();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getBufferSize <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Buffer Size</em>' attribute.
	 * @see #getBufferSize()
	 * @generated
	 */
	void setBufferSize(int value);

} // TimedCommunicationLink
