/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.NamedElement;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.SynchronousMessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl;
import org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.ModalMessageEvent;
import org.muml.udbm.*;
import org.muml.udbm.clockconstraint.RelationalOperator;

/**
 * <!-- begin-user-doc --> An implementation of the model object ' <em><b>Timed
 * MSD Runtime State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#getFederationString <em>Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#getBufferString <em>Buffer String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimingConditionsInRequirementsSatisfiable <em>Timing Conditions In Requirements Satisfiable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimingConditionsInAssumptionsSatisfiable <em>Timing Conditions In Assumptions Satisfiable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimedSafetyViolationOccurredInRequirements <em>Timed Safety Violation Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimedSafetyViolationOccurredInAssumptions <em>Timed Safety Violation Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#getGoodFederationString <em>Good Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#getBadFederationString <em>Bad Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#getWinningFederationString <em>Winning Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimeInconsistencyOccurredInRequirements <em>Time Inconsistency Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isTimeInconsistencyOccurredInAssumptions <em>Time Inconsistency Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl#isUpperBoundRealTimeInconsistency <em>Upper Bound Real Time Inconsistency</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedMSDRuntimeStateImpl extends MSDRuntimeStateImpl implements TimedMSDRuntimeState {

	/**
	 * The default value of the '{@link #getFederationString() <em>Federation String</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFederationString()
	 * @generated
	 * @ordered
	 */
	protected static final String FEDERATION_STRING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFederationString() <em>Federation String</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFederationString()
	 * @generated
	 * @ordered
	 */
	protected String federationString = FEDERATION_STRING_EDEFAULT;
	/**
	 * The default value of the '{@link #getBufferString() <em>Buffer String</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBufferString()
	 * @generated
	 * @ordered
	 */
	protected static final String BUFFER_STRING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBufferString() <em>Buffer String</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBufferString()
	 * @generated
	 * @ordered
	 */
	protected String bufferString = BUFFER_STRING_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimingConditionsInRequirementsSatisfiable() <em>Timing Conditions In Requirements Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimingConditionsInRequirementsSatisfiable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimingConditionsInRequirementsSatisfiable() <em>Timing Conditions In Requirements Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimingConditionsInRequirementsSatisfiable()
	 * @generated
	 * @ordered
	 */
	protected boolean timingConditionsInRequirementsSatisfiable = TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimingConditionsInAssumptionsSatisfiable() <em>Timing Conditions In Assumptions Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimingConditionsInAssumptionsSatisfiable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimingConditionsInAssumptionsSatisfiable() <em>Timing Conditions In Assumptions Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimingConditionsInAssumptionsSatisfiable()
	 * @generated
	 * @ordered
	 */
	protected boolean timingConditionsInAssumptionsSatisfiable = TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimedSafetyViolationOccurredInRequirements() <em>Timed Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedSafetyViolationOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimedSafetyViolationOccurredInRequirements() <em>Timed Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedSafetyViolationOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected boolean timedSafetyViolationOccurredInRequirements = TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimedSafetyViolationOccurredInAssumptions() <em>Timed Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimedSafetyViolationOccurredInAssumptions() <em>Timed Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected boolean timedSafetyViolationOccurredInAssumptions = TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
	/**
	 * The default value of the '{@link #getGoodFederationString() <em>Good Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoodFederationString()
	 * @generated
	 * @ordered
	 */
	protected static final String GOOD_FEDERATION_STRING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getGoodFederationString() <em>Good Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoodFederationString()
	 * @generated
	 * @ordered
	 */
	protected String goodFederationString = GOOD_FEDERATION_STRING_EDEFAULT;
	/**
	 * The default value of the '{@link #getBadFederationString() <em>Bad Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBadFederationString()
	 * @generated
	 * @ordered
	 */
	protected static final String BAD_FEDERATION_STRING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBadFederationString() <em>Bad Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBadFederationString()
	 * @generated
	 * @ordered
	 */
	protected String badFederationString = BAD_FEDERATION_STRING_EDEFAULT;
	/**
	 * The default value of the '{@link #getWinningFederationString() <em>Winning Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWinningFederationString()
	 * @generated
	 * @ordered
	 */
	protected static final String WINNING_FEDERATION_STRING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getWinningFederationString() <em>Winning Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWinningFederationString()
	 * @generated
	 * @ordered
	 */
	protected String winningFederationString = WINNING_FEDERATION_STRING_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimeInconsistencyOccurredInRequirements() <em>Time Inconsistency Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimeInconsistencyOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimeInconsistencyOccurredInRequirements() <em>Time Inconsistency Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimeInconsistencyOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected boolean timeInconsistencyOccurredInRequirements = TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS_EDEFAULT;
	/**
	 * The default value of the '{@link #isTimeInconsistencyOccurredInAssumptions() <em>Time Inconsistency Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimeInconsistencyOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isTimeInconsistencyOccurredInAssumptions() <em>Time Inconsistency Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimeInconsistencyOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected boolean timeInconsistencyOccurredInAssumptions = TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
	/**
	 * The default value of the '{@link #isUpperBoundRealTimeInconsistency() <em>Upper Bound Real Time Inconsistency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpperBoundRealTimeInconsistency()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UPPER_BOUND_REAL_TIME_INCONSISTENCY_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isUpperBoundRealTimeInconsistency() <em>Upper Bound Real Time Inconsistency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpperBoundRealTimeInconsistency()
	 * @generated
	 * @ordered
	 */
	protected boolean upperBoundRealTimeInconsistency = UPPER_BOUND_REAL_TIME_INCONSISTENCY_EDEFAULT;
	private LinkedList<TimeConditionEvent> currentTimeConditionEvents;
	private Set<UDBMClock> resetClocks;
	private Map<List<EObject>, Queue<AsynchronousReceiveEvent>> senderAndReceiverToQueue;
	private Map<AsynchronousReceiveEvent, SimpleClockConstraint> earliestDelayMessageEventsToConstraints;
	private Map<TimeConditionEvent, SimpleClockConstraint> earliestDelayTimeConditionEventsToConstraints;

	private void computeEarliestDelayEvents() {
		earliestDelayMessageEventsToConstraints = new HashMap<>();
		earliestDelayTimeConditionEventsToConstraints = new HashMap<>();
		List<AsynchronousReceiveEvent> currentReceivedEvents = new LinkedList<>();
		for (Queue<AsynchronousReceiveEvent> receiveEventQueue : senderAndReceiverToQueue.values()) {
			if (receiveEventQueue.size() > 0) {
				AsynchronousReceiveEvent firstReceiveEvent = receiveEventQueue.peek();
				currentReceivedEvents.add(firstReceiveEvent);
			}
		}

		// TODO: refactor to avoid redundancy
		// filter out any events that will only occur after other events have to
		// have occurred before
		for (AsynchronousReceiveEvent event : currentReceivedEvents) {
			SimpleClockConstraint eventConstraint = getGeqLowerBound(event);
			if (eventConstraint.getClock() == null)
				continue;
			boolean amongEarliest = true;
			for (AsynchronousReceiveEvent otherEvent : currentReceivedEvents) {
				if (otherEvent != event) {
					SimpleClockConstraint otherEventConstraint = getGrUpperBound(otherEvent);
					if (otherEventConstraint.getClock() == null)
						continue;
					// check if another event must have been received before the
					// current one in any case
					if (isFulfilledBefore(otherEventConstraint, eventConstraint)) {
						amongEarliest = false;
						break;
					}
				}
			}
			for (TimeConditionEvent otherEvent : currentTimeConditionEvents) {
				if (!isDelayEvent(otherEvent))
					continue;
				SimpleClockConstraint otherEventConstraint = getConstraint(otherEvent);
				if (isFulfilledBefore(otherEventConstraint, eventConstraint)) {
					amongEarliest = false;
					break;
				}
			}
			if (amongEarliest) {
				earliestDelayMessageEventsToConstraints.put(event, getGrUpperBound(event));
			}
		}

		for (TimeConditionEvent event : currentTimeConditionEvents) {
			if (!isDelayEvent(event))
				continue;
			SimpleClockConstraint eventConstraint = getConstraint(event);

			boolean amongEarliest = true;
			for (AsynchronousReceiveEvent otherEvent : currentReceivedEvents) {
				SimpleClockConstraint otherEventConstraint = getGrUpperBound(otherEvent);
				if (otherEventConstraint.getClock() == null)
					continue;

				// check if another event must have been received before the
				// current one in any case
				if (isFulfilledBefore(otherEventConstraint, eventConstraint)) {
					amongEarliest = false;
					break;
				}
			}
			for (TimeConditionEvent otherEvent : currentTimeConditionEvents) {
				if (otherEvent != event) {
					if (!isDelayEvent(otherEvent))
						continue;
					SimpleClockConstraint otherEventConstraint = getConstraint(otherEvent);

					if (isFulfilledBefore(otherEventConstraint, eventConstraint)) {
						amongEarliest = false;
						break;
					}
				}
			}
			if (amongEarliest) {
				earliestDelayTimeConditionEventsToConstraints.put(event, eventConstraint);
			}
		}
	}

	public SimpleClockConstraint getConstraint(TimeConditionEvent event) {
		return getRuntimeUtil().parseClockConstraintForFragment(event.getClockCondition(), federation,
				event.getClockName());
	}

	private SimpleClockConstraint getGrUpperBound(AsynchronousReceiveEvent event) {
		Integer[] minMaxDelay = getRuntimeUtil().getMinMaxDelay(event);
		UDBMClock clock = federation.getFromClock(event.getClockName());
		return new SimpleClockConstraint(clock, RelationalOperator.GreaterOperator, minMaxDelay[1]);
	}

	private SimpleClockConstraint getGeqLowerBound(AsynchronousReceiveEvent event) {
		Integer[] minMaxDelay = getRuntimeUtil().getMinMaxDelay(event);
		UDBMClock clock = federation.getFromClock(event.getClockName());
		return new SimpleClockConstraint(clock, RelationalOperator.GreaterOrEqualOperator, minMaxDelay[0]);
	}

	private boolean isFulfilledBefore(SimpleClockConstraint c1, SimpleClockConstraint c2) {
		if (c1.getClock().getId() == c2.getClock().getId()) {
			return compareConstraintsFromIdenticalClocks(c1, c2);
		} else {
			return compareConstraintsFromDifferentClocks(c1, c2);
		}
	}

	private boolean compareConstraintsFromIdenticalClocks(SimpleClockConstraint c1, SimpleClockConstraint c2) {
		Federation testFed1 = (Federation) federation.clone();
		testFed1.up();
		Federation testFed2 = (Federation) testFed1.clone();
		testFed1.and(c1);
		testFed2.and(c2);
		if (testFed1.equals(testFed2))
			// / c1 and c2 are fulfilled at the same time
			return false;

		this.applyNegatedDelay(testFed2, c1);

		if (testFed2.isEmpty()) {
			// c1 is fulfilled before c2
			return true;
		}
		return false;
	}

	private boolean compareConstraintsFromDifferentClocks(SimpleClockConstraint c1, SimpleClockConstraint c2) {
		Federation federationCopy1 = (Federation) federation.clone();
		federationCopy1.up();

		Federation federationCopy2 = (Federation) federationCopy1.clone();
		federationCopy1.and(c1);
		federationCopy2.and(c2);

		SimpleClockConstraint clockFrom_c1 = (SimpleClockConstraint) (federationCopy1.getLowerBound(c1.getClock()));
		SimpleClockConstraint clockFrom_c2 = (SimpleClockConstraint) (federationCopy2.getLowerBound(c2.getClock()));

		if (clockFrom_c1.getValue() < clockFrom_c2.getValue()) {
			/*
			 * debug
			 * 
			 * String outputString = c1.toString() + " ### " + c2.toString();
			 * 
			 * if(c1.getClock().getName() != c2.getClock().getName()){
			 * JOptionPane.showMessageDialog(null, "TRUE --- " + outputString);
			 * }
			 */

			return true;
		} else {
			/*
			 * debug String outputString = c1.toString() + " ### " +
			 * c2.toString();
			 * 
			 * 
			 * if(c1.getClock().getName() != c2.getClock().getName()){
			 * JOptionPane.showMessageDialog(null, "FALSE --- " + outputString);
			 * }
			 */
			return false;
		}
	}

	/**
	 * @generated NOT
	 */
	public Map<List<EObject>, Queue<AsynchronousReceiveEvent>> getMessageQueuesForLinks() {
		return senderAndReceiverToQueue;
	}

	/**
	 * @generated NOT
	 */
	public LinkedList<TimeConditionEvent> getCurrentTimeConditionEvents() {
		return currentTimeConditionEvents;
	}

	/**
	 * @generated NOT
	 */
	private Federation federation;

	/**
	 * @generated NOT
	 */
	public Federation getFederation() {
		return federation;
	}

	/**
	 * @generated NOT
	 */
	public void setFederation(Federation federation) {
		if (federation != null){
			this.federation = federation;
			this.setFederationString(createFederationString());
			this.getStringToStringAnnotationMap().put("stateFederation", this.getFederationString());
		}
		
	}

	/**
	 * @generated NOT
	 */
	protected Set<UDBMClock> controllableClocks;

	/**
	 * @generated NOT
	 */
	public Set<UDBMClock> getControllableClocks() {
		return controllableClocks;
	}

	/**
	 * @generated NOT
	 */
	protected Map<UDBMClock, Set<EObject>> clockToLastResetObservers;

	/**
	 * @generated NOT
	 */
	public Map<UDBMClock, Set<EObject>> getClockToLastResetObservers() {
		return clockToLastResetObservers;
	}

	/**
	 * @generated NOT
	 */
	public Set<EObject> getLastResetObservers(UDBMClock clock) {
		return clockToLastResetObservers.get(clock);
	}

	/**
	 * @generated NOT
	 */
	public void addClockToLastResetObserver(UDBMClock clock, EObject lastResetObserver) {
		Set<EObject> observers = clockToLastResetObservers.get(clock);
		if (observers == null) {
			observers = new HashSet<EObject>();
			clockToLastResetObservers.put(clock, observers);
		}
		observers.add(lastResetObserver);
	}

	protected String createFederationString() {
		if (getStateGraph() != null) {
			Federation lookedUpFederation = ((TimedMSDRuntimeStateGraph) getStateGraph()).getFederation(federation);
			return lookedUpFederation.toString();
//			return ((TimedRuntimeUtil) getRuntimeUtil()).createFederationString(lookedUpFederation);
		}
		// + "DEBUG:\n\t"
		// + getClockToLastResetObservers().hashCode()
		// + "\\n"
		// + getControllableClocks().hashCode()
		// + "\\n"
		// + (testFed != null ? testFed.hashCode() + "\\n" : "")
		// + (senderAndReceiverToQueue != null ? senderAndReceiverToQueue
		// .hashCode() : "")
		// + "\\n"
		// + getActiveProcesses().hashCode();
		return "";
	}
	
	public String createFederationString(Federation federation) {
		return ((TimedRuntimeUtil) getRuntimeUtil()).createFederationString(federation);
	}

	@Override
	protected void performStepOnActiveProcesses(MessageEvent messageEvent) {
		setProcessesToCurrent();
		for (ActiveProcess process : new LinkedList<>(getActiveProcesses())) {
			if (process instanceof TimedActiveMSD) {
				TimedActiveMSD activeMSD = (TimedActiveMSD) process;
				if (!getRuntimeUtil().isEnforceEarliestDelaysFirst()
						&& activeMSD.isInHotCutDueToTimeConditionWithLowerBound()
						&& !activeMSD.getColdForbiddenEvents().values().contains(messageEvent)) {
					boolean isAssumptionMSD = RuntimeUtil.Helper.isEnvironmentAssumptionProcess(activeMSD);
					if (isAssumptionMSD)
						setSafetyViolationOccurredInAssumptions(true);
					else
						setSafetyViolationOccurredInRequirements(true);
					removeViolatedProcesses();
				}
			}
		}
		super.performStepOnActiveProcesses(messageEvent);
		performPostProcessingForStepOnActiveMSDs(new HashSet<>(getActiveProcesses()));
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void updateMSDModalMessageEvents(ExecutionSemantics executionSemantics) {

		// this.setFederation((Federation) getFederation().clone());
		HashSet<TimeConditionEvent> hiddenConditionEvents = new HashSet<>();
		HashSet<TimeConditionEvent> delayEvents = new HashSet<>();
		for (TimeConditionEvent event : new HashSet<>(currentTimeConditionEvents)) {
			if (isDelayEvent(event)) {
				if (!getRuntimeUtil().isEnforceEarliestDelaysFirst()
						|| earliestDelayTimeConditionEventsToConstraints.containsKey(event)){
					delayEvents.add(event);
				}
				else {
					currentTimeConditionEvents.remove(event);
				}
				
			} else {
				hiddenConditionEvents.add(event);
			}
		}

		if (hiddenConditionEvents.isEmpty()) {
			for (ActiveProcess activeMSD : getActiveProcesses()) {
				((TimedActiveMSD) activeMSD).setCurrentRuntimeState(this);
			}
			super.updateMSDModalMessageEvents(executionSemantics);

			// remove messages for full queues
			HashSet<MessageEvent> eventsToRemove = new HashSet<>();
			for (MessageEvent event : getMessageEventToModalMessageEventMap().keySet()) {
				if (event instanceof AsynchronousSendEvent) {
					if (getQueue(event).size() >= getRuntimeUtil().getBufferSize(event))
						eventsToRemove.add(event);
				}
			}
			for (MessageEvent eventToRemove : eventsToRemove)
				getMessageEventToModalMessageEventMap().removeKey(eventToRemove);

			if (senderAndReceiverToQueue != null) {
				// compute initial interval of next federation for testing if
				// min delay of each receivable event is feasible
				Federation testFed = (Federation) federation.clone();
				testFed.up();
				applyReceiveEventUpperBounds(testFed);
				for (TimeConditionEvent delayEvent : delayEvents) {
					this.applyTightestUpperBound(testFed, getRuntimeUtil().parseClockConstraintForFragment(
							delayEvent.getClockCondition(), testFed, delayEvent.getClockName()));
				}
				for (Queue<AsynchronousReceiveEvent> receiveEventQueue : senderAndReceiverToQueue.values()) {
					if (receiveEventQueue.size() > 0) {
						AsynchronousReceiveEvent firstReceiveEvent = receiveEventQueue.peek();
						if (!earliestDelayMessageEventsToConstraints.containsKey(firstReceiveEvent)
								|| firstReceiveEvent.getClockName() == null)
							continue;
						Federation eventTestFed = (Federation) testFed.clone();
						applyMinDelay(eventTestFed, firstReceiveEvent.getMinDelay(), firstReceiveEvent.getClockName());
						if (!eventTestFed.isEmpty()) {

							ModalMessageEvent modalMessageEvent = RuntimeFactory.eINSTANCE.createMSDModalMessageEvent();
							modalMessageEvent.setRepresentedMessageEvent(firstReceiveEvent);
							modalMessageEvent.setAssumptionsModality(RuntimeFactory.eINSTANCE.createMSDModality());
							modalMessageEvent.setRequirementsModality(RuntimeFactory.eINSTANCE.createMSDModality());
							modalMessageEvent.getAssumptionsModality().setMandatory(true);
							// if (!getObjectSystem().isEnvironmentMessageEvent(
							// firstReceiveEvent))
							// modalMessageEvent.getRequirementsModality()
							// .setMandatory(true);
							getMessageEventToModalMessageEventMap().put(firstReceiveEvent, modalMessageEvent);
						}
					}
				}
			}

			// Set<MessageEvent> oldMessageEvents =
			// getMessageEventToModalMessageEventMap()
			// .keySet();
			// for (MessageEvent event : oldMessageEvents) {
			// Integer[] minMaxDelay = getRuntimeUtil().getMinMaxDelay(event);
			// if ((minMaxDelay[0] != 0 || minMaxDelay[1] != 0)
			// && !earliestDelayMessageEvents.contains(event)) {
			// getMessageEventToModalMessageEventMap().removeKey(event);
			// }
			// }
		} else {
			currentTimeConditionEvents.removeAll(delayEvents);
			getMessageEventToModalMessageEventMap().clear();
		}
		this.setFederationString(createFederationString());
	}

	private void updateTimeEvents() {
		currentTimeConditionEvents.clear();

		for (ActiveProcess activeMSD : getActiveProcesses()) {
			if (activeMSD instanceof TimedActiveMSD) {
				TimedActiveMSD activeTimedMSD = (TimedActiveMSD) activeMSD;
				activeTimedMSD.setCurrentRuntimeState(this);
				activeTimedMSD.calculateRelevantTimeConditionEvents();
				activeTimedMSD.setCurrentRuntimeState(null);
				currentTimeConditionEvents.addAll(activeTimedMSD.getCurrentTimeConditionEvents());
			}
		}
		setFederation(getFederation());
	}

	/**
	 * @generated NOT
	 */
	public boolean isDelayEvent(TimeConditionEvent event) {
		return event.getOperatorString().contains(">") && event.isHot();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public TimedMSDRuntimeStateImpl() {
		super();
		currentTimeConditionEvents = new LinkedList<TimeConditionEvent>();
		resetClocks = new HashSet<UDBMClock>();
		clockToLastResetObservers = new HashMap<UDBMClock, Set<EObject>>();
		controllableClocks = new HashSet<UDBMClock>();
	}

	/**
	 * @generated NOT
	 */
	public void initQueue() {
		senderAndReceiverToQueue = new HashMap<>();
		for (List<EObject> comPair : getRuntimeUtil().getAsynchronousSendersAndReceivers()) {
			senderAndReceiverToQueue.put(comPair, new LinkedList<AsynchronousReceiveEvent>());
		}
		updateBufferString();
	}

	/**
	 * @generated NOT
	 */
	@Override
	public TimedRuntimeUtil getRuntimeUtil() {
		return (TimedRuntimeUtil) super.getRuntimeUtil();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getFederationString() {
		return federationString;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFederationString(String newFederationString) {
		String oldFederationString = federationString;
		federationString = newFederationString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING, oldFederationString, federationString));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getBufferString() {
		return bufferString;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setBufferString(String newBufferString) {
		String oldBufferString = bufferString;
		bufferString = newBufferString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING, oldBufferString, bufferString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimingConditionsInRequirementsSatisfiable() {
		return timingConditionsInRequirementsSatisfiable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimingConditionsInRequirementsSatisfiable(boolean newTimingConditionsInRequirementsSatisfiable) {
		boolean oldTimingConditionsInRequirementsSatisfiable = timingConditionsInRequirementsSatisfiable;
		timingConditionsInRequirementsSatisfiable = newTimingConditionsInRequirementsSatisfiable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE, oldTimingConditionsInRequirementsSatisfiable, timingConditionsInRequirementsSatisfiable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimingConditionsInAssumptionsSatisfiable() {
		return timingConditionsInAssumptionsSatisfiable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimingConditionsInAssumptionsSatisfiable(boolean newTimingConditionsInAssumptionsSatisfiable) {
		boolean oldTimingConditionsInAssumptionsSatisfiable = timingConditionsInAssumptionsSatisfiable;
		timingConditionsInAssumptionsSatisfiable = newTimingConditionsInAssumptionsSatisfiable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE, oldTimingConditionsInAssumptionsSatisfiable, timingConditionsInAssumptionsSatisfiable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimedSafetyViolationOccurredInRequirements() {
		return timedSafetyViolationOccurredInRequirements;
	}

	public void setTimedSafetyViolationOccurredInRequirements(boolean newTimedSafetyViolationOccurredInRequirements) {
		if (this.isTimedSafetyViolationOccurredInAssumptions()){
			return;
		}
		boolean oldTimedSafetyViolationOccurredInRequirements = timedSafetyViolationOccurredInRequirements;
		timedSafetyViolationOccurredInRequirements = newTimedSafetyViolationOccurredInRequirements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS, oldTimedSafetyViolationOccurredInRequirements, timedSafetyViolationOccurredInRequirements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimedSafetyViolationOccurredInAssumptions() {
		return timedSafetyViolationOccurredInAssumptions;
	}

	public void setTimedSafetyViolationOccurredInAssumptions(boolean newTimedSafetyViolationOccurredInAssumptions) {
		if (newTimedSafetyViolationOccurredInAssumptions == true){
			this.setTimedSafetyViolationOccurredInRequirements(false);
		}
		boolean oldTimedSafetyViolationOccurredInAssumptions = timedSafetyViolationOccurredInAssumptions;
		timedSafetyViolationOccurredInAssumptions = newTimedSafetyViolationOccurredInAssumptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS, oldTimedSafetyViolationOccurredInAssumptions, timedSafetyViolationOccurredInAssumptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGoodFederationString() {
		return goodFederationString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGoodFederationString(String newGoodFederationString) {
		String oldGoodFederationString = goodFederationString;
		goodFederationString = newGoodFederationString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING, oldGoodFederationString, goodFederationString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBadFederationString() {
		return badFederationString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBadFederationString(String newBadFederationString) {
		String oldBadFederationString = badFederationString;
		badFederationString = newBadFederationString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING, oldBadFederationString, badFederationString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWinningFederationString() {
		return winningFederationString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWinningFederationString(String newWinningFederationString) {
		String oldWinningFederationString = winningFederationString;
		winningFederationString = newWinningFederationString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING, oldWinningFederationString, winningFederationString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimeInconsistencyOccurredInRequirements() {
		return timeInconsistencyOccurredInRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeInconsistencyOccurredInRequirements(boolean newTimeInconsistencyOccurredInRequirements) {
		boolean oldTimeInconsistencyOccurredInRequirements = timeInconsistencyOccurredInRequirements;
		timeInconsistencyOccurredInRequirements = newTimeInconsistencyOccurredInRequirements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS, oldTimeInconsistencyOccurredInRequirements, timeInconsistencyOccurredInRequirements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimeInconsistencyOccurredInAssumptions() {
		return timeInconsistencyOccurredInAssumptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeInconsistencyOccurredInAssumptions(boolean newTimeInconsistencyOccurredInAssumptions) {
		boolean oldTimeInconsistencyOccurredInAssumptions = timeInconsistencyOccurredInAssumptions;
		timeInconsistencyOccurredInAssumptions = newTimeInconsistencyOccurredInAssumptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS, oldTimeInconsistencyOccurredInAssumptions, timeInconsistencyOccurredInAssumptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUpperBoundRealTimeInconsistency() {
		return upperBoundRealTimeInconsistency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperBoundRealTimeInconsistency(boolean newUpperBoundRealTimeInconsistency) {
		boolean oldUpperBoundRealTimeInconsistency = upperBoundRealTimeInconsistency;
		upperBoundRealTimeInconsistency = newUpperBoundRealTimeInconsistency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY, oldUpperBoundRealTimeInconsistency, upperBoundRealTimeInconsistency));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING:
				return getFederationString();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING:
				return getBufferString();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE:
				return isTimingConditionsInRequirementsSatisfiable();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE:
				return isTimingConditionsInAssumptionsSatisfiable();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				return isTimedSafetyViolationOccurredInRequirements();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return isTimedSafetyViolationOccurredInAssumptions();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING:
				return getGoodFederationString();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING:
				return getBadFederationString();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING:
				return getWinningFederationString();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS:
				return isTimeInconsistencyOccurredInRequirements();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS:
				return isTimeInconsistencyOccurredInAssumptions();
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY:
				return isUpperBoundRealTimeInconsistency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING:
				setFederationString((String)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING:
				setBufferString((String)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE:
				setTimingConditionsInRequirementsSatisfiable((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE:
				setTimingConditionsInAssumptionsSatisfiable((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				setTimedSafetyViolationOccurredInRequirements((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setTimedSafetyViolationOccurredInAssumptions((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING:
				setGoodFederationString((String)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING:
				setBadFederationString((String)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING:
				setWinningFederationString((String)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS:
				setTimeInconsistencyOccurredInRequirements((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS:
				setTimeInconsistencyOccurredInAssumptions((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY:
				setUpperBoundRealTimeInconsistency((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING:
				setFederationString(FEDERATION_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING:
				setBufferString(BUFFER_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE:
				setTimingConditionsInRequirementsSatisfiable(TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE:
				setTimingConditionsInAssumptionsSatisfiable(TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				setTimedSafetyViolationOccurredInRequirements(TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setTimedSafetyViolationOccurredInAssumptions(TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING:
				setGoodFederationString(GOOD_FEDERATION_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING:
				setBadFederationString(BAD_FEDERATION_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING:
				setWinningFederationString(WINNING_FEDERATION_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS:
				setTimeInconsistencyOccurredInRequirements(TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS:
				setTimeInconsistencyOccurredInAssumptions(TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY:
				setUpperBoundRealTimeInconsistency(UPPER_BOUND_REAL_TIME_INCONSISTENCY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING:
				return FEDERATION_STRING_EDEFAULT == null ? federationString != null : !FEDERATION_STRING_EDEFAULT.equals(federationString);
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BUFFER_STRING:
				return BUFFER_STRING_EDEFAULT == null ? bufferString != null : !BUFFER_STRING_EDEFAULT.equals(bufferString);
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE:
				return timingConditionsInRequirementsSatisfiable != TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE:
				return timingConditionsInAssumptionsSatisfiable != TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				return timedSafetyViolationOccurredInRequirements != TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return timedSafetyViolationOccurredInAssumptions != TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING:
				return GOOD_FEDERATION_STRING_EDEFAULT == null ? goodFederationString != null : !GOOD_FEDERATION_STRING_EDEFAULT.equals(goodFederationString);
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING:
				return BAD_FEDERATION_STRING_EDEFAULT == null ? badFederationString != null : !BAD_FEDERATION_STRING_EDEFAULT.equals(badFederationString);
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING:
				return WINNING_FEDERATION_STRING_EDEFAULT == null ? winningFederationString != null : !WINNING_FEDERATION_STRING_EDEFAULT.equals(winningFederationString);
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS:
				return timeInconsistencyOccurredInRequirements != TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS:
				return timeInconsistencyOccurredInAssumptions != TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY:
				return upperBoundRealTimeInconsistency != UPPER_BOUND_REAL_TIME_INCONSISTENCY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (federationString: ");
		result.append(federationString);
		result.append(", bufferString: ");
		result.append(bufferString);
		result.append(", timingConditionsInRequirementsSatisfiable: ");
		result.append(timingConditionsInRequirementsSatisfiable);
		result.append(", timingConditionsInAssumptionsSatisfiable: ");
		result.append(timingConditionsInAssumptionsSatisfiable);
		result.append(", timedSafetyViolationOccurredInRequirements: ");
		result.append(timedSafetyViolationOccurredInRequirements);
		result.append(", timedSafetyViolationOccurredInAssumptions: ");
		result.append(timedSafetyViolationOccurredInAssumptions);
		result.append(", goodFederationString: ");
		result.append(goodFederationString);
		result.append(", badFederationString: ");
		result.append(badFederationString);
		result.append(", winningFederationString: ");
		result.append(winningFederationString);
		result.append(", timeInconsistencyOccurredInRequirements: ");
		result.append(timeInconsistencyOccurredInRequirements);
		result.append(", timeInconsistencyOccurredInAssumptions: ");
		result.append(timeInconsistencyOccurredInAssumptions);
		result.append(", upperBoundRealTimeInconsistency: ");
		result.append(upperBoundRealTimeInconsistency);
		result.append(')');
		return result.toString();
	}

	protected void setProcessesToCurrent() {
		for (ActiveProcess process : getActiveProcesses()) {
			if (process instanceof TimedActiveMSD) {
				((TimedActiveMSD) process).setCurrentRuntimeState(this);
			}
		}
	}

	private void wait(int min, int max) {
		if (federation.sizeOfClockZone() > 0) {
			ClockZone zone = federation.iteratorOfClockZone().next();
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext()) {
				ClockConstraint constr = ccIt.next();
				if (constr instanceof SimpleClockConstraint) {
					SimpleClockConstraint simpleCC = (SimpleClockConstraint) constr;
					// add min to lower bounds
					if (simpleCC.getRelationalOperator().toString().contains(">")
							|| simpleCC.getRelationalOperator().toString().equals("==")) {
						simpleCC.setValue(simpleCC.getValue() + min);
						federation.and(simpleCC);
					}
					// add max to upper bounds
					if (simpleCC.getRelationalOperator().toString().contains("<")
							|| simpleCC.getRelationalOperator().toString().equals("==")) {
						simpleCC.setValue(simpleCC.getValue() + max);
						federation.and(simpleCC);
					}
				}
			}
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void performStep(Event event) {
		if (event == null || federation == null){
			return;
		}
		
		setProcessesToCurrent();
		if (event instanceof MessageEvent) {
			federation.up();
			TimedRuntimeUtil timedUtil = (TimedRuntimeUtil) getRuntimeUtil();
			Integer[] minMaxDelay = timedUtil.getMinMaxDelay(((MessageEvent) event).getSendingObject(),
					((MessageEvent) event).getReceivingObject());
			UDBMClock eventClock = null;
			if (event instanceof AsynchronousSendEvent) {
				if (minMaxDelay[0] != 0 || minMaxDelay[1] != 0) {
					eventClock = createAsyncEventClock((AsynchronousMessageEvent) event);
				}
			}
			if (event instanceof AsynchronousReceiveEvent) {
				if (minMaxDelay[0] != 0)
					applyMinDelay(minMaxDelay[0], ((AsynchronousReceiveEvent) event).getClockName());
			}
			applyReceiveEventUpperBounds(federation);
			if (event instanceof SynchronousMessageEvent) {
				// modify federation to take into account delays of
				// communication
				// links
				if (minMaxDelay[0] != 0 || minMaxDelay[1] != 0) {
					wait(minMaxDelay[0], minMaxDelay[1]);
				}
			}

			updateTimeEvents();
			if (getRuntimeUtil().isEnforceEarliestDelaysFirst()) {
				computeEarliestDelayEvents();
				// SergejJ: auskommentiert
				/*
				 * System.out.println("state " +
				 * getStringToStringAnnotationMap().get("passedIndex"));
				 * System.out.println("federation " + federation); System.out
				 * .println("earliestDelayTimeConditionEventsToConstraints:" +
				 * earliestDelayTimeConditionEventsToConstraints .keySet());
				 * System.out.println("earliestDelayMessageEventsToConstraints:"
				 * + earliestDelayMessageEventsToConstraints.keySet());
				 */

				for (TimeConditionEvent delayEvent : earliestDelayTimeConditionEventsToConstraints.keySet()) {
					SimpleClockConstraint constraint = getConstraint(delayEvent);
					this.applyTightestUpperBound(federation, constraint);
					// applyNegatedDelay(constraint);
				}
			}
			if (event instanceof TimeRestrictedMessageEvent) {
				TimeRestrictedMessageEvent restrictedEvent = (TimeRestrictedMessageEvent) event;
				if (restrictedEvent.getClockName() != null) {
					federation.up();
					UDBMClock clock = federation.getFromClock(restrictedEvent.getClockName());
					if (restrictedEvent.getMinBound() > 0 || !restrictedEvent.isExcludeMinBound()) {
						RelationalOperator lbOperator = restrictedEvent.isExcludeMinBound()
								? RelationalOperator.GreaterOperator : RelationalOperator.GreaterOrEqualOperator;
						SimpleClockConstraint lowerBound = new SimpleClockConstraint(clock, lbOperator,
								restrictedEvent.getMinBound());
						federation.and(lowerBound);
					}
					if (restrictedEvent.getMaxBound() > 0 || !restrictedEvent.isExcludeMaxBound()) {
						RelationalOperator ubOperator = restrictedEvent.isExcludeMaxBound()
								? RelationalOperator.LessOperator : RelationalOperator.LessOrEqualOperator;
						SimpleClockConstraint upperBound = new SimpleClockConstraint(clock, ubOperator,
								restrictedEvent.getMaxBound());
						federation.and(upperBound);
					}
				}
			}
			super.performStep(event);
			if (event instanceof AsynchronousReceiveEvent) {
				getQueue((MessageEvent) event).remove();
				if (minMaxDelay[0] != 0 || minMaxDelay[1] != 0) {
					removeClock(((AsynchronousReceiveEvent) event).getClockName());
				}
			}
			if (event instanceof AsynchronousSendEvent) {
				AsynchronousSendEvent asyncSendEvent = (AsynchronousSendEvent) event;
				Object param = null;
				if (asyncSendEvent.isSetBooleanParameterValue()) {
					param = asyncSendEvent.isBooleanParameterValue();
				} else if (asyncSendEvent.isSetStringParameterValue()) {
					param = asyncSendEvent.getStringParameterValue();
				} else if (asyncSendEvent.isSetIntegerParameterValue()) {
					param = asyncSendEvent.getIntegerParameterValue();
				} else if (asyncSendEvent.isSetEObjectParameterValue()) {
					param = asyncSendEvent.getEObjectParameterValue();
				}
				AsynchronousReceiveEvent receiveEvent = (AsynchronousReceiveEvent) ((MSDObjectSystem) getObjectSystem())
						.getMessageEvent(asyncSendEvent.getOperation(),
								((AsynchronousSendEvent) event).getSideEffectOnEStructuralFeature(),
								asyncSendEvent.getSendingObject(), asyncSendEvent.getReceivingObject(), param,
								TimedRuntimeFactory.eINSTANCE.createAsynchronousReceiveEvent());
				receiveEvent.setMinDelay(minMaxDelay[0]);
				receiveEvent.setMaxDelay(minMaxDelay[1]);
				receiveEvent.getProgressedActiveMSDs().addAll(asyncSendEvent.getProgressedActiveMSDs());
				receiveEvent.getProgressedMessages().addAll(asyncSendEvent.getProgressedMessages());
				getQueue(asyncSendEvent).add(receiveEvent);
				if (minMaxDelay[0] != 0 || minMaxDelay[1] != 0) {
					receiveEvent.setClockName(eventClock.getName());
				}
			}
		} else if (event instanceof TimeConditionEvent) {
			handleTimeConditionEvent((TimeConditionEvent) event);
		}
		// in these cases it makes no sense to progress at all because the
		// system
		// or the environment has lost for sure.
		if (isSafetyViolationOccurredInAssumptions()
				|| isSafetyViolationOccurredInRequirements() && !hasActiveAssumptionProcesses()) { // this
																									// may
																									// be
																									// a
																									// bit
																									// too
																									// optimistic
		}

		// update list of clocks that can be controlled
		if (event instanceof MessageEvent) {
			final MessageEvent messageEvent = (MessageEvent) event;

			Set<UDBMClock> currentClocks = new HashSet<>();
			Iterator<? extends UDBMClock> clockIt = federation.iteratorOfClock();
			while (clockIt.hasNext())
				currentClocks.add(clockIt.next());
			controllableClocks.retainAll(currentClocks);
			clockToLastResetObservers.keySet().retainAll(currentClocks);

			// all observable clocks with a fixed value are controllable
			for (UDBMClock clock : clockToLastResetObservers.keySet()) {
				if (clockToLastResetObservers.get(clock).contains(messageEvent.getReceivingObject())) {
					ClockConstraint up = federation.getUpperBound(clock);
					if (up instanceof SimpleClockConstraint) {
						SimpleClockConstraint sUp = (SimpleClockConstraint) up;
						if (sUp.getValue() == 0) // upper bound of 0 is always
													// exact
							controllableClocks.add(clock);
						else {
							ClockConstraint low = federation.getLowerBound(clock);
							if (low instanceof SimpleClockConstraint) {
								SimpleClockConstraint sLow = (SimpleClockConstraint) low;
								if (sUp.getValue() == sLow.getValue())
									controllableClocks.add(clock);
							}
						}
					}
				}
			}
		}

		// update clock reset observers
		for (UDBMClock clock : getResetClocks()) {
			Set<EObject> observers = getClockToLastResetObservers().get(clock);
			if (observers != null)
				observers.clear();
			controllableClocks.remove(clock);
		}

		if (event instanceof MessageEvent) {
			final MessageEvent messageEvent = (MessageEvent) event;

			// update clock reset observers
			for (UDBMClock clock : getResetClocks()) {
				addClockToLastResetObserver(clock, messageEvent.getSendingObject());
				addClockToLastResetObserver(clock, messageEvent.getReceivingObject());
			}

			for (UDBMClock clock : clockToLastResetObservers.keySet()) {
				if (clockToLastResetObservers.get(clock).contains(messageEvent.getSendingObject()))
					controllableClocks.add(clock);
			}
		} else {
			final TimeConditionEvent timeCondEvent = (TimeConditionEvent) event;
			for (EObject oldObserver : timeCondEvent.getClockObservers())
				for (UDBMClock clock : getResetClocks()) {
					addClockToLastResetObserver(clock, oldObserver);
				}
		}

		updateTimeEvents();

		// set controllability and visibility for time condition events
		for (TimeConditionEvent timeConditionEvent : getCurrentTimeConditionEvents()) {
			UDBMClock clock = federation.getFromClock(timeConditionEvent.getClockName());
			timeConditionEvent.setTimedControllable(getControllableClocks().contains(clock));
			if (getLastResetObservers(clock) != null)
				timeConditionEvent.getClockObservers().addAll(getLastResetObservers(clock));
		}

		// make timed delay transitions uncontrollable if there are
		// uncontrollable message events - except if assumptions violating
		// or asynchronously receiving
		for (MessageEvent messageEvent : getMessageEventToModalMessageEventMap().keySet()) {
			if (messageEvent instanceof SynchronousMessageEvent || messageEvent instanceof AsynchronousSendEvent) {
				boolean controllable = getObjectSystem().isControllable(messageEvent.getSendingObject());

				if (!controllable && !(getMessageEventToModalMessageEventMap().get(messageEvent)
						.getAssumptionsModality().isSafetyViolating())) {
					for (TimeConditionEvent timeConditionEvent : getCurrentTimeConditionEvents()) {
						if (isDelayEvent(timeConditionEvent))
							timeConditionEvent.setTimedControllable(false);
					}
					break;
				}
			}
		}

		if (getRuntimeUtil().isEnforceEarliestDelaysFirst()) {
			computeEarliestDelayEvents();
			System.out.println("state " + getStringToStringAnnotationMap().get("passedIndex"));
//			System.out.println("federation " + federation);
//			System.out.println("earliestDelayTimeConditionEventsToConstraints:"
//					+ earliestDelayTimeConditionEventsToConstraints.keySet());
//			System.out.println(
//					"earliestDelayMessageEventsToConstraints:" + earliestDelayMessageEventsToConstraints.keySet());

			for (TimeConditionEvent delayEvent : earliestDelayTimeConditionEventsToConstraints.keySet()) {

				SimpleClockConstraint constraint = getConstraint(delayEvent);

				if (isDelayEvent(delayEvent)) {
					this.applyTightestUpperBound(federation, constraint);
				}
			}
		}
		// for (AsynchronousReceiveEvent receiveEvent :
		// earliestDelayMessageEventsToConstraints
		// .keySet()) {
		// SimpleClockConstraint constraint = getGrUpperBound(receiveEvent);
		// applyNegatedDelay(constraint);
		// }
		assert (federation.getClockZone().size() == 1);
		setFederation(federation);
		updateBufferString();

	}

	private void applyTightestUpperBound(Federation federation, SimpleClockConstraint constraint) {
		SimpleClockConstraint tightestUB = getNegatedConstraint(constraint);
		// Determine tightest upper bound by converting (<=, value) to (<, value
		// + 1)
		if (tightestUB.getRelationalOperator().equals(RelationalOperator.LessOrEqualOperator))
			tightestUB = new SimpleClockConstraint(tightestUB.getClock(), RelationalOperator.LessOperator,
					tightestUB.getValue() + 1);
		else
			tightestUB = new SimpleClockConstraint(tightestUB.getClock(), RelationalOperator.LessOrEqualOperator,
					tightestUB.getValue());
		federation.and(tightestUB);
	}

	public Set<UDBMClock> getCurrentClocks() {
		Iterator<? extends UDBMClock> clockIt = federation.iteratorOfClock();
		Set<UDBMClock> clocks = new HashSet<>();
		while (clockIt.hasNext())
			clocks.add(clockIt.next());
		return clocks;
	}

	private void applyReceiveEventUpperBounds(Federation federation) {
		for (Queue<AsynchronousReceiveEvent> queue : senderAndReceiverToQueue.values()) {
			AsynchronousReceiveEvent firstEvent = queue.peek();
			if (firstEvent != null) {
				Integer[] firstEventMinMaxDelay = getRuntimeUtil().getMinMaxDelay(firstEvent.getSendingObject(),
						firstEvent.getReceivingObject());
				if (firstEventMinMaxDelay[1] != 0)
					applyMaxDelay(federation, firstEventMinMaxDelay[1], firstEvent.getClockName());
			}
		}
	}

	private int getFirstFreeInstanceNumberForClock(String clockQualifiedName) {
		Set<Integer> usedNumbers = new HashSet<>();
		@SuppressWarnings("unchecked")
		Iterator<UDBMClock> clockIt = (Iterator<UDBMClock>) federation.iteratorOfClock();
		while (clockIt.hasNext()) {
			UDBMClock clock = clockIt.next();
			String[] clockNameSegments = clock.getName().split("_");
			if (clockNameSegments[0].equals(clockQualifiedName)) {
				int usedNum = Integer.parseInt(clockNameSegments[1]);
				usedNumbers.add(usedNum);
			}
		}

		int result = 0;
		while (true) {
			if (!usedNumbers.contains(result))
				return result;
			result++;
		}
	}

	private void removeClock(String removedClockInstanceName) {
//		System.out.println("removing clock " + removedClockInstanceName);
		UDBMClock removedClock = federation.getFromClock(removedClockInstanceName);

		HashSet<UDBMClock> clocks = new HashSet<UDBMClock>();
		Iterator<? extends UDBMClock> it = federation.iteratorOfClock();
		while (it.hasNext()) {
			UDBMClock clock = it.next();
			if (!"zeroclock".equals(clock.getName()) && clock != removedClock)
				clocks.add(clock);
		}

		HashSet<ClockConstraint> constraints = new HashSet<ClockConstraint>();

		for (ClockZone zone : federation.getClockZone()) {
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext()) {
				ClockConstraint constraint = (ClockConstraint) ccIt.next().clone();
				if (constraint instanceof SimpleClockConstraint) {
					if (((SimpleClockConstraint) constraint).getClock() != removedClock)
						constraints.add(constraint);
				} else if (constraint instanceof DifferenceClockConstraint) {
					DifferenceClockConstraint diffConstraint = ((DifferenceClockConstraint) constraint);
					if (diffConstraint.getClockMinuend() != removedClock
							&& diffConstraint.getClockSubtrahend() != removedClock)
						constraints.add(constraint);
				}
			}
		}

//		System.out.println("fed before removal: " + federation);
		federation = getRuntimeUtil().getFederationFactory().createFederation(clocks, constraints);
		setFederation(federation);
//		System.out.println("fed after removal: " + federation);
	}

	private UDBMClock createAsyncEventClock(AsynchronousMessageEvent event) {
		String clockQualifiedName = getConnectionString(event.getSendingObject(), event.getReceivingObject());
		int instanceNumber = getFirstFreeInstanceNumberForClock(clockQualifiedName);
		String clockInstanceName = clockQualifiedName + "_" + instanceNumber;

		UDBMClock newClock = getRuntimeUtil().getClockInstanceForName(clockInstanceName);
		HashSet<UDBMClock> newFederationClocks = new HashSet<UDBMClock>();
		Iterator<? extends UDBMClock> clockIt = federation.iteratorOfClock();
		while (clockIt.hasNext()) {
			UDBMClock clock = clockIt.next();
			if (!"zeroclock".equals(clock.getName()))
				newFederationClocks.add(clock);
		}
		newFederationClocks.add(newClock);

		HashSet<ClockConstraint> constraints = new HashSet<ClockConstraint>();
		for (ClockZone zone : federation.getClockZone()) {
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext())
				constraints.add((ClockConstraint) ccIt.next().clone());
		}

		federation = getRuntimeUtil().getFederationFactory().createFederation(newFederationClocks, constraints);
		HashSet<UDBMClock> clocksToReset = new HashSet<UDBMClock>();
		clocksToReset.add(newClock);
		federation.applyResets(clocksToReset);

		// addClockToLastResetObserver(newClock, event.getSendingObject());
		// addClockToLastResetObserver(newClock, event.getReceivingObject());
		addResetClock(newClock);
		return newClock;
	}

	private void applyMaxDelay(Federation federation, Integer integer, String clockName) {
		UDBMClock clock = federation.getFromClock(clockName);
		SimpleClockConstraint constraint = new SimpleClockConstraint(clock, RelationalOperator.LessOrEqualOperator,
				integer);
		federation.and(constraint);
	}

	private void applyMinDelay(Integer integer, String clockName) {
		applyMinDelay(federation, integer, clockName);
	}

	private void applyMinDelay(Federation federation, Integer integer, String clockName) {
		UDBMClock clock = federation.getFromClock(clockName);
		SimpleClockConstraint constraint = new SimpleClockConstraint(clock, RelationalOperator.GreaterOrEqualOperator,
				integer);
		federation.and(constraint);
	}

	private void handleTimeConditionEvent(TimeConditionEvent event) {
		 federation.up();
		ElementContainer container = ((TimedMSDRuntimeStateGraph) getStateGraph()).getElementContainer();
		for (ActiveProcess process : this.getActiveProcesses()) {
			ActiveState lookedUpState1 = container.getActiveState((ActiveMSD) process, (ActiveMSDCut) process.getCurrentState());
			ActiveState lookedUpState2 = container.getActiveState((ActiveMSD) process, (ActiveMSDCut) event.getActiveMSDCut());

			if (lookedUpState1 == lookedUpState2) {
				SimpleClockConstraint constraint = ((TimedRuntimeUtil) getRuntimeUtil())
						.parseClockConstraintForFragment(event.getClockCondition(), federation,
								(TimedActiveMSD) process);

				if (isDelayEvent(event)) {
					Federation testFed = (Federation) federation.clone();
					testFed.and(constraint);
					if (testFed.isEmpty()) {
						federation.up();
						federation.and(constraint);
						this.applyTightestUpperBound(federation, constraint);
					} else {
						federation.and(constraint);
					}
				} else {
					if (event.isConstraintFulfilled()) {
						try {
							federation.and(constraint);
						} catch (Exception e) {
						}
					} else {
						this.applyNegatedDelay(federation, constraint);
					}
				}
				performPostProcessingAfterTimeEvent(process);
				return;
			}
		}
	}

	protected void performPostProcessingAfterTimeEvent(ActiveProcess process) {
		Set<ActiveProcess> activeProcessesNeedingFurtherProcessing = new HashSet<>();
		activeProcessesNeedingFurtherProcessing.add(process);
		super.performPostProcessingForStepOnActiveMSDs(activeProcessesNeedingFurtherProcessing);
		performPostProcessingForStepOnActiveMSDs(new HashSet<ActiveProcess>(getActiveProcesses()));
		// in these cases it makes no sense to progress at all because the
		// system
		// or the environment has lost for sure.
		if (isSafetyViolationOccurredInAssumptions()
				|| isSafetyViolationOccurredInRequirements() && !hasActiveAssumptionProcesses()) { // this
																									// may
																									// be
																									// a
																									// bit
																									// too
																									// optimistic
//			removeAllProcesses();
		}
	}
	
	protected void performPostProcessingForStepOnActiveMSDs(
			Set<ActiveProcess> activeProcessesNeedingFurtherProcessing) {
		// TODO:optimize
		// always postprocess all active MSDs to handle time conditions
		activeProcessesNeedingFurtherProcessing = new HashSet<ActiveProcess>(getActiveProcesses());
		
		EList<ActiveProcess> activeProcessesWhereRevelantEventsNeedRecalculation = new BasicEList<ActiveProcess>(
				activeProcessesNeedingFurtherProcessing);
		// 4) Progress hidden events and evaluate lifeline bindings
		while (!activeProcessesNeedingFurtherProcessing.isEmpty()) {
			ActiveProcess activeProcess = activeProcessesNeedingFurtherProcessing
					.iterator().next();
			boolean nextActiveProcessNeedsFurtherProcessing = false;

			// progress a hidden event. If this changed the cut (the method then
			// returns true), the cut needs to be considered in another
			// iteration of this loop.
			ActiveMSDProgress activeMSDProgress = activeProcess
					.progressEnabledHiddenEvents();
			if (activeMSDProgress == ActiveMSDProgress.PROGRESS) {
				nextActiveProcessNeedsFurtherProcessing = true;
			} else if (activeMSDProgress == ActiveMSDProgress.COLD_VIOLATION) {
				handleColdViolations(null, activeProcess, null);
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.SAFETY_VIOLATION) {
				if (RuntimeUtil.Helper
						.isEnvironmentAssumptionProcess(activeProcess)) {
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					setSafetyViolationOccurredInRequirements(true);
				}
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.TIMED_COLD_VIOLATION) {
				handleColdViolations(null, activeProcess, null);
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.TIMED_SAFETY_VIOLATION) {
				if (RuntimeUtil.Helper
						.isEnvironmentAssumptionProcess(activeProcess)) {
					this.setTimedSafetyViolationOccurredInAssumptions(true);
				} else {
					if (!this.isTimedSafetyViolationOccurredInAssumptions()){
						this.setTimedSafetyViolationOccurredInRequirements(true);
					}
				}
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			}
			else if (activeMSDProgress == ActiveMSDProgress.NO_PROGRESS) {
				// do nothing.
			} else if (activeMSDProgress == ActiveMSDProgress.TERMINAL_CUT_REACHED) {
				disposeState(activeProcess, null);
			}

			if (activeMSDProgress != ActiveMSDProgress.TIMED_COLD_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.TIMED_SAFETY_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.COLD_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.SAFETY_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.TERMINAL_CUT_REACHED) {
				if (activeProcess instanceof ActiveMSD) {
					// returns a list, because binding lifelines may lead to a
					// multiplication of cuts if multiple candidate objects for
					// a
					// lifeline binding exist.
					EList<ActiveMSD> activeMSDsWhereAdditionalLifelinesWereBound = evaluateNextLifelineBindingExpression((ActiveMSD) activeProcess);

					// if the returned list is empty...
					if (activeMSDsWhereAdditionalLifelinesWereBound == null
							|| activeMSDsWhereAdditionalLifelinesWereBound
									.isEmpty())
						nextActiveProcessNeedsFurtherProcessing = false;
					else {
						activeProcessesNeedingFurtherProcessing
								.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
						activeProcessesWhereRevelantEventsNeedRecalculation
								.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
						nextActiveProcessNeedsFurtherProcessing = true;
					}
				} else if (activeProcess instanceof ActiveMSS) {
					EList<ActiveMSS> activeMSSsWhereAdditionalRolesWereBound = evaluateNextRoleBindingExpression((ActiveMSS) activeProcess);

					// if the returned list is empty...
					if (activeMSSsWhereAdditionalRolesWereBound == null
							|| activeMSSsWhereAdditionalRolesWereBound
									.isEmpty())
						nextActiveProcessNeedsFurtherProcessing = false;
					else {
						activeProcessesNeedingFurtherProcessing
								.addAll(activeMSSsWhereAdditionalRolesWereBound);
						activeProcessesWhereRevelantEventsNeedRecalculation
								.addAll(activeMSSsWhereAdditionalRolesWereBound);
						nextActiveProcessNeedsFurtherProcessing = true;
					}
				}
			}

			if (!nextActiveProcessNeedsFurtherProcessing)
				activeProcessesNeedingFurtherProcessing.remove(activeProcess);
		}

		// 5) for each cut update relevant events
		// ---> moved to updateMSDModalMessageEvents
		// logger.debug("5) for each cut update relevant events");
		// for (ActiveMSD activeMSD:
		// activeMSDsWhereRevelantEventsNeedRecalculation){
		// boolean isAssumptionMSD =
		// activeMSD.getMsdUtil().isEnvironmentAssumption();
		//
		// if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
		// || !isAssumptionMSD && isSafetyViolationOccurredInRequirements())
		// continue;
		//
		// activeMSD.calculateRelevantEvents();
		// }

		getActiveMSDChangedDuringPerformStep().clear();
		getActiveMSDChangedDuringPerformStep().addAll(
				activeProcessesWhereRevelantEventsNeedRecalculation);

		removeViolatedProcesses();
		
	}

	protected ActiveMSD createActiveMSD() {
		TimedActiveMSD timedMSD = TimedRuntimeFactory.eINSTANCE.createTimedActiveMSD();
		timedMSD.setCurrentRuntimeState(this);
		return timedMSD;
	}

	protected EList<ActiveMSD> createActiveMSDs(MessageEvent messageEvent) {
		if (messageEvent instanceof AsynchronousReceiveEvent)
			return new BasicEList<>();
		ElementContainer container = ((MSDRuntimeStateGraph) getStateGraph()).getElementContainer();
		EList<ActiveMSD> newMSDs = super.createActiveMSDs(messageEvent);
		// update clock (instance) visibilities
		for (ActiveMSD newMSD : newMSDs) {
			((TimedActiveMSD) newMSD).addClocksForMSD();
			for (String msdClockInstanceName : ((TimedActiveMSD) newMSD).getClockInstanceNames()) {
				UDBMClock clock = federation.getFromClock(msdClockInstanceName);
				addResetClock(clock);
			}
			if (messageEvent instanceof AsynchronousSendEvent)
				((AsynchronousSendEvent) messageEvent).getProgressedActiveMSDs()
						.add((TimedActiveMSD) container.getActiveProcess(newMSD));
		}

		return newMSDs;
	}

	@Override
	protected void disposeState(ActiveProcess activeMSD,
			Iterator<ActiveProcess> currentStateActiveMSDIteratorToRemoveFrom) {
//		System.out.println("disposing of active MSD " + activeMSD);
		if (activeMSD instanceof TimedActiveMSD) {
			for (String clockName : ((TimedActiveMSD) activeMSD).getClockInstanceNames()) {
				UDBMClock clock = federation.getFromClock(clockName);
				clockToLastResetObservers.remove(clock);
				controllableClocks.remove(clock);
			}
			((TimedActiveMSD) activeMSD).removeClocksForMSD();
		}
		super.disposeState(activeMSD, currentStateActiveMSDIteratorToRemoveFrom);
	}

	/**
	 * @generated NOT
	 */
	public Set<UDBMClock> getResetClocks() {
		return resetClocks;
	}

	/**
	 * @generated NOT
	 */
	public void addResetClock(UDBMClock clock) {
		resetClocks.add(clock);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		super.setRuntimeUtil(newRuntimeUtil);
		if (getFederation() == null && getFederationString() == null && this.getRuntimeUtil() != null
				&& this.getStateGraph() != null) {
			Federation federation = ((TimedRuntimeUtil) getRuntimeUtil()).getFederationFactory()
					.createZeroFederation(new HashSet<UDBMClock>());
			this.setFederation(
					(Federation) ((TimedMSDRuntimeStateGraph) this.getStateGraph()).getFederation(federation).clone());
		}
	}

	@Override
	protected boolean progressActiveMSD(ActiveMSD activeMSD, MessageEvent messageEvent,
			Iterator<ActiveProcess> currentStateActiveMSDIteratorToRemoveFrom) {

		if (messageEvent instanceof AsynchronousMessageEvent) {
			boolean handled = false;
			if (messageEvent instanceof AsynchronousSendEvent) {
				ElementContainer container = ((MSDRuntimeStateGraph) getStateGraph()).getElementContainer();
				AsynchronousSendEvent sendEvent = (AsynchronousSendEvent) messageEvent;
				for (Map.Entry<NamedElement, MessageEvent> messageToEnabledMessageEventMapEntry : activeMSD
						.getEnabledEvents()) {
					if (messageToEnabledMessageEventMapEntry.getValue() == messageEvent) {
						Message message = (Message) messageToEnabledMessageEventMapEntry.getKey();

						if (RuntimeUtil.Helper.isParameterUnifiable(messageEvent,
								messageToEnabledMessageEventMapEntry.getValue(), activeMSD)) {
							Lifeline sendingLifeline = RuntimeUtil.Helper.getSendingLifeline(message);
							activeMSD.getCurrentState().progressCutLocationOnLifeline(sendingLifeline,
									RuntimeUtil.Helper.getSendingMessageOccurrenceSpecification(message));

							sendEvent.getProgressedActiveMSDs()
									.add((TimedActiveMSD) container.getActiveProcess(activeMSD));
							handled = true;
							break;
						}
					}
				}

			} else if (messageEvent instanceof AsynchronousReceiveEvent) {
				AsynchronousReceiveEvent receiveEvent = (AsynchronousReceiveEvent) messageEvent;
				Queue<AsynchronousReceiveEvent> buffer = getQueue(messageEvent);
				if (messageEvent == buffer.peek()) {
					for (Message message : activeMSD.getInteraction().getMessages()) {
						if (activeMSD.getCurrentState().isMOSEnabled(
								RuntimeUtil.Helper.getReceivingMessageOccurrenceSpecification(message),
								RuntimeUtil.Helper.getReceivingLifeline(message))
								&& listContainsActiveProcess(receiveEvent.getProgressedActiveMSDs(), activeMSD)
								&& (RuntimeUtil.Helper.getFirstMessageForMSD(activeMSD.getInteraction()) == message
										&& messageEvent.getOperation() == getRuntimeUtil().getOperation(message)
										|| receiveEvent.getProgressedMessages().contains(message))) {

							Lifeline receivingLifeline = RuntimeUtil.Helper.getReceivingLifeline(message);
							activeMSD.getCurrentState().progressCutLocationOnLifeline(receivingLifeline,
									RuntimeUtil.Helper.getReceivingMessageOccurrenceSpecification(message));
							handled = true;
							break;
						}
					}
				}
			}
			if (activeMSD.getCurrentState().terminalCutReached()) {
				disposeState(activeMSD, currentStateActiveMSDIteratorToRemoveFrom);
				return false;
			}
			return handled;
		} else
			return super.progressActiveMSD(activeMSD, messageEvent, currentStateActiveMSDIteratorToRemoveFrom);
	}

	private boolean listContainsActiveProcess(EList<? extends ActiveProcess> processList, ActiveProcess process) {
		ElementContainer container = ((MSDRuntimeStateGraph) getStateGraph()).getElementContainer();
		ActiveProcess lookedUpProcess = container.getActiveProcess(process);
		for (ActiveProcess otherProc : processList) {
			if (container.getActiveProcess(otherProc) == lookedUpProcess)
				return true;
		}
		return false;
	}

	/**
	 * @generated NOT
	 */
	public Queue<AsynchronousReceiveEvent> getQueue(MessageEvent event) {
		return getQueue(event.getSendingObject(), event.getReceivingObject());
	}

	/**
	 * @generated NOT
	 */
	public Queue<AsynchronousReceiveEvent> getQueue(EObject sender, EObject receiver) {
		LinkedList<EObject> comPair = new LinkedList<>();
		comPair.add(sender);
		comPair.add(receiver);
		return senderAndReceiverToQueue.get(comPair);
	}

	@Override
	public void setMessageQueuesForLinks(Map<List<EObject>, Queue<AsynchronousReceiveEvent>> queuesForLinks) {
		senderAndReceiverToQueue = queuesForLinks;
	}

	private String getConnectionString(EObject sender, EObject receiver) {
		return RuntimeUtil.Helper.getEObjectName(sender) + "to" + RuntimeUtil.Helper.getEObjectName(receiver);
	}

	private void updateBufferString() {
		String newString = "";
		for (Entry<List<EObject>, Queue<AsynchronousReceiveEvent>> entry : senderAndReceiverToQueue.entrySet()) {
			if (entry.getValue().size() > 0) {
				String currentBufferString = "";
				for (AsynchronousReceiveEvent event : entry.getValue())
					currentBufferString += event.getMessageName() + ", ";
				currentBufferString = currentBufferString.replaceFirst(",\\s\\z", "");
				newString += getConnectionString(entry.getKey().get(0), entry.getKey().get(1)) + ": ["
						+ currentBufferString + "]\\n";
			}
		}
		setBufferString(newString);
	}

	@Override
	protected void handleViolations(MessageEvent messageEvent, Iterator<ActiveProcess> activeProcessesIterator) {
		if (!(messageEvent instanceof AsynchronousReceiveEvent))
			super.handleViolations(messageEvent, activeProcessesIterator);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void setControllableClocks(Set<UDBMClock> controllableClocks) {
		this.controllableClocks = controllableClocks;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void applyNegatedDelay(SimpleClockConstraint constraint) {
		applyNegatedDelay(federation, constraint);
	}

	private void applyNegatedDelay(Federation federation, SimpleClockConstraint constraint) {
		federation.and(getNegatedConstraint(constraint));
	}

	private SimpleClockConstraint getNegatedConstraint(SimpleClockConstraint originalConstraint) {
		RelationalOperator newOp = null;
		switch (originalConstraint.getRelationalOperator()) {
		case EqualOperator:
			throw new IllegalArgumentException();
		case GreaterOrEqualOperator:
			newOp = RelationalOperator.LessOperator;
			break;
		case GreaterOperator:
			newOp = RelationalOperator.LessOrEqualOperator;
			break;
		case LessOrEqualOperator:
			newOp = RelationalOperator.GreaterOperator;
			break;
		case LessOperator:
			newOp = RelationalOperator.GreaterOrEqualOperator;
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		SimpleClockConstraint scc = new SimpleClockConstraint(originalConstraint.getClock(), newOp, originalConstraint.getValue());
		scc.setAsNegatedDelayConstraint();
		
		return scc;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public boolean isEnvironmentMessageEvent(MessageEvent messageEvent) {
		return messageEvent instanceof AsynchronousReceiveEvent || super.isEnvironmentMessageEvent(messageEvent);
	}

	@Override
	public boolean isHotOrColdDelayEvent(TimeConditionEvent event) {
		return event.getOperatorString().contains(">");
	}
} // TimedMSDRuntimeStateImpl
