/**
 */
package org.scenariotools.msd.timed.timedruntime;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.muml.udbm.ClockZone;
import org.muml.udbm.Federation;
import org.muml.udbm.SimpleClockConstraint;
import org.muml.udbm.UDBMClock;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed MSD Runtime State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getFederationString <em>Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBufferString <em>Buffer String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInRequirementsSatisfiable <em>Timing Conditions In Requirements Satisfiable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInAssumptionsSatisfiable <em>Timing Conditions In Assumptions Satisfiable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInRequirements <em>Timed Safety Violation Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInAssumptions <em>Timed Safety Violation Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getGoodFederationString <em>Good Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBadFederationString <em>Bad Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getWinningFederationString <em>Winning Federation String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInRequirements <em>Time Inconsistency Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInAssumptions <em>Time Inconsistency Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isUpperBoundRealTimeInconsistency <em>Upper Bound Real Time Inconsistency</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState()
 * @model
 * @generated
 */
public interface TimedMSDRuntimeState extends MSDRuntimeState {
	/**
	 * Returns the value of the '<em><b>Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Federation String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Federation String</em>' attribute.
	 * @see #setFederationString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_FederationString()
	 * @model
	 * @generated
	 */
	String getFederationString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getFederationString <em>Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Federation String</em>' attribute.
	 * @see #getFederationString()
	 * @generated
	 */
	void setFederationString(String value);

	/**
	 * Returns the value of the '<em><b>Buffer String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Buffer String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Buffer String</em>' attribute.
	 * @see #setBufferString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_BufferString()
	 * @model
	 * @generated
	 */
	String getBufferString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBufferString <em>Buffer String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Buffer String</em>' attribute.
	 * @see #getBufferString()
	 * @generated
	 */
	void setBufferString(String value);

	/**
	 * Returns the value of the '<em><b>Timing Conditions In Requirements Satisfiable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timing Conditions In Requirements Satisfiable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timing Conditions In Requirements Satisfiable</em>' attribute.
	 * @see #setTimingConditionsInRequirementsSatisfiable(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimingConditionsInRequirementsSatisfiable()
	 * @model
	 * @generated
	 */
	boolean isTimingConditionsInRequirementsSatisfiable();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInRequirementsSatisfiable <em>Timing Conditions In Requirements Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timing Conditions In Requirements Satisfiable</em>' attribute.
	 * @see #isTimingConditionsInRequirementsSatisfiable()
	 * @generated
	 */
	void setTimingConditionsInRequirementsSatisfiable(boolean value);

	/**
	 * Returns the value of the '<em><b>Timing Conditions In Assumptions Satisfiable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timing Conditions In Assumptions Satisfiable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timing Conditions In Assumptions Satisfiable</em>' attribute.
	 * @see #setTimingConditionsInAssumptionsSatisfiable(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimingConditionsInAssumptionsSatisfiable()
	 * @model
	 * @generated
	 */
	boolean isTimingConditionsInAssumptionsSatisfiable();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInAssumptionsSatisfiable <em>Timing Conditions In Assumptions Satisfiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timing Conditions In Assumptions Satisfiable</em>' attribute.
	 * @see #isTimingConditionsInAssumptionsSatisfiable()
	 * @generated
	 */
	void setTimingConditionsInAssumptionsSatisfiable(boolean value);

	/**
	 * Returns the value of the '<em><b>Timed Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Safety Violation Occurred In Requirements</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Safety Violation Occurred In Requirements</em>' attribute.
	 * @see #setTimedSafetyViolationOccurredInRequirements(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimedSafetyViolationOccurredInRequirements()
	 * @model
	 * @generated
	 */
	boolean isTimedSafetyViolationOccurredInRequirements();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInRequirements <em>Timed Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timed Safety Violation Occurred In Requirements</em>' attribute.
	 * @see #isTimedSafetyViolationOccurredInRequirements()
	 * @generated
	 */
	void setTimedSafetyViolationOccurredInRequirements(boolean value);

	/**
	 * Returns the value of the '<em><b>Timed Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Safety Violation Occurred In Assumptions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #setTimedSafetyViolationOccurredInAssumptions(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimedSafetyViolationOccurredInAssumptions()
	 * @model
	 * @generated
	 */
	boolean isTimedSafetyViolationOccurredInAssumptions();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInAssumptions <em>Timed Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timed Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #isTimedSafetyViolationOccurredInAssumptions()
	 * @generated
	 */
	void setTimedSafetyViolationOccurredInAssumptions(boolean value);

	/**
	 * Returns the value of the '<em><b>Good Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Good Federation String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Good Federation String</em>' attribute.
	 * @see #setGoodFederationString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_GoodFederationString()
	 * @model
	 * @generated
	 */
	String getGoodFederationString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getGoodFederationString <em>Good Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Good Federation String</em>' attribute.
	 * @see #getGoodFederationString()
	 * @generated
	 */
	void setGoodFederationString(String value);

	/**
	 * Returns the value of the '<em><b>Bad Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bad Federation String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bad Federation String</em>' attribute.
	 * @see #setBadFederationString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_BadFederationString()
	 * @model
	 * @generated
	 */
	String getBadFederationString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBadFederationString <em>Bad Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bad Federation String</em>' attribute.
	 * @see #getBadFederationString()
	 * @generated
	 */
	void setBadFederationString(String value);

	/**
	 * Returns the value of the '<em><b>Winning Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Winning Federation String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Winning Federation String</em>' attribute.
	 * @see #setWinningFederationString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_WinningFederationString()
	 * @model
	 * @generated
	 */
	String getWinningFederationString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getWinningFederationString <em>Winning Federation String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Winning Federation String</em>' attribute.
	 * @see #getWinningFederationString()
	 * @generated
	 */
	void setWinningFederationString(String value);

	/**
	 * Returns the value of the '<em><b>Time Inconsistency Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Inconsistency Occurred In Requirements</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Inconsistency Occurred In Requirements</em>' attribute.
	 * @see #setTimeInconsistencyOccurredInRequirements(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimeInconsistencyOccurredInRequirements()
	 * @model
	 * @generated
	 */
	boolean isTimeInconsistencyOccurredInRequirements();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInRequirements <em>Time Inconsistency Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Inconsistency Occurred In Requirements</em>' attribute.
	 * @see #isTimeInconsistencyOccurredInRequirements()
	 * @generated
	 */
	void setTimeInconsistencyOccurredInRequirements(boolean value);

	/**
	 * Returns the value of the '<em><b>Time Inconsistency Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Inconsistency Occurred In Assumptions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Inconsistency Occurred In Assumptions</em>' attribute.
	 * @see #setTimeInconsistencyOccurredInAssumptions(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_TimeInconsistencyOccurredInAssumptions()
	 * @model
	 * @generated
	 */
	boolean isTimeInconsistencyOccurredInAssumptions();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInAssumptions <em>Time Inconsistency Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Inconsistency Occurred In Assumptions</em>' attribute.
	 * @see #isTimeInconsistencyOccurredInAssumptions()
	 * @generated
	 */
	void setTimeInconsistencyOccurredInAssumptions(boolean value);

	/**
	 * Returns the value of the '<em><b>Upper Bound Real Time Inconsistency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Bound Real Time Inconsistency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Bound Real Time Inconsistency</em>' attribute.
	 * @see #setUpperBoundRealTimeInconsistency(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeState_UpperBoundRealTimeInconsistency()
	 * @model
	 * @generated
	 */
	boolean isUpperBoundRealTimeInconsistency();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isUpperBoundRealTimeInconsistency <em>Upper Bound Real Time Inconsistency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Bound Real Time Inconsistency</em>' attribute.
	 * @see #isUpperBoundRealTimeInconsistency()
	 * @generated
	 */
	void setUpperBoundRealTimeInconsistency(boolean value);

	/**
	 * @generated NOT
	 */
	public Federation getFederation();

	/**
	 * @generated NOT
	 */
	public void setFederation(Federation federation);
		
	/**
	 * @generated NOT
	 */
	public LinkedList<TimeConditionEvent> getCurrentTimeConditionEvents();
	
	/**
	 * @generated NOT
	 */
	public Set<EObject> getLastResetObservers(UDBMClock clock);

	/**
	 * @generated NOT
	 */
	public void addClockToLastResetObserver(UDBMClock clock, EObject lastResetObserver);	
	/**
	 * @generated NOT
	 */
	public Map<UDBMClock, Set<EObject>> getClockToLastResetObservers();
	/**
	 * @generated NOT
	 */
	public Set<UDBMClock> getResetClocks();
	/**
	 * @generated NOT
	 */
	public void addResetClock(UDBMClock clock);
	/**
	 * @generated NOT
	 */
	public Set<UDBMClock> getControllableClocks();
	/**
	 * @generated NOT
	 */
	public void setControllableClocks(Set<UDBMClock> controllableClocks);
	
	/**
	 * @generated NOT
	 */
	public Map<List<EObject>, Queue<AsynchronousReceiveEvent>> getMessageQueuesForLinks();
	/**
	 * @generated NOT
	 */
	public void setMessageQueuesForLinks(Map<List<EObject>, Queue<AsynchronousReceiveEvent>> queuesForLinks);
	/**
	 * @generated NOT
	 */
	@Override
	public TimedRuntimeUtil getRuntimeUtil();
	/**
	 * @generated NOT
	 */
	public Queue<AsynchronousReceiveEvent> getQueue(MessageEvent event);

	/**
	 * @generated NOT
	 */
	public Queue<AsynchronousReceiveEvent> getQueue(EObject sender,
			EObject receiver);

	/**
	 * @generated NOT
	 */
	public void initQueue();

	/**
	 * @generated NOT
	 */
	public void applyNegatedDelay(SimpleClockConstraint constraint);

	/**
	 * @generated NOT
	 */
	boolean isDelayEvent(TimeConditionEvent event);

	/**
	 * @generated NOT
	 */
	boolean isHotOrColdDelayEvent(TimeConditionEvent event);
}
