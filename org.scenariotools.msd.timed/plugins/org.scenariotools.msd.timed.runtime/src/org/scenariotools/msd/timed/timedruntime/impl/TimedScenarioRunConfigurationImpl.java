/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedScenarioRunConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Scenario Run Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimedScenarioRunConfigurationImpl extends ScenarioRunConfigurationImpl implements TimedScenarioRunConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedScenarioRunConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_SCENARIO_RUN_CONFIGURATION;
	}

} //TimedScenarioRunConfigurationImpl
