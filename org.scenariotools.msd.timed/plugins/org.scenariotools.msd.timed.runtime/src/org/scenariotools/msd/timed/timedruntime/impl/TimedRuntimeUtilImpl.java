/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Stereotype;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.impl.RuntimeUtilImpl;

import org.muml.udbm.ClockConstraint;
import org.muml.udbm.ClockZone;
import org.muml.udbm.DifferenceClockConstraint;
import org.muml.udbm.Federation;
import org.muml.udbm.FederationFactory;
import org.muml.udbm.SimpleClockConstraint;
import org.muml.udbm.UDBMClock;
import org.muml.udbm.clockconstraint.RelationalOperator;
import org.muml.udbm.java.JavaFederationFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Util</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class TimedRuntimeUtilImpl extends RuntimeUtilImpl implements
		TimedRuntimeUtil {
	private boolean enforceEarliestDelaysFirst = true;
	private FederationFactory federationFactory;
	private Map<String, UDBMClock> clockInstanceNames2ClockInstances;
	private Map<List<EObject>, Integer[]> sendersAndReceivers2minAndMaxDelays;
	private Map<List<EObject>, Integer> sendersAndReceivers2BufferSize;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public TimedRuntimeUtilImpl() {
		super();
		interaction2ClockNamesMap = new HashMap<>();
		federationFactory = new JavaFederationFactory();
		clockInstanceNames2ClockInstances = new HashMap<>();
	}

	public UDBMClock getClockInstanceForName(String name) {
		UDBMClock clock = clockInstanceNames2ClockInstances.get(name);
		if (clock == null) {
			clock = new UDBMClock(name, name);
			clockInstanceNames2ClockInstances.put(name, clock);
		}
		return clock;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_RUNTIME_UTIL;
	}

	/**
	 * @generated NOT
	 */
	public boolean isTimeCondition(CombinedFragment combinedFragment) {
		for (Stereotype stereotype : combinedFragment.getAppliedStereotypes()) {
			if ("TimeCondition".equals(stereotype.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated NOT
	 */
	public boolean isClockReset(CombinedFragment combinedFragment) {
		for (Stereotype stereotype : combinedFragment.getAppliedStereotypes()) {
			if ("ClockReset".equals(stereotype.getName())) {
				return true;
			}
		}
		return false;
	}

	protected RelationalOperator parseOperatorForClockConstraint(
			CombinedFragment constraint) {
		String guardString = RuntimeUtil.Helper
				.getCombinedFragmentGuardString(constraint);
		RelationalOperator operator = null;
		for (RelationalOperator operatorCandidate : RelationalOperator.values()) {
			if (guardString.contains(operatorCandidate.toString()))
				if (operator == null
						|| !operator.toString().contains("=")) // GreaterEqual
																		// and
																		// LesserEqual
																		// overrule
																		// Greater
																		// and
																		// Lesser
					operator = operatorCandidate;
		}
		return operator;
	}

	/**
	 * @generated NOT
	 */
	public SimpleClockConstraint parseClockConstraintForFragment(
			CombinedFragment fragment, Federation federation, TimedActiveMSD msd) {
		String guardString = RuntimeUtil.Helper
				.getCombinedFragmentGuardString(fragment);
		RelationalOperator operator = parseOperatorForClockConstraint(fragment);
		String[] timeGuardComponents = guardString.split(operator.toString());
		String clockName = timeGuardComponents[0].trim();
//		System.out.println("clock="+clockName);
		String instanceName=msd.getClockInstanceNameForClockName(clockName);
//		System.out.println("clockinstance="+instanceName);
		return parseClockConstraintForFragment(fragment, federation,
				instanceName);
	}

	/**
	 * @generated NOT
	 */
	public SimpleClockConstraint parseClockConstraintForFragment(
			CombinedFragment fragment, Federation federation,
			String clockInstanceName) {
		String guardString = RuntimeUtil.Helper
				.getCombinedFragmentGuardString(fragment);
		RelationalOperator operator = parseOperatorForClockConstraint(fragment);
		String[] timeGuardComponents = guardString.split(operator.toString());
		int comparedValue = Integer.parseInt(timeGuardComponents[1].trim());
		UDBMClock clock = federation.getFromClock(clockInstanceName);

		return new SimpleClockConstraint(clock, operator, comparedValue);
	}

	/**
	 * @Generated NOT
	 */
	@Override
	public Set<String> getClockNamesForMSD(Interaction interaction) {
		Set<String> result = interaction2ClockNamesMap.get(interaction);
		if (result == null) {
			result = new HashSet<>();
			interaction2ClockNamesMap.put(interaction, result);
		}
		return result;
	}

	private Map<Interaction, Set<String>> interaction2ClockNamesMap;

	/**
	 * @Generated NOT
	 */
	@Override
	public void init(UmlPackage2EPackage rootUmlPackage2EPackage,
			EList<Package> useCaseSpecificationsToBeConsidered) {
		super.init(rootUmlPackage2EPackage, useCaseSpecificationsToBeConsidered);
		for (Interaction interaction : getInteractions()) {
			for (InteractionFragment fragment : interaction.getFragments()) {
				if (fragment instanceof CombinedFragment) {
					CombinedFragment combinedFragment = (CombinedFragment) fragment;
					if (isTimeCondition(combinedFragment)) {
						RelationalOperator operator = parseOperatorForClockConstraint(combinedFragment);
						String guardString = RuntimeUtil.Helper
								.getCombinedFragmentGuardString(combinedFragment);
						String clockName = guardString.split(operator
								.toString())[0].trim();
						getClockNamesForMSD(interaction).add(clockName);
					}
					if (isClockReset(combinedFragment)) {
						String resetString = RuntimeUtil.Helper
								.getCombinedFragmentGuardString(combinedFragment);
						String clockName = resetString.split("=")[0].trim();
						getClockNamesForMSD(interaction).add(clockName);
					}
				}
			}
		}
	}

	/**
	 * @Generated NOT
	 */
	@Override
	public FederationFactory getFederationFactory() {
		return federationFactory;
	}

	/**
	 * @generated NOT
	 */
	public String createFederationString(Federation federation) {
		assert (federation.sizeOfClockZone() <= 1);
		if (federation.sizeOfClockZone() == 1) {
			Map<UDBMClock, SimpleClockConstraint> clock2lowerBound = new HashMap<>();
			Map<UDBMClock, SimpleClockConstraint> clock2upperBound = new HashMap<>();
			Map<List<UDBMClock>, DifferenceClockConstraint> clockPair2bound = new HashMap<>();

			ClockZone zone = federation.iteratorOfClockZone().next();
			Iterator<ClockConstraint> ccIt = zone.iteratorOfClockConstraint();
			while (ccIt.hasNext()) {
				ClockConstraint cc = ccIt.next();
				if (cc instanceof SimpleClockConstraint) {
					SimpleClockConstraint scc = (SimpleClockConstraint) cc;
					if (scc.getRelationalOperator().toString().contains(">"))
						clock2lowerBound.put(scc.getClock(), scc);
					else if (scc.getRelationalOperator().toString()
							.contains("<"))
						clock2upperBound.put(scc.getClock(), scc);
				} else if (cc instanceof DifferenceClockConstraint) {
					DifferenceClockConstraint dcc = (DifferenceClockConstraint) cc;
					List<UDBMClock> clockPair = new LinkedList<UDBMClock>();
					clockPair.add(dcc.getClockMinuend());
					clockPair.add(dcc.getClockSubtrahend());
					clockPair2bound.put(clockPair, dcc);
				}
			}
			StringBuilder sb = new StringBuilder();
			boolean notEmpty = false;
			Iterator<? extends UDBMClock> clockIt = federation
					.iteratorOfClock();
			while (clockIt.hasNext()) {
				UDBMClock clock = clockIt.next();
				if (clock.getId().equals("zeroclock"))
					continue;
				SimpleClockConstraint lb = clock2lowerBound.get(clock);
				SimpleClockConstraint ub = clock2upperBound.get(clock);
				if (lb != null || ub != null) {
					if (ub == null) {
						sb.append(clock.getName());
						sb.append(lb.getRelationalOperator());
						sb.append(lb.getValue());
					} else if (lb == null) {
						sb.append(clock.getName());
						sb.append(ub.getRelationalOperator());
						sb.append(ub.getValue());
					} else if (ub.getRelationalOperator().toString()
							.contains("=")
							&& lb.getRelationalOperator().toString()
									.contains("=")
							&& ub.getValue() == lb.getValue()) {
						sb.append(clock.getName());
						sb.append("=");
						sb.append(lb.getValue());
					} else {
						if (lb != null) {
							sb.append(lb.getValue());
							sb.append(lb.getRelationalOperator().toString()
									.replace('>', '<'));
						}
						sb.append(clock.getName());
						sb.append(ub.getRelationalOperator());
						sb.append(ub.getValue());
					}
					if (clockIt.hasNext())
						sb.append(", ");
					notEmpty = true;
				}
			}
			if (notEmpty)
				sb.append("\\n");
			clockIt = federation.iteratorOfClock();
			while (clockIt.hasNext()) {
				UDBMClock clock = clockIt.next();
				if (clock.getId().equals("zeroclock"))
					continue;
				boolean notEmpty2 = false;
				Iterator<? extends UDBMClock> clockIt2 = federation
						.iteratorOfClock();
				while (clockIt2.hasNext()) {
					UDBMClock clock2 = clockIt2.next();
					if (clock2.getId().equals("zeroclock"))
						continue;
					List<UDBMClock> clockPair = new LinkedList<>();
					clockPair.add(clock);
					clockPair.add(clock2);
					DifferenceClockConstraint dcc = clockPair2bound
							.remove(clockPair);
					List<UDBMClock> oppositeClockPair = new LinkedList<>();
					oppositeClockPair.add(clock2);
					oppositeClockPair.add(clock);
					DifferenceClockConstraint oppositeDcc = clockPair2bound
							.remove(oppositeClockPair);
					if (dcc != null || oppositeDcc != null) {
						if (dcc != null && oppositeDcc != null
								&& dcc.getValue() == 0
								&& oppositeDcc.getValue() == 0) {
							sb.append(dcc.getClockMinuend().getName());
							sb.append("=");
							sb.append(dcc.getClockSubtrahend().getName());
						} else {
							if (dcc != null) {
								sb.append(dcc.toString());
							}
							if (dcc != null && oppositeDcc != null)
								sb.append(", ");
							if (oppositeDcc != null) {
								sb.append(oppositeDcc.toString());
							}
						}
						if (clockIt2.hasNext())
							sb.append("\\n");
						notEmpty2 = true;
					}
				}
				if (notEmpty2 && clockIt.hasNext())
					sb.append("\\n");
			}

			if (sb.length() != 0)
				return sb.toString().replace("_0", "");
			else
				return "true\\n";
		} else
			return "false\\n";
	}

	/**
	 * @generated NOT
	 */
	public Federation createUnrestrictedFederationWithSameClocks(
			Federation federation) {
		HashSet<UDBMClock> clocks = new HashSet<>();
		Iterator<? extends UDBMClock> clockIt = federation.iteratorOfClock();
		while (clockIt.hasNext()) {
			clocks.add(clockIt.next());
		}
		return federationFactory.createFederation(clocks,
				new HashSet<ClockConstraint>());
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void init(EList<CommunicationLink> communicationLinks) {
		sendersAndReceivers2minAndMaxDelays = new HashMap<>();
		sendersAndReceivers2BufferSize = new HashMap<>();
		for (CommunicationLink comLink : communicationLinks) {
			if (comLink instanceof TimedCommunicationLink) {
				TimedCommunicationLink timedLink = (TimedCommunicationLink) comLink;
				List<EObject> comPair = new LinkedList<EObject>();
				comPair.add(comLink.getSender());
				comPair.add(comLink.getReceiver());
				Integer[] minMaxDelay = new Integer[] {
						timedLink.getMinDelay(), timedLink.getMaxDelay() };
				sendersAndReceivers2minAndMaxDelays.put(comPair, minMaxDelay);
				if (timedLink.isAsynchronous())
					sendersAndReceivers2BufferSize.put(comPair,
							timedLink.getBufferSize());
			}
		}
	}

	/**
	 * @generated NOT
	 */
	public Integer[] getMinMaxDelay(EObject sender, EObject receiver) {
		List<EObject> pair=new LinkedList<>();
		pair.add(sender);
		pair.add(receiver);
		Integer[] minMaxDelay = sendersAndReceivers2minAndMaxDelays
				.get(pair);
		if (minMaxDelay != null)
			return minMaxDelay;
		else
			return new Integer[] { 0, 0 };
	}

	/**
	 * @generated NOT
	 */
	public Integer[] getMinMaxDelay(MessageEvent messageEvent) {
		return getMinMaxDelay(messageEvent.getSendingObject(),
				messageEvent.getReceivingObject());
	}

	/**
	 * @generated NOT
	 */
	public Integer getBufferSize(EObject sender, EObject receiver) {
		List<EObject> pair=new LinkedList<>();
		pair.add(sender);
		pair.add(receiver);
		Integer size = sendersAndReceivers2BufferSize.get(pair);
		return size != null ? size : 0;
	}

	/**
	 * @generated NOT
	 */
	public Integer getBufferSize(MessageEvent messageEvent) {
		return getBufferSize(messageEvent.getSendingObject(),
				messageEvent.getReceivingObject());
	}

	/**
	 * @generated NOT
	 */
	public Set<List<EObject>> getAsynchronousSendersAndReceivers() {
		return sendersAndReceivers2BufferSize.keySet();
	}

	@Override
	public MessageEvent createMessageEvent(EObject sendingObject,
			EObject receivingObject) {
		if(getBufferSize(sendingObject, receivingObject)>0)
			return TimedRuntimeFactory.eINSTANCE.createAsynchronousSendEvent();
		else
			return TimedRuntimeFactory.eINSTANCE.createTimeRestrictedMessageEvent();
	}

	/**
	 * @generated NOT
	 */
	public boolean isEnforceEarliestDelaysFirst() {
		return enforceEarliestDelaysFirst;
	}

	/**
	 * @generated NOT
	 */
	public void setEnforceEarliestDelaysFirst(boolean enforceEarliestDelaysFirst) {
		this.enforceEarliestDelaysFirst = enforceEarliestDelaysFirst;
	}
} // TimedRuntimeUtilImpl
