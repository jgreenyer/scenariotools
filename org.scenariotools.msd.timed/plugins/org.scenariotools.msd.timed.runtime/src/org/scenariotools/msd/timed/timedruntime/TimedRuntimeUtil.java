/**
 */
package org.scenariotools.msd.timed.timedruntime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Interaction;
import org.muml.udbm.Federation;
import org.muml.udbm.FederationFactory;
import org.muml.udbm.SimpleClockConstraint;
import org.muml.udbm.UDBMClock;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Util</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedRuntimeUtil()
 * @model
 * @generated
 */
public interface TimedRuntimeUtil extends RuntimeUtil {
	/**
	 * @generated NOT
	 */
	public boolean isTimeCondition(CombinedFragment combinedFragment);

	/**
	 * @generated NOT
	 */
	public boolean isClockReset(CombinedFragment combinedFragment);

	/**
	 * @generated NOT
	 */
	public SimpleClockConstraint parseClockConstraintForFragment(
			CombinedFragment fragment, Federation federation, TimedActiveMSD msd);

	/**
	 * @generated NOT
	 */
	public SimpleClockConstraint parseClockConstraintForFragment(
			CombinedFragment fragment, Federation federation,
			String clockInstanceName);

	public class TimedMSDRuntimeStateCopier extends MSDRuntimeStateCopier {

		@Override
		public MSDRuntimeState copy(MSDRuntimeState msdRuntimeState, Event event) {
			if (msdRuntimeState instanceof TimedMSDRuntimeState) {
				TimedMSDRuntimeState newRuntimeState = (TimedMSDRuntimeState) super
						.copy(msdRuntimeState, event);
				TimedMSDRuntimeState oldState = ((TimedMSDRuntimeState) msdRuntimeState);
				newRuntimeState.setFederation((Federation) oldState
						.getFederation().clone());
				newRuntimeState.setControllableClocks(new HashSet<UDBMClock>());
				newRuntimeState.getControllableClocks().addAll(
						oldState.getControllableClocks());

				HashMap<List<EObject>, Queue<AsynchronousReceiveEvent>> newMap = new HashMap<>();
				for (Entry<List<EObject>, Queue<AsynchronousReceiveEvent>> oldQueueForLinkEntry : oldState
						.getMessageQueuesForLinks().entrySet())
					newMap.put(new LinkedList<>(oldQueueForLinkEntry.getKey()),
							new LinkedList<AsynchronousReceiveEvent>(
									oldQueueForLinkEntry.getValue()));
				newRuntimeState.setMessageQueuesForLinks(newMap);

				// copy clock reset observers
				Iterator<? extends UDBMClock> clockIt = newRuntimeState
						.getFederation().iteratorOfClock();
				while (clockIt.hasNext()) {
					UDBMClock clock = clockIt.next();
					Set<EObject> observers = oldState
							.getLastResetObservers(clock);
					if (observers != null)
						for (EObject observer : observers)
							newRuntimeState.addClockToLastResetObserver(clock,
									observer);
				}
				
				// correct contained active msds
//				for (ActiveProcess activeProcess : msdRuntimeState
//						.getActiveProcesses()) {
//					if (activeProcess instanceof TimedActiveMSD) {
//						TimedActiveMSD msd = (TimedActiveMSD) activeProcess;
//						ElementContainer container = ((MSDRuntimeStateGraph) msdRuntimeState
//								.getStateGraph()).getElementContainer();
//						TimedActiveMSD containedActiveProcess = (TimedActiveMSD) container
//								.getActiveProcess(msd);
//						containedActiveProcess
//								.copyClocksFromTimedActiveMSD(msd);
//					}
//				}

				// correct copies of active msds in new runtime state
//				for (ActiveProcess activeProcess : newRuntimeState
//						.getActiveProcesses()) {
//					TimedActiveMSD msd = (TimedActiveMSD) activeProcess;
//					ElementContainer container = ((MSDRuntimeStateGraph) msdRuntimeState
//							.getStateGraph()).getElementContainer();
//					TimedActiveMSD containedActiveProcess = (TimedActiveMSD) container
//							.getActiveProcess(msd);
//					msd.copyClocksFromTimedActiveMSD(containedActiveProcess);
//					// remove process from list of unchanged ones if event
//					// refers to it
//					if (event instanceof TimeConditionEvent) {
//						TimeConditionEvent tcEvent = (TimeConditionEvent) event;
//						if (activeProcess.getCurrentState() == container
//								.getActiveState(tcEvent.getActiveMSDCut())) {
//							this.getUnchangedActiveMSDsEList().remove(
//									activeProcess);
//						}
//					}
//				}

				// newRuntimeState.setStateGraph(msdRuntimeState.getStateGraph());
				return newRuntimeState;
			} else
				return super.copy(msdRuntimeState, event);
		}

	}

	/**
	 * @Generated NOT
	 */
	public Set<String> getClockNamesForMSD(Interaction interaction);

	/**
	 * @Generated NOT
	 */
	public FederationFactory getFederationFactory();

	/**
	 * @Generated NOT
	 */
	public UDBMClock getClockInstanceForName(String name);

	/**
	 * @generated NOT
	 */
	public String createFederationString(Federation federation);

	/**
	 * @generated NOT
	 */
	public Federation createUnrestrictedFederationWithSameClocks(
			Federation federation);

	/**
	 * @generated NOT
	 */
	public void init(EList<CommunicationLink> communicationLinks);

	/**
	 * @generated NOT
	 */
	public Integer[] getMinMaxDelay(EObject sender, EObject receiver);

	/**
	 * @generated NOT
	 */
	public Integer[] getMinMaxDelay(MessageEvent messageEvent);

	/**
	 * @generated NOT
	 */
	public Set<List<EObject>> getAsynchronousSendersAndReceivers();

	/**
	 * @generated NOT
	 */
	public Integer getBufferSize(EObject sender, EObject receiver);

	/**
	 * @generated NOT
	 */
	public Integer getBufferSize(MessageEvent messageEvent);
	/**
	 * @generated NOT
	 */
	public boolean isEnforceEarliestDelaysFirst();
	/**
	 * @generated NOT
	 */
	public void setEnforceEarliestDelaysFirst(boolean enforceEarliestDelaysFirst);
} // TimedRuntimeUtil
