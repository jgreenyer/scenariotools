/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.eclipse.emf.common.util.EList;
import org.muml.udbm.Federation;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed MSD Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph#getTimeConditionEvents <em>Time Condition Events</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeStateGraph()
 * @model
 * @generated
 */
public interface TimedMSDRuntimeStateGraph extends MSDRuntimeStateGraph {

	/**
	 * Returns the value of the '<em><b>Time Condition Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Condition Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Condition Events</em>' containment reference list.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedMSDRuntimeStateGraph_TimeConditionEvents()
	 * @model containment="true"
	 * @generated
	 */
	EList<TimeConditionEvent> getTimeConditionEvents();
	/**
	 * @Generated NOT
	 */
	public Federation getFederation(Federation federation);
} // TimedMSDRuntimeStateGraph
