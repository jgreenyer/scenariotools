/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface TimedRuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "timedruntime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.runtime.timed/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "timedruntime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedRuntimePackage eINSTANCE = org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl <em>Timed MSD Runtime State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedMSDRuntimeState()
	 * @generated
	 */
	int TIMED_MSD_RUNTIME_STATE = 0;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = RuntimePackage.MSD_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__OUTGOING_TRANSITION = RuntimePackage.MSD_RUNTIME_STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__INCOMING_TRANSITION = RuntimePackage.MSD_RUNTIME_STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__STATE_GRAPH = RuntimePackage.MSD_RUNTIME_STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__LABEL = RuntimePackage.MSD_RUNTIME_STATE__LABEL;

	/**
	 * The feature id for the '<em><b>Excluded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__EXCLUDED = RuntimePackage.MSD_RUNTIME_STATE__EXCLUDED;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__OBJECT_SYSTEM = RuntimePackage.MSD_RUNTIME_STATE__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = RuntimePackage.MSD_RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = RuntimePackage.MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = RuntimePackage.MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = RuntimePackage.MSD_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP;

	/**
	 * The feature id for the '<em><b>Active Processes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__ACTIVE_PROCESSES = RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__RUNTIME_UTIL = RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Active MSD Changed During Perform Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP = RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP;

	/**
	 * The feature id for the '<em><b>Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Buffer String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__BUFFER_STRING = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Timing Conditions In Requirements Satisfiable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Timing Conditions In Assumptions Satisfiable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Timed Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Timed Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Good Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Bad Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Winning Federation String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Time Inconsistency Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Time Inconsistency Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Upper Bound Real Time Inconsistency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Timed MSD Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_FEATURE_COUNT = RuntimePackage.MSD_RUNTIME_STATE_FEATURE_COUNT + 12;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl <em>Timed Active MSD</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedActiveMSD()
	 * @generated
	 */
	int TIMED_ACTIVE_MSD = 1;

	/**
	 * The feature id for the '<em><b>Variable Valuations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__VARIABLE_VALUATIONS = RuntimePackage.ACTIVE_MSD__VARIABLE_VALUATIONS;

	/**
	 * The feature id for the '<em><b>Cold Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__COLD_FORBIDDEN_EVENTS = RuntimePackage.ACTIVE_MSD__COLD_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Hot Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__HOT_FORBIDDEN_EVENTS = RuntimePackage.ACTIVE_MSD__HOT_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Violating Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__VIOLATING_EVENTS = RuntimePackage.ACTIVE_MSD__VIOLATING_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__ENABLED_EVENTS = RuntimePackage.ACTIVE_MSD__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__RUNTIME_UTIL = RuntimePackage.ACTIVE_MSD__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__CURRENT_STATE = RuntimePackage.ACTIVE_MSD__CURRENT_STATE;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__OBJECT_SYSTEM = RuntimePackage.ACTIVE_MSD__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__ID = RuntimePackage.ACTIVE_MSD__ID;

	/**
	 * The feature id for the '<em><b>Lifeline Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__LIFELINE_BINDINGS = RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__INTERACTION = RuntimePackage.ACTIVE_MSD__INTERACTION;

	/**
	 * The feature id for the '<em><b>Msd Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__MSD_UTIL = RuntimePackage.ACTIVE_MSD__MSD_UTIL;

	/**
	 * The feature id for the '<em><b>Current Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE = RuntimePackage.ACTIVE_MSD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Clock Name To Clock Instance Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP = RuntimePackage.ACTIVE_MSD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Timed Active MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_ACTIVE_MSD_FEATURE_COUNT = RuntimePackage.ACTIVE_MSD_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateGraphImpl <em>Timed MSD Runtime State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateGraphImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedMSDRuntimeStateGraph()
	 * @generated
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH = 2;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__STATES = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__START_STATE = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__START_STATE;

	/**
	 * The feature id for the '<em><b>Strategy Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__STRATEGY_KIND = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__STRATEGY_KIND;

	/**
	 * The feature id for the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Key Wrapper To MSD Runtime State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP = RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP;

	/**
	 * The feature id for the '<em><b>Time Condition Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Timed MSD Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT = RuntimePackage.MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimeUtilImpl <em>Util</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimeUtilImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedRuntimeUtil()
	 * @generated
	 */
	int TIMED_RUNTIME_UTIL = 3;

	/**
	 * The feature id for the '<em><b>Uml2ecore Mapping</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__UML2ECORE_MAPPING = UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING;

	/**
	 * The feature id for the '<em><b>Root UML Package To EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE = UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE;

	/**
	 * The feature id for the '<em><b>First Message Operation To Interaction Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP = UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP;

	/**
	 * The feature id for the '<em><b>First Transition Operation To State Machine Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP = UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction To String To Lifeline Map Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP = UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP;

	/**
	 * The feature id for the '<em><b>Lifeline To Interaction Fragments Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP = UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP;

	/**
	 * The feature id for the '<em><b>System Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__SYSTEM_CLASSES = UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES;

	/**
	 * The feature id for the '<em><b>Environment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__ENVIRONMENT_CLASSES = UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES;

	/**
	 * The feature id for the '<em><b>Uml Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__UML_PACKAGES = UtilPackage.RUNTIME_UTIL__UML_PACKAGES;

	/**
	 * The feature id for the '<em><b>Package To Collaborations Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP = UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP;

	/**
	 * The feature id for the '<em><b>Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__INTERACTIONS = UtilPackage.RUNTIME_UTIL__INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Integrated Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__INTEGRATED_PACKAGE = UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE;

	/**
	 * The feature id for the '<em><b>Environment Sendable Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES = UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES;

	/**
	 * The feature id for the '<em><b>System Sendable Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES = UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES;

	/**
	 * The feature id for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED;

	/**
	 * The feature id for the '<em><b>Ocl Registry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__OCL_REGISTRY = UtilPackage.RUNTIME_UTIL__OCL_REGISTRY;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH = UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Final Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__FINAL_FRAGMENT = UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Interaction To MSD Util Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP = UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Operand To Lifeline To Interaction Fragments Map Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP = UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP;

	/**
	 * The feature id for the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__STATE_MACHINES = UtilPackage.RUNTIME_UTIL__STATE_MACHINES;

	/**
	 * The feature id for the '<em><b>State Machine To Initial State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP = UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP;

	/**
	 * The number of structural features of the '<em>Util</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_RUNTIME_UTIL_FEATURE_COUNT = UtilPackage.RUNTIME_UTIL_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl <em>Time Condition Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimeConditionEvent()
	 * @generated
	 */
	int TIME_CONDITION_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__CLOCK_NAME = EventsPackage.EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__OPERATOR_STRING = EventsPackage.EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Compare Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__COMPARE_VALUE = EventsPackage.EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Clock Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__CLOCK_CONDITION = EventsPackage.EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Active MSD Cut</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__ACTIVE_MSD_CUT = EventsPackage.EVENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Constraint Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED = EventsPackage.EVENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__HOT = EventsPackage.EVENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Clock Observers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__CLOCK_OBSERVERS = EventsPackage.EVENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Timed Controllable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__TIMED_CONTROLLABLE = EventsPackage.EVENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Timed Controllable Set Manually</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY = EventsPackage.EVENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Time Condition Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_CONDITION_EVENT_FEATURE_COUNT = EventsPackage.EVENT_FEATURE_COUNT + 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl <em>Timed Communication Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedCommunicationLink()
	 * @generated
	 */
	int TIMED_COMMUNICATION_LINK = 5;

	/**
	 * The feature id for the '<em><b>Forbidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__FORBIDDEN = ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__OPTIONAL = ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__SENDER = ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__RECEIVER = ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER;

	/**
	 * The feature id for the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__MIN_DELAY = ScenariorunconfigurationPackage.COMMUNICATION_LINK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__MAX_DELAY = ScenariorunconfigurationPackage.COMMUNICATION_LINK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Asynchronous</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__ASYNCHRONOUS = ScenariorunconfigurationPackage.COMMUNICATION_LINK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Buffer Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK__BUFFER_SIZE = ScenariorunconfigurationPackage.COMMUNICATION_LINK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Timed Communication Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_COMMUNICATION_LINK_FEATURE_COUNT = ScenariorunconfigurationPackage.COMMUNICATION_LINK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl <em>Asynchronous Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousMessageEvent()
	 * @generated
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__RUNTIME_MESSAGE = EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__SENDING_OBJECT = EventsPackage.MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__RECEIVING_OBJECT = EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE_NAME = EventsPackage.MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = EventsPackage.MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = EventsPackage.MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = EventsPackage.MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__STRING_PARAMETER_VALUE = EventsPackage.MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__PARAMETERIZED = EventsPackage.MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE = EventsPackage.MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__OPERATION = EventsPackage.MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = EventsPackage.MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE = EventsPackage.MESSAGE_EVENT__MESSAGE;

	/**
	 * The feature id for the '<em><b>Progressed Active MS Ds</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS = EventsPackage.MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Progressed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES = EventsPackage.MESSAGE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asynchronous Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT = EventsPackage.MESSAGE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousSendEventImpl <em>Asynchronous Send Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousSendEventImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousSendEvent()
	 * @generated
	 */
	int ASYNCHRONOUS_SEND_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__RUNTIME_MESSAGE = ASYNCHRONOUS_MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__ABSTRACT_MESSAGE_EVENT = ASYNCHRONOUS_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__CONCRETE_MESSAGE_EVENT = ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__SENDING_OBJECT = ASYNCHRONOUS_MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__RECEIVING_OBJECT = ASYNCHRONOUS_MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__MESSAGE_NAME = ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__BOOLEAN_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__INTEGER_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__EOBJECT_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__STRING_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__PARAMETERIZED = ASYNCHRONOUS_MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__CONCRETE = ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__OPERATION = ASYNCHRONOUS_MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = ASYNCHRONOUS_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__MESSAGE = ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE;

	/**
	 * The feature id for the '<em><b>Progressed Active MS Ds</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__PROGRESSED_ACTIVE_MS_DS = ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS;

	/**
	 * The feature id for the '<em><b>Progressed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT__PROGRESSED_MESSAGES = ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES;

	/**
	 * The number of structural features of the '<em>Asynchronous Send Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_SEND_EVENT_FEATURE_COUNT = ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl <em>Asynchronous Receive Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousReceiveEvent()
	 * @generated
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__RUNTIME_MESSAGE = ASYNCHRONOUS_MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__ABSTRACT_MESSAGE_EVENT = ASYNCHRONOUS_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__CONCRETE_MESSAGE_EVENT = ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__SENDING_OBJECT = ASYNCHRONOUS_MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__RECEIVING_OBJECT = ASYNCHRONOUS_MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__MESSAGE_NAME = ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__BOOLEAN_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__INTEGER_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__EOBJECT_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__STRING_PARAMETER_VALUE = ASYNCHRONOUS_MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__PARAMETERIZED = ASYNCHRONOUS_MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__CONCRETE = ASYNCHRONOUS_MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__OPERATION = ASYNCHRONOUS_MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = ASYNCHRONOUS_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__MESSAGE = ASYNCHRONOUS_MESSAGE_EVENT__MESSAGE;

	/**
	 * The feature id for the '<em><b>Progressed Active MS Ds</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__PROGRESSED_ACTIVE_MS_DS = ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS;

	/**
	 * The feature id for the '<em><b>Progressed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__PROGRESSED_MESSAGES = ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES;

	/**
	 * The feature id for the '<em><b>Min Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY = ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY = ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME = ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Asynchronous Receive Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_RECEIVE_EVENT_FEATURE_COUNT = ASYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedScenarioRunConfigurationImpl <em>Timed Scenario Run Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedScenarioRunConfigurationImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedScenarioRunConfiguration()
	 * @generated
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Uml2 Ecore Mapping</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING;

	/**
	 * The feature id for the '<em><b>Simulation Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS;

	/**
	 * The feature id for the '<em><b>Static Model Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS;

	/**
	 * The feature id for the '<em><b>Role To EObject Mapping Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER;

	/**
	 * The feature id for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Execution Semantics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS;

	/**
	 * The feature id for the '<em><b>Store Additional MSD Modal Message Event Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA;

	/**
	 * The feature id for the '<em><b>Dynamic Object System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Only Communicate Via Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS;

	/**
	 * The feature id for the '<em><b>Communication Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS;

	/**
	 * The feature id for the '<em><b>Only Sync Via Communication Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS;

	/**
	 * The feature id for the '<em><b>Strategy Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE;

	/**
	 * The number of structural features of the '<em>Timed Scenario Run Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMED_SCENARIO_RUN_CONFIGURATION_FEATURE_COUNT = ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl <em>Time Restricted Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl
	 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimeRestrictedMessageEvent()
	 * @generated
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__RUNTIME_MESSAGE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__SENDING_OBJECT = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__RECEIVING_OBJECT = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__MESSAGE_NAME = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__STRING_PARAMETER_VALUE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__PARAMETERIZED = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__CONCRETE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__OPERATION = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__MESSAGE = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT__MESSAGE;

	/**
	 * The feature id for the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Exclude Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Exclude Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Time Restricted Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_RESTRICTED_MESSAGE_EVENT_FEATURE_COUNT = EventsPackage.SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT + 5;

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState <em>Timed MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed MSD Runtime State</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState
	 * @generated
	 */
	EClass getTimedMSDRuntimeState();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getFederationString <em>Federation String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Federation String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getFederationString()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_FederationString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBufferString <em>Buffer String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Buffer String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBufferString()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_BufferString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInRequirementsSatisfiable <em>Timing Conditions In Requirements Satisfiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timing Conditions In Requirements Satisfiable</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInRequirementsSatisfiable()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimingConditionsInRequirementsSatisfiable();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInAssumptionsSatisfiable <em>Timing Conditions In Assumptions Satisfiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timing Conditions In Assumptions Satisfiable</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimingConditionsInAssumptionsSatisfiable()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimingConditionsInAssumptionsSatisfiable();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInRequirements <em>Timed Safety Violation Occurred In Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timed Safety Violation Occurred In Requirements</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInRequirements()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimedSafetyViolationOccurredInRequirements();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInAssumptions <em>Timed Safety Violation Occurred In Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timed Safety Violation Occurred In Assumptions</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimedSafetyViolationOccurredInAssumptions()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimedSafetyViolationOccurredInAssumptions();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getGoodFederationString <em>Good Federation String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Good Federation String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getGoodFederationString()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_GoodFederationString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBadFederationString <em>Bad Federation String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bad Federation String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getBadFederationString()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_BadFederationString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getWinningFederationString <em>Winning Federation String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Winning Federation String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#getWinningFederationString()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_WinningFederationString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInRequirements <em>Time Inconsistency Occurred In Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Inconsistency Occurred In Requirements</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInRequirements()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimeInconsistencyOccurredInRequirements();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInAssumptions <em>Time Inconsistency Occurred In Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Inconsistency Occurred In Assumptions</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isTimeInconsistencyOccurredInAssumptions()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_TimeInconsistencyOccurredInAssumptions();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isUpperBoundRealTimeInconsistency <em>Upper Bound Real Time Inconsistency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound Real Time Inconsistency</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState#isUpperBoundRealTimeInconsistency()
	 * @see #getTimedMSDRuntimeState()
	 * @generated
	 */
	EAttribute getTimedMSDRuntimeState_UpperBoundRealTimeInconsistency();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD <em>Timed Active MSD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Active MSD</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedActiveMSD
	 * @generated
	 */
	EClass getTimedActiveMSD();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getCurrentRuntimeState <em>Current Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Runtime State</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getCurrentRuntimeState()
	 * @see #getTimedActiveMSD()
	 * @generated
	 */
	EReference getTimedActiveMSD_CurrentRuntimeState();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getClockNameToClockInstanceMap <em>Clock Name To Clock Instance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Clock Name To Clock Instance Map</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getClockNameToClockInstanceMap()
	 * @see #getTimedActiveMSD()
	 * @generated
	 */
	EReference getTimedActiveMSD_ClockNameToClockInstanceMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph <em>Timed MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed MSD Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph
	 * @generated
	 */
	EClass getTimedMSDRuntimeStateGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph#getTimeConditionEvents <em>Time Condition Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Time Condition Events</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph#getTimeConditionEvents()
	 * @see #getTimedMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getTimedMSDRuntimeStateGraph_TimeConditionEvents();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil <em>Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Util</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil
	 * @generated
	 */
	EClass getTimedRuntimeUtil();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent <em>Time Condition Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Condition Event</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent
	 * @generated
	 */
	EClass getTimeConditionEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockName <em>Clock Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock Name</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockName()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_ClockName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getOperatorString <em>Operator String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator String</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getOperatorString()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_OperatorString();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getCompareValue <em>Compare Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compare Value</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getCompareValue()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_CompareValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockCondition <em>Clock Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Clock Condition</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockCondition()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EReference getTimeConditionEvent_ClockCondition();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getActiveMSDCut <em>Active MSD Cut</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active MSD Cut</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getActiveMSDCut()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EReference getTimeConditionEvent_ActiveMSDCut();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isConstraintFulfilled <em>Constraint Fulfilled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraint Fulfilled</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isConstraintFulfilled()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_ConstraintFulfilled();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isHot <em>Hot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hot</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isHot()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_Hot();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockObservers <em>Clock Observers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Clock Observers</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockObservers()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EReference getTimeConditionEvent_ClockObservers();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllable <em>Timed Controllable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timed Controllable</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllable()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_TimedControllable();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllableSetManually <em>Timed Controllable Set Manually</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timed Controllable Set Manually</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllableSetManually()
	 * @see #getTimeConditionEvent()
	 * @generated
	 */
	EAttribute getTimeConditionEvent_TimedControllableSetManually();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink <em>Timed Communication Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Communication Link</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink
	 * @generated
	 */
	EClass getTimedCommunicationLink();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMinDelay <em>Min Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Delay</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMinDelay()
	 * @see #getTimedCommunicationLink()
	 * @generated
	 */
	EAttribute getTimedCommunicationLink_MinDelay();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMaxDelay <em>Max Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Delay</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getMaxDelay()
	 * @see #getTimedCommunicationLink()
	 * @generated
	 */
	EAttribute getTimedCommunicationLink_MaxDelay();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#isAsynchronous <em>Asynchronous</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Asynchronous</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#isAsynchronous()
	 * @see #getTimedCommunicationLink()
	 * @generated
	 */
	EAttribute getTimedCommunicationLink_Asynchronous();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getBufferSize <em>Buffer Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Buffer Size</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink#getBufferSize()
	 * @see #getTimedCommunicationLink()
	 * @generated
	 */
	EAttribute getTimedCommunicationLink_BufferSize();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent <em>Asynchronous Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asynchronous Message Event</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent
	 * @generated
	 */
	EClass getAsynchronousMessageEvent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedActiveMSDs <em>Progressed Active MS Ds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Progressed Active MS Ds</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedActiveMSDs()
	 * @see #getAsynchronousMessageEvent()
	 * @generated
	 */
	EReference getAsynchronousMessageEvent_ProgressedActiveMSDs();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedMessages <em>Progressed Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Progressed Messages</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedMessages()
	 * @see #getAsynchronousMessageEvent()
	 * @generated
	 */
	EReference getAsynchronousMessageEvent_ProgressedMessages();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent <em>Asynchronous Send Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asynchronous Send Event</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent
	 * @generated
	 */
	EClass getAsynchronousSendEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent <em>Asynchronous Receive Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asynchronous Receive Event</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent
	 * @generated
	 */
	EClass getAsynchronousReceiveEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMinDelay <em>Min Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Delay</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMinDelay()
	 * @see #getAsynchronousReceiveEvent()
	 * @generated
	 */
	EAttribute getAsynchronousReceiveEvent_MinDelay();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMaxDelay <em>Max Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Delay</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getMaxDelay()
	 * @see #getAsynchronousReceiveEvent()
	 * @generated
	 */
	EAttribute getAsynchronousReceiveEvent_MaxDelay();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getClockName <em>Clock Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock Name</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent#getClockName()
	 * @see #getAsynchronousReceiveEvent()
	 * @generated
	 */
	EAttribute getAsynchronousReceiveEvent_ClockName();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimedScenarioRunConfiguration <em>Timed Scenario Run Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timed Scenario Run Configuration</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedScenarioRunConfiguration
	 * @generated
	 */
	EClass getTimedScenarioRunConfiguration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent <em>Time Restricted Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Restricted Message Event</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent
	 * @generated
	 */
	EClass getTimeRestrictedMessageEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getClockName <em>Clock Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock Name</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getClockName()
	 * @see #getTimeRestrictedMessageEvent()
	 * @generated
	 */
	EAttribute getTimeRestrictedMessageEvent_ClockName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMinBound <em>Min Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Bound</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMinBound()
	 * @see #getTimeRestrictedMessageEvent()
	 * @generated
	 */
	EAttribute getTimeRestrictedMessageEvent_MinBound();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMaxBound <em>Max Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Bound</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMaxBound()
	 * @see #getTimeRestrictedMessageEvent()
	 * @generated
	 */
	EAttribute getTimeRestrictedMessageEvent_MaxBound();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMinBound <em>Exclude Min Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exclude Min Bound</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMinBound()
	 * @see #getTimeRestrictedMessageEvent()
	 * @generated
	 */
	EAttribute getTimeRestrictedMessageEvent_ExcludeMinBound();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMaxBound <em>Exclude Max Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exclude Max Bound</em>'.
	 * @see org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMaxBound()
	 * @see #getTimeRestrictedMessageEvent()
	 * @generated
	 */
	EAttribute getTimeRestrictedMessageEvent_ExcludeMaxBound();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TimedRuntimeFactory getTimedRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl <em>Timed MSD Runtime State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedMSDRuntimeState()
		 * @generated
		 */
		EClass TIMED_MSD_RUNTIME_STATE = eINSTANCE.getTimedMSDRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Federation String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING = eINSTANCE.getTimedMSDRuntimeState_FederationString();

		/**
		 * The meta object literal for the '<em><b>Buffer String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__BUFFER_STRING = eINSTANCE.getTimedMSDRuntimeState_BufferString();

		/**
		 * The meta object literal for the '<em><b>Timing Conditions In Requirements Satisfiable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE = eINSTANCE.getTimedMSDRuntimeState_TimingConditionsInRequirementsSatisfiable();

		/**
		 * The meta object literal for the '<em><b>Timing Conditions In Assumptions Satisfiable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE = eINSTANCE.getTimedMSDRuntimeState_TimingConditionsInAssumptionsSatisfiable();

		/**
		 * The meta object literal for the '<em><b>Timed Safety Violation Occurred In Requirements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = eINSTANCE.getTimedMSDRuntimeState_TimedSafetyViolationOccurredInRequirements();

		/**
		 * The meta object literal for the '<em><b>Timed Safety Violation Occurred In Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = eINSTANCE.getTimedMSDRuntimeState_TimedSafetyViolationOccurredInAssumptions();

		/**
		 * The meta object literal for the '<em><b>Good Federation String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING = eINSTANCE.getTimedMSDRuntimeState_GoodFederationString();

		/**
		 * The meta object literal for the '<em><b>Bad Federation String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING = eINSTANCE.getTimedMSDRuntimeState_BadFederationString();

		/**
		 * The meta object literal for the '<em><b>Winning Federation String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING = eINSTANCE.getTimedMSDRuntimeState_WinningFederationString();

		/**
		 * The meta object literal for the '<em><b>Time Inconsistency Occurred In Requirements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS = eINSTANCE.getTimedMSDRuntimeState_TimeInconsistencyOccurredInRequirements();

		/**
		 * The meta object literal for the '<em><b>Time Inconsistency Occurred In Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS = eINSTANCE.getTimedMSDRuntimeState_TimeInconsistencyOccurredInAssumptions();

		/**
		 * The meta object literal for the '<em><b>Upper Bound Real Time Inconsistency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY = eINSTANCE.getTimedMSDRuntimeState_UpperBoundRealTimeInconsistency();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl <em>Timed Active MSD</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedActiveMSDImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedActiveMSD()
		 * @generated
		 */
		EClass TIMED_ACTIVE_MSD = eINSTANCE.getTimedActiveMSD();

		/**
		 * The meta object literal for the '<em><b>Current Runtime State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE = eINSTANCE.getTimedActiveMSD_CurrentRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Clock Name To Clock Instance Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP = eINSTANCE.getTimedActiveMSD_ClockNameToClockInstanceMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateGraphImpl <em>Timed MSD Runtime State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateGraphImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedMSDRuntimeStateGraph()
		 * @generated
		 */
		EClass TIMED_MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getTimedMSDRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Time Condition Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS = eINSTANCE.getTimedMSDRuntimeStateGraph_TimeConditionEvents();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimeUtilImpl <em>Util</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimeUtilImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedRuntimeUtil()
		 * @generated
		 */
		EClass TIMED_RUNTIME_UTIL = eINSTANCE.getTimedRuntimeUtil();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl <em>Time Condition Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimeConditionEvent()
		 * @generated
		 */
		EClass TIME_CONDITION_EVENT = eINSTANCE.getTimeConditionEvent();

		/**
		 * The meta object literal for the '<em><b>Clock Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__CLOCK_NAME = eINSTANCE.getTimeConditionEvent_ClockName();

		/**
		 * The meta object literal for the '<em><b>Operator String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__OPERATOR_STRING = eINSTANCE.getTimeConditionEvent_OperatorString();

		/**
		 * The meta object literal for the '<em><b>Compare Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__COMPARE_VALUE = eINSTANCE.getTimeConditionEvent_CompareValue();

		/**
		 * The meta object literal for the '<em><b>Clock Condition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_CONDITION_EVENT__CLOCK_CONDITION = eINSTANCE.getTimeConditionEvent_ClockCondition();

		/**
		 * The meta object literal for the '<em><b>Active MSD Cut</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_CONDITION_EVENT__ACTIVE_MSD_CUT = eINSTANCE.getTimeConditionEvent_ActiveMSDCut();

		/**
		 * The meta object literal for the '<em><b>Constraint Fulfilled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED = eINSTANCE.getTimeConditionEvent_ConstraintFulfilled();

		/**
		 * The meta object literal for the '<em><b>Hot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__HOT = eINSTANCE.getTimeConditionEvent_Hot();

		/**
		 * The meta object literal for the '<em><b>Clock Observers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_CONDITION_EVENT__CLOCK_OBSERVERS = eINSTANCE.getTimeConditionEvent_ClockObservers();

		/**
		 * The meta object literal for the '<em><b>Timed Controllable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__TIMED_CONTROLLABLE = eINSTANCE.getTimeConditionEvent_TimedControllable();

		/**
		 * The meta object literal for the '<em><b>Timed Controllable Set Manually</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY = eINSTANCE.getTimeConditionEvent_TimedControllableSetManually();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl <em>Timed Communication Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedCommunicationLink()
		 * @generated
		 */
		EClass TIMED_COMMUNICATION_LINK = eINSTANCE.getTimedCommunicationLink();

		/**
		 * The meta object literal for the '<em><b>Min Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_COMMUNICATION_LINK__MIN_DELAY = eINSTANCE.getTimedCommunicationLink_MinDelay();

		/**
		 * The meta object literal for the '<em><b>Max Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_COMMUNICATION_LINK__MAX_DELAY = eINSTANCE.getTimedCommunicationLink_MaxDelay();

		/**
		 * The meta object literal for the '<em><b>Asynchronous</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_COMMUNICATION_LINK__ASYNCHRONOUS = eINSTANCE.getTimedCommunicationLink_Asynchronous();

		/**
		 * The meta object literal for the '<em><b>Buffer Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMED_COMMUNICATION_LINK__BUFFER_SIZE = eINSTANCE.getTimedCommunicationLink_BufferSize();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl <em>Asynchronous Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousMessageEvent()
		 * @generated
		 */
		EClass ASYNCHRONOUS_MESSAGE_EVENT = eINSTANCE.getAsynchronousMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Progressed Active MS Ds</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS = eINSTANCE.getAsynchronousMessageEvent_ProgressedActiveMSDs();

		/**
		 * The meta object literal for the '<em><b>Progressed Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES = eINSTANCE.getAsynchronousMessageEvent_ProgressedMessages();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousSendEventImpl <em>Asynchronous Send Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousSendEventImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousSendEvent()
		 * @generated
		 */
		EClass ASYNCHRONOUS_SEND_EVENT = eINSTANCE.getAsynchronousSendEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl <em>Asynchronous Receive Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getAsynchronousReceiveEvent()
		 * @generated
		 */
		EClass ASYNCHRONOUS_RECEIVE_EVENT = eINSTANCE.getAsynchronousReceiveEvent();

		/**
		 * The meta object literal for the '<em><b>Min Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY = eINSTANCE.getAsynchronousReceiveEvent_MinDelay();

		/**
		 * The meta object literal for the '<em><b>Max Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY = eINSTANCE.getAsynchronousReceiveEvent_MaxDelay();

		/**
		 * The meta object literal for the '<em><b>Clock Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME = eINSTANCE.getAsynchronousReceiveEvent_ClockName();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimedScenarioRunConfigurationImpl <em>Timed Scenario Run Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedScenarioRunConfigurationImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimedScenarioRunConfiguration()
		 * @generated
		 */
		EClass TIMED_SCENARIO_RUN_CONFIGURATION = eINSTANCE.getTimedScenarioRunConfiguration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl <em>Time Restricted Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl
		 * @see org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimePackageImpl#getTimeRestrictedMessageEvent()
		 * @generated
		 */
		EClass TIME_RESTRICTED_MESSAGE_EVENT = eINSTANCE.getTimeRestrictedMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Clock Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME = eINSTANCE.getTimeRestrictedMessageEvent_ClockName();

		/**
		 * The meta object literal for the '<em><b>Min Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND = eINSTANCE.getTimeRestrictedMessageEvent_MinBound();

		/**
		 * The meta object literal for the '<em><b>Max Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND = eINSTANCE.getTimeRestrictedMessageEvent_MaxBound();

		/**
		 * The meta object literal for the '<em><b>Exclude Min Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND = eINSTANCE.getTimeRestrictedMessageEvent_ExcludeMinBound();

		/**
		 * The meta object literal for the '<em><b>Exclude Max Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND = eINSTANCE.getTimeRestrictedMessageEvent_ExcludeMaxBound();

	}

} //TimedRuntimePackage
