/**
 */
package org.scenariotools.msd.timed.timedruntime;

import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.common.util.EMap;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.keywrapper.TimedActiveMSDKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Active MSD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getCurrentRuntimeState <em>Current Runtime State</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getClockNameToClockInstanceMap <em>Clock Name To Clock Instance Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedActiveMSD()
 * @model
 * @generated
 */
public interface TimedActiveMSD extends ActiveMSD {

	/**
	 * Returns the value of the '<em><b>Current Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Runtime State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Runtime State</em>' reference.
	 * @see #setCurrentRuntimeState(MSDRuntimeState)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedActiveMSD_CurrentRuntimeState()
	 * @model
	 * @generated
	 */
	MSDRuntimeState getCurrentRuntimeState();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD#getCurrentRuntimeState <em>Current Runtime State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Runtime State</em>' reference.
	 * @see #getCurrentRuntimeState()
	 * @generated
	 */
	void setCurrentRuntimeState(MSDRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Clock Name To Clock Instance Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name To Clock Instance Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name To Clock Instance Map</em>' map.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedActiveMSD_ClockNameToClockInstanceMap()
	 * @model mapType="org.scenariotools.msd.runtime.StringToStringMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<String, String> getClockNameToClockInstanceMap();

	/**
	 * @generated NOT
	 */
	public Set<TimeConditionEvent> getCurrentTimeConditionEvents();

	/**
	 * @generated NOT
	 */
	public void calculateRelevantTimeConditionEvents();

	/**
	 * @generated NOT
	 */
	public void addClocksForMSD();

	/**
	 * @generated NOT
	 */
	public void removeClocksForMSD();

	/**
	 * @Generated NOT
	 */
	public String getClockInstanceNameForClockName(String name);

	/**
	 * @Generated NOT
	 */
	public Collection<String> getClockInstanceNames();

	/**
	 * @generated NOT
	 */
	public void copyClocksFromTimedActiveMSD(TimedActiveMSD otherActiveMSD);
	/**
	 * @generated NOT
	 */
	public boolean isInHotCutDueToTimeConditionWithLowerBound();
} // TimedActiveMSD
