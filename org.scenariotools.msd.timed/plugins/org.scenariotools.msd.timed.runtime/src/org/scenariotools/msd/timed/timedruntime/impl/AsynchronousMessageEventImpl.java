/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.uml2.uml.Message;
import org.scenariotools.events.impl.MessageEventImpl;
import org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asynchronous Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl#getProgressedActiveMSDs <em>Progressed Active MS Ds</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousMessageEventImpl#getProgressedMessages <em>Progressed Messages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AsynchronousMessageEventImpl extends MessageEventImpl implements AsynchronousMessageEvent {
	/**
	 * The cached value of the '{@link #getProgressedActiveMSDs() <em>Progressed Active MS Ds</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressedActiveMSDs()
	 * @generated
	 * @ordered
	 */
	protected EList<TimedActiveMSD> progressedActiveMSDs;
	/**
	 * The cached value of the '{@link #getProgressedMessages() <em>Progressed Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressedMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> progressedMessages;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AsynchronousMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.ASYNCHRONOUS_MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimedActiveMSD> getProgressedActiveMSDs() {
		if (progressedActiveMSDs == null) {
			progressedActiveMSDs = new EObjectResolvingEList<TimedActiveMSD>(TimedActiveMSD.class, this, TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS);
		}
		return progressedActiveMSDs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getProgressedMessages() {
		if (progressedMessages == null) {
			progressedMessages = new EObjectResolvingEList<Message>(Message.class, this, TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES);
		}
		return progressedMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS:
				return getProgressedActiveMSDs();
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES:
				return getProgressedMessages();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS:
				getProgressedActiveMSDs().clear();
				getProgressedActiveMSDs().addAll((Collection<? extends TimedActiveMSD>)newValue);
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES:
				getProgressedMessages().clear();
				getProgressedMessages().addAll((Collection<? extends Message>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS:
				getProgressedActiveMSDs().clear();
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES:
				getProgressedMessages().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS:
				return progressedActiveMSDs != null && !progressedActiveMSDs.isEmpty();
			case TimedRuntimePackage.ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES:
				return progressedMessages != null && !progressedMessages.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AsynchronousMessageEventImpl
