/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Message;
import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asynchronous Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedActiveMSDs <em>Progressed Active MS Ds</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent#getProgressedMessages <em>Progressed Messages</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousMessageEvent()
 * @model
 * @generated
 */
public interface AsynchronousMessageEvent extends MessageEvent {

	/**
	 * Returns the value of the '<em><b>Progressed Active MS Ds</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.timed.timedruntime.TimedActiveMSD}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Progressed Active MS Ds</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progressed Active MS Ds</em>' reference list.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousMessageEvent_ProgressedActiveMSDs()
	 * @model
	 * @generated
	 */
	EList<TimedActiveMSD> getProgressedActiveMSDs();

	/**
	 * Returns the value of the '<em><b>Progressed Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Progressed Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progressed Messages</em>' reference list.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousMessageEvent_ProgressedMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getProgressedMessages();
} // AsynchronousMessageEvent
