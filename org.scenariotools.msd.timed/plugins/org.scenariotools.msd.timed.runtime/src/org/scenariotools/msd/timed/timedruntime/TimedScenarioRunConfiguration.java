/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timed Scenario Run Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimedScenarioRunConfiguration()
 * @model
 * @generated
 */
public interface TimedScenarioRunConfiguration extends ScenarioRunConfiguration {

} // TimedScenarioRunConfiguration
