/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.uml2.uml.CombinedFragment;
import org.scenariotools.events.impl.EventImpl;

import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Condition Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getOperatorString <em>Operator String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getCompareValue <em>Compare Value</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getClockCondition <em>Clock Condition</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getActiveMSDCut <em>Active MSD Cut</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#isConstraintFulfilled <em>Constraint Fulfilled</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#getClockObservers <em>Clock Observers</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#isTimedControllable <em>Timed Controllable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeConditionEventImpl#isTimedControllableSetManually <em>Timed Controllable Set Manually</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeConditionEventImpl extends EventImpl implements TimeConditionEvent {
	/**
	 * The default value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected static final String CLOCK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected String clockName = CLOCK_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorString() <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorString()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATOR_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperatorString() <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorString()
	 * @generated
	 * @ordered
	 */
	protected String operatorString = OPERATOR_STRING_EDEFAULT;

	/**
	 * The default value of the '{@link #getCompareValue() <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareValue()
	 * @generated
	 * @ordered
	 */
	protected static final int COMPARE_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCompareValue() <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareValue()
	 * @generated
	 * @ordered
	 */
	protected int compareValue = COMPARE_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClockCondition() <em>Clock Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockCondition()
	 * @generated
	 * @ordered
	 */
	protected CombinedFragment clockCondition;

	/**
	 * The cached value of the '{@link #getActiveMSDCut() <em>Active MSD Cut</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveMSDCut()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSDCut activeMSDCut;

	/**
	 * The default value of the '{@link #isConstraintFulfilled() <em>Constraint Fulfilled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstraintFulfilled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONSTRAINT_FULFILLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConstraintFulfilled() <em>Constraint Fulfilled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstraintFulfilled()
	 * @generated
	 * @ordered
	 */
	protected boolean constraintFulfilled = CONSTRAINT_FULFILLED_EDEFAULT;

	/**
	 * The default value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected boolean hot = HOT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClockObservers() <em>Clock Observers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockObservers()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> clockObservers;

	/**
	 * The default value of the '{@link #isTimedControllable() <em>Timed Controllable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedControllable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMED_CONTROLLABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTimedControllable() <em>Timed Controllable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedControllable()
	 * @generated
	 * @ordered
	 */
	protected boolean timedControllable = TIMED_CONTROLLABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isTimedControllableSetManually() <em>Timed Controllable Set Manually</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedControllableSetManually()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TIMED_CONTROLLABLE_SET_MANUALLY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTimedControllableSetManually() <em>Timed Controllable Set Manually</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTimedControllableSetManually()
	 * @generated
	 * @ordered
	 */
	protected boolean timedControllableSetManually = TIMED_CONTROLLABLE_SET_MANUALLY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeConditionEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIME_CONDITION_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClockName() {
		return clockName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClockName(String newClockName) {
		String oldClockName = clockName;
		clockName = newClockName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME, oldClockName, clockName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperatorString() {
		return operatorString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatorString(String newOperatorString) {
		String oldOperatorString = operatorString;
		operatorString = newOperatorString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING, oldOperatorString, operatorString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCompareValue() {
		return compareValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompareValue(int newCompareValue) {
		int oldCompareValue = compareValue;
		compareValue = newCompareValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE, oldCompareValue, compareValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CombinedFragment getClockCondition() {
		if (clockCondition != null && clockCondition.eIsProxy()) {
			InternalEObject oldClockCondition = (InternalEObject)clockCondition;
			clockCondition = (CombinedFragment)eResolveProxy(oldClockCondition);
			if (clockCondition != oldClockCondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION, oldClockCondition, clockCondition));
			}
		}
		return clockCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CombinedFragment basicGetClockCondition() {
		return clockCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClockCondition(CombinedFragment newClockCondition) {
		CombinedFragment oldClockCondition = clockCondition;
		clockCondition = newClockCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION, oldClockCondition, clockCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDCut getActiveMSDCut() {
		if (activeMSDCut != null && activeMSDCut.eIsProxy()) {
			InternalEObject oldActiveMSDCut = (InternalEObject)activeMSDCut;
			activeMSDCut = (ActiveMSDCut)eResolveProxy(oldActiveMSDCut);
			if (activeMSDCut != oldActiveMSDCut) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT, oldActiveMSDCut, activeMSDCut));
			}
		}
		return activeMSDCut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDCut basicGetActiveMSDCut() {
		return activeMSDCut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveMSDCut(ActiveMSDCut newActiveMSDCut) {
		ActiveMSDCut oldActiveMSDCut = activeMSDCut;
		activeMSDCut = newActiveMSDCut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT, oldActiveMSDCut, activeMSDCut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstraintFulfilled() {
		return constraintFulfilled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintFulfilled(boolean newConstraintFulfilled) {
		boolean oldConstraintFulfilled = constraintFulfilled;
		constraintFulfilled = newConstraintFulfilled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED, oldConstraintFulfilled, constraintFulfilled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHot() {
		return hot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHot(boolean newHot) {
		boolean oldHot = hot;
		hot = newHot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__HOT, oldHot, hot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getClockObservers() {
		if (clockObservers == null) {
			clockObservers = new EObjectResolvingEList<EObject>(EObject.class, this, TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_OBSERVERS);
		}
		return clockObservers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimedControllable() {
		return timedControllable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimedControllable(boolean newTimedControllable) {
		boolean oldTimedControllable = timedControllable;
		timedControllable = newTimedControllable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE, oldTimedControllable, timedControllable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTimedControllableSetManually() {
		return timedControllableSetManually;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimedControllableSetManually(boolean newTimedControllableSetManually) {
		boolean oldTimedControllableSetManually = timedControllableSetManually;
		timedControllableSetManually = newTimedControllableSetManually;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY, oldTimedControllableSetManually, timedControllableSetManually));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME:
				return getClockName();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING:
				return getOperatorString();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE:
				return getCompareValue();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION:
				if (resolve) return getClockCondition();
				return basicGetClockCondition();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT:
				if (resolve) return getActiveMSDCut();
				return basicGetActiveMSDCut();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED:
				return isConstraintFulfilled();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__HOT:
				return isHot();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_OBSERVERS:
				return getClockObservers();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE:
				return isTimedControllable();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY:
				return isTimedControllableSetManually();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME:
				setClockName((String)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING:
				setOperatorString((String)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE:
				setCompareValue((Integer)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION:
				setClockCondition((CombinedFragment)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT:
				setActiveMSDCut((ActiveMSDCut)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED:
				setConstraintFulfilled((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__HOT:
				setHot((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_OBSERVERS:
				getClockObservers().clear();
				getClockObservers().addAll((Collection<? extends EObject>)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE:
				setTimedControllable((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY:
				setTimedControllableSetManually((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME:
				setClockName(CLOCK_NAME_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING:
				setOperatorString(OPERATOR_STRING_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE:
				setCompareValue(COMPARE_VALUE_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION:
				setClockCondition((CombinedFragment)null);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT:
				setActiveMSDCut((ActiveMSDCut)null);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED:
				setConstraintFulfilled(CONSTRAINT_FULFILLED_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__HOT:
				setHot(HOT_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_OBSERVERS:
				getClockObservers().clear();
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE:
				setTimedControllable(TIMED_CONTROLLABLE_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY:
				setTimedControllableSetManually(TIMED_CONTROLLABLE_SET_MANUALLY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_NAME:
				return CLOCK_NAME_EDEFAULT == null ? clockName != null : !CLOCK_NAME_EDEFAULT.equals(clockName);
			case TimedRuntimePackage.TIME_CONDITION_EVENT__OPERATOR_STRING:
				return OPERATOR_STRING_EDEFAULT == null ? operatorString != null : !OPERATOR_STRING_EDEFAULT.equals(operatorString);
			case TimedRuntimePackage.TIME_CONDITION_EVENT__COMPARE_VALUE:
				return compareValue != COMPARE_VALUE_EDEFAULT;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_CONDITION:
				return clockCondition != null;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__ACTIVE_MSD_CUT:
				return activeMSDCut != null;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED:
				return constraintFulfilled != CONSTRAINT_FULFILLED_EDEFAULT;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__HOT:
				return hot != HOT_EDEFAULT;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__CLOCK_OBSERVERS:
				return clockObservers != null && !clockObservers.isEmpty();
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE:
				return timedControllable != TIMED_CONTROLLABLE_EDEFAULT;
			case TimedRuntimePackage.TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY:
				return timedControllableSetManually != TIMED_CONTROLLABLE_SET_MANUALLY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return (isConstraintFulfilled()?"":"not ")+getClockName()+getOperatorString()+getCompareValue();
	}

} //TimeConditionEventImpl
