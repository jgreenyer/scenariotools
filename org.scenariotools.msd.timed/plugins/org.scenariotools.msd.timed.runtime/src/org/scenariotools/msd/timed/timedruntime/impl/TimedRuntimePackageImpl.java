/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.timed.timedruntime.AsynchronousMessageEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.timed.timedruntime.TimedScenarioRunConfiguration;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimedRuntimePackageImpl extends EPackageImpl implements TimedRuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedMSDRuntimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedActiveMSDEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedMSDRuntimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedRuntimeUtilEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeConditionEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedCommunicationLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asynchronousMessageEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asynchronousSendEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asynchronousReceiveEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedScenarioRunConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeRestrictedMessageEventEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TimedRuntimePackageImpl() {
		super(eNS_URI, TimedRuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TimedRuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TimedRuntimePackage init() {
		if (isInited) return (TimedRuntimePackage)EPackage.Registry.INSTANCE.getEPackage(TimedRuntimePackage.eNS_URI);

		// Obtain or create and register package
		TimedRuntimePackageImpl theTimedRuntimePackage = (TimedRuntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TimedRuntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TimedRuntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();
		ScenariorunconfigurationPackage.eINSTANCE.eClass();
		UtilPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTimedRuntimePackage.createPackageContents();

		// Initialize created meta-data
		theTimedRuntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTimedRuntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TimedRuntimePackage.eNS_URI, theTimedRuntimePackage);
		return theTimedRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedMSDRuntimeState() {
		return timedMSDRuntimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_FederationString() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_BufferString() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimingConditionsInRequirementsSatisfiable() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimingConditionsInAssumptionsSatisfiable() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimedSafetyViolationOccurredInRequirements() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimedSafetyViolationOccurredInAssumptions() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_GoodFederationString() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_BadFederationString() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_WinningFederationString() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimeInconsistencyOccurredInRequirements() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_TimeInconsistencyOccurredInAssumptions() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedMSDRuntimeState_UpperBoundRealTimeInconsistency() {
		return (EAttribute)timedMSDRuntimeStateEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedActiveMSD() {
		return timedActiveMSDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedActiveMSD_CurrentRuntimeState() {
		return (EReference)timedActiveMSDEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedActiveMSD_ClockNameToClockInstanceMap() {
		return (EReference)timedActiveMSDEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedMSDRuntimeStateGraph() {
		return timedMSDRuntimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimedMSDRuntimeStateGraph_TimeConditionEvents() {
		return (EReference)timedMSDRuntimeStateGraphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedRuntimeUtil() {
		return timedRuntimeUtilEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeConditionEvent() {
		return timeConditionEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_ClockName() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_OperatorString() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_CompareValue() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeConditionEvent_ClockCondition() {
		return (EReference)timeConditionEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeConditionEvent_ActiveMSDCut() {
		return (EReference)timeConditionEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_ConstraintFulfilled() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_Hot() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeConditionEvent_ClockObservers() {
		return (EReference)timeConditionEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_TimedControllable() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeConditionEvent_TimedControllableSetManually() {
		return (EAttribute)timeConditionEventEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedCommunicationLink() {
		return timedCommunicationLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedCommunicationLink_MinDelay() {
		return (EAttribute)timedCommunicationLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedCommunicationLink_MaxDelay() {
		return (EAttribute)timedCommunicationLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedCommunicationLink_Asynchronous() {
		return (EAttribute)timedCommunicationLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedCommunicationLink_BufferSize() {
		return (EAttribute)timedCommunicationLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsynchronousMessageEvent() {
		return asynchronousMessageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAsynchronousMessageEvent_ProgressedActiveMSDs() {
		return (EReference)asynchronousMessageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAsynchronousMessageEvent_ProgressedMessages() {
		return (EReference)asynchronousMessageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsynchronousSendEvent() {
		return asynchronousSendEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsynchronousReceiveEvent() {
		return asynchronousReceiveEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsynchronousReceiveEvent_MinDelay() {
		return (EAttribute)asynchronousReceiveEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsynchronousReceiveEvent_MaxDelay() {
		return (EAttribute)asynchronousReceiveEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsynchronousReceiveEvent_ClockName() {
		return (EAttribute)asynchronousReceiveEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedScenarioRunConfiguration() {
		return timedScenarioRunConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeRestrictedMessageEvent() {
		return timeRestrictedMessageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeRestrictedMessageEvent_ClockName() {
		return (EAttribute)timeRestrictedMessageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeRestrictedMessageEvent_MinBound() {
		return (EAttribute)timeRestrictedMessageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeRestrictedMessageEvent_MaxBound() {
		return (EAttribute)timeRestrictedMessageEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeRestrictedMessageEvent_ExcludeMinBound() {
		return (EAttribute)timeRestrictedMessageEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeRestrictedMessageEvent_ExcludeMaxBound() {
		return (EAttribute)timeRestrictedMessageEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedRuntimeFactory getTimedRuntimeFactory() {
		return (TimedRuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timedMSDRuntimeStateEClass = createEClass(TIMED_MSD_RUNTIME_STATE);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__FEDERATION_STRING);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__BUFFER_STRING);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_REQUIREMENTS_SATISFIABLE);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIMING_CONDITIONS_IN_ASSUMPTIONS_SATISFIABLE);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIMED_SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__GOOD_FEDERATION_STRING);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__BAD_FEDERATION_STRING);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__WINNING_FEDERATION_STRING);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_REQUIREMENTS);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__TIME_INCONSISTENCY_OCCURRED_IN_ASSUMPTIONS);
		createEAttribute(timedMSDRuntimeStateEClass, TIMED_MSD_RUNTIME_STATE__UPPER_BOUND_REAL_TIME_INCONSISTENCY);

		timedActiveMSDEClass = createEClass(TIMED_ACTIVE_MSD);
		createEReference(timedActiveMSDEClass, TIMED_ACTIVE_MSD__CURRENT_RUNTIME_STATE);
		createEReference(timedActiveMSDEClass, TIMED_ACTIVE_MSD__CLOCK_NAME_TO_CLOCK_INSTANCE_MAP);

		timedMSDRuntimeStateGraphEClass = createEClass(TIMED_MSD_RUNTIME_STATE_GRAPH);
		createEReference(timedMSDRuntimeStateGraphEClass, TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS);

		timedRuntimeUtilEClass = createEClass(TIMED_RUNTIME_UTIL);

		timeConditionEventEClass = createEClass(TIME_CONDITION_EVENT);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__CLOCK_NAME);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__OPERATOR_STRING);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__COMPARE_VALUE);
		createEReference(timeConditionEventEClass, TIME_CONDITION_EVENT__CLOCK_CONDITION);
		createEReference(timeConditionEventEClass, TIME_CONDITION_EVENT__ACTIVE_MSD_CUT);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__CONSTRAINT_FULFILLED);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__HOT);
		createEReference(timeConditionEventEClass, TIME_CONDITION_EVENT__CLOCK_OBSERVERS);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__TIMED_CONTROLLABLE);
		createEAttribute(timeConditionEventEClass, TIME_CONDITION_EVENT__TIMED_CONTROLLABLE_SET_MANUALLY);

		timedCommunicationLinkEClass = createEClass(TIMED_COMMUNICATION_LINK);
		createEAttribute(timedCommunicationLinkEClass, TIMED_COMMUNICATION_LINK__MIN_DELAY);
		createEAttribute(timedCommunicationLinkEClass, TIMED_COMMUNICATION_LINK__MAX_DELAY);
		createEAttribute(timedCommunicationLinkEClass, TIMED_COMMUNICATION_LINK__ASYNCHRONOUS);
		createEAttribute(timedCommunicationLinkEClass, TIMED_COMMUNICATION_LINK__BUFFER_SIZE);

		asynchronousMessageEventEClass = createEClass(ASYNCHRONOUS_MESSAGE_EVENT);
		createEReference(asynchronousMessageEventEClass, ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_ACTIVE_MS_DS);
		createEReference(asynchronousMessageEventEClass, ASYNCHRONOUS_MESSAGE_EVENT__PROGRESSED_MESSAGES);

		asynchronousSendEventEClass = createEClass(ASYNCHRONOUS_SEND_EVENT);

		asynchronousReceiveEventEClass = createEClass(ASYNCHRONOUS_RECEIVE_EVENT);
		createEAttribute(asynchronousReceiveEventEClass, ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY);
		createEAttribute(asynchronousReceiveEventEClass, ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY);
		createEAttribute(asynchronousReceiveEventEClass, ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME);

		timedScenarioRunConfigurationEClass = createEClass(TIMED_SCENARIO_RUN_CONFIGURATION);

		timeRestrictedMessageEventEClass = createEClass(TIME_RESTRICTED_MESSAGE_EVENT);
		createEAttribute(timeRestrictedMessageEventEClass, TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME);
		createEAttribute(timeRestrictedMessageEventEClass, TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND);
		createEAttribute(timeRestrictedMessageEventEClass, TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND);
		createEAttribute(timeRestrictedMessageEventEClass, TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND);
		createEAttribute(timeRestrictedMessageEventEClass, TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		UtilPackage theUtilPackage = (UtilPackage)EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		ScenariorunconfigurationPackage theScenariorunconfigurationPackage = (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timedMSDRuntimeStateEClass.getESuperTypes().add(theRuntimePackage.getMSDRuntimeState());
		timedActiveMSDEClass.getESuperTypes().add(theRuntimePackage.getActiveMSD());
		timedMSDRuntimeStateGraphEClass.getESuperTypes().add(theRuntimePackage.getMSDRuntimeStateGraph());
		timedRuntimeUtilEClass.getESuperTypes().add(theUtilPackage.getRuntimeUtil());
		timeConditionEventEClass.getESuperTypes().add(theEventsPackage.getEvent());
		timedCommunicationLinkEClass.getESuperTypes().add(theScenariorunconfigurationPackage.getCommunicationLink());
		asynchronousMessageEventEClass.getESuperTypes().add(theEventsPackage.getMessageEvent());
		asynchronousSendEventEClass.getESuperTypes().add(this.getAsynchronousMessageEvent());
		asynchronousReceiveEventEClass.getESuperTypes().add(this.getAsynchronousMessageEvent());
		timedScenarioRunConfigurationEClass.getESuperTypes().add(theScenariorunconfigurationPackage.getScenarioRunConfiguration());
		timeRestrictedMessageEventEClass.getESuperTypes().add(theEventsPackage.getSynchronousMessageEvent());

		// Initialize classes and features; add operations and parameters
		initEClass(timedMSDRuntimeStateEClass, TimedMSDRuntimeState.class, "TimedMSDRuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimedMSDRuntimeState_FederationString(), theEcorePackage.getEString(), "federationString", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_BufferString(), theEcorePackage.getEString(), "bufferString", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimingConditionsInRequirementsSatisfiable(), theEcorePackage.getEBoolean(), "timingConditionsInRequirementsSatisfiable", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimingConditionsInAssumptionsSatisfiable(), theEcorePackage.getEBoolean(), "timingConditionsInAssumptionsSatisfiable", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimedSafetyViolationOccurredInRequirements(), theEcorePackage.getEBoolean(), "timedSafetyViolationOccurredInRequirements", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimedSafetyViolationOccurredInAssumptions(), theEcorePackage.getEBoolean(), "timedSafetyViolationOccurredInAssumptions", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_GoodFederationString(), theEcorePackage.getEString(), "goodFederationString", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_BadFederationString(), theEcorePackage.getEString(), "badFederationString", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_WinningFederationString(), theEcorePackage.getEString(), "winningFederationString", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimeInconsistencyOccurredInRequirements(), theEcorePackage.getEBoolean(), "timeInconsistencyOccurredInRequirements", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_TimeInconsistencyOccurredInAssumptions(), theEcorePackage.getEBoolean(), "timeInconsistencyOccurredInAssumptions", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedMSDRuntimeState_UpperBoundRealTimeInconsistency(), theEcorePackage.getEBoolean(), "upperBoundRealTimeInconsistency", null, 0, 1, TimedMSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedActiveMSDEClass, TimedActiveMSD.class, "TimedActiveMSD", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimedActiveMSD_CurrentRuntimeState(), theRuntimePackage.getMSDRuntimeState(), null, "currentRuntimeState", null, 0, 1, TimedActiveMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimedActiveMSD_ClockNameToClockInstanceMap(), theRuntimePackage.getStringToStringMapEntry(), null, "clockNameToClockInstanceMap", null, 0, -1, TimedActiveMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedMSDRuntimeStateGraphEClass, TimedMSDRuntimeStateGraph.class, "TimedMSDRuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimedMSDRuntimeStateGraph_TimeConditionEvents(), this.getTimeConditionEvent(), null, "timeConditionEvents", null, 0, -1, TimedMSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedRuntimeUtilEClass, TimedRuntimeUtil.class, "TimedRuntimeUtil", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timeConditionEventEClass, TimeConditionEvent.class, "TimeConditionEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeConditionEvent_ClockName(), theEcorePackage.getEString(), "clockName", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_OperatorString(), theEcorePackage.getEString(), "operatorString", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_CompareValue(), theEcorePackage.getEInt(), "compareValue", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeConditionEvent_ClockCondition(), theUMLPackage.getCombinedFragment(), null, "clockCondition", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeConditionEvent_ActiveMSDCut(), theRuntimePackage.getActiveMSDCut(), null, "activeMSDCut", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_ConstraintFulfilled(), theEcorePackage.getEBoolean(), "constraintFulfilled", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_Hot(), theEcorePackage.getEBoolean(), "hot", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeConditionEvent_ClockObservers(), theEcorePackage.getEObject(), null, "clockObservers", null, 0, -1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_TimedControllable(), theEcorePackage.getEBoolean(), "timedControllable", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeConditionEvent_TimedControllableSetManually(), theEcorePackage.getEBoolean(), "timedControllableSetManually", null, 0, 1, TimeConditionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedCommunicationLinkEClass, TimedCommunicationLink.class, "TimedCommunicationLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimedCommunicationLink_MinDelay(), theEcorePackage.getEInt(), "minDelay", null, 0, 1, TimedCommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedCommunicationLink_MaxDelay(), theEcorePackage.getEInt(), "maxDelay", null, 0, 1, TimedCommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedCommunicationLink_Asynchronous(), theEcorePackage.getEBoolean(), "asynchronous", null, 0, 1, TimedCommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedCommunicationLink_BufferSize(), theEcorePackage.getEInt(), "bufferSize", null, 0, 1, TimedCommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(asynchronousMessageEventEClass, AsynchronousMessageEvent.class, "AsynchronousMessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAsynchronousMessageEvent_ProgressedActiveMSDs(), this.getTimedActiveMSD(), null, "progressedActiveMSDs", null, 0, -1, AsynchronousMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAsynchronousMessageEvent_ProgressedMessages(), theUMLPackage.getMessage(), null, "progressedMessages", null, 0, -1, AsynchronousMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(asynchronousSendEventEClass, AsynchronousSendEvent.class, "AsynchronousSendEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(asynchronousReceiveEventEClass, AsynchronousReceiveEvent.class, "AsynchronousReceiveEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAsynchronousReceiveEvent_MinDelay(), theEcorePackage.getEInt(), "minDelay", null, 0, 1, AsynchronousReceiveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsynchronousReceiveEvent_MaxDelay(), theEcorePackage.getEInt(), "maxDelay", null, 0, 1, AsynchronousReceiveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsynchronousReceiveEvent_ClockName(), theEcorePackage.getEString(), "clockName", null, 0, 1, AsynchronousReceiveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedScenarioRunConfigurationEClass, TimedScenarioRunConfiguration.class, "TimedScenarioRunConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timeRestrictedMessageEventEClass, TimeRestrictedMessageEvent.class, "TimeRestrictedMessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeRestrictedMessageEvent_ClockName(), theEcorePackage.getEString(), "clockName", null, 0, 1, TimeRestrictedMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeRestrictedMessageEvent_MinBound(), theEcorePackage.getEInt(), "minBound", null, 0, 1, TimeRestrictedMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeRestrictedMessageEvent_MaxBound(), theEcorePackage.getEInt(), "maxBound", null, 0, 1, TimeRestrictedMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeRestrictedMessageEvent_ExcludeMinBound(), theEcorePackage.getEBoolean(), "excludeMinBound", null, 0, 1, TimeRestrictedMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeRestrictedMessageEvent_ExcludeMaxBound(), theEcorePackage.getEBoolean(), "excludeMaxBound", null, 0, 1, TimeRestrictedMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TimedRuntimePackageImpl
