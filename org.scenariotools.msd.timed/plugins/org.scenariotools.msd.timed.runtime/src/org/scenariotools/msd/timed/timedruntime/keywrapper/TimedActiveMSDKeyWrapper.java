package org.scenariotools.msd.timed.timedruntime.keywrapper;

import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDKeyWrapper;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;

public class TimedActiveMSDKeyWrapper extends ActiveMSDKeyWrapper {

	public TimedActiveMSDKeyWrapper(ActiveMSD activeMSD) {
		super(activeMSD);
		if (activeMSD instanceof TimedActiveMSD) {
			TimedActiveMSD timedMSD = ((TimedActiveMSD) activeMSD);
			addSubObject(timedMSD.getClockNameToClockInstanceMap());
		}
	}

}
