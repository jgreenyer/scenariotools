/**
 */
package org.scenariotools.msd.timed.timedruntime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asynchronous Send Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getAsynchronousSendEvent()
 * @model
 * @generated
 */
public interface AsynchronousSendEvent extends AsynchronousMessageEvent {
} // AsynchronousSendEvent
