/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl;

import org.scenariotools.msd.timed.timedruntime.TimedCommunicationLink;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Communication Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl#isAsynchronous <em>Asynchronous</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedCommunicationLinkImpl#getBufferSize <em>Buffer Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedCommunicationLinkImpl extends CommunicationLinkImpl implements TimedCommunicationLink {
	/**
	 * The default value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_DELAY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected int minDelay = MIN_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_DELAY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected int maxDelay = MAX_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #isAsynchronous() <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ASYNCHRONOUS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAsynchronous() <em>Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected boolean asynchronous = ASYNCHRONOUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getBufferSize() <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBufferSize()
	 * @generated
	 * @ordered
	 */
	protected static final int BUFFER_SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBufferSize() <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBufferSize()
	 * @generated
	 * @ordered
	 */
	protected int bufferSize = BUFFER_SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedCommunicationLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_COMMUNICATION_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinDelay() {
		return minDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinDelay(int newMinDelay) {
		int oldMinDelay = minDelay;
		minDelay = newMinDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MIN_DELAY, oldMinDelay, minDelay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxDelay() {
		return maxDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxDelay(int newMaxDelay) {
		int oldMaxDelay = maxDelay;
		maxDelay = newMaxDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MAX_DELAY, oldMaxDelay, maxDelay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAsynchronous() {
		return asynchronous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronous(boolean newAsynchronous) {
		boolean oldAsynchronous = asynchronous;
		asynchronous = newAsynchronous;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_COMMUNICATION_LINK__ASYNCHRONOUS, oldAsynchronous, asynchronous));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBufferSize() {
		return bufferSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBufferSize(int newBufferSize) {
		int oldBufferSize = bufferSize;
		bufferSize = newBufferSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIMED_COMMUNICATION_LINK__BUFFER_SIZE, oldBufferSize, bufferSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MIN_DELAY:
				return getMinDelay();
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MAX_DELAY:
				return getMaxDelay();
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__ASYNCHRONOUS:
				return isAsynchronous();
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__BUFFER_SIZE:
				return getBufferSize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MIN_DELAY:
				setMinDelay((Integer)newValue);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MAX_DELAY:
				setMaxDelay((Integer)newValue);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__ASYNCHRONOUS:
				setAsynchronous((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__BUFFER_SIZE:
				setBufferSize((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MIN_DELAY:
				setMinDelay(MIN_DELAY_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MAX_DELAY:
				setMaxDelay(MAX_DELAY_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__ASYNCHRONOUS:
				setAsynchronous(ASYNCHRONOUS_EDEFAULT);
				return;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__BUFFER_SIZE:
				setBufferSize(BUFFER_SIZE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MIN_DELAY:
				return minDelay != MIN_DELAY_EDEFAULT;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__MAX_DELAY:
				return maxDelay != MAX_DELAY_EDEFAULT;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__ASYNCHRONOUS:
				return asynchronous != ASYNCHRONOUS_EDEFAULT;
			case TimedRuntimePackage.TIMED_COMMUNICATION_LINK__BUFFER_SIZE:
				return bufferSize != BUFFER_SIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minDelay: ");
		result.append(minDelay);
		result.append(", maxDelay: ");
		result.append(maxDelay);
		result.append(", asynchronous: ");
		result.append(asynchronous);
		result.append(", bufferSize: ");
		result.append(bufferSize);
		result.append(')');
		return result.toString();
	}

} //TimedCommunicationLinkImpl
