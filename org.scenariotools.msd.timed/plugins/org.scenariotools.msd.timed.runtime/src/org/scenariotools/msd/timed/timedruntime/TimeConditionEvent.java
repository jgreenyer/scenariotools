/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.CombinedFragment;
import org.scenariotools.events.Event;
import org.scenariotools.msd.runtime.ActiveMSDCut;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Condition Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getOperatorString <em>Operator String</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getCompareValue <em>Compare Value</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockCondition <em>Clock Condition</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getActiveMSDCut <em>Active MSD Cut</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isConstraintFulfilled <em>Constraint Fulfilled</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockObservers <em>Clock Observers</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllable <em>Timed Controllable</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllableSetManually <em>Timed Controllable Set Manually</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent()
 * @model
 * @generated
 */
public interface TimeConditionEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name</em>' attribute.
	 * @see #setClockName(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_ClockName()
	 * @model
	 * @generated
	 */
	String getClockName();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockName <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Name</em>' attribute.
	 * @see #getClockName()
	 * @generated
	 */
	void setClockName(String value);

	/**
	 * Returns the value of the '<em><b>Operator String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator String</em>' attribute.
	 * @see #setOperatorString(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_OperatorString()
	 * @model
	 * @generated
	 */
	String getOperatorString();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getOperatorString <em>Operator String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator String</em>' attribute.
	 * @see #getOperatorString()
	 * @generated
	 */
	void setOperatorString(String value);

	/**
	 * Returns the value of the '<em><b>Compare Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compare Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compare Value</em>' attribute.
	 * @see #setCompareValue(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_CompareValue()
	 * @model
	 * @generated
	 */
	int getCompareValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getCompareValue <em>Compare Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compare Value</em>' attribute.
	 * @see #getCompareValue()
	 * @generated
	 */
	void setCompareValue(int value);

	/**
	 * Returns the value of the '<em><b>Clock Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Condition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Condition</em>' reference.
	 * @see #setClockCondition(CombinedFragment)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_ClockCondition()
	 * @model
	 * @generated
	 */
	CombinedFragment getClockCondition();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getClockCondition <em>Clock Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Condition</em>' reference.
	 * @see #getClockCondition()
	 * @generated
	 */
	void setClockCondition(CombinedFragment value);

	/**
	 * Returns the value of the '<em><b>Active MSD Cut</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Cut</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Cut</em>' reference.
	 * @see #setActiveMSDCut(ActiveMSDCut)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_ActiveMSDCut()
	 * @model
	 * @generated
	 */
	ActiveMSDCut getActiveMSDCut();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#getActiveMSDCut <em>Active MSD Cut</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active MSD Cut</em>' reference.
	 * @see #getActiveMSDCut()
	 * @generated
	 */
	void setActiveMSDCut(ActiveMSDCut value);

	/**
	 * Returns the value of the '<em><b>Constraint Fulfilled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Fulfilled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Fulfilled</em>' attribute.
	 * @see #setConstraintFulfilled(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_ConstraintFulfilled()
	 * @model
	 * @generated
	 */
	boolean isConstraintFulfilled();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isConstraintFulfilled <em>Constraint Fulfilled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint Fulfilled</em>' attribute.
	 * @see #isConstraintFulfilled()
	 * @generated
	 */
	void setConstraintFulfilled(boolean value);

	/**
	 * Returns the value of the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hot</em>' attribute.
	 * @see #setHot(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_Hot()
	 * @model
	 * @generated
	 */
	boolean isHot();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isHot <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hot</em>' attribute.
	 * @see #isHot()
	 * @generated
	 */
	void setHot(boolean value);

	/**
	 * Returns the value of the '<em><b>Clock Observers</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Observers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Observers</em>' reference list.
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_ClockObservers()
	 * @model
	 * @generated
	 */
	EList<EObject> getClockObservers();

	/**
	 * Returns the value of the '<em><b>Timed Controllable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Controllable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Controllable</em>' attribute.
	 * @see #setTimedControllable(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_TimedControllable()
	 * @model
	 * @generated
	 */
	boolean isTimedControllable();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllable <em>Timed Controllable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timed Controllable</em>' attribute.
	 * @see #isTimedControllable()
	 * @generated
	 */
	void setTimedControllable(boolean value);

	/**
	 * Returns the value of the '<em><b>Timed Controllable Set Manually</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Controllable Set Manually</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Controllable Set Manually</em>' attribute.
	 * @see #setTimedControllableSetManually(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeConditionEvent_TimedControllableSetManually()
	 * @model
	 * @generated
	 */
	boolean isTimedControllableSetManually();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeConditionEvent#isTimedControllableSetManually <em>Timed Controllable Set Manually</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timed Controllable Set Manually</em>' attribute.
	 * @see #isTimedControllableSetManually()
	 * @generated
	 */
	void setTimedControllableSetManually(boolean value);

} // TimeConditionEvent
