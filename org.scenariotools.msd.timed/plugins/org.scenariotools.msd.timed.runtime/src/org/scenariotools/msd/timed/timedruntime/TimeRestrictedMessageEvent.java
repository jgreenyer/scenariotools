/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.scenariotools.events.SynchronousMessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Restricted Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMinBound <em>Min Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMaxBound <em>Max Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMinBound <em>Exclude Min Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMaxBound <em>Exclude Max Bound</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent()
 * @model
 * @generated
 */
public interface TimeRestrictedMessageEvent extends SynchronousMessageEvent {
	/**
	 * Returns the value of the '<em><b>Clock Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock Name</em>' attribute.
	 * @see #setClockName(String)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent_ClockName()
	 * @model
	 * @generated
	 */
	String getClockName();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getClockName <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock Name</em>' attribute.
	 * @see #getClockName()
	 * @generated
	 */
	void setClockName(String value);

	/**
	 * Returns the value of the '<em><b>Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Bound</em>' attribute.
	 * @see #setMinBound(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent_MinBound()
	 * @model
	 * @generated
	 */
	int getMinBound();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMinBound <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Bound</em>' attribute.
	 * @see #getMinBound()
	 * @generated
	 */
	void setMinBound(int value);

	/**
	 * Returns the value of the '<em><b>Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Bound</em>' attribute.
	 * @see #setMaxBound(int)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent_MaxBound()
	 * @model
	 * @generated
	 */
	int getMaxBound();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#getMaxBound <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Bound</em>' attribute.
	 * @see #getMaxBound()
	 * @generated
	 */
	void setMaxBound(int value);

	/**
	 * Returns the value of the '<em><b>Exclude Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclude Min Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclude Min Bound</em>' attribute.
	 * @see #setExcludeMinBound(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent_ExcludeMinBound()
	 * @model
	 * @generated
	 */
	boolean isExcludeMinBound();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMinBound <em>Exclude Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exclude Min Bound</em>' attribute.
	 * @see #isExcludeMinBound()
	 * @generated
	 */
	void setExcludeMinBound(boolean value);

	/**
	 * Returns the value of the '<em><b>Exclude Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclude Max Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclude Max Bound</em>' attribute.
	 * @see #setExcludeMaxBound(boolean)
	 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage#getTimeRestrictedMessageEvent_ExcludeMaxBound()
	 * @model
	 * @generated
	 */
	boolean isExcludeMaxBound();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent#isExcludeMaxBound <em>Exclude Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exclude Max Bound</em>' attribute.
	 * @see #isExcludeMaxBound()
	 * @generated
	 */
	void setExcludeMaxBound(boolean value);

} // TimeRestrictedMessageEvent
