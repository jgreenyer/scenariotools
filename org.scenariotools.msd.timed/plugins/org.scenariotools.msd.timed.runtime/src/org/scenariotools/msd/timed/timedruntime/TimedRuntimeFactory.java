/**
 */
package org.scenariotools.msd.timed.timedruntime;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage
 * @generated
 */
public interface TimedRuntimeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimedRuntimeFactory eINSTANCE = org.scenariotools.msd.timed.timedruntime.impl.TimedRuntimeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Timed MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed MSD Runtime State</em>'.
	 * @generated
	 */
	TimedMSDRuntimeState createTimedMSDRuntimeState();

	/**
	 * Returns a new object of class '<em>Timed Active MSD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Active MSD</em>'.
	 * @generated
	 */
	TimedActiveMSD createTimedActiveMSD();

	/**
	 * Returns a new object of class '<em>Timed MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed MSD Runtime State Graph</em>'.
	 * @generated
	 */
	TimedMSDRuntimeStateGraph createTimedMSDRuntimeStateGraph();

	/**
	 * Returns a new object of class '<em>Util</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Util</em>'.
	 * @generated
	 */
	TimedRuntimeUtil createTimedRuntimeUtil();

	/**
	 * Returns a new object of class '<em>Time Condition Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Condition Event</em>'.
	 * @generated
	 */
	TimeConditionEvent createTimeConditionEvent();

	/**
	 * Returns a new object of class '<em>Timed Communication Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Communication Link</em>'.
	 * @generated
	 */
	TimedCommunicationLink createTimedCommunicationLink();

	/**
	 * Returns a new object of class '<em>Asynchronous Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asynchronous Message Event</em>'.
	 * @generated
	 */
	AsynchronousMessageEvent createAsynchronousMessageEvent();

	/**
	 * Returns a new object of class '<em>Asynchronous Send Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asynchronous Send Event</em>'.
	 * @generated
	 */
	AsynchronousSendEvent createAsynchronousSendEvent();

	/**
	 * Returns a new object of class '<em>Asynchronous Receive Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asynchronous Receive Event</em>'.
	 * @generated
	 */
	AsynchronousReceiveEvent createAsynchronousReceiveEvent();

	/**
	 * Returns a new object of class '<em>Timed Scenario Run Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Scenario Run Configuration</em>'.
	 * @generated
	 */
	TimedScenarioRunConfiguration createTimedScenarioRunConfiguration();

	/**
	 * Returns a new object of class '<em>Time Restricted Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Restricted Message Event</em>'.
	 * @generated
	 */
	TimeRestrictedMessageEvent createTimeRestrictedMessageEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TimedRuntimePackage getTimedRuntimePackage();

} //TimedRuntimeFactory
