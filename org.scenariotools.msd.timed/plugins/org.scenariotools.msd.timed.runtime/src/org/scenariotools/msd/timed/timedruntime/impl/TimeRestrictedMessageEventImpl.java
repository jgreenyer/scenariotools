/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.events.impl.SynchronousMessageEventImpl;
import org.scenariotools.msd.timed.timedruntime.TimeRestrictedMessageEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Restricted Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl#getClockName <em>Clock Name</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl#getMinBound <em>Min Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl#getMaxBound <em>Max Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl#isExcludeMinBound <em>Exclude Min Bound</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimeRestrictedMessageEventImpl#isExcludeMaxBound <em>Exclude Max Bound</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeRestrictedMessageEventImpl extends SynchronousMessageEventImpl implements TimeRestrictedMessageEvent {
	/**
	 * The default value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected static final String CLOCK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected String clockName = CLOCK_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinBound() <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinBound()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinBound() <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinBound()
	 * @generated
	 * @ordered
	 */
	protected int minBound = MIN_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxBound() <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBound()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxBound() <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBound()
	 * @generated
	 * @ordered
	 */
	protected int maxBound = MAX_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #isExcludeMinBound() <em>Exclude Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExcludeMinBound()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXCLUDE_MIN_BOUND_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExcludeMinBound() <em>Exclude Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExcludeMinBound()
	 * @generated
	 * @ordered
	 */
	protected boolean excludeMinBound = EXCLUDE_MIN_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #isExcludeMaxBound() <em>Exclude Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExcludeMaxBound()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXCLUDE_MAX_BOUND_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExcludeMaxBound() <em>Exclude Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExcludeMaxBound()
	 * @generated
	 * @ordered
	 */
	protected boolean excludeMaxBound = EXCLUDE_MAX_BOUND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeRestrictedMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIME_RESTRICTED_MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClockName() {
		return clockName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClockName(String newClockName) {
		String oldClockName = clockName;
		clockName = newClockName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME, oldClockName, clockName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinBound() {
		return minBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinBound(int newMinBound) {
		int oldMinBound = minBound;
		minBound = newMinBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND, oldMinBound, minBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxBound() {
		return maxBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxBound(int newMaxBound) {
		int oldMaxBound = maxBound;
		maxBound = newMaxBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND, oldMaxBound, maxBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExcludeMinBound() {
		return excludeMinBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExcludeMinBound(boolean newExcludeMinBound) {
		boolean oldExcludeMinBound = excludeMinBound;
		excludeMinBound = newExcludeMinBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND, oldExcludeMinBound, excludeMinBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExcludeMaxBound() {
		return excludeMaxBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExcludeMaxBound(boolean newExcludeMaxBound) {
		boolean oldExcludeMaxBound = excludeMaxBound;
		excludeMaxBound = newExcludeMaxBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND, oldExcludeMaxBound, excludeMaxBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME:
				return getClockName();
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND:
				return getMinBound();
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND:
				return getMaxBound();
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND:
				return isExcludeMinBound();
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND:
				return isExcludeMaxBound();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME:
				setClockName((String)newValue);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND:
				setMinBound((Integer)newValue);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND:
				setMaxBound((Integer)newValue);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND:
				setExcludeMinBound((Boolean)newValue);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND:
				setExcludeMaxBound((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME:
				setClockName(CLOCK_NAME_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND:
				setMinBound(MIN_BOUND_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND:
				setMaxBound(MAX_BOUND_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND:
				setExcludeMinBound(EXCLUDE_MIN_BOUND_EDEFAULT);
				return;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND:
				setExcludeMaxBound(EXCLUDE_MAX_BOUND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__CLOCK_NAME:
				return CLOCK_NAME_EDEFAULT == null ? clockName != null : !CLOCK_NAME_EDEFAULT.equals(clockName);
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MIN_BOUND:
				return minBound != MIN_BOUND_EDEFAULT;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__MAX_BOUND:
				return maxBound != MAX_BOUND_EDEFAULT;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MIN_BOUND:
				return excludeMinBound != EXCLUDE_MIN_BOUND_EDEFAULT;
			case TimedRuntimePackage.TIME_RESTRICTED_MESSAGE_EVENT__EXCLUDE_MAX_BOUND:
				return excludeMaxBound != EXCLUDE_MAX_BOUND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (clockName: ");
		result.append(clockName);
		result.append(", minBound: ");
		result.append(minBound);
		result.append(", maxBound: ");
		result.append(maxBound);
		result.append(", excludeMinBound: ");
		result.append(excludeMinBound);
		result.append(", excludeMaxBound: ");
		result.append(excludeMaxBound);
		result.append(')');
		return result.toString();
	}

} //TimeRestrictedMessageEventImpl
