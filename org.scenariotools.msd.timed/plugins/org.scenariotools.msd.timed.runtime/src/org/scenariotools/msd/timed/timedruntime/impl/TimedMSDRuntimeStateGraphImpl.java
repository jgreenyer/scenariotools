/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.timed.timedruntime.TimeConditionEvent;
import org.scenariotools.msd.timed.timedruntime.TimedActiveMSD;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeStateGraph;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeFactory;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimeUtil.TimedMSDRuntimeStateCopier;
import org.scenariotools.msd.timed.timedruntime.keywrapper.TimedMSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.RuntimeUtil.MSDRuntimeStateCopier;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

import org.muml.udbm.Federation;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Timed MSD Runtime State Graph</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.TimedMSDRuntimeStateGraphImpl#getTimeConditionEvents <em>Time Condition Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedMSDRuntimeStateGraphImpl extends MSDRuntimeStateGraphImpl
		implements TimedMSDRuntimeStateGraph {
	/**
	 * The cached value of the '{@link #getTimeConditionEvents()
	 * <em>Time Condition Events</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTimeConditionEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<TimeConditionEvent> timeConditionEvents;
	private HashMap<Long, List<Federation>> federations;

	@Override
	protected TimedRuntimeUtil createRuntimeUtil() {
		return TimedRuntimeFactory.eINSTANCE.createTimedRuntimeUtil();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected TimedMSDRuntimeStateGraphImpl() {
		super();
		federations = new HashMap<>();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public TimedMSDRuntimeState init(
			ScenarioRunConfiguration scenarioRunConfiguration) {
		TimedRuntimeUtil runtimeUtil = createRuntimeUtil();
		runtimeUtil.init(scenarioRunConfiguration.getCommunicationLinks());
		TimedMSDRuntimeState initialState = (TimedMSDRuntimeState) super.init(
				scenarioRunConfiguration, runtimeUtil);
		return initialState;
	}

	@Override
	public EList<Transition> generateAllSuccessors(RuntimeState runtimeState) {
		EList<Transition> allTransitions = new BasicEList<Transition>();

		TimedMSDRuntimeState timedMSDRuntimeState = (TimedMSDRuntimeState) runtimeState;
		for (TimeConditionEvent event : timedMSDRuntimeState.getCurrentTimeConditionEvents()) {
			if (!(event.isHot() && event.getOperatorString().contains("<") && !event.isConstraintFulfilled() && RuntimeUtil.Helper.isEnvironmentAssumption(event.getClockCondition().getEnclosingInteraction()))){
				if (!timedMSDRuntimeState.isSafetyViolationOccurredInAssumptions() 
					&& !timedMSDRuntimeState.isSafetyViolationOccurredInRequirements()
					&& !timedMSDRuntimeState.isTimedSafetyViolationOccurredInAssumptions()
					&& !timedMSDRuntimeState.isTimedSafetyViolationOccurredInRequirements()){
					allTransitions.add(generateSuccessor(runtimeState, event));
				}
			}
		}

		for (MessageEvent messageEvent : runtimeState.getMessageEventToModalMessageEventMap().keySet()) {
			if (messageEvent.isConcrete() && !(runtimeState.getMessageEventToModalMessageEventMap().get(messageEvent).getAssumptionsModality().isSafetyViolating() && !runtimeState.getObjectSystem().isControllable(messageEvent.getSendingObject()))){
				if (!runtimeState.isSafetyViolationOccurredInAssumptions() 
						&& !runtimeState.isSafetyViolationOccurredInRequirements()
						&& !((TimedMSDRuntimeState) runtimeState).isTimedSafetyViolationOccurredInAssumptions()
						&& !((TimedMSDRuntimeState) runtimeState).isTimedSafetyViolationOccurredInRequirements()){
					allTransitions.add(generateSuccessor(runtimeState, messageEvent));
				}
			}
		}
		return allTransitions;
	}

	@Override
	public Transition generateSuccessor(RuntimeState state, Event event) {
		Transition result = super.generateSuccessor(state, event);
		if (event instanceof TimeConditionEvent)
			this.getTimeConditionEvents().add((TimeConditionEvent) event);

		// tidy up temporary references of all processes
		for (ActiveProcess process : getElementContainer().getActiveProcesses()) {
			if (process instanceof TimedActiveMSD)
				((TimedActiveMSD) process).setCurrentRuntimeState(null);
		}

		return result;
	}

	@Override
	protected MSDRuntimeStateKeyWrapper createMSDRuntimeStateKeyWrapper(
			MSDRuntimeState msdRuntimeState) {
		// System.out.println("---creating wrapper for state---");
		// System.out.println("\tfedString: "+((TimedMSDRuntimeState)
		// msdRuntimeState).getFederationString());
		MSDRuntimeStateKeyWrapper wrapper = new TimedMSDRuntimeStateKeyWrapper(
				msdRuntimeState);
		// System.out.println("\thashCode: "+wrapper.hashCode());
		// System.out.println("------");
		return wrapper;
	}

	@Override
	protected void lookUpStateConstituents(MSDRuntimeState msdRuntimeState,
			EList<ActiveProcess> unchangedActiveMSDsEList) {
		List<ActiveProcess> originalProcesses = new LinkedList<>(
				msdRuntimeState.getActiveProcesses());
		super.lookUpStateConstituents(msdRuntimeState, unchangedActiveMSDsEList);
		for (ActiveProcess activeProcess : originalProcesses) {
			if (activeProcess instanceof TimedActiveMSD) {
				TimedActiveMSD containedProcess = (TimedActiveMSD) getElementContainer()
						.getActiveProcess(activeProcess);
//				containedProcess
//						.copyClocksFromTimedActiveMSD((TimedActiveMSD) activeProcess);
				containedProcess.setCurrentRuntimeState(msdRuntimeState);
			}
		}
		TimedMSDRuntimeState timedState = ((TimedMSDRuntimeState) msdRuntimeState);
		timedState.setFederation(getFederation(timedState.getFederation()));
	}

	private long getHashCode(Federation federation) {
		return federation.getFullHash();
	}

	@Override
	protected void initializeInitialMSDRuntimeState(
			MSDRuntimeState initialMSDRuntimeState,
			ExecutionSemantics executionSemantics) {
		((TimedMSDRuntimeState) initialMSDRuntimeState).initQueue();
		super.initializeInitialMSDRuntimeState(initialMSDRuntimeState,
				executionSemantics);
	}

	/**
	 * @Generated NOT
	 */
	public Federation getFederation(Federation federation) {
//		System.out.println("getFederation("+federation+"[hashCode="+federation.hashCode()+"]):");
//		System.out.println("\thashCode: "+federation.hashCode());
		long hashCode = getHashCode(federation);
//		System.out.println("\thash: "+hashCode);
		List<Federation> sameKeyFeds = federations.get(hashCode);
		if (sameKeyFeds == null) {
			sameKeyFeds = new LinkedList<>();
			federations.put(hashCode, sameKeyFeds);
		}
		for (Federation candidate : sameKeyFeds) {
			if (candidate.equals(federation)) {
//				System.out.println("\tfederation is OLD");
//				System.out.println("\treturning "+candidate+", hashCode: "+candidate.hashCode());
				return candidate;
			}
		}
		// no match
		sameKeyFeds.add(federation);
		 if(sameKeyFeds.size()>1){
//		 System.out.println("\tcollision!");
		 for(Federation fed:sameKeyFeds){
//		 System.out.println("\t\tfed: "+fed+", hashCode:"+fed.hashCode());
		 }
		 }
//		System.out.println("\tfederation is NEW");
		return federation;
	}

	@Override
	protected MSDRuntimeState createMSDRuntimeState() {
		TimedMSDRuntimeState result = TimedRuntimeFactory.eINSTANCE
				.createTimedMSDRuntimeState();
		result.setStateGraph(this);
		return result;
	}

	@Override
	protected MSDRuntimeStateCopier createMSDRuntimeStateCopier() {
		return new TimedMSDRuntimeStateCopier();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.TIMED_MSD_RUNTIME_STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimeConditionEvent> getTimeConditionEvents() {
		if (timeConditionEvents == null) {
			timeConditionEvents = new EObjectContainmentEList<TimeConditionEvent>(TimeConditionEvent.class, this, TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS);
		}
		return timeConditionEvents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				return ((InternalEList<?>)getTimeConditionEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				return getTimeConditionEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				getTimeConditionEvents().clear();
				getTimeConditionEvents().addAll((Collection<? extends TimeConditionEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				getTimeConditionEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.TIMED_MSD_RUNTIME_STATE_GRAPH__TIME_CONDITION_EVENTS:
				return timeConditionEvents != null && !timeConditionEvents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // TimedMSDRuntimeStateGraphImpl
