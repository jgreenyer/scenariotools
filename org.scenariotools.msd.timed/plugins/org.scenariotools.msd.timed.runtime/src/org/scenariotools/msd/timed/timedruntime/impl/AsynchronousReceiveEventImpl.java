/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.msd.timed.timedruntime.AsynchronousReceiveEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Asynchronous Receive Event</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl#getMinDelay <em>Min Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl#getMaxDelay <em>Max Delay</em>}</li>
 *   <li>{@link org.scenariotools.msd.timed.timedruntime.impl.AsynchronousReceiveEventImpl#getClockName <em>Clock Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AsynchronousReceiveEventImpl extends AsynchronousMessageEventImpl
		implements AsynchronousReceiveEvent {
	/**
	 * The default value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_DELAY_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getMinDelay() <em>Min Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMinDelay()
	 * @generated
	 * @ordered
	 */
	protected int minDelay = MIN_DELAY_EDEFAULT;
	/**
	 * The default value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_DELAY_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getMaxDelay() <em>Max Delay</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMaxDelay()
	 * @generated
	 * @ordered
	 */
	protected int maxDelay = MAX_DELAY_EDEFAULT;
	/**
	 * The default value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected static final String CLOCK_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getClockName() <em>Clock Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClockName()
	 * @generated
	 * @ordered
	 */
	protected String clockName = CLOCK_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AsynchronousReceiveEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.ASYNCHRONOUS_RECEIVE_EVENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getClockName() {
		return clockName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClockName(String newClockName) {
		String oldClockName = clockName;
		clockName = newClockName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME, oldClockName, clockName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinDelay() {
		return minDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinDelay(int newMinDelay) {
		int oldMinDelay = minDelay;
		minDelay = newMinDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY, oldMinDelay, minDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxDelay() {
		return maxDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxDelay(int newMaxDelay) {
		int oldMaxDelay = maxDelay;
		maxDelay = newMaxDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY, oldMaxDelay, maxDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY:
				return getMinDelay();
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY:
				return getMaxDelay();
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME:
				return getClockName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY:
				setMinDelay((Integer)newValue);
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY:
				setMaxDelay((Integer)newValue);
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME:
				setClockName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY:
				setMinDelay(MIN_DELAY_EDEFAULT);
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY:
				setMaxDelay(MAX_DELAY_EDEFAULT);
				return;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME:
				setClockName(CLOCK_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MIN_DELAY:
				return minDelay != MIN_DELAY_EDEFAULT;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__MAX_DELAY:
				return maxDelay != MAX_DELAY_EDEFAULT;
			case TimedRuntimePackage.ASYNCHRONOUS_RECEIVE_EVENT__CLOCK_NAME:
				return CLOCK_NAME_EDEFAULT == null ? clockName != null : !CLOCK_NAME_EDEFAULT.equals(clockName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return "async receive: "
				+ super.toString()
				+ ((minDelay != 0 || maxDelay != 0) ? "[" + minDelay + ","
						+ maxDelay + "]" : "");
	}

} // AsynchronousReceiveEventImpl
