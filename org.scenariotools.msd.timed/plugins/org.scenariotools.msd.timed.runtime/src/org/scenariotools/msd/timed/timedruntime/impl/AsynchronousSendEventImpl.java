/**
 */
package org.scenariotools.msd.timed.timedruntime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.msd.timed.timedruntime.AsynchronousSendEvent;
import org.scenariotools.msd.timed.timedruntime.TimedRuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asynchronous Send Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AsynchronousSendEventImpl extends AsynchronousMessageEventImpl implements AsynchronousSendEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AsynchronousSendEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimedRuntimePackage.Literals.ASYNCHRONOUS_SEND_EVENT;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return "async send: "+super.toString();
	}

} //AsynchronousSendEventImpl
