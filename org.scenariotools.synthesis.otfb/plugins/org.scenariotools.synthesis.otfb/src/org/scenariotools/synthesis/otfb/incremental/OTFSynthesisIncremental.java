package org.scenariotools.synthesis.otfb.incremental;

import org.scenariotools.synthesis.otfb.OTFSynthesis;

public class OTFSynthesisIncremental extends OTFSynthesis{

	@Override
	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithmIncremental();
	}
	
	protected OTFBAlgorithmIncremental getOTFBAlgorithmIncremental() {
		return (OTFBAlgorithmIncremental) getOTFBAlgorithm();
	}
	
	
}
