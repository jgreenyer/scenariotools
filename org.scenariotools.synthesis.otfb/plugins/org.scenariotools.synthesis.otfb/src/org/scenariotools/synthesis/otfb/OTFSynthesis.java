package org.scenariotools.synthesis.otfb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.StrategyKind;
import org.scenariotools.stategraph.Transition;

public class OTFSynthesis {

	protected RuntimeStateGraph newController;
	protected OTFBAlgorithm otfbAlgorithm;

	protected Map<RuntimeState, RuntimeState> specStateToControllerStateMap;

	protected boolean buechiStrategyExists;
	
	protected static Logger logger = Activator.getLogManager().getLogger(
			OTFBAlgorithm.class.getName());
	
	public OTFSynthesis() {
		this(true);
	}
	
	public OTFSynthesis(boolean includeSafetyViolatingStatesInController) {
		this.includeSafetyViolatingStatesInController = includeSafetyViolatingStatesInController;
	}
	

	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithm();
	}

	public boolean otfSynthesizeController(RuntimeStateGraph runtimeStateGraph) {

		buechiStrategyExists = otfbAlgorithm
				.OTFB((RuntimeState) runtimeStateGraph.getStartState());

		extractController();

		return buechiStrategyExists;
	}
	
	protected Set<Transition> excludedTransitions;
	
	protected Set<RuntimeState> visitedOuter;
	protected Set<RuntimeState> visitedInner;
	protected Stack<RuntimeState> stackOuter;
	protected Stack<Transition> stackInner;
	protected boolean includeSafetyViolatingStatesInController = false;
	
	/**
	 * Extracts the controller as follows
	 * 1. Perform nested DFS over state graph returned by the synthesis algorithm
	 * 		This procedure will mark certain transitions as excluded (adding them to the excludedTransitions set)
	 * 		Excluded transitions are:
	 * 		a. in case there IS a strategy:
	 * 			1. controllable transitions to losing states
	 * 			2. controllable transitions that open loops back to their source state without encountering goal states ("unlive loops") 
	 * 		b. in case there IS NO strategy:
	 * 			1. uncontrollable transitions to winning states
	 * 			2. uncontrollable transitions that open loops back to their source state with encountering goal states ("live loops")
	 *   (a.1./b.1 will be added by the outer DFS, a.2/b.2 will be added by the inner DFS)
	 * 2. Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph
	 *    
	 * (This should in visit each state in the state graph AT MOST THREE TIMES.) 
	 */
	protected void extractController() {
		if (logger.isDebugEnabled()){
			logger.debug("EXTRACTING CONTROLLER");
		}
		
		excludedTransitions = new HashSet<Transition>();
		
		visitedOuter = new HashSet<RuntimeState>();
		stackOuter = new Stack<RuntimeState>();
		visitedInner = new HashSet<RuntimeState>();
		
		outerDFS(otfbAlgorithm.getStartState());

		// Extraction by DFS traversal of state graph excluding the excludedTransitions.
		createControllerFromStateGraph();
		
	}

	/**
	 * Returns true iff 
	 * 		   strategy exists and state is not goal
	 *      OR no strategy exists and state is goal
	 *   which is equivalent to:
	 *         strategy exists XOR state is goal
	 * @param state
	 * @return
	 */
	protected boolean isRelevantStartStateForLoopDetection(State state){
		return buechiStrategyExists ^ otfbAlgorithm.getGoal().contains(state);
	}
	
	/**
	 * Returns true iff 
	 * 		   strategy exists and transition is controllable
	 *      OR no strategy exists and transition is uncontrollable
	 *   which is equivalent to:
	 *         strategy exists XOR transition is uncontrollable
	 * @param transition
	 * @return
	 */
	protected boolean canTransitionBeRemoved(Transition transition){
		return buechiStrategyExists ^ !otfbAlgorithm.isControllableWrapper(transition);
	}
	
	/**
	 * Returns true iff transition can be removed and 
	 * 		   strategy exists and target state is losing
	 * 		OR no strategy exists and target state is not losing
	 *    which is equivalent to:
	 *    	strategy exists XOR target state is not losing
	 * @param transition
	 * @return
	 */
	protected boolean mustTransitionBeRemoved(Transition transition){
		return (canTransitionBeRemoved(transition)
				&& (buechiStrategyExists ^ otfbAlgorithm.getWinAndNotLoseBuechiStates().contains(transition.getTargetState())))
			|| removeSafetyViolatingStatesInController((RuntimeState) transition.getTargetState());
	}
	
	/** 
	 * returns true if includeSafetyViolatingStatesInController is false and
	 *  a) a strategy exists and there is an assumption safety violation in the state
	 *  a) no strategy exists and there is an requirements safety violation in the state
	 * @param state
	 * @return
	 */
	private boolean removeSafetyViolatingStatesInController(RuntimeState state){
		return !includeSafetyViolatingStatesInController
				&& (buechiStrategyExists && state.isSafetyViolationOccurredInAssumptions()
						|| !buechiStrategyExists && state.isSafetyViolationOccurredInRequirements());
	}
	
	// for eliminating transitions to deadlock states
	protected Map<RuntimeState,Set<Transition>> stateToIncomingTransitionsMap;
	
	/**
	 * 
	 * @param startState
	 */
	protected void outerDFS(RuntimeState startState) {

		stackOuter.push(startState);
		visitedOuter.add(startState);
		
		stateToIncomingTransitionsMap = new HashMap<RuntimeState,Set<Transition>>();
		stateToIncomingTransitionsMap.put(startState, new HashSet<Transition>()); // no need to remember incoming transitions for start state
		
		
		while (!stackOuter.isEmpty()) {
			RuntimeState s = stackOuter.peek();
			RuntimeState unvisitedSuccessor = null;
			for (Transition t : s.getOutgoingTransition()) {
				if (excludedTransitions.contains(t)) continue;
				if (mustTransitionBeRemoved(t)){
					excludeTransition(t);
					continue;
				}
				RuntimeState targetState = (RuntimeState) t.getTargetState();
				
				if (!visitedOuter.contains(targetState)){
					// unvisited successor found:
					unvisitedSuccessor = targetState;
					
					// remember incoming transition -- not necessary for DFS, only for eliminating paths to deadlocks.
					// This elimination is triggered when popping states from the stack, see below.
					Set<Transition> incomingTransitions = stateToIncomingTransitionsMap.get(unvisitedSuccessor);
					if (incomingTransitions == null){
						incomingTransitions = new HashSet<Transition>();
						stateToIncomingTransitionsMap.put(unvisitedSuccessor, incomingTransitions);
					}
					incomingTransitions.add(t);

					// one unvisited successor found, now we continue with the surrounding while loop.
					break;
				}
			}
			if (unvisitedSuccessor != null){ // there was an unvisited successor
				stackOuter.push(unvisitedSuccessor);
				visitedOuter.add(unvisitedSuccessor);
			}else{ // there was no unvisited successor
				stackOuter.pop(); // removes s from stack
				
				 
				// continue with loop detection only if s remains reachable
				if (!excludeIncomingTransitionIfDeadlockState(s)){// this call eliminates path to deadlock
					
					// continue with loop detection
					if(isRelevantStartStateForLoopDetection(s)){
						for (Transition t : s.getOutgoingTransition()) {
							if (canTransitionBeRemoved(t) && 
									innerDFS(t)){
								excludeTransition(t);
								// s may now be a deadlock state, eliminate again 
								excludeIncomingTransitionIfDeadlockState(s);
							} 
						}
					}
				}
			}
		}
	}
	
	/**
	 * removes transitions leading to s if s is a deadlock state.
	 * 
	 * @param s
	 * @return
	 */
	private boolean excludeIncomingTransitionIfDeadlockState(
			RuntimeState s) {
		
		if (isSafetyViolatingStateThatCanStayDeadlockInController(s)){
			if (logger.isDebugEnabled()){
				logger.debug("state " + getStateIndex(s) + " can stay deadlock state");
			}
			return false;
		}
		
		int outGoingNonExcludedTransitions = s.getOutgoingTransition().size();
		for (Transition outTransition : s.getOutgoingTransition()) {
			if (excludedTransitions.contains(outTransition))
				outGoingNonExcludedTransitions--;
		}
		
		if (outGoingNonExcludedTransitions == 0){
			Set<Transition> transitionsToExclude = stateToIncomingTransitionsMap.get(s);
			assert (transitionsToExclude != null); // cannot be null if state was visited.
			excludedTransitions.addAll(transitionsToExclude);
			if (logger.isDebugEnabled()){
				logger.debug("state " + getStateIndex(s) + " must not be reachable in controller");
			}
			return true;
		}else{
			if (logger.isDebugEnabled()){
				logger.debug("state " + getStateIndex(s) + " can (for now) remain reachable in controller");
			}
			return false;
		}
	}
	
	protected boolean isSafetyViolatingStateThatCanStayDeadlockInController(RuntimeState runtimeState){
		boolean returnValue = (buechiStrategyExists && runtimeState.isSafetyViolationOccurredInAssumptions() //&& otfbAlgorithm.hasMandatoryMessageEvents(runtimeState,false)
				||!buechiStrategyExists && runtimeState.isSafetyViolationOccurredInRequirements() && !otfbAlgorithm.hasMandatoryMessageEvents(runtimeState,true));
		return returnValue;
	}

	/**
	 * The procedure is called from outerDFS:
	 * returns true if it finds cycle from the source state and via the target state of the startTransition back to the source state.
	 * 
	 * a) if a strategy exists: 
	 * 	1. the source state of startTransition is a non-goal state 
	 *  2. only cycles will be searched without goal states    
	 * 
	 * b) if no strategy exists: 
	 * 	1. the source state of startTransition is a goal state 
	 *  2. only cycles will be searched with goal states    
	 * 
	 * @return
	 */
	private boolean innerDFS(Transition startTransition) {
		if (logger.isDebugEnabled()){
			logger.debug("inner DFS called for transition : " + getTransitionString(startTransition));
		}
		
		// return true if the transition itself is a cycle
		if (startTransition.getSourceState() == startTransition.getTargetState())
			return true;
		
		// return false if target state of transition is not relevant (e.g., there is a strategy 
		// and the target state is a goal state, which means this will not be an unlive loop).
		if (!isRelevantStartStateForLoopDetection(startTransition.getTargetState()))
			return false;
		
		stackInner = new Stack<Transition>();
		stackInner.push(startTransition);
		RuntimeState startState = (RuntimeState) startTransition.getSourceState();
		
		while(!stackInner.isEmpty()){
			Transition t = stackInner.pop();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if (targetState == startState) {
				// cycle detected!
				if (logger.isDebugEnabled()){
					logger.debug("cycleDetected");
				}
				return true;
			}
			if (!visitedInner.contains(targetState)){
				for (Transition outgoingTransition : targetState.getOutgoingTransition()) {
					// only explore not already excluded transitions
					if (!excludedTransitions.contains(outgoingTransition)){
						// only explore not already excluded transitions
						if (isRelevantStartStateForLoopDetection(outgoingTransition.getTargetState())){
							if (logger.isDebugEnabled()){
								logger.debug("inner DFS -- pushing transition " + getTransitionString(outgoingTransition));
							}
							stackInner.push(outgoingTransition);							
						}else{
							if (logger.isDebugEnabled()){
								logger.debug("inner DFS -- target state not relevant for loop, stopping fwd exploration here: " +  getTransitionString(outgoingTransition));
							}
						}
					}
					else{
						if (logger.isDebugEnabled()){
							logger.debug("inner DFS -- transition already excluded: " +  getTransitionString(outgoingTransition));
						}
					}
				}
				visitedInner.add(targetState);
			}
		}

		// no cycle detected
		return false;
	}
	
	protected void excludeTransition(Transition transition){
		if (logger.isDebugEnabled()){
			logger.debug("excludeTransition " + getTransitionString(transition));
		}
		if (canTransitionBeRemoved(transition)){
			excludedTransitions.add(transition);
		}
	}	
	
	// stack is a transition-stack and not a state-stack because this simplifies the translation of transitions 
	protected Stack<Transition> stackForExtraction;
	protected Set<Event> eventsOnControllerTransition;

	/**
	 *  Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph.
	 */
	private void createControllerFromStateGraph() {
		newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();
		stackForExtraction = new Stack<Transition>();
		// keyset of this map also acts as passed set for depth-first-traversal of graph in createControllerFromStateGraph.
		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
		
		eventsOnControllerTransition = new HashSet<Event>();
		
		if(buechiStrategyExists)
			newController.setStrategyKind(StrategyKind.STRATEGY);
		else
			newController.setStrategyKind(StrategyKind.COUNTER_STRATEGY);
		
		RuntimeState startState = otfbAlgorithm.getStartState();
		RuntimeState controllerStartState = (RuntimeState) createControllerState(startState);
		specStateToControllerStateMap.put(startState, controllerStartState);
		newController.setStartState(controllerStartState);
		pushOutTransitionsOnStack(startState, stackForExtraction);
		
		while(!stackForExtraction.isEmpty()){
			Transition t = stackForExtraction.pop();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if(!specStateToControllerStateMap.containsKey(targetState)){
				RuntimeState controllerTargetState = createControllerState(targetState);
				specStateToControllerStateMap.put(targetState, controllerTargetState);
				createControllerTransition(t);
				pushOutTransitionsOnStack(targetState, stackForExtraction);
			}else{ // only translate transition
				createControllerTransition(t);
			}
		}
		Set<Event> eventsNotOnControllerTransition = new HashSet<Event>(otfbAlgorithm.getEventsOnExploredTransitions());
		eventsNotOnControllerTransition.removeAll(eventsOnControllerTransition);
		
		RuntimeState stateWithForbiddenEvents = RuntimeFactory.eINSTANCE.createRuntimeState();
		stateWithForbiddenEvents.getStringToStringAnnotationMap().put("passedIndex", "forbidden");
		newController.getStates().add(stateWithForbiddenEvents);
		
		for (Event messageEvent : eventsNotOnControllerTransition) {
			createControllerTransition(stateWithForbiddenEvents, stateWithForbiddenEvents, messageEvent);
		}
	}
	
	private RuntimeState createControllerState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();
		newControllerState.getStringToStringAnnotationMap().put("passedIndex", getStateIndex(state));
		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		if (state.getStringToBooleanAnnotationMap().get("win") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("win",
					state.getStringToBooleanAnnotationMap().get("win"));
		if (state.getStringToBooleanAnnotationMap().get("loseBuechi") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi",
					state.getStringToBooleanAnnotationMap().get("loseBuechi"));
		if (state.getStringToBooleanAnnotationMap().get("goal") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("goal",
					state.getStringToBooleanAnnotationMap().get("goal"));
		if (state.isSafetyViolationOccurredInRequirements())
			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		newController.getStates().add(newControllerState);
		return newControllerState;
	} 

	protected void createControllerTransition(Transition transition){
		createControllerTransition(
				specStateToControllerStateMap.get(transition.getSourceState()),
				specStateToControllerStateMap.get(transition.getTargetState()),
				transition.getEvent()
				);
	} 
	
	protected void createControllerTransition(State sourceControllerState, State targetControllerState, Event event){
		Transition controllerTransition = StategraphFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
		eventsOnControllerTransition.add(event);
	}
	
	protected void pushOutTransitionsOnStack(State state, Stack<Transition> stack){
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (!excludedTransitions.contains(outgoingTransition)){
				stack.push(outgoingTransition);
			}
		}
	}

	protected String getTransitionString(Transition t){
		return getStateIndex(t.getSourceState()) + " -> " + getStateIndex(t.getTargetState()) + " " + t.getLabel() ; 
	}
	
	protected String getStateIndex(State state){
		return state.getStringToStringAnnotationMap().get("passedIndex");
	}
	
	

//	protected void postProcessSuccessfulSynthesisResult() {
//		Set<RuntimeState> winAndNotLoseBuechiStates = otfbAlgorithm
//				.getWinAndNotLoseBuechiStates();
//		computeExcludedElements(winAndNotLoseBuechiStates);
//		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
//		newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();
//
//		// add start state first
//		RuntimeState newControllerStartState = RuntimeFactory.eINSTANCE
//				.createRuntimeState();
//		newControllerStartState.setObjectSystem(((RuntimeState) otfbAlgorithm
//				.getStartState()).getObjectSystem());
//		newController.getStates().add(newControllerStartState);
//		specStateToControllerStateMap.put(
//				(RuntimeState) otfbAlgorithm.getStartState(),
//				newControllerStartState);
//		newController.setStartState(newControllerStartState);
//
//		for (RuntimeState winningSpecificationState : winAndNotLoseBuechiStates) {
//			if (winningSpecificationState != otfbAlgorithm.getStartState()
//					&& !excludedStates.contains(
//							winningSpecificationState)) {
//				// create new newController state, add it to newController, set
//				// as
//				// start state if applicable
//				RuntimeState newControllerState = RuntimeFactory.eINSTANCE
//						.createRuntimeState();
//				newControllerState.setObjectSystem(winningSpecificationState
//						.getObjectSystem());
//				newController.getStates().add(newControllerState);
//				// add new state to map
//				specStateToControllerStateMap.put(winningSpecificationState,
//						newControllerState);
//			}
//		}
//
//		for (RuntimeState winningSpecificationState : winAndNotLoseBuechiStates) {
//			// create corresponding newController transitions for all
//			// transitions between winning states
//			for (Transition outTransition : winningSpecificationState
//					.getOutgoingTransition()) {
//				if (!excludedTransitions.contains(
//								outTransition)) {
//					RuntimeState sourceState = specStateToControllerStateMap
//							.get(winningSpecificationState);
//					RuntimeState targetState = specStateToControllerStateMap
//							.get(outTransition.getTargetState());
//					Transition newControllerTransition = StategraphFactory.eINSTANCE
//							.createTransition();
//					newControllerTransition.setSourceState(sourceState);
//					newControllerTransition.setTargetState(targetState);
//
//					// reference original event
//					newControllerTransition.setEvent(outTransition
//							.getEvent());
//				}
//			}
//		}
//	}
//		
//	private Set<RuntimeState> excludedStates;
//	private Set<Transition> excludedTransitions;
//	
//	
//	// called by computeCounterStrategyExcludedElements(...)
//	// called by computeExcludedElements(...) 
//	private Set<RuntimeState> computeReachableStates(){
//		Set<RuntimeState> result=new HashSet<RuntimeState>();
//		Set<RuntimeState> open=new HashSet<RuntimeState>();
//		open.add(otfbAlgorithm.getStartState());
//		
//		while(!open.isEmpty()){
//			RuntimeState current = open.iterator().next();
//			open.remove(current);
//			if(!excludedStates.contains(current) && !result.contains(current)) {
//				for(Transition outTransition:current.getOutgoingTransition()){
//					if(!excludedTransitions.contains(outTransition) && !result.contains(outTransition.getTargetState())){
//						RuntimeState targetState=(RuntimeState)outTransition.getTargetState();
//						if(!result.contains(targetState))
//							open.add(targetState);
//					}
//				}
//			}
//			result.add(current);
//		}
//		return result;
//	}
//	
//	private void computeExcludedElements(Set<RuntimeState> winAndNotLoseBuechiStates){
//		excludedStates = new HashSet<RuntimeState>();
//		excludedTransitions = new HashSet<Transition>();
//		
//		//remove transitions with target not in set of winning states
//		//remove uncontrollable transitions to states with assumption violations
//		for (RuntimeState state : winAndNotLoseBuechiStates) {
//			for (Transition outTransition : state.getOutgoingTransition()) {
//				if (outTransition.getTargetState() == null
//						|| !winAndNotLoseBuechiStates.contains(outTransition
//								.getTargetState()))
//					excludeTransition(outTransition,false);
//				
//				RuntimeState targetState = (RuntimeState) outTransition
//						.getTargetState();
//				if (targetState.isSafetyViolationOccurredInAssumptions()
//						&& !otfbAlgorithm.isControllable(outTransition))
//					excludeTransitionNonrecursively(outTransition,false);
//					
//			}
//		}
//
//		//remove unlive loops
//		for(Transition transition:otfbAlgorithm.getUnliveLoopTransitions()){
//			excludeTransition(transition,false);
//		}
//		
//		Set<RuntimeState> reachableStates = computeReachableStates();
//		
//		//remove unreachable states
//		for(RuntimeState state:winAndNotLoseBuechiStates){
//			if(!reachableStates.contains(state)){
//				if (logger.isDebugEnabled()){
//					logger.debug("remove unreachable states (computeExcludedElements)");
//				}
//				excludeState(state,false);
//			}
//		}
//	}
//
//	// called by postProcessUnsuccessfulSynthesisResult() 
//	private void computeCounterStrategyExcludedElements(Set<RuntimeState> losingStates){
//		excludedStates = new HashSet<RuntimeState>();
//		excludedTransitions = new HashSet<Transition>();
//		
//		//remove transitions with target not in set of losing states
//		//remove uncontrollable transitions to states with assumption violations
//		for (RuntimeState state : losingStates) {
//			for (Transition outTransition : state.getOutgoingTransition()) {
//				RuntimeState targetState = (RuntimeState) outTransition
//						.getTargetState();
//				if (targetState == null
//						|| !losingStates.contains(targetState))
//					excludeTransition(outTransition,true);
//				
////				if (targetState.isSafetyViolationOccurredInRequirements()
////						&& otfbAlgorithm.isControllable(outTransition))
////					excludeTransition(outTransition);
//			}
//		}
//
//		//remove live loops
//		for(Transition transition:otfbAlgorithm.getLiveLoopTransitions()){
//			excludeTransition(transition,true);
//		}
//		
//		Set<RuntimeState> reachableStates = computeReachableStates();
//		
//		//remove unreachable states
//		for(RuntimeState state:losingStates){
//			if(!reachableStates.contains(state)){
//				if (logger.isDebugEnabled()){
//					logger.debug("remove unreachable states (computeCounterStrategyExcludedElements)");
//				}
//				excludeState(state,true);
//			}
//		}
//	}
//
//	private void excludeState(RuntimeState stateToExclude, boolean env) {
//		excludedStates.add(stateToExclude);
//		if (logger.isDebugEnabled()){
//			logger.debug(stateToExclude.getStringToStringAnnotationMap());
//		}
//		for(Transition inTransition: stateToExclude.getIncomingTransition()){
//			if(!excludedTransitions.contains(inTransition))
//				excludeTransition(inTransition,env);
//		}
//		for(Transition outTransition: stateToExclude.getOutgoingTransition()){
//			if(!excludedTransitions.contains(outTransition))
//				excludeTransition(outTransition,env);
//		}
//	}
//
//	private boolean isDeadlockState(RuntimeState state) {
//		for(Transition outTransition:state.getOutgoingTransition()){
//			if(!excludedTransitions.contains(outTransition)){
//				return false;
//			}
//		}
//		return true;
//	}
//
//	private void excludeTransition(Transition transitionToExclude, boolean env) {
//		excludeTransitionNonrecursively(transitionToExclude, env);
//		RuntimeState sourceState = (RuntimeState) transitionToExclude
//				.getSourceState();
//		
//		if(!isControllable(transitionToExclude, env)){
//		if (logger.isDebugEnabled()){
//			logger.debug("excludeTransition1");
//		}
//			excludeState(sourceState,env);
//		}
//		// exclude states without unmarked outgoing transitions, except if environment assumptions are violated
//		else if (isDeadlockState(sourceState)
//				&& !excludedStates.contains(sourceState)
//				&& (!env && !sourceState.isSafetyViolationOccurredInAssumptions()
//						|| env && (otfbAlgorithm.isGoal(sourceState)	|| 
//								otfbAlgorithm.hasMandatoryMessageEvents(sourceState, true))
//						)) {
//			if (logger.isDebugEnabled()){
//				logger.debug("excludeTransition2");
//			}
//			excludeState(sourceState,env);
//		}
//	}
//	
//	private void excludeTransitionNonrecursively(Transition transitionToExclude, boolean env){
//		if (excludedTransitions.contains(transitionToExclude))
//			return;
//		excludedTransitions.add(transitionToExclude);
//	}
//	
//	boolean isControllable(Transition transition, boolean env){
//		return otfbAlgorithm.isControllable(transition) ^ env;
//	}
//
	public Map<RuntimeState, RuntimeState> getSpecStateToControllerStateMap() {
		return specStateToControllerStateMap;
	}
//
//	protected void postProcessUnsuccessfulSynthesisResult() {
//		Set<RuntimeState> losingStates = otfbAlgorithm.getLosingStates();
//		
//		
////		@SuppressWarnings("unchecked")
////		Set<RuntimeState> losingStates = new HashSet<RuntimeState>((Collection<? extends RuntimeState>) otfbAlgorithm.getStartState().getStateGraph().getStates()); 
////		losingStates.removeAll(otfbAlgorithm.getWinAndNotLoseBuechiStates());
//		
////		//debug stuff
//			if (logger.isDebugEnabled()){
//				logger.debug(losingStates.size());
//				logger.debut(otfbAlgorithm.getLosingStates().size());
//			}
////		Set<RuntimeState> diff = new HashSet<RuntimeState>(losingStates);
////		diff.removeAll(otfbAlgorithm.getLosingStates());
////		if (logger.isDebugEnabled()){
//				logger.debug(diff);
//			}
//		
//		computeCounterStrategyExcludedElements(losingStates);
//		if (logger.isDebugEnabled()){
//			logger.debug(excludedStates.size());
//		}
//
//		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
//		newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();
//
//		// add start state first
//		RuntimeState newControllerStartState = RuntimeFactory.eINSTANCE
//				.createRuntimeState();
//		newControllerStartState.setObjectSystem(((RuntimeState) otfbAlgorithm
//				.getStartState()).getObjectSystem());
//		newController.getStates().add(newControllerStartState);
//		specStateToControllerStateMap.put(
//				(RuntimeState) otfbAlgorithm.getStartState(),
//				newControllerStartState);
//		newController.setStartState(newControllerStartState);
//
//		for (RuntimeState losingSpecificationState : losingStates) {
//			if (losingSpecificationState != otfbAlgorithm.getStartState()
//					&& !excludedStates.contains(
//							losingSpecificationState)) {
//				// create new newController state, add it to newController, set
//				// as
//				// start state if applicable
//				RuntimeState newControllerState = RuntimeFactory.eINSTANCE
//						.createRuntimeState();
//				newControllerState.setObjectSystem(losingSpecificationState
//						.getObjectSystem());
//				newController.getStates().add(newControllerState);
//				// add new state to map
//				specStateToControllerStateMap.put(losingSpecificationState,
//						newControllerState);
//			}
//		}
//
//		for (RuntimeState losingSpecificationState : losingStates) {
//			// create corresponding newController transitions for all
//			// transitions between losing states
//			if(!excludedStates.contains(losingSpecificationState))
//				for (Transition outTransition : losingSpecificationState
//						.getOutgoingTransition()) {
//					if (!excludedTransitions.contains(
//									outTransition)) {
//						RuntimeState sourceState = specStateToControllerStateMap
//								.get(losingSpecificationState);
//						RuntimeState targetState = specStateToControllerStateMap
//								.get(outTransition.getTargetState());
//						Transition newControllerTransition = StategraphFactory.eINSTANCE
//								.createTransition();
//						newControllerTransition.setSourceState(sourceState);
//						newControllerTransition.setTargetState(targetState);
//	
//						// reference original event
//						newControllerTransition.setEvent(outTransition
//								.getEvent());
//					}
//				}
//		}
//	}

	public OTFBAlgorithm getOTFBAlgorithm() {
		return otfbAlgorithm;
	}

	public RuntimeStateGraph getNewController() {
		return newController;
	}
}
