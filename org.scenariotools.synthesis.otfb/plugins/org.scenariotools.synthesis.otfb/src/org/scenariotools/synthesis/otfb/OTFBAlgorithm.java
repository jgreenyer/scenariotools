package org.scenariotools.synthesis.otfb;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class OTFBAlgorithm {

	protected static Logger logger = Activator.getLogManager().getLogger(
			OTFBAlgorithm.class.getName());
	
	protected LinkedList<RuntimeState> goal;
	public LinkedList<RuntimeState> getGoal() {
		return goal;
	}

	protected LinkedList<RuntimeState> lose;
	protected Map<RuntimeState, LinkedList<RuntimeState>> requiredGoals;
	protected Map<RuntimeState, LinkedList<Transition>> depend;

	protected RuntimeState startState;
//	Set<Transition> unliveLoopTransitions;
//	Set<Transition> liveLoopTransitions;
//	Map<RuntimeState, Set<RuntimeState>> predecessorSet;
	
	public boolean OTFB(RuntimeState startState) {
		initPostProcessingRelevantStructures();
		this.startState = startState;
		goal = new LinkedList<RuntimeState>();
		if (isGoal(startState)){
			goal.add(startState);
		}
		lose = new LinkedList<RuntimeState>();
		requiredGoals = new HashMap<RuntimeState, LinkedList<RuntimeState>>();
		
		initStateAnnotations(startState);

		if(!OTFR(startState)){
			lose.add(startState);
			
			
			//decoration -- only relevant for debug purposes / graphical rendering 
			if(logger.isDebugEnabled()){
				addStateAnnotation(startState,"+l");
				setStateStatusFlags((RuntimeStateGraph) startState.eContainer());
			}
			
			return false;
		}
		
		Stack<RuntimeState> reevaluate = getUndecidedGoalStates();
		while(!reevaluate.isEmpty()){
			RuntimeState g = reevaluate.pop();
			if(!OTFR(g)){
				lose.add(g);
				
				//decoration -- only relevant for debug purposes / graphical rendering
				if(logger.isDebugEnabled())
					addStateAnnotation(g,"+l");

				if (g == startState) {

					//decoration -- only relevant for debug purposes / graphical rendering 
					if(logger.isDebugEnabled())
						setStateStatusFlags((RuntimeStateGraph) startState.eContainer());
					
					return false;	
				}
			}
			else{
//				if (logger.isDebugEnabled()){
//					cleanRequiredGoals(g);
//					Assert.isTrue(requiredGoals.containsKey(g));
//				}
			}
			if (reevaluate.isEmpty())
				reevaluate = getUndecidedGoalStates();
		}
		
		//cleanup
		cleanAllRequiredGoals();
		for (RuntimeState winningState : requiredGoals.keySet()) {
			removeDanglingTemporaryTransitions(winningState.getOutgoingTransition());
		}
		
		//decoration -- only relevant for debug purposes / graphical rendering 
		if(logger.isDebugEnabled())
			setStateStatusFlags((RuntimeStateGraph) startState.eContainer());

		return true;
	}
	
	protected void initPostProcessingRelevantStructures(){
		eventsOnExploredTransitions = new HashSet<>();
	}
	
	protected Stack<RuntimeState> getUndecidedGoalStates() {
		Stack<RuntimeState> undecidedGoalStates = new Stack<RuntimeState>();
		for (RuntimeState goalState : goal) {
			if (!lose.contains(goalState)){
				cleanRequiredGoals(goalState);
				if (requiredGoals.get(goalState) == null && !lose.contains(goalState))
					undecidedGoalStates.add(goalState);
			}
		}
		
		return undecidedGoalStates;
	}

	protected boolean OTFR(RuntimeState startState) {
		//logger.debug("OTFR with state: " + startState);
		LinkedList<RuntimeState> passed = new LinkedList<RuntimeState>(); // a set of visited states
//		Stack<Transition> waiting = new Stack<Transition>(); //stack of transitions to be explored
		LinkedBlockingDeque<Transition> waiting = new LinkedBlockingDeque<Transition>(); //stack of transitions to be explored
		depend = new HashMap<RuntimeState, LinkedList<Transition>>(); // maps states to transitions from predecessors		
		// only relevant for post-processing
		addStateAnnotation(startState, "+OTFR");

		passed.add(startState);
		
		addTransitionsForAllOutgoingConcreteMessageEvents(startState, waiting);
		while (!waiting.isEmpty() && !requiredGoals.containsKey(startState)) {
			Transition t = waiting.pop();
			
//			boolean newTransition = false;
			
			if (t.getTargetState() == null){
				t = generateSuccessor(t);
				initStateAnnotations((RuntimeState) t.getTargetState());
			}
			
			RuntimeState sourceState = (RuntimeState) t.getSourceState();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			
			//logger.debug("POP transition: " +  t.getEvent() + " " + sourceState + " " + targetState);
				
			if (!passed.contains(targetState)) { // fwd exploration
				passed.add(targetState);
				addDepend(depend, targetState, t);
				cleanRequiredGoals(targetState);
				if (requiredGoals.containsKey(targetState)
						|| lose.contains(targetState)
						|| isGoal(targetState)){
						waiting.push(t);
					//if(targetState.isSafetyViolationOccurredInAssumptions()) win.add(targetState);//we can assume that from here we can always win.
				}else{
					addTransitionsForAllOutgoingConcreteMessageEvents(targetState, waiting);
				}
			} else { // bwd re-evaluation
				if (logger.isDebugEnabled()){
					logger.debug("bdw-evaluating:"+sourceState.getStringToStringAnnotationMap().get("passedIndex"));
					logger.debug("lose:"+lose.contains(sourceState));
					logger.debug("isLose:"+isLose(sourceState));
				}
				
				if(!lose.contains(sourceState) // only if the losing status changes 
						&& isLose(sourceState)){ 
//					logger.debug("marking losing: " + sourceState);
					lose.add(sourceState);
					if (depend.get(sourceState) != null){
						LinkedList<Transition> dependingTransitions = depend.get(sourceState);
						for (Transition transition: dependingTransitions){
								waiting.push(transition);
						}
					}
					
					removeOutgoingTransitionsFromStack(waiting, sourceState);// JG: optimization
					
					// only relevant for post-processing
					addStateAnnotation(sourceState,"+l");
				}else if (!requiredGoals.containsKey(sourceState) // only if the winning status changes
						&& updateRequiredGoals(sourceState)) {
						
//					logger.debug("marking winning: " + sourceState + " req goal states: " + requiredGoals.get(sourceState));
					if (depend.get(sourceState) != null){
						LinkedList<Transition> dependingTransitions = depend.get(sourceState);
						for (Transition transition: dependingTransitions){
								waiting.push(transition);
						}
					}
					
					removeOutgoingTransitionsFromStack(waiting, sourceState);// JG: optimization

					// only relevant for post-processing
					addStateAnnotation(sourceState,"+w");
					
				}
				//necessary?:
//				if (!requiredGoals.containsKey(targetState) 
//						&& !lose.contains(targetState))
				addDepend(depend, targetState, t);

			}

			// JG: why is this necessary?
//			if(closesUnliveLoop)
//				waiting.push(t);
			
			
		}
//		if (logger.isDebugEnabled()){
//			boolean isWinning = requiredGoals.containsKey(startState);
//			cleanRequiredGoals(startState);
//			Assert.isTrue(isWinning == requiredGoals.containsKey(startState));
//		}
		
		
		//decoration -- only relevant for debug purposes / graphical rendering 
		addStateAnnotation(startState, "-OTFR");
		
		
//		logger.debug("OTFR returns: " + requiredGoals.containsKey(startState));
		return requiredGoals.containsKey(startState);
	}

	
	protected void cleanRequiredGoals(RuntimeState state) {
		if (lose.contains(state)){
			requiredGoals.remove(state);
			return;
		}
		LinkedList<RuntimeState> requiredGoalStates = requiredGoals.get(state);
		if (requiredGoalStates != null){
			for (RuntimeState runtimeState : requiredGoalStates) {
				if (lose.contains(runtimeState)){
					requiredGoals.remove(state);
					return;
				}
			}
		}
	}
	
	/**
	 * Removes requiredGoals entries for states that are losing or depend on goal states that are losing.
	 */
	protected void cleanAllRequiredGoals() {
		Iterator<Map.Entry<RuntimeState, LinkedList<RuntimeState>>> requiredGoalsKeySetIterator = requiredGoals.entrySet().iterator();
		outer:
		while (requiredGoalsKeySetIterator.hasNext()) {
			Entry<RuntimeState, LinkedList<RuntimeState>> requiredGoalsEntry = 
					(Map.Entry<org.scenariotools.runtime.RuntimeState, LinkedList<org.scenariotools.runtime.RuntimeState>>) requiredGoalsKeySetIterator.next();
			if (lose.contains(requiredGoalsEntry.getKey())){
				requiredGoalsKeySetIterator.remove();
				break;
			}
			LinkedList<RuntimeState> requiredGoalStates = requiredGoalsEntry.getValue();
			Assert.isTrue(requiredGoalStates != null && !requiredGoalStates.isEmpty());
			for (RuntimeState runtimeState : requiredGoalStates) {
				if (lose.contains(runtimeState)){
					requiredGoalsKeySetIterator.remove();
					break outer;
				}
			}
			
		}
	}


	/**
	 * returns true if an entry in requiredGoals can be created for a state. Such an entry can be created
	 * I) if there is one state reachable via a controllable transition that has an entry in requiredGoals 
	 *    and where the value set V in requireGoals for that state does not contain any losing state. 
	 *    Then an entry state->V is created in requiredGoals
	 * II) OR if there is at least one outgoing uncontrollable transition and
	 *    for all uncontrollable outgoing transitions all successors have an entry in requiredGoals
	 *    and where the value sets V1..Vn for these successors in requiredGoals do not contain any losing state. 
	 *    Then an entry state->U1..nVi (union of V1..Vn) is created in requiredGoals for the respective successor 
	 *    states.
	 * @param state
	 * @return
	 */
	protected boolean updateRequiredGoals(RuntimeState state) {
		
		if (state.getOutgoingTransition().isEmpty()) return false;
		
		EList<Transition> controllableOutgoingTransitions = new BasicEList<>();
		EList<Transition> uncontrollableOutgoingTransitions = new BasicEList<>();
		
		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllableWrapper(transition)){
				controllableOutgoingTransitions.add(transition);
			}else{
				uncontrollableOutgoingTransitions.add(transition);
			}
		}

		for (Transition transition : controllableOutgoingTransitions) {
			RuntimeState targetState = (RuntimeState) transition.getTargetState();
			if(targetState != null){
				if (lose.contains(targetState)) continue;
				if (isGoal(targetState)){
					LinkedList<RuntimeState> newRequiredGoalsValueSet = new LinkedList<RuntimeState>();
					newRequiredGoalsValueSet.add(targetState);
					requiredGoals.put(state, newRequiredGoalsValueSet);
					if (targetState.isSafetyViolationOccurredInAssumptions() && !targetState.isSafetyViolationOccurredInRequirements()){
						LinkedList<RuntimeState> selfRequired = new LinkedList<RuntimeState>();
						selfRequired.add(targetState);
						requiredGoals.put(targetState, selfRequired);
					}
					return true;
				}else {
					cleanRequiredGoals(targetState);
					LinkedList<RuntimeState> targetStateRequiredGoals = requiredGoals.get(targetState);
					if (targetStateRequiredGoals != null){ // target state has entry in requiredGoals after cleanRequiredGoals. This means that the targetState is marked "winning"
						// mark state winning, based on the same required goals as the winning target state.
						requiredGoals.put(state, targetStateRequiredGoals);
						return true;
					}	
				}
			}
		}
		
		// if reaches here, there were no outgoing controllable transitions leading to goal or winning states 
		
		// return false if there are no outgoing uncontrollable transitions
		if (uncontrollableOutgoingTransitions.isEmpty()) return false;

		
		LinkedList<RuntimeState> requiredGoalsForState = new LinkedList<RuntimeState>();
		
		for (Transition transition : uncontrollableOutgoingTransitions) {
			RuntimeState targetState = (RuntimeState) transition.getTargetState();
			if (targetState == null){ // not explored yet, so we cannot say anything about the winning status of the state
				return false;
			}
			if (lose.contains(targetState)){ // losing successor -> state cannot be winning
				return false;
			}
			cleanRequiredGoals(targetState);
			LinkedList<RuntimeState> targetStateRequiredGoals = requiredGoals.get(targetState);
			if (isGoal(targetState)){ // successor is goal state -> remember goal state as required goal state
				requiredGoalsForState.add(targetState);
			}else if (targetStateRequiredGoals != null){ // successor is winning state -> remember required goal states of target state
														 // as required goal states for current state 
				Assert.isTrue(!targetStateRequiredGoals.isEmpty());
				requiredGoalsForState.addAll(targetStateRequiredGoals);
			}else{ // successor not winning -> state cannot be winning
				return false;
			}
		}
		
		// the above loop must have had at least one iteration in which it either returned false at some point 
		// or have at least added one element in requiredGoalsForState 
		Assert.isTrue(!requiredGoalsForState.isEmpty());

		requiredGoals.put(state, requiredGoalsForState);
		
		return true;

	}

	/**
	 * returns true if
	 *   1. state has no outgoing transitions
	 *   2. all outgoing controllable or at least one outgoing uncontrollable transition lead to a losing state
	 * @param state
	 * @return
	 */
	protected boolean isLose(RuntimeState state) {
		if (logger.isDebugEnabled()){
			logger.debug("isLose(\"+sourceState.getStringToStringAnnotationMap().get(\"passedIndex\")+\")");
		}
		if (state.getOutgoingTransition().isEmpty())
			return true;
		boolean noUncontrollableSuccessors = true;
		boolean losingUncontrollableSuccessor = false;
		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllableWrapper(transition)) {
				if (transition.getTargetState() == null) // it's an unexplored temporary transition
					return false; // non-losing controllable successor
				if (!lose.contains(transition.getTargetState()))
					return false; // non-losing controllable successor
			} else {
				noUncontrollableSuccessors = false;
				if (transition.getTargetState() == null) // it's an unexplored temporary transition
					continue;
				if (lose.contains(transition.getTargetState()))
					losingUncontrollableSuccessor = true; //may NOT return true here, because there may still be controllable transitions
			}
		}
		//no non-losing controllable successors at this point
		return (noUncontrollableSuccessors 
				|| losingUncontrollableSuccessor //env will pick losing move if possible  
				|| (state.isSafetyViolationOccurredInRequirements() && !state.isSafetyViolationOccurredInAssumptions() 
						&& !hasMandatoryMessageEvents(state, true))); //env will not prevent system from losing if not mandatory
	}

	protected Transition generateSuccessor(Transition t) {
		RuntimeState sourceState = (RuntimeState) t.getSourceState();
		t.setSourceState(null); // must detach temporary transition from source
								// state again!
		t = ((RuntimeStateGraph) sourceState.getStateGraph())
				.generateSuccessor(sourceState, t.getEvent());
		
//		t.getStringToStringAnnotationMap().put("exploreOrder", c1+"_"+c2);
		return t;
	}
	
	protected void addDepend(Map<RuntimeState, LinkedList<Transition>> depend, RuntimeState q, Transition t) {
		LinkedList<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new LinkedList<Transition>();
		}
		if (!dependingTransitions.contains(t)){
			dependingTransitions.add(t);
		}
		depend.put(q, dependingTransitions);
	}
	
	public boolean isGoal(RuntimeState q) {
		if (lose.contains(q)){
			return false;
		}
		
		if (goal.contains(q))
			return true;
		else {
			// 1. no safety violation must have occurred in the requirements and there must not be any active events in active req. MSDs
			// OR 2. there was a safety violation of the assumptions
			// OR 3. if a safety violation occurred in the requirements, there are active messages left in the assumptions,
			if (!q.isSafetyViolationOccurredInRequirements() && !hasMandatoryMessageEvents(q, false)
					|| q.isSafetyViolationOccurredInAssumptions()
					|| q.isSafetyViolationOccurredInRequirements() && hasMandatoryMessageEvents(q, true)
					) {
				goal.add(q);
				return true;
			}
		}
		return false;
	}
	
	public boolean hasMandatoryMessageEvents(RuntimeState runtimeState,
			boolean forAssumptions) {
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();
			if (forAssumptions) {
				if (modalMessageEvent.getAssumptionsModality().isMandatory())
					return true;
			} else {
				if (modalMessageEvent.getRequirementsModality().isMandatory())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Removes the outgoing transitions of the given source state from the given stack and removes 
	 * unexplored transitions from the source state, i.e., transitions where the target state is null. 
	 */
	protected void removeOutgoingTransitionsFromStack(LinkedBlockingDeque<Transition> waiting,
			RuntimeState sourceState) {
		for (Iterator<Transition> transitionsIterator = sourceState.getOutgoingTransition().iterator(); 
				transitionsIterator.hasNext(); ) {
			Transition transition = transitionsIterator.next();
			waiting.remove(transition);
			// remove unexplored tmp transitions
			if (transition.getTargetState() == null) 
				transitionsIterator.remove();
		}
	}
	
	/**
	 * 
	 * Generates successors of the given runtime state and pushes the outgoing
	 * transitions on the top of the passed waiting stack. It is ensured that
	 * among the pushed transitions, the controllable transitions are on the top
	 * of the stack.
	 * 
	 * With the parameter <code>addOnlyTransitionsToNotPassedStates</code> set
	 * to TRUE, only transitions
	 * 
	 * 
	 * @param runtimeState
	 * @param waiting
	 * @param addOnlyTransitionsToNotPassedStates
	 */
	protected boolean addTransitionsForAllOutgoingConcreteMessageEvents(RuntimeState runtimeState, LinkedBlockingDeque<Transition> waiting) {
		boolean atLeastOneTransitionWasAdded = false;
		
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState.getMessageEventToModalMessageEventMap().entrySet()) {
			MessageEvent messageEvent = messageEventToModalMessageEventMapEntry.getKey();
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry.getValue();

			if (!messageEvent.isConcrete())
				continue;

			Transition transition = runtimeState.getEventToTransitionMap().get(messageEvent);

			if (transition == null){
				transition = createTmpTransition(runtimeState, messageEvent);
			}
			
			eventsOnExploredTransitions.add(messageEvent);

			if(getTransitionPriority(transition, modalMessageEvent)==0){
					waiting.push(transition);
			}
			else{
					waiting.addLast(transition);
			}
			atLeastOneTransitionWasAdded = true;
		}	
		return atLeastOneTransitionWasAdded;
	}
	
	protected Set<Event> eventsOnExploredTransitions;
	
	public Set<Event> getEventsOnExploredTransitions(){
		return eventsOnExploredTransitions;
	}
	
	/**
	 * 
	 * "0" is the highest priority.
	 * 
	 * @param transition
	 * @param modalMessageEvent
	 * @return
	 */
	protected int getTransitionPriority(Transition transition, ModalMessageEvent modalMessageEvent){
		if (this.isControllableWrapper(transition) == false && modalMessageEvent != null && modalMessageEvent.getAssumptionsModality().isSafetyViolating()){
			return 2;
		}
		else if (this.isControllableWrapper(transition) == false && modalMessageEvent != null && modalMessageEvent.getRequirementsModality().isSafetyViolating()){
			return 3;
		}
		else if (this.isControllableWrapper(transition) == true && modalMessageEvent != null && modalMessageEvent.getAssumptionsModality().isSafetyViolating()){
			return 4;
		}
		else if (this.isControllableWrapper(transition) == true && modalMessageEvent != null && modalMessageEvent.getRequirementsModality().isSafetyViolating()){
			return 5;
		}
		else if (this.isControllableWrapper(transition) == false){
			return 6;
		}
		else if (this.isControllableWrapper(transition) == true){
			return 7;
		}
		return 8;
	}
	
	protected Transition createTmpTransition(RuntimeState runtimeState,
			MessageEvent messageEvent) {
		Transition tmpTransition = StategraphFactory.eINSTANCE
				.createTransition();
		tmpTransition.setEvent(messageEvent);
		tmpTransition.setSourceState(runtimeState);
		return tmpTransition;
	}
	
	protected boolean isControllableWrapper(RuntimeState runtimeState,
			MessageEvent messageEvent) {
		return runtimeState.getObjectSystem().isControllable((EObject)(messageEvent.getSendingObject()));
	}

	protected boolean isControllableWrapper(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(
						((MessageEvent) transition.getEvent())
								.getSendingObject());
	}

	protected void removeDanglingTemporaryTransitions(
			Collection<Transition> transitions) {
		for (Transition transition : transitions) {
			if (transition.getTargetState() == null){
				
				EList<Transition> outgoingTransitions = transition.getSourceState().getOutgoingTransition();
				transition.setEvent(null);
				if (transition.getTargetState() != null){				
					EList<Transition> incommingTransitions = transition.getTargetState().getIncomingTransition();
					incommingTransitions.remove(transition);
				}
				outgoingTransitions.remove(transition);
				transition.setSourceState(null);
			}
		}
	}

	
	public Set<RuntimeState> getWinAndNotLoseBuechiStates(){
		return requiredGoals.keySet();
	}

	public LinkedList<RuntimeState> getLosingStates(){
		return lose;
	}

	public RuntimeState getStartState() {
		return startState;
	}

	public void setStartState(RuntimeState startState) {
		this.startState = startState;
	}
	

	private int passedStatesCounter = 1;
	
	private Set<RuntimeState> globalPassed=new HashSet<RuntimeState>();
	
	protected void initStateAnnotations(RuntimeState state){
		if(logger.isDebugEnabled()){
			if (!globalPassed.contains(state)) {
				state.getStringToStringAnnotationMap().put("passedIndex",
						String.valueOf(passedStatesCounter++));
				state.getStringToStringAnnotationMap().removeKey("stateLog");
				state.getStringToBooleanAnnotationMap().removeKey("win");
				state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
				state.getStringToBooleanAnnotationMap().removeKey("goal");
				if (hasMandatoryMessageEvents(state, false)){
					addStateAnnotation(state, "hasActiveReqMsg");
				}
				if (hasMandatoryMessageEvents(state, true)){
					addStateAnnotation(state, "hasActiveAssumMsg");
				}
				globalPassed.add(state);
			}
		}
	}
	
	protected void addStateAnnotation(RuntimeState state, String newAnnotation){
		if (logger.isDebugEnabled()){
			String stateLog=state.getStringToStringAnnotationMap().get("stateLog");
			String newStateLog;
			if (stateLog != null && !stateLog.isEmpty())
				newStateLog=stateLog + ", " + newAnnotation
						+ "(" + (passedStatesCounter-1) + ")";
			else
				newStateLog = newAnnotation + "(" + (passedStatesCounter-1) + ")";
			String[] lines = newStateLog.split("\\n");
			if(lines[lines.length-1].length()>35)
				newStateLog+= "\n";
			state.getStringToStringAnnotationMap().put("stateLog",newStateLog);
		}
	}
	
	protected void setStateStatusFlags(RuntimeStateGraph graph){
		if (logger.isDebugEnabled()){
			for(State state:graph.getStates()){
				((RuntimeState)state).getStringToBooleanAnnotationMap().put("goal", goal.contains(state));
				((RuntimeState)state).getStringToBooleanAnnotationMap().put("loseBuechi", lose.contains(state));
				((RuntimeState)state).getStringToBooleanAnnotationMap().put("win", requiredGoals.containsKey(state));
			}
		}
	}
}
