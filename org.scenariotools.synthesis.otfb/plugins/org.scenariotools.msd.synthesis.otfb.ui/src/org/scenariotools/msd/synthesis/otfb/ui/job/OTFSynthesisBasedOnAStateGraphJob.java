package org.scenariotools.msd.synthesis.otfb.ui.job;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.strategy2mss.configuration.InterpreterConfigurationUtil;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.synthesis.otfb.OTFSynthesis;

import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

public class OTFSynthesisBasedOnAStateGraphJob extends Job {

	protected IFile msdruntimeResourceFile;
	protected ResourceSet resourceSet;
	protected MSDRuntimeStateGraph msdRuntimeStateGraph;
	
	protected Resource msdruntimeResource;
	protected Resource controllerResource;
	protected OTFSynthesis otfSynthesis;
	protected URI msdruntimeResourceFileURI;
	protected URI controllerFileURI;
	protected IProgressMonitor monitor;
	

	public OTFSynthesisBasedOnAStateGraphJob(String name,
			IFile msdruntimeResourceFile,
			ResourceSet resourceSet, MSDRuntimeStateGraph msdRuntimeStateGraph) {
		super(name);
		this.msdruntimeResourceFile = msdruntimeResourceFile;
		this.resourceSet = resourceSet;
		this.msdRuntimeStateGraph = msdRuntimeStateGraph;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		this.monitor = monitor;
		
//		Resource msdruntimeResource;
		controllerResource = null;

		msdruntimeResourceFileURI = URI
				.createPlatformResourceURI(
						msdruntimeResourceFile.getFullPath()
								.toString(), true).trimFileExtension()
				.appendFileExtension("msdruntime");
		controllerFileURI = URI.createPlatformResourceURI(
				msdruntimeResourceFile.getFullPath()
						.removeFileExtension().toString()
						+ "Controller", true).appendFileExtension("msdruntime");

		try {
			msdruntimeResource = resourceSet.getResource(
					msdruntimeResourceFileURI, true);
			controllerResource = resourceSet.getResource(controllerFileURI,
					true);
			if (msdruntimeResource != null) {
				MessageDialog
						.openError(
								new Shell(),
								"An error occurred while creating the interpreter configuration",
								"There already exists a MSD runtime file at the default location:\n"
										+ msdruntimeResourceFileURI
										+ "\n\n"
										+ "Remove it first to create a new one.");
			}
			if (controllerResource != null) {
				MessageDialog
						.openError(
								new Shell(),
								"An error occurred while creating the interpreter configuration",
								"There already exists a controller file at the default location:\n"
										+ controllerFileURI
										+ "\n\n"
										+ "Remove it first to create a new one.");
			}

			return Status.CANCEL_STATUS;
		} catch (Exception e) {
			// that's good. Continue.
		}

		long currentTimeMillis = System.currentTimeMillis();

		// canReachAnotherGoalState(msdRuntimeStartState);
		otfSynthesis = createOTFSynthesis();
		boolean buechiStrategyExists = otfSynthesis
				.otfSynthesizeController(msdRuntimeStateGraph);
		Set<RuntimeState> winningStates = otfSynthesis.getOTFBAlgorithm()
				.getWinAndNotLoseBuechiStates();

		// System.out.println(winningStates);

		long currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;

		// remove reference to RuntimeUtil:
		msdRuntimeStateGraph.setRuntimeUtil(null);
		for (State state : msdRuntimeStateGraph.getStates()) {
			MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
			msdRuntimeState.setRuntimeUtil(null);
		}

		for (ActiveProcess activeProcess : msdRuntimeStateGraph
				.getElementContainer().getActiveProcesses()) {
			activeProcess.setRuntimeUtil(null);
			if(activeProcess instanceof ActiveMSD)
				((ActiveMSD)activeProcess).setMsdUtil(null);
		}
		for (ActiveState activeMSDCut : msdRuntimeStateGraph
				.getElementContainer().getActiveStates()) {
			activeMSDCut.setRuntimeUtil(null);
		}
		for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph
				.getElementContainer().getObjectSystems()) {
			msdObjectSystem.setRuntimeUtil(null);
		}
		
		postExplorationFinished(msdRuntimeStateGraph, currentTimeMillisDelta,
				buechiStrategyExists, winningStates, this);

		return Status.OK_STATUS;
	}
	
	public static void postExplorationFinished(
			final RuntimeStateGraph msdRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates
			) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;
				int numberOfActiveProcesses = 0;

				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
					numberOfActiveProcesses += ((MSDRuntimeState)state).getActiveProcesses().size();
				}
				float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
						/ ((float) numberOfStates);
				
				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size()+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberOfActiveProcessesPerState);
			}
		});
	}

	public void postExplorationFinished(
			final RuntimeStateGraph msdRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates,
			final OTFSynthesisBasedOnAStateGraphJob self) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;
				int numberOfActiveProcesses = 0;

				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
					numberOfActiveProcesses += ((MSDRuntimeState)state).getActiveProcesses().size();
				}
				float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
						/ ((float) numberOfStates);
				
				boolean userChoice = MessageDialog.open(
						6,
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size()+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberOfActiveProcessesPerState
								+ "\n\nDo you want to save the synthesised controller/"
								+ (buechiStrategyExists ? "strategy?" : "counter-strategy?"),
						0);
				
				if(userChoice){
					self.saveSynthesisResult();
					self.transformationOnUserConfirm(otfSynthesis, buechiStrategyExists, self);
				}
			}
		});
	}
	
	protected void transformationOnUserConfirm(
			final OTFSynthesis otfSynthesis, 
			final boolean buechiStrategyExists,
			final OTFSynthesisBasedOnAStateGraphJob self){
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				boolean choice =  MessageDialog.open(
									6,
									new Shell(),
									"Save as MSS",
									"Do you want to create the "
									+ (buechiStrategyExists ? "strategy " : "counter-strategy ")
									+ "as an MSS for simulation?",
									0);
				if(choice)
					self.performTransformation(otfSynthesis, self);
			}
		});
	}
	
	protected void performTransformation(
			OTFSynthesis otfSynthesis,
			OTFSynthesisBasedOnAStateGraphJob self){
		
				//Transformation: msdruntime to uml
				//create InterpreterConfiguration for Transformation
				Configuration tggInterpreterConfiguration = InterpreterConfigurationUtil.getTGGInterpreterConfiguration(
						otfSynthesis.getNewController(), 
						msdruntimeResourceFile);
				
				Resource configurationResource = resourceSet.createResource(URI.createPlatformResourceURI(
						msdruntimeResourceFile.getFullPath()
						.removeFileExtension().toString()
						+ "Controller", true).appendFileExtension("interpreterconfiguration"));
				configurationResource.getContents().add(tggInterpreterConfiguration);
				try {
					configurationResource.save(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//execute InterpreterConfiguration
				final Configuration reloadedTGGInterpreterConfiguration = InterpreterconfigurationUtil.loadInterpreterConfiguration(configurationResource.getURI());
				
				Interpreter interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
				interpreter.setConfiguration(reloadedTGGInterpreterConfiguration);
				interpreter.initializeConfiguration();
				IStatus interpreterStatus = interpreter.performTransformation(self.monitor);
				interpreter.storeResult();
				//StartInterpreterAction.postTransformationFinished(interpreterStatus, interpreter);
				//Transformation end 

	}
	
	public void saveSynthesisResult(){
		msdruntimeResource = resourceSet
				.createResource(this.msdruntimeResourceFileURI);
		msdruntimeResource.getContents().add(
				msdRuntimeStateGraph);

		if (this.otfSynthesis.getNewController() != null) {
			controllerResource = resourceSet.createResource(controllerFileURI);
			controllerResource.getContents().add(
					this.otfSynthesis.getNewController());
		}

		Map<String, Boolean> options = new HashMap<String, Boolean>();
		options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE); 
		
		try {
			msdruntimeResource
					.save(options);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (controllerResource != null) {
			try {
				controllerResource.save(options);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected OTFSynthesis createOTFSynthesis() {
		OTFSynthesis otfSynthesis = new OTFSynthesis();
		otfSynthesis.initializeOTFBAlgorithm();
		return otfSynthesis;
	}

}
