package org.scenariotools.msd.synthesis.otfb.ui.job;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.synthesis.otfb.OTFSynthesis;

public class OTFSynthesisJobNTimes extends Job {

	private ScenarioRunConfiguration configuration;
	boolean buechiStrategyExists;
	int numberOfIterations;
	int totalNumberOfStates;
	int totalNumberOfTransitions;
	int totalNumberOfWinningStates;
	float sumOfAvgProcsPerState;
	float totalTime;

	public OTFSynthesisJobNTimes(String name,
			ScenarioRunConfiguration configuration) {
		super(name);
		this.configuration = configuration;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		askNumberOfIterations();
		for(int i=0;i<numberOfIterations;i++){
			
			MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
					.createMSDRuntimeStateGraph();

			msdRuntimeStateGraph
					.init(configuration);
					
			long currentTimeMillis = System.currentTimeMillis();
	
			// canReachAnotherGoalState(msdRuntimeStartState);
			OTFSynthesis otfSynthesis = createOTFSynthesis();
			boolean buechiStrategyExists = otfSynthesis
					.otfSynthesizeController(msdRuntimeStateGraph);
			Set<RuntimeState> winningStates = otfSynthesis.getOTFBAlgorithm()
					.getWinAndNotLoseBuechiStates();
	
			// System.out.println(winningStates);
	
			long currentTimeMillisDelta = System.currentTimeMillis()
					- currentTimeMillis;
	
			// remove reference to RuntimeUtil:
			msdRuntimeStateGraph.setRuntimeUtil(null);
			for (State state : msdRuntimeStateGraph.getStates()) {
				MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
				msdRuntimeState.setRuntimeUtil(null);
			}
	
	
			Map<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE); 
			
			accumulateResults(msdRuntimeStateGraph, currentTimeMillisDelta,
					buechiStrategyExists, winningStates);
		}
		if(numberOfIterations > 0){
			displayFinalResults(buechiStrategyExists, numberOfIterations, totalNumberOfStates, totalNumberOfTransitions, totalNumberOfWinningStates, sumOfAvgProcsPerState, totalTime);
		}
		return Status.OK_STATUS;
	}
	
	
	public void askNumberOfIterations(){
		final InputDialog inputDlg = new InputDialog(null,
	            "Number Of Iterations", "#Iterations", "1", null);
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				if(inputDlg.open() == Window.OK)
					numberOfIterations=Integer.parseInt(inputDlg.getValue());
			}
		});
		
	}

	public void accumulateResults(
		RuntimeStateGraph msdRuntimeStateGraph,
		long currentTimeMillisDelta,
		boolean buechiStrategyExists,
		Set<RuntimeState> winningStates) {

		int numberOfStates = 0;
		int numberOfTransitions = 0;
		int numberOfActiveProcesses = 0;

		for (State state : msdRuntimeStateGraph.getStates()) {
			numberOfStates++;
			numberOfTransitions += state.getOutgoingTransition().size();
			numberOfActiveProcesses += ((MSDRuntimeState)state).getActiveProcesses().size();
		}
		float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
				/ ((float) numberOfStates);
			
		totalNumberOfStates += numberOfStates;
		totalNumberOfTransitions += numberOfTransitions;
		totalNumberOfWinningStates += winningStates.size();
		sumOfAvgProcsPerState += avgNumberOfActiveProcessesPerState;
		totalTime += currentTimeMillisDelta;
		if(buechiStrategyExists)
			this.buechiStrategyExists = true;
	}

	public static void displayFinalResults(
			final boolean buechiStrategyExists,
			final int numberOfIterations,
			final int totalNumberOfStates,
			final int totalNumberOfTransitions,
			final int totalNumberOfWinningStates,
			final float sumOfAvgProcsPerState,
			final float totalTime) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				float avgNumberOfStates = ((float) totalNumberOfStates)
						/ ((float) numberOfIterations);
				float avgNumberOfTransitions = ((float) totalNumberOfTransitions)
						/ ((float) numberOfIterations);
				float avgNumberOfWinningStates = ((float) totalNumberOfWinningStates)
						/ ((float) numberOfIterations);
				float avgNumberActProcsPerState = ((float) sumOfAvgProcsPerState)
						/ ((float) numberOfIterations);
				float avgTime = ((float) totalTime)
						/ ((float) numberOfIterations);
				
				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Synthesis was executed "+numberOfIterations+" times, on average " 
								+ avgNumberOfStates
								+ " states were explored, "
								+ avgNumberOfTransitions
								+ " transitions were created.\n"
								+ "Avg. Time taken: " + (avgTime)
								+ " milliseconds.\n"
								+ "Avg. Number of winning states: "
								+ avgNumberOfWinningStates+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberActProcsPerState);
			}
		});
	}

	protected OTFSynthesis createOTFSynthesis() {
		OTFSynthesis otfSynthesis = new OTFSynthesis();
		otfSynthesis.initializeOTFBAlgorithm();
		return otfSynthesis;
	}
}
