package org.scenariotools.msd.synthesis.otfb.ui.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.synthesis.otfb.ui.job.OTFSynthesisJob;

public class OTFSynthesisAction implements IObjectActionDelegate{

	private Shell shell;

	/**
	 * Constructor for Action1.
	 */
	public OTFSynthesisAction() {
		super();
	}
	
	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		if (!"scenariorunconfiguration".equals(((IFile)structuredSelection.getFirstElement()).getFileExtension())){
			postProblem("You must select a *.scenariorunconfiguration file.");
		}
		
		IFile scenarioRunConfigurationResourceFile = (IFile) structuredSelection.getFirstElement();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);

		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

		MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();

		MSDRuntimeState msdRuntimeStartState = msdRuntimeStateGraph
				.init(scenarioRunConfiguration);

		Job job = new OTFSynthesisJob("On-the-fly Controller Synthesis from MSD specification", scenarioRunConfigurationResourceFile, resourceSet, msdRuntimeStateGraph);

		job.setUser(true);
		job.schedule();
	}
	
	
	protected void postProblem(final String problem) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"A problem occurred",
						problem);
			}
		});
	}

}
