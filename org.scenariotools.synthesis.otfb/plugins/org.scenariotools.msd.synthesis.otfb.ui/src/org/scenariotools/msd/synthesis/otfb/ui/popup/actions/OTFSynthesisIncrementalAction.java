package org.scenariotools.msd.synthesis.otfb.ui.popup.actions;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.synthesis.otfb.ui.job.OTFSynthesisIncrementalJob;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

public class OTFSynthesisIncrementalAction extends OTFSynthesisAction{

	public IFile getScenarioRunConfigurationResourceFile() {
		return scenarioRunConfigurationResourceFile;
	}

	public IFile getOldControllerResourceFile() {
		return oldControllerResourceFile;
	}

	public ResourceSet getResourceSet() {
		return resourceSet;
	}

	public MSDRuntimeStateGraph getMsdRuntimeStateGraph() {
		return msdRuntimeStateGraph;
	}

	public RuntimeStateGraph getBaseController() {
		return baseController;
	}

	private IFile scenarioRunConfigurationResourceFile = null;
	private IFile oldControllerResourceFile = null;
	private ResourceSet resourceSet;
	private MSDRuntimeStateGraph msdRuntimeStateGraph;
	private RuntimeStateGraph baseController;
	
	
	
	/**
	 * Constructor for Action1.
	 */
	public OTFSynthesisIncrementalAction() {
		super();
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		List<?> selectionList = structuredSelection.toList();

		scenarioRunConfigurationResourceFile = null;
		oldControllerResourceFile = null;
		
		if ("scenariorunconfiguration".equals(((IFile)selectionList.get(0)).getFileExtension())
				&& "msdruntime".equals(((IFile)selectionList.get(1)).getFileExtension())){
			scenarioRunConfigurationResourceFile = ((IFile)selectionList.get(0));
			oldControllerResourceFile = ((IFile)selectionList.get(1));
		}else if ("scenariorunconfiguration".equals(((IFile)selectionList.get(1)).getFileExtension())
				&& "msdruntime".equals(((IFile)selectionList.get(0)).getFileExtension())){
			scenarioRunConfigurationResourceFile = ((IFile)selectionList.get(1));
			oldControllerResourceFile = ((IFile)selectionList.get(0));
		}else{
			postProblem("You must select two files, one *.scenariorunconfiguration, one *.msdruntime (containing an old controller).");
			return;
		}
		
		
		resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);
		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

		reconfigureScenarioRunConfiguration(scenarioRunConfiguration);
		
		Resource oldControllerResource = resourceSet
				.getResource(URI.createPlatformResourceURI(oldControllerResourceFile.getFullPath()
						.toString(), true), true);
		baseController = (RuntimeStateGraph) oldControllerResource
				.getContents().get(0);

		msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();

		RuntimeState msdRuntimeStartState = msdRuntimeStateGraph
				.init(scenarioRunConfiguration);

		Job job = createSynthesisJob();
		boolean dialogOk=openDialog(job);
		if (dialogOk){
			job.setUser(true);
			job.schedule();
		}
	}

	protected void reconfigureScenarioRunConfiguration(
			ScenarioRunConfiguration scenarioRunConfiguration) {
		// TODO Auto-generated method stub
		
	}
	
	protected boolean openDialog(Job job){
		//empty method to override
		return true;
	}
	protected Job createSynthesisJob(){
		return new OTFSynthesisIncrementalJob("On-the-fly Incremental Controller Synthesis from an MSD specification and base controller", getScenarioRunConfigurationResourceFile(), getResourceSet(), getMsdRuntimeStateGraph(), getBaseController());
	}

}
