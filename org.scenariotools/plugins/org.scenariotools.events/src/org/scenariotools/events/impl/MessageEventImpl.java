/**
 */
package org.scenariotools.events.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Message;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getRuntimeMessage <em>Runtime Message</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getAbstractMessageEvent <em>Abstract Message Event</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getConcreteMessageEvent <em>Concrete Message Event</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getStringParameterValue <em>String Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class MessageEventImpl extends EventImpl implements MessageEvent {
	/**
	 * The cached value of the '{@link #getRuntimeMessage() <em>Runtime Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeMessage()
	 * @generated
	 * @ordered
	 */
	protected RuntimeMessage runtimeMessage;

	/**
	 * The cached value of the '{@link #getAbstractMessageEvent() <em>Abstract Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected MessageEvent abstractMessageEvent;

	/**
	 * The cached value of the '{@link #getConcreteMessageEvent() <em>Concrete Message Event</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConcreteMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> concreteMessageEvent;

	/**
	 * The default value of the '{@link #getMessageName() <em>Message Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageName()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isBooleanParameterValue() <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBooleanParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BOOLEAN_PARAMETER_VALUE_EDEFAULT = false;

	/**
	 * The default value of the '{@link #getIntegerParameterValue() <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_PARAMETER_VALUE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getStringParameterValue() <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final String STRING_PARAMETER_VALUE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isParameterized() <em>Parameterized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParameterized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARAMETERIZED_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isConcrete() <em>Concrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConcrete()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONCRETE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected Message message;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeMessage getRuntimeMessage() {
		if (runtimeMessage != null && runtimeMessage.eIsProxy()) {
			InternalEObject oldRuntimeMessage = (InternalEObject)runtimeMessage;
			runtimeMessage = (RuntimeMessage)eResolveProxy(oldRuntimeMessage);
			if (runtimeMessage != oldRuntimeMessage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE, oldRuntimeMessage, runtimeMessage));
			}
		}
		return runtimeMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeMessage basicGetRuntimeMessage() {
		return runtimeMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeMessage(RuntimeMessage newRuntimeMessage) {
		RuntimeMessage oldRuntimeMessage = runtimeMessage;
		runtimeMessage = newRuntimeMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE, oldRuntimeMessage, runtimeMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent getAbstractMessageEvent() {
		if (abstractMessageEvent != null && abstractMessageEvent.eIsProxy()) {
			InternalEObject oldAbstractMessageEvent = (InternalEObject)abstractMessageEvent;
			abstractMessageEvent = (MessageEvent)eResolveProxy(oldAbstractMessageEvent);
			if (abstractMessageEvent != oldAbstractMessageEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT, oldAbstractMessageEvent, abstractMessageEvent));
			}
		}
		return abstractMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent basicGetAbstractMessageEvent() {
		return abstractMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractMessageEvent(MessageEvent newAbstractMessageEvent, NotificationChain msgs) {
		MessageEvent oldAbstractMessageEvent = abstractMessageEvent;
		abstractMessageEvent = newAbstractMessageEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT, oldAbstractMessageEvent, newAbstractMessageEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractMessageEvent(MessageEvent newAbstractMessageEvent) {
		if (newAbstractMessageEvent != abstractMessageEvent) {
			NotificationChain msgs = null;
			if (abstractMessageEvent != null)
				msgs = ((InternalEObject)abstractMessageEvent).eInverseRemove(this, EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT, MessageEvent.class, msgs);
			if (newAbstractMessageEvent != null)
				msgs = ((InternalEObject)newAbstractMessageEvent).eInverseAdd(this, EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT, MessageEvent.class, msgs);
			msgs = basicSetAbstractMessageEvent(newAbstractMessageEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT, newAbstractMessageEvent, newAbstractMessageEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getConcreteMessageEvent() {
		if (concreteMessageEvent == null) {
			concreteMessageEvent = new EObjectWithInverseResolvingEList<MessageEvent>(MessageEvent.class, this, EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT, EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT);
		}
		return concreteMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EObject getSendingObject() {
		return getRuntimeMessage().getSendingObject();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EObject getReceivingObject() {
		return getRuntimeMessage().getReceivingObject();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getMessageName() {
		return getRuntimeMessage().getMessageName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isBooleanParameterValue() {
		return getRuntimeMessage().isBooleanParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetBooleanParameterValue() {
		return getRuntimeMessage().isSetBooleanParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getIntegerParameterValue() {
		return getRuntimeMessage().getIntegerParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetIntegerParameterValue() {
		return getRuntimeMessage().isSetIntegerParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EObject getEObjectParameterValue() {
		return getRuntimeMessage().getEObjectParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetEObjectParameterValue() {
		return getRuntimeMessage().isSetEObjectParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getStringParameterValue() {
		return getRuntimeMessage().getStringParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetStringParameterValue() {
		return getRuntimeMessage().isSetStringParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isParameterized() {
		return getRuntimeMessage().isParameterized();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isConcrete() {
		return getRuntimeMessage().isConcrete();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EOperation getOperation() {
		return getRuntimeMessage().getOperation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EStructuralFeature getSideEffectOnEStructuralFeature() {
		return getRuntimeMessage().getSideEffectOnEStructuralFeature();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getMessage() {
		if (message != null && message.eIsProxy()) {
			InternalEObject oldMessage = (InternalEObject)message;
			message = (Message)eResolveProxy(oldMessage);
			if (message != oldMessage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__MESSAGE, oldMessage, message));
			}
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(Message newMessage) {
		Message oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				if (abstractMessageEvent != null)
					msgs = ((InternalEObject)abstractMessageEvent).eInverseRemove(this, EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT, MessageEvent.class, msgs);
				return basicSetAbstractMessageEvent((MessageEvent)otherEnd, msgs);
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConcreteMessageEvent()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				return basicSetAbstractMessageEvent(null, msgs);
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				return ((InternalEList<?>)getConcreteMessageEvent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE:
				if (resolve) return getRuntimeMessage();
				return basicGetRuntimeMessage();
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				if (resolve) return getAbstractMessageEvent();
				return basicGetAbstractMessageEvent();
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				return getConcreteMessageEvent();
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				return getSendingObject();
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				return getReceivingObject();
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
				return getMessageName();
			case EventsPackage.MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE:
				return isBooleanParameterValue();
			case EventsPackage.MESSAGE_EVENT__INTEGER_PARAMETER_VALUE:
				return getIntegerParameterValue();
			case EventsPackage.MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE:
				return getEObjectParameterValue();
			case EventsPackage.MESSAGE_EVENT__STRING_PARAMETER_VALUE:
				return getStringParameterValue();
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized();
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete();
			case EventsPackage.MESSAGE_EVENT__OPERATION:
				return getOperation();
			case EventsPackage.MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				return getSideEffectOnEStructuralFeature();
			case EventsPackage.MESSAGE_EVENT__MESSAGE:
				if (resolve) return getMessage();
				return basicGetMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE:
				setRuntimeMessage((RuntimeMessage)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				setAbstractMessageEvent((MessageEvent)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				getConcreteMessageEvent().clear();
				getConcreteMessageEvent().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__MESSAGE:
				setMessage((Message)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE:
				setRuntimeMessage((RuntimeMessage)null);
				return;
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				setAbstractMessageEvent((MessageEvent)null);
				return;
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				getConcreteMessageEvent().clear();
				return;
			case EventsPackage.MESSAGE_EVENT__MESSAGE:
				setMessage((Message)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__RUNTIME_MESSAGE:
				return runtimeMessage != null;
			case EventsPackage.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT:
				return abstractMessageEvent != null;
			case EventsPackage.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT:
				return concreteMessageEvent != null && !concreteMessageEvent.isEmpty();
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				return getSendingObject() != null;
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				return getReceivingObject() != null;
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
				return MESSAGE_NAME_EDEFAULT == null ? getMessageName() != null : !MESSAGE_NAME_EDEFAULT.equals(getMessageName());
			case EventsPackage.MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE:
				return isSetBooleanParameterValue();
			case EventsPackage.MESSAGE_EVENT__INTEGER_PARAMETER_VALUE:
				return isSetIntegerParameterValue();
			case EventsPackage.MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE:
				return isSetEObjectParameterValue();
			case EventsPackage.MESSAGE_EVENT__STRING_PARAMETER_VALUE:
				return isSetStringParameterValue();
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized() != PARAMETERIZED_EDEFAULT;
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete() != CONCRETE_EDEFAULT;
			case EventsPackage.MESSAGE_EVENT__OPERATION:
				return getOperation() != null;
			case EventsPackage.MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				return getSideEffectOnEStructuralFeature() != null;
			case EventsPackage.MESSAGE_EVENT__MESSAGE:
				return message != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public String toString() {
		String returnString = getMessageName();
		if (isParameterized()){
			returnString += "(";				
			if (getEObjectParameterValue() != null){
				returnString += getEObjectName(getEObjectParameterValue());
			}else if(isSetBooleanParameterValue()){
				returnString += isBooleanParameterValue();
			}else if(isSetIntegerParameterValue()){
				returnString += getIntegerParameterValue();
			}else if(isSetStringParameterValue()){
				returnString += getStringParameterValue();
			}else{
				returnString += "?";
			}
			returnString += ")";
		}else{
			returnString += "()";
		}
		return returnString + " [" + getEObjectName(getSendingObject()) + "->" + getEObjectName(getReceivingObject()) + "]";
	}
	
	protected String getEObjectName(EObject eObject) {
		if(eObject == null) return "<null>";
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())
					&& eAttribute.getEType().getInstanceClass() == String.class) {
				return (String) eObject.eGet(eAttribute);
			}
		}
		return eObject.hashCode() + "";
	}

} //MessageEventImpl
