/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.SendMessageEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Send Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SendMessageEventImpl extends MessageEventImpl implements SendMessageEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SendMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.SEND_MESSAGE_EVENT;
	}

} //SendMessageEventImpl
