/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.SynchronousMessageEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synchronous Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SynchronousMessageEventImpl extends MessageEventImpl implements SynchronousMessageEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SynchronousMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.SYNCHRONOUS_MESSAGE_EVENT;
	}

} //SynchronousMessageEventImpl
