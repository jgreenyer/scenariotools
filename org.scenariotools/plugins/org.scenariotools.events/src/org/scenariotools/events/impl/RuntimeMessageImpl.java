/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.events.util.EventsUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Runtime Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getStringParameterValue <em>String Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.RuntimeMessageImpl#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuntimeMessageImpl extends RuntimeEObjectImpl implements RuntimeMessage {
	/**
	 * The cached value of the '{@link #getSendingObject() <em>Sending Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject sendingObject;

	/**
	 * The cached value of the '{@link #getReceivingObject() <em>Receiving Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceivingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject receivingObject;

	/**
	 * The default value of the '{@link #getMessageName() <em>Message Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageName()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIntegerParameterValue() <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_PARAMETER_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerParameterValue() <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerParameterValue()
	 * @generated
	 * @ordered
	 */
	protected int integerParameterValue = INTEGER_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the Integer Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean integerParameterValueESet;

	/**
	 * The default value of the '{@link #isBooleanParameterValue() <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBooleanParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BOOLEAN_PARAMETER_VALUE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBooleanParameterValue() <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBooleanParameterValue()
	 * @generated
	 * @ordered
	 */
	protected boolean booleanParameterValue = BOOLEAN_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the Boolean Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean booleanParameterValueESet;

	/**
	 * The cached value of the '{@link #getEObjectParameterValue() <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectParameterValue()
	 * @generated
	 * @ordered
	 */
	protected EObject eObjectParameterValue;

	/**
	 * This is true if the EObject Parameter Value reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean eObjectParameterValueESet;

	/**
	 * The default value of the '{@link #getStringParameterValue() <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final String STRING_PARAMETER_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStringParameterValue() <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringParameterValue()
	 * @generated
	 * @ordered
	 */
	protected String stringParameterValue = STRING_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the String Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean stringParameterValueESet;

	/**
	 * The default value of the '{@link #isConcrete() <em>Concrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConcrete()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONCRETE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConcrete() <em>Concrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConcrete()
	 * @generated
	 * @ordered
	 */
	protected boolean concrete = CONCRETE_EDEFAULT;

	/**
	 * The default value of the '{@link #isParameterized() <em>Parameterized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParameterized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARAMETERIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation operation;

	/**
	 * The cached value of the '{@link #getSideEffectOnEStructuralFeature() <em>Side Effect On EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSideEffectOnEStructuralFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature sideEffectOnEStructuralFeature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.RUNTIME_MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getSendingObject() {
		if (sendingObject != null && sendingObject.eIsProxy()) {
			InternalEObject oldSendingObject = (InternalEObject)sendingObject;
			sendingObject = eResolveProxy(oldSendingObject);
			if (sendingObject != oldSendingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT, oldSendingObject, sendingObject));
			}
		}
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetSendingObject() {
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSendingObject(EObject newSendingObject) {
		EObject oldSendingObject = sendingObject;
		sendingObject = newSendingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT, oldSendingObject, sendingObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getReceivingObject() {
		if (receivingObject != null && receivingObject.eIsProxy()) {
			InternalEObject oldReceivingObject = (InternalEObject)receivingObject;
			receivingObject = eResolveProxy(oldReceivingObject);
			if (receivingObject != oldReceivingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
			}
		}
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetReceivingObject() {
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceivingObject(EObject newReceivingObject) {
		EObject oldReceivingObject = receivingObject;
		receivingObject = newReceivingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getMessageName() {
		try {
			return getOperation().getName();
		} catch (NullPointerException e) {
			return "<undefined>";
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerParameterValue() {
		return integerParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerParameterValue(int newIntegerParameterValue) {
		int oldIntegerParameterValue = integerParameterValue;
		integerParameterValue = newIntegerParameterValue;
		boolean oldIntegerParameterValueESet = integerParameterValueESet;
		integerParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE, oldIntegerParameterValue, integerParameterValue, !oldIntegerParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIntegerParameterValue() {
		int oldIntegerParameterValue = integerParameterValue;
		boolean oldIntegerParameterValueESet = integerParameterValueESet;
		integerParameterValue = INTEGER_PARAMETER_VALUE_EDEFAULT;
		integerParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE, oldIntegerParameterValue, INTEGER_PARAMETER_VALUE_EDEFAULT, oldIntegerParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIntegerParameterValue() {
		return integerParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBooleanParameterValue() {
		return booleanParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBooleanParameterValue(boolean newBooleanParameterValue) {
		boolean oldBooleanParameterValue = booleanParameterValue;
		booleanParameterValue = newBooleanParameterValue;
		boolean oldBooleanParameterValueESet = booleanParameterValueESet;
		booleanParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE, oldBooleanParameterValue, booleanParameterValue, !oldBooleanParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBooleanParameterValue() {
		boolean oldBooleanParameterValue = booleanParameterValue;
		boolean oldBooleanParameterValueESet = booleanParameterValueESet;
		booleanParameterValue = BOOLEAN_PARAMETER_VALUE_EDEFAULT;
		booleanParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE, oldBooleanParameterValue, BOOLEAN_PARAMETER_VALUE_EDEFAULT, oldBooleanParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBooleanParameterValue() {
		return booleanParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getEObjectParameterValue() {
		if (eObjectParameterValue != null && eObjectParameterValue.eIsProxy()) {
			InternalEObject oldEObjectParameterValue = (InternalEObject)eObjectParameterValue;
			eObjectParameterValue = eResolveProxy(oldEObjectParameterValue);
			if (eObjectParameterValue != oldEObjectParameterValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, eObjectParameterValue));
			}
		}
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetEObjectParameterValue() {
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEObjectParameterValue(EObject newEObjectParameterValue) {
		EObject oldEObjectParameterValue = eObjectParameterValue;
		eObjectParameterValue = newEObjectParameterValue;
		boolean oldEObjectParameterValueESet = eObjectParameterValueESet;
		eObjectParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, eObjectParameterValue, !oldEObjectParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEObjectParameterValue() {
		EObject oldEObjectParameterValue = eObjectParameterValue;
		boolean oldEObjectParameterValueESet = eObjectParameterValueESet;
		eObjectParameterValue = null;
		eObjectParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, null, oldEObjectParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEObjectParameterValue() {
		return eObjectParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStringParameterValue() {
		return stringParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringParameterValue(String newStringParameterValue) {
		String oldStringParameterValue = stringParameterValue;
		stringParameterValue = newStringParameterValue;
		boolean oldStringParameterValueESet = stringParameterValueESet;
		stringParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE, oldStringParameterValue, stringParameterValue, !oldStringParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStringParameterValue() {
		String oldStringParameterValue = stringParameterValue;
		boolean oldStringParameterValueESet = stringParameterValueESet;
		stringParameterValue = STRING_PARAMETER_VALUE_EDEFAULT;
		stringParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE, oldStringParameterValue, STRING_PARAMETER_VALUE_EDEFAULT, oldStringParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStringParameterValue() {
		return stringParameterValueESet;
	}

	private boolean isConcreteIsComputed;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isConcrete() {
		if (!isConcreteIsComputed){
			if (!getOperation().getEParameters().isEmpty()){
				 concrete = (getEObjectParameterValue() != null || isSetBooleanParameterValue() || isSetIntegerParameterValue() || isSetStringParameterValue());
			}else{
				concrete = true;
			}
			isConcreteIsComputed = true;
		}
		return concrete;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isParameterized() {
		return !getOperation().getEParameters().isEmpty();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperation() {
		if (operation != null && operation.eIsProxy()) {
			InternalEObject oldOperation = (InternalEObject)operation;
			operation = (EOperation)eResolveProxy(oldOperation);
			if (operation != oldOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.RUNTIME_MESSAGE__OPERATION, oldOperation, operation));
			}
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(EOperation newOperation) {
		EOperation oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getSideEffectOnEStructuralFeature() {
		if (sideEffectOnEStructuralFeature != null && sideEffectOnEStructuralFeature.eIsProxy()) {
			InternalEObject oldSideEffectOnEStructuralFeature = (InternalEObject)sideEffectOnEStructuralFeature;
			sideEffectOnEStructuralFeature = (EStructuralFeature)eResolveProxy(oldSideEffectOnEStructuralFeature);
			if (sideEffectOnEStructuralFeature != oldSideEffectOnEStructuralFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE, oldSideEffectOnEStructuralFeature, sideEffectOnEStructuralFeature));
			}
		}
		return sideEffectOnEStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetSideEffectOnEStructuralFeature() {
		return sideEffectOnEStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSideEffectOnEStructuralFeature(EStructuralFeature newSideEffectOnEStructuralFeature) {
		EStructuralFeature oldSideEffectOnEStructuralFeature = sideEffectOnEStructuralFeature;
		sideEffectOnEStructuralFeature = newSideEffectOnEStructuralFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE, oldSideEffectOnEStructuralFeature, sideEffectOnEStructuralFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT:
				if (resolve) return getSendingObject();
				return basicGetSendingObject();
			case EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT:
				if (resolve) return getReceivingObject();
				return basicGetReceivingObject();
			case EventsPackage.RUNTIME_MESSAGE__MESSAGE_NAME:
				return getMessageName();
			case EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE:
				return getIntegerParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE:
				return isBooleanParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE:
				if (resolve) return getEObjectParameterValue();
				return basicGetEObjectParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE:
				return getStringParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__CONCRETE:
				return isConcrete();
			case EventsPackage.RUNTIME_MESSAGE__PARAMETERIZED:
				return isParameterized();
			case EventsPackage.RUNTIME_MESSAGE__OPERATION:
				if (resolve) return getOperation();
				return basicGetOperation();
			case EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				if (resolve) return getSideEffectOnEStructuralFeature();
				return basicGetSideEffectOnEStructuralFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT:
				setSendingObject((EObject)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT:
				setReceivingObject((EObject)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE:
				setIntegerParameterValue((Integer)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE:
				setBooleanParameterValue((Boolean)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE:
				setEObjectParameterValue((EObject)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE:
				setStringParameterValue((String)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__OPERATION:
				setOperation((EOperation)newValue);
				return;
			case EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				setSideEffectOnEStructuralFeature((EStructuralFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT:
				setSendingObject((EObject)null);
				return;
			case EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT:
				setReceivingObject((EObject)null);
				return;
			case EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE:
				unsetIntegerParameterValue();
				return;
			case EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE:
				unsetBooleanParameterValue();
				return;
			case EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE:
				unsetEObjectParameterValue();
				return;
			case EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE:
				unsetStringParameterValue();
				return;
			case EventsPackage.RUNTIME_MESSAGE__OPERATION:
				setOperation((EOperation)null);
				return;
			case EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				setSideEffectOnEStructuralFeature((EStructuralFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.RUNTIME_MESSAGE__SENDING_OBJECT:
				return sendingObject != null;
			case EventsPackage.RUNTIME_MESSAGE__RECEIVING_OBJECT:
				return receivingObject != null;
			case EventsPackage.RUNTIME_MESSAGE__MESSAGE_NAME:
				return MESSAGE_NAME_EDEFAULT == null ? getMessageName() != null : !MESSAGE_NAME_EDEFAULT.equals(getMessageName());
			case EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE:
				return isSetIntegerParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE:
				return isSetBooleanParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE:
				return isSetEObjectParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE:
				return isSetStringParameterValue();
			case EventsPackage.RUNTIME_MESSAGE__CONCRETE:
				return concrete != CONCRETE_EDEFAULT;
			case EventsPackage.RUNTIME_MESSAGE__PARAMETERIZED:
				return isParameterized() != PARAMETERIZED_EDEFAULT;
			case EventsPackage.RUNTIME_MESSAGE__OPERATION:
				return operation != null;
			case EventsPackage.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE:
				return sideEffectOnEStructuralFeature != null;
		}
		return super.eIsSet(featureID);
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		String returnString = getMessageName() + "(";
		if (getOperation() != null && !getOperation().getEParameters().isEmpty()){
			EParameter parameter = getOperation().getEParameters().get(0);
			returnString += parameter.getName();
			if (isConcrete())
				returnString += "=" + EventsUtil.getParameterValueString(this);
			else
				returnString += "=?";
		}
		returnString += ")";

		returnString += " [";
		if (getSendingObject() != null){
			returnString += EventsUtil.getName(getSendingObject());
		}
		returnString += "->";
		if (getReceivingObject() != null){
			returnString += EventsUtil.getName(getReceivingObject());
		}
		returnString += "]";
		
		return returnString;
	}
	
//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated
//	 */
//	@Override
//	public String toString() {
//		if (eIsProxy()) return super.toString();
//
//		StringBuffer result = new StringBuffer(super.toString());
//		result.append(" (integerParameterValue: ");
//		if (integerParameterValueESet) result.append(integerParameterValue); else result.append("<unset>");
//		result.append(", booleanParameterValue: ");
//		if (booleanParameterValueESet) result.append(booleanParameterValue); else result.append("<unset>");
//		result.append(", stringParameterValue: ");
//		if (stringParameterValueESet) result.append(stringParameterValue); else result.append("<unset>");
//		result.append(", concrete: ");
//		result.append(concrete);
//		result.append(')');
//		return result.toString();
//	}

} //RuntimeMessageImpl
