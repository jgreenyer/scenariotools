/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.ReceiveMessageEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receive Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ReceiveMessageEventImpl extends MessageEventImpl implements ReceiveMessageEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceiveMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.RECEIVE_MESSAGE_EVENT;
	}

} //ReceiveMessageEventImpl
