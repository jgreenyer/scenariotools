/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * represents the sending of an asychronous message
 * <!-- end-model-doc -->
 *
 *
 * @see org.scenariotools.events.EventsPackage#getSendMessageEvent()
 * @model
 * @generated
 */
public interface SendMessageEvent extends MessageEvent {
} // SendMessageEvent
