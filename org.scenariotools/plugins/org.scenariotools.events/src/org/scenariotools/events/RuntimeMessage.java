/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Runtime Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * represents a message sent from a sending object to a receiving object at runtime.
 * If the referenced operation specifies a (Boolean, Integer, String, EObject) parameter,
 * the message can carry a corresponding value. Note that only operations/messages
 * with at most one parameter are supported.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getStringParameterValue <em>String Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.scenariotools.events.RuntimeMessage#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage()
 * @model
 * @generated
 */
public interface RuntimeMessage extends EObject {
	/**
	 * Returns the value of the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sending Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sending Object</em>' reference.
	 * @see #setSendingObject(EObject)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_SendingObject()
	 * @model
	 * @generated
	 */
	EObject getSendingObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getSendingObject <em>Sending Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sending Object</em>' reference.
	 * @see #getSendingObject()
	 * @generated
	 */
	void setSendingObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiving Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiving Object</em>' reference.
	 * @see #setReceivingObject(EObject)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_ReceivingObject()
	 * @model
	 * @generated
	 */
	EObject getReceivingObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getReceivingObject <em>Receiving Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiving Object</em>' reference.
	 * @see #getReceivingObject()
	 * @generated
	 */
	void setReceivingObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Name</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_MessageName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getMessageName();

	/**
	 * Returns the value of the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Parameter Value</em>' attribute.
	 * @see #isSetIntegerParameterValue()
	 * @see #unsetIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_IntegerParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	int getIntegerParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Parameter Value</em>' attribute.
	 * @see #isSetIntegerParameterValue()
	 * @see #unsetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @generated
	 */
	void setIntegerParameterValue(int value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.RuntimeMessage#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @generated
	 */
	void unsetIntegerParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.RuntimeMessage#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Integer Parameter Value</em>' attribute is set.
	 * @see #unsetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @generated
	 */
	boolean isSetIntegerParameterValue();

	/**
	 * Returns the value of the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_BooleanParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isBooleanParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @generated
	 */
	void setBooleanParameterValue(boolean value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.RuntimeMessage#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	void unsetBooleanParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.RuntimeMessage#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Boolean Parameter Value</em>' attribute is set.
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	boolean isSetBooleanParameterValue();

	/**
	 * Returns the value of the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject Parameter Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject Parameter Value</em>' reference.
	 * @see #isSetEObjectParameterValue()
	 * @see #unsetEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_EObjectParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	EObject getEObjectParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EObject Parameter Value</em>' reference.
	 * @see #isSetEObjectParameterValue()
	 * @see #unsetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @generated
	 */
	void setEObjectParameterValue(EObject value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.RuntimeMessage#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @generated
	 */
	void unsetEObjectParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.RuntimeMessage#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EObject Parameter Value</em>' reference is set.
	 * @see #unsetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @generated
	 */
	boolean isSetEObjectParameterValue();

	/**
	 * Returns the value of the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_StringParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	String getStringParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @generated
	 */
	void setStringParameterValue(String value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.RuntimeMessage#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	void unsetStringParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.RuntimeMessage#getStringParameterValue <em>String Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String Parameter Value</em>' attribute is set.
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	boolean isSetStringParameterValue();

	/**
	 * Returns the value of the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Is false if the referenced operation specifies a parameter, but the message does specify a value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Concrete</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_Concrete()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	boolean isConcrete();

	/**
	 * Returns the value of the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * true if the referenced operation specifies a parameter
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameterized</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_Parameterized()
	 * @model transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	boolean isParameterized();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #setOperation(EOperation)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_Operation()
	 * @model
	 * @generated
	 */
	EOperation getOperation();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(EOperation value);

	/**
	 * Returns the value of the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Side Effect On EStructural Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Side Effect On EStructural Feature</em>' reference.
	 * @see #setSideEffectOnEStructuralFeature(EStructuralFeature)
	 * @see org.scenariotools.events.EventsPackage#getRuntimeMessage_SideEffectOnEStructuralFeature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getSideEffectOnEStructuralFeature();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.RuntimeMessage#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Side Effect On EStructural Feature</em>' reference.
	 * @see #getSideEffectOnEStructuralFeature()
	 * @generated
	 */
	void setSideEffectOnEStructuralFeature(EStructuralFeature value);

} // RuntimeMessage
