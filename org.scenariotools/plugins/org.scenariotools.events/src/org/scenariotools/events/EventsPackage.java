/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Basic package defining messages and synchronous and asynchronous message events.
 * <!-- end-model-doc -->
 * @see org.scenariotools.events.EventsFactory
 * @model kind="package"
 * @generated
 */
public interface EventsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "events";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.events/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "events";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EventsPackage eINSTANCE = org.scenariotools.events.impl.EventsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.EventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 0;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.RuntimeMessageImpl <em>Runtime Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.RuntimeMessageImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getRuntimeMessage()
	 * @generated
	 */
	int RUNTIME_MESSAGE = 1;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__SENDING_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__RECEIVING_OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__MESSAGE_NAME = 2;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE = 4;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE = 5;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__STRING_PARAMETER_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__CONCRETE = 7;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__PARAMETERIZED = 8;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__OPERATION = 9;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = 10;

	/**
	 * The number of structural features of the '<em>Runtime Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Runtime Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_MESSAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.MessageEventImpl <em>Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.MessageEventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getMessageEvent()
	 * @generated
	 */
	int MESSAGE_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__RUNTIME_MESSAGE = EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__SENDING_OBJECT = EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__RECEIVING_OBJECT = EVENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__MESSAGE_NAME = EVENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = EVENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = EVENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = EVENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__STRING_PARAMETER_VALUE = EVENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__PARAMETERIZED = EVENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__CONCRETE = EVENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__OPERATION = EVENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = EVENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__MESSAGE = EVENT_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.SendMessageEventImpl <em>Send Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.SendMessageEventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getSendMessageEvent()
	 * @generated
	 */
	int SEND_MESSAGE_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__RUNTIME_MESSAGE = MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__SENDING_OBJECT = MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__RECEIVING_OBJECT = MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__MESSAGE_NAME = MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__STRING_PARAMETER_VALUE = MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__PARAMETERIZED = MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__CONCRETE = MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__OPERATION = MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT__MESSAGE = MESSAGE_EVENT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Send Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT_FEATURE_COUNT = MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Send Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_MESSAGE_EVENT_OPERATION_COUNT = MESSAGE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.ReceiveMessageEventImpl <em>Receive Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.ReceiveMessageEventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getReceiveMessageEvent()
	 * @generated
	 */
	int RECEIVE_MESSAGE_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__RUNTIME_MESSAGE = MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__SENDING_OBJECT = MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__RECEIVING_OBJECT = MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__MESSAGE_NAME = MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__STRING_PARAMETER_VALUE = MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__PARAMETERIZED = MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__CONCRETE = MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__OPERATION = MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT__MESSAGE = MESSAGE_EVENT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Receive Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT_FEATURE_COUNT = MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Receive Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVE_MESSAGE_EVENT_OPERATION_COUNT = MESSAGE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.SynchronousMessageEventImpl <em>Synchronous Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.SynchronousMessageEventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getSynchronousMessageEvent()
	 * @generated
	 */
	int SYNCHRONOUS_MESSAGE_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__RUNTIME_MESSAGE = MESSAGE_EVENT__RUNTIME_MESSAGE;

	/**
	 * The feature id for the '<em><b>Abstract Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Concrete Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__SENDING_OBJECT = MESSAGE_EVENT__SENDING_OBJECT;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__RECEIVING_OBJECT = MESSAGE_EVENT__RECEIVING_OBJECT;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__MESSAGE_NAME = MESSAGE_EVENT__MESSAGE_NAME;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = MESSAGE_EVENT__INTEGER_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__STRING_PARAMETER_VALUE = MESSAGE_EVENT__STRING_PARAMETER_VALUE;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__PARAMETERIZED = MESSAGE_EVENT__PARAMETERIZED;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__CONCRETE = MESSAGE_EVENT__CONCRETE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__OPERATION = MESSAGE_EVENT__OPERATION;

	/**
	 * The feature id for the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT__MESSAGE = MESSAGE_EVENT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Synchronous Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT_FEATURE_COUNT = MESSAGE_EVENT_FEATURE_COUNT + 0;


	/**
	 * The number of operations of the '<em>Synchronous Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_MESSAGE_EVENT_OPERATION_COUNT = MESSAGE_EVENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.scenariotools.events.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.RuntimeMessage <em>Runtime Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Runtime Message</em>'.
	 * @see org.scenariotools.events.RuntimeMessage
	 * @generated
	 */
	EClass getRuntimeMessage();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.RuntimeMessage#getSendingObject <em>Sending Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sending Object</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getSendingObject()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EReference getRuntimeMessage_SendingObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.RuntimeMessage#getReceivingObject <em>Receiving Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiving Object</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getReceivingObject()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EReference getRuntimeMessage_ReceivingObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#getMessageName <em>Message Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Name</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getMessageName()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_MessageName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#getIntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getIntegerParameterValue()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_IntegerParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#isBooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#isBooleanParameterValue()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_BooleanParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.RuntimeMessage#getEObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getEObjectParameterValue()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EReference getRuntimeMessage_EObjectParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#getStringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getStringParameterValue()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_StringParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#isConcrete <em>Concrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Concrete</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#isConcrete()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_Concrete();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.RuntimeMessage#isParameterized <em>Parameterized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameterized</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#isParameterized()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EAttribute getRuntimeMessage_Parameterized();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.RuntimeMessage#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getOperation()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EReference getRuntimeMessage_Operation();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.RuntimeMessage#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Side Effect On EStructural Feature</em>'.
	 * @see org.scenariotools.events.RuntimeMessage#getSideEffectOnEStructuralFeature()
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	EReference getRuntimeMessage_SideEffectOnEStructuralFeature();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.SendMessageEvent <em>Send Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Send Message Event</em>'.
	 * @see org.scenariotools.events.SendMessageEvent
	 * @generated
	 */
	EClass getSendMessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.ReceiveMessageEvent <em>Receive Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receive Message Event</em>'.
	 * @see org.scenariotools.events.ReceiveMessageEvent
	 * @generated
	 */
	EClass getReceiveMessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.SynchronousMessageEvent <em>Synchronous Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synchronous Message Event</em>'.
	 * @see org.scenariotools.events.SynchronousMessageEvent
	 * @generated
	 */
	EClass getSynchronousMessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.MessageEvent <em>Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event</em>'.
	 * @see org.scenariotools.events.MessageEvent
	 * @generated
	 */
	EClass getMessageEvent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getRuntimeMessage <em>Runtime Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Message</em>'.
	 * @see org.scenariotools.events.MessageEvent#getRuntimeMessage()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_RuntimeMessage();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getAbstractMessageEvent <em>Abstract Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Abstract Message Event</em>'.
	 * @see org.scenariotools.events.MessageEvent#getAbstractMessageEvent()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_AbstractMessageEvent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.events.MessageEvent#getConcreteMessageEvent <em>Concrete Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Concrete Message Event</em>'.
	 * @see org.scenariotools.events.MessageEvent#getConcreteMessageEvent()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ConcreteMessageEvent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getSendingObject <em>Sending Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sending Object</em>'.
	 * @see org.scenariotools.events.MessageEvent#getSendingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_SendingObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getReceivingObject <em>Receiving Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiving Object</em>'.
	 * @see org.scenariotools.events.MessageEvent#getReceivingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ReceivingObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#getMessageName <em>Message Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Name</em>'.
	 * @see org.scenariotools.events.MessageEvent#getMessageName()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_MessageName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#isBooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.events.MessageEvent#isBooleanParameterValue()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_BooleanParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#getIntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.events.MessageEvent#getIntegerParameterValue()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_IntegerParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getEObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.events.MessageEvent#getEObjectParameterValue()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_EObjectParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#getStringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.events.MessageEvent#getStringParameterValue()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_StringParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#isParameterized <em>Parameterized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameterized</em>'.
	 * @see org.scenariotools.events.MessageEvent#isParameterized()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Parameterized();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#isConcrete <em>Concrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Concrete</em>'.
	 * @see org.scenariotools.events.MessageEvent#isConcrete()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Concrete();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see org.scenariotools.events.MessageEvent#getOperation()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_Operation();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Side Effect On EStructural Feature</em>'.
	 * @see org.scenariotools.events.MessageEvent#getSideEffectOnEStructuralFeature()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_SideEffectOnEStructuralFeature();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Message</em>'.
	 * @see org.scenariotools.events.MessageEvent#getMessage()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_Message();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EventsFactory getEventsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.EventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.RuntimeMessageImpl <em>Runtime Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.RuntimeMessageImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getRuntimeMessage()
		 * @generated
		 */
		EClass RUNTIME_MESSAGE = eINSTANCE.getRuntimeMessage();

		/**
		 * The meta object literal for the '<em><b>Sending Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_MESSAGE__SENDING_OBJECT = eINSTANCE.getRuntimeMessage_SendingObject();

		/**
		 * The meta object literal for the '<em><b>Receiving Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_MESSAGE__RECEIVING_OBJECT = eINSTANCE.getRuntimeMessage_ReceivingObject();

		/**
		 * The meta object literal for the '<em><b>Message Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__MESSAGE_NAME = eINSTANCE.getRuntimeMessage_MessageName();

		/**
		 * The meta object literal for the '<em><b>Integer Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE = eINSTANCE.getRuntimeMessage_IntegerParameterValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE = eINSTANCE.getRuntimeMessage_BooleanParameterValue();

		/**
		 * The meta object literal for the '<em><b>EObject Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE = eINSTANCE.getRuntimeMessage_EObjectParameterValue();

		/**
		 * The meta object literal for the '<em><b>String Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__STRING_PARAMETER_VALUE = eINSTANCE.getRuntimeMessage_StringParameterValue();

		/**
		 * The meta object literal for the '<em><b>Concrete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__CONCRETE = eINSTANCE.getRuntimeMessage_Concrete();

		/**
		 * The meta object literal for the '<em><b>Parameterized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_MESSAGE__PARAMETERIZED = eINSTANCE.getRuntimeMessage_Parameterized();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_MESSAGE__OPERATION = eINSTANCE.getRuntimeMessage_Operation();

		/**
		 * The meta object literal for the '<em><b>Side Effect On EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = eINSTANCE.getRuntimeMessage_SideEffectOnEStructuralFeature();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.SendMessageEventImpl <em>Send Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.SendMessageEventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getSendMessageEvent()
		 * @generated
		 */
		EClass SEND_MESSAGE_EVENT = eINSTANCE.getSendMessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.ReceiveMessageEventImpl <em>Receive Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.ReceiveMessageEventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getReceiveMessageEvent()
		 * @generated
		 */
		EClass RECEIVE_MESSAGE_EVENT = eINSTANCE.getReceiveMessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.SynchronousMessageEventImpl <em>Synchronous Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.SynchronousMessageEventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getSynchronousMessageEvent()
		 * @generated
		 */
		EClass SYNCHRONOUS_MESSAGE_EVENT = eINSTANCE.getSynchronousMessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.MessageEventImpl <em>Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.MessageEventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getMessageEvent()
		 * @generated
		 */
		EClass MESSAGE_EVENT = eINSTANCE.getMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Runtime Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__RUNTIME_MESSAGE = eINSTANCE.getMessageEvent_RuntimeMessage();

		/**
		 * The meta object literal for the '<em><b>Abstract Message Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT = eINSTANCE.getMessageEvent_AbstractMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Concrete Message Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT = eINSTANCE.getMessageEvent_ConcreteMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Sending Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__SENDING_OBJECT = eINSTANCE.getMessageEvent_SendingObject();

		/**
		 * The meta object literal for the '<em><b>Receiving Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__RECEIVING_OBJECT = eINSTANCE.getMessageEvent_ReceivingObject();

		/**
		 * The meta object literal for the '<em><b>Message Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__MESSAGE_NAME = eINSTANCE.getMessageEvent_MessageName();

		/**
		 * The meta object literal for the '<em><b>Boolean Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE = eINSTANCE.getMessageEvent_BooleanParameterValue();

		/**
		 * The meta object literal for the '<em><b>Integer Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__INTEGER_PARAMETER_VALUE = eINSTANCE.getMessageEvent_IntegerParameterValue();

		/**
		 * The meta object literal for the '<em><b>EObject Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE = eINSTANCE.getMessageEvent_EObjectParameterValue();

		/**
		 * The meta object literal for the '<em><b>String Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__STRING_PARAMETER_VALUE = eINSTANCE.getMessageEvent_StringParameterValue();

		/**
		 * The meta object literal for the '<em><b>Parameterized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__PARAMETERIZED = eINSTANCE.getMessageEvent_Parameterized();

		/**
		 * The meta object literal for the '<em><b>Concrete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__CONCRETE = eINSTANCE.getMessageEvent_Concrete();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__OPERATION = eINSTANCE.getMessageEvent_Operation();

		/**
		 * The meta object literal for the '<em><b>Side Effect On EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE = eINSTANCE.getMessageEvent_SideEffectOnEStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__MESSAGE = eINSTANCE.getMessageEvent_Message();

	}

} //EventsPackage
