/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Most abstract event class
 * <!-- end-model-doc -->
 *
 *
 * @see org.scenariotools.events.EventsPackage#getEvent()
 * @model abstract="true"
 * @generated
 */
public interface Event extends EObject {
} // Event
