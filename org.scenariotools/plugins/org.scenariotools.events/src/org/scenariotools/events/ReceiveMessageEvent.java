/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receive Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * represents the receiving of an asychronous message
 * <!-- end-model-doc -->
 *
 *
 * @see org.scenariotools.events.EventsPackage#getReceiveMessageEvent()
 * @model
 * @generated
 */
public interface ReceiveMessageEvent extends MessageEvent {
} // ReceiveMessageEvent
