/**
 */
package org.scenariotools.events;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * abstract event class representing a message being sent at runtime
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.events.MessageEvent#getRuntimeMessage <em>Runtime Message</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getAbstractMessageEvent <em>Abstract Message Event</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getConcreteMessageEvent <em>Concrete Message Event</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getStringParameterValue <em>String Parameter Value</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getSideEffectOnEStructuralFeature <em>Side Effect On EStructural Feature</em>}</li>
 *   <li>{@link org.scenariotools.events.MessageEvent#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.events.EventsPackage#getMessageEvent()
 * @model abstract="true"
 * @generated
 */
public interface MessageEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Runtime Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Message</em>' reference.
	 * @see #setRuntimeMessage(RuntimeMessage)
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_RuntimeMessage()
	 * @model
	 * @generated
	 */
	RuntimeMessage getRuntimeMessage();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.MessageEvent#getRuntimeMessage <em>Runtime Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Message</em>' reference.
	 * @see #getRuntimeMessage()
	 * @generated
	 */
	void setRuntimeMessage(RuntimeMessage value);

	/**
	 * Returns the value of the '<em><b>Abstract Message Event</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.events.MessageEvent#getConcreteMessageEvent <em>Concrete Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Message Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Message Event</em>' reference.
	 * @see #setAbstractMessageEvent(MessageEvent)
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_AbstractMessageEvent()
	 * @see org.scenariotools.events.MessageEvent#getConcreteMessageEvent
	 * @model opposite="concreteMessageEvent"
	 * @generated
	 */
	MessageEvent getAbstractMessageEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.MessageEvent#getAbstractMessageEvent <em>Abstract Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Message Event</em>' reference.
	 * @see #getAbstractMessageEvent()
	 * @generated
	 */
	void setAbstractMessageEvent(MessageEvent value);

	/**
	 * Returns the value of the '<em><b>Concrete Message Event</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.events.MessageEvent#getAbstractMessageEvent <em>Abstract Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concrete Message Event</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concrete Message Event</em>' reference list.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_ConcreteMessageEvent()
	 * @see org.scenariotools.events.MessageEvent#getAbstractMessageEvent
	 * @model opposite="abstractMessageEvent"
	 * @generated
	 */
	EList<MessageEvent> getConcreteMessageEvent();

	/**
	 * Returns the value of the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sending Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sending Object</em>' reference.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_SendingObject()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EObject getSendingObject();

	/**
	 * Returns the value of the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiving Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiving Object</em>' reference.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_ReceivingObject()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EObject getReceivingObject();

	/**
	 * Returns the value of the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Name</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_MessageName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getMessageName();

	/**
	 * Returns the value of the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_BooleanParameterValue()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isBooleanParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.MessageEvent#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Boolean Parameter Value</em>' attribute is set.
	 * @see #isBooleanParameterValue()
	 * @generated
	 */
	boolean isSetBooleanParameterValue();

	/**
	 * Returns the value of the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Parameter Value</em>' attribute.
	 * @see #isSetIntegerParameterValue()
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_IntegerParameterValue()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getIntegerParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.MessageEvent#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Integer Parameter Value</em>' attribute is set.
	 * @see #getIntegerParameterValue()
	 * @generated
	 */
	boolean isSetIntegerParameterValue();

	/**
	 * Returns the value of the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject Parameter Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject Parameter Value</em>' reference.
	 * @see #isSetEObjectParameterValue()
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_EObjectParameterValue()
	 * @model resolveProxies="false" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EObject getEObjectParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.MessageEvent#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EObject Parameter Value</em>' reference is set.
	 * @see #getEObjectParameterValue()
	 * @generated
	 */
	boolean isSetEObjectParameterValue();

	/**
	 * Returns the value of the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_StringParameterValue()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getStringParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.MessageEvent#getStringParameterValue <em>String Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String Parameter Value</em>' attribute is set.
	 * @see #getStringParameterValue()
	 * @generated
	 */
	boolean isSetStringParameterValue();

	/**
	 * Returns the value of the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * true if the referenced operation specifies a parameter
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameterized</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_Parameterized()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isParameterized();

	/**
	 * Returns the value of the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Is false if the referenced operation specifies a parameter, but the message does specify a value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Concrete</em>' attribute.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_Concrete()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isConcrete();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_Operation()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EOperation getOperation();

	/**
	 * Returns the value of the '<em><b>Side Effect On EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Side Effect On EStructural Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Side Effect On EStructural Feature</em>' reference.
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_SideEffectOnEStructuralFeature()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EStructuralFeature getSideEffectOnEStructuralFeature();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference.
	 * @see #setMessage(Message)
	 * @see org.scenariotools.events.EventsPackage#getMessageEvent_Message()
	 * @model
	 * @generated
	 */
	Message getMessage();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.MessageEvent#getMessage <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(Message value);

} // MessageEvent
