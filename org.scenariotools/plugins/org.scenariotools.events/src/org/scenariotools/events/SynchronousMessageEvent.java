/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronous Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * represents the sending and receiving of a message as one, synchronous event.
 * <!-- end-model-doc -->
 *
 *
 * @see org.scenariotools.events.EventsPackage#getSynchronousMessageEvent()
 * @model
 * @generated
 */
public interface SynchronousMessageEvent extends MessageEvent {
} // SynchronousMessageEvent
