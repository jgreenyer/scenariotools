package org.scenariotools.synthesis.graphviz.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.synthesis.graphviz.GraphvizExporter;
import org.scenariotools.synthesis.graphviz.RuntimeStateGraphExporter;

public class ExportControllerAction implements IObjectActionDelegate {
	protected String jobName="Graphviz export.";
	protected GraphvizExporter createExporter(){
		return new RuntimeStateGraphExporter();
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.getFirstElement();

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.getResource(
				URI.createPlatformResourceURI(file.getFullPath().toString(),
						true), true);

		final Object contents = resource
				.getContents().get(0);

		final Job job = new Job(jobName) {

			protected IStatus run(IProgressMonitor monitor) {

				GraphvizExporter exporter = createExporter();
				exporter.setInputFile(file);
				exporter.export(contents);

				return Status.OK_STATUS;
			}

		};

		job.setUser(true);
		job.schedule();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

}
