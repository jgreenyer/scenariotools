package org.scenariotools.synthesis.graphviz;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class GraphvizExporter {
	public enum OutputFormat {
		PDF, PNG, DOT, PLAIN
	}

	protected OutputFormat outputFormat = OutputFormat.PDF;
	protected StringBuffer graphvizData;
	private String outPath;

	private IFile inputFile;

	public GraphvizExporter() {
		graphvizData = new StringBuffer();
	}

	public void export(Object obj) {
		graphvizData.append("digraph G {\n}");
		outputGraph();
		makeImage();
	}

	public void export(Object obj, OutputFormat outputFormat) {
		this.outputFormat = outputFormat;
		export(obj);
	}

	public void setInputFile(IFile inputFile) {
		this.inputFile = inputFile;
		try {
			outPath = inputFile.getLocation().removeFileExtension()
					.addFileExtension("gv").toOSString();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void outputGraph() {
		try {
			FileWriter writer = new FileWriter(outPath);
			writer.write(graphvizData.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getOutputFormatString() {
		switch (outputFormat) {
		case PDF:
			return "pdf";
		case PNG:
			return "png";
		case DOT:
			return "dot";
		case PLAIN:
			return "plain";
		}
		return "";
	}

	protected void makeImage() {
		String filePath = outPath.substring(0, outPath.length() - 3);
		try {
			String cmdString = "dot -T" + getOutputFormatString() + " -o"
					+ filePath + "." + getOutputFormatString() + " \""
					+ outPath + "\"";
//			System.out.println(cmdString);
			final Process proc = Runtime.getRuntime().exec(cmdString);
			new Thread() {
				@Override
				public void run() {
					BufferedReader outReader = new BufferedReader(
							new InputStreamReader(proc.getInputStream()));
					BufferedReader errReader = new BufferedReader(
							new InputStreamReader(proc.getErrorStream()));
					try {
//						System.out.println("output:");
						String line = outReader.readLine();
						while (line != null) {
//							System.out.println(line);
							line = outReader.readLine();
						}
//						System.out.println("errors:");
						line = errReader.readLine();
						while (line != null) {
//							System.out.println(line);
							line = errReader.readLine();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}.start();
			proc.waitFor();

			if (outputFormat == OutputFormat.PNG) {
				Shell shell = new Shell(SWT.DIALOG_TRIM | SWT.RESIZE);
				Image image = new Image(Display.getCurrent(), filePath + ".png");
				shell.setLayout(new GridLayout(1, false));
				shell.setText("Graphviz output");
				shell.setBackgroundImage(image);
				shell.setSize(Display.getCurrent().getBounds().width * 3 / 4,
						Display.getCurrent().getBounds().height * 3 / 4);
				shell.open();
			}
			inputFile.getParent().refreshLocal(IResource.DEPTH_ONE, null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getOutPath() {
		return outPath;
	}

	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}

}