package org.scenariotools.synthesis.graphviz;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.timed.timedruntime.TimedMSDRuntimeState;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class RuntimeStateGraphExporter extends GraphvizExporter {
	protected RuntimeStateGraph graph;
	private int stateNumber;

	public void export(Object object) {
		RuntimeUtil.Helper.resetObjectNumbers();
		this.graph = (RuntimeStateGraph) object;
		graphvizData.append("digraph G {\n");
		graphvizData.append("node[style=filled, gradientangle=90]\n");
		stateNumber = 0;

		Map<String, Set<State>> clusterNameToStateSetMap = new HashMap<String, Set<State>>();
		Set<State> unclusteredStates = new HashSet<State>();
		for (State state : graph.getStates()) {
			String clusterString = state.getStringToStringAnnotationMap().get(
					"visCluster");
			if (clusterString != null && !clusterString.isEmpty()) {
				Set<State> statesInClusterSet = clusterNameToStateSetMap
						.get(clusterString);
				if (statesInClusterSet == null) {
					statesInClusterSet = new HashSet<State>();
					clusterNameToStateSetMap.put(clusterString,
							statesInClusterSet);
				}
				statesInClusterSet.add(state);
			} else {
				unclusteredStates.add(state);
			}
		}

		for (Map.Entry<String, Set<State>> clusterNameToStateSetMapEntry : clusterNameToStateSetMap
				.entrySet()) {
			graphvizData.append("subgraph cluster_"
					+ clusterNameToStateSetMapEntry.getKey() + " {\n");
			graphvizData.append("label = \""
					+ clusterNameToStateSetMapEntry.getKey() + "\"");
			for (State state : clusterNameToStateSetMapEntry.getValue())
				exportState((RuntimeState) state);
			graphvizData.append("}\n");
		}

		for (State state : unclusteredStates)
			exportState((RuntimeState) state);

		graphvizData.append("}\n");
		outputGraph();
		makeImage();
	}

	protected String getStateAnnotations(RuntimeState state) {
		String stateLog = state.getStringToStringAnnotationMap()
				.get("stateLog");
		if (stateLog != null)
			return stateLog.replace("\n", "<br align=\"left\"/>") + "<br align=\"left\"/>";
		else
			return "";
	}

	protected void exportState(RuntimeState state) {
		graphvizData.append(state.hashCode() + "[label=<");
		boolean modalMsgs = (state.getMessageEventToModalMessageEventMap()
				.size() > 0);
		boolean activeMsds = (state instanceof MSDRuntimeState)
				&& ((MSDRuntimeState) state).getActiveProcesses().size() > 0;

//				if (!activeMsds && !modalMsgs) {
//					graphvizData.append(getControllerStateString(state));
//				} else {
					graphvizData.append(getMsdStateString((MSDRuntimeState) state));
					//graphvizData.append("<br align=\"left\"/>");
//				}

		if (state.isSafetyViolationOccurredInAssumptions()) {
			graphvizData
					.append("<font color=\"darkgreen\">- Safety-violation in Assumptions -<br/></font>");
		}
		if (state.isSafetyViolationOccurredInRequirements()) {
			graphvizData
					.append("<font color=\"red2\">- Safety-violation in Requirements -<br/></font>");
		}
		
		if (((TimedMSDRuntimeState) state).isTimedSafetyViolationOccurredInAssumptions()) {
			graphvizData
					.append("<font color=\"darkgreen\">- Timed safety-violation in Assumptions -<br/></font>");
		}
		if (((TimedMSDRuntimeState) state).isTimedSafetyViolationOccurredInRequirements()) {
			graphvizData
					.append("<font color=\"red2\">- Timed safety-violation in Requirements -<br/></font>");
		}
		
		if (((TimedMSDRuntimeState) state).isTimeInconsistencyOccurredInRequirements()) {
			graphvizData
					.append("<font color=\"red2\">- Time inconsistency in Requirements -<br/></font>");
		}
		
		if (((TimedMSDRuntimeState) state).isTimeInconsistencyOccurredInAssumptions()) {
			graphvizData
					.append("<font color=\"darkgreen\">- Time inconsistency in Assumptions -<br/></font>");
		}

		if (((TimedMSDRuntimeState) state).isTimingConditionsInAssumptionsSatisfiable()) {
			graphvizData
					.append("<font color=\"darkgreen\">- Timing conditions in Assumptions are not satisfiable -<br/></font>");
		}
		if (((TimedMSDRuntimeState) state).isTimingConditionsInRequirementsSatisfiable()) {
			graphvizData
					.append("<font color=\"red2\">- Timing conditions in Requirements are not satisfiable -<br/></font>");
		}
		
		if (((TimedMSDRuntimeState) state).isUpperBoundRealTimeInconsistency()){
			graphvizData
					.append("<font color=\"red2\">- Upper bound real time inconsistency -<br/></font>");
		}

		String stateAnnotations=getStateAnnotations(state);
		graphvizData.append(stateAnnotations);

		graphvizData.append(">");

		outputNodeShape(!modalMsgs && !activeMsds && stateAnnotations.equals(""));

		boolean stateWin = false;
		boolean stateLoseBuechi = false;
		boolean stateGoal = false;
		if (state.getStringToBooleanAnnotationMap().get("win") != null) {
			stateWin = state.getStringToBooleanAnnotationMap().get("win");
			stateLoseBuechi = state.getStringToBooleanAnnotationMap().get(
					"loseBuechi");
			stateGoal = state.getStringToBooleanAnnotationMap().get("goal");
		}

		if (graph.getStartState() == state) {
			graphvizData.append(", style=\"rounded,filled\", color=\"red\"");
		} else {
			graphvizData.append(", style=\"rounded,filled\"");
		}

		if (stateWin && stateLoseBuechi)
			graphvizData.append(", fillcolor=\"rosybrown1:green\"");
		else if (stateWin && !stateGoal)
			graphvizData.append(", fillcolor=\"palegreen:white\"");
		else if (stateWin && stateGoal)
			graphvizData.append(", fillcolor=\"green:white\"");
		else if (stateLoseBuechi)
			graphvizData.append(", fillcolor=\"rosybrown1:white\"");
		else
			graphvizData.append(", fillcolor=\"lightgray:white\"");
		if (stateGoal) {
			graphvizData.append(", penwidth=\"3\"");
			if (graph.getStartState() != state)
				graphvizData.append(", color=\"blue\"");
		}
		Boolean requirementsSafetyViolation = state.getStringToBooleanAnnotationMap().get("requirementsSafetyViolation");
		Boolean assumptionSafetyViolation = state.getStringToBooleanAnnotationMap().get("assumptionSafetyViolation");
		Boolean timedRequirementsSafetyViolation = state.getStringToBooleanAnnotationMap().get("timedRequirementsSafetyViolation");
		Boolean timedAssumptionSafetyViolation = state.getStringToBooleanAnnotationMap().get("timedAssumptionSafetyViolation");
		
		if(requirementsSafetyViolation != null && assumptionSafetyViolation != null
				&& requirementsSafetyViolation && assumptionSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"gold\"");
		else if(requirementsSafetyViolation != null
				&& requirementsSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"deeppink\"");
		else if(assumptionSafetyViolation != null
				&& assumptionSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"cyan4\"");
		
		if(timedRequirementsSafetyViolation != null && timedAssumptionSafetyViolation != null
				&& timedRequirementsSafetyViolation && timedAssumptionSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"gold\"");
		else if(timedRequirementsSafetyViolation != null
				&& timedRequirementsSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"deeppink\"");
		else if(timedAssumptionSafetyViolation != null
				&& timedAssumptionSafetyViolation)
			graphvizData.append(", penwidth=\"3\", color=\"cyan4\"");

		
		
		graphvizData.append("];\n");
		for (Transition transition : state.getOutgoingTransition()) {
			exportTransition(transition);
		}
	}
	
	
	protected void outputNodeShape(boolean plain) {
		if (plain)
			graphvizData.append(", shape=circle");
		else
			graphvizData.append(", shape=box");
	}

	protected String getStateNumber(RuntimeState state){
		String result = "";
		String passedIndex = state.getStringToStringAnnotationMap().get(
				"passedIndex");
		if (passedIndex != null && !passedIndex.isEmpty())
			result+=passedIndex + "<br/>";
		
		if (passedIndex == null || passedIndex.isEmpty())
			result = (stateNumber++) + "<br/>";
		return result;
	}

	protected String getControllerStateString(RuntimeState state) {
		return getStateNumber(state);
	}

	protected String getMsdStateString(MSDRuntimeState state) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getStateNumber(state));
		for (ActiveProcess activeProcess : state.getActiveProcesses()) {
			if (activeProcess instanceof ActiveMSD)
				stringBuilder
						.append(getActiveMsdString((ActiveMSD) activeProcess)
								+ "<br align=\"left\"/>");
			else if (activeProcess instanceof ActiveMSS)
				stringBuilder
						.append(getActiveMssString((ActiveMSS) activeProcess)
								+ "<br align=\"left\"/>");
			else
				stringBuilder.append(activeProcess.getCurrentState() + "<br align=\"left\"/>");
		}
		return stringBuilder.toString();
	}

	protected String getActiveMssString(ActiveMSS amss) {
		String returnString = "";
		if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(amss)) {
			returnString += "A-MSS ";
		} else {
			returnString += "R-MSS ";
		}

		String cutString = "";
		cutString += " (" + amss.getCurrentState().getUmlState().getName()
				+ ")";

		return returnString + amss.getStateMachine().getName() + cutString;
	}

	private String getActiveMsdString(ActiveMSD amsd) {
		String returnString = "";
		if (RuntimeUtil.Helper.isEnvironmentAssumption(amsd.getInteraction())) {
			returnString += "A-MSD ";
		} else {
			returnString += "R-MSD ";
		}

		String cutString = "";
		cutString += " (";

		LinkedList<Lifeline> lifelines = new LinkedList<Lifeline>(amsd
				.getLifelineBindings().getLifelineToEObjectMap().keySet());
		Collections.sort(lifelines, new Comparator<Lifeline>() {
			@Override
			public int compare(Lifeline o1, Lifeline o2) {
				return o1.getRepresents().getName()
						.compareTo(o2.getRepresents().getName());
			}
		});
		int i = 0;
		int numLifelines = lifelines.size();
		for (Lifeline lifeline : lifelines) {
			cutString += lifeline.getRepresents().getName() + ":";
			cutString += RuntimeUtil.Helper
					.getInteractionFragmentNumberOnLifeline(
							((ActiveMSDCut) amsd.getCurrentState())
									.getLifelineToInteractionFragmentMap().get(
											lifeline), lifeline);

			if (i < numLifelines - 1) {// not last one
				cutString += ", ";
			}
			i++;
		}
		cutString += ")";
		String variablesString=", ";
		for(Entry<String,Boolean> entry:amsd.getVariableValuations().getVariableNameToBooleanValueMap())
			variablesString+=entry.getKey()+"="+entry.getValue()+", ";
		for(Entry<String,EObject> entry:amsd.getVariableValuations().getVariableNameToEObjectValueMap())
			variablesString+=entry.getKey()+"="+entry.getValue()+", ";
		for(Entry<String,Integer> entry:amsd.getVariableValuations().getVariableNameToIntegerValueMap())
			variablesString+=entry.getKey()+"="+entry.getValue()+", ";
		for(Entry<String,String> entry:amsd.getVariableValuations().getVariableNameToStringValueMap())
			variablesString+=entry.getKey()+"="+entry.getValue()+", ";
		variablesString=variablesString.substring(0, variablesString.length()-2);

		return returnString + amsd.getInteraction().getName() + cutString + variablesString;

	}

	protected void exportTransition(Transition transition) {
		if (transition.getTargetState() == null){
			return;
		}
		graphvizData.append(transition.getSourceState().hashCode() + "->"
				+ transition.getTargetState().hashCode() + "[");
		exportTransitionAttributeString(transition);
		graphvizData.append("];\n");
	}
	
	protected void exportTransitionAttributeString(Transition transition){
		if (transition.getEvent() == null) {
			if (transition.getStringToStringAnnotationMap().get(
					"transitionLabel") != null) {
				graphvizData.append("label=<"
						+ transition.getStringToStringAnnotationMap().get(
								"transitionLabel") + ">");
			}
			if (transition.getStringToStringAnnotationMap().get(
					"transitionColor") != null) {
				graphvizData.append(", color=\""
						+ transition.getStringToStringAnnotationMap().get(
								"transitionColor") + "\"");
			}
		} else {
			graphvizData.append("label=<"
					+ getEventLabel(transition.getEvent()) + ">");
		}

		if (!isControllable(transition)) {
			graphvizData.append(", style=\"dashed\"");
		}
	}

	protected boolean isControllable(Transition transition) {
		return !(((RuntimeState) transition.getSourceState()).getObjectSystem() != null
				&& transition.getEvent() != null
				&& transition.getEvent() instanceof MessageEvent && !((RuntimeState) transition
					.getSourceState()).getObjectSystem().isControllable(
				((MessageEvent) transition.getEvent()).getSendingObject()));
	}

	protected String getEventLabel(Event event) {
		if (event instanceof MessageEvent)
			return getMessageEventLabel((MessageEvent) event);
		return "";
	}

	protected String getMessageEventLabel(MessageEvent event) {
		if (event == null)
			return "update";
		else
			return event.getMessageName()
					+ "("
					+ (event.isParameterized() ? getParameterValueString(event)
							: "")
					+ ") ["
					+ RuntimeUtil.Helper.getEObjectName(event
							.getSendingObject())
					+ "-&gt;"
					+ RuntimeUtil.Helper.getEObjectName(event
							.getReceivingObject()) + "]";
	}

	private String getParameterValueString(MessageEvent event) {
		if (event.isSetBooleanParameterValue())
			return String.valueOf(event.isBooleanParameterValue());
		else if (event.isSetEObjectParameterValue())
			return String.valueOf(event.getEObjectParameterValue());
		else if (event.isSetIntegerParameterValue())
			return String.valueOf(event.getIntegerParameterValue());
		else if (event.isSetStringParameterValue())
			return String.valueOf(event.getStringParameterValue());
		return "?";
	}
}
