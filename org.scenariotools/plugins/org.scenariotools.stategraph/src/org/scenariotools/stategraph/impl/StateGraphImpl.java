/**
 */
package org.scenariotools.stategraph.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.stategraph.StategraphPackage;
import org.scenariotools.stategraph.StrategyKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.impl.StateGraphImpl#getStates <em>States</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.impl.StateGraphImpl#getStartState <em>Start State</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.impl.StateGraphImpl#getStrategyKind <em>Strategy Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateGraphImpl extends RuntimeEObjectImpl implements StateGraph {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected State startState;

	/**
	 * The default value of the '{@link #getStrategyKind() <em>Strategy Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategyKind()
	 * @generated
	 * @ordered
	 */
	protected static final StrategyKind STRATEGY_KIND_EDEFAULT = StrategyKind.OTHER;

	/**
	 * The cached value of the '{@link #getStrategyKind() <em>Strategy Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategyKind()
	 * @generated
	 * @ordered
	 */
	protected StrategyKind strategyKind = STRATEGY_KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StategraphPackage.Literals.STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentWithInverseEList<State>(State.class, this, StategraphPackage.STATE_GRAPH__STATES, StategraphPackage.STATE__STATE_GRAPH);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject)startState;
			startState = (State)eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StategraphPackage.STATE_GRAPH__START_STATE, oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(State newStartState) {
		State oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StategraphPackage.STATE_GRAPH__START_STATE, oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrategyKind getStrategyKind() {
		return strategyKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategyKind(StrategyKind newStrategyKind) {
		StrategyKind oldStrategyKind = strategyKind;
		strategyKind = newStrategyKind == null ? STRATEGY_KIND_EDEFAULT : newStrategyKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StategraphPackage.STATE_GRAPH__STRATEGY_KIND, oldStrategyKind, strategyKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStates()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				return getStates();
			case StategraphPackage.STATE_GRAPH__START_STATE:
				if (resolve) return getStartState();
				return basicGetStartState();
			case StategraphPackage.STATE_GRAPH__STRATEGY_KIND:
				return getStrategyKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case StategraphPackage.STATE_GRAPH__START_STATE:
				setStartState((State)newValue);
				return;
			case StategraphPackage.STATE_GRAPH__STRATEGY_KIND:
				setStrategyKind((StrategyKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				getStates().clear();
				return;
			case StategraphPackage.STATE_GRAPH__START_STATE:
				setStartState((State)null);
				return;
			case StategraphPackage.STATE_GRAPH__STRATEGY_KIND:
				setStrategyKind(STRATEGY_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StategraphPackage.STATE_GRAPH__STATES:
				return states != null && !states.isEmpty();
			case StategraphPackage.STATE_GRAPH__START_STATE:
				return startState != null;
			case StategraphPackage.STATE_GRAPH__STRATEGY_KIND:
				return strategyKind != STRATEGY_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (strategyKind: ");
		result.append(strategyKind);
		result.append(')');
		return result.toString();
	}

} //StateGraphImpl
