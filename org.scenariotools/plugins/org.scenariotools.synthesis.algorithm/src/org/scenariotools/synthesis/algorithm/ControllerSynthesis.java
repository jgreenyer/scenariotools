package org.scenariotools.synthesis.algorithm;

import org.scenariotools.runtime.RuntimeStateGraph;

public interface ControllerSynthesis {
	
	public RuntimeStateGraph synthesizeController(RuntimeStateGraph specification, boolean includeNonDeterminism);

}
