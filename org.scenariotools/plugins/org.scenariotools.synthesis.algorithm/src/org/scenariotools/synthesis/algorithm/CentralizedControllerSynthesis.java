package org.scenariotools.synthesis.algorithm;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class CentralizedControllerSynthesis implements ControllerSynthesis {

	/**
	 * Searches a centralized implementation for the given specification.
	 * 
	 * @param specGraph
	 *            the specification to take into account
	 * @return a valid centralized implementation of the specification specGraph
	 */
	public RuntimeStateGraph synthesizeController(RuntimeStateGraph specGraph,
			boolean includeNonDeterminism) {

		// contains states identified as losing
		Set<RuntimeState> losing = new HashSet<RuntimeState>();

		// contains events already evaluated for the given state
		Map<RuntimeState, Set<MessageEvent>> evaluatedEvents = new HashMap<RuntimeState, Set<MessageEvent>>();

		// contains states which still have to be (re-)considered by the
		// algorithm, because
		// 1. they are losing but currently reachable, or
		// 2. they are goal states but the uncontrollable predecessors have not
		// been determined yet, or
		// 3. they are no goal states but no controllable predecessor has been
		// selected yet
		Deque<RuntimeState> unhandled = new LinkedList<RuntimeState>();

		// maps (keys of) specification states to implementation states
		Map<RuntimeState, RuntimeState> specState2ImplState = new HashMap<RuntimeState, RuntimeState>();
		Map<RuntimeState, RuntimeState> implState2SpecState = new HashMap<RuntimeState, RuntimeState>();

		// initial spec. state corresponds to initial impl. state and will be
		// examined first
		RuntimeState specStartState = (RuntimeState) specGraph.getStartState();
		unhandled.add(specStartState);

		// create initial implementation
		RuntimeStateGraph implementation = RuntimeFactory.eINSTANCE
				.createRuntimeStateGraph();
		RuntimeState implStartState = RuntimeFactory.eINSTANCE
				.createRuntimeState();
		implementation.getStates().add(implStartState);
		implementation.setStartState(implStartState);
		implStartState.setObjectSystem(specStartState.getObjectSystem());

		specState2ImplState.put(specStartState, implStartState);
		implState2SpecState.put(implStartState, specStartState);

		while (!unhandled.isEmpty()) {
			RuntimeState current = unhandled.removeFirst();

			if (losing.contains(current)) {
				// 1. state is losing, need to make unreachable

				if (current == specStartState) {
					// losing cannot be prevented, because the start state is
					// always reachable; no implementation possible
					return null;
				} else {
					System.out.println("isolating state: " + current);
					State currentImplState = specState2ImplState.get(current);
					// remove incoming controllable implementation
					// transitions
					if (currentImplState != null) {
						List<Transition> oldIncomingImplTransitions = new LinkedList<Transition>(
								currentImplState.getIncomingTransition());

						// loop over all incoming implementation transitions
						for (Transition incomingTransition : oldIncomingImplTransitions) {
							System.out
									.println("isolating: handling transition "
											+ incomingTransition
											+ " with event "
											+ incomingTransition.getEvent());
							if (isControllable(incomingTransition)) {
								System.out.println("isControllable!");
								unhandled.add(implState2SpecState
										.get(incomingTransition
												.getSourceState()));
								incomingTransition.setSourceState(null);
								incomingTransition.setTargetState(null);
							}
						}
					}

					// loop over all incoming specification transitions
					for (Transition transition : current
							.getIncomingTransition()) {
						// propagate losing status to source states of
						// uncontrollable transitions
						if (isGoalState((RuntimeState) transition
								.getSourceState())) {
							// mark source state as losing if it
							// uncontrollably
							// can reach the current one
							RuntimeState sourceState = (RuntimeState) transition
									.getSourceState();
							losing.add(sourceState);
							unhandled.add(sourceState);
						}
					}
				}
			} else if (isGoalState(current)) {
				// 2. goal state, need to determine uncontrollably reachable
				// successors,
				// provided there is a corresponding implementation state

				State currentImplState = specState2ImplState.get(current);
				if (currentImplState != null) {
					// generate all uncontrollable successors
					for (ModalMessageEvent modalEvent : current
							.getMessageEventToModalMessageEventMap().values()) {
						if (!isControllable(
								modalEvent.getRepresentedMessageEvent(),
								current)
								&& modalEvent.getRepresentedMessageEvent()
										.isConcrete()) {

							addSuccessorTransition(specGraph, unhandled,
									specState2ImplState, implState2SpecState,
									implementation, current, currentImplState,
									modalEvent.getRepresentedMessageEvent());
						}
					}
				}
			} else {
				// 3. need to select a successor transition and add it to the
				// controller

				State currentImplState = specState2ImplState.get(current);
				if (currentImplState != null) {
					Set<MessageEvent> temporaryDisabledEvents = new HashSet<MessageEvent>();

					MessageEvent selectedEvent;
					Transition newImplTransition = null;
					do {
						// select one controllable successor
						selectedEvent = selectControllableAllowedEvent(current,
								evaluatedEvents.get(current),
								temporaryDisabledEvents);
						System.out.println("selected: " + selectedEvent);
						if (selectedEvent != null) {
							newImplTransition = addSuccessorTransition(
									specGraph, unhandled, specState2ImplState,
									implState2SpecState, implementation,
									current, currentImplState, selectedEvent);

							if (closesUnliveLoop(newImplTransition)) {
								// remove implementation transition again,
								// because it would cause to an un-live loop
								newImplTransition.setSourceState(null);
								newImplTransition.setTargetState(null);
								temporaryDisabledEvents
										.add((MessageEvent) newImplTransition
												.getEvent());
								newImplTransition = null;
							} else {
								// mark selected event as evaluated
								Set<MessageEvent> evaluatedEventsSet = evaluatedEvents
										.get(current);
								if (evaluatedEventsSet == null) {
									evaluatedEventsSet = new HashSet<MessageEvent>();
									evaluatedEvents.put(current,
											evaluatedEventsSet);
								}
								evaluatedEventsSet.add(selectedEvent);

								if (includeNonDeterminism)
									// mark event of already added transition as
									// disabled
									temporaryDisabledEvents
											.add((MessageEvent) newImplTransition
													.getEvent());
							}

						}
						// loop while no new transition has been added yet, but
						// only if it's not already known that there are no
						// allowed events anymore
					} while ((newImplTransition == null || includeNonDeterminism)
							&& selectedEvent != null);

					if (currentImplState.getOutgoingTransition().size() == 0) {
						// no transition could be added (or previously existed)
						// -> mark state as losing
						unhandled.add(current);
						losing.add(current);
					}
				}
			}
		}
		for (RuntimeState losingState : losing)
			System.out.println("losing state" + losingState);
		return removeUnreachableStates(implementation);
	}

	private RuntimeStateGraph removeUnreachableStates(
			RuntimeStateGraph implementation) {
		RuntimeStateGraph retainedImplementation = RuntimeFactory.eINSTANCE
				.createRuntimeStateGraph();
		Deque<RuntimeState> open = new LinkedList<RuntimeState>();
		Set<RuntimeState> closed = new HashSet<RuntimeState>();

		RuntimeState startStateCopy = ((RuntimeState) implementation
				.getStartState());
		open.add((RuntimeState) startStateCopy);
		retainedImplementation.getStates().add(startStateCopy);
		retainedImplementation.setStartState(startStateCopy);

		while (!open.isEmpty()) {
			RuntimeState current = open.removeFirst();
			for (Transition transition : current.getOutgoingTransition()) {
				RuntimeState target = (RuntimeState) transition
						.getTargetState();
				if (!closed.contains(target)) {
					open.add(target);
					retainedImplementation.getStates().add(target);
				}
			}
			closed.add(current);
		}

		return retainedImplementation;
	}

	private Transition addSuccessorTransition(RuntimeStateGraph specGraph,
			Deque<RuntimeState> unhandled,
			Map<RuntimeState, RuntimeState> specState2ImplState,
			Map<RuntimeState, RuntimeState> implState2SpecState,
			RuntimeStateGraph implementation, RuntimeState current,
			State currentImplState, MessageEvent selectedEvent) {
		// determine new spec. state and note for handling
		RuntimeState newSpecState = (RuntimeState)(specGraph.generateSuccessor(current,
				selectedEvent).getTargetState());
		RuntimeState newImplState = specState2ImplState.get(newSpecState);
		if (newImplState == null) {
			// create new impl. state
			newImplState = RuntimeFactory.eINSTANCE.createRuntimeState();
			implementation.getStates().add(newImplState);
			specState2ImplState.put(newSpecState, newImplState);
			implState2SpecState.put(newImplState, newSpecState);
			unhandled.add(newSpecState);
		}

		newImplState.setObjectSystem(newSpecState.getObjectSystem());

		// create new impl. transition and wire it
		Transition newTransiton = StategraphFactory.eINSTANCE
				.createTransition();
		newTransiton.setTargetState(newImplState);
		newTransiton.setSourceState(currentImplState);
		newTransiton.setEvent(selectedEvent);

		return newTransiton;
	}

	private boolean isControllable(Transition transition) {
		return isControllable(transition.getEvent(),
				transition.getSourceState());
	}

	protected boolean isControllable(Event event, State sourceState) {
		return event instanceof MessageEvent
				&& sourceState instanceof RuntimeState
				&& ((RuntimeState) sourceState).getObjectSystem()
						.isControllable(
								((MessageEvent) event).getSendingObject());
	}

	private MessageEvent selectControllableAllowedEvent(RuntimeState state,
			Set<MessageEvent> losingEvents,
			Set<MessageEvent> temporaryLosingEvents) {
		if (losingEvents == null)
			losingEvents = new HashSet<MessageEvent>();
		for (ModalMessageEvent modalEvent : state
				.getMessageEventToModalMessageEventMap().values()) {
			if (modalEvent.getRepresentedMessageEvent().isConcrete()
					&& isControllable(modalEvent.getRepresentedMessageEvent(),
							state)
					&& !modalEvent.getRequirementsModality()
							.isSafetyViolating()
					&& !losingEvents.contains(modalEvent
							.getRepresentedMessageEvent())
					&& !temporaryLosingEvents.contains(modalEvent
							.getRepresentedMessageEvent())) {
				if (modalEvent.getRepresentedMessageEvent().getMessageName()
						.equals("enterAllowed")) {
					System.out.println("name:"
							+ modalEvent.getRepresentedMessageEvent()
									.getMessageName());
					System.out.println("param:"
							+ modalEvent.getRepresentedMessageEvent()
									.isBooleanParameterValue());
					System.out.println("hash:"
							+ modalEvent.getRepresentedMessageEvent()
									.hashCode());
				}
				return modalEvent.getRepresentedMessageEvent();
			}
		}
		return null;
	}

	private boolean isGoalState(RuntimeState state) {
		boolean goal = true;
		for (ModalMessageEvent modalEvent : state
				.getMessageEventToModalMessageEventMap().values()) {
			if (modalEvent.getRequirementsModality().isMandatory()) {
				goal = false;
				break;
			}
		}
		return goal;
	}

	// searches implementation for loop of only controllable transitions
	// involving newTransition
	private boolean closesUnliveLoop(Transition newImplTransition) {
		if (!isControllable(newImplTransition))
			return false;

		Set<RuntimeState> closed = new HashSet<RuntimeState>();
		Deque<RuntimeState> open = new LinkedList<RuntimeState>();

		// start at target state
		open.add((RuntimeState) newImplTransition.getSourceState());

		while (!open.isEmpty()) {
			RuntimeState current = open.removeFirst();

			for (Transition transition : current.getIncomingTransition()) {
				RuntimeState predecessor = (RuntimeState) transition
						.getSourceState();
				if (!closed.contains(predecessor)) {
					if (isControllable(transition)) {
						if (newImplTransition.getTargetState() == predecessor)
							return true;
						open.add(predecessor);
					}
				}
			}
			closed.add(current);
		}

		return false;
	}
}
