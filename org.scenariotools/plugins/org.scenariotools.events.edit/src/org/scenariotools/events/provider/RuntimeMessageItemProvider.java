/**
 */
package org.scenariotools.events.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.RuntimeMessage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.events.RuntimeMessage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeMessageItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeMessageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSendingObjectPropertyDescriptor(object);
			addReceivingObjectPropertyDescriptor(object);
			addMessageNamePropertyDescriptor(object);
			addIntegerParameterValuePropertyDescriptor(object);
			addBooleanParameterValuePropertyDescriptor(object);
			addEObjectParameterValuePropertyDescriptor(object);
			addStringParameterValuePropertyDescriptor(object);
			addConcretePropertyDescriptor(object);
			addParameterizedPropertyDescriptor(object);
			addOperationPropertyDescriptor(object);
			addSideEffectOnEStructuralFeaturePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Sending Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSendingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_sendingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_sendingObject_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__SENDING_OBJECT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Receiving Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReceivingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_receivingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_receivingObject_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__RECEIVING_OBJECT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_messageName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_messageName_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__MESSAGE_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Integer Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegerParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_integerParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_integerParameterValue_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Boolean Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBooleanParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_booleanParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_booleanParameterValue_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the EObject Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEObjectParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_eObjectParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_eObjectParameterValue_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__EOBJECT_PARAMETER_VALUE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the String Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStringParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_stringParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_stringParameterValue_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Concrete feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcretePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_concrete_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_concrete_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__CONCRETE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parameterized feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterizedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_parameterized_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_parameterized_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__PARAMETERIZED,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_operation_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__OPERATION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Side Effect On EStructural Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSideEffectOnEStructuralFeaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeMessage_sideEffectOnEStructuralFeature_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeMessage_sideEffectOnEStructuralFeature_feature", "_UI_RuntimeMessage_type"),
				 EventsPackage.Literals.RUNTIME_MESSAGE__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns RuntimeMessage.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RuntimeMessage"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RuntimeMessage)object).getMessageName();
		return label == null || label.length() == 0 ?
			getString("_UI_RuntimeMessage_type") :
			getString("_UI_RuntimeMessage_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RuntimeMessage.class)) {
			case EventsPackage.RUNTIME_MESSAGE__MESSAGE_NAME:
			case EventsPackage.RUNTIME_MESSAGE__INTEGER_PARAMETER_VALUE:
			case EventsPackage.RUNTIME_MESSAGE__BOOLEAN_PARAMETER_VALUE:
			case EventsPackage.RUNTIME_MESSAGE__STRING_PARAMETER_VALUE:
			case EventsPackage.RUNTIME_MESSAGE__CONCRETE:
			case EventsPackage.RUNTIME_MESSAGE__PARAMETERIZED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EventsEditPlugin.INSTANCE;
	}

}
