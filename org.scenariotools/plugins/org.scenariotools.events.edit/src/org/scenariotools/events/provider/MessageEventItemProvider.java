/**
 */
package org.scenariotools.events.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.MessageEvent;

/**
 * This is the item provider adapter for a {@link org.scenariotools.events.MessageEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MessageEventItemProvider
	extends EventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRuntimeMessagePropertyDescriptor(object);
			addAbstractMessageEventPropertyDescriptor(object);
			addConcreteMessageEventPropertyDescriptor(object);
			addSendingObjectPropertyDescriptor(object);
			addReceivingObjectPropertyDescriptor(object);
			addMessageNamePropertyDescriptor(object);
			addBooleanParameterValuePropertyDescriptor(object);
			addIntegerParameterValuePropertyDescriptor(object);
			addEObjectParameterValuePropertyDescriptor(object);
			addStringParameterValuePropertyDescriptor(object);
			addParameterizedPropertyDescriptor(object);
			addConcretePropertyDescriptor(object);
			addOperationPropertyDescriptor(object);
			addSideEffectOnEStructuralFeaturePropertyDescriptor(object);
			addMessagePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Runtime Message feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuntimeMessagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_runtimeMessage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_runtimeMessage_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__RUNTIME_MESSAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Abstract Message Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractMessageEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_abstractMessageEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_abstractMessageEvent_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__ABSTRACT_MESSAGE_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Concrete Message Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcreteMessageEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_concreteMessageEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_concreteMessageEvent_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__CONCRETE_MESSAGE_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sending Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSendingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_sendingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_sendingObject_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__SENDING_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Receiving Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReceivingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_receivingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_receivingObject_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__RECEIVING_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_messageName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_messageName_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__MESSAGE_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Boolean Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBooleanParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_booleanParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_booleanParameterValue_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Integer Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegerParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_integerParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_integerParameterValue_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__INTEGER_PARAMETER_VALUE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the EObject Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEObjectParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_eObjectParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_eObjectParameterValue_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__EOBJECT_PARAMETER_VALUE,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the String Parameter Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStringParameterValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_stringParameterValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_stringParameterValue_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__STRING_PARAMETER_VALUE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parameterized feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterizedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_parameterized_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_parameterized_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__PARAMETERIZED,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Concrete feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcretePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_concrete_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_concrete_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__CONCRETE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_operation_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__OPERATION,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Side Effect On EStructural Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSideEffectOnEStructuralFeaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_sideEffectOnEStructuralFeature_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_sideEffectOnEStructuralFeature_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__SIDE_EFFECT_ON_ESTRUCTURAL_FEATURE,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_message_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_message_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__MESSAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns MessageEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MessageEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MessageEvent)object).getMessageName();
		return label == null || label.length() == 0 ?
			getString("_UI_MessageEvent_type") :
			getString("_UI_MessageEvent_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MessageEvent.class)) {
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
			case EventsPackage.MESSAGE_EVENT__BOOLEAN_PARAMETER_VALUE:
			case EventsPackage.MESSAGE_EVENT__INTEGER_PARAMETER_VALUE:
			case EventsPackage.MESSAGE_EVENT__STRING_PARAMETER_VALUE:
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
