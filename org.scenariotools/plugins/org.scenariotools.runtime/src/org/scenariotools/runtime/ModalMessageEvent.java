/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modal Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.ModalMessageEvent#getRepresentedMessageEvent <em>Represented Message Event</em>}</li>
 *   <li>{@link org.scenariotools.runtime.ModalMessageEvent#getRequirementsModality <em>Requirements Modality</em>}</li>
 *   <li>{@link org.scenariotools.runtime.ModalMessageEvent#getAssumptionsModality <em>Assumptions Modality</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.runtime.RuntimePackage#getModalMessageEvent()
 * @model
 * @generated
 */
public interface ModalMessageEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Represented Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Represented Message Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Represented Message Event</em>' reference.
	 * @see #setRepresentedMessageEvent(MessageEvent)
	 * @see org.scenariotools.runtime.RuntimePackage#getModalMessageEvent_RepresentedMessageEvent()
	 * @model
	 * @generated
	 */
	MessageEvent getRepresentedMessageEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.ModalMessageEvent#getRepresentedMessageEvent <em>Represented Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Represented Message Event</em>' reference.
	 * @see #getRepresentedMessageEvent()
	 * @generated
	 */
	void setRepresentedMessageEvent(MessageEvent value);

	/**
	 * Returns the value of the '<em><b>Requirements Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirements Modality</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirements Modality</em>' containment reference.
	 * @see #setRequirementsModality(Modality)
	 * @see org.scenariotools.runtime.RuntimePackage#getModalMessageEvent_RequirementsModality()
	 * @model containment="true"
	 * @generated
	 */
	Modality getRequirementsModality();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.ModalMessageEvent#getRequirementsModality <em>Requirements Modality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirements Modality</em>' containment reference.
	 * @see #getRequirementsModality()
	 * @generated
	 */
	void setRequirementsModality(Modality value);

	/**
	 * Returns the value of the '<em><b>Assumptions Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assumptions Modality</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assumptions Modality</em>' containment reference.
	 * @see #setAssumptionsModality(Modality)
	 * @see org.scenariotools.runtime.RuntimePackage#getModalMessageEvent_AssumptionsModality()
	 * @model containment="true"
	 * @generated
	 */
	Modality getAssumptionsModality();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.ModalMessageEvent#getAssumptionsModality <em>Assumptions Modality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assumptions Modality</em>' containment reference.
	 * @see #getAssumptionsModality()
	 * @generated
	 */
	void setAssumptionsModality(Modality value);

} // ModalMessageEvent
