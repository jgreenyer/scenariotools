/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.Modality#isSafetyViolating <em>Safety Violating</em>}</li>
 *   <li>{@link org.scenariotools.runtime.Modality#isMandatory <em>Mandatory</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.runtime.RuntimePackage#getModality()
 * @model
 * @generated
 */
public interface Modality extends EObject {
	/**
	 * Returns the value of the '<em><b>Safety Violating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violating</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violating</em>' attribute.
	 * @see #setSafetyViolating(boolean)
	 * @see org.scenariotools.runtime.RuntimePackage#getModality_SafetyViolating()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolating();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.Modality#isSafetyViolating <em>Safety Violating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violating</em>' attribute.
	 * @see #isSafetyViolating()
	 * @generated
	 */
	void setSafetyViolating(boolean value);

	/**
	 * Returns the value of the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory</em>' attribute.
	 * @see #setMandatory(boolean)
	 * @see org.scenariotools.runtime.RuntimePackage#getModality_Mandatory()
	 * @model
	 * @generated
	 */
	boolean isMandatory();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.Modality#isMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory</em>' attribute.
	 * @see #isMandatory()
	 * @generated
	 */
	void setMandatory(boolean value);

} // Modality
