/**
 */
package org.scenariotools.runtime.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.scenariotools.events.MessageEvent;

import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.Modality;
import org.scenariotools.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Modal Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.impl.ModalMessageEventImpl#getRepresentedMessageEvent <em>Represented Message Event</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.ModalMessageEventImpl#getRequirementsModality <em>Requirements Modality</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.ModalMessageEventImpl#getAssumptionsModality <em>Assumptions Modality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModalMessageEventImpl extends RuntimeEObjectImpl implements ModalMessageEvent {
	/**
	 * The cached value of the '{@link #getRepresentedMessageEvent() <em>Represented Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentedMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected MessageEvent representedMessageEvent;

	/**
	 * The cached value of the '{@link #getRequirementsModality() <em>Requirements Modality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequirementsModality()
	 * @generated
	 * @ordered
	 */
	protected Modality requirementsModality;

	/**
	 * The cached value of the '{@link #getAssumptionsModality() <em>Assumptions Modality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssumptionsModality()
	 * @generated
	 * @ordered
	 */
	protected Modality assumptionsModality;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModalMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MODAL_MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent getRepresentedMessageEvent() {
		if (representedMessageEvent != null && representedMessageEvent.eIsProxy()) {
			InternalEObject oldRepresentedMessageEvent = (InternalEObject)representedMessageEvent;
			representedMessageEvent = (MessageEvent)eResolveProxy(oldRepresentedMessageEvent);
			if (representedMessageEvent != oldRepresentedMessageEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT, oldRepresentedMessageEvent, representedMessageEvent));
			}
		}
		return representedMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent basicGetRepresentedMessageEvent() {
		return representedMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentedMessageEvent(MessageEvent newRepresentedMessageEvent) {
		MessageEvent oldRepresentedMessageEvent = representedMessageEvent;
		representedMessageEvent = newRepresentedMessageEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT, oldRepresentedMessageEvent, representedMessageEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modality getRequirementsModality() {
		return requirementsModality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequirementsModality(Modality newRequirementsModality, NotificationChain msgs) {
		Modality oldRequirementsModality = requirementsModality;
		requirementsModality = newRequirementsModality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY, oldRequirementsModality, newRequirementsModality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequirementsModality(Modality newRequirementsModality) {
		if (newRequirementsModality != requirementsModality) {
			NotificationChain msgs = null;
			if (requirementsModality != null)
				msgs = ((InternalEObject)requirementsModality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY, null, msgs);
			if (newRequirementsModality != null)
				msgs = ((InternalEObject)newRequirementsModality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY, null, msgs);
			msgs = basicSetRequirementsModality(newRequirementsModality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY, newRequirementsModality, newRequirementsModality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modality getAssumptionsModality() {
		return assumptionsModality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssumptionsModality(Modality newAssumptionsModality, NotificationChain msgs) {
		Modality oldAssumptionsModality = assumptionsModality;
		assumptionsModality = newAssumptionsModality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY, oldAssumptionsModality, newAssumptionsModality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssumptionsModality(Modality newAssumptionsModality) {
		if (newAssumptionsModality != assumptionsModality) {
			NotificationChain msgs = null;
			if (assumptionsModality != null)
				msgs = ((InternalEObject)assumptionsModality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY, null, msgs);
			if (newAssumptionsModality != null)
				msgs = ((InternalEObject)newAssumptionsModality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY, null, msgs);
			msgs = basicSetAssumptionsModality(newAssumptionsModality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY, newAssumptionsModality, newAssumptionsModality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY:
				return basicSetRequirementsModality(null, msgs);
			case RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY:
				return basicSetAssumptionsModality(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT:
				if (resolve) return getRepresentedMessageEvent();
				return basicGetRepresentedMessageEvent();
			case RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY:
				return getRequirementsModality();
			case RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY:
				return getAssumptionsModality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT:
				setRepresentedMessageEvent((MessageEvent)newValue);
				return;
			case RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY:
				setRequirementsModality((Modality)newValue);
				return;
			case RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY:
				setAssumptionsModality((Modality)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT:
				setRepresentedMessageEvent((MessageEvent)null);
				return;
			case RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY:
				setRequirementsModality((Modality)null);
				return;
			case RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY:
				setAssumptionsModality((Modality)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT:
				return representedMessageEvent != null;
			case RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY:
				return requirementsModality != null;
			case RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY:
				return assumptionsModality != null;
		}
		return super.eIsSet(featureID);
	}

} //ModalMessageEventImpl
