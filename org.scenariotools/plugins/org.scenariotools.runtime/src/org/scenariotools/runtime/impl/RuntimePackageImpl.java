/**
 */
package org.scenariotools.runtime.impl;

import java.util.Map;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.scenariotools.events.EventsPackage;

import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.Modality;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimePackageImpl extends EPackageImpl implements RuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modalMessageEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modalityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventToModalMessageEventMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventToTransitionMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.runtime.RuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuntimePackageImpl() {
		super(eNS_URI, RuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuntimePackage init() {
		if (isInited) return (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Obtain or create and register package
		RuntimePackageImpl theRuntimePackage = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RuntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StategraphPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRuntimePackage.createPackageContents();

		// Initialize created meta-data
		theRuntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuntimePackage.eNS_URI, theRuntimePackage);
		return theRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeState() {
		return runtimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_ObjectSystem() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_MessageEventToModalMessageEventMap() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeState_SafetyViolationOccurredInRequirements() {
		return (EAttribute)runtimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeState_SafetyViolationOccurredInAssumptions() {
		return (EAttribute)runtimeStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_EventToTransitionMap() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeStateGraph() {
		return runtimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event() {
		return runtimeStateGraphEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__Init() {
		return runtimeStateGraphEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState() {
		return runtimeStateGraphEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__IsEventInAlphabet__Event() {
		return runtimeStateGraphEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSystem() {
		return objectSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_RootObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_ControllableObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_UncontrollableObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_RootObjectsContained() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__IsControllable__EObject() {
		return objectSystemEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__IsEnvironmentRuntimeMessage__RuntimeMessage() {
		return objectSystemEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__IsEnvironmentMessageEvent__MessageEvent() {
		return objectSystemEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__ContainsEObject__EObject() {
		return objectSystemEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModalMessageEvent() {
		return modalMessageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModalMessageEvent_RepresentedMessageEvent() {
		return (EReference)modalMessageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModalMessageEvent_RequirementsModality() {
		return (EReference)modalMessageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModalMessageEvent_AssumptionsModality() {
		return (EReference)modalMessageEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModality() {
		return modalityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModality_SafetyViolating() {
		return (EAttribute)modalityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModality_Mandatory() {
		return (EAttribute)modalityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventToModalMessageEventMapEntry() {
		return messageEventToModalMessageEventMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventToModalMessageEventMapEntry_Value() {
		return (EReference)messageEventToModalMessageEventMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventToModalMessageEventMapEntry_Key() {
		return (EReference)messageEventToModalMessageEventMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventToTransitionMapEntry() {
		return eventToTransitionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToTransitionMapEntry_Key() {
		return (EReference)eventToTransitionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToTransitionMapEntry_Value() {
		return (EReference)eventToTransitionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactory getRuntimeFactory() {
		return (RuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		runtimeStateEClass = createEClass(RUNTIME_STATE);
		createEReference(runtimeStateEClass, RUNTIME_STATE__OBJECT_SYSTEM);
		createEReference(runtimeStateEClass, RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP);
		createEAttribute(runtimeStateEClass, RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS);
		createEAttribute(runtimeStateEClass, RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS);
		createEReference(runtimeStateEClass, RUNTIME_STATE__EVENT_TO_TRANSITION_MAP);

		runtimeStateGraphEClass = createEClass(RUNTIME_STATE_GRAPH);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___INIT);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT);

		objectSystemEClass = createEClass(OBJECT_SYSTEM);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__ROOT_OBJECTS);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__CONTROLLABLE_OBJECTS);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__ROOT_OBJECTS_CONTAINED);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___IS_ENVIRONMENT_RUNTIME_MESSAGE__RUNTIMEMESSAGE);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT);

		modalMessageEventEClass = createEClass(MODAL_MESSAGE_EVENT);
		createEReference(modalMessageEventEClass, MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT);
		createEReference(modalMessageEventEClass, MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY);
		createEReference(modalMessageEventEClass, MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY);

		modalityEClass = createEClass(MODALITY);
		createEAttribute(modalityEClass, MODALITY__SAFETY_VIOLATING);
		createEAttribute(modalityEClass, MODALITY__MANDATORY);

		messageEventToModalMessageEventMapEntryEClass = createEClass(MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY);
		createEReference(messageEventToModalMessageEventMapEntryEClass, MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__VALUE);
		createEReference(messageEventToModalMessageEventMapEntryEClass, MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__KEY);

		eventToTransitionMapEntryEClass = createEClass(EVENT_TO_TRANSITION_MAP_ENTRY);
		createEReference(eventToTransitionMapEntryEClass, EVENT_TO_TRANSITION_MAP_ENTRY__KEY);
		createEReference(eventToTransitionMapEntryEClass, EVENT_TO_TRANSITION_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StategraphPackage theStategraphPackage = (StategraphPackage)EPackage.Registry.INSTANCE.getEPackage(StategraphPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		runtimeStateEClass.getESuperTypes().add(theStategraphPackage.getState());
		runtimeStateGraphEClass.getESuperTypes().add(theStategraphPackage.getStateGraph());

		// Initialize classes, features, and operations; add parameters
		initEClass(runtimeStateEClass, RuntimeState.class, "RuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuntimeState_ObjectSystem(), this.getObjectSystem(), null, "objectSystem", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeState_MessageEventToModalMessageEventMap(), this.getMessageEventToModalMessageEventMapEntry(), null, "messageEventToModalMessageEventMap", null, 0, -1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeState_SafetyViolationOccurredInRequirements(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInRequirements", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeState_SafetyViolationOccurredInAssumptions(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInAssumptions", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeState_EventToTransitionMap(), this.getEventToTransitionMapEntry(), null, "eventToTransitionMap", null, 0, -1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(runtimeStateGraphEClass, RuntimeStateGraph.class, "RuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event(), theStategraphPackage.getTransition(), "generateSuccessor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRuntimeState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getRuntimeStateGraph__Init(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState(), theStategraphPackage.getTransition(), "generateAllSuccessors", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRuntimeState(), "runtimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRuntimeStateGraph__IsEventInAlphabet__Event(), ecorePackage.getEBoolean(), "isEventInAlphabet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(objectSystemEClass, ObjectSystem.class, "ObjectSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectSystem_RootObjects(), theEcorePackage.getEObject(), null, "rootObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystem_ControllableObjects(), theEcorePackage.getEObject(), null, "controllableObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystem_UncontrollableObjects(), theEcorePackage.getEObject(), null, "uncontrollableObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystem_RootObjectsContained(), theEcorePackage.getEObject(), null, "rootObjectsContained", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getObjectSystem__IsControllable__EObject(), ecorePackage.getEBoolean(), "isControllable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getObjectSystem__IsEnvironmentRuntimeMessage__RuntimeMessage(), theEcorePackage.getEBoolean(), "isEnvironmentRuntimeMessage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getRuntimeMessage(), "runtimeMessage", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getObjectSystem__IsEnvironmentMessageEvent__MessageEvent(), theEcorePackage.getEBoolean(), "isEnvironmentMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getObjectSystem__ContainsEObject__EObject(), ecorePackage.getEBoolean(), "containsEObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(modalMessageEventEClass, ModalMessageEvent.class, "ModalMessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModalMessageEvent_RepresentedMessageEvent(), theEventsPackage.getMessageEvent(), null, "representedMessageEvent", null, 0, 1, ModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModalMessageEvent_RequirementsModality(), this.getModality(), null, "requirementsModality", null, 0, 1, ModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModalMessageEvent_AssumptionsModality(), this.getModality(), null, "assumptionsModality", null, 0, 1, ModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modalityEClass, Modality.class, "Modality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModality_SafetyViolating(), theEcorePackage.getEBoolean(), "safetyViolating", null, 0, 1, Modality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModality_Mandatory(), theEcorePackage.getEBoolean(), "mandatory", null, 0, 1, Modality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageEventToModalMessageEventMapEntryEClass, Map.Entry.class, "MessageEventToModalMessageEventMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEventToModalMessageEventMapEntry_Value(), this.getModalMessageEvent(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEventToModalMessageEventMapEntry_Key(), theEventsPackage.getMessageEvent(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventToTransitionMapEntryEClass, Map.Entry.class, "EventToTransitionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventToTransitionMapEntry_Key(), theEventsPackage.getEvent(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventToTransitionMapEntry_Value(), theStategraphPackage.getTransition(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //RuntimePackageImpl
