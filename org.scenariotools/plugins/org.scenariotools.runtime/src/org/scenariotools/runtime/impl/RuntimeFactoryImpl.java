/**
 */
package org.scenariotools.runtime.impl;

import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.*;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeFactoryImpl extends EFactoryImpl implements RuntimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RuntimeFactory init() {
		try {
			RuntimeFactory theRuntimeFactory = (RuntimeFactory)EPackage.Registry.INSTANCE.getEFactory(RuntimePackage.eNS_URI);
			if (theRuntimeFactory != null) {
				return theRuntimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RuntimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RuntimePackage.RUNTIME_STATE: return createRuntimeState();
			case RuntimePackage.RUNTIME_STATE_GRAPH: return createRuntimeStateGraph();
			case RuntimePackage.OBJECT_SYSTEM: return createObjectSystem();
			case RuntimePackage.MODAL_MESSAGE_EVENT: return createModalMessageEvent();
			case RuntimePackage.MODALITY: return createModality();
			case RuntimePackage.MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY: return (EObject)createMessageEventToModalMessageEventMapEntry();
			case RuntimePackage.EVENT_TO_TRANSITION_MAP_ENTRY: return (EObject)createEventToTransitionMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeState createRuntimeState() {
		RuntimeStateImpl runtimeState = new RuntimeStateImpl();
		return runtimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeStateGraph createRuntimeStateGraph() {
		RuntimeStateGraphImpl runtimeStateGraph = new RuntimeStateGraphImpl();
		return runtimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSystem createObjectSystem() {
		ObjectSystemImpl objectSystem = new ObjectSystemImpl();
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModalMessageEvent createModalMessageEvent() {
		ModalMessageEventImpl modalMessageEvent = new ModalMessageEventImpl();
		return modalMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modality createModality() {
		ModalityImpl modality = new ModalityImpl();
		return modality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<MessageEvent, ModalMessageEvent> createMessageEventToModalMessageEventMapEntry() {
		MessageEventToModalMessageEventMapEntryImpl messageEventToModalMessageEventMapEntry = new MessageEventToModalMessageEventMapEntryImpl();
		return messageEventToModalMessageEventMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Event, Transition> createEventToTransitionMapEntry() {
		EventToTransitionMapEntryImpl eventToTransitionMapEntry = new EventToTransitionMapEntryImpl();
		return eventToTransitionMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimePackage getRuntimePackage() {
		return (RuntimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RuntimePackage getPackage() {
		return RuntimePackage.eINSTANCE;
	}

} //RuntimeFactoryImpl
