/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.common.util.EMap;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.RuntimeState#getObjectSystem <em>Object System</em>}</li>
 *   <li>{@link org.scenariotools.runtime.RuntimeState#getMessageEventToModalMessageEventMap <em>Message Event To Modal Message Event Map</em>}</li>
 *   <li>{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements <em>Safety Violation Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.runtime.RuntimeState#getEventToTransitionMap <em>Event To Transition Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState()
 * @model
 * @generated
 */
public interface RuntimeState extends State {
	/**
	 * Returns the value of the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System</em>' reference.
	 * @see #setObjectSystem(ObjectSystem)
	 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState_ObjectSystem()
	 * @model
	 * @generated
	 */
	ObjectSystem getObjectSystem();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.RuntimeState#getObjectSystem <em>Object System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object System</em>' reference.
	 * @see #getObjectSystem()
	 * @generated
	 */
	void setObjectSystem(ObjectSystem value);

	/**
	 * Returns the value of the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.events.MessageEvent},
	 * and the value is of type {@link org.scenariotools.runtime.ModalMessageEvent},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event To Modal Message Event Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event To Modal Message Event Map</em>' map.
	 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState_MessageEventToModalMessageEventMap()
	 * @model mapType="org.scenariotools.runtime.MessageEventToModalMessageEventMapEntry<org.scenariotools.events.MessageEvent, org.scenariotools.runtime.ModalMessageEvent>"
	 * @generated
	 */
	EMap<MessageEvent, ModalMessageEvent> getMessageEventToModalMessageEventMap();

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violation Occurred In Requirements</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violation Occurred In Requirements</em>' attribute.
	 * @see #setSafetyViolationOccurredInRequirements(boolean)
	 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState_SafetyViolationOccurredInRequirements()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurredInRequirements();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements <em>Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred In Requirements</em>' attribute.
	 * @see #isSafetyViolationOccurredInRequirements()
	 * @generated
	 */
	void setSafetyViolationOccurredInRequirements(boolean value);

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violation Occurred In Assumptions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #setSafetyViolationOccurredInAssumptions(boolean)
	 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState_SafetyViolationOccurredInAssumptions()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurredInAssumptions();

	/**
	 * Sets the value of the '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 */
	void setSafetyViolationOccurredInAssumptions(boolean value);

	/**
	 * Returns the value of the '<em><b>Event To Transition Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.events.Event},
	 * and the value is of type {@link org.scenariotools.stategraph.Transition},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event To Transition Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event To Transition Map</em>' map.
	 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeState_EventToTransitionMap()
	 * @model mapType="org.scenariotools.runtime.EventToTransitionMapEntry<org.scenariotools.events.Event, org.scenariotools.stategraph.Transition>"
	 * @generated
	 */
	EMap<Event, Transition> getEventToTransitionMap();

	String getPassedIndex();

} // RuntimeState
