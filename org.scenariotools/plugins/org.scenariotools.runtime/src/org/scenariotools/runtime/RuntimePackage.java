/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This is the base runtime model.
 * It consists of classes defining a runtime state space where we specify that each state specifies a certain configurantion of controllable and uncontrollable objects. A runtime state can also consist of other things, but this is determined by a specification or implementation model that we do not specify here in more detail.
 * 
 * Whether objects are controllable or not is determine
 * in more detail what a runtime state is made up of. This is determined only in specializations of this model. The only thing that we define here to be common to all ScenarioTools runtime models is that every state consists (among other things) there is a set of 
 * <!-- end-model-doc -->
 * @see org.scenariotools.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.runtime/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = org.scenariotools.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.RuntimeStateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.RuntimeStateImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeState()
	 * @generated
	 */
	int RUNTIME_STATE = 0;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__OUTGOING_TRANSITION = StategraphPackage.STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__INCOMING_TRANSITION = StategraphPackage.STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STATE_GRAPH = StategraphPackage.STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__LABEL = StategraphPackage.STATE__LABEL;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__OBJECT_SYSTEM = StategraphPackage.STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = StategraphPackage.STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = StategraphPackage.STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = StategraphPackage.STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = StategraphPackage.STATE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_FEATURE_COUNT = StategraphPackage.STATE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_OPERATION_COUNT = StategraphPackage.STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.RuntimeStateGraphImpl <em>State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.RuntimeStateGraphImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeStateGraph()
	 * @generated
	 */
	int RUNTIME_STATE_GRAPH = 1;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH__STATES = StategraphPackage.STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH__START_STATE = StategraphPackage.STATE_GRAPH__START_STATE;

	/**
	 * The feature id for the '<em><b>Strategy Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH__STRATEGY_KIND = StategraphPackage.STATE_GRAPH__STRATEGY_KIND;

	/**
	 * The number of structural features of the '<em>State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH_FEATURE_COUNT = StategraphPackage.STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Successor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___INIT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate All Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Event In Alphabet</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH_OPERATION_COUNT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.ObjectSystemImpl <em>Object System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.ObjectSystemImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getObjectSystem()
	 * @generated
	 */
	int OBJECT_SYSTEM = 2;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__ROOT_OBJECTS = 0;

	/**
	 * The feature id for the '<em><b>Controllable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = 1;

	/**
	 * The feature id for the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = 2;

	/**
	 * The feature id for the '<em><b>Root Objects Contained</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__ROOT_OBJECTS_CONTAINED = 3;

	/**
	 * The number of structural features of the '<em>Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Is Controllable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = 0;

	/**
	 * The operation id for the '<em>Is Environment Runtime Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___IS_ENVIRONMENT_RUNTIME_MESSAGE__RUNTIMEMESSAGE = 1;

	/**
	 * The operation id for the '<em>Is Environment Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT = 2;

	/**
	 * The operation id for the '<em>Contains EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = 3;

	/**
	 * The number of operations of the '<em>Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.ModalMessageEventImpl <em>Modal Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.ModalMessageEventImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getModalMessageEvent()
	 * @generated
	 */
	int MODAL_MESSAGE_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Represented Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Requirements Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY = 1;

	/**
	 * The feature id for the '<em><b>Assumptions Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY = 2;

	/**
	 * The number of structural features of the '<em>Modal Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODAL_MESSAGE_EVENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Modal Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODAL_MESSAGE_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.ModalityImpl <em>Modality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.ModalityImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getModality()
	 * @generated
	 */
	int MODALITY = 4;

	/**
	 * The feature id for the '<em><b>Safety Violating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODALITY__SAFETY_VIOLATING = 0;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODALITY__MANDATORY = 1;

	/**
	 * The number of structural features of the '<em>Modality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODALITY_FEATURE_COUNT = 2;


	/**
	 * The number of operations of the '<em>Modality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODALITY_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.MessageEventToModalMessageEventMapEntryImpl <em>Message Event To Modal Message Event Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.MessageEventToModalMessageEventMapEntryImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getMessageEventToModalMessageEventMapEntry()
	 * @generated
	 */
	int MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Message Event To Modal Message Event Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message Event To Modal Message Event Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.EventToTransitionMapEntryImpl <em>Event To Transition Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.EventToTransitionMapEntryImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getEventToTransitionMapEntry()
	 * @generated
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY = 6;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	EClass getRuntimeState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.runtime.RuntimeState#getObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object System</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getObjectSystem()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_ObjectSystem();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.runtime.RuntimeState#getMessageEventToModalMessageEventMap <em>Message Event To Modal Message Event Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Message Event To Modal Message Event Map</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getMessageEventToModalMessageEventMap()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_MessageEventToModalMessageEventMap();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements <em>Safety Violation Occurred In Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Requirements</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EAttribute getRuntimeState_SafetyViolationOccurredInRequirements();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Assumptions</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EAttribute getRuntimeState_SafetyViolationOccurredInAssumptions();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.runtime.RuntimeState#getEventToTransitionMap <em>Event To Transition Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Event To Transition Map</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getEventToTransitionMap()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_EventToTransitionMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.RuntimeStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Graph</em>'.
	 * @see org.scenariotools.runtime.RuntimeStateGraph
	 * @generated
	 */
	EClass getRuntimeStateGraph();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#generateSuccessor(org.scenariotools.runtime.RuntimeState, org.scenariotools.events.Event) <em>Generate Successor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Successor</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#generateSuccessor(org.scenariotools.runtime.RuntimeState, org.scenariotools.events.Event)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#init() <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#init()
	 * @generated
	 */
	EOperation getRuntimeStateGraph__Init();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#generateAllSuccessors(org.scenariotools.runtime.RuntimeState) <em>Generate All Successors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate All Successors</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#generateAllSuccessors(org.scenariotools.runtime.RuntimeState)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#isEventInAlphabet(org.scenariotools.events.Event) <em>Is Event In Alphabet</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Event In Alphabet</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#isEventInAlphabet(org.scenariotools.events.Event)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__IsEventInAlphabet__Event();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.ObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem
	 * @generated
	 */
	EClass getObjectSystem();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getRootObjects <em>Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Root Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getRootObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_RootObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getControllableObjects <em>Controllable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Controllable Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getControllableObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_ControllableObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getUncontrollableObjects <em>Uncontrollable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uncontrollable Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getUncontrollableObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_UncontrollableObjects();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.runtime.ObjectSystem#getRootObjectsContained <em>Root Objects Contained</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Objects Contained</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getRootObjectsContained()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_RootObjectsContained();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#isControllable(org.eclipse.emf.ecore.EObject) <em>Is Controllable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Controllable</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#isControllable(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getObjectSystem__IsControllable__EObject();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#isEnvironmentRuntimeMessage(org.scenariotools.events.RuntimeMessage) <em>Is Environment Runtime Message</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Environment Runtime Message</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#isEnvironmentRuntimeMessage(org.scenariotools.events.RuntimeMessage)
	 * @generated
	 */
	EOperation getObjectSystem__IsEnvironmentRuntimeMessage__RuntimeMessage();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#isEnvironmentMessageEvent(org.scenariotools.events.MessageEvent) <em>Is Environment Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Environment Message Event</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#isEnvironmentMessageEvent(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getObjectSystem__IsEnvironmentMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject) <em>Contains EObject</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains EObject</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getObjectSystem__ContainsEObject__EObject();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.ModalMessageEvent <em>Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modal Message Event</em>'.
	 * @see org.scenariotools.runtime.ModalMessageEvent
	 * @generated
	 */
	EClass getModalMessageEvent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.runtime.ModalMessageEvent#getRepresentedMessageEvent <em>Represented Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Represented Message Event</em>'.
	 * @see org.scenariotools.runtime.ModalMessageEvent#getRepresentedMessageEvent()
	 * @see #getModalMessageEvent()
	 * @generated
	 */
	EReference getModalMessageEvent_RepresentedMessageEvent();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.runtime.ModalMessageEvent#getRequirementsModality <em>Requirements Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Requirements Modality</em>'.
	 * @see org.scenariotools.runtime.ModalMessageEvent#getRequirementsModality()
	 * @see #getModalMessageEvent()
	 * @generated
	 */
	EReference getModalMessageEvent_RequirementsModality();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.runtime.ModalMessageEvent#getAssumptionsModality <em>Assumptions Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assumptions Modality</em>'.
	 * @see org.scenariotools.runtime.ModalMessageEvent#getAssumptionsModality()
	 * @see #getModalMessageEvent()
	 * @generated
	 */
	EReference getModalMessageEvent_AssumptionsModality();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.Modality <em>Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modality</em>'.
	 * @see org.scenariotools.runtime.Modality
	 * @generated
	 */
	EClass getModality();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.Modality#isSafetyViolating <em>Safety Violating</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violating</em>'.
	 * @see org.scenariotools.runtime.Modality#isSafetyViolating()
	 * @see #getModality()
	 * @generated
	 */
	EAttribute getModality_SafetyViolating();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.Modality#isMandatory <em>Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mandatory</em>'.
	 * @see org.scenariotools.runtime.Modality#isMandatory()
	 * @see #getModality()
	 * @generated
	 */
	EAttribute getModality_Mandatory();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Message Event To Modal Message Event Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event To Modal Message Event Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.runtime.ModalMessageEvent" valueContainment="true"
	 *        keyType="org.scenariotools.events.MessageEvent" keyRequired="true"
	 * @generated
	 */
	EClass getMessageEventToModalMessageEventMapEntry();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventToModalMessageEventMapEntry()
	 * @generated
	 */
	EReference getMessageEventToModalMessageEventMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventToModalMessageEventMapEntry()
	 * @generated
	 */
	EReference getMessageEventToModalMessageEventMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Event To Transition Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event To Transition Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.events.Event"
	 *        valueType="org.scenariotools.stategraph.Transition"
	 * @generated
	 */
	EClass getEventToTransitionMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getEventToTransitionMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getEventToTransitionMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.RuntimeStateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.RuntimeStateImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeState()
		 * @generated
		 */
		EClass RUNTIME_STATE = eINSTANCE.getRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Object System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__OBJECT_SYSTEM = eINSTANCE.getRuntimeState_ObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Message Event To Modal Message Event Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = eINSTANCE.getRuntimeState_MessageEventToModalMessageEventMap();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = eINSTANCE.getRuntimeState_SafetyViolationOccurredInRequirements();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = eINSTANCE.getRuntimeState_SafetyViolationOccurredInAssumptions();

		/**
		 * The meta object literal for the '<em><b>Event To Transition Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = eINSTANCE.getRuntimeState_EventToTransitionMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.RuntimeStateGraphImpl <em>State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.RuntimeStateGraphImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeStateGraph()
		 * @generated
		 */
		EClass RUNTIME_STATE_GRAPH = eINSTANCE.getRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Generate Successor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT = eINSTANCE.getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___INIT = eINSTANCE.getRuntimeStateGraph__Init();

		/**
		 * The meta object literal for the '<em><b>Generate All Successors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE = eINSTANCE.getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState();

		/**
		 * The meta object literal for the '<em><b>Is Event In Alphabet</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT = eINSTANCE.getRuntimeStateGraph__IsEventInAlphabet__Event();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.ObjectSystemImpl <em>Object System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.ObjectSystemImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getObjectSystem()
		 * @generated
		 */
		EClass OBJECT_SYSTEM = eINSTANCE.getObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__ROOT_OBJECTS = eINSTANCE.getObjectSystem_RootObjects();

		/**
		 * The meta object literal for the '<em><b>Controllable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = eINSTANCE.getObjectSystem_ControllableObjects();

		/**
		 * The meta object literal for the '<em><b>Uncontrollable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = eINSTANCE.getObjectSystem_UncontrollableObjects();

		/**
		 * The meta object literal for the '<em><b>Root Objects Contained</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__ROOT_OBJECTS_CONTAINED = eINSTANCE.getObjectSystem_RootObjectsContained();

		/**
		 * The meta object literal for the '<em><b>Is Controllable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = eINSTANCE.getObjectSystem__IsControllable__EObject();

		/**
		 * The meta object literal for the '<em><b>Is Environment Runtime Message</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___IS_ENVIRONMENT_RUNTIME_MESSAGE__RUNTIMEMESSAGE = eINSTANCE.getObjectSystem__IsEnvironmentRuntimeMessage__RuntimeMessage();

		/**
		 * The meta object literal for the '<em><b>Is Environment Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getObjectSystem__IsEnvironmentMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Contains EObject</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = eINSTANCE.getObjectSystem__ContainsEObject__EObject();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.ModalMessageEventImpl <em>Modal Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.ModalMessageEventImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getModalMessageEvent()
		 * @generated
		 */
		EClass MODAL_MESSAGE_EVENT = eINSTANCE.getModalMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Represented Message Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT = eINSTANCE.getModalMessageEvent_RepresentedMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Requirements Modality</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY = eINSTANCE.getModalMessageEvent_RequirementsModality();

		/**
		 * The meta object literal for the '<em><b>Assumptions Modality</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY = eINSTANCE.getModalMessageEvent_AssumptionsModality();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.ModalityImpl <em>Modality</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.ModalityImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getModality()
		 * @generated
		 */
		EClass MODALITY = eINSTANCE.getModality();

		/**
		 * The meta object literal for the '<em><b>Safety Violating</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODALITY__SAFETY_VIOLATING = eINSTANCE.getModality_SafetyViolating();

		/**
		 * The meta object literal for the '<em><b>Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODALITY__MANDATORY = eINSTANCE.getModality_Mandatory();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.MessageEventToModalMessageEventMapEntryImpl <em>Message Event To Modal Message Event Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.MessageEventToModalMessageEventMapEntryImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getMessageEventToModalMessageEventMapEntry()
		 * @generated
		 */
		EClass MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY = eINSTANCE.getMessageEventToModalMessageEventMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__VALUE = eINSTANCE.getMessageEventToModalMessageEventMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP_ENTRY__KEY = eINSTANCE.getMessageEventToModalMessageEventMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.EventToTransitionMapEntryImpl <em>Event To Transition Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.EventToTransitionMapEntryImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getEventToTransitionMapEntry()
		 * @generated
		 */
		EClass EVENT_TO_TRANSITION_MAP_ENTRY = eINSTANCE.getEventToTransitionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_TRANSITION_MAP_ENTRY__KEY = eINSTANCE.getEventToTransitionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = eINSTANCE.getEventToTransitionMapEntry_Value();

	}

} //RuntimePackage
