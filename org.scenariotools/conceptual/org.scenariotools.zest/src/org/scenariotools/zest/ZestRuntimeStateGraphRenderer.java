package org.scenariotools.zest;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class ZestRuntimeStateGraphRenderer {
	private RuntimeStateGraph graphModel;
	private Graph graph;
	private Composite parent;

	public ZestRuntimeStateGraphRenderer(Composite parent) {
		this.parent = parent;
	}

	public RuntimeStateGraph getGraphModel() {
		return graphModel;
	}

	public void setGraphModel(RuntimeStateGraph graphModel) {
		this.graphModel = graphModel;
	}

	public void render() {
		if (graph != null) {
			graph.dispose();
		}
		graph = new Graph(parent, SWT.NONE);
		SpringLayoutAlgorithm layoutAlg=new SpringLayoutAlgorithm(
				ZestStyles.NONE);
		layoutAlg.setIterations(5000);
		layoutAlg.setSpringGravitation(20);
		layoutAlg.setSpringMove(0.1);
		layoutAlg.setSpringStrain(10);
		layoutAlg.setSpringLength(0.5);
		graph.setLayoutAlgorithm(layoutAlg, false);
		Map<State, GraphNode> modelStateToGraphNodeMap = new HashMap<State, GraphNode>();
		int num=0;
		for (State state : graphModel.getStates()) {
			GraphNode newNode = new GraphNode(graph, SWT.NONE);
			if (state == graphModel.getStartState())
				newNode.setBorderWidth(4);
			modelStateToGraphNodeMap.put(state, newNode);
			newNode.setText(String.valueOf(num++));
		}

		for (State sourceState : graphModel.getStates()) {
			for (Transition outTransition : sourceState.getOutgoingTransition()) {
				if (outTransition.getTargetState() != null) {
					State targetState = outTransition.getTargetState();
					GraphConnection newConnection = new GraphConnection(graph,
							SWT.NONE,
							modelStateToGraphNodeMap.get(sourceState),
							modelStateToGraphNodeMap.get(targetState));
					newConnection
							.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
					
					if (sourceState == targetState)
						newConnection.setCurveDepth(40);
					else
						newConnection.setCurveDepth((sourceState.getOutgoingTransition().size()-1)*10);
					
					newConnection.setText(outTransition.getEvent().toString());
					newConnection.setWeight(0.5);
				}
			}
		}
		parent.layout();
		graph.applyLayout();
		parent.redraw();
	}
}
