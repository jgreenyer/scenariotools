package org.scenariotools.zest;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.runtime.RuntimeStateGraph;

public class ZestGraphView extends ViewPart {
	private ZestRuntimeStateGraphRenderer renderer;

	public RuntimeStateGraph getGraphModel() {
		return renderer.getGraphModel();
	}

	public void setGraphModel(RuntimeStateGraph graphModel) {
		renderer.setGraphModel(graphModel);
	}

	@Override
	public void createPartControl(Composite parent) {
		renderer=new ZestRuntimeStateGraphRenderer(parent);
	}
	
	public void render(){
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				renderer.render();
			}
			
		});
	}

	@Override
	public void setFocus() {
	}

	public static ZestGraphView getDefaultView(){
		try {
			return (ZestGraphView) PlatformUI
					.getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.showView("org.scenariotools.zest.ZestGraphView");
		} catch (PartInitException e) {
			e.printStackTrace();
			return null;
		}
	}
}
