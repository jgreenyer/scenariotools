package org.scenariotools.zest;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.runtime.RuntimeStateGraph;

public class ZestVisualizationAction implements IObjectActionDelegate {
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		IFile file = (IFile) structuredSelection.getFirstElement();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource graphResource = resourceSet.getResource(
				URI.createPlatformResourceURI(file.getFullPath().toString(),
						true), true);

		RuntimeStateGraph graph = (RuntimeStateGraph) graphResource
				.getContents().get(0);

		ZestGraphView graphView = ZestGraphView.getDefaultView();
		graphView.setGraphModel(graph);
		graphView.render();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

}
