Steps to rebuild the update site after building the plug-ins:
* If there are new feature projects: add them in the category.xml (in this folder).
* Delete all .jar-files in the "updatesite" folder (in this project) and its subfolders.
* In eclipse, select File -> Export... and then select in the category "Plug-in Development" the entry "Deployable features". 
	Click "Next" to start the corresponding export wizard.
* In the export wizard, select all "org.scenariotools.*"-features.
* In the "Destination" tab, select the "updatesite" folder (in this project) as path for "Directory".
	(only necessary on the first export)
* In the "Options" tab, check "Categorize repository" and select (via "Browse") the "category.xml" in this project.
	(only necessary on the first export)
* Click "Finish" and wait for the export to complete.
* In the "updatesite" folder, open or unzip the file "content.jar" (using, e.g., 7Zip in windows) and open the contained file "content.xml" for editing.
	(this might not be possible from within the eclipse UI, so you might need to use a file manager like Windows-Explorer)
* In "content.xml", change the "name" attribute of the "repository" (root-)tag to "ScenarioTools".
* Search for the "references"-tag (child of "repository" tag). For each of its "repository"-children, set the attribute "options" to 1.
	(Eclipse will add these entries as update sites, they are disabled for options='0', enabled for options='1')
* Update the "content.jar" to contain the new version of the "content.xml". The update site is now complete.
* If using SVN via eclipse, make sure to refresh the project before attempting to commit the regenerated repository.
* When committing the changes, select the .jar-files of the old repository for deletion.