package org.scenariotools.dynupdating.synthesis.otfb;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.synthesis.otfb.incremental.OTFSynthesisIncremental;

public class OTFDynUpdatingSynthesisIncremental extends OTFSynthesisIncremental {

	// private Map<State, EList<State>> candidateMap = new HashMap<State,
	// EList<State>>();
	private State startStateofBaseControllerCopy;
	private HistoryRelationAlgorithm historyRelationAlgorithm;
	private Map<State, State> baseControllerStateToStateCopyMap;

	@Override
	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithmIncrementalForDyamicUpdates();
	}

	public OTFBAlgorithmIncrementalForDyamicUpdates getAlgorithmIncrementalForDyamicUpdates() {
		return (OTFBAlgorithmIncrementalForDyamicUpdates) getOTFBAlgorithm();
	}
	
	public void setHistoryRelationAlgorithm(
			HistoryRelationAlgorithm historyRelationAlgorithm) {
		this.historyRelationAlgorithm = historyRelationAlgorithm;
	}

	@Override
	protected void extractController() {
		super.extractController();
		
		copyBaseControllerIntoNewController();
		initializeHistoryRelationAlgorithmDataStructures();		
		historyRelationAlgorithm.computeHistoryRelation();	
		drawCandidateMapTransitions();
		createUpdateTransitions();
	}

	private void copyBaseControllerIntoNewController() {		
		// adding labels to states of new controller
		int count = 0;		
		for (State newControllerState : getNewController().getStates()) {
			addVisualizationLabels(newControllerState, "newController", "new_"+count++);
		}		
		
		baseControllerStateToStateCopyMap = new HashMap<State, State>();
		// copy all the states of the base controller into the new controller
		for (State baseControllerState : getAlgorithmIncrementalForDyamicUpdates().getBaseController().getStates()) {
			RuntimeState baseControllerStateCopy = RuntimeFactory.eINSTANCE.createRuntimeState();
			addVisualizationLabels(baseControllerStateCopy, "oldController", "old_"+count++);						
			baseControllerStateCopy.setObjectSystem(getAlgorithmIncrementalForDyamicUpdates().getStartState().getObjectSystem());
			getNewController().getStates().add(baseControllerStateCopy);
			// add a mapping between states of the base controller and its copy
			baseControllerStateToStateCopyMap.put(baseControllerState,baseControllerStateCopy);			
		}
		
		// set start state of base controller copy
		startStateofBaseControllerCopy = baseControllerStateToStateCopyMap.get(getAlgorithmIncrementalForDyamicUpdates().getBaseController().getStartState());
		
		// copy all the transitions of base controller into the base controller copy
		for (Map.Entry<State, State> baseControllerStateToStateCopyMapEntry : baseControllerStateToStateCopyMap
				.entrySet()) {
			State baseControllerState = baseControllerStateToStateCopyMapEntry
					.getKey();
			State baseControllerStateCopy = baseControllerStateToStateCopyMapEntry
					.getValue();
			for (Transition baseControllerTransition : baseControllerState
					.getOutgoingTransition()) {
				Transition baseControllerTransitionCopy = createTransition(
						baseControllerStateCopy,
						baseControllerStateToStateCopyMap
								.get(baseControllerTransition.getTargetState()),
						null, null);
				baseControllerTransitionCopy.setEvent(baseControllerTransition
						.getEvent());
			}
		}
	}

	private void addVisualizationLabels(State s, String visCluster, String visLabel) {
		s.getStringToStringAnnotationMap().put("visCluster", visCluster);
		s.getStringToStringAnnotationMap().put("visLabel", visLabel);		
	}
		
	private void initializeHistoryRelationAlgorithmDataStructures() {
		
		historyRelationAlgorithm.setCandidateMap(initializeCandidateMap());
		historyRelationAlgorithm
				.setCurrentControllerMessageEventToNewControllerMessageEventMap(initializeCurrentToNewMessageEventMap());
		historyRelationAlgorithm
				.setStartStateofCurrentController(startStateofBaseControllerCopy);
		historyRelationAlgorithm.setSpecificationStateGraph(getSpecificationStateGraph());
	}

	private Map<State,EList<State>> initializeCandidateMap() {
		// the candidate Map contains a mapping between states of the base controller copy to the list of states of new controller
		Map<State, EList<State>> candidateMap = new HashMap<State, EList<State>>();
		for (Entry<State, EList<State>> baseControllerStateToNewControllerStatesHRCandidatesMapEntry : getAlgorithmIncrementalForDyamicUpdates()
				.getBaseControllerStateToNewControllerStatesHRCandidatesMap()
				.entrySet()) {
			State stateCopyOfBaseController = baseControllerStateToStateCopyMap
					.get(baseControllerStateToNewControllerStatesHRCandidatesMapEntry
							.getKey());
			EList<State> stateOfNewControllerList = new BasicEList<State>();
			for (State specificationState : baseControllerStateToNewControllerStatesHRCandidatesMapEntry
					.getValue()) {
				State stateOfNewController = getSpecStateToControllerStateMap().get(specificationState);
				if (stateOfNewController != null)
					stateOfNewControllerList.add(stateOfNewController);
			}
			
			candidateMap.put(stateCopyOfBaseController,
					stateOfNewControllerList);
		}
		return candidateMap;
	}
	
	private Map<MessageEvent, MessageEvent> initializeCurrentToNewMessageEventMap() {
		// create the "inverse" of the newMessageEventToBaseControllerMessageEventMap
		Map<MessageEvent, MessageEvent> baseControllerMessageEventToNewControllerMessageEventMap = new HashMap<MessageEvent, MessageEvent>();		
		for (Map.Entry<MessageEvent, MessageEvent> newMessageEventToBaseControllerMessageEventMapEntry : getAlgorithmIncrementalForDyamicUpdates()
				.getNewMessageEventToBaseControllerMessageEventMap().entrySet()) {
			baseControllerMessageEventToNewControllerMessageEventMap.put(
					newMessageEventToBaseControllerMessageEventMapEntry
							.getValue(),
					newMessageEventToBaseControllerMessageEventMapEntry
							.getKey());
		}		
		return baseControllerMessageEventToNewControllerMessageEventMap;
	}
	
	private RuntimeStateGraph getSpecificationStateGraph() {
		return (RuntimeStateGraph)getAlgorithmIncrementalForDyamicUpdates().getStartState().getStateGraph();
	}
	
	private void drawCandidateMapTransitions() {
		for (Entry<State, EList<State>> candidateMapEntry : historyRelationAlgorithm.getCandidateMap().entrySet()) {
			for (State newControllerState : candidateMapEntry.getValue()) {
				createTransition(candidateMapEntry.getKey(), newControllerState, "candidate", "cyan");
			}
		}
	}

	private void createUpdateTransitions() {		
		for (Map.Entry<State, EList<State>> candidateMapEntry : historyRelationAlgorithm.getCandidateMap().entrySet()) {
			
			if (candidateMapEntry.getValue().size() == 1) {
				// there is a one to one mapping -> create the update transition
				State currentControllerStateCopy = candidateMapEntry.getKey();
				State newControllerState = candidateMapEntry.getValue().iterator().next();
				createTransition(currentControllerStateCopy,newControllerState, "update", "green");
				// remove all outgoing transitions of the updatable states
				// removeOutgoingTransition(currentControllerStateCopy);
			} else {
				// distinguish if it is a don't know case
				// Take the state of the current controller
				State q = candidateMapEntry.getKey();
				// Take the first mapped state of the new controller
				State sPrime = candidateMapEntry.getValue().get(0);
				// Take the second mapped state of the new controller
				State qPrime = candidateMapEntry.getValue().get(1);
				// Identify the transition that links sPrime to qPrime
				for (Transition outgoingTransitionOfSPrime : sPrime.getOutgoingTransition()) {
					if (outgoingTransitionOfSPrime.getTargetState().equals(qPrime)) {
						if (getAlgorithmIncrementalForDyamicUpdates().baseControllerAlphabetContainsEqualMessageEvent(outgoingTransitionOfSPrime.getEvent())) {
							// it is not a "don't know" case
							removeOutgoingTransition(q);
							// create update transition for the first mapping						
							createTransition(q, sPrime, "update", "green");
							// no need to check other outgoing transitions
							break;
						}
					}
				}
			}
		}
	}

	private void removeOutgoingTransition(State state) {
		state.getOutgoingTransition().clear();
	}

	private Transition createTransition(State sourceState, State targetState,
			String transitionLabel, String transitionColor) {
		Transition transition = StategraphFactory.eINSTANCE.createTransition();
		transition.setSourceState(sourceState);
		transition.setTargetState(targetState);
		if (transitionLabel != null)
			transition.getStringToStringAnnotationMap().put("transitionLabel",
					transitionLabel);
		if (transitionColor != null)
			transition.getStringToStringAnnotationMap().put("transitionColor",
					transitionColor);
		return transition;
	}

	

}
