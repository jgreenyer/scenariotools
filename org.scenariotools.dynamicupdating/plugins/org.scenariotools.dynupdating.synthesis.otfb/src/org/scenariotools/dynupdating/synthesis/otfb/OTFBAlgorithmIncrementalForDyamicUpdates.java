package org.scenariotools.dynupdating.synthesis.otfb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.synthesis.otfb.incremental.OTFBAlgorithmIncremental;

public class OTFBAlgorithmIncrementalForDyamicUpdates extends
		OTFBAlgorithmIncremental {

	private Map<State, EList<State>> newStateToBaseControllerStatesHRCandidatesMap;
	private Map<State, EList<State>> baseControllerStateToNewControllerStatesHRCandidatesMap;

	public Map<State, EList<State>> getNewStateToBaseControllerStatesHRCandidatesMap() {
		return newStateToBaseControllerStatesHRCandidatesMap;
	}

	public Map<State, EList<State>> getBaseControllerStateToNewControllerStatesHRCandidatesMap() {
		return baseControllerStateToNewControllerStatesHRCandidatesMap;
	}

	@Override
	public boolean OTFB(RuntimeState startState) {
		newStateToBaseControllerStatesHRCandidatesMap = new HashMap<State, EList<State>>();
		baseControllerStateToNewControllerStatesHRCandidatesMap = new HashMap<State, EList<State>>();

		addCorrespondingHRCandidateState(startState, getBaseController()
				.getStartState());

		return super.OTFB(startState);
	}

	/**
	 * This method first delegates to the super class method which will create a
	 * new transition $t' = <s', m, q'>$ in the new state graph, $s'$ is the
	 * source state, $q'$ is the target state, $m$ is the message event.
	 * 
	 * The superclass method will also maintain a correspondence of states which
	 * is the basis for the incremental synthesis.
	 * 
	 * Here, in addition, we will maintain a correspondence of HISTORY RELATION
	 * CANDIDATES between states in the state graph and base controller states.
	 * This follows similar rules as for the non-history-relation candidates, as
	 * follows: If the source state $s'$ has a corresponding state $s$ in the
	 * base controller, it tries to find a state $q$ in the base controller that
	 * corresponds to the target state $q'$ of the newly explored transition.
	 * 
	 * (1) The state $q'$ corresponds to a state $q$ if there exists a
	 * transition $t = <s, m, q>$ in the base controller
	 * 
	 * (Here comes the difference:) (2) The state $q'$ corresponds to $s$ if $m$
	 * is an ENVIRONMENT event that does not belong to the alphabet of the base
	 * controller.
	 * 
	 * (3) The state $s'$ corresponds to a state $q$ if there exists a
	 * transition $t = <s, m, q>$ and $m$ is an ENVIRONMENT event that does not
	 * belong to the new controller.
	 * 
	 * case (1) and (2) are handled in this method, since they depend on the
	 * transition $t'$ that is generated here. case (3) instead depends on
	 * already existing transitions in the base controller. Therefore, this case
	 * is handled in the method addCorrespondingState().
	 * 
	 */
	@Override
	protected Transition generateSuccessor(Transition t) {
		t = super.generateSuccessor(t);

		// target state is $q'$
		RuntimeState targetState = (RuntimeState) t.getTargetState();

		if (targetState.equals(targetState.getStateGraph().getStartState()))
			return t;

		if (newStateToBaseControllerStatesHRCandidatesMap.get(t
				.getSourceState()) != null) {

			// correspondingBaseControllerState is $s$
			List<State> correspondingBaseControllerStates = new BasicEList<State>(
					newStateToBaseControllerStatesHRCandidatesMap.get(t
							.getSourceState()));
			// check if it's case (1) or case (2)
			if (baseControllerAlphabetContainsEqualMessageEvent(t.getEvent())) {
				// (1) we find corresponding transitions in the base controller
				// (a transition with the same event)
				for (State correspondingBaseControllerState : correspondingBaseControllerStates) {
					for (Transition baseControllerStateOutgoingTransition : correspondingBaseControllerState
							.getOutgoingTransition()) {
						if (eventsEqual(
								baseControllerStateOutgoingTransition
										.getEvent(),
								t.getEvent())) { // a transition with the same
													// event is found
							if (!baseControllerStateOutgoingTransition
									.getTargetState().equals(getStartState())) {
								addCorrespondingHRCandidateState(targetState,
										baseControllerStateOutgoingTransition
												.getTargetState());
								// System.out.println("  adding corresponding state: "
								// + targetState + " corresponds to " +
								// baseControllerStateOutgoingTransition.getTargetState());
							}
						}
					}
				}
			} else {
				// case (2) if the transition is labeled with an ENVIRONMENT
				// event that is not in the alphabet of the
				// base controller, the target state will correspond to the same
				// state as the source state.
				if (!getStartState().getObjectSystem().isControllable(
						((MessageEvent) t.getEvent()).getSendingObject())) {
					for (State correspondingBaseControllerState : correspondingBaseControllerStates) {
						if (!correspondingBaseControllerState
								.equals(getStartState())) {
							addCorrespondingHRCandidateState(targetState,
									correspondingBaseControllerState);
						}
					}
				}
			}

		}

		return t;
	}

	private void addCorrespondingHRCandidateState(State newState,
			State baseControllerState) {

		boolean newCorrespondenceWasAdded = false;

		EList<State> baseControllerStatesCorrespondingToNewState = newStateToBaseControllerStatesHRCandidatesMap
				.get(newState);
		if (baseControllerStatesCorrespondingToNewState == null) {
			baseControllerStatesCorrespondingToNewState = new BasicEList<State>();
			newStateToBaseControllerStatesHRCandidatesMap.put(newState,
					baseControllerStatesCorrespondingToNewState);
		}
		if (!baseControllerStatesCorrespondingToNewState
				.contains(baseControllerState))
			if (baseControllerStatesCorrespondingToNewState
					.add(baseControllerState))
				newCorrespondenceWasAdded = true;

		EList<State> newStatesCorrespondingToBaseControllerState = baseControllerStateToNewControllerStatesHRCandidatesMap
				.get(baseControllerState);
		if (newStatesCorrespondingToBaseControllerState == null) {
			newStatesCorrespondingToBaseControllerState = new BasicEList<State>();
			baseControllerStateToNewControllerStatesHRCandidatesMap.put(
					baseControllerState,
					newStatesCorrespondingToBaseControllerState);
		}
		if (!newStatesCorrespondingToBaseControllerState.contains(newState))
			if (newStatesCorrespondingToBaseControllerState.add(newState))
				newCorrespondenceWasAdded = true;

		if (!newCorrespondenceWasAdded)
			return;

		// case (3), see API doc of generateSuccessor:
		// if a correspondence is added between base controller state $q$ and
		// new controller state $q'$, and $q$
		// has a transition to a state $q2$ labeled with an event that is not in
		// the alphabet of the new controller,
		// then all states in the new controller that correspond to $q$ also
		// belong to $q2$.
		//
		// We thus iterate over all outgoing transitions of $q$ (the
		// baseControllerState) and, if the event is an ENVIRONMENT event which
		// does not belong to the new controller's alphabet, call
		// addCorrespondingState($q2$, $q''$)
		// for each $q''$ in the new controller that corresponds to $q$
		for (Transition transition : baseControllerState
				.getOutgoingTransition()) {
			boolean isEventInAlphabet = ((RuntimeStateGraph) newState
					.getStateGraph()).isEventInAlphabet(transition.getEvent());
			System.out.println(" the event "
					+ ((MessageEvent) transition.getEvent()).getOperation()
							.getName()
					+ " is in the Alphabet of the new controller? "
					+ isEventInAlphabet);
			if ((!((RuntimeStateGraph) newState.getStateGraph())
					.isEventInAlphabet(transition.getEvent()))
					&& !newStateToBaseControllerStatesHRCandidatesMap.get(
							newState).contains(transition.getTargetState())
					&& (!transition.getTargetState()
							.equals(baseControllerState.getStateGraph()
									.getStartState()))
					&& !((RuntimeState) baseControllerState).getObjectSystem()
							.isControllable(
									((MessageEvent) transition.getEvent())
											.getSendingObject())) {
				addCorrespondingHRCandidateState(newState,
						transition.getTargetState());
			}

		}

	}

}
