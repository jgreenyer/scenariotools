package org.scenariotools.dynupdating.synthesis.otfb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public abstract class HistoryRelationAlgorithm {
	protected State startStateofCurrentController;
	protected State startStateOfNewController;
	protected Map<State,EList<State>> candidateMap;
	protected Map <MessageEvent,MessageEvent> currentControllerMessageEventToNewControllerMessageEventMap;
	private RuntimeStateGraph specificationStateGraph;
	
	public void computeHistoryRelation(){
		boolean newCandidateRemoved=true;
		while(newCandidateRemoved){
			newCandidateRemoved=false;
			for (Iterator<Entry<State, EList<State>>> candidateMapEntryIterator =candidateMap.entrySet().iterator(); candidateMapEntryIterator.hasNext();  ) {
				Entry<State, EList<State>> candidateMapEntry =candidateMapEntryIterator.next();
				State q = candidateMapEntry.getKey();
				if (!q.equals(startStateofCurrentController)) { // never remove entry with start state of the base controller.					
					for (Iterator<State> qPrimeListIterator = candidateMapEntry.getValue().iterator(); qPrimeListIterator.hasNext(); ) {
						State qPrime =qPrimeListIterator.next();
						if (qPrime == null) {
							continue;
						}
						// get all predecessors of qPrime with the corresponding transition's message event
						Map <MessageEvent,State> messageEventToIncomingSourceStatesOfQPrimeMap=getIncomingMessageEventToPredecessorMap(qPrime);
						// iterate over all incoming transitions of q
						for(Transition t : q.getIncomingTransition()) {
							State s= t.getSourceState();
							MessageEvent messageEventOfT=(MessageEvent)t.getEvent();						
							if(isPredecessorToConsider(q, s)){								
								// get message event of corresponding transition leading to qPrime, will be null if such a transition does not exist 
								MessageEvent messageEventOfTPrime=currentControllerMessageEventToNewControllerMessageEventMap.get(messageEventOfT);
								State sPrime = null;
								if (!isEventInTheAlphabetOfNewController(messageEventOfT)) {
									sPrime = qPrime;
								}
								else {
									if (messageEventOfTPrime != null) {								
										sPrime = messageEventToIncomingSourceStatesOfQPrimeMap.get(messageEventOfTPrime);
									}
								}
		
								if (candidateMap.get(s) == null // s is not in candidate.
										// for the incoming transition t of the current controller there is no corresponding message event in the new controller
										|| !candidateMap.get(s).contains(sPrime)) {
									// remove the candidate pair <q,qPrime>
									qPrimeListIterator.remove();							
									newCandidateRemoved=true;
									break; // do not consider further incoming transitions
								}
							}
							
						}
					}
					if (candidateMapEntry.getValue().isEmpty()) {
						// we remove all the candidate pairs <q,qPrime> for state q
						// we can remove the candidate map entry of q
						candidateMapEntryIterator.remove();
					}
				}			
			}
		}
	}
	
	public State getStartStateofCurrentController() {
		return startStateofCurrentController;
	}
	public void setStartStateofCurrentController(State startStateofCurrentController) {
		this.startStateofCurrentController = startStateofCurrentController;
	}
	public State getStartStateOfNewController() {
		return startStateOfNewController;
	}
	public void setStartStateOfNewController(State startStateOfNewController) {
		this.startStateOfNewController = startStateOfNewController;
	}
	
	public Map<State, EList<State>> getCandidateMap() {
		return candidateMap;
	}
	public void setCandidateMap(Map<State, EList<State>> candidateMap) {
		this.candidateMap = candidateMap;
	}
	public Map<MessageEvent, MessageEvent> getCurrentControllerMessageEventToNewControllerMessageEventMap() {
		return currentControllerMessageEventToNewControllerMessageEventMap;
	}
	public void setCurrentControllerMessageEventToNewControllerMessageEventMap(
			Map<MessageEvent, MessageEvent> currentControllerMessageEventToNewControllerMessageEventMap) {
		this.currentControllerMessageEventToNewControllerMessageEventMap = currentControllerMessageEventToNewControllerMessageEventMap;
	}
	public void setSpecificationStateGraph(RuntimeStateGraph specificationStateGraph) {
		this.specificationStateGraph = specificationStateGraph;
		
	}
	
	protected Map<MessageEvent,State> getIncomingMessageEventToPredecessorMap (State s){
		Map<MessageEvent,State> resultingMap=new HashMap<MessageEvent,State>();		
		for (Transition t:s.getIncomingTransition()){			
			MessageEvent e=(MessageEvent)t.getEvent();
			resultingMap.put(e,t.getSourceState());
		}		
		return resultingMap;
	}
	
	protected abstract boolean isPredecessorToConsider(State state, State predecessor);

	protected boolean isEventInTheAlphabetOfNewController(Event event){
		return specificationStateGraph.isEventInAlphabet(event);
	}
	

}
