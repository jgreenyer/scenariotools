package org.scenariotools.dynupdating.synthesis.otfb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class CycleFreeHistoryRelationAlgorithm extends HistoryRelationAlgorithm {
	private HashMap<State, Set<State>> stateToNecessaryPredecessorsMap = new HashMap<State, Set<State>>();
	public HashMap<State, Set<State>> getStateToNecessaryPredecessorsMap() {
		return stateToNecessaryPredecessorsMap;
	}

	public void computeHistoryRelation(){
		
		computeNecesaryPredecessorsMap(startStateofCurrentController, null);
		super.computeHistoryRelation();
	}

	
	public void computeNecesaryPredecessorsMap(State current, State predecessor){
		boolean toStop=true;
		if(!stateToNecessaryPredecessorsMap.containsKey(current)){
			// visiting the current state for the first time
			//predecessors for current not yet defined
			Set<State> necessaryPredecessorSet=new HashSet<State>();			
			if (predecessor!=null){
				//current is not the initial state				
				necessaryPredecessorSet.add(predecessor);	
				necessaryPredecessorSet.addAll(stateToNecessaryPredecessorsMap.get(predecessor));
			}
			//you have to visit for the first time your successors
			toStop=false;
			stateToNecessaryPredecessorsMap.put(current,necessaryPredecessorSet);
			
			printMap(current, stateToNecessaryPredecessorsMap.get(current));
		}
		else{// the current state has been already visited once		
			//create predecessorSet as the union of predecessor's predecessors with itself
			Set<State> predecessorsOfPrecedessor=new HashSet<State>(stateToNecessaryPredecessorsMap.get(predecessor));
			predecessorsOfPrecedessor.add(predecessor);
			System.out.println("Before eventual removal");
			printMap(current, stateToNecessaryPredecessorsMap.get(current));
			
			//if the intersection between Pred(current) and Pred(qpre) is equal to Pred(current)
			if(predecessorsOfPrecedessor.containsAll(stateToNecessaryPredecessorsMap.get(current))){
				toStop=true;
			}
			else{
				//we have to update the Pred(current) set with the intersection between Pred(current) and Pred(qpre)
				stateToNecessaryPredecessorsMap.get(current).retainAll(predecessorsOfPrecedessor);
				//continue the exploration
				toStop=false;
			}
			System.out.println("After eventual removal");
			printMap(current, stateToNecessaryPredecessorsMap.get(current));
		}
		if (!toStop){
			for(Transition outgoingTransition: current.getOutgoingTransition()){
				State successor=outgoingTransition.getTargetState();
				computeNecesaryPredecessorsMap(successor, current);
			}
		}
	}
	
	@Override
	protected boolean isPredecessorToConsider(State state, State predecessor) {		
		return !stateToNecessaryPredecessorsMap.get(predecessor).contains(state);
	}
	
	
	private void printMap(State state, Set<State> set){
		String setLabel="[";
		for (State s: set){
			String label=s.getStringToStringAnnotationMap().get("visLabel");
			if(label==null){
				// we are testing the method without computing the synthesis
				label=s.getStringToStringAnnotationMap().get("label");
				}
			
			setLabel+=label+ " ";
		}
		setLabel+="]";
		String label=state.getStringToStringAnnotationMap().get("visLabel");
		if(label==null){
			// we are testing the method without computing the synthesis
			label=state.getStringToStringAnnotationMap().get("label");
		}
		System.out.println(label +": "+setLabel);
	}
	public static void main(String[] args){
		StateGraph graph=StategraphFactory.eINSTANCE.createStateGraph();
		State s0=StategraphFactory.eINSTANCE.createState();
		s0.getStringToStringAnnotationMap().put("label", "s0");
		graph.getStates().add(s0);
		graph.setStartState(s0);
		State s1=StategraphFactory.eINSTANCE.createState();
		s1.getStringToStringAnnotationMap().put("label", "s1");
		graph.getStates().add(s1);
		State s2=StategraphFactory.eINSTANCE.createState();
		s2.getStringToStringAnnotationMap().put("label", "s2");
		graph.getStates().add(s2);
		State s3=StategraphFactory.eINSTANCE.createState();
		s3.getStringToStringAnnotationMap().put("label", "s3");
		graph.getStates().add(s3);
		Transition a=StategraphFactory.eINSTANCE.createTransition();
		s0.getOutgoingTransition().add(a);
		s1.getIncomingTransition().add(a);
		Transition b=StategraphFactory.eINSTANCE.createTransition();
		s0.getOutgoingTransition().add(b);
		s2.getIncomingTransition().add(b);
		Transition c=StategraphFactory.eINSTANCE.createTransition();
		s1.getOutgoingTransition().add(c);
		s2.getIncomingTransition().add(c);
		Transition d=StategraphFactory.eINSTANCE.createTransition();
		s2.getOutgoingTransition().add(d);
		s1.getIncomingTransition().add(d);
		Transition e=StategraphFactory.eINSTANCE.createTransition();
		s2.getOutgoingTransition().add(e);
		s3.getIncomingTransition().add(e);
		CycleFreeHistoryRelationAlgorithm hr=new CycleFreeHistoryRelationAlgorithm();
		
		hr.computeNecesaryPredecessorsMap(s0, null);
		
		System.out.println("###FINAL RESULT###");
		for(State s: hr.getStateToNecessaryPredecessorsMap().keySet()){
			hr.printMap(s, hr.getStateToNecessaryPredecessorsMap().get(s));
		}
	}


}
