package org.scenariotools.dynupdating.globalcontroller.ui.popup.actions;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.dynupdating.globalcontroller.DynamicUpdatingController;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;

public class BuildDynUpdatingGlobalControllerAction implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public BuildDynUpdatingGlobalControllerAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		List<?> selectionList = structuredSelection.toList();
		
		final IFile stateGraphCurrentVersionFile;
		final IFile stateGraphNewVersionFile ;
		
		if ( !("msdruntime".equals(((IFile)selectionList.get(0)).getFileExtension() )) || !("msdruntime".equals(((IFile)selectionList.get(1)).getFileExtension())) ) {
			MessageDialog.openError(new Shell(), "Invalid selection", "Both model files must have msdruntime extension");;
			return;
		}
		
		if (((IFile)selectionList.get(0)).getParent().getName().contains("v1")){
			stateGraphCurrentVersionFile=(IFile)selectionList.get(0);
			stateGraphNewVersionFile=(IFile)selectionList.get(1);					
		}
		
		else if (((IFile)selectionList.get(0)).getParent().getName().contains("v2")){
			stateGraphNewVersionFile=(IFile)selectionList.get(0);
			stateGraphCurrentVersionFile=(IFile)selectionList.get(1);
								
		}
		else {
			MessageDialog.openError(new Shell(), "Invalid selection", "Please mark a model file of the current version of the controller contained in the folder *_v1 and a model of the new version contained in a folder *_v2");;
			return;
		}

		final ResourceSet resourceSet =new ResourceSetImpl();
		final Resource currentVersionResource= resourceSet.getResource(URI.createPlatformResourceURI(stateGraphCurrentVersionFile.getFullPath().toString(), true), true);
		final Resource newVersionResource= resourceSet.getResource(URI.createPlatformResourceURI(stateGraphNewVersionFile.getFullPath().toString(), true), true);
		
		final IFile dynControllerFile=stateGraphCurrentVersionFile;

		final Job job = new Job("Build Dynamic Updating Global Controller.") {

			protected IStatus run(IProgressMonitor monitor) {

				
				RuntimeStateGraph currentController=(RuntimeStateGraph)currentVersionResource.getContents().get(0);
				RuntimeStateGraph newController=(RuntimeStateGraph)newVersionResource.getContents().get(0);		
				
				URI dynamicControllerFileURI = URI
						.createPlatformResourceURI(	dynControllerFile.getFullPath().removeLastSegments(3).toString(), true).
						appendSegment("DynamicGlobalController").appendSegment("dynamicGlobalController").appendFileExtension("msdruntime");
				
				Resource dynamicControllerResource=null;
				try {
					dynamicControllerResource = resourceSet.getResource(dynamicControllerFileURI, true);
					if (dynamicControllerFileURI != null){
						MessageDialog.openError(new Shell(), "An error occurred while creating the interpreter configuration", 
								"There already exists a dynamically updating controller file at the default location:\n" 
								+ dynamicControllerFileURI + "\n\n"
								+ "Remove it first to create a new one.");
					}
					
					return Status.CANCEL_STATUS;
				} catch (Exception e) {
					// that's good. Continue.
				}
				
				dynamicControllerResource=resourceSet.createResource(dynamicControllerFileURI);
				DynamicUpdatingController dynamicUpdatingController = new DynamicUpdatingController();				
				dynamicUpdatingController.setDynamicControllerResource(dynamicControllerResource);
				dynamicUpdatingController.generateDynamicController(currentController,newController);


				postBuildFinished();
				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
	    job.schedule();
	}

	public static void postBuildFinished(){
		Display.getDefault().asyncExec (new Runnable() {
			
			public void run() {				
				
				MessageDialog.openInformation(
						new Shell(),
						"Ui",
						"Build Dynamic Updating Global Controller was executed.");
			}
		});
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {		
	}

}
