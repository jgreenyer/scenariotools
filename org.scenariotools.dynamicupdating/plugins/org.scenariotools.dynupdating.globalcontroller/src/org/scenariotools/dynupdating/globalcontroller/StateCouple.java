package org.scenariotools.dynupdating.globalcontroller;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

public class StateCouple {
	private RuntimeState currentControllerState;
	private RuntimeState newControllerState;
	
	public StateCouple(RuntimeState currentControllerState,RuntimeState newControllerState){
		this.currentControllerState=currentControllerState;
		this.newControllerState=newControllerState;
	}

	public RuntimeState getCurrentControllerState() {
		return currentControllerState;
	}

	public void setCurrentControllerState(RuntimeState currentControllerState) {
		this.currentControllerState = currentControllerState;
	}

	public RuntimeState getNewControllerState() {
		return newControllerState;
	}

	public void setNewControllerState(RuntimeState newControllerState) {
		this.newControllerState = newControllerState;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((currentControllerState == null) ? 0
						: currentControllerState.hashCode());
		result = prime
				* result
				+ ((newControllerState == null) ? 0 : newControllerState
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateCouple other = (StateCouple) obj;
		if (currentControllerState == null) {
			if (other.currentControllerState != null)
				return false;
		} else if (!currentControllerState.equals(other.currentControllerState))
			return false;
		if (newControllerState == null) {
			if (other.newControllerState != null)
				return false;
		} else if (!newControllerState.equals(other.newControllerState))
			return false;
		return true;
	}
	public EList<StateCouple> getOutgoingCouples(){
		RuntimeState s=currentControllerState;
		RuntimeState sPrime=newControllerState;
		EList<StateCouple> outgoingCouples=new BasicEList<StateCouple>();
		for (Transition t: s.getOutgoingTransition()){
			MessageEvent e=(MessageEvent)t.getEvent();
			for(Transition tPrime: sPrime.getOutgoingTransition()){
				MessageEvent ePrime=(MessageEvent)t.getEvent();
				if (e.getMessageName().equals(ePrime.getMessageName())){
					RuntimeState targetS=(RuntimeState)t.getTargetState();
					RuntimeState targetSPrime=(RuntimeState)tPrime.getTargetState();
					outgoingCouples.add(new StateCouple(targetS, targetSPrime));
				}
			}
		}
		return outgoingCouples;
	}
	
}
