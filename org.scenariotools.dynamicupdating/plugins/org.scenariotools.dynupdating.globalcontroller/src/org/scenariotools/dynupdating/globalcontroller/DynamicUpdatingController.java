package org.scenariotools.dynupdating.globalcontroller;




import java.io.IOException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class DynamicUpdatingController {
	private Resource dynamicControllerResource;
	private RuntimeStateGraph currentController;
	private RuntimeStateGraph mergedGraph;
	private RuntimeState initialStateOfCurrentController;
	private RuntimeStateGraph newController;
	private RuntimeState initialStateOfNewController;
	private Set<StateCouple> historySet;
	private EList<StateCouple> candidateQueue;
	public DynamicUpdatingController(){
		
	}
	public void generateDynamicController(RuntimeStateGraph currentController, RuntimeStateGraph newController){
		/*
		System.out.println("Current State Graph: \n"+currentController.getStates());
		System.out.println("Currrent State Graph Initial State Label: "+ currentController.getStartState().getLabel());		
		System.out.println("New State Graph: \n"+newController.getStates());
		System.out.println("New State Graph Initial State Label: "+ newController.getStartState().getLabel());
		*/

		//create a state graph that includes the two stategraph of the current version and the new version.
		/*
		//@ Joel When I copy the graph I have an exception saying that 
		 * The object 'org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl@187bbb53' is not contained in a resource.
		 * This is due to the fact that the MSD specific RuntimeGraph has some references to MSD specific objects that are not copied here.
		 * How do I copy all the related references but at the same time beeing independent of the MSD specific part?		 
		 * */
		//TODO copy the controller in an appropriate way
		//RuntimeStateGraph mergedGraph = EcoreUtil.copy(currentController);
	
		this.currentController=EcoreUtil.copy(currentController);
		//this.currentController=currentController;
		
		
		mergedGraph = this.currentController;
		initialStateOfCurrentController =(RuntimeState) mergedGraph.getStartState();
		
		//TODO copy the controller in an appropriate way
		this.newController=EcoreUtil.copy(newController);
		//this.newController=newController;
		
		initialStateOfNewController=(RuntimeState) this.newController.getStartState();
		
		
		//Algorithm: Compute the history relation mapping
		computeHistoryRelation();		
		
		createHistoryMappingTransitions();
		
		
		mergedGraph.getStates().addAll(this.newController.getStates());
		//Save the dynamic updating controller into a file
		dynamicControllerResource.getContents().add(mergedGraph);
		
		try {
			dynamicControllerResource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void computeHistoryRelation() {
		//1) Initialization
		historySet= new HashSet<StateCouple>();
		// the initial states are by definition part of the historySet
		StateCouple initialStateCouple=new StateCouple(initialStateOfCurrentController, initialStateOfNewController);
		historySet.add(initialStateCouple);		
		// initialize the candidate set
		candidateQueue= new UniqueEList<StateCouple>();
		
		//2) Compute the candidate queue
		//2.1) Do a breadth first search to find the next state
		
		EList<StateCouple>toVisitStateCouplesQueue=new UniqueEList<StateCouple>();
		
		//useful to mark the state as visited
		EMap<StateCouple, Boolean> visitedStates=new BasicEMap<StateCouple,Boolean>();
		//enqueue initialStateOfCurrentController onto toVisitQueue
		
		toVisitStateCouplesQueue.add(initialStateCouple);
		
		//mark initialStateOfCurrentController as visited
		visitedStates.put(initialStateCouple, true);
		
		RuntimeState consideredStateOfCurrentController;
		RuntimeState consideredStateOfNewController;
		while(!toVisitStateCouplesQueue.isEmpty()){
			StateCouple consideredStateCouple=toVisitStateCouplesQueue.get(0);
			consideredStateOfCurrentController= consideredStateCouple.getCurrentControllerState();			
			consideredStateOfNewController=consideredStateCouple.getNewControllerState();
			toVisitStateCouplesQueue.remove(0);
			// do not insert the initial state in the candidate queue
			if (!consideredStateCouple.equals(initialStateCouple)){				
				candidateQueue.add(consideredStateCouple);
			}
			for (Transition tCurrent: consideredStateOfCurrentController.getOutgoingTransition()){
				for(Transition tNew: consideredStateOfNewController.getOutgoingTransition()){
					MessageEvent currentControllerEvent=(MessageEvent) tCurrent.getEvent();
					MessageEvent newControllerEvent=(MessageEvent) tNew.getEvent();
					if(currentControllerEvent.getMessageName().equals(newControllerEvent.getMessageName())){
						//put in to the two queue the identified states.
						RuntimeState currentTargetState=(RuntimeState)tCurrent.getTargetState();
						RuntimeState newTargetState=(RuntimeState)tNew.getTargetState();	
						StateCouple mayBeVisitedStateCouple=new StateCouple(currentTargetState, newTargetState);
						if (null==visitedStates.get(mayBeVisitedStateCouple) || !visitedStates.get(mayBeVisitedStateCouple)){
							visitedStates.put(mayBeVisitedStateCouple,true);
							toVisitStateCouplesQueue.add(toVisitStateCouplesQueue.size(),mayBeVisitedStateCouple);
							
						}
						
					}
				}
			}
			
		}
		
		System.out.println("#### PRINT CANDIDATE QUEUE");
		printStateMapping(candidateQueue);		
		
		boolean candidatesRemoved=false;
		// 3) Finding the History Relation Mapping
		while(!candidateQueue.isEmpty() || candidatesRemoved ){
			candidatesRemoved=false;
			StateCouple candidateStateCouple=candidateQueue.get(0);
			//remove from the queue
			candidateQueue.remove(0);
			EList<StateCouple>sourceCoupleList= new UniqueEList<StateCouple>();
			RuntimeState q= candidateStateCouple.getCurrentControllerState();
			RuntimeState qPrime= candidateStateCouple.getNewControllerState();
			
			HashMap<String,RuntimeState> incomingMap=getIncomingSourceStates(qPrime);
			for(Transition t: q.getIncomingTransition()){
				MessageEvent incomingEvent= (MessageEvent) t.getEvent();
				RuntimeState sourceStateOfCurrentController=(RuntimeState)t.getSourceState();
				RuntimeState sourceStateOfNewController= incomingMap.get(incomingEvent.getMessageName());
				if(sourceStateOfNewController == null){
					//There is no state in the new controller that with the same incoming transition of the current one					
					//Remove the candidate couple and all its dependent couples from the queue
					candidateQueue.remove(candidateStateCouple);
					candidatesRemoved=removeDependentCandidate(candidateStateCouple, candidatesRemoved);
					break;					
				}
				else{
					StateCouple sourceStateCouple=new StateCouple(sourceStateOfCurrentController, sourceStateOfNewController);
					if (candidateQueue.contains(sourceStateCouple)){
						sourceCoupleList.add(sourceStateCouple);
						//put the candidate couple at the end of the candidate queue
						candidateQueue.add(candidateQueue.size(),candidateStateCouple);
					}
					else if(!historySet.contains(sourceStateCouple)){
						//the source state couple does not belong to the candidate queue and does not belong to the HistorySet						
						//Remove the candidate couple and all its dependent couples from the queue
						candidateQueue.remove(candidateStateCouple);
						candidatesRemoved=removeDependentCandidate(candidateStateCouple, candidatesRemoved);
						break;
					}
					
				}
				
			}
			if (historySet.containsAll(sourceCoupleList)){
				historySet.add(candidateStateCouple);
				
			}	
			
		}
		//all the remaining couples in candidateQueue depend with each other -> put in H
		historySet.addAll(candidateQueue);
		
		//Print History Mapping
		System.out.println("###Print History Mapping");
		printStateMapping(historySet);
		
		
	}
	
	private void createHistoryMappingTransitions(){
		for (StateCouple sc: historySet){
			//Add the history relation transition 
			Transition hTransition=StategraphFactory.eINSTANCE
					.createTransition();
			hTransition.getStringToStringAnnotationMap().put("History Relation","h");
			hTransition.setSourceState(sc.getCurrentControllerState());
			hTransition.setTargetState(sc.getNewControllerState());			

		}
	}
	//recursively removes all the dependent candidates
	private boolean removeDependentCandidate(StateCouple sc, boolean candidatesRemoved){		
		for (StateCouple outgoingCouple: sc.getOutgoingCouples()){
			if (candidateQueue.contains(outgoingCouple)){
				candidateQueue.remove(outgoingCouple);
				removeDependentCandidate(outgoingCouple,candidatesRemoved);
				candidatesRemoved=true;
			}
		}
		return candidatesRemoved;
	}

	private void findUpdatableStates(){
		RuntimeState s;
		
	}
	
	private HashMap<String,RuntimeState> getIncomingSourceStates (RuntimeState s){
		HashMap<String,	RuntimeState> resultingMap=new HashMap<String,RuntimeState>();
		for (Transition t:s.getIncomingTransition()){			
			MessageEvent e=(MessageEvent)t.getEvent();
			resultingMap.put(e.getMessageName(),(RuntimeState)t.getSourceState());
		}
		return resultingMap;
	}
	private void printStateMapping (Collection<StateCouple> mappingList){
		
		for (StateCouple sc: mappingList){
			System.out.println(sc.getCurrentControllerState().getLabel() +" ---> " + sc.getNewControllerState().getLabel());
		}
	}
	public Resource getDynamicControllerResource() {
		return dynamicControllerResource;
	}
	public void setDynamicControllerResource(Resource dynamicControllerResource) {
		this.dynamicControllerResource = dynamicControllerResource;
	}
	
	
}
