package org.scenariotools.dynupdating.synthesis.otfb.ui.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.scenariotools.dynupdating.synthesis.otfb.HistoryRelationAlgorithm;
import org.scenariotools.dynupdating.synthesis.otfb.OTFDynUpdatingSynthesisIncremental;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.synthesis.otfb.ui.job.OTFSynthesisIncrementalJob;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.synthesis.otfb.OTFSynthesis;
import org.scenariotools.synthesis.otfb.incremental.OTFBAlgorithmIncremental;

public class OTFDynUpdatingSynthesisIncrementalJob extends
		OTFSynthesisIncrementalJob {
	private HistoryRelationAlgorithm historyRelationAlgorithm;

	public OTFDynUpdatingSynthesisIncrementalJob(String name,
			IFile scenarioRunConfigurationResourceFile,
			ResourceSet resourceSet, MSDRuntimeStateGraph msdRuntimeStateGraph,
			RuntimeStateGraph baseController) {
		super(name, scenarioRunConfigurationResourceFile, resourceSet,
				msdRuntimeStateGraph, baseController);		
	}
	
	protected OTFSynthesis createOTFSynthesis(){
		OTFDynUpdatingSynthesisIncremental otfDynUpdatingSynthesisIncremental = new OTFDynUpdatingSynthesisIncremental();
		otfDynUpdatingSynthesisIncremental.initializeOTFBAlgorithm();
		otfDynUpdatingSynthesisIncremental.setHistoryRelationAlgorithm(this.historyRelationAlgorithm);
		((OTFBAlgorithmIncremental)otfDynUpdatingSynthesisIncremental.getOTFBAlgorithm()).setInitialBaseController(getBaseController());
		return otfDynUpdatingSynthesisIncremental;
	}
	public void setHistoryRelationAlgorithm(HistoryRelationAlgorithm historyRelationAlgorithm) {
		this.historyRelationAlgorithm = historyRelationAlgorithm;
	}

}
