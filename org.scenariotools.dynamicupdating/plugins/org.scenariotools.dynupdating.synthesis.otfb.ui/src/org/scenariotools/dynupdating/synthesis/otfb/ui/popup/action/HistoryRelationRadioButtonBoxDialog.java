package org.scenariotools.dynupdating.synthesis.otfb.ui.popup.action;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class HistoryRelationRadioButtonBoxDialog extends TitleAreaDialog{

	final String[] historyRelationOptions= new String[]{"Updatability criterion based on recent histories","Updatability criterion based on cycle-free recent histories"};
	public static enum Option {FUNDAMENTAL, CYCLE_FREE};
	private Option selectedOption;
	


	private Button[] radios;
	public HistoryRelationRadioButtonBoxDialog(Shell parentShell) {
		super(parentShell);
		setHelpAvailable(false);
	}

	@Override
	public void create() {
		super.create();
		// Set the title
		setTitle("Incremental Synthesis Of Dynamically Updating Controllers");
		// Set the message
		setMessage("Choose the criterion for dynamic updates", 
				IMessageProvider.INFORMATION);		
		getShell().setSize(new Point(500,200));
		setHelpAvailable(false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;		
		// layout.horizontalAlignment = GridData.FILL;
		parent.setLayout(layout);		
		radios =new Button[historyRelationOptions.length];
		for (int i=0; i<historyRelationOptions.length; i++ ){
			GridData gridData = new GridData();			
			radios[i]=new Button(parent, SWT.RADIO);
			radios[i].setText(historyRelationOptions[i]);
			radios[i].setLayoutData(gridData);
		}
		//set the first option as default
		radios[0].setSelection(true);
		return parent;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.LEFT;
		
		

		parent.setLayoutData(gridData);
		// Create Run button
		// Own method as we need to overview the SelectionAdapter
		createOkButton(parent, OK, "Run", true);
		// Add a SelectionListener

		// Create Cancel button
		Button cancelButton = 
				createButton(parent, CANCEL, "Cancel", false);
		// Add a SelectionListener
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});
	}

	protected Button createOkButton(Composite parent, int id, 
			String label,
			boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {			
					okPressed();			
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		setButtonLayoutData(button);
		return button;
	}


	@Override
	protected boolean isResizable() {
		return false;
	}

	@Override
	protected void okPressed() {	
		 for (int i = 0; i < radios.length; i++){
             if (radios[i].getSelection())
            	 selectedOption=Option.values()[i];          
         }
		super.okPressed();
	}
	public Option getSelectedOption() {
		return selectedOption;
	}


} 
