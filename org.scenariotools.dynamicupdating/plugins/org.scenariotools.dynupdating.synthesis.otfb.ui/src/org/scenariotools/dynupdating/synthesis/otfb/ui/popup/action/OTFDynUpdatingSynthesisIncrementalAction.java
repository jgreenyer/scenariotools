package org.scenariotools.dynupdating.synthesis.otfb.ui.popup.action;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.window.Window;
import org.scenariotools.dynupdating.synthesis.otfb.CycleFreeHistoryRelationAlgorithm;
import org.scenariotools.dynupdating.synthesis.otfb.RecentHistoryRelationAlgorithm;
import org.scenariotools.dynupdating.synthesis.otfb.HistoryRelationAlgorithm;
import org.scenariotools.dynupdating.synthesis.otfb.ui.job.OTFDynUpdatingSynthesisIncrementalJob;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.synthesis.otfb.ui.popup.actions.OTFSynthesisIncrementalAction;

public class OTFDynUpdatingSynthesisIncrementalAction extends
		OTFSynthesisIncrementalAction {
	
	private HistoryRelationAlgorithm historyRelationAlgorithm;
	
	@Override
	protected boolean openDialog(Job job){

		HistoryRelationRadioButtonBoxDialog dialog = new HistoryRelationRadioButtonBoxDialog(null);		
		dialog.create();		
		int result = dialog.open();
		
		if( result == Window.OK){
			if(dialog.getSelectedOption()==HistoryRelationRadioButtonBoxDialog.Option.FUNDAMENTAL){
				historyRelationAlgorithm=new RecentHistoryRelationAlgorithm();				
			}
			else if (dialog.getSelectedOption()==HistoryRelationRadioButtonBoxDialog.Option.CYCLE_FREE){
				historyRelationAlgorithm=new CycleFreeHistoryRelationAlgorithm();
			}			
			else{
				//no other options currently supported
				throw new UnsupportedOperationException();
			}			
			((OTFDynUpdatingSynthesisIncrementalJob)job).setHistoryRelationAlgorithm(historyRelationAlgorithm);
			return true;
		}
		return false;	  		  
		
		 
	}
	
	@Override
	protected Job createSynthesisJob() {
		return new OTFDynUpdatingSynthesisIncrementalJob("On-the-fly Incremental Controller Synthesis of a dynamically updating controller from an MSD specification and base controller", getScenarioRunConfigurationResourceFile(), getResourceSet(), getMsdRuntimeStateGraph(), getBaseController());
	}
	
	@Override
	protected void reconfigureScenarioRunConfiguration(
			ScenarioRunConfiguration scenarioRunConfiguration) {
		scenarioRunConfiguration.setExecutionSemantics(ExecutionSemantics.PLAY_OUT_BUT_WAITING_POSSIBLE);
	}
}
