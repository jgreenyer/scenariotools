package org.scenariotools.msd.runtime.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry;

/**
 * @generated
 */
public class RuntimeEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (RuntimeVisualIDRegistry.getVisualID(view)) {

			case RuntimeStateGraphEditPart.VISUAL_ID:
				return new RuntimeStateGraphEditPart(view);

			case RuntimeStateEditPart.VISUAL_ID:
				return new RuntimeStateEditPart(view);

			case RuntimeStateLabelEditPart.VISUAL_ID:
				return new RuntimeStateLabelEditPart(view);

			case TransitionEditPart.VISUAL_ID:
				return new TransitionEditPart(view);

			case TransitionLabelEditPart.VISUAL_ID:
				return new TransitionLabelEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
