package org.scenariotools.msd.runtime.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateGraphEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionEditPart;
import org.scenariotools.msd.runtime.diagram.providers.RuntimeElementTypes;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphPackage;
import org.scenariotools.stategraph.Transition;

/**
 * @generated
 */
public class RuntimeDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<RuntimeNodeDescriptor> getSemanticChildren(View view) {
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateGraphEditPart.VISUAL_ID:
			return getRuntimeStateGraph_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeNodeDescriptor> getRuntimeStateGraph_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		RuntimeStateGraph modelElement = (RuntimeStateGraph) view.getElement();
		LinkedList<RuntimeNodeDescriptor> result = new LinkedList<RuntimeNodeDescriptor>();
		for (Iterator<?> it = modelElement.getStates().iterator(); it.hasNext();) {
			State childElement = (State) it.next();
			int visualID = RuntimeVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RuntimeStateEditPart.VISUAL_ID) {
				result.add(new RuntimeNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getContainedLinks(View view) {
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateGraphEditPart.VISUAL_ID:
			return getRuntimeStateGraph_1000ContainedLinks(view);
		case RuntimeStateEditPart.VISUAL_ID:
			return getRuntimeState_2001ContainedLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getIncomingLinks(View view) {
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateEditPart.VISUAL_ID:
			return getRuntimeState_2001IncomingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getOutgoingLinks(View view) {
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateEditPart.VISUAL_ID:
			return getRuntimeState_2001OutgoingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getRuntimeStateGraph_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getRuntimeState_2001ContainedLinks(
			View view) {
		RuntimeState modelElement = (RuntimeState) view.getElement();
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getTransition_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getRuntimeState_2001IncomingLinks(
			View view) {
		RuntimeState modelElement = (RuntimeState) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Transition_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getTransition_4001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getRuntimeState_2001OutgoingLinks(
			View view) {
		RuntimeState modelElement = (RuntimeState) view.getElement();
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RuntimeLinkDescriptor> getTransition_4001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<RuntimeLinkDescriptor> getContainedTypeModelFacetLinks_Transition_4001(
			State container) {
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingTransition().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != RuntimeVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTargetState();
			State src = link.getSourceState();
			result.add(new RuntimeLinkDescriptor(src, dst, link,
					RuntimeElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RuntimeLinkDescriptor> getIncomingTypeModelFacetLinks_Transition_4001(
			State target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != StategraphPackage.eINSTANCE
					.getTransition_TargetState()
					|| false == setting.getEObject() instanceof Transition) {
				continue;
			}
			Transition link = (Transition) setting.getEObject();
			if (TransitionEditPart.VISUAL_ID != RuntimeVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			State src = link.getSourceState();
			result.add(new RuntimeLinkDescriptor(src, target, link,
					RuntimeElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RuntimeLinkDescriptor> getOutgoingTypeModelFacetLinks_Transition_4001(
			State source) {
		State container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof State) {
				container = (State) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<RuntimeLinkDescriptor> result = new LinkedList<RuntimeLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingTransition().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != RuntimeVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTargetState();
			State src = link.getSourceState();
			if (src != source) {
				continue;
			}
			result.add(new RuntimeLinkDescriptor(src, dst, link,
					RuntimeElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<RuntimeNodeDescriptor> getSemanticChildren(View view) {
			return RuntimeDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RuntimeLinkDescriptor> getContainedLinks(View view) {
			return RuntimeDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RuntimeLinkDescriptor> getIncomingLinks(View view) {
			return RuntimeDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RuntimeLinkDescriptor> getOutgoingLinks(View view) {
			return RuntimeDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
