package org.scenariotools.msd.runtime.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.scenariotools.msd.runtime.diagram.providers.RuntimeElementTypes;

/**
 * @generated
 */
public class RuntimePaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createRuntime1Group());
	}

	/**
	 * Creates "runtime" palette tool group
	 * @generated
	 */
	private PaletteContainer createRuntime1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Runtime1Group_title);
		paletteContainer.setId("createRuntime1Group"); //$NON-NLS-1$
		paletteContainer.add(createRuntimeState1CreationTool());
		paletteContainer.add(createStateOutgoingTransition2CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRuntimeState1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.RuntimeState1CreationTool_title,
				Messages.RuntimeState1CreationTool_desc,
				Collections
						.singletonList(RuntimeElementTypes.RuntimeState_2001));
		entry.setId("createRuntimeState1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(RuntimeElementTypes
				.getImageDescriptor(RuntimeElementTypes.RuntimeState_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStateOutgoingTransition2CreationTool() {
		ToolEntry entry = new ToolEntry(
				Messages.StateOutgoingTransition2CreationTool_title,
				Messages.StateOutgoingTransition2CreationTool_desc, null, null) {
		};
		entry.setId("createStateOutgoingTransition2CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
