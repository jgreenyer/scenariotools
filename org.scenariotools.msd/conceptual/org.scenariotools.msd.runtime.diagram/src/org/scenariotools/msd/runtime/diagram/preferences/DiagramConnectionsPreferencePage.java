package org.scenariotools.msd.runtime.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.ConnectionsPreferencePage;
import org.scenariotools.msd.runtime.diagram.part.RuntimeDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramConnectionsPreferencePage extends ConnectionsPreferencePage {

	/**
	 * @generated
	 */
	public DiagramConnectionsPreferencePage() {
		setPreferenceStore(RuntimeDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
