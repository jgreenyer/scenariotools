package org.scenariotools.msd.runtime.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;
import org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry;

/**
 * @generated
 */
public class RuntimeNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 4003;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof RuntimeNavigatorItem) {
			RuntimeNavigatorItem item = (RuntimeNavigatorItem) element;
			return RuntimeVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
