package org.scenariotools.msd.runtime.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateLabelEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionLabelEditPart;
import org.scenariotools.msd.runtime.diagram.parsers.MessageFormatParser;
import org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * @generated
 */
public class RuntimeParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser runtimeStateLabel_5001Parser;

	/**
	 * @generated NOT
	 */
	private IParser getRuntimeStateLabel_5001Parser() {
		if (runtimeStateLabel_5001Parser == null) {
//			EAttribute[] features = new EAttribute[] { StategraphPackage.eINSTANCE
//					.getState_Label() };
//			MessageFormatParser parser = new MessageFormatParser(features);
//			runtimeStateLabel_5001Parser = parser;
			runtimeStateLabel_5001Parser = new IParser() {
				
				@Override
				public IParserEditStatus isValidEditString(IAdaptable element,
						String editString) {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public boolean isAffectingEvent(Object event, int flags) {
					// TODO Auto-generated method stub
					return false;
				}
				
				@Override
				public String getPrintString(IAdaptable element, int flags) {
					if (element instanceof EObjectAdapter){
						Object object = ((EObjectAdapter)element).getRealObject();
							if (object instanceof RuntimeState)
								return ((RuntimeState) object).getStringToStringAnnotationMap().get("visLabel");
					}
					return null;
				}
				
				@Override
				public ICommand getParseCommand(IAdaptable element, String newString,
						int flags) {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getEditString(IAdaptable element, int flags) {
					if (element instanceof RuntimeState)
						return ((RuntimeState) element).getStringToStringAnnotationMap().get("visLabel");
					return null;
				}
				
				@Override
				public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
					// TODO Auto-generated method stub
					return null;
				}
			};
		}
		return runtimeStateLabel_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser transitionLabel_6001Parser;

	/**
	 * @generated
	 */
	private IParser getTransitionLabel_6001Parser() {
		if (transitionLabel_6001Parser == null) {
			EAttribute[] features = new EAttribute[] { StategraphPackage.eINSTANCE
					.getTransition_Label() };
			MessageFormatParser parser = new MessageFormatParser(features);
			transitionLabel_6001Parser = parser;
		}
		return transitionLabel_6001Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case RuntimeStateLabelEditPart.VISUAL_ID:
			return getRuntimeStateLabel_5001Parser();
		case TransitionLabelEditPart.VISUAL_ID:
			return getTransitionLabel_6001Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(RuntimeVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(RuntimeVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (RuntimeElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
