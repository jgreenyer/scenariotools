package org.scenariotools.msd.runtime.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String RuntimeCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String RuntimeDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String RuntimeNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String RuntimeDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String RuntimeElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Runtime1Group_title;

	/**
	 * @generated
	 */
	public static String RuntimeState1CreationTool_title;

	/**
	 * @generated
	 */
	public static String RuntimeState1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StateOutgoingTransition2CreationTool_title;

	/**
	 * @generated
	 */
	public static String StateOutgoingTransition2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RuntimeState_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RuntimeState_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RuntimeStateGraph_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String RuntimeModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String RuntimeModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
