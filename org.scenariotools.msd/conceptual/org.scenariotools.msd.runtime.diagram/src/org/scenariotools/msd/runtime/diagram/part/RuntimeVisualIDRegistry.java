package org.scenariotools.msd.runtime.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateGraphEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateLabelEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionLabelEditPart;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class RuntimeVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.scenariotools.msd.runtime.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (RuntimeStateGraphEditPart.MODEL_ID.equals(view.getType())) {
				return RuntimeStateGraphEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				RuntimeDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (RuntimePackage.eINSTANCE.getRuntimeStateGraph().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((RuntimeStateGraph) domainElement)) {
			return RuntimeStateGraphEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
				.getModelID(containerView);
		if (!RuntimeStateGraphEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (RuntimeStateGraphEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RuntimeStateGraphEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case RuntimeStateGraphEditPart.VISUAL_ID:
			if (RuntimePackage.eINSTANCE.getRuntimeState().isSuperTypeOf(
					domainElement.eClass())) {
				return RuntimeStateEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
				.getModelID(containerView);
		if (!RuntimeStateGraphEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (RuntimeStateGraphEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RuntimeStateGraphEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case RuntimeStateGraphEditPart.VISUAL_ID:
			if (RuntimeStateEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RuntimeStateEditPart.VISUAL_ID:
			if (RuntimeStateLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TransitionEditPart.VISUAL_ID:
			if (TransitionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (StategraphPackage.eINSTANCE.getTransition().isSuperTypeOf(
				domainElement.eClass())) {
			return TransitionEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(RuntimeStateGraph element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case RuntimeStateGraphEditPart.VISUAL_ID:
			return false;
		case RuntimeStateEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
