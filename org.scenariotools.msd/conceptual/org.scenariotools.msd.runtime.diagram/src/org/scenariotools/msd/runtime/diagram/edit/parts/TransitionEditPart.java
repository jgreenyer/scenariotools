package org.scenariotools.msd.runtime.diagram.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.scenariotools.msd.runtime.diagram.edit.policies.TransitionItemSemanticEditPolicy;
import org.scenariotools.stategraph.Transition;

/**
 * @generated
 */
public class TransitionEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4001;

	/**
	 * @generated
	 */
	public TransitionEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new TransitionItemSemanticEditPolicy());
	}

	/**
	 * @generated NOT
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof TransitionLabelEditPart) {
			((TransitionLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureTransitionEventLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof TransitionLabelEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new RuntimeStateOutgoingTransitionFigure();
	}

	/**
	 * @generated
	 */
	public RuntimeStateOutgoingTransitionFigure getPrimaryShape() {
		return (RuntimeStateOutgoingTransitionFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class RuntimeStateOutgoingTransitionFigure extends
			PolylineConnectionEx {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureTransitionEventLabelFigure;

		/**
		 * @generated
		 */
		public RuntimeStateOutgoingTransitionFigure() {

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated NOT
		 */
		private void createContents() {

			fFigureTransitionEventLabelFigure = new WrappingLabel();

			fFigureTransitionEventLabelFigure.setText("<..>");
			
			if (getModel() != null && getModel() instanceof Transition){
				Transition transition = (Transition) getModel();
				if (transition.getEvent() != null)
					fFigureTransitionEventLabelFigure.setText(transition.getEvent().toString());
				else {
					fFigureTransitionEventLabelFigure.setText("update");
				}
			}

			this.add(fFigureTransitionEventLabelFigure);

		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureTransitionEventLabelFigure() {
			return fFigureTransitionEventLabelFigure;
		}

	}

}
