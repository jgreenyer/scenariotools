package org.scenariotools.msd.runtime.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;
import org.scenariotools.msd.runtime.diagram.part.RuntimeDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(RuntimeDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
