package org.scenariotools.msd.runtime.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;
import org.scenariotools.msd.runtime.diagram.part.RuntimeDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(RuntimeDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
