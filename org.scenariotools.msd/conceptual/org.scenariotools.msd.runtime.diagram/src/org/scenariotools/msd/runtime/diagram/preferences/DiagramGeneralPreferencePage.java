package org.scenariotools.msd.runtime.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;
import org.scenariotools.msd.runtime.diagram.part.RuntimeDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	 * @generated
	 */
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(RuntimeDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
