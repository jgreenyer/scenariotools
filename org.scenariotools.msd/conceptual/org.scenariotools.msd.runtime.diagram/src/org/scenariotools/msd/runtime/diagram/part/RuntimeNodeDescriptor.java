package org.scenariotools.msd.runtime.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class RuntimeNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	 * @generated
	 */
	public RuntimeNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
