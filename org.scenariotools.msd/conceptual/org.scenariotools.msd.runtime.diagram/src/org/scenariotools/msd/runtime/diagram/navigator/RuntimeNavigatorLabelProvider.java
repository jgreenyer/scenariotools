package org.scenariotools.msd.runtime.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateGraphEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.RuntimeStateLabelEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionEditPart;
import org.scenariotools.msd.runtime.diagram.edit.parts.TransitionLabelEditPart;
import org.scenariotools.msd.runtime.diagram.part.RuntimeDiagramEditorPlugin;
import org.scenariotools.msd.runtime.diagram.part.RuntimeVisualIDRegistry;
import org.scenariotools.msd.runtime.diagram.providers.RuntimeElementTypes;
import org.scenariotools.msd.runtime.diagram.providers.RuntimeParserProvider;

/**
 * @generated
 */
public class RuntimeNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		RuntimeDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		RuntimeDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof RuntimeNavigatorItem
				&& !isOwnView(((RuntimeNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof RuntimeNavigatorGroup) {
			RuntimeNavigatorGroup group = (RuntimeNavigatorGroup) element;
			return RuntimeDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof RuntimeNavigatorItem) {
			RuntimeNavigatorItem navigatorItem = (RuntimeNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://org.scenariotools.runtime/1.0?RuntimeState", RuntimeElementTypes.RuntimeState_2001); //$NON-NLS-1$
		case RuntimeStateGraphEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://org.scenariotools.runtime/1.0?RuntimeStateGraph", RuntimeElementTypes.RuntimeStateGraph_1000); //$NON-NLS-1$
		case TransitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://org.scenariotools.stategraph/1.0?Transition", RuntimeElementTypes.Transition_4001); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = RuntimeDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& RuntimeElementTypes.isKnownElementType(elementType)) {
			image = RuntimeElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof RuntimeNavigatorGroup) {
			RuntimeNavigatorGroup group = (RuntimeNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof RuntimeNavigatorItem) {
			RuntimeNavigatorItem navigatorItem = (RuntimeNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (RuntimeVisualIDRegistry.getVisualID(view)) {
		case RuntimeStateEditPart.VISUAL_ID:
			return getRuntimeState_2001Text(view);
		case RuntimeStateGraphEditPart.VISUAL_ID:
			return getRuntimeStateGraph_1000Text(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getRuntimeState_2001Text(View view) {
		IParser parser = RuntimeParserProvider.getParser(
				RuntimeElementTypes.RuntimeState_2001,
				view.getElement() != null ? view.getElement() : view,
				RuntimeVisualIDRegistry
						.getType(RuntimeStateLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RuntimeDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTransition_4001Text(View view) {
		IParser parser = RuntimeParserProvider.getParser(
				RuntimeElementTypes.Transition_4001,
				view.getElement() != null ? view.getElement() : view,
				RuntimeVisualIDRegistry
						.getType(TransitionLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			RuntimeDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRuntimeStateGraph_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return RuntimeStateGraphEditPart.MODEL_ID
				.equals(RuntimeVisualIDRegistry.getModelID(view));
	}

}
