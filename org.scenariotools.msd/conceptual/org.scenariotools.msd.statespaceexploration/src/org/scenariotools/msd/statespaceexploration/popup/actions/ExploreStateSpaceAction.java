package org.scenariotools.msd.statespaceexploration.popup.actions;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class ExploreStateSpaceAction implements IObjectActionDelegate {

	private Shell shell;
	protected static String jobName="MSD state space exploration.";
	protected static String infoTitle="State Space Exploration";
	
	/**
	 * Constructor for Action1.
	 */
	public ExploreStateSpaceAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
	
	
	protected MSDRuntimeStateGraph createMSDRuntimeStateGraph(){
		return RuntimeFactory.eINSTANCE.createMSDRuntimeStateGraph();
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

			IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
			final IFile file = (IFile) structuredSelection.getFirstElement();
	
			final ResourceSet resourceSet = new ResourceSetImpl();
			final Resource scenarioRunConfigurationResource = resourceSet.getResource(URI.createPlatformResourceURI(file.getFullPath().toString(), true), true);
			
			ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource.getContents().get(0);
			
			final MSDRuntimeStateGraph msdRuntimeStateGraph = createMSDRuntimeStateGraph();
			
			final MSDRuntimeState msdRuntimeStartState = msdRuntimeStateGraph.init(scenarioRunConfiguration);
			
			final Job job = new Job(jobName){

				private Set<RuntimeState> exploredStates = new HashSet<RuntimeState>();
				
				protected IStatus run(IProgressMonitor monitor) {

					Resource scenarioRunConfigurationResource;
					@SuppressWarnings("null")
					URI scenarioRunConfigurationFileURI = URI.createPlatformResourceURI(file.getFullPath().toString(), true)
					.trimFileExtension().appendFileExtension("msdruntime");
					try {
						scenarioRunConfigurationResource = resourceSet.getResource(scenarioRunConfigurationFileURI, true);
						if (scenarioRunConfigurationResource != null){
							MessageDialog.openError(new Shell(), "An error occurred while creating the interpreter configuration", 
									"There already exists a MSD runtime file at the default location:\n" 
									+ scenarioRunConfigurationFileURI + "\n\n"
									+ "Remove it first to create a new one.");
						}
						
						return Status.CANCEL_STATUS;
					} catch (Exception e) {
						// that's good. Continue.
					}

					
					long currentTimeMillis = System.currentTimeMillis();
					
					DFS(msdRuntimeStartState, msdRuntimeStateGraph, 0);
					
					long currentTimeMillisDelta = System.currentTimeMillis() - currentTimeMillis;					

					
					//remove reference to RuntimeUtil:
					msdRuntimeStateGraph.setRuntimeUtil(null);
					for (State state : msdRuntimeStateGraph.getStates()) {
						MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
						msdRuntimeState.setRuntimeUtil(null);
					}
					
					for (ActiveProcess activeProcess : msdRuntimeStateGraph.getElementContainer().getActiveProcesses()) {
						activeProcess.setRuntimeUtil(null);
						if(activeProcess instanceof ActiveMSD)
							((ActiveMSD)activeProcess).setMsdUtil(null);
					}
					for (ActiveState activeState : msdRuntimeStateGraph.getElementContainer().getActiveStates()) {
						activeState.setRuntimeUtil(null);
					}
					for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph.getElementContainer().getObjectSystems()) {
						msdObjectSystem.setRuntimeUtil(null);
					}

					

					scenarioRunConfigurationResource = 
							resourceSet.createResource(scenarioRunConfigurationFileURI);
						scenarioRunConfigurationResource.getContents().add(msdRuntimeStateGraph);
						try {
							scenarioRunConfigurationResource.save(Collections.EMPTY_MAP);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}


		        	postExplorationFinished(msdRuntimeStateGraph, currentTimeMillisDelta);

					return Status.OK_STATUS;
				}
				
				
				
				private void DFS(RuntimeState runtimeState, RuntimeStateGraph runtimeStateGraph, int depth){
					if (depth > 12) return;
					//System.out.println("DFS called with depth " + depth);
					if (exploredStates.contains(runtimeState)) { // already visited.
						//System.out.println("state already visited by DFS: " + runtimeState);
						return; 
					}else{
						//System.out.println("state not yet visited by DFS: " + runtimeState);						
					}
					exploredStates.add(runtimeState);

					List<Transition> allOutgoingTransitions = runtimeStateGraph.generateAllSuccessors(runtimeState);
//					List<RuntimeState> allSuccessorStates = runtimeStateGraph.generateAllSuccessors(runtimeState);
//					List<Transition> allOutgoingTransitions = generateAllNonSafetyViolatingSuccessors(runtimeStateGraph, runtimeState);
					
					
//					for (MessageEvent messageEvent : runtimeState.getMessageEventToModalMessageEventMap().keySet()) {
//						System.out.println(messageEvent);
//					}
					for (Transition transition : allOutgoingTransitions) {
						DFS((RuntimeState) transition.getTargetState(), runtimeStateGraph, depth+1);
					}
				}
				
				private EList<Transition> generateAllNonSafetyViolatingSuccessors(RuntimeStateGraph runtimeStateGraph, RuntimeState runtimeState) {
					EList<Transition> allOutgoingTransitions = new BasicEList<Transition>();
					for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState.getMessageEventToModalMessageEventMap().entrySet()) {
						if ( !messageEventToModalMessageEventMapEntry.getValue().getAssumptionsModality().isSafetyViolating()){
							allOutgoingTransitions.add(runtimeStateGraph.generateSuccessor(runtimeState, messageEventToModalMessageEventMapEntry.getKey()));
						}
					}
					return allOutgoingTransitions;
				}

				
			};
						
		    job.setUser(true);
		    job.schedule();
	}

	public static void postExplorationFinished(final MSDRuntimeStateGraph msdRuntimeStateGraph, final long currentTimeMillisDelta){
		Display.getDefault().asyncExec (new Runnable() {
			@Override
			public void run() {
				
				int numberOfStates = 0;
				int numberOfTransitions = 0;
				
				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					for (Transition transition : state.getOutgoingTransition()) {
						numberOfTransitions++;
					}
				}
				
				MessageDialog.openInformation(
						new Shell(),
						infoTitle,
						"Explore State Space was executed, " + numberOfStates + " states were explored, " + numberOfTransitions + " transitions were created.\n" +
								"Time taken: " + (currentTimeMillisDelta) + " milliseconds.");
			}
		});
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
