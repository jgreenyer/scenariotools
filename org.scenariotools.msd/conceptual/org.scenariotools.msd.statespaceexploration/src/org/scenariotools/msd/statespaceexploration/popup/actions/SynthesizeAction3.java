package org.scenariotools.msd.statespaceexploration.popup.actions;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class SynthesizeAction3 implements IObjectActionDelegate {

	private Shell shell;

	/**
	 * Constructor for Action1.
	 */
	public SynthesizeAction3() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.getFirstElement();

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(file.getFullPath()
						.toString(), true), true);

		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

		final MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();

		final MSDRuntimeState msdRuntimeStartState = msdRuntimeStateGraph
				.init(scenarioRunConfiguration);

		final Job job = new Job("MSD state space exploration.") {

			protected IStatus run(IProgressMonitor monitor) {

				Resource scenarioRunConfigurationResource;

				URI scenarioRunConfigurationFileURI = URI
						.createPlatformResourceURI(
								file.getFullPath().toString(), true)
						.trimFileExtension().appendFileExtension("msdruntime");
				try {
					scenarioRunConfigurationResource = resourceSet.getResource(
							scenarioRunConfigurationFileURI, true);
					if (scenarioRunConfigurationResource != null) {
						MessageDialog
								.openError(
										new Shell(),
										"An error occurred while creating the interpreter configuration",
										"There already exists a MSD runtime file at the default location:\n"
												+ scenarioRunConfigurationFileURI
												+ "\n\n"
												+ "Remove it first to create a new one.");
					}

					return Status.CANCEL_STATUS;
				} catch (Exception e) {
					// that's good. Continue.
				}

				long currentTimeMillis = System.currentTimeMillis();

				// canReachAnotherGoalState(msdRuntimeStartState);
				Set<RuntimeState> winningStates = OTFComputeBuechiWinningStrategy(msdRuntimeStartState);
				boolean buechiStrategyExists = winningStates
						.contains(msdRuntimeStartState);

				System.out.println(winningStates);

				long currentTimeMillisDelta = System.currentTimeMillis()
						- currentTimeMillis;

				// remove reference to RuntimeUtil:
				msdRuntimeStateGraph.setRuntimeUtil(null);
				for (State state : msdRuntimeStateGraph.getStates()) {
					MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
					msdRuntimeState.setRuntimeUtil(null);
				}

				for (ActiveProcess activeProcess : msdRuntimeStateGraph.getElementContainer().getActiveProcesses()) {
					activeProcess.setRuntimeUtil(null);
					if(activeProcess instanceof ActiveMSD)
						((ActiveMSD)activeProcess).setMsdUtil(null);
				}
				for (ActiveState activeState : msdRuntimeStateGraph.getElementContainer().getActiveStates()) {
					activeState.setRuntimeUtil(null);
				}
				for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph
						.getElementContainer().getObjectSystems()) {
					msdObjectSystem.setRuntimeUtil(null);
				}

				scenarioRunConfigurationResource = resourceSet
						.createResource(scenarioRunConfigurationFileURI);
				scenarioRunConfigurationResource.getContents().add(
						msdRuntimeStateGraph);
				try {
					scenarioRunConfigurationResource
							.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				postExplorationFinished(msdRuntimeStateGraph,
						currentTimeMillisDelta, buechiStrategyExists,
						winningStates);

				return Status.OK_STATUS;
			}

			Set<RuntimeState> goal;
			Set<RuntimeState> win;
			Set<RuntimeState> passed;
			Set<RuntimeState> loseBuechi;
			Map<RuntimeState, List<Transition>> depend;

			private void addDepend(RuntimeState q, Transition e) {
				List<Transition> dependingTransitions = depend.get(q);
				if (dependingTransitions == null) {
					dependingTransitions = new UniqueEList<Transition>();
					depend.put(q, dependingTransitions);
				}
				dependingTransitions.add(e);
			}

			private boolean isGoal(RuntimeState q) {
				if (goal.contains(q))
					return true;
				else if (!(q.isSafetyViolationOccurredInRequirements() || hasMandatoryMessageEvents(
						q, false))
						|| q.isSafetyViolationOccurredInAssumptions()
						|| hasMandatoryMessageEvents(q, true)) {
					goal.add(q);
					return true;
				}
				return false;
			}

			private Set<RuntimeState> OTFComputeBuechiWinningStrategy(
					RuntimeState startState) {

				// **************
				// INITIALIZATION
				// **************
				win = new HashSet<RuntimeState>();
				goal = new HashSet<RuntimeState>();
				passed = new HashSet<RuntimeState>();
				depend = new HashMap<RuntimeState, List<Transition>>();
				loseBuechi = new HashSet<RuntimeState>();

				// **************
				// First run of OTFUR
				// **************
				OTFUR(startState);
				if(!win.contains(startState))
					return new HashSet<RuntimeState>();

				Set<RuntimeState> reevaluate = new HashSet<RuntimeState>();

				// terminate loops if
				// 1. all goal states are winning, i.e., from every found goal
				// state we can guarantee to reach another goal state, OR
				// 2. startState is a buechi-losing state, i.e., during the
				// first or a subsequent run of the reachability analysis and
				// its backward
				// reevaluation, we determined that it is impossible to
				// guarantee repeatedly reaching a goal state from startState.
				HashSet<RuntimeState> goalButNotWinOrLoseBuechi;
				goalButNotWinOrLoseBuechi = new HashSet<RuntimeState>(goal);
				goalButNotWinOrLoseBuechi.removeAll(win);
				goalButNotWinOrLoseBuechi.removeAll(loseBuechi);
				while(!goalButNotWinOrLoseBuechi.isEmpty()) {
						reevaluate.add(goalButNotWinOrLoseBuechi.iterator()
								.next());
					do {

						// Assert.isTrue(winningStatesRemoved.size() != 0 ||
						// winningStatesAdded.size() != 0);

						// for debug reasons only.
						// Set<RuntimeState> currentWinningStates = new
						// HashSet<RuntimeState>(win);

						RuntimeState q2 = reevaluate.iterator().next();
						reevaluate.remove(q2);

						// **************
						// Subsequent runs of OTFUR
						// **************
						win.remove(q2);
						OTFUR(q2);

						if (!win.contains(q2)) {
							if(q2==startState)
								return new HashSet<RuntimeState>();
							loseBuechi.add(q2);
							for (Transition depTrans : depend.get(q2)){
								RuntimeState dependent=(RuntimeState) depTrans
										.getSourceState();
								if(!loseBuechi.contains(dependent))
									reevaluate.add(dependent);
							}
						}

						// Assert.isTrue(win.contains(q0) ||
						// topExploredState != q0);
						// Assert.isTrue(!win.contains(topExploredState) ||
						// topExploredState == q0);


						// for debug reasons only.

						// winningStatesRemoved = new
						// HashSet<RuntimeState>(win);
						// winningStatesRemoved.removeAll(currentWinningStates);
						//
						// winningStatesAdded = new
						// HashSet<RuntimeState>(currentWinningStates);
						// winningStatesAdded.removeAll(win);

					} while (!reevaluate.isEmpty());
					goalButNotWinOrLoseBuechi = new HashSet<RuntimeState>(goal);
					goalButNotWinOrLoseBuechi.removeAll(win);
					goalButNotWinOrLoseBuechi.removeAll(loseBuechi);
				}
				return win;
			}

			/**
			 * By forward exploration and backward reevaluation, we mark all the
			 * states as winning from where we can guarantee reaching a goal
			 * state. Goal states are only winning if it can be guaranteed to
			 * again reach a goal state from them.
			 * 
			 * During backward reevaluation, states that were marked winning
			 * previously can be unmarked again iff we determine that we may not
			 * guarantee reaching a goal state from this state infinitely often.
			 * 
			 * If during backward evaluation we find that startState is not
			 * winning and there are other states depending on startState to be
			 * winning (in the global depend map), then backward reevaluation
			 * goes further backward in the depending states than startState.
			 * I.e., states that were marked winning by a previous run of the
			 * operation can be unmarked winning again. If this happens and the
			 * unmarked state has unexplored successors by which we may yet
			 * guarantee reaching another goal state, then we schedule the
			 * transitions to these successors for a new forward exploration.
			 * 
			 * From this follows that upon termination we can be sure that for
			 * all passed states that are not marked winning, it is impossible
			 * to guarantee reaching a goal state infinitely often.
			 * 
			 * @param startState
			 * @return
			 */
			private boolean OTFUR(RuntimeState startState) {

				System.out
						.println("OTFUR called with state "
								+ RuntimeUtil.Helper
										.getMSDRuntimeStateString((MSDRuntimeState) startState));

				//TODO: what was this for? contradiction to alg in TR?
				// must not be called with state that is marked winning.
				//Assert.isTrue(!win.contains(startState));

				RuntimeStateGraph runtimeStateGraph = (RuntimeStateGraph) startState
						.getStateGraph();

				// **************
				// INITIALIZATION
				// **************
				Stack<Transition> waiting = new Stack<Transition>();
				passed.add(startState);
				addTransitionsForAllOutgoingConcreteMessageEvents(startState,
						waiting);

				// **************
				// LOOP
				// **************
				// break the loop if
				// - no more transitions on waiting stack are waiting to be
				// explored OR
				// - the start state was marked winning or losing
				while (!waiting.isEmpty() && !win.contains(startState)
						//&& !lose.contains(startState) 
						//&& !loseBuechi.contains(startState)
						) {

					printStack(waiting);

					Transition t = waiting.pop();

					MessageEvent messageEvent = (MessageEvent) t.getEvent();

					// a transition on the stack may be an already explored
					// transition or just means to
					// memorize the source state and the message event upon
					// which we want to explore a new
					// actual successor state and actual transition in the MSD
					// state space graph.
					// The generation of the target state and the actual
					// transition in the MSD state space graph
					// is done here.
					if (t.getTargetState() == null) {
						RuntimeState sourceState = (RuntimeState) t
								.getSourceState();
						t.setSourceState(null); // must detach temporary
												// transition from source state
												// again!
						// t is now the actual transition with the explored
						// target state.
						t = runtimeStateGraph.generateSuccessor(sourceState,
								t.getEvent());
					}

					RuntimeState targetState = (RuntimeState) t
							.getTargetState();
					RuntimeState sourceState = (RuntimeState) t
							.getSourceState();

					if (!passed.contains(targetState)) { // if the target state
															// was not visited

						System.out
								.println("exploring transition with message event "
										+ messageEvent);

						// mark target state as visited
						passed.add(targetState);

						// remember (for later backward reevaluation) that the
						// winning status of the source state
						// depends on the target state via this transition e.
						addDepend(targetState, t);

						// put the outgoing transitions of the target state on
						// the stack.
						addTransitionsForAllOutgoingConcreteMessageEvents(
								targetState, waiting);

						// if the target state is a goal state or another state
						// marked winning (i.e., for which we
						// think that it could be guaranteed to reach goal
						// states infinitely often), push
						// transition t to the waiting stack, so the source
						// state will be reevaluated in the
						// next iteration of the loop.
						// Do not reevaluate buechi-losing states, i.e. states
						// from where it can not be guaranteed
						// that infinitely often a goal state can be reached
						boolean successorIsGoalOrWinning = isGoal(targetState)
								|| win.contains(targetState);
						if (successorIsGoalOrWinning
								&& !loseBuechi.contains(targetState)) {
							Assert.isTrue(!waiting.contains(t));
							waiting.push(t);
						}
					} else {
						// the status of the target state depends on this source
						// state
						addDepend(targetState, t);

						// can we guarantee reaching a goal or (currently) win
						// successor?
						boolean winStar = canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(sourceState);

						System.out
								.println("reevaluating transition with message event  "
										+ messageEvent);
						System.out
								.println("can guarantee to reach winning or goal successor from source state: "
										+ winStar);

						if (winStar) { // if yes
							// and the source state is not already marked
							// winning, we have to
							// schedule all depending transitions for further
							// backward reevaluation
							if (!win.contains(sourceState)) {
								if (depend.get(sourceState) != null)
									pushOnStackIfNotAlreadyContained(
											depend.get(sourceState), waiting);
								// and mark the source state as winning
								win.add(sourceState);
							}
						}
						/*
						 * else { // If the source state then has no further
						 * unexplored outgoing transitions, we know that // it
						 * is impossible from here to guarantee reaching a
						 * winning state. Thus, we can now mark // this state as
						 * loosing and we schedule all the depending edges for
						 * reevaluation.
						 * if(!hasUnexploredOutgoingTransitions(sourceState,
						 * waiting)){ win.remove(sourceState); // mark it as a
						 * loosing state, schedule depending edges for
						 * reevaluation only // if it is marked loosing for the
						 * first time. if(!lose.contains(sourceState)){
						 * lose.add(sourceState); if(depend.get(sourceState) !=
						 * null){
						 * pushOnStackIfNotAlreadyContained(depend.get(sourceState
						 * ), waiting);
						 * //waiting.addAll(depend.get(sourceState));
						 * 
						 * // this triggers a backward reevaluation for the
						 * start state if (sourceState == startState &&
						 * sourceState != q0){ Transition
						 * principalDependingTransition =
						 * depend.get(startState).get(0);
						 * waiting.remove(principalDependingTransition);
						 * waiting.push(principalDependingTransition);
						 * startState = (RuntimeState)
						 * principalDependingTransition.getSourceState(); } }
						 * }else{
						 * System.out.println("source state already loosing"); }
						 * } }
						 */

						// // this triggers a backward reevaluation for the
						// start state
						// if (lose.contains(startState) &&
						// depend.get(startState) != null &&
						// !depend.get(startState).isEmpty()){
						//
						// printStack(waiting);
						//
						// // go back to the state that was added first (when
						// coming from the original source)
						// Transition principalDependingTransition =
						// depend.get(startState).get(0);
						// waiting.add(principalDependingTransition);
						// startState = (RuntimeState)
						// principalDependingTransition.getSourceState();
						//
						// System.out.println(RuntimeUtil.Helper.getMSDRuntimeStateString((MSDRuntimeState)
						// startState));
						// backward = true;
						// }
					}
				}

				// while these temporary transitions are a good idea to explore
				// successor states in a lazy way,
				// we must ensure that no dangling unexplored transtions remain.
				removeDanglingTemporaryTransitions(waiting);

				return win.contains(startState);
			}

			private void removeDanglingTemporaryTransitions(
					Collection<Transition> transitions) {
				for (Transition transition : transitions) {
					if (transition.getTargetState() == null)
						transition.setSourceState(null);
				}
			}

			private void pushOnStackIfNotAlreadyContained(
					Collection<Transition> transitions,
					Stack<Transition> waiting) {
				for (Transition transition : transitions) {
					if (!waiting.contains(transition)) {
						waiting.push(transition);
					} else {
						System.out.println("already contained ... ");
					}
				}
			}

			private void printStack(Stack<Transition> waiting) {
				String stackString = "stack: ";
				for (Transition transition : waiting) {
					stackString += ((MessageEvent) transition.getEvent())
							.getOperation().getName() + ", ";
					Assert.isTrue(waiting.indexOf(transition,
							waiting.indexOf(transition) + 1) < 0);
				}
				System.out.println(stackString);
			}

			/**
			 * Returns true if 1. at least one controllable outgoing transition
			 * OR 2. all uncontrollable outgoing transitions (if any)
			 * 
			 * lead to a successor which is winning or goal.
			 * 
			 * @param runtimeState
			 * @return
			 */
			private boolean canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(
					RuntimeState runtimeState) {
				boolean stateHasUncontrollableOutgoingTransitions = false;
				boolean hasUnexploredUncontrollableTransition = false;
				boolean stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = false;
				for (Transition outgoingTransition : runtimeState
						.getOutgoingTransition()) {
					if (outgoingTransition.getTargetState() == null) { // it is
																		// a
																		// temporary
																		// transition
						if (!isControllable(runtimeState,
								(MessageEvent) outgoingTransition.getEvent()))
							hasUnexploredUncontrollableTransition = true;
						continue;
					}
					System.out
							.println("isGoal((RuntimeState) outgoingTransition.getTargetState()) "
									+ isGoal((RuntimeState) outgoingTransition
											.getTargetState()));
					System.out
							.println("win.contains(outgoingTransition.getTargetState()) "
									+ win.contains(outgoingTransition
											.getTargetState()));
					System.out
							.println("!loseBuechi.contains(outgoingTransition.getTargetState()) "
									+ !loseBuechi.contains(outgoingTransition
											.getTargetState()));

					// goal or winning, but must not be losing
					boolean targetStateWinningOrGoal = (isGoal((RuntimeState) outgoingTransition
							.getTargetState()) || win
							.contains(outgoingTransition.getTargetState())) && !loseBuechi
							.contains(outgoingTransition.getTargetState());
					if (isControllable(outgoingTransition)) {
						if (targetStateWinningOrGoal)
							return true;
					} else {
						stateHasUncontrollableOutgoingTransitions = true;
						if (!targetStateWinningOrGoal)
							stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = true;
					}
				}
				return stateHasUncontrollableOutgoingTransitions
						&& !stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState
						&& !hasUnexploredUncontrollableTransition;
			}

			/**
			 * 
			 * Generates successors of the given runtime state and pushes the
			 * outgoing transitions on the top of the passed waiting stack. It
			 * is ensured that among the pushed transitions, the controllable
			 * transitions are on the top of the stack.
			 * 
			 * With the parameter
			 * <code>addOnlyTransitionsToNotPassedStates</code> set to TRUE,
			 * only transitions
			 * 
			 * 
			 * @param runtimeState
			 * @param waiting
			 * @param addOnlyTransitionsToNotPassedStates
			 */
			private boolean addTransitionsForAllOutgoingConcreteMessageEvents(
					RuntimeState runtimeState, Stack<Transition> waiting) {
				boolean atLeastOneTransitionWasAdded = false;
				int sizeOfWaitingStack = waiting.size();
				for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
						.getMessageEventToModalMessageEventMap().entrySet()) {
					MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
							.getKey();
					ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
							.getValue();

					if (!messageEvent.isConcrete())
						continue;

					if (isControllable(runtimeState, messageEvent)
							&& !modalMessageEvent.getRequirementsModality()
									.isMandatory())
						continue;

					Transition transition = runtimeState
							.getEventToTransitionMap().get(messageEvent);

					// if(addOnlyTransitionsToNotPassedStates
					// && transition != null
					// && passed.contains(transition.getTargetState())){
					// continue;
					// }

					if (transition == null)
						transition = createTmpTransition(runtimeState,
								messageEvent);

					Assert.isTrue(!waiting.contains(transition));

					if (isControllable(transition)
							&& !modalMessageEvent.getRequirementsModality()
									.isSafetyViolating()) {
						waiting.push(transition);
					} else {
						waiting.add(sizeOfWaitingStack, transition);
					}
					System.out.println("Adding " + messageEvent
							+ " to waiting stack.");
					atLeastOneTransitionWasAdded = true;

				}
				return atLeastOneTransitionWasAdded;
			}

			private Transition createTmpTransition(RuntimeState runtimeState,
					MessageEvent messageEvent) {
				Transition tmpTransition = StategraphFactory.eINSTANCE
						.createTransition();
				tmpTransition.setEvent(messageEvent);
				tmpTransition.setSourceState(runtimeState);
				return tmpTransition;
			}

			private boolean hasMandatoryMessageEvents(
					RuntimeState runtimeState, boolean forAssumptions) {
				for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
						.getMessageEventToModalMessageEventMap().entrySet()) {
					// MessageEvent messageEvent =
					// messageEventToModalMessageEventMapEntry.getKey();
					ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
							.getValue();
					if (forAssumptions) {
						if (modalMessageEvent.getAssumptionsModality()
								.isMandatory())
							return true;
					} else {
						if (modalMessageEvent.getRequirementsModality()
								.isMandatory())
							return true;
					}
				}
				return false;
			}

			private boolean isControllable(RuntimeState runtimeState,
					MessageEvent messageEvent) {
				return runtimeState.getObjectSystem().isControllable(
						messageEvent.getSendingObject());
			}

			private boolean isControllable(Transition transition) {
				return ((RuntimeState) transition.getSourceState())
						.getObjectSystem().isControllable(
								((MessageEvent) transition.getEvent())
										.getSendingObject());
			}

		};

		job.setUser(true);
		job.schedule();
	}

	public static void postExplorationFinished(
			final MSDRuntimeStateGraph msdRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;

				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
				}

				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size());
			}
		});
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
