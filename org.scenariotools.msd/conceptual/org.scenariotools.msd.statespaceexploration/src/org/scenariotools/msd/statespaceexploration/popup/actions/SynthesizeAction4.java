package org.scenariotools.msd.statespaceexploration.popup.actions;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class SynthesizeAction4 implements IObjectActionDelegate {

	
	private Shell shell;

	/**
	 * Constructor for Action1.
	 */
	public SynthesizeAction4() {
		super();
	}
	
	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.getFirstElement();

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(file.getFullPath()
						.toString(), true), true);

		ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource
				.getContents().get(0);

		final MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();

		final MSDRuntimeState msdRuntimeStartState = msdRuntimeStateGraph
				.init(scenarioRunConfiguration);

		final Job job = new Job("MSD state space exploration.") {

			protected IStatus run(IProgressMonitor monitor) {

				Resource scenarioRunConfigurationResource;

				URI scenarioRunConfigurationFileURI = URI
						.createPlatformResourceURI(
								file.getFullPath().toString(), true)
						.trimFileExtension().appendFileExtension("msdruntime");
				try {
					scenarioRunConfigurationResource = resourceSet.getResource(
							scenarioRunConfigurationFileURI, true);
					if (scenarioRunConfigurationResource != null) {
						MessageDialog
								.openError(
										new Shell(),
										"An error occurred while creating the interpreter configuration",
										"There already exists a MSD runtime file at the default location:\n"
												+ scenarioRunConfigurationFileURI
												+ "\n\n"
												+ "Remove it first to create a new one.");
					}

					return Status.CANCEL_STATUS;
				} catch (Exception e) {
					// that's good. Continue.
				}

				long currentTimeMillis = System.currentTimeMillis();

				// canReachAnotherGoalState(msdRuntimeStartState);
				boolean buechiStrategyExists = OTFB(msdRuntimeStartState);
				Set<RuntimeState> winningStates = win;

				System.out.println(winningStates);

				long currentTimeMillisDelta = System.currentTimeMillis()
						- currentTimeMillis;

				// remove reference to RuntimeUtil:
				msdRuntimeStateGraph.setRuntimeUtil(null);
				for (State state : msdRuntimeStateGraph.getStates()) {
					MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
					msdRuntimeState.setRuntimeUtil(null);
				}

				for (ActiveProcess activeProcess : msdRuntimeStateGraph.getElementContainer().getActiveProcesses()) {
					activeProcess.setRuntimeUtil(null);
					if(activeProcess instanceof ActiveMSD)
						((ActiveMSD)activeProcess).setMsdUtil(null);
				}
				for (ActiveState activeState : msdRuntimeStateGraph.getElementContainer().getActiveStates()) {
					activeState.setRuntimeUtil(null);
				}
				for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph
						.getElementContainer().getObjectSystems()) {
					msdObjectSystem.setRuntimeUtil(null);
				}

				scenarioRunConfigurationResource = resourceSet
						.createResource(scenarioRunConfigurationFileURI);
				scenarioRunConfigurationResource.getContents().add(
						msdRuntimeStateGraph);
				try {
					scenarioRunConfigurationResource
							.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				postExplorationFinished(msdRuntimeStateGraph,
						currentTimeMillisDelta, buechiStrategyExists,
						winningStates);

				return Status.OK_STATUS;
			}

			Set<RuntimeState> passed = new HashSet<RuntimeState>();
			Set<RuntimeState> goal = new HashSet<RuntimeState>();
			Set<RuntimeState> win = new HashSet<RuntimeState>();
			Set<RuntimeState> loseBuechi = new HashSet<RuntimeState>();
			Map<RuntimeState, List<Transition>> depend  = new HashMap<RuntimeState, List<Transition>>();
			
			
			private boolean OTFR(RuntimeState startState){
				Stack<Transition> waiting = new Stack<Transition>();

				passed.add(startState);
				addTransitionsForAllOutgoingConcreteMessageEvents(startState, waiting);
				while (!waiting.isEmpty() && !win.contains(startState)) {

					Transition t = waiting.pop();
					if (t.getTargetState() == null)
						t = generateSuccessor(t);
					RuntimeState targetState = (RuntimeState) t.getTargetState();
					RuntimeState sourceState = (RuntimeState) t.getSourceState();
					
					//Assert.isTrue(!win.contains(sourceState));
					if(win.contains(sourceState)) continue;
					
					if(!passed.contains(targetState)){ // fwd exploration
						passed.add(targetState);
						addDepend(targetState, t);
						addTransitionsForAllOutgoingConcreteMessageEvents(
								targetState, waiting);
						if ((isGoal(targetState) || win.contains(targetState))
								&& !loseBuechi.contains(targetState))
							waiting.push(t);
					}else{ // bwd re-evaluation
						addDepend(targetState, t);
						if (canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(sourceState)){
							win.add(sourceState);
							if (depend.get(sourceState) != null)
								waiting.addAll(depend.get(sourceState));
						}	
					}	
				}
				removeDanglingTemporaryTransitions(waiting);
				return win.contains(startState);
			}

			private boolean OTFB(MSDRuntimeState startState) {
				Set<RuntimeState> reevaluate = new HashSet<RuntimeState>();
				if(!OTFR(startState))
					return false;
				Set<RuntimeState> goalAndNotWinOrLoseBuechi = getGoalAndNotWinAndNotLoseBuechiStates();
				while (!goalAndNotWinOrLoseBuechi.isEmpty()) {
					RuntimeState q = goalAndNotWinOrLoseBuechi.iterator().next();
					reevaluate.add(q);
					do{
						RuntimeState q2 = reevaluate.iterator().next();
						reevaluate.remove(q2);
						win.remove(q2);
						if (!OTFR(q2)){
							if (q2 == startState) 
								return false;
							Assert.isTrue(!loseBuechi.contains(q2));
							loseBuechi.add(q2);
							for (Transition t : depend.get(q2)) {
								if (!loseBuechi.contains(t.getSourceState()))
									reevaluate.add((RuntimeState) t.getSourceState());
							}
						}
					}while(!reevaluate.isEmpty());
					goalAndNotWinOrLoseBuechi = getGoalAndNotWinAndNotLoseBuechiStates();
				}
				return true;
			}
			
			
			private Set<RuntimeState> getGoalAndNotWinAndNotLoseBuechiStates(){
				Set<RuntimeState> goalCopy = new HashSet<RuntimeState>(goal);
				goalCopy.removeAll(win);
				goalCopy.removeAll(loseBuechi);
				return goalCopy;
			}

			
			/**
			 * Returns true if 1. at least one controllable outgoing transition
			 * OR 2. all uncontrollable outgoing transitions (if any)
			 * 
			 * lead to a successor which is winning or goal and not loseBuechi
			 * 
			 * @param runtimeState
			 * @return
			 */
			private boolean canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(
					RuntimeState runtimeState) {
				boolean stateHasUncontrollableOutgoingTransitions = false;
				boolean hasUnexploredUncontrollableTransition = false;
				boolean stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState = false;
				for (Transition outgoingTransition : runtimeState
						.getOutgoingTransition()) {
					if (outgoingTransition.getTargetState() == null) { // it is a temporary transition
						if (!isControllable(runtimeState,(MessageEvent) outgoingTransition.getEvent()))
							hasUnexploredUncontrollableTransition = true;
						continue;
					}
					State targetState = outgoingTransition.getTargetState();
//					System.out.println("isGoal((RuntimeState) outgoingTransition.getTargetState()) "
//									+ isGoal((RuntimeState) targetState));
//					System.out.println("win.contains(outgoingTransition.getTargetState()) "
//									+ win.contains(targetState));
//					System.out.println("!loseBuechi.contains(outgoingTransition.getTargetState()) "
//									+ !loseBuechi.contains(targetState));
					// goal or winning, but must not be losing
					boolean targetStateWinningOrGoalAndNotLoseBuechi = 
							(isGoal((RuntimeState) targetState) || win.contains(targetState)) 
								&& !loseBuechi.contains(targetState);
					if (isControllable(outgoingTransition)) {
						if (targetStateWinningOrGoalAndNotLoseBuechi)
							return true;
					} else {
						stateHasUncontrollableOutgoingTransitions = true;
						if (!targetStateWinningOrGoalAndNotLoseBuechi)
							stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState = true;
					}
				}
				return stateHasUncontrollableOutgoingTransitions
						&& !stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState
						&& !hasUnexploredUncontrollableTransition;
			}
			
			private boolean isGoal(RuntimeState q) {
				if (goal.contains(q))
					return true;
				else if (!(q.isSafetyViolationOccurredInRequirements() || hasMandatoryMessageEvents(
						q, false))
						|| q.isSafetyViolationOccurredInAssumptions()
						|| hasMandatoryMessageEvents(q, true)) {
					goal.add(q);
					return true;
				}
				return false;
			}
			
			private void addDepend(RuntimeState q, Transition e) {
				List<Transition> dependingTransitions = depend.get(q);
				if (dependingTransitions == null) {
					dependingTransitions = new UniqueEList<Transition>();
					depend.put(q, dependingTransitions);
				}
				dependingTransitions.add(e);
			}
			
			private Transition generateSuccessor(Transition e) {
				RuntimeState sourceState = (RuntimeState) e.getSourceState();
				e.setSourceState(null); // must detach temporary transition from source state again!
				e = ((RuntimeStateGraph) sourceState.getStateGraph()).generateSuccessor(sourceState, e.getEvent());
				return e;
			}
			
			/**
			 * 
			 * Generates successors of the given runtime state and pushes the
			 * outgoing transitions on the top of the passed waiting stack. It
			 * is ensured that among the pushed transitions, the controllable
			 * transitions are on the top of the stack.
			 * 
			 * With the parameter
			 * <code>addOnlyTransitionsToNotPassedStates</code> set to TRUE,
			 * only transitions
			 * 
			 * 
			 * @param runtimeState
			 * @param waiting
			 * @param addOnlyTransitionsToNotPassedStates
			 */
			private boolean addTransitionsForAllOutgoingConcreteMessageEvents(
					RuntimeState runtimeState, Stack<Transition> waiting) {
				boolean atLeastOneTransitionWasAdded = false;
				int sizeOfWaitingStack = waiting.size();
				for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
						.getMessageEventToModalMessageEventMap().entrySet()) {
					MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
							.getKey();
					ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
							.getValue();

					if (!messageEvent.isConcrete())
						continue;

					if (isControllable(runtimeState, messageEvent)
							&& !modalMessageEvent.getRequirementsModality()
									.isMandatory())
						continue;

					Transition transition = runtimeState
							.getEventToTransitionMap().get(messageEvent);

					// if(addOnlyTransitionsToNotPassedStates
					// && transition != null
					// && passed.contains(transition.getTargetState())){
					// continue;
					// }

					if (transition == null)
						transition = createTmpTransition(runtimeState,
								messageEvent);

					Assert.isTrue(!waiting.contains(transition));

					if (isControllable(transition)
							&& !modalMessageEvent.getRequirementsModality()
									.isSafetyViolating()) {
						waiting.push(transition);
					} else {
						waiting.add(sizeOfWaitingStack, transition);
					}
//					System.out.println("Adding " + messageEvent
//							+ " to waiting stack.");
					atLeastOneTransitionWasAdded = true;

				}
				return atLeastOneTransitionWasAdded;
			}

			private Transition createTmpTransition(RuntimeState runtimeState,
					MessageEvent messageEvent) {
				Transition tmpTransition = StategraphFactory.eINSTANCE
						.createTransition();
				tmpTransition.setEvent(messageEvent);
				tmpTransition.setSourceState(runtimeState);
				return tmpTransition;
			}

			private boolean hasMandatoryMessageEvents(
					RuntimeState runtimeState, boolean forAssumptions) {
				for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
						.getMessageEventToModalMessageEventMap().entrySet()) {
					// MessageEvent messageEvent =
					// messageEventToModalMessageEventMapEntry.getKey();
					ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
							.getValue();
					if (forAssumptions) {
						if (modalMessageEvent.getAssumptionsModality()
								.isMandatory())
							return true;
					} else {
						if (modalMessageEvent.getRequirementsModality()
								.isMandatory())
							return true;
					}
				}
				return false;
			}

			private boolean isControllable(RuntimeState runtimeState,
					MessageEvent messageEvent) {
				return runtimeState.getObjectSystem().isControllable(
						messageEvent.getSendingObject());
			}

			private boolean isControllable(Transition transition) {
				return ((RuntimeState) transition.getSourceState())
						.getObjectSystem().isControllable(
								((MessageEvent) transition.getEvent())
										.getSendingObject());
			}
			
			private void removeDanglingTemporaryTransitions(
					Collection<Transition> transitions) {
				for (Transition transition : transitions) {
					if (transition.getTargetState() == null)
						transition.setSourceState(null);
				}
			}
		};
		
		job.setUser(true);
		job.schedule();
	}
	
	
	public static void postExplorationFinished(
			final MSDRuntimeStateGraph msdRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;

				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
				}

				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size());
			}
		});
	}
}
