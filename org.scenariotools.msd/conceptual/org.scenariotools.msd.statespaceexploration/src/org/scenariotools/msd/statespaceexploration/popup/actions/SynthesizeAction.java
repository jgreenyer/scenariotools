package org.scenariotools.msd.statespaceexploration.popup.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class SynthesizeAction implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public SynthesizeAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

			IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
			final IFile file = (IFile) structuredSelection.getFirstElement();
	
			final ResourceSet resourceSet = new ResourceSetImpl();
			final Resource scenarioRunConfigurationResource = resourceSet.getResource(URI.createPlatformResourceURI(file.getFullPath().toString(), true), true);
			
			ScenarioRunConfiguration scenarioRunConfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource.getContents().get(0);
			
			final MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE.createMSDRuntimeStateGraph();
			
			final MSDRuntimeState msdRuntimeStartState = msdRuntimeStateGraph.init(scenarioRunConfiguration);
			
			final Job job = new Job("MSD state space exploration."){

				private Set<RuntimeState> exploredStates = new HashSet<RuntimeState>();
				
				protected IStatus run(IProgressMonitor monitor) {

					Resource scenarioRunConfigurationResource;
					@SuppressWarnings("null")
					URI scenarioRunConfigurationFileURI = URI.createPlatformResourceURI(file.getFullPath().toString(), true)
					.trimFileExtension().appendFileExtension("msdruntime");
					try {
						scenarioRunConfigurationResource = resourceSet.getResource(scenarioRunConfigurationFileURI, true);
						if (scenarioRunConfigurationResource != null){
							MessageDialog.openError(new Shell(), "An error occurred while creating the interpreter configuration", 
									"There already exists a MSD runtime file at the default location:\n" 
									+ scenarioRunConfigurationFileURI + "\n\n"
									+ "Remove it first to create a new one.");
						}
						
						return Status.CANCEL_STATUS;
					} catch (Exception e) {
						// that's good. Continue.
					}

					
					long currentTimeMillis = System.currentTimeMillis();
					
					//canReachAnotherGoalState(msdRuntimeStartState);
					boolean buechiStrategyExists = buechiWinningStrategyExists(msdRuntimeStartState);
					
					long currentTimeMillisDelta = System.currentTimeMillis() - currentTimeMillis;					

					
					//remove reference to RuntimeUtil:
					msdRuntimeStateGraph.setRuntimeUtil(null);
					for (State state : msdRuntimeStateGraph.getStates()) {
						MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
						msdRuntimeState.setRuntimeUtil(null);
					}
					
					for (ActiveProcess activeProcess : msdRuntimeStateGraph.getElementContainer().getActiveProcesses()) {
						activeProcess.setRuntimeUtil(null);
						if(activeProcess instanceof ActiveMSD)
							((ActiveMSD)activeProcess).setMsdUtil(null);
					}
					for (ActiveState activeState : msdRuntimeStateGraph.getElementContainer().getActiveStates()) {
						activeState.setRuntimeUtil(null);
					}
					for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph.getElementContainer().getObjectSystems()) {
						msdObjectSystem.setRuntimeUtil(null);
					}

					

					scenarioRunConfigurationResource = 
							resourceSet.createResource(scenarioRunConfigurationFileURI);
						scenarioRunConfigurationResource.getContents().add(msdRuntimeStateGraph);
						try {
							scenarioRunConfigurationResource.save(Collections.EMPTY_MAP);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}


		        	postExplorationFinished(msdRuntimeStateGraph, currentTimeMillisDelta, buechiStrategyExists);

					return Status.OK_STATUS;
				}
				
				
				Map<RuntimeState, Boolean> goal = new HashMap<RuntimeState, Boolean>();
				Map<RuntimeState, Boolean> win = new HashMap<RuntimeState, Boolean>();

				private boolean isGoal(RuntimeState runtimeState){
					if(!goal.keySet().contains(runtimeState)){
						if (!runtimeState.isSafetyViolationOccurredInRequirements() && !hasMandatoryMessageEvents(runtimeState)){
							goal.put(runtimeState, true);
						}else{
							goal.put(runtimeState, false);
						}
					}
					return goal.get(runtimeState);
				}
				
				private boolean isWin(RuntimeState runtimeState){
					if(!win.keySet().contains(runtimeState)){
						boolean isGoal = isGoal(runtimeState);
						win.put(runtimeState, isGoal);
						return isGoal;
					}
					return win.get(runtimeState);
				}

				
				private boolean hasMandatoryMessageEvents(RuntimeState runtimeState){
					for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState.getMessageEventToModalMessageEventMap().entrySet()) {
						//MessageEvent messageEvent = messageEventToModalMessageEventMapEntry.getKey();
						ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry.getValue();
						if (modalMessageEvent.getRequirementsModality().isMandatory())
							return true;
					}
					return false;
				}
			
				/**
				 * A winning successor can be reached if
				 * 1. at least one controllable outgoing transition leads to a winning successor OR
				 * 2. there are uncontrollable outgoing transitions and all lead to a winning successor
				 * 
				 * @param runtimeState
				 * @return
				 */
				private boolean canGuaranteeToReachWinningSuccessor(RuntimeState runtimeState){
					boolean stateHasUncontrollableOutgoingTransitions = false;
					boolean stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = false;
					for (Transition outgoingTransition : runtimeState.getOutgoingTransition()) {
						if (outgoingTransition.getTargetState() == null) // may be a temporary transition
							continue; 
						boolean targetStateWinning = isWin((RuntimeState) outgoingTransition.getTargetState());
						if(isControllable(outgoingTransition)){
							if (targetStateWinning) return true;
						}else{
							stateHasUncontrollableOutgoingTransitions = true;
							if (!targetStateWinning) 
								stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = true;
						}
					}
					return stateHasUncontrollableOutgoingTransitions && !stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState;
				}
				
				
				/**
				 * A winning successor can be reached if
				 * 1. at least one controllable outgoing transition leads to a winning successor OR
				 * 2. there are uncontrollable outgoing transitions and all lead to a winning successor
				 * 
				 * the reachable goal states are returned
				 * 
				 * @param runtimeState
				 * @return
				 */
				private Set<Transition> canGuaranteeToReachWinningSuccessor_G(RuntimeState runtimeState){
					
					Set<Transition> reachableWinningTransitions = new HashSet<Transition>();
					
					boolean stateHasUncontrollableOutgoingTransitions = false;
					boolean stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = false;
					for (Transition outgoingTransition : runtimeState.getOutgoingTransition()) {
						if (outgoingTransition.getTargetState() == null) // may be a temporary transition
							continue; 
						RuntimeState targetState = (RuntimeState) outgoingTransition.getTargetState();
						boolean targetStateWinning = isWin(targetState);
						if(isControllable(outgoingTransition)){
							if (targetStateWinning){
								reachableWinningTransitions.clear();
								reachableWinningTransitions.add(outgoingTransition);
								return reachableWinningTransitions;
							}
						}else{
							stateHasUncontrollableOutgoingTransitions = true;
							if (!targetStateWinning) 
								stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState = true;
							else
								reachableWinningTransitions.add(outgoingTransition);
						}
					}
					
					if(!stateHasUncontrollableOutgoingTransitions || stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningState)
						reachableWinningTransitions.clear();
					
					return reachableWinningTransitions;
				}

				
				
				private List<RuntimeState> newGoalStates(Map<RuntimeState, Boolean> goalMap, Map<RuntimeState, Boolean> goalMapWithPotentiallyMoreGoalStates){
					List<RuntimeState> newGoalStates = new ArrayList<RuntimeState>();
					for (Iterator<Entry<RuntimeState, Boolean>> goalMapWithPotentiallyMoreGoalStatesEntrySetIterator = goalMapWithPotentiallyMoreGoalStates.entrySet().iterator(); 
							goalMapWithPotentiallyMoreGoalStatesEntrySetIterator.hasNext();) {
						Entry<RuntimeState, Boolean> goalMapEntry = goalMapWithPotentiallyMoreGoalStatesEntrySetIterator.next();
						if(goalMapEntry.getValue()){
							if(!goalMap.containsKey(goalMapEntry.getKey())
									|| goalMap.get(goalMapEntry.getKey()) == false){
								newGoalStates.add(goalMapEntry.getKey());
							}
						}
					}
					return newGoalStates;
				}
				
				private List<RuntimeState> newGoalStates(List<RuntimeState> goalStates, Map<RuntimeState, Boolean> goalMapWithPotentiallyMoreGoalStates){
					List<RuntimeState> newGoalStates = new ArrayList<RuntimeState>();
					for (Iterator<Entry<RuntimeState, Boolean>> goalMapWithPotentiallyMoreGoalStatesEntrySetIterator = goalMapWithPotentiallyMoreGoalStates.entrySet().iterator(); 
							goalMapWithPotentiallyMoreGoalStatesEntrySetIterator.hasNext();) {
						Entry<RuntimeState, Boolean> goalMapEntry = goalMapWithPotentiallyMoreGoalStatesEntrySetIterator.next();
						if(goalMapEntry.getValue()){
							if(!goalStates.contains(goalMapEntry.getKey())){
								newGoalStates.add(goalMapEntry.getKey());
							}
						}
					}
					return newGoalStates;
				}

				
				private	Map<RuntimeState, Set<RuntimeState>> stateToReachableGoalStatesMap = new HashMap<RuntimeState, Set<RuntimeState>>();
				private void addReachableGoalStatesForState(RuntimeState runtimeState, Set<RuntimeState> goalStates){
					if(stateToReachableGoalStatesMap.get(runtimeState) == null){
						stateToReachableGoalStatesMap.put(runtimeState, new HashSet<RuntimeState>());
					}
					stateToReachableGoalStatesMap.get(runtimeState).addAll(goalStates);
				}
				private void addReachableGoalStateForState(RuntimeState runtimeState, RuntimeState goalState){
					if(stateToReachableGoalStatesMap.get(runtimeState) == null){
						stateToReachableGoalStatesMap.put(runtimeState, new HashSet<RuntimeState>());
					}
					stateToReachableGoalStatesMap.get(runtimeState).add(goalState);
				}

				
				private boolean buechiWinningStrategyExists(RuntimeState startState){
					Set<RuntimeState> initiallyReachableGoalStates = canReachAnotherGoalState_G(startState);
					if(initiallyReachableGoalStates == null)
						return false;
					
					Set<RuntimeState> waiting = new HashSet<RuntimeState>(initiallyReachableGoalStates);
					Set<RuntimeState> goalStatesFromWhereOtherGoalStatesAreReachable = new HashSet<RuntimeState>();
					
					while (!waiting.isEmpty()) {
						RuntimeState reachableGoalState = waiting.iterator().next();
						Set<RuntimeState> furtherReachableGoalStates = canReachAnotherGoalState_G(reachableGoalState);
						if(furtherReachableGoalStates.isEmpty())
							return false;
						else
							goalStatesFromWhereOtherGoalStatesAreReachable.add(reachableGoalState);
						for (RuntimeState furtherReachableGoalState : furtherReachableGoalStates) {
							if(!goalStatesFromWhereOtherGoalStatesAreReachable.contains(furtherReachableGoalState))
								waiting.add(furtherReachableGoalState);
						}
					}
					
					return true;
				}
				
				/**
				 * Another goal state can be can be reached if
				 * 1. at least one controllable outgoing transition leads to a state from where a strategy exists to reach a goal state OR
				 * 2. there are uncontrollable outgoing transitions and all lead to a state from where a strategy exists to reach a goal state
				 * @param runtimeState
				 * @return
				 */
				private boolean canReachAnotherGoalState(RuntimeState runtimeState){
					RuntimeStateGraph runtimeStateGraph = (RuntimeStateGraph) runtimeState.getStateGraph();
					boolean stateHasUncontrollableOutgoingTransitions = false;
					boolean stateHasUncontrollableOutgoingTransitionsLeadingToAStateFromWhereNoStrategyExistsToReachAGoalState = false;
					
					for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState.getMessageEventToModalMessageEventMap().entrySet()) {
						Transition successorTransition = runtimeStateGraph.generateSuccessor(runtimeState, messageEventToModalMessageEventMapEntry.getKey());
						boolean targetStateHasStrategyToReachAGoalState = OTFUR_BPG((RuntimeState) successorTransition.getTargetState()).isEmpty();
						if(targetStateHasStrategyToReachAGoalState)
							addReachableGoalStatesForState(runtimeState, stateToReachableGoalStatesMap.get(successorTransition.getTargetState()));
						//boolean targetStateHasStrategyToReachAGoalState = OTFUR((RuntimeState) successorTransition.getTargetState());
						if (isControllable(successorTransition)) {
							if (targetStateHasStrategyToReachAGoalState)
								return true;
						} else {
							stateHasUncontrollableOutgoingTransitions = true;
							if (!isControllable(successorTransition))
								stateHasUncontrollableOutgoingTransitionsLeadingToAStateFromWhereNoStrategyExistsToReachAGoalState = true;
						}
					}
					System.out.println(stateToReachableGoalStatesMap.get(runtimeState));
					return stateHasUncontrollableOutgoingTransitions && 
							!stateHasUncontrollableOutgoingTransitionsLeadingToAStateFromWhereNoStrategyExistsToReachAGoalState;					
				}
				
				private Set<RuntimeState> canReachAnotherGoalState_G(RuntimeState runtimeState){
					if(canReachAnotherGoalState(runtimeState))
						return stateToReachableGoalStatesMap.get(runtimeState);
					else
						return null;
				}
				
				private boolean OTFUR(RuntimeState startState){
					
					RuntimeStateGraph runtimeStateGraph = (RuntimeStateGraph) startState.getStateGraph();

					// **************
					// INITIALIZATION
					// **************
					Set<RuntimeState> passed = new HashSet<RuntimeState>();
					Stack<Transition> waiting = new Stack<Transition>();
					Map<RuntimeState, Set<Transition>> depend = new HashMap<RuntimeState, Set<Transition>>();
					passed.add(startState);
					addAllOutgoingMessageEvents(startState, waiting);

					// **************
					//     LOOP
					// **************
					while (waiting.size() != 0 && !isWin(startState)) {
						// a transition on the stack may be an already explored transition or just means to
						// memorize the source state and the message event upon which we want to explore a new
						// actual successor state and actual transition in the MSD state space graph.
						// The generation of the target state and the actual transition in the MSD state space graph
						// is done in the subsequent step.
						Transition e = waiting.pop();
						if (e.getTargetState() == null){
							RuntimeState sourceState = (RuntimeState) e.getSourceState();
							e.setSourceState(null); // must detach temporary transition from source state again!
							e = runtimeStateGraph.generateSuccessor(sourceState, e.getEvent());
							//System.out.println("exploring new state");
						}
						System.out.println(e.getEvent());
						RuntimeState targetState = (RuntimeState) e.getTargetState();
						RuntimeState sourceState = (RuntimeState) e.getSourceState();
						if(!passed.contains(targetState)){
							passed.add(targetState);
							if (depend.get(targetState) == null){
								depend.put(targetState, new HashSet<Transition>());
							}
							depend.get(targetState).add(e);
							boolean isGoalTargetState = isGoal(targetState);
							if(isGoalTargetState)
								win.put(targetState, true);
							addAllOutgoingMessageEvents(targetState, waiting);
							if (isWin(targetState))
								waiting.add(e);
						}else{
							boolean winStar = canGuaranteeToReachWinningSuccessor(sourceState);
							if (winStar){
								if (depend.get(sourceState) != null){
									for (Transition transition : depend.get(sourceState)) {
										waiting.push(transition);
									}
								}
								win.put(sourceState, true);
							}
							if(!isWin(targetState)){
								if (depend.get(targetState) == null){
									depend.put(targetState, new HashSet<Transition>());
								}
								depend.get(targetState).add(e);								
							}
						}
					}
					
					for (Transition transition : waiting) {
						if (transition.getTargetState() == null)
							transition.setSourceState(null);
					}
					
					return isWin(startState);
				}

							
				private Set<RuntimeState> OTFUR_BPG(RuntimeState startState){
					
					RuntimeStateGraph runtimeStateGraph = (RuntimeStateGraph) startState.getStateGraph();

					if(isGoal(startState) && stateToReachableGoalStatesMap.get(startState) == null){
						Set<RuntimeState> reachableGoalStateSet = new HashSet<RuntimeState>();
						reachableGoalStateSet.add(startState);
						return reachableGoalStateSet;
					}else if(isWin(startState)){
						return stateToReachableGoalStatesMap.get(startState);
					}

					
					// **************
					// INITIALIZATION
					// **************
					Set<RuntimeState> passed = new HashSet<RuntimeState>();
					Stack<Transition> waiting = new Stack<Transition>();
					Map<RuntimeState, Set<Transition>> depend = new HashMap<RuntimeState, Set<Transition>>();
					passed.add(startState);
					addAllOutgoingMessageEvents(startState, waiting);

					// **************
					//     LOOP
					// **************
					while (waiting.size() != 0 && !isWin(startState)) {
						// a transition on the stack may be an already explored transition or just means to
						// memorize the source state and the message event upon which we want to explore a new
						// actual successor state and actual transition in the MSD state space graph.
						// The generation of the target state and the actual transition in the MSD state space graph
						// is done in the subsequent step.
						Transition e = waiting.pop();
						if (e.getTargetState() == null){
							RuntimeState sourceState = (RuntimeState) e.getSourceState();
							e.setSourceState(null); // must detach temporary transition from source state again!
							e = runtimeStateGraph.generateSuccessor(sourceState, e.getEvent());
							//System.out.println("exploring new state");
						}
						System.out.println(e.getEvent());
						RuntimeState targetState = (RuntimeState) e.getTargetState();
						RuntimeState sourceState = (RuntimeState) e.getSourceState();
						if(!passed.contains(targetState)){
							passed.add(targetState);
							if (depend.get(targetState) == null){
								depend.put(targetState, new HashSet<Transition>());
							}
							depend.get(targetState).add(e);
							boolean isGoalTargetState = isGoal(targetState);
							if(isGoalTargetState)
								win.put(targetState, true);
							addAllOutgoingMessageEvents(targetState, waiting);
							if (isWin(targetState))
								waiting.add(e);
						}else{
							// compute winning states that are guaranteed to be reachable
							Set<Transition> reachableWinningTransitions = canGuaranteeToReachWinningSuccessor_G(sourceState);
							// if there is such a state, the source state is winning, too.
							boolean winStar = !reachableWinningTransitions.isEmpty();
							if (winStar){
								if (depend.get(sourceState) != null){
									// schedule transitions to depending states for reevaluation
									for (Transition transition : depend.get(sourceState)) {
										waiting.push(transition);
									}
								}
								// set source state to be a winning state.
								win.put(sourceState, true);
								// for the source state, remember the goal states that are guaranteed to be reached.
								for (Transition reachableWinningTransition : reachableWinningTransitions) {
									Set<RuntimeState> runtimeStatesReachableFromTargetState = stateToReachableGoalStatesMap.get(reachableWinningTransition.getTargetState());
									if (runtimeStatesReachableFromTargetState == null){
										Assert.isTrue(goal.get(reachableWinningTransition.getTargetState()));
										addReachableGoalStateForState(sourceState, (RuntimeState) reachableWinningTransition.getTargetState());										
									}else{
										addReachableGoalStatesForState(sourceState, runtimeStatesReachableFromTargetState);										
									}
								}
							}
							if(!isWin(targetState)){
								if (depend.get(targetState) == null){
									depend.put(targetState, new HashSet<Transition>());
								}
								depend.get(targetState).add(e);								
							}
						}
					}
					
					for (Transition transition : waiting) {
						if (transition.getTargetState() == null)
							transition.setSourceState(null);
					}
					
					Assert.isTrue(win.get(startState) == !stateToReachableGoalStatesMap.get(startState).isEmpty());
					
					return stateToReachableGoalStatesMap.get(startState);
				}
				
				/**
				 * Generate successors of the given runtime state and push outgoing transitions onto the
				 * stack.
				 * Controllable transitions are pushed last, 
				 * 
				 * @param runtimeState
				 * @param waiting
				 */
				private void addAllOutgoingMessageEvents(RuntimeState runtimeState, Stack<Transition> waiting){
					//RuntimeStateGraph runtimeStateGraph = (RuntimeStateGraph) runtimeState.getStateGraph();
					//runtimeStateGraph.generateAllSuccessors(runtimeState);
					int sizeOfWaitingStack = waiting.size();
					for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState.getMessageEventToModalMessageEventMap().entrySet()) {
						MessageEvent messageEvent = messageEventToModalMessageEventMapEntry.getKey();
						ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry.getValue();
						Transition tmpTransition = createTmpTransition(runtimeState, messageEvent);
						if (isControllable(tmpTransition) && !modalMessageEvent.getRequirementsModality().isSafetyViolating()){
							waiting.push(tmpTransition);
						}else{
							waiting.add(sizeOfWaitingStack, tmpTransition);
						}
					}
					
				}
				
				private Transition createTmpTransition(RuntimeState runtimeState, MessageEvent messageEvent){
					Transition tmpTransition = StategraphFactory.eINSTANCE.createTransition();
					tmpTransition.setEvent(messageEvent);
					tmpTransition.setSourceState(runtimeState);
					return tmpTransition;
				}

				
				private boolean isControllable(RuntimeState runtimeState, MessageEvent messageEvent){
					return runtimeState.getObjectSystem().isControllable(messageEvent.getSendingObject());
				}
				private boolean isControllable(Transition transition){
					return ((RuntimeState)transition.getSourceState()).getObjectSystem().isControllable(((MessageEvent)transition.getEvent()).getSendingObject());
				}
				
			};
						
		    job.setUser(true);
		    job.schedule();
	}

	public static void postExplorationFinished(final MSDRuntimeStateGraph msdRuntimeStateGraph, final long currentTimeMillisDelta, final boolean buechiStrategyExists){
		Display.getDefault().asyncExec (new Runnable() {
			@Override
			public void run() {
				
				int numberOfStates = 0;
				int numberOfTransitions = 0;
				
				for (State state : msdRuntimeStateGraph.getStates()) {
					numberOfStates++;
					for (Transition transition : state.getOutgoingTransition()) {
						numberOfTransitions++;
					}
				}
				
				MessageDialog.openInformation(
						new Shell(),
						"B�chi strategy exists: " + (buechiStrategyExists ? "TRUE " : "FALSE "),
						"Explore State Space was executed, " + numberOfStates + " states were explored, " + numberOfTransitions + " transitions were created.\n" +
								"Time taken: " + (currentTimeMillisDelta) + " milliseconds.");
			}
		});
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
