/**
 */
package org.scenariotools.msd.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.runtime.provider.ModalMessageEventItemProvider;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.runtime.MSDModalMessageEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MSDModalMessageEventItemProvider
	extends ModalMessageEventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDModalMessageEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEnabledInActiveProcessPropertyDescriptor(object);
			addColdViolatingInActiveProcessPropertyDescriptor(object);
			addSafetyViolatingInActiveProcessPropertyDescriptor(object);
			addParentSymbolicMSDModalMessageEventPropertyDescriptor(object);
			addConcreteMSDModalMessageEventPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Enabled In Active Process feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnabledInActiveProcessPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDModalMessageEvent_enabledInActiveProcess_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDModalMessageEvent_enabledInActiveProcess_feature", "_UI_MSDModalMessageEvent_type"),
				 RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cold Violating In Active Process feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColdViolatingInActiveProcessPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDModalMessageEvent_coldViolatingInActiveProcess_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDModalMessageEvent_coldViolatingInActiveProcess_feature", "_UI_MSDModalMessageEvent_type"),
				 RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Safety Violating In Active Process feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyViolatingInActiveProcessPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDModalMessageEvent_safetyViolatingInActiveProcess_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDModalMessageEvent_safetyViolatingInActiveProcess_feature", "_UI_MSDModalMessageEvent_type"),
				 RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parent Symbolic MSD Modal Message Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParentSymbolicMSDModalMessageEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDModalMessageEvent_parentSymbolicMSDModalMessageEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDModalMessageEvent_parentSymbolicMSDModalMessageEvent_feature", "_UI_MSDModalMessageEvent_type"),
				 RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Concrete MSD Modal Message Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcreteMSDModalMessageEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDModalMessageEvent_concreteMSDModalMessageEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDModalMessageEvent_concreteMSDModalMessageEvent_feature", "_UI_MSDModalMessageEvent_type"),
				 RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns MSDModalMessageEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MSDModalMessageEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_MSDModalMessageEvent_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(org.scenariotools.runtime.RuntimePackage.Literals.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY,
				 RuntimeFactory.eINSTANCE.createMSDModality()));

		newChildDescriptors.add
			(createChildParameter
				(org.scenariotools.runtime.RuntimePackage.Literals.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY,
				 RuntimeFactory.eINSTANCE.createMSDModality()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == org.scenariotools.runtime.RuntimePackage.Literals.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY ||
			childFeature == org.scenariotools.runtime.RuntimePackage.Literals.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
