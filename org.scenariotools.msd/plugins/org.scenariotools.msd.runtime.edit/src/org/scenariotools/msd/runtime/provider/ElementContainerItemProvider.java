/**
 */
package org.scenariotools.msd.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.runtime.ElementContainer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementContainerItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESSES);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATES);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEMS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ElementContainer.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ElementContainer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ElementContainer_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ElementContainer.class)) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESSES,
				 RuntimeFactory.eINSTANCE.createActiveProcess()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESSES,
				 RuntimeFactory.eINSTANCE.createActiveMSS()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESSES,
				 RuntimeFactory.eINSTANCE.createActiveMSD()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS,
				 RuntimeFactory.eINSTANCE.createActiveMSDVariableValuations()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATES,
				 RuntimeFactory.eINSTANCE.createActiveState()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATES,
				 RuntimeFactory.eINSTANCE.createActiveMSDCut()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATES,
				 RuntimeFactory.eINSTANCE.createActiveMSSState()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS,
				 RuntimeFactory.eINSTANCE.createActiveMSDLifelineBindings()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS,
				 RuntimeFactory.eINSTANCE.createActiveMSSRoleBindings()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEMS,
				 RuntimeFactory.eINSTANCE.createMSDObjectSystem()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createInteractionUse()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createPartDecomposition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createInteractionOperand()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createOccurrenceSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createActionExecutionSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createBehaviorExecutionSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createCombinedFragment()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createConsiderIgnoreFragment()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createContinuation()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createMessageOccurrenceSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createDestructionOccurrenceSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createExecutionOccurrenceSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__FINAL_FRAGMENT,
				 UMLFactory.eINSTANCE.createStateInvariant()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS,
				 RuntimeFactory.eINSTANCE.createActiveMSSRoleBindings()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY)));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS ||
			childFeature == RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
