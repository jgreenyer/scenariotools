/**
 */
package org.scenariotools.msd.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsFactory;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.runtime.provider.ObjectSystemItemProvider;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.runtime.MSDObjectSystem} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MSDObjectSystemItemProvider
	extends ObjectSystemItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDObjectSystemItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRuntimeUtilPropertyDescriptor(object);
			addAssumptionMSDInitializingMessageEventsPropertyDescriptor(object);
			addRequirementMSDInitializingMessageEventsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Runtime Util feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuntimeUtilPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDObjectSystem_runtimeUtil_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDObjectSystem_runtimeUtil_feature", "_UI_MSDObjectSystem_type"),
				 RuntimePackage.Literals.MSD_OBJECT_SYSTEM__RUNTIME_UTIL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Assumption MSD Initializing Message Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAssumptionMSDInitializingMessageEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDObjectSystem_assumptionMSDInitializingMessageEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDObjectSystem_assumptionMSDInitializingMessageEvents_feature", "_UI_MSDObjectSystem_type"),
				 RuntimePackage.Literals.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Requirement MSD Initializing Message Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequirementMSDInitializingMessageEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSDObjectSystem_requirementMSDInitializingMessageEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSDObjectSystem_requirementMSDInitializingMessageEvents_feature", "_UI_MSDObjectSystem_type"),
				 RuntimePackage.Literals.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER);
			childrenFeatures.add(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MSDObjectSystem.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MSDObjectSystem"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_MSDObjectSystem_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MSDObjectSystem.class)) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS,
				 RuntimeFactory.eINSTANCE.createMSDModalMessageEvent()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS,
				 RuntimeFactory.eINSTANCE.createMSDModalMessageEvent()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.EOBJECT_TO_EOPERATION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.EOBJECT_TO_EOPERATION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ECLASS_TO_EOBJECT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.EOBJECT_TO_UML_CLASS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.LIFELINE_TO_EOBJECT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.PROPERTY_TO_EOBJECT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER,
				 Roles2eobjectsFactory.eINSTANCE.createRoleToEObjectMappingContainer()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER,
				 RuntimeFactory.eINSTANCE.createMessageEventContainer()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY)));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS ||
			childFeature == RuntimePackage.Literals.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS ||
			childFeature == RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP ||
			childFeature == RuntimePackage.Literals.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
