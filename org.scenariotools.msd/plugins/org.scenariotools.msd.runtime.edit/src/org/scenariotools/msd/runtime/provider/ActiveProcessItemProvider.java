/**
 */
package org.scenariotools.msd.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.runtime.ActiveProcess} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActiveProcessItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveProcessItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVariableValuationsPropertyDescriptor(object);
			addRuntimeUtilPropertyDescriptor(object);
			addCurrentStatePropertyDescriptor(object);
			addObjectSystemPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Variable Valuations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVariableValuationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActiveProcess_variableValuations_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActiveProcess_variableValuations_feature", "_UI_ActiveProcess_type"),
				 RuntimePackage.Literals.ACTIVE_PROCESS__VARIABLE_VALUATIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Runtime Util feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuntimeUtilPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActiveProcess_runtimeUtil_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActiveProcess_runtimeUtil_feature", "_UI_ActiveProcess_type"),
				 RuntimePackage.Literals.ACTIVE_PROCESS__RUNTIME_UTIL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Current State feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActiveProcess_currentState_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActiveProcess_currentState_feature", "_UI_ActiveProcess_type"),
				 RuntimePackage.Literals.ACTIVE_PROCESS__CURRENT_STATE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Object System feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectSystemPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActiveProcess_objectSystem_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActiveProcess_objectSystem_feature", "_UI_ActiveProcess_type"),
				 RuntimePackage.Literals.ACTIVE_PROCESS__OBJECT_SYSTEM,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActiveProcess_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActiveProcess_id_feature", "_UI_ActiveProcess_type"),
				 RuntimePackage.Literals.ACTIVE_PROCESS__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PROCESS__VIOLATING_EVENTS);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PROCESS__ENABLED_EVENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ActiveProcess.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ActiveProcess"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ActiveProcess)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_ActiveProcess_type") :
			getString("_UI_ActiveProcess_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ActiveProcess.class)) {
			case RuntimePackage.ACTIVE_PROCESS__ID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PROCESS__VIOLATING_EVENTS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PROCESS__ENABLED_EVENTS,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY)));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RuntimePackage.Literals.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS ||
			childFeature == RuntimePackage.Literals.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS ||
			childFeature == RuntimePackage.Literals.ACTIVE_PROCESS__VIOLATING_EVENTS ||
			childFeature == RuntimePackage.Literals.ACTIVE_PROCESS__ENABLED_EVENTS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
