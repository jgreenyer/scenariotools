/**
 */
package org.scenariotools.msd.util.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.runtime.provider.RuntimeEditPlugin;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.util.UML2EcoreMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UML2EcoreMappingItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UML2EcoreMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRootUMLPackageToEPackagePropertyDescriptor(object);
			addMergedUMLPackagesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Root UML Package To EPackage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootUMLPackageToEPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UML2EcoreMapping_rootUMLPackageToEPackage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UML2EcoreMapping_rootUMLPackageToEPackage_feature", "_UI_UML2EcoreMapping_type"),
				 UtilPackage.Literals.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged UML Packages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMergedUMLPackagesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UML2EcoreMapping_mergedUMLPackages_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UML2EcoreMapping_mergedUMLPackages_feature", "_UI_UML2EcoreMapping_type"),
				 UtilPackage.Literals.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP);
			childrenFeatures.add(UtilPackage.Literals.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UML2EcoreMapping.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UML2EcoreMapping"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_UML2EcoreMapping_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UML2EcoreMapping.class)) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.UML_CLASS_TO_ECLASS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.ECLASS_TO_UML_CLASS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.EOPERATION2_UML_OPERATION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.UML_OPERATION_TO_EOPERATION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
