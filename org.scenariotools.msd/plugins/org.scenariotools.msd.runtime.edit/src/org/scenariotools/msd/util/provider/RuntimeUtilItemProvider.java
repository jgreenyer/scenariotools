/**
 */
package org.scenariotools.msd.util.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.runtime.provider.RuntimeEditPlugin;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.util.RuntimeUtil} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeUtilItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtilItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRootUMLPackageToEPackagePropertyDescriptor(object);
			addSystemClassesPropertyDescriptor(object);
			addEnvironmentClassesPropertyDescriptor(object);
			addUmlPackagesPropertyDescriptor(object);
			addInteractionsPropertyDescriptor(object);
			addIntegratedPackagePropertyDescriptor(object);
			addEnvironmentSendableMessagesPropertyDescriptor(object);
			addSystemSendableMessagesPropertyDescriptor(object);
			addUseCaseSpecificationsToBeConsideredPropertyDescriptor(object);
			addOclRegistryPropertyDescriptor(object);
			addMsdRuntimeStateGraphPropertyDescriptor(object);
			addFinalFragmentPropertyDescriptor(object);
			addStateMachinesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Root UML Package To EPackage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootUMLPackageToEPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_rootUMLPackageToEPackage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_rootUMLPackageToEPackage_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the System Classes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystemClassesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_systemClasses_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_systemClasses_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__SYSTEM_CLASSES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Environment Classes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnvironmentClassesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_environmentClasses_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_environmentClasses_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__ENVIRONMENT_CLASSES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Uml Packages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUmlPackagesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_umlPackages_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_umlPackages_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__UML_PACKAGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interactions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInteractionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_interactions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_interactions_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__INTERACTIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}


	/**
	 * This adds a property descriptor for the Integrated Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegratedPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_integratedPackage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_integratedPackage_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__INTEGRATED_PACKAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}


	/**
	 * This adds a property descriptor for the Environment Sendable Messages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnvironmentSendableMessagesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_environmentSendableMessages_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_environmentSendableMessages_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the System Sendable Messages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystemSendableMessagesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_systemSendableMessages_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_systemSendableMessages_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Case Specifications To Be Considered feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseCaseSpecificationsToBeConsideredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_useCaseSpecificationsToBeConsidered_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_useCaseSpecificationsToBeConsidered_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ocl Registry feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOclRegistryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_oclRegistry_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_oclRegistry_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__OCL_REGISTRY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Msd Runtime State Graph feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMsdRuntimeStateGraphPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_msdRuntimeStateGraph_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_msdRuntimeStateGraph_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Final Fragment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFinalFragmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_finalFragment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_finalFragment_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__FINAL_FRAGMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the State Machines feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStateMachinesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeUtil_stateMachines_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeUtil_stateMachines_feature", "_UI_RuntimeUtil_type"),
				 UtilPackage.Literals.RUNTIME_UTIL__STATE_MACHINES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__UML2ECORE_MAPPING);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP);
			childrenFeatures.add(UtilPackage.Literals.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns RuntimeUtil.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RuntimeUtil"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_RuntimeUtil_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RuntimeUtil.class)) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__UML2ECORE_MAPPING,
				 UtilFactory.eINSTANCE.createUML2EcoreMapping()));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.PACKAGE_TO_COLLABORATION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.INTERACTION_TO_MSD_UTIL_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(UtilPackage.Literals.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP,
				 UtilFactory.eINSTANCE.create(UtilPackage.Literals.STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
