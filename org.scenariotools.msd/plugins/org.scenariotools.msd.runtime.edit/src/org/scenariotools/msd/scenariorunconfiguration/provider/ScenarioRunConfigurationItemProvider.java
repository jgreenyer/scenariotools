/**
 */
package org.scenariotools.msd.scenariorunconfiguration.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.provider.RuntimeEditPlugin;
import org.scenariotools.msd.scenariorunconfiguration.RuntimeMode;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenarioRunConfigurationItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfigurationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addUml2EcoreMappingPropertyDescriptor(object);
			addSimulationRootObjectsPropertyDescriptor(object);
			addStaticModelRootObjectsPropertyDescriptor(object);
			addRoleToEObjectMappingContainerPropertyDescriptor(object);
			addUseCaseSpecificationsToBeConsideredPropertyDescriptor(object);
			addMsdRuntimeStateGraphPropertyDescriptor(object);
			addExecutionSemanticsPropertyDescriptor(object);
			addStoreAdditionalMSDModalMessageEventDataPropertyDescriptor(object);
			addDynamicObjectSystemPropertyDescriptor(object);
			addOnlyCommunicateViaLinksPropertyDescriptor(object);
			addCommunicationLinksPropertyDescriptor(object);
			addOnlySyncViaCommunicationLinksPropertyDescriptor(object);
			addStrategyPackagePropertyDescriptor(object);
			addHandleRealTimeInconsistenciesConservativelyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Uml2 Ecore Mapping feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUml2EcoreMappingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_uml2EcoreMapping_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_uml2EcoreMapping_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Simulation Root Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimulationRootObjectsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_simulationRootObjects_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_simulationRootObjects_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Static Model Root Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStaticModelRootObjectsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_staticModelRootObjects_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_staticModelRootObjects_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Role To EObject Mapping Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRoleToEObjectMappingContainerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_roleToEObjectMappingContainer_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_roleToEObjectMappingContainer_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Case Specifications To Be Considered feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseCaseSpecificationsToBeConsideredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_useCaseSpecificationsToBeConsidered_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_useCaseSpecificationsToBeConsidered_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Msd Runtime State Graph feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMsdRuntimeStateGraphPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_msdRuntimeStateGraph_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_msdRuntimeStateGraph_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Semantics feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionSemanticsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_executionSemantics_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_executionSemantics_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Store Additional MSD Modal Message Event Data feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStoreAdditionalMSDModalMessageEventDataPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_storeAdditionalMSDModalMessageEventData_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_storeAdditionalMSDModalMessageEventData_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Dynamic Object System feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDynamicObjectSystemPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_dynamicObjectSystem_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_dynamicObjectSystem_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Only Communicate Via Links feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOnlyCommunicateViaLinksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_onlyCommunicateViaLinks_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_onlyCommunicateViaLinks_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Communication Links feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCommunicationLinksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_communicationLinks_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_communicationLinks_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Only Sync Via Communication Links feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOnlySyncViaCommunicationLinksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_onlySyncViaCommunicationLinks_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_onlySyncViaCommunicationLinks_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Strategy Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrategyPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_strategyPackage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_strategyPackage_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Handle Real Time Inconsistencies Conservatively feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHandleRealTimeInconsistenciesConservativelyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ScenarioRunConfiguration_handleRealTimeInconsistenciesConservatively_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ScenarioRunConfiguration_handleRealTimeInconsistenciesConservatively_feature", "_UI_ScenarioRunConfiguration_type"),
				 ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ScenarioRunConfiguration.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ScenarioRunConfiguration"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ExecutionSemantics labelValue = ((ScenarioRunConfiguration)object).getExecutionSemantics();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_ScenarioRunConfiguration_type") :
			getString("_UI_ScenarioRunConfiguration_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ScenarioRunConfiguration.class)) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS:
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA:
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM:
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS:
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS:
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS,
				 ScenariorunconfigurationFactory.eINSTANCE.createCommunicationLink()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
