/*
 * generated by Xtext
 */
package org.scenariotools.msd.xtext.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import org.scenariotools.msd.xtext.ui.internal.UmlActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class UmlExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return UmlActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return UmlActivator.getInstance().getInjector(UmlActivator.ORG_SCENARIOTOOLS_MSD_XTEXT_UML);
	}
	
}
