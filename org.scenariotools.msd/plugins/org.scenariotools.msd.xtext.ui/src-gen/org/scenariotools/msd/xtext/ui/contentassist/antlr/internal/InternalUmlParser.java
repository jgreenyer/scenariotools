package org.scenariotools.msd.xtext.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.msd.xtext.services.UmlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUmlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'assert'", "'sec'", "'alt'", "'opt'", "'break'", "'par'", "'strict'", "'loop'", "'critical'", "'neg'", "'ignore'", "'consider'", "'asynchCall'", "'synchCall'", "'createMessage'", "'deleteMessage'", "'reply'", "'asynchSignal'", "'package'", "'{'", "'}'", "'class'", "'operation'", "';'", "'attribute'", "'('", "','", "')'", "'type='", "'collaboration'", "'ownedBehavior'", "'combinedFragment'", "'covered='", "'comment'", "'interactionOperand'", "'interactionConstraint'", "'lifeline'", "'represents='", "'message'", "'receive='", "'send='", "'signature='", "'messageOccurrenceSpecification'", "'.'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUmlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUmlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUmlParser.tokenNames; }
    public String getGrammarFileName() { return "../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g"; }


     
     	private UmlGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(UmlGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:61:1: ( ruleModel EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:69:1: ruleModel : ( ( rule__Model__PackagedElementAssignment ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:73:2: ( ( ( rule__Model__PackagedElementAssignment ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:74:1: ( ( rule__Model__PackagedElementAssignment ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:74:1: ( ( rule__Model__PackagedElementAssignment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:75:1: ( rule__Model__PackagedElementAssignment )
            {
             before(grammarAccess.getModelAccess().getPackagedElementAssignment()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:76:1: ( rule__Model__PackagedElementAssignment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:76:2: rule__Model__PackagedElementAssignment
            {
            pushFollow(FOLLOW_rule__Model__PackagedElementAssignment_in_ruleModel94);
            rule__Model__PackagedElementAssignment();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getPackagedElementAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRulePackage"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:88:1: entryRulePackage : rulePackage EOF ;
    public final void entryRulePackage() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:89:1: ( rulePackage EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:90:1: rulePackage EOF
            {
             before(grammarAccess.getPackageRule()); 
            pushFollow(FOLLOW_rulePackage_in_entryRulePackage121);
            rulePackage();

            state._fsp--;

             after(grammarAccess.getPackageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePackage128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePackage"


    // $ANTLR start "rulePackage"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:97:1: rulePackage : ( ( rule__Package__Group__0 ) ) ;
    public final void rulePackage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:101:2: ( ( ( rule__Package__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:102:1: ( ( rule__Package__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:102:1: ( ( rule__Package__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:103:1: ( rule__Package__Group__0 )
            {
             before(grammarAccess.getPackageAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:104:1: ( rule__Package__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:104:2: rule__Package__Group__0
            {
            pushFollow(FOLLOW_rule__Package__Group__0_in_rulePackage154);
            rule__Package__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPackageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePackage"


    // $ANTLR start "entryRulePackageableElement"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:116:1: entryRulePackageableElement : rulePackageableElement EOF ;
    public final void entryRulePackageableElement() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:117:1: ( rulePackageableElement EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:118:1: rulePackageableElement EOF
            {
             before(grammarAccess.getPackageableElementRule()); 
            pushFollow(FOLLOW_rulePackageableElement_in_entryRulePackageableElement181);
            rulePackageableElement();

            state._fsp--;

             after(grammarAccess.getPackageableElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePackageableElement188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePackageableElement"


    // $ANTLR start "rulePackageableElement"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:125:1: rulePackageableElement : ( ( rule__PackageableElement__Alternatives ) ) ;
    public final void rulePackageableElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:129:2: ( ( ( rule__PackageableElement__Alternatives ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:130:1: ( ( rule__PackageableElement__Alternatives ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:130:1: ( ( rule__PackageableElement__Alternatives ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:131:1: ( rule__PackageableElement__Alternatives )
            {
             before(grammarAccess.getPackageableElementAccess().getAlternatives()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:132:1: ( rule__PackageableElement__Alternatives )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:132:2: rule__PackageableElement__Alternatives
            {
            pushFollow(FOLLOW_rule__PackageableElement__Alternatives_in_rulePackageableElement214);
            rule__PackageableElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPackageableElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePackageableElement"


    // $ANTLR start "entryRuleClass"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:144:1: entryRuleClass : ruleClass EOF ;
    public final void entryRuleClass() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:145:1: ( ruleClass EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:146:1: ruleClass EOF
            {
             before(grammarAccess.getClassRule()); 
            pushFollow(FOLLOW_ruleClass_in_entryRuleClass241);
            ruleClass();

            state._fsp--;

             after(grammarAccess.getClassRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleClass248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:153:1: ruleClass : ( ( rule__Class__Group__0 ) ) ;
    public final void ruleClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:157:2: ( ( ( rule__Class__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:158:1: ( ( rule__Class__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:158:1: ( ( rule__Class__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:159:1: ( rule__Class__Group__0 )
            {
             before(grammarAccess.getClassAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:160:1: ( rule__Class__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:160:2: rule__Class__Group__0
            {
            pushFollow(FOLLOW_rule__Class__Group__0_in_ruleClass274);
            rule__Class__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleOperation"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:172:1: entryRuleOperation : ruleOperation EOF ;
    public final void entryRuleOperation() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:173:1: ( ruleOperation EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:174:1: ruleOperation EOF
            {
             before(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_ruleOperation_in_entryRuleOperation301);
            ruleOperation();

            state._fsp--;

             after(grammarAccess.getOperationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOperation308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:181:1: ruleOperation : ( ( rule__Operation__Group__0 ) ) ;
    public final void ruleOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:185:2: ( ( ( rule__Operation__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:186:1: ( ( rule__Operation__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:186:1: ( ( rule__Operation__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:187:1: ( rule__Operation__Group__0 )
            {
             before(grammarAccess.getOperationAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:188:1: ( rule__Operation__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:188:2: rule__Operation__Group__0
            {
            pushFollow(FOLLOW_rule__Operation__Group__0_in_ruleOperation334);
            rule__Operation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleProperty"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:200:1: entryRuleProperty : ruleProperty EOF ;
    public final void entryRuleProperty() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:201:1: ( ruleProperty EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:202:1: ruleProperty EOF
            {
             before(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_ruleProperty_in_entryRuleProperty361);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getPropertyRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProperty368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:209:1: ruleProperty : ( ( rule__Property__Group__0 ) ) ;
    public final void ruleProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:213:2: ( ( ( rule__Property__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:214:1: ( ( rule__Property__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:214:1: ( ( rule__Property__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:215:1: ( rule__Property__Group__0 )
            {
             before(grammarAccess.getPropertyAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:216:1: ( rule__Property__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:216:2: rule__Property__Group__0
            {
            pushFollow(FOLLOW_rule__Property__Group__0_in_ruleProperty394);
            rule__Property__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleValueSpecification"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:228:1: entryRuleValueSpecification : ruleValueSpecification EOF ;
    public final void entryRuleValueSpecification() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:229:1: ( ruleValueSpecification EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:230:1: ruleValueSpecification EOF
            {
             before(grammarAccess.getValueSpecificationRule()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_entryRuleValueSpecification421);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getValueSpecificationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleValueSpecification428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValueSpecification"


    // $ANTLR start "ruleValueSpecification"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:237:1: ruleValueSpecification : ( ( rule__ValueSpecification__Alternatives ) ) ;
    public final void ruleValueSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:241:2: ( ( ( rule__ValueSpecification__Alternatives ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:242:1: ( ( rule__ValueSpecification__Alternatives ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:242:1: ( ( rule__ValueSpecification__Alternatives ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:243:1: ( rule__ValueSpecification__Alternatives )
            {
             before(grammarAccess.getValueSpecificationAccess().getAlternatives()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:244:1: ( rule__ValueSpecification__Alternatives )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:244:2: rule__ValueSpecification__Alternatives
            {
            pushFollow(FOLLOW_rule__ValueSpecification__Alternatives_in_ruleValueSpecification454);
            rule__ValueSpecification__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueSpecificationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValueSpecification"


    // $ANTLR start "entryRuleLiteralUnlimitedNatural"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:258:1: entryRuleLiteralUnlimitedNatural : ruleLiteralUnlimitedNatural EOF ;
    public final void entryRuleLiteralUnlimitedNatural() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:259:1: ( ruleLiteralUnlimitedNatural EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:260:1: ruleLiteralUnlimitedNatural EOF
            {
             before(grammarAccess.getLiteralUnlimitedNaturalRule()); 
            pushFollow(FOLLOW_ruleLiteralUnlimitedNatural_in_entryRuleLiteralUnlimitedNatural483);
            ruleLiteralUnlimitedNatural();

            state._fsp--;

             after(grammarAccess.getLiteralUnlimitedNaturalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteralUnlimitedNatural490); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteralUnlimitedNatural"


    // $ANTLR start "ruleLiteralUnlimitedNatural"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:267:1: ruleLiteralUnlimitedNatural : ( ( rule__LiteralUnlimitedNatural__ValueAssignment ) ) ;
    public final void ruleLiteralUnlimitedNatural() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:271:2: ( ( ( rule__LiteralUnlimitedNatural__ValueAssignment ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:272:1: ( ( rule__LiteralUnlimitedNatural__ValueAssignment ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:272:1: ( ( rule__LiteralUnlimitedNatural__ValueAssignment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:273:1: ( rule__LiteralUnlimitedNatural__ValueAssignment )
            {
             before(grammarAccess.getLiteralUnlimitedNaturalAccess().getValueAssignment()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:274:1: ( rule__LiteralUnlimitedNatural__ValueAssignment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:274:2: rule__LiteralUnlimitedNatural__ValueAssignment
            {
            pushFollow(FOLLOW_rule__LiteralUnlimitedNatural__ValueAssignment_in_ruleLiteralUnlimitedNatural516);
            rule__LiteralUnlimitedNatural__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLiteralUnlimitedNaturalAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteralUnlimitedNatural"


    // $ANTLR start "entryRuleLiteralString"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:286:1: entryRuleLiteralString : ruleLiteralString EOF ;
    public final void entryRuleLiteralString() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:287:1: ( ruleLiteralString EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:288:1: ruleLiteralString EOF
            {
             before(grammarAccess.getLiteralStringRule()); 
            pushFollow(FOLLOW_ruleLiteralString_in_entryRuleLiteralString543);
            ruleLiteralString();

            state._fsp--;

             after(grammarAccess.getLiteralStringRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteralString550); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteralString"


    // $ANTLR start "ruleLiteralString"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:295:1: ruleLiteralString : ( ( rule__LiteralString__ValueAssignment ) ) ;
    public final void ruleLiteralString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:299:2: ( ( ( rule__LiteralString__ValueAssignment ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:300:1: ( ( rule__LiteralString__ValueAssignment ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:300:1: ( ( rule__LiteralString__ValueAssignment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:301:1: ( rule__LiteralString__ValueAssignment )
            {
             before(grammarAccess.getLiteralStringAccess().getValueAssignment()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:302:1: ( rule__LiteralString__ValueAssignment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:302:2: rule__LiteralString__ValueAssignment
            {
            pushFollow(FOLLOW_rule__LiteralString__ValueAssignment_in_ruleLiteralString576);
            rule__LiteralString__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLiteralStringAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteralString"


    // $ANTLR start "entryRuleCollaboration"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:314:1: entryRuleCollaboration : ruleCollaboration EOF ;
    public final void entryRuleCollaboration() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:315:1: ( ruleCollaboration EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:316:1: ruleCollaboration EOF
            {
             before(grammarAccess.getCollaborationRule()); 
            pushFollow(FOLLOW_ruleCollaboration_in_entryRuleCollaboration603);
            ruleCollaboration();

            state._fsp--;

             after(grammarAccess.getCollaborationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCollaboration610); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollaboration"


    // $ANTLR start "ruleCollaboration"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:323:1: ruleCollaboration : ( ( rule__Collaboration__Group__0 ) ) ;
    public final void ruleCollaboration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:327:2: ( ( ( rule__Collaboration__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:328:1: ( ( rule__Collaboration__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:328:1: ( ( rule__Collaboration__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:329:1: ( rule__Collaboration__Group__0 )
            {
             before(grammarAccess.getCollaborationAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:330:1: ( rule__Collaboration__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:330:2: rule__Collaboration__Group__0
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__0_in_ruleCollaboration636);
            rule__Collaboration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCollaborationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollaboration"


    // $ANTLR start "entryRuleInteraction"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:342:1: entryRuleInteraction : ruleInteraction EOF ;
    public final void entryRuleInteraction() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:343:1: ( ruleInteraction EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:344:1: ruleInteraction EOF
            {
             before(grammarAccess.getInteractionRule()); 
            pushFollow(FOLLOW_ruleInteraction_in_entryRuleInteraction663);
            ruleInteraction();

            state._fsp--;

             after(grammarAccess.getInteractionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteraction670); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:351:1: ruleInteraction : ( ( rule__Interaction__Group__0 ) ) ;
    public final void ruleInteraction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:355:2: ( ( ( rule__Interaction__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:356:1: ( ( rule__Interaction__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:356:1: ( ( rule__Interaction__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:357:1: ( rule__Interaction__Group__0 )
            {
             before(grammarAccess.getInteractionAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:358:1: ( rule__Interaction__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:358:2: rule__Interaction__Group__0
            {
            pushFollow(FOLLOW_rule__Interaction__Group__0_in_ruleInteraction696);
            rule__Interaction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleInteractionFragment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:370:1: entryRuleInteractionFragment : ruleInteractionFragment EOF ;
    public final void entryRuleInteractionFragment() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:371:1: ( ruleInteractionFragment EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:372:1: ruleInteractionFragment EOF
            {
             before(grammarAccess.getInteractionFragmentRule()); 
            pushFollow(FOLLOW_ruleInteractionFragment_in_entryRuleInteractionFragment723);
            ruleInteractionFragment();

            state._fsp--;

             after(grammarAccess.getInteractionFragmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionFragment730); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:379:1: ruleInteractionFragment : ( ( rule__InteractionFragment__Alternatives ) ) ;
    public final void ruleInteractionFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:383:2: ( ( ( rule__InteractionFragment__Alternatives ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:384:1: ( ( rule__InteractionFragment__Alternatives ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:384:1: ( ( rule__InteractionFragment__Alternatives ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:385:1: ( rule__InteractionFragment__Alternatives )
            {
             before(grammarAccess.getInteractionFragmentAccess().getAlternatives()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:386:1: ( rule__InteractionFragment__Alternatives )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:386:2: rule__InteractionFragment__Alternatives
            {
            pushFollow(FOLLOW_rule__InteractionFragment__Alternatives_in_ruleInteractionFragment756);
            rule__InteractionFragment__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInteractionFragmentAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleCombinedFragment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:398:1: entryRuleCombinedFragment : ruleCombinedFragment EOF ;
    public final void entryRuleCombinedFragment() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:399:1: ( ruleCombinedFragment EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:400:1: ruleCombinedFragment EOF
            {
             before(grammarAccess.getCombinedFragmentRule()); 
            pushFollow(FOLLOW_ruleCombinedFragment_in_entryRuleCombinedFragment783);
            ruleCombinedFragment();

            state._fsp--;

             after(grammarAccess.getCombinedFragmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCombinedFragment790); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCombinedFragment"


    // $ANTLR start "ruleCombinedFragment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:407:1: ruleCombinedFragment : ( ( rule__CombinedFragment__Group__0 ) ) ;
    public final void ruleCombinedFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:411:2: ( ( ( rule__CombinedFragment__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:412:1: ( ( rule__CombinedFragment__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:412:1: ( ( rule__CombinedFragment__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:413:1: ( rule__CombinedFragment__Group__0 )
            {
             before(grammarAccess.getCombinedFragmentAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:414:1: ( rule__CombinedFragment__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:414:2: rule__CombinedFragment__Group__0
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__0_in_ruleCombinedFragment816);
            rule__CombinedFragment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCombinedFragmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCombinedFragment"


    // $ANTLR start "entryRuleComment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:426:1: entryRuleComment : ruleComment EOF ;
    public final void entryRuleComment() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:427:1: ( ruleComment EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:428:1: ruleComment EOF
            {
             before(grammarAccess.getCommentRule()); 
            pushFollow(FOLLOW_ruleComment_in_entryRuleComment843);
            ruleComment();

            state._fsp--;

             after(grammarAccess.getCommentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleComment850); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComment"


    // $ANTLR start "ruleComment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:435:1: ruleComment : ( ( rule__Comment__Group__0 ) ) ;
    public final void ruleComment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:439:2: ( ( ( rule__Comment__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:440:1: ( ( rule__Comment__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:440:1: ( ( rule__Comment__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:441:1: ( rule__Comment__Group__0 )
            {
             before(grammarAccess.getCommentAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:442:1: ( rule__Comment__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:442:2: rule__Comment__Group__0
            {
            pushFollow(FOLLOW_rule__Comment__Group__0_in_ruleComment876);
            rule__Comment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCommentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComment"


    // $ANTLR start "entryRuleInteractionOperand"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:454:1: entryRuleInteractionOperand : ruleInteractionOperand EOF ;
    public final void entryRuleInteractionOperand() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:455:1: ( ruleInteractionOperand EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:456:1: ruleInteractionOperand EOF
            {
             before(grammarAccess.getInteractionOperandRule()); 
            pushFollow(FOLLOW_ruleInteractionOperand_in_entryRuleInteractionOperand903);
            ruleInteractionOperand();

            state._fsp--;

             after(grammarAccess.getInteractionOperandRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionOperand910); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteractionOperand"


    // $ANTLR start "ruleInteractionOperand"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:463:1: ruleInteractionOperand : ( ( rule__InteractionOperand__Group__0 ) ) ;
    public final void ruleInteractionOperand() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:467:2: ( ( ( rule__InteractionOperand__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:468:1: ( ( rule__InteractionOperand__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:468:1: ( ( rule__InteractionOperand__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:469:1: ( rule__InteractionOperand__Group__0 )
            {
             before(grammarAccess.getInteractionOperandAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:470:1: ( rule__InteractionOperand__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:470:2: rule__InteractionOperand__Group__0
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__0_in_ruleInteractionOperand936);
            rule__InteractionOperand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionOperandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteractionOperand"


    // $ANTLR start "entryRuleInteractionConstraint"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:482:1: entryRuleInteractionConstraint : ruleInteractionConstraint EOF ;
    public final void entryRuleInteractionConstraint() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:483:1: ( ruleInteractionConstraint EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:484:1: ruleInteractionConstraint EOF
            {
             before(grammarAccess.getInteractionConstraintRule()); 
            pushFollow(FOLLOW_ruleInteractionConstraint_in_entryRuleInteractionConstraint963);
            ruleInteractionConstraint();

            state._fsp--;

             after(grammarAccess.getInteractionConstraintRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionConstraint970); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteractionConstraint"


    // $ANTLR start "ruleInteractionConstraint"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:491:1: ruleInteractionConstraint : ( ( rule__InteractionConstraint__Group__0 ) ) ;
    public final void ruleInteractionConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:495:2: ( ( ( rule__InteractionConstraint__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:496:1: ( ( rule__InteractionConstraint__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:496:1: ( ( rule__InteractionConstraint__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:497:1: ( rule__InteractionConstraint__Group__0 )
            {
             before(grammarAccess.getInteractionConstraintAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:498:1: ( rule__InteractionConstraint__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:498:2: rule__InteractionConstraint__Group__0
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__0_in_ruleInteractionConstraint996);
            rule__InteractionConstraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteractionConstraint"


    // $ANTLR start "entryRuleLifeline"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:510:1: entryRuleLifeline : ruleLifeline EOF ;
    public final void entryRuleLifeline() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:511:1: ( ruleLifeline EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:512:1: ruleLifeline EOF
            {
             before(grammarAccess.getLifelineRule()); 
            pushFollow(FOLLOW_ruleLifeline_in_entryRuleLifeline1023);
            ruleLifeline();

            state._fsp--;

             after(grammarAccess.getLifelineRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLifeline1030); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLifeline"


    // $ANTLR start "ruleLifeline"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:519:1: ruleLifeline : ( ( rule__Lifeline__Group__0 ) ) ;
    public final void ruleLifeline() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:523:2: ( ( ( rule__Lifeline__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:524:1: ( ( rule__Lifeline__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:524:1: ( ( rule__Lifeline__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:525:1: ( rule__Lifeline__Group__0 )
            {
             before(grammarAccess.getLifelineAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:526:1: ( rule__Lifeline__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:526:2: rule__Lifeline__Group__0
            {
            pushFollow(FOLLOW_rule__Lifeline__Group__0_in_ruleLifeline1056);
            rule__Lifeline__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLifelineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLifeline"


    // $ANTLR start "entryRuleMessage"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:538:1: entryRuleMessage : ruleMessage EOF ;
    public final void entryRuleMessage() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:539:1: ( ruleMessage EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:540:1: ruleMessage EOF
            {
             before(grammarAccess.getMessageRule()); 
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage1083);
            ruleMessage();

            state._fsp--;

             after(grammarAccess.getMessageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage1090); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:547:1: ruleMessage : ( ( rule__Message__Group__0 ) ) ;
    public final void ruleMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:551:2: ( ( ( rule__Message__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:552:1: ( ( rule__Message__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:552:1: ( ( rule__Message__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:553:1: ( rule__Message__Group__0 )
            {
             before(grammarAccess.getMessageAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:554:1: ( rule__Message__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:554:2: rule__Message__Group__0
            {
            pushFollow(FOLLOW_rule__Message__Group__0_in_ruleMessage1116);
            rule__Message__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMessageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleMessageOccurrenceSpecification"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:566:1: entryRuleMessageOccurrenceSpecification : ruleMessageOccurrenceSpecification EOF ;
    public final void entryRuleMessageOccurrenceSpecification() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:567:1: ( ruleMessageOccurrenceSpecification EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:568:1: ruleMessageOccurrenceSpecification EOF
            {
             before(grammarAccess.getMessageOccurrenceSpecificationRule()); 
            pushFollow(FOLLOW_ruleMessageOccurrenceSpecification_in_entryRuleMessageOccurrenceSpecification1143);
            ruleMessageOccurrenceSpecification();

            state._fsp--;

             after(grammarAccess.getMessageOccurrenceSpecificationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessageOccurrenceSpecification1150); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessageOccurrenceSpecification"


    // $ANTLR start "ruleMessageOccurrenceSpecification"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:575:1: ruleMessageOccurrenceSpecification : ( ( rule__MessageOccurrenceSpecification__Group__0 ) ) ;
    public final void ruleMessageOccurrenceSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:579:2: ( ( ( rule__MessageOccurrenceSpecification__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:580:1: ( ( rule__MessageOccurrenceSpecification__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:580:1: ( ( rule__MessageOccurrenceSpecification__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:581:1: ( rule__MessageOccurrenceSpecification__Group__0 )
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:582:1: ( rule__MessageOccurrenceSpecification__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:582:2: rule__MessageOccurrenceSpecification__Group__0
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__0_in_ruleMessageOccurrenceSpecification1176);
            rule__MessageOccurrenceSpecification__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessageOccurrenceSpecification"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:594:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:595:1: ( ruleQualifiedName EOF )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:596:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1203);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName1210); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:603:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:607:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:608:1: ( ( rule__QualifiedName__Group__0 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:608:1: ( ( rule__QualifiedName__Group__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:609:1: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:610:1: ( rule__QualifiedName__Group__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:610:2: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1236);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleInteractionOperatorKind"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:623:1: ruleInteractionOperatorKind : ( ( rule__InteractionOperatorKind__Alternatives ) ) ;
    public final void ruleInteractionOperatorKind() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:627:1: ( ( ( rule__InteractionOperatorKind__Alternatives ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:628:1: ( ( rule__InteractionOperatorKind__Alternatives ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:628:1: ( ( rule__InteractionOperatorKind__Alternatives ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:629:1: ( rule__InteractionOperatorKind__Alternatives )
            {
             before(grammarAccess.getInteractionOperatorKindAccess().getAlternatives()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:630:1: ( rule__InteractionOperatorKind__Alternatives )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:630:2: rule__InteractionOperatorKind__Alternatives
            {
            pushFollow(FOLLOW_rule__InteractionOperatorKind__Alternatives_in_ruleInteractionOperatorKind1273);
            rule__InteractionOperatorKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInteractionOperatorKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteractionOperatorKind"


    // $ANTLR start "ruleMessageSort"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:642:1: ruleMessageSort : ( ( rule__MessageSort__Alternatives ) ) ;
    public final void ruleMessageSort() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:646:1: ( ( ( rule__MessageSort__Alternatives ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:647:1: ( ( rule__MessageSort__Alternatives ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:647:1: ( ( rule__MessageSort__Alternatives ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:648:1: ( rule__MessageSort__Alternatives )
            {
             before(grammarAccess.getMessageSortAccess().getAlternatives()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:649:1: ( rule__MessageSort__Alternatives )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:649:2: rule__MessageSort__Alternatives
            {
            pushFollow(FOLLOW_rule__MessageSort__Alternatives_in_ruleMessageSort1309);
            rule__MessageSort__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMessageSortAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessageSort"


    // $ANTLR start "rule__PackageableElement__Alternatives"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:660:1: rule__PackageableElement__Alternatives : ( ( rulePackage ) | ( ruleClass ) | ( ruleCollaboration ) );
    public final void rule__PackageableElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:664:1: ( ( rulePackage ) | ( ruleClass ) | ( ruleCollaboration ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 29:
                {
                alt1=1;
                }
                break;
            case 32:
                {
                alt1=2;
                }
                break;
            case 40:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:665:1: ( rulePackage )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:665:1: ( rulePackage )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:666:1: rulePackage
                    {
                     before(grammarAccess.getPackageableElementAccess().getPackageParserRuleCall_0()); 
                    pushFollow(FOLLOW_rulePackage_in_rule__PackageableElement__Alternatives1344);
                    rulePackage();

                    state._fsp--;

                     after(grammarAccess.getPackageableElementAccess().getPackageParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:671:6: ( ruleClass )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:671:6: ( ruleClass )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:672:1: ruleClass
                    {
                     before(grammarAccess.getPackageableElementAccess().getClassParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleClass_in_rule__PackageableElement__Alternatives1361);
                    ruleClass();

                    state._fsp--;

                     after(grammarAccess.getPackageableElementAccess().getClassParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:677:6: ( ruleCollaboration )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:677:6: ( ruleCollaboration )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:678:1: ruleCollaboration
                    {
                     before(grammarAccess.getPackageableElementAccess().getCollaborationParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleCollaboration_in_rule__PackageableElement__Alternatives1378);
                    ruleCollaboration();

                    state._fsp--;

                     after(grammarAccess.getPackageableElementAccess().getCollaborationParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PackageableElement__Alternatives"


    // $ANTLR start "rule__ValueSpecification__Alternatives"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:688:1: rule__ValueSpecification__Alternatives : ( ( ruleLiteralUnlimitedNatural ) | ( ruleLiteralString ) );
    public final void rule__ValueSpecification__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:692:1: ( ( ruleLiteralUnlimitedNatural ) | ( ruleLiteralString ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_INT) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_STRING) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:693:1: ( ruleLiteralUnlimitedNatural )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:693:1: ( ruleLiteralUnlimitedNatural )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:694:1: ruleLiteralUnlimitedNatural
                    {
                     before(grammarAccess.getValueSpecificationAccess().getLiteralUnlimitedNaturalParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleLiteralUnlimitedNatural_in_rule__ValueSpecification__Alternatives1410);
                    ruleLiteralUnlimitedNatural();

                    state._fsp--;

                     after(grammarAccess.getValueSpecificationAccess().getLiteralUnlimitedNaturalParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:699:6: ( ruleLiteralString )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:699:6: ( ruleLiteralString )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:700:1: ruleLiteralString
                    {
                     before(grammarAccess.getValueSpecificationAccess().getLiteralStringParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleLiteralString_in_rule__ValueSpecification__Alternatives1427);
                    ruleLiteralString();

                    state._fsp--;

                     after(grammarAccess.getValueSpecificationAccess().getLiteralStringParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueSpecification__Alternatives"


    // $ANTLR start "rule__InteractionFragment__Alternatives"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:710:1: rule__InteractionFragment__Alternatives : ( ( ruleMessageOccurrenceSpecification ) | ( ruleCombinedFragment ) );
    public final void rule__InteractionFragment__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:714:1: ( ( ruleMessageOccurrenceSpecification ) | ( ruleCombinedFragment ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==53) ) {
                alt3=1;
            }
            else if ( (LA3_0==42) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:715:1: ( ruleMessageOccurrenceSpecification )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:715:1: ( ruleMessageOccurrenceSpecification )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:716:1: ruleMessageOccurrenceSpecification
                    {
                     before(grammarAccess.getInteractionFragmentAccess().getMessageOccurrenceSpecificationParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleMessageOccurrenceSpecification_in_rule__InteractionFragment__Alternatives1459);
                    ruleMessageOccurrenceSpecification();

                    state._fsp--;

                     after(grammarAccess.getInteractionFragmentAccess().getMessageOccurrenceSpecificationParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:721:6: ( ruleCombinedFragment )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:721:6: ( ruleCombinedFragment )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:722:1: ruleCombinedFragment
                    {
                     before(grammarAccess.getInteractionFragmentAccess().getCombinedFragmentParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleCombinedFragment_in_rule__InteractionFragment__Alternatives1476);
                    ruleCombinedFragment();

                    state._fsp--;

                     after(grammarAccess.getInteractionFragmentAccess().getCombinedFragmentParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionFragment__Alternatives"


    // $ANTLR start "rule__InteractionOperatorKind__Alternatives"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:732:1: rule__InteractionOperatorKind__Alternatives : ( ( ( 'assert' ) ) | ( ( 'sec' ) ) | ( ( 'alt' ) ) | ( ( 'opt' ) ) | ( ( 'break' ) ) | ( ( 'par' ) ) | ( ( 'strict' ) ) | ( ( 'loop' ) ) | ( ( 'critical' ) ) | ( ( 'neg' ) ) | ( ( 'ignore' ) ) | ( ( 'consider' ) ) );
    public final void rule__InteractionOperatorKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:736:1: ( ( ( 'assert' ) ) | ( ( 'sec' ) ) | ( ( 'alt' ) ) | ( ( 'opt' ) ) | ( ( 'break' ) ) | ( ( 'par' ) ) | ( ( 'strict' ) ) | ( ( 'loop' ) ) | ( ( 'critical' ) ) | ( ( 'neg' ) ) | ( ( 'ignore' ) ) | ( ( 'consider' ) ) )
            int alt4=12;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 14:
                {
                alt4=4;
                }
                break;
            case 15:
                {
                alt4=5;
                }
                break;
            case 16:
                {
                alt4=6;
                }
                break;
            case 17:
                {
                alt4=7;
                }
                break;
            case 18:
                {
                alt4=8;
                }
                break;
            case 19:
                {
                alt4=9;
                }
                break;
            case 20:
                {
                alt4=10;
                }
                break;
            case 21:
                {
                alt4=11;
                }
                break;
            case 22:
                {
                alt4=12;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:737:1: ( ( 'assert' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:737:1: ( ( 'assert' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:738:1: ( 'assert' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getAssertEnumLiteralDeclaration_0()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:739:1: ( 'assert' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:739:3: 'assert'
                    {
                    match(input,11,FOLLOW_11_in_rule__InteractionOperatorKind__Alternatives1509); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getAssertEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:744:6: ( ( 'sec' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:744:6: ( ( 'sec' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:745:1: ( 'sec' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getSeqEnumLiteralDeclaration_1()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:746:1: ( 'sec' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:746:3: 'sec'
                    {
                    match(input,12,FOLLOW_12_in_rule__InteractionOperatorKind__Alternatives1530); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getSeqEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:751:6: ( ( 'alt' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:751:6: ( ( 'alt' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:752:1: ( 'alt' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getAltEnumLiteralDeclaration_2()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:753:1: ( 'alt' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:753:3: 'alt'
                    {
                    match(input,13,FOLLOW_13_in_rule__InteractionOperatorKind__Alternatives1551); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getAltEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:758:6: ( ( 'opt' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:758:6: ( ( 'opt' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:759:1: ( 'opt' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getOptEnumLiteralDeclaration_3()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:760:1: ( 'opt' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:760:3: 'opt'
                    {
                    match(input,14,FOLLOW_14_in_rule__InteractionOperatorKind__Alternatives1572); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getOptEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:765:6: ( ( 'break' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:765:6: ( ( 'break' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:766:1: ( 'break' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getBreakEnumLiteralDeclaration_4()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:767:1: ( 'break' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:767:3: 'break'
                    {
                    match(input,15,FOLLOW_15_in_rule__InteractionOperatorKind__Alternatives1593); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getBreakEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:772:6: ( ( 'par' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:772:6: ( ( 'par' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:773:1: ( 'par' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getParEnumLiteralDeclaration_5()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:774:1: ( 'par' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:774:3: 'par'
                    {
                    match(input,16,FOLLOW_16_in_rule__InteractionOperatorKind__Alternatives1614); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getParEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:779:6: ( ( 'strict' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:779:6: ( ( 'strict' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:780:1: ( 'strict' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getStrictEnumLiteralDeclaration_6()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:781:1: ( 'strict' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:781:3: 'strict'
                    {
                    match(input,17,FOLLOW_17_in_rule__InteractionOperatorKind__Alternatives1635); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getStrictEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:786:6: ( ( 'loop' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:786:6: ( ( 'loop' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:787:1: ( 'loop' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getLoopEnumLiteralDeclaration_7()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:788:1: ( 'loop' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:788:3: 'loop'
                    {
                    match(input,18,FOLLOW_18_in_rule__InteractionOperatorKind__Alternatives1656); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getLoopEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:793:6: ( ( 'critical' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:793:6: ( ( 'critical' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:794:1: ( 'critical' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getCriticalEnumLiteralDeclaration_8()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:795:1: ( 'critical' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:795:3: 'critical'
                    {
                    match(input,19,FOLLOW_19_in_rule__InteractionOperatorKind__Alternatives1677); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getCriticalEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:800:6: ( ( 'neg' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:800:6: ( ( 'neg' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:801:1: ( 'neg' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getNegEnumLiteralDeclaration_9()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:802:1: ( 'neg' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:802:3: 'neg'
                    {
                    match(input,20,FOLLOW_20_in_rule__InteractionOperatorKind__Alternatives1698); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getNegEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:807:6: ( ( 'ignore' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:807:6: ( ( 'ignore' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:808:1: ( 'ignore' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getIgnoreEnumLiteralDeclaration_10()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:809:1: ( 'ignore' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:809:3: 'ignore'
                    {
                    match(input,21,FOLLOW_21_in_rule__InteractionOperatorKind__Alternatives1719); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getIgnoreEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:814:6: ( ( 'consider' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:814:6: ( ( 'consider' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:815:1: ( 'consider' )
                    {
                     before(grammarAccess.getInteractionOperatorKindAccess().getConsiderEnumLiteralDeclaration_11()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:816:1: ( 'consider' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:816:3: 'consider'
                    {
                    match(input,22,FOLLOW_22_in_rule__InteractionOperatorKind__Alternatives1740); 

                    }

                     after(grammarAccess.getInteractionOperatorKindAccess().getConsiderEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperatorKind__Alternatives"


    // $ANTLR start "rule__MessageSort__Alternatives"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:826:1: rule__MessageSort__Alternatives : ( ( ( 'asynchCall' ) ) | ( ( 'synchCall' ) ) | ( ( 'createMessage' ) ) | ( ( 'deleteMessage' ) ) | ( ( 'reply' ) ) | ( ( 'asynchSignal' ) ) );
    public final void rule__MessageSort__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:830:1: ( ( ( 'asynchCall' ) ) | ( ( 'synchCall' ) ) | ( ( 'createMessage' ) ) | ( ( 'deleteMessage' ) ) | ( ( 'reply' ) ) | ( ( 'asynchSignal' ) ) )
            int alt5=6;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt5=1;
                }
                break;
            case 24:
                {
                alt5=2;
                }
                break;
            case 25:
                {
                alt5=3;
                }
                break;
            case 26:
                {
                alt5=4;
                }
                break;
            case 27:
                {
                alt5=5;
                }
                break;
            case 28:
                {
                alt5=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:831:1: ( ( 'asynchCall' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:831:1: ( ( 'asynchCall' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:832:1: ( 'asynchCall' )
                    {
                     before(grammarAccess.getMessageSortAccess().getAsynchCallEnumLiteralDeclaration_0()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:833:1: ( 'asynchCall' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:833:3: 'asynchCall'
                    {
                    match(input,23,FOLLOW_23_in_rule__MessageSort__Alternatives1776); 

                    }

                     after(grammarAccess.getMessageSortAccess().getAsynchCallEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:838:6: ( ( 'synchCall' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:838:6: ( ( 'synchCall' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:839:1: ( 'synchCall' )
                    {
                     before(grammarAccess.getMessageSortAccess().getSynchCallEnumLiteralDeclaration_1()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:840:1: ( 'synchCall' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:840:3: 'synchCall'
                    {
                    match(input,24,FOLLOW_24_in_rule__MessageSort__Alternatives1797); 

                    }

                     after(grammarAccess.getMessageSortAccess().getSynchCallEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:845:6: ( ( 'createMessage' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:845:6: ( ( 'createMessage' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:846:1: ( 'createMessage' )
                    {
                     before(grammarAccess.getMessageSortAccess().getCreateMessageEnumLiteralDeclaration_2()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:847:1: ( 'createMessage' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:847:3: 'createMessage'
                    {
                    match(input,25,FOLLOW_25_in_rule__MessageSort__Alternatives1818); 

                    }

                     after(grammarAccess.getMessageSortAccess().getCreateMessageEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:852:6: ( ( 'deleteMessage' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:852:6: ( ( 'deleteMessage' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:853:1: ( 'deleteMessage' )
                    {
                     before(grammarAccess.getMessageSortAccess().getDeleteMessageEnumLiteralDeclaration_3()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:854:1: ( 'deleteMessage' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:854:3: 'deleteMessage'
                    {
                    match(input,26,FOLLOW_26_in_rule__MessageSort__Alternatives1839); 

                    }

                     after(grammarAccess.getMessageSortAccess().getDeleteMessageEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:859:6: ( ( 'reply' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:859:6: ( ( 'reply' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:860:1: ( 'reply' )
                    {
                     before(grammarAccess.getMessageSortAccess().getReplyEnumLiteralDeclaration_4()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:861:1: ( 'reply' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:861:3: 'reply'
                    {
                    match(input,27,FOLLOW_27_in_rule__MessageSort__Alternatives1860); 

                    }

                     after(grammarAccess.getMessageSortAccess().getReplyEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:866:6: ( ( 'asynchSignal' ) )
                    {
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:866:6: ( ( 'asynchSignal' ) )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:867:1: ( 'asynchSignal' )
                    {
                     before(grammarAccess.getMessageSortAccess().getAsynchSignalEnumLiteralDeclaration_5()); 
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:868:1: ( 'asynchSignal' )
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:868:3: 'asynchSignal'
                    {
                    match(input,28,FOLLOW_28_in_rule__MessageSort__Alternatives1881); 

                    }

                     after(grammarAccess.getMessageSortAccess().getAsynchSignalEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageSort__Alternatives"


    // $ANTLR start "rule__Package__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:880:1: rule__Package__Group__0 : rule__Package__Group__0__Impl rule__Package__Group__1 ;
    public final void rule__Package__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:884:1: ( rule__Package__Group__0__Impl rule__Package__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:885:2: rule__Package__Group__0__Impl rule__Package__Group__1
            {
            pushFollow(FOLLOW_rule__Package__Group__0__Impl_in_rule__Package__Group__01914);
            rule__Package__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package__Group__1_in_rule__Package__Group__01917);
            rule__Package__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__0"


    // $ANTLR start "rule__Package__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:892:1: rule__Package__Group__0__Impl : ( 'package' ) ;
    public final void rule__Package__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:896:1: ( ( 'package' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:897:1: ( 'package' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:897:1: ( 'package' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:898:1: 'package'
            {
             before(grammarAccess.getPackageAccess().getPackageKeyword_0()); 
            match(input,29,FOLLOW_29_in_rule__Package__Group__0__Impl1945); 
             after(grammarAccess.getPackageAccess().getPackageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__0__Impl"


    // $ANTLR start "rule__Package__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:911:1: rule__Package__Group__1 : rule__Package__Group__1__Impl rule__Package__Group__2 ;
    public final void rule__Package__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:915:1: ( rule__Package__Group__1__Impl rule__Package__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:916:2: rule__Package__Group__1__Impl rule__Package__Group__2
            {
            pushFollow(FOLLOW_rule__Package__Group__1__Impl_in_rule__Package__Group__11976);
            rule__Package__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package__Group__2_in_rule__Package__Group__11979);
            rule__Package__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__1"


    // $ANTLR start "rule__Package__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:923:1: rule__Package__Group__1__Impl : ( ( rule__Package__NameAssignment_1 ) ) ;
    public final void rule__Package__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:927:1: ( ( ( rule__Package__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:928:1: ( ( rule__Package__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:928:1: ( ( rule__Package__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:929:1: ( rule__Package__NameAssignment_1 )
            {
             before(grammarAccess.getPackageAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:930:1: ( rule__Package__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:930:2: rule__Package__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Package__NameAssignment_1_in_rule__Package__Group__1__Impl2006);
            rule__Package__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPackageAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__1__Impl"


    // $ANTLR start "rule__Package__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:940:1: rule__Package__Group__2 : rule__Package__Group__2__Impl rule__Package__Group__3 ;
    public final void rule__Package__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:944:1: ( rule__Package__Group__2__Impl rule__Package__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:945:2: rule__Package__Group__2__Impl rule__Package__Group__3
            {
            pushFollow(FOLLOW_rule__Package__Group__2__Impl_in_rule__Package__Group__22036);
            rule__Package__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package__Group__3_in_rule__Package__Group__22039);
            rule__Package__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__2"


    // $ANTLR start "rule__Package__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:952:1: rule__Package__Group__2__Impl : ( '{' ) ;
    public final void rule__Package__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:956:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:957:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:957:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:958:1: '{'
            {
             before(grammarAccess.getPackageAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,30,FOLLOW_30_in_rule__Package__Group__2__Impl2067); 
             after(grammarAccess.getPackageAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__2__Impl"


    // $ANTLR start "rule__Package__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:971:1: rule__Package__Group__3 : rule__Package__Group__3__Impl rule__Package__Group__4 ;
    public final void rule__Package__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:975:1: ( rule__Package__Group__3__Impl rule__Package__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:976:2: rule__Package__Group__3__Impl rule__Package__Group__4
            {
            pushFollow(FOLLOW_rule__Package__Group__3__Impl_in_rule__Package__Group__32098);
            rule__Package__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package__Group__4_in_rule__Package__Group__32101);
            rule__Package__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__3"


    // $ANTLR start "rule__Package__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:983:1: rule__Package__Group__3__Impl : ( ( rule__Package__PackagedElementAssignment_3 )* ) ;
    public final void rule__Package__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:987:1: ( ( ( rule__Package__PackagedElementAssignment_3 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:988:1: ( ( rule__Package__PackagedElementAssignment_3 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:988:1: ( ( rule__Package__PackagedElementAssignment_3 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:989:1: ( rule__Package__PackagedElementAssignment_3 )*
            {
             before(grammarAccess.getPackageAccess().getPackagedElementAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:990:1: ( rule__Package__PackagedElementAssignment_3 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==29||LA6_0==32||LA6_0==40) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:990:2: rule__Package__PackagedElementAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Package__PackagedElementAssignment_3_in_rule__Package__Group__3__Impl2128);
            	    rule__Package__PackagedElementAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getPackageAccess().getPackagedElementAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__3__Impl"


    // $ANTLR start "rule__Package__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1000:1: rule__Package__Group__4 : rule__Package__Group__4__Impl ;
    public final void rule__Package__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1004:1: ( rule__Package__Group__4__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1005:2: rule__Package__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Package__Group__4__Impl_in_rule__Package__Group__42159);
            rule__Package__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__4"


    // $ANTLR start "rule__Package__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1011:1: rule__Package__Group__4__Impl : ( '}' ) ;
    public final void rule__Package__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1015:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1016:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1016:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1017:1: '}'
            {
             before(grammarAccess.getPackageAccess().getRightCurlyBracketKeyword_4()); 
            match(input,31,FOLLOW_31_in_rule__Package__Group__4__Impl2187); 
             after(grammarAccess.getPackageAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__Group__4__Impl"


    // $ANTLR start "rule__Class__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1040:1: rule__Class__Group__0 : rule__Class__Group__0__Impl rule__Class__Group__1 ;
    public final void rule__Class__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1044:1: ( rule__Class__Group__0__Impl rule__Class__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1045:2: rule__Class__Group__0__Impl rule__Class__Group__1
            {
            pushFollow(FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02228);
            rule__Class__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02231);
            rule__Class__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0"


    // $ANTLR start "rule__Class__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1052:1: rule__Class__Group__0__Impl : ( 'class' ) ;
    public final void rule__Class__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1056:1: ( ( 'class' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1057:1: ( 'class' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1057:1: ( 'class' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1058:1: 'class'
            {
             before(grammarAccess.getClassAccess().getClassKeyword_0()); 
            match(input,32,FOLLOW_32_in_rule__Class__Group__0__Impl2259); 
             after(grammarAccess.getClassAccess().getClassKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0__Impl"


    // $ANTLR start "rule__Class__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1071:1: rule__Class__Group__1 : rule__Class__Group__1__Impl rule__Class__Group__2 ;
    public final void rule__Class__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1075:1: ( rule__Class__Group__1__Impl rule__Class__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1076:2: rule__Class__Group__1__Impl rule__Class__Group__2
            {
            pushFollow(FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12290);
            rule__Class__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12293);
            rule__Class__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1"


    // $ANTLR start "rule__Class__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1083:1: rule__Class__Group__1__Impl : ( ( rule__Class__NameAssignment_1 ) ) ;
    public final void rule__Class__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1087:1: ( ( ( rule__Class__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1088:1: ( ( rule__Class__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1088:1: ( ( rule__Class__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1089:1: ( rule__Class__NameAssignment_1 )
            {
             before(grammarAccess.getClassAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1090:1: ( rule__Class__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1090:2: rule__Class__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2320);
            rule__Class__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1100:1: rule__Class__Group__2 : rule__Class__Group__2__Impl rule__Class__Group__3 ;
    public final void rule__Class__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1104:1: ( rule__Class__Group__2__Impl rule__Class__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1105:2: rule__Class__Group__2__Impl rule__Class__Group__3
            {
            pushFollow(FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22350);
            rule__Class__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22353);
            rule__Class__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2"


    // $ANTLR start "rule__Class__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1112:1: rule__Class__Group__2__Impl : ( '{' ) ;
    public final void rule__Class__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1116:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1117:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1117:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1118:1: '{'
            {
             before(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,30,FOLLOW_30_in_rule__Class__Group__2__Impl2381); 
             after(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2__Impl"


    // $ANTLR start "rule__Class__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1131:1: rule__Class__Group__3 : rule__Class__Group__3__Impl rule__Class__Group__4 ;
    public final void rule__Class__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1135:1: ( rule__Class__Group__3__Impl rule__Class__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1136:2: rule__Class__Group__3__Impl rule__Class__Group__4
            {
            pushFollow(FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32412);
            rule__Class__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32415);
            rule__Class__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3"


    // $ANTLR start "rule__Class__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1143:1: rule__Class__Group__3__Impl : ( ( rule__Class__OwnedOperationAssignment_3 )* ) ;
    public final void rule__Class__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1147:1: ( ( ( rule__Class__OwnedOperationAssignment_3 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1148:1: ( ( rule__Class__OwnedOperationAssignment_3 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1148:1: ( ( rule__Class__OwnedOperationAssignment_3 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1149:1: ( rule__Class__OwnedOperationAssignment_3 )*
            {
             before(grammarAccess.getClassAccess().getOwnedOperationAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1150:1: ( rule__Class__OwnedOperationAssignment_3 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==33) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1150:2: rule__Class__OwnedOperationAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Class__OwnedOperationAssignment_3_in_rule__Class__Group__3__Impl2442);
            	    rule__Class__OwnedOperationAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getClassAccess().getOwnedOperationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3__Impl"


    // $ANTLR start "rule__Class__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1160:1: rule__Class__Group__4 : rule__Class__Group__4__Impl rule__Class__Group__5 ;
    public final void rule__Class__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1164:1: ( rule__Class__Group__4__Impl rule__Class__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1165:2: rule__Class__Group__4__Impl rule__Class__Group__5
            {
            pushFollow(FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42473);
            rule__Class__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42476);
            rule__Class__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4"


    // $ANTLR start "rule__Class__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1172:1: rule__Class__Group__4__Impl : ( ( rule__Class__OwnedAttributeAssignment_4 )* ) ;
    public final void rule__Class__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1176:1: ( ( ( rule__Class__OwnedAttributeAssignment_4 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1177:1: ( ( rule__Class__OwnedAttributeAssignment_4 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1177:1: ( ( rule__Class__OwnedAttributeAssignment_4 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1178:1: ( rule__Class__OwnedAttributeAssignment_4 )*
            {
             before(grammarAccess.getClassAccess().getOwnedAttributeAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1179:1: ( rule__Class__OwnedAttributeAssignment_4 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==35) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1179:2: rule__Class__OwnedAttributeAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Class__OwnedAttributeAssignment_4_in_rule__Class__Group__4__Impl2503);
            	    rule__Class__OwnedAttributeAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getClassAccess().getOwnedAttributeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4__Impl"


    // $ANTLR start "rule__Class__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1189:1: rule__Class__Group__5 : rule__Class__Group__5__Impl ;
    public final void rule__Class__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1193:1: ( rule__Class__Group__5__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1194:2: rule__Class__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52534);
            rule__Class__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5"


    // $ANTLR start "rule__Class__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1200:1: rule__Class__Group__5__Impl : ( '}' ) ;
    public final void rule__Class__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1204:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1205:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1205:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1206:1: '}'
            {
             before(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5()); 
            match(input,31,FOLLOW_31_in_rule__Class__Group__5__Impl2562); 
             after(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5__Impl"


    // $ANTLR start "rule__Operation__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1231:1: rule__Operation__Group__0 : rule__Operation__Group__0__Impl rule__Operation__Group__1 ;
    public final void rule__Operation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1235:1: ( rule__Operation__Group__0__Impl rule__Operation__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1236:2: rule__Operation__Group__0__Impl rule__Operation__Group__1
            {
            pushFollow(FOLLOW_rule__Operation__Group__0__Impl_in_rule__Operation__Group__02605);
            rule__Operation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Operation__Group__1_in_rule__Operation__Group__02608);
            rule__Operation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__0"


    // $ANTLR start "rule__Operation__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1243:1: rule__Operation__Group__0__Impl : ( 'operation' ) ;
    public final void rule__Operation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1247:1: ( ( 'operation' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1248:1: ( 'operation' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1248:1: ( 'operation' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1249:1: 'operation'
            {
             before(grammarAccess.getOperationAccess().getOperationKeyword_0()); 
            match(input,33,FOLLOW_33_in_rule__Operation__Group__0__Impl2636); 
             after(grammarAccess.getOperationAccess().getOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__0__Impl"


    // $ANTLR start "rule__Operation__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1262:1: rule__Operation__Group__1 : rule__Operation__Group__1__Impl rule__Operation__Group__2 ;
    public final void rule__Operation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1266:1: ( rule__Operation__Group__1__Impl rule__Operation__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1267:2: rule__Operation__Group__1__Impl rule__Operation__Group__2
            {
            pushFollow(FOLLOW_rule__Operation__Group__1__Impl_in_rule__Operation__Group__12667);
            rule__Operation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Operation__Group__2_in_rule__Operation__Group__12670);
            rule__Operation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__1"


    // $ANTLR start "rule__Operation__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1274:1: rule__Operation__Group__1__Impl : ( ( rule__Operation__NameAssignment_1 ) ) ;
    public final void rule__Operation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1278:1: ( ( ( rule__Operation__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1279:1: ( ( rule__Operation__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1279:1: ( ( rule__Operation__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1280:1: ( rule__Operation__NameAssignment_1 )
            {
             before(grammarAccess.getOperationAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1281:1: ( rule__Operation__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1281:2: rule__Operation__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Operation__NameAssignment_1_in_rule__Operation__Group__1__Impl2697);
            rule__Operation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__1__Impl"


    // $ANTLR start "rule__Operation__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1291:1: rule__Operation__Group__2 : rule__Operation__Group__2__Impl ;
    public final void rule__Operation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1295:1: ( rule__Operation__Group__2__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1296:2: rule__Operation__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Operation__Group__2__Impl_in_rule__Operation__Group__22727);
            rule__Operation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__2"


    // $ANTLR start "rule__Operation__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1302:1: rule__Operation__Group__2__Impl : ( ';' ) ;
    public final void rule__Operation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1306:1: ( ( ';' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1307:1: ( ';' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1307:1: ( ';' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1308:1: ';'
            {
             before(grammarAccess.getOperationAccess().getSemicolonKeyword_2()); 
            match(input,34,FOLLOW_34_in_rule__Operation__Group__2__Impl2755); 
             after(grammarAccess.getOperationAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Group__2__Impl"


    // $ANTLR start "rule__Property__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1327:1: rule__Property__Group__0 : rule__Property__Group__0__Impl rule__Property__Group__1 ;
    public final void rule__Property__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1331:1: ( rule__Property__Group__0__Impl rule__Property__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1332:2: rule__Property__Group__0__Impl rule__Property__Group__1
            {
            pushFollow(FOLLOW_rule__Property__Group__0__Impl_in_rule__Property__Group__02792);
            rule__Property__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__1_in_rule__Property__Group__02795);
            rule__Property__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0"


    // $ANTLR start "rule__Property__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1339:1: rule__Property__Group__0__Impl : ( 'attribute' ) ;
    public final void rule__Property__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1343:1: ( ( 'attribute' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1344:1: ( 'attribute' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1344:1: ( 'attribute' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1345:1: 'attribute'
            {
             before(grammarAccess.getPropertyAccess().getAttributeKeyword_0()); 
            match(input,35,FOLLOW_35_in_rule__Property__Group__0__Impl2823); 
             after(grammarAccess.getPropertyAccess().getAttributeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0__Impl"


    // $ANTLR start "rule__Property__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1358:1: rule__Property__Group__1 : rule__Property__Group__1__Impl rule__Property__Group__2 ;
    public final void rule__Property__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1362:1: ( rule__Property__Group__1__Impl rule__Property__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1363:2: rule__Property__Group__1__Impl rule__Property__Group__2
            {
            pushFollow(FOLLOW_rule__Property__Group__1__Impl_in_rule__Property__Group__12854);
            rule__Property__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__2_in_rule__Property__Group__12857);
            rule__Property__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1"


    // $ANTLR start "rule__Property__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1370:1: rule__Property__Group__1__Impl : ( ( rule__Property__NameAssignment_1 ) ) ;
    public final void rule__Property__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1374:1: ( ( ( rule__Property__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1375:1: ( ( rule__Property__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1375:1: ( ( rule__Property__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1376:1: ( rule__Property__NameAssignment_1 )
            {
             before(grammarAccess.getPropertyAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1377:1: ( rule__Property__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1377:2: rule__Property__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Property__NameAssignment_1_in_rule__Property__Group__1__Impl2884);
            rule__Property__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1__Impl"


    // $ANTLR start "rule__Property__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1387:1: rule__Property__Group__2 : rule__Property__Group__2__Impl rule__Property__Group__3 ;
    public final void rule__Property__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1391:1: ( rule__Property__Group__2__Impl rule__Property__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1392:2: rule__Property__Group__2__Impl rule__Property__Group__3
            {
            pushFollow(FOLLOW_rule__Property__Group__2__Impl_in_rule__Property__Group__22914);
            rule__Property__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__3_in_rule__Property__Group__22917);
            rule__Property__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2"


    // $ANTLR start "rule__Property__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1399:1: rule__Property__Group__2__Impl : ( ( rule__Property__Group_2__0 )? ) ;
    public final void rule__Property__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1403:1: ( ( ( rule__Property__Group_2__0 )? ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1404:1: ( ( rule__Property__Group_2__0 )? )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1404:1: ( ( rule__Property__Group_2__0 )? )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1405:1: ( rule__Property__Group_2__0 )?
            {
             before(grammarAccess.getPropertyAccess().getGroup_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1406:1: ( rule__Property__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==39) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1406:2: rule__Property__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Property__Group_2__0_in_rule__Property__Group__2__Impl2944);
                    rule__Property__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2__Impl"


    // $ANTLR start "rule__Property__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1416:1: rule__Property__Group__3 : rule__Property__Group__3__Impl rule__Property__Group__4 ;
    public final void rule__Property__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1420:1: ( rule__Property__Group__3__Impl rule__Property__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1421:2: rule__Property__Group__3__Impl rule__Property__Group__4
            {
            pushFollow(FOLLOW_rule__Property__Group__3__Impl_in_rule__Property__Group__32975);
            rule__Property__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__4_in_rule__Property__Group__32978);
            rule__Property__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3"


    // $ANTLR start "rule__Property__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1428:1: rule__Property__Group__3__Impl : ( '(' ) ;
    public final void rule__Property__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1432:1: ( ( '(' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1433:1: ( '(' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1433:1: ( '(' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1434:1: '('
            {
             before(grammarAccess.getPropertyAccess().getLeftParenthesisKeyword_3()); 
            match(input,36,FOLLOW_36_in_rule__Property__Group__3__Impl3006); 
             after(grammarAccess.getPropertyAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3__Impl"


    // $ANTLR start "rule__Property__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1447:1: rule__Property__Group__4 : rule__Property__Group__4__Impl rule__Property__Group__5 ;
    public final void rule__Property__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1451:1: ( rule__Property__Group__4__Impl rule__Property__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1452:2: rule__Property__Group__4__Impl rule__Property__Group__5
            {
            pushFollow(FOLLOW_rule__Property__Group__4__Impl_in_rule__Property__Group__43037);
            rule__Property__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__5_in_rule__Property__Group__43040);
            rule__Property__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4"


    // $ANTLR start "rule__Property__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1459:1: rule__Property__Group__4__Impl : ( ( rule__Property__LowerValueAssignment_4 ) ) ;
    public final void rule__Property__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1463:1: ( ( ( rule__Property__LowerValueAssignment_4 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1464:1: ( ( rule__Property__LowerValueAssignment_4 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1464:1: ( ( rule__Property__LowerValueAssignment_4 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1465:1: ( rule__Property__LowerValueAssignment_4 )
            {
             before(grammarAccess.getPropertyAccess().getLowerValueAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1466:1: ( rule__Property__LowerValueAssignment_4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1466:2: rule__Property__LowerValueAssignment_4
            {
            pushFollow(FOLLOW_rule__Property__LowerValueAssignment_4_in_rule__Property__Group__4__Impl3067);
            rule__Property__LowerValueAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getLowerValueAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4__Impl"


    // $ANTLR start "rule__Property__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1476:1: rule__Property__Group__5 : rule__Property__Group__5__Impl rule__Property__Group__6 ;
    public final void rule__Property__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1480:1: ( rule__Property__Group__5__Impl rule__Property__Group__6 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1481:2: rule__Property__Group__5__Impl rule__Property__Group__6
            {
            pushFollow(FOLLOW_rule__Property__Group__5__Impl_in_rule__Property__Group__53097);
            rule__Property__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__6_in_rule__Property__Group__53100);
            rule__Property__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__5"


    // $ANTLR start "rule__Property__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1488:1: rule__Property__Group__5__Impl : ( ',' ) ;
    public final void rule__Property__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1492:1: ( ( ',' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1493:1: ( ',' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1493:1: ( ',' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1494:1: ','
            {
             before(grammarAccess.getPropertyAccess().getCommaKeyword_5()); 
            match(input,37,FOLLOW_37_in_rule__Property__Group__5__Impl3128); 
             after(grammarAccess.getPropertyAccess().getCommaKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__5__Impl"


    // $ANTLR start "rule__Property__Group__6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1507:1: rule__Property__Group__6 : rule__Property__Group__6__Impl rule__Property__Group__7 ;
    public final void rule__Property__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1511:1: ( rule__Property__Group__6__Impl rule__Property__Group__7 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1512:2: rule__Property__Group__6__Impl rule__Property__Group__7
            {
            pushFollow(FOLLOW_rule__Property__Group__6__Impl_in_rule__Property__Group__63159);
            rule__Property__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__7_in_rule__Property__Group__63162);
            rule__Property__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__6"


    // $ANTLR start "rule__Property__Group__6__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1519:1: rule__Property__Group__6__Impl : ( ( rule__Property__UpperValueAssignment_6 ) ) ;
    public final void rule__Property__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1523:1: ( ( ( rule__Property__UpperValueAssignment_6 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1524:1: ( ( rule__Property__UpperValueAssignment_6 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1524:1: ( ( rule__Property__UpperValueAssignment_6 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1525:1: ( rule__Property__UpperValueAssignment_6 )
            {
             before(grammarAccess.getPropertyAccess().getUpperValueAssignment_6()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1526:1: ( rule__Property__UpperValueAssignment_6 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1526:2: rule__Property__UpperValueAssignment_6
            {
            pushFollow(FOLLOW_rule__Property__UpperValueAssignment_6_in_rule__Property__Group__6__Impl3189);
            rule__Property__UpperValueAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getUpperValueAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__6__Impl"


    // $ANTLR start "rule__Property__Group__7"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1536:1: rule__Property__Group__7 : rule__Property__Group__7__Impl rule__Property__Group__8 ;
    public final void rule__Property__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1540:1: ( rule__Property__Group__7__Impl rule__Property__Group__8 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1541:2: rule__Property__Group__7__Impl rule__Property__Group__8
            {
            pushFollow(FOLLOW_rule__Property__Group__7__Impl_in_rule__Property__Group__73219);
            rule__Property__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group__8_in_rule__Property__Group__73222);
            rule__Property__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__7"


    // $ANTLR start "rule__Property__Group__7__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1548:1: rule__Property__Group__7__Impl : ( ')' ) ;
    public final void rule__Property__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1552:1: ( ( ')' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1553:1: ( ')' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1553:1: ( ')' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1554:1: ')'
            {
             before(grammarAccess.getPropertyAccess().getRightParenthesisKeyword_7()); 
            match(input,38,FOLLOW_38_in_rule__Property__Group__7__Impl3250); 
             after(grammarAccess.getPropertyAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__7__Impl"


    // $ANTLR start "rule__Property__Group__8"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1567:1: rule__Property__Group__8 : rule__Property__Group__8__Impl ;
    public final void rule__Property__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1571:1: ( rule__Property__Group__8__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1572:2: rule__Property__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__Property__Group__8__Impl_in_rule__Property__Group__83281);
            rule__Property__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__8"


    // $ANTLR start "rule__Property__Group__8__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1578:1: rule__Property__Group__8__Impl : ( ';' ) ;
    public final void rule__Property__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1582:1: ( ( ';' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1583:1: ( ';' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1583:1: ( ';' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1584:1: ';'
            {
             before(grammarAccess.getPropertyAccess().getSemicolonKeyword_8()); 
            match(input,34,FOLLOW_34_in_rule__Property__Group__8__Impl3309); 
             after(grammarAccess.getPropertyAccess().getSemicolonKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__8__Impl"


    // $ANTLR start "rule__Property__Group_2__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1615:1: rule__Property__Group_2__0 : rule__Property__Group_2__0__Impl rule__Property__Group_2__1 ;
    public final void rule__Property__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1619:1: ( rule__Property__Group_2__0__Impl rule__Property__Group_2__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1620:2: rule__Property__Group_2__0__Impl rule__Property__Group_2__1
            {
            pushFollow(FOLLOW_rule__Property__Group_2__0__Impl_in_rule__Property__Group_2__03358);
            rule__Property__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Property__Group_2__1_in_rule__Property__Group_2__03361);
            rule__Property__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__0"


    // $ANTLR start "rule__Property__Group_2__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1627:1: rule__Property__Group_2__0__Impl : ( 'type=' ) ;
    public final void rule__Property__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1631:1: ( ( 'type=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1632:1: ( 'type=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1632:1: ( 'type=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1633:1: 'type='
            {
             before(grammarAccess.getPropertyAccess().getTypeKeyword_2_0()); 
            match(input,39,FOLLOW_39_in_rule__Property__Group_2__0__Impl3389); 
             after(grammarAccess.getPropertyAccess().getTypeKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__0__Impl"


    // $ANTLR start "rule__Property__Group_2__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1646:1: rule__Property__Group_2__1 : rule__Property__Group_2__1__Impl ;
    public final void rule__Property__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1650:1: ( rule__Property__Group_2__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1651:2: rule__Property__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Property__Group_2__1__Impl_in_rule__Property__Group_2__13420);
            rule__Property__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__1"


    // $ANTLR start "rule__Property__Group_2__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1657:1: rule__Property__Group_2__1__Impl : ( ( rule__Property__TypeAssignment_2_1 ) ) ;
    public final void rule__Property__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1661:1: ( ( ( rule__Property__TypeAssignment_2_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1662:1: ( ( rule__Property__TypeAssignment_2_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1662:1: ( ( rule__Property__TypeAssignment_2_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1663:1: ( rule__Property__TypeAssignment_2_1 )
            {
             before(grammarAccess.getPropertyAccess().getTypeAssignment_2_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1664:1: ( rule__Property__TypeAssignment_2_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1664:2: rule__Property__TypeAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Property__TypeAssignment_2_1_in_rule__Property__Group_2__1__Impl3447);
            rule__Property__TypeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getTypeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__1__Impl"


    // $ANTLR start "rule__Collaboration__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1678:1: rule__Collaboration__Group__0 : rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1 ;
    public final void rule__Collaboration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1682:1: ( rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1683:2: rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__0__Impl_in_rule__Collaboration__Group__03481);
            rule__Collaboration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Collaboration__Group__1_in_rule__Collaboration__Group__03484);
            rule__Collaboration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__0"


    // $ANTLR start "rule__Collaboration__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1690:1: rule__Collaboration__Group__0__Impl : ( 'collaboration' ) ;
    public final void rule__Collaboration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1694:1: ( ( 'collaboration' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1695:1: ( 'collaboration' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1695:1: ( 'collaboration' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1696:1: 'collaboration'
            {
             before(grammarAccess.getCollaborationAccess().getCollaborationKeyword_0()); 
            match(input,40,FOLLOW_40_in_rule__Collaboration__Group__0__Impl3512); 
             after(grammarAccess.getCollaborationAccess().getCollaborationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__0__Impl"


    // $ANTLR start "rule__Collaboration__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1709:1: rule__Collaboration__Group__1 : rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2 ;
    public final void rule__Collaboration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1713:1: ( rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1714:2: rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__1__Impl_in_rule__Collaboration__Group__13543);
            rule__Collaboration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Collaboration__Group__2_in_rule__Collaboration__Group__13546);
            rule__Collaboration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__1"


    // $ANTLR start "rule__Collaboration__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1721:1: rule__Collaboration__Group__1__Impl : ( ( rule__Collaboration__NameAssignment_1 ) ) ;
    public final void rule__Collaboration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1725:1: ( ( ( rule__Collaboration__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1726:1: ( ( rule__Collaboration__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1726:1: ( ( rule__Collaboration__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1727:1: ( rule__Collaboration__NameAssignment_1 )
            {
             before(grammarAccess.getCollaborationAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1728:1: ( rule__Collaboration__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1728:2: rule__Collaboration__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Collaboration__NameAssignment_1_in_rule__Collaboration__Group__1__Impl3573);
            rule__Collaboration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCollaborationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__1__Impl"


    // $ANTLR start "rule__Collaboration__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1738:1: rule__Collaboration__Group__2 : rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3 ;
    public final void rule__Collaboration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1742:1: ( rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1743:2: rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__2__Impl_in_rule__Collaboration__Group__23603);
            rule__Collaboration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Collaboration__Group__3_in_rule__Collaboration__Group__23606);
            rule__Collaboration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__2"


    // $ANTLR start "rule__Collaboration__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1750:1: rule__Collaboration__Group__2__Impl : ( '{' ) ;
    public final void rule__Collaboration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1754:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1755:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1755:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1756:1: '{'
            {
             before(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,30,FOLLOW_30_in_rule__Collaboration__Group__2__Impl3634); 
             after(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__2__Impl"


    // $ANTLR start "rule__Collaboration__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1769:1: rule__Collaboration__Group__3 : rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4 ;
    public final void rule__Collaboration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1773:1: ( rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1774:2: rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__3__Impl_in_rule__Collaboration__Group__33665);
            rule__Collaboration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Collaboration__Group__4_in_rule__Collaboration__Group__33668);
            rule__Collaboration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__3"


    // $ANTLR start "rule__Collaboration__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1781:1: rule__Collaboration__Group__3__Impl : ( ( rule__Collaboration__OwnedAttributeAssignment_3 )* ) ;
    public final void rule__Collaboration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1785:1: ( ( ( rule__Collaboration__OwnedAttributeAssignment_3 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1786:1: ( ( rule__Collaboration__OwnedAttributeAssignment_3 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1786:1: ( ( rule__Collaboration__OwnedAttributeAssignment_3 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1787:1: ( rule__Collaboration__OwnedAttributeAssignment_3 )*
            {
             before(grammarAccess.getCollaborationAccess().getOwnedAttributeAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1788:1: ( rule__Collaboration__OwnedAttributeAssignment_3 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==35) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1788:2: rule__Collaboration__OwnedAttributeAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Collaboration__OwnedAttributeAssignment_3_in_rule__Collaboration__Group__3__Impl3695);
            	    rule__Collaboration__OwnedAttributeAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getCollaborationAccess().getOwnedAttributeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__3__Impl"


    // $ANTLR start "rule__Collaboration__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1798:1: rule__Collaboration__Group__4 : rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5 ;
    public final void rule__Collaboration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1802:1: ( rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1803:2: rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__4__Impl_in_rule__Collaboration__Group__43726);
            rule__Collaboration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Collaboration__Group__5_in_rule__Collaboration__Group__43729);
            rule__Collaboration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__4"


    // $ANTLR start "rule__Collaboration__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1810:1: rule__Collaboration__Group__4__Impl : ( ( rule__Collaboration__OwnedBehaviorAssignment_4 )* ) ;
    public final void rule__Collaboration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1814:1: ( ( ( rule__Collaboration__OwnedBehaviorAssignment_4 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1815:1: ( ( rule__Collaboration__OwnedBehaviorAssignment_4 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1815:1: ( ( rule__Collaboration__OwnedBehaviorAssignment_4 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1816:1: ( rule__Collaboration__OwnedBehaviorAssignment_4 )*
            {
             before(grammarAccess.getCollaborationAccess().getOwnedBehaviorAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1817:1: ( rule__Collaboration__OwnedBehaviorAssignment_4 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==41) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1817:2: rule__Collaboration__OwnedBehaviorAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Collaboration__OwnedBehaviorAssignment_4_in_rule__Collaboration__Group__4__Impl3756);
            	    rule__Collaboration__OwnedBehaviorAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getCollaborationAccess().getOwnedBehaviorAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__4__Impl"


    // $ANTLR start "rule__Collaboration__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1827:1: rule__Collaboration__Group__5 : rule__Collaboration__Group__5__Impl ;
    public final void rule__Collaboration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1831:1: ( rule__Collaboration__Group__5__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1832:2: rule__Collaboration__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Collaboration__Group__5__Impl_in_rule__Collaboration__Group__53787);
            rule__Collaboration__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__5"


    // $ANTLR start "rule__Collaboration__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1838:1: rule__Collaboration__Group__5__Impl : ( '}' ) ;
    public final void rule__Collaboration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1842:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1843:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1843:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1844:1: '}'
            {
             before(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_5()); 
            match(input,31,FOLLOW_31_in_rule__Collaboration__Group__5__Impl3815); 
             after(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__5__Impl"


    // $ANTLR start "rule__Interaction__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1869:1: rule__Interaction__Group__0 : rule__Interaction__Group__0__Impl rule__Interaction__Group__1 ;
    public final void rule__Interaction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1873:1: ( rule__Interaction__Group__0__Impl rule__Interaction__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1874:2: rule__Interaction__Group__0__Impl rule__Interaction__Group__1
            {
            pushFollow(FOLLOW_rule__Interaction__Group__0__Impl_in_rule__Interaction__Group__03858);
            rule__Interaction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__1_in_rule__Interaction__Group__03861);
            rule__Interaction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0"


    // $ANTLR start "rule__Interaction__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1881:1: rule__Interaction__Group__0__Impl : ( 'ownedBehavior' ) ;
    public final void rule__Interaction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1885:1: ( ( 'ownedBehavior' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1886:1: ( 'ownedBehavior' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1886:1: ( 'ownedBehavior' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1887:1: 'ownedBehavior'
            {
             before(grammarAccess.getInteractionAccess().getOwnedBehaviorKeyword_0()); 
            match(input,41,FOLLOW_41_in_rule__Interaction__Group__0__Impl3889); 
             after(grammarAccess.getInteractionAccess().getOwnedBehaviorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0__Impl"


    // $ANTLR start "rule__Interaction__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1900:1: rule__Interaction__Group__1 : rule__Interaction__Group__1__Impl rule__Interaction__Group__2 ;
    public final void rule__Interaction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1904:1: ( rule__Interaction__Group__1__Impl rule__Interaction__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1905:2: rule__Interaction__Group__1__Impl rule__Interaction__Group__2
            {
            pushFollow(FOLLOW_rule__Interaction__Group__1__Impl_in_rule__Interaction__Group__13920);
            rule__Interaction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__2_in_rule__Interaction__Group__13923);
            rule__Interaction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1"


    // $ANTLR start "rule__Interaction__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1912:1: rule__Interaction__Group__1__Impl : ( ( rule__Interaction__NameAssignment_1 ) ) ;
    public final void rule__Interaction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1916:1: ( ( ( rule__Interaction__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1917:1: ( ( rule__Interaction__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1917:1: ( ( rule__Interaction__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1918:1: ( rule__Interaction__NameAssignment_1 )
            {
             before(grammarAccess.getInteractionAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1919:1: ( rule__Interaction__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1919:2: rule__Interaction__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Interaction__NameAssignment_1_in_rule__Interaction__Group__1__Impl3950);
            rule__Interaction__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1__Impl"


    // $ANTLR start "rule__Interaction__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1929:1: rule__Interaction__Group__2 : rule__Interaction__Group__2__Impl rule__Interaction__Group__3 ;
    public final void rule__Interaction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1933:1: ( rule__Interaction__Group__2__Impl rule__Interaction__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1934:2: rule__Interaction__Group__2__Impl rule__Interaction__Group__3
            {
            pushFollow(FOLLOW_rule__Interaction__Group__2__Impl_in_rule__Interaction__Group__23980);
            rule__Interaction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__3_in_rule__Interaction__Group__23983);
            rule__Interaction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2"


    // $ANTLR start "rule__Interaction__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1941:1: rule__Interaction__Group__2__Impl : ( '{' ) ;
    public final void rule__Interaction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1945:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1946:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1946:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1947:1: '{'
            {
             before(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,30,FOLLOW_30_in_rule__Interaction__Group__2__Impl4011); 
             after(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2__Impl"


    // $ANTLR start "rule__Interaction__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1960:1: rule__Interaction__Group__3 : rule__Interaction__Group__3__Impl rule__Interaction__Group__4 ;
    public final void rule__Interaction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1964:1: ( rule__Interaction__Group__3__Impl rule__Interaction__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1965:2: rule__Interaction__Group__3__Impl rule__Interaction__Group__4
            {
            pushFollow(FOLLOW_rule__Interaction__Group__3__Impl_in_rule__Interaction__Group__34042);
            rule__Interaction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__4_in_rule__Interaction__Group__34045);
            rule__Interaction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3"


    // $ANTLR start "rule__Interaction__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1972:1: rule__Interaction__Group__3__Impl : ( ( rule__Interaction__OwnedCommentAssignment_3 )* ) ;
    public final void rule__Interaction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1976:1: ( ( ( rule__Interaction__OwnedCommentAssignment_3 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1977:1: ( ( rule__Interaction__OwnedCommentAssignment_3 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1977:1: ( ( rule__Interaction__OwnedCommentAssignment_3 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1978:1: ( rule__Interaction__OwnedCommentAssignment_3 )*
            {
             before(grammarAccess.getInteractionAccess().getOwnedCommentAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1979:1: ( rule__Interaction__OwnedCommentAssignment_3 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==44) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1979:2: rule__Interaction__OwnedCommentAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Interaction__OwnedCommentAssignment_3_in_rule__Interaction__Group__3__Impl4072);
            	    rule__Interaction__OwnedCommentAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getOwnedCommentAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3__Impl"


    // $ANTLR start "rule__Interaction__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1989:1: rule__Interaction__Group__4 : rule__Interaction__Group__4__Impl rule__Interaction__Group__5 ;
    public final void rule__Interaction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1993:1: ( rule__Interaction__Group__4__Impl rule__Interaction__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:1994:2: rule__Interaction__Group__4__Impl rule__Interaction__Group__5
            {
            pushFollow(FOLLOW_rule__Interaction__Group__4__Impl_in_rule__Interaction__Group__44103);
            rule__Interaction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__5_in_rule__Interaction__Group__44106);
            rule__Interaction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4"


    // $ANTLR start "rule__Interaction__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2001:1: rule__Interaction__Group__4__Impl : ( ( rule__Interaction__LifelineAssignment_4 )* ) ;
    public final void rule__Interaction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2005:1: ( ( ( rule__Interaction__LifelineAssignment_4 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2006:1: ( ( rule__Interaction__LifelineAssignment_4 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2006:1: ( ( rule__Interaction__LifelineAssignment_4 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2007:1: ( rule__Interaction__LifelineAssignment_4 )*
            {
             before(grammarAccess.getInteractionAccess().getLifelineAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2008:1: ( rule__Interaction__LifelineAssignment_4 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==47) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2008:2: rule__Interaction__LifelineAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Interaction__LifelineAssignment_4_in_rule__Interaction__Group__4__Impl4133);
            	    rule__Interaction__LifelineAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getLifelineAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4__Impl"


    // $ANTLR start "rule__Interaction__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2018:1: rule__Interaction__Group__5 : rule__Interaction__Group__5__Impl rule__Interaction__Group__6 ;
    public final void rule__Interaction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2022:1: ( rule__Interaction__Group__5__Impl rule__Interaction__Group__6 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2023:2: rule__Interaction__Group__5__Impl rule__Interaction__Group__6
            {
            pushFollow(FOLLOW_rule__Interaction__Group__5__Impl_in_rule__Interaction__Group__54164);
            rule__Interaction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__6_in_rule__Interaction__Group__54167);
            rule__Interaction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__5"


    // $ANTLR start "rule__Interaction__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2030:1: rule__Interaction__Group__5__Impl : ( ( rule__Interaction__MessageAssignment_5 )* ) ;
    public final void rule__Interaction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2034:1: ( ( ( rule__Interaction__MessageAssignment_5 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2035:1: ( ( rule__Interaction__MessageAssignment_5 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2035:1: ( ( rule__Interaction__MessageAssignment_5 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2036:1: ( rule__Interaction__MessageAssignment_5 )*
            {
             before(grammarAccess.getInteractionAccess().getMessageAssignment_5()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2037:1: ( rule__Interaction__MessageAssignment_5 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==49) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2037:2: rule__Interaction__MessageAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__Interaction__MessageAssignment_5_in_rule__Interaction__Group__5__Impl4194);
            	    rule__Interaction__MessageAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getMessageAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__5__Impl"


    // $ANTLR start "rule__Interaction__Group__6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2047:1: rule__Interaction__Group__6 : rule__Interaction__Group__6__Impl rule__Interaction__Group__7 ;
    public final void rule__Interaction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2051:1: ( rule__Interaction__Group__6__Impl rule__Interaction__Group__7 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2052:2: rule__Interaction__Group__6__Impl rule__Interaction__Group__7
            {
            pushFollow(FOLLOW_rule__Interaction__Group__6__Impl_in_rule__Interaction__Group__64225);
            rule__Interaction__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Interaction__Group__7_in_rule__Interaction__Group__64228);
            rule__Interaction__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__6"


    // $ANTLR start "rule__Interaction__Group__6__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2059:1: rule__Interaction__Group__6__Impl : ( ( rule__Interaction__FragmentAssignment_6 )* ) ;
    public final void rule__Interaction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2063:1: ( ( ( rule__Interaction__FragmentAssignment_6 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2064:1: ( ( rule__Interaction__FragmentAssignment_6 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2064:1: ( ( rule__Interaction__FragmentAssignment_6 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2065:1: ( rule__Interaction__FragmentAssignment_6 )*
            {
             before(grammarAccess.getInteractionAccess().getFragmentAssignment_6()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2066:1: ( rule__Interaction__FragmentAssignment_6 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==42||LA15_0==53) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2066:2: rule__Interaction__FragmentAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__Interaction__FragmentAssignment_6_in_rule__Interaction__Group__6__Impl4255);
            	    rule__Interaction__FragmentAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getFragmentAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__6__Impl"


    // $ANTLR start "rule__Interaction__Group__7"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2076:1: rule__Interaction__Group__7 : rule__Interaction__Group__7__Impl ;
    public final void rule__Interaction__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2080:1: ( rule__Interaction__Group__7__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2081:2: rule__Interaction__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Interaction__Group__7__Impl_in_rule__Interaction__Group__74286);
            rule__Interaction__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__7"


    // $ANTLR start "rule__Interaction__Group__7__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2087:1: rule__Interaction__Group__7__Impl : ( '}' ) ;
    public final void rule__Interaction__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2091:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2092:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2092:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2093:1: '}'
            {
             before(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_7()); 
            match(input,31,FOLLOW_31_in_rule__Interaction__Group__7__Impl4314); 
             after(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__7__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2122:1: rule__CombinedFragment__Group__0 : rule__CombinedFragment__Group__0__Impl rule__CombinedFragment__Group__1 ;
    public final void rule__CombinedFragment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2126:1: ( rule__CombinedFragment__Group__0__Impl rule__CombinedFragment__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2127:2: rule__CombinedFragment__Group__0__Impl rule__CombinedFragment__Group__1
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__0__Impl_in_rule__CombinedFragment__Group__04361);
            rule__CombinedFragment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group__1_in_rule__CombinedFragment__Group__04364);
            rule__CombinedFragment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__0"


    // $ANTLR start "rule__CombinedFragment__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2134:1: rule__CombinedFragment__Group__0__Impl : ( 'combinedFragment' ) ;
    public final void rule__CombinedFragment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2138:1: ( ( 'combinedFragment' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2139:1: ( 'combinedFragment' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2139:1: ( 'combinedFragment' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2140:1: 'combinedFragment'
            {
             before(grammarAccess.getCombinedFragmentAccess().getCombinedFragmentKeyword_0()); 
            match(input,42,FOLLOW_42_in_rule__CombinedFragment__Group__0__Impl4392); 
             after(grammarAccess.getCombinedFragmentAccess().getCombinedFragmentKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__0__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2153:1: rule__CombinedFragment__Group__1 : rule__CombinedFragment__Group__1__Impl rule__CombinedFragment__Group__2 ;
    public final void rule__CombinedFragment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2157:1: ( rule__CombinedFragment__Group__1__Impl rule__CombinedFragment__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2158:2: rule__CombinedFragment__Group__1__Impl rule__CombinedFragment__Group__2
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__1__Impl_in_rule__CombinedFragment__Group__14423);
            rule__CombinedFragment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group__2_in_rule__CombinedFragment__Group__14426);
            rule__CombinedFragment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__1"


    // $ANTLR start "rule__CombinedFragment__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2165:1: rule__CombinedFragment__Group__1__Impl : ( '{' ) ;
    public final void rule__CombinedFragment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2169:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2170:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2170:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2171:1: '{'
            {
             before(grammarAccess.getCombinedFragmentAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,30,FOLLOW_30_in_rule__CombinedFragment__Group__1__Impl4454); 
             after(grammarAccess.getCombinedFragmentAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__1__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2184:1: rule__CombinedFragment__Group__2 : rule__CombinedFragment__Group__2__Impl rule__CombinedFragment__Group__3 ;
    public final void rule__CombinedFragment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2188:1: ( rule__CombinedFragment__Group__2__Impl rule__CombinedFragment__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2189:2: rule__CombinedFragment__Group__2__Impl rule__CombinedFragment__Group__3
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__2__Impl_in_rule__CombinedFragment__Group__24485);
            rule__CombinedFragment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group__3_in_rule__CombinedFragment__Group__24488);
            rule__CombinedFragment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__2"


    // $ANTLR start "rule__CombinedFragment__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2196:1: rule__CombinedFragment__Group__2__Impl : ( ( rule__CombinedFragment__InteractionOperatorAssignment_2 ) ) ;
    public final void rule__CombinedFragment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2200:1: ( ( ( rule__CombinedFragment__InteractionOperatorAssignment_2 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2201:1: ( ( rule__CombinedFragment__InteractionOperatorAssignment_2 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2201:1: ( ( rule__CombinedFragment__InteractionOperatorAssignment_2 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2202:1: ( rule__CombinedFragment__InteractionOperatorAssignment_2 )
            {
             before(grammarAccess.getCombinedFragmentAccess().getInteractionOperatorAssignment_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2203:1: ( rule__CombinedFragment__InteractionOperatorAssignment_2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2203:2: rule__CombinedFragment__InteractionOperatorAssignment_2
            {
            pushFollow(FOLLOW_rule__CombinedFragment__InteractionOperatorAssignment_2_in_rule__CombinedFragment__Group__2__Impl4515);
            rule__CombinedFragment__InteractionOperatorAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCombinedFragmentAccess().getInteractionOperatorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__2__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2213:1: rule__CombinedFragment__Group__3 : rule__CombinedFragment__Group__3__Impl rule__CombinedFragment__Group__4 ;
    public final void rule__CombinedFragment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2217:1: ( rule__CombinedFragment__Group__3__Impl rule__CombinedFragment__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2218:2: rule__CombinedFragment__Group__3__Impl rule__CombinedFragment__Group__4
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__3__Impl_in_rule__CombinedFragment__Group__34545);
            rule__CombinedFragment__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group__4_in_rule__CombinedFragment__Group__34548);
            rule__CombinedFragment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__3"


    // $ANTLR start "rule__CombinedFragment__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2225:1: rule__CombinedFragment__Group__3__Impl : ( ( rule__CombinedFragment__Group_3__0 )* ) ;
    public final void rule__CombinedFragment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2229:1: ( ( ( rule__CombinedFragment__Group_3__0 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2230:1: ( ( rule__CombinedFragment__Group_3__0 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2230:1: ( ( rule__CombinedFragment__Group_3__0 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2231:1: ( rule__CombinedFragment__Group_3__0 )*
            {
             before(grammarAccess.getCombinedFragmentAccess().getGroup_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2232:1: ( rule__CombinedFragment__Group_3__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==43) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2232:2: rule__CombinedFragment__Group_3__0
            	    {
            	    pushFollow(FOLLOW_rule__CombinedFragment__Group_3__0_in_rule__CombinedFragment__Group__3__Impl4575);
            	    rule__CombinedFragment__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getCombinedFragmentAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__3__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2242:1: rule__CombinedFragment__Group__4 : rule__CombinedFragment__Group__4__Impl rule__CombinedFragment__Group__5 ;
    public final void rule__CombinedFragment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2246:1: ( rule__CombinedFragment__Group__4__Impl rule__CombinedFragment__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2247:2: rule__CombinedFragment__Group__4__Impl rule__CombinedFragment__Group__5
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__4__Impl_in_rule__CombinedFragment__Group__44606);
            rule__CombinedFragment__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group__5_in_rule__CombinedFragment__Group__44609);
            rule__CombinedFragment__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__4"


    // $ANTLR start "rule__CombinedFragment__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2254:1: rule__CombinedFragment__Group__4__Impl : ( ( ( rule__CombinedFragment__OperandAssignment_4 ) ) ( ( rule__CombinedFragment__OperandAssignment_4 )* ) ) ;
    public final void rule__CombinedFragment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2258:1: ( ( ( ( rule__CombinedFragment__OperandAssignment_4 ) ) ( ( rule__CombinedFragment__OperandAssignment_4 )* ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2259:1: ( ( ( rule__CombinedFragment__OperandAssignment_4 ) ) ( ( rule__CombinedFragment__OperandAssignment_4 )* ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2259:1: ( ( ( rule__CombinedFragment__OperandAssignment_4 ) ) ( ( rule__CombinedFragment__OperandAssignment_4 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2260:1: ( ( rule__CombinedFragment__OperandAssignment_4 ) ) ( ( rule__CombinedFragment__OperandAssignment_4 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2260:1: ( ( rule__CombinedFragment__OperandAssignment_4 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2261:1: ( rule__CombinedFragment__OperandAssignment_4 )
            {
             before(grammarAccess.getCombinedFragmentAccess().getOperandAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2262:1: ( rule__CombinedFragment__OperandAssignment_4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2262:2: rule__CombinedFragment__OperandAssignment_4
            {
            pushFollow(FOLLOW_rule__CombinedFragment__OperandAssignment_4_in_rule__CombinedFragment__Group__4__Impl4638);
            rule__CombinedFragment__OperandAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getCombinedFragmentAccess().getOperandAssignment_4()); 

            }

            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2265:1: ( ( rule__CombinedFragment__OperandAssignment_4 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2266:1: ( rule__CombinedFragment__OperandAssignment_4 )*
            {
             before(grammarAccess.getCombinedFragmentAccess().getOperandAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2267:1: ( rule__CombinedFragment__OperandAssignment_4 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==45) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2267:2: rule__CombinedFragment__OperandAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__CombinedFragment__OperandAssignment_4_in_rule__CombinedFragment__Group__4__Impl4650);
            	    rule__CombinedFragment__OperandAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getCombinedFragmentAccess().getOperandAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__4__Impl"


    // $ANTLR start "rule__CombinedFragment__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2278:1: rule__CombinedFragment__Group__5 : rule__CombinedFragment__Group__5__Impl ;
    public final void rule__CombinedFragment__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2282:1: ( rule__CombinedFragment__Group__5__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2283:2: rule__CombinedFragment__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group__5__Impl_in_rule__CombinedFragment__Group__54683);
            rule__CombinedFragment__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__5"


    // $ANTLR start "rule__CombinedFragment__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2289:1: rule__CombinedFragment__Group__5__Impl : ( '}' ) ;
    public final void rule__CombinedFragment__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2293:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2294:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2294:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2295:1: '}'
            {
             before(grammarAccess.getCombinedFragmentAccess().getRightCurlyBracketKeyword_5()); 
            match(input,31,FOLLOW_31_in_rule__CombinedFragment__Group__5__Impl4711); 
             after(grammarAccess.getCombinedFragmentAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group__5__Impl"


    // $ANTLR start "rule__CombinedFragment__Group_3__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2320:1: rule__CombinedFragment__Group_3__0 : rule__CombinedFragment__Group_3__0__Impl rule__CombinedFragment__Group_3__1 ;
    public final void rule__CombinedFragment__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2324:1: ( rule__CombinedFragment__Group_3__0__Impl rule__CombinedFragment__Group_3__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2325:2: rule__CombinedFragment__Group_3__0__Impl rule__CombinedFragment__Group_3__1
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group_3__0__Impl_in_rule__CombinedFragment__Group_3__04754);
            rule__CombinedFragment__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CombinedFragment__Group_3__1_in_rule__CombinedFragment__Group_3__04757);
            rule__CombinedFragment__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group_3__0"


    // $ANTLR start "rule__CombinedFragment__Group_3__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2332:1: rule__CombinedFragment__Group_3__0__Impl : ( 'covered=' ) ;
    public final void rule__CombinedFragment__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2336:1: ( ( 'covered=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2337:1: ( 'covered=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2337:1: ( 'covered=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2338:1: 'covered='
            {
             before(grammarAccess.getCombinedFragmentAccess().getCoveredKeyword_3_0()); 
            match(input,43,FOLLOW_43_in_rule__CombinedFragment__Group_3__0__Impl4785); 
             after(grammarAccess.getCombinedFragmentAccess().getCoveredKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group_3__0__Impl"


    // $ANTLR start "rule__CombinedFragment__Group_3__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2351:1: rule__CombinedFragment__Group_3__1 : rule__CombinedFragment__Group_3__1__Impl ;
    public final void rule__CombinedFragment__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2355:1: ( rule__CombinedFragment__Group_3__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2356:2: rule__CombinedFragment__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__CombinedFragment__Group_3__1__Impl_in_rule__CombinedFragment__Group_3__14816);
            rule__CombinedFragment__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group_3__1"


    // $ANTLR start "rule__CombinedFragment__Group_3__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2362:1: rule__CombinedFragment__Group_3__1__Impl : ( ( rule__CombinedFragment__CoveredAssignment_3_1 ) ) ;
    public final void rule__CombinedFragment__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2366:1: ( ( ( rule__CombinedFragment__CoveredAssignment_3_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2367:1: ( ( rule__CombinedFragment__CoveredAssignment_3_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2367:1: ( ( rule__CombinedFragment__CoveredAssignment_3_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2368:1: ( rule__CombinedFragment__CoveredAssignment_3_1 )
            {
             before(grammarAccess.getCombinedFragmentAccess().getCoveredAssignment_3_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2369:1: ( rule__CombinedFragment__CoveredAssignment_3_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2369:2: rule__CombinedFragment__CoveredAssignment_3_1
            {
            pushFollow(FOLLOW_rule__CombinedFragment__CoveredAssignment_3_1_in_rule__CombinedFragment__Group_3__1__Impl4843);
            rule__CombinedFragment__CoveredAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCombinedFragmentAccess().getCoveredAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__Group_3__1__Impl"


    // $ANTLR start "rule__Comment__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2383:1: rule__Comment__Group__0 : rule__Comment__Group__0__Impl rule__Comment__Group__1 ;
    public final void rule__Comment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2387:1: ( rule__Comment__Group__0__Impl rule__Comment__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2388:2: rule__Comment__Group__0__Impl rule__Comment__Group__1
            {
            pushFollow(FOLLOW_rule__Comment__Group__0__Impl_in_rule__Comment__Group__04877);
            rule__Comment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Comment__Group__1_in_rule__Comment__Group__04880);
            rule__Comment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__0"


    // $ANTLR start "rule__Comment__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2395:1: rule__Comment__Group__0__Impl : ( 'comment' ) ;
    public final void rule__Comment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2399:1: ( ( 'comment' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2400:1: ( 'comment' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2400:1: ( 'comment' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2401:1: 'comment'
            {
             before(grammarAccess.getCommentAccess().getCommentKeyword_0()); 
            match(input,44,FOLLOW_44_in_rule__Comment__Group__0__Impl4908); 
             after(grammarAccess.getCommentAccess().getCommentKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__0__Impl"


    // $ANTLR start "rule__Comment__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2414:1: rule__Comment__Group__1 : rule__Comment__Group__1__Impl rule__Comment__Group__2 ;
    public final void rule__Comment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2418:1: ( rule__Comment__Group__1__Impl rule__Comment__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2419:2: rule__Comment__Group__1__Impl rule__Comment__Group__2
            {
            pushFollow(FOLLOW_rule__Comment__Group__1__Impl_in_rule__Comment__Group__14939);
            rule__Comment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Comment__Group__2_in_rule__Comment__Group__14942);
            rule__Comment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__1"


    // $ANTLR start "rule__Comment__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2426:1: rule__Comment__Group__1__Impl : ( '{' ) ;
    public final void rule__Comment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2430:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2431:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2431:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2432:1: '{'
            {
             before(grammarAccess.getCommentAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,30,FOLLOW_30_in_rule__Comment__Group__1__Impl4970); 
             after(grammarAccess.getCommentAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__1__Impl"


    // $ANTLR start "rule__Comment__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2445:1: rule__Comment__Group__2 : rule__Comment__Group__2__Impl rule__Comment__Group__3 ;
    public final void rule__Comment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2449:1: ( rule__Comment__Group__2__Impl rule__Comment__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2450:2: rule__Comment__Group__2__Impl rule__Comment__Group__3
            {
            pushFollow(FOLLOW_rule__Comment__Group__2__Impl_in_rule__Comment__Group__25001);
            rule__Comment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Comment__Group__3_in_rule__Comment__Group__25004);
            rule__Comment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__2"


    // $ANTLR start "rule__Comment__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2457:1: rule__Comment__Group__2__Impl : ( ( rule__Comment__BodyAssignment_2 ) ) ;
    public final void rule__Comment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2461:1: ( ( ( rule__Comment__BodyAssignment_2 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2462:1: ( ( rule__Comment__BodyAssignment_2 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2462:1: ( ( rule__Comment__BodyAssignment_2 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2463:1: ( rule__Comment__BodyAssignment_2 )
            {
             before(grammarAccess.getCommentAccess().getBodyAssignment_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2464:1: ( rule__Comment__BodyAssignment_2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2464:2: rule__Comment__BodyAssignment_2
            {
            pushFollow(FOLLOW_rule__Comment__BodyAssignment_2_in_rule__Comment__Group__2__Impl5031);
            rule__Comment__BodyAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCommentAccess().getBodyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__2__Impl"


    // $ANTLR start "rule__Comment__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2474:1: rule__Comment__Group__3 : rule__Comment__Group__3__Impl ;
    public final void rule__Comment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2478:1: ( rule__Comment__Group__3__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2479:2: rule__Comment__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Comment__Group__3__Impl_in_rule__Comment__Group__35061);
            rule__Comment__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__3"


    // $ANTLR start "rule__Comment__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2485:1: rule__Comment__Group__3__Impl : ( '}' ) ;
    public final void rule__Comment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2489:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2490:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2490:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2491:1: '}'
            {
             before(grammarAccess.getCommentAccess().getRightCurlyBracketKeyword_3()); 
            match(input,31,FOLLOW_31_in_rule__Comment__Group__3__Impl5089); 
             after(grammarAccess.getCommentAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__3__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2512:1: rule__InteractionOperand__Group__0 : rule__InteractionOperand__Group__0__Impl rule__InteractionOperand__Group__1 ;
    public final void rule__InteractionOperand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2516:1: ( rule__InteractionOperand__Group__0__Impl rule__InteractionOperand__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2517:2: rule__InteractionOperand__Group__0__Impl rule__InteractionOperand__Group__1
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__0__Impl_in_rule__InteractionOperand__Group__05128);
            rule__InteractionOperand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__1_in_rule__InteractionOperand__Group__05131);
            rule__InteractionOperand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__0"


    // $ANTLR start "rule__InteractionOperand__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2524:1: rule__InteractionOperand__Group__0__Impl : ( 'interactionOperand' ) ;
    public final void rule__InteractionOperand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2528:1: ( ( 'interactionOperand' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2529:1: ( 'interactionOperand' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2529:1: ( 'interactionOperand' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2530:1: 'interactionOperand'
            {
             before(grammarAccess.getInteractionOperandAccess().getInteractionOperandKeyword_0()); 
            match(input,45,FOLLOW_45_in_rule__InteractionOperand__Group__0__Impl5159); 
             after(grammarAccess.getInteractionOperandAccess().getInteractionOperandKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__0__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2543:1: rule__InteractionOperand__Group__1 : rule__InteractionOperand__Group__1__Impl rule__InteractionOperand__Group__2 ;
    public final void rule__InteractionOperand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2547:1: ( rule__InteractionOperand__Group__1__Impl rule__InteractionOperand__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2548:2: rule__InteractionOperand__Group__1__Impl rule__InteractionOperand__Group__2
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__1__Impl_in_rule__InteractionOperand__Group__15190);
            rule__InteractionOperand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__2_in_rule__InteractionOperand__Group__15193);
            rule__InteractionOperand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__1"


    // $ANTLR start "rule__InteractionOperand__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2555:1: rule__InteractionOperand__Group__1__Impl : ( () ) ;
    public final void rule__InteractionOperand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2559:1: ( ( () ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2560:1: ( () )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2560:1: ( () )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2561:1: ()
            {
             before(grammarAccess.getInteractionOperandAccess().getInteractionOperandAction_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2562:1: ()
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2564:1: 
            {
            }

             after(grammarAccess.getInteractionOperandAccess().getInteractionOperandAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__1__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2574:1: rule__InteractionOperand__Group__2 : rule__InteractionOperand__Group__2__Impl rule__InteractionOperand__Group__3 ;
    public final void rule__InteractionOperand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2578:1: ( rule__InteractionOperand__Group__2__Impl rule__InteractionOperand__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2579:2: rule__InteractionOperand__Group__2__Impl rule__InteractionOperand__Group__3
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__2__Impl_in_rule__InteractionOperand__Group__25251);
            rule__InteractionOperand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__3_in_rule__InteractionOperand__Group__25254);
            rule__InteractionOperand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__2"


    // $ANTLR start "rule__InteractionOperand__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2586:1: rule__InteractionOperand__Group__2__Impl : ( '{' ) ;
    public final void rule__InteractionOperand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2590:1: ( ( '{' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2591:1: ( '{' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2591:1: ( '{' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2592:1: '{'
            {
             before(grammarAccess.getInteractionOperandAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,30,FOLLOW_30_in_rule__InteractionOperand__Group__2__Impl5282); 
             after(grammarAccess.getInteractionOperandAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__2__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2605:1: rule__InteractionOperand__Group__3 : rule__InteractionOperand__Group__3__Impl rule__InteractionOperand__Group__4 ;
    public final void rule__InteractionOperand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2609:1: ( rule__InteractionOperand__Group__3__Impl rule__InteractionOperand__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2610:2: rule__InteractionOperand__Group__3__Impl rule__InteractionOperand__Group__4
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__3__Impl_in_rule__InteractionOperand__Group__35313);
            rule__InteractionOperand__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__4_in_rule__InteractionOperand__Group__35316);
            rule__InteractionOperand__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__3"


    // $ANTLR start "rule__InteractionOperand__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2617:1: rule__InteractionOperand__Group__3__Impl : ( ( rule__InteractionOperand__Group_3__0 )* ) ;
    public final void rule__InteractionOperand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2621:1: ( ( ( rule__InteractionOperand__Group_3__0 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2622:1: ( ( rule__InteractionOperand__Group_3__0 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2622:1: ( ( rule__InteractionOperand__Group_3__0 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2623:1: ( rule__InteractionOperand__Group_3__0 )*
            {
             before(grammarAccess.getInteractionOperandAccess().getGroup_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2624:1: ( rule__InteractionOperand__Group_3__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==43) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2624:2: rule__InteractionOperand__Group_3__0
            	    {
            	    pushFollow(FOLLOW_rule__InteractionOperand__Group_3__0_in_rule__InteractionOperand__Group__3__Impl5343);
            	    rule__InteractionOperand__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getInteractionOperandAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__3__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2634:1: rule__InteractionOperand__Group__4 : rule__InteractionOperand__Group__4__Impl rule__InteractionOperand__Group__5 ;
    public final void rule__InteractionOperand__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2638:1: ( rule__InteractionOperand__Group__4__Impl rule__InteractionOperand__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2639:2: rule__InteractionOperand__Group__4__Impl rule__InteractionOperand__Group__5
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__4__Impl_in_rule__InteractionOperand__Group__45374);
            rule__InteractionOperand__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__5_in_rule__InteractionOperand__Group__45377);
            rule__InteractionOperand__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__4"


    // $ANTLR start "rule__InteractionOperand__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2646:1: rule__InteractionOperand__Group__4__Impl : ( ( rule__InteractionOperand__GuardAssignment_4 )? ) ;
    public final void rule__InteractionOperand__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2650:1: ( ( ( rule__InteractionOperand__GuardAssignment_4 )? ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2651:1: ( ( rule__InteractionOperand__GuardAssignment_4 )? )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2651:1: ( ( rule__InteractionOperand__GuardAssignment_4 )? )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2652:1: ( rule__InteractionOperand__GuardAssignment_4 )?
            {
             before(grammarAccess.getInteractionOperandAccess().getGuardAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2653:1: ( rule__InteractionOperand__GuardAssignment_4 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==46) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2653:2: rule__InteractionOperand__GuardAssignment_4
                    {
                    pushFollow(FOLLOW_rule__InteractionOperand__GuardAssignment_4_in_rule__InteractionOperand__Group__4__Impl5404);
                    rule__InteractionOperand__GuardAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInteractionOperandAccess().getGuardAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__4__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2663:1: rule__InteractionOperand__Group__5 : rule__InteractionOperand__Group__5__Impl rule__InteractionOperand__Group__6 ;
    public final void rule__InteractionOperand__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2667:1: ( rule__InteractionOperand__Group__5__Impl rule__InteractionOperand__Group__6 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2668:2: rule__InteractionOperand__Group__5__Impl rule__InteractionOperand__Group__6
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__5__Impl_in_rule__InteractionOperand__Group__55435);
            rule__InteractionOperand__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group__6_in_rule__InteractionOperand__Group__55438);
            rule__InteractionOperand__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__5"


    // $ANTLR start "rule__InteractionOperand__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2675:1: rule__InteractionOperand__Group__5__Impl : ( ( rule__InteractionOperand__FragmentAssignment_5 )* ) ;
    public final void rule__InteractionOperand__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2679:1: ( ( ( rule__InteractionOperand__FragmentAssignment_5 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2680:1: ( ( rule__InteractionOperand__FragmentAssignment_5 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2680:1: ( ( rule__InteractionOperand__FragmentAssignment_5 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2681:1: ( rule__InteractionOperand__FragmentAssignment_5 )*
            {
             before(grammarAccess.getInteractionOperandAccess().getFragmentAssignment_5()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2682:1: ( rule__InteractionOperand__FragmentAssignment_5 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==42||LA20_0==53) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2682:2: rule__InteractionOperand__FragmentAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__InteractionOperand__FragmentAssignment_5_in_rule__InteractionOperand__Group__5__Impl5465);
            	    rule__InteractionOperand__FragmentAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getInteractionOperandAccess().getFragmentAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__5__Impl"


    // $ANTLR start "rule__InteractionOperand__Group__6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2692:1: rule__InteractionOperand__Group__6 : rule__InteractionOperand__Group__6__Impl ;
    public final void rule__InteractionOperand__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2696:1: ( rule__InteractionOperand__Group__6__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2697:2: rule__InteractionOperand__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group__6__Impl_in_rule__InteractionOperand__Group__65496);
            rule__InteractionOperand__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__6"


    // $ANTLR start "rule__InteractionOperand__Group__6__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2703:1: rule__InteractionOperand__Group__6__Impl : ( '}' ) ;
    public final void rule__InteractionOperand__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2707:1: ( ( '}' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2708:1: ( '}' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2708:1: ( '}' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2709:1: '}'
            {
             before(grammarAccess.getInteractionOperandAccess().getRightCurlyBracketKeyword_6()); 
            match(input,31,FOLLOW_31_in_rule__InteractionOperand__Group__6__Impl5524); 
             after(grammarAccess.getInteractionOperandAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group__6__Impl"


    // $ANTLR start "rule__InteractionOperand__Group_3__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2736:1: rule__InteractionOperand__Group_3__0 : rule__InteractionOperand__Group_3__0__Impl rule__InteractionOperand__Group_3__1 ;
    public final void rule__InteractionOperand__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2740:1: ( rule__InteractionOperand__Group_3__0__Impl rule__InteractionOperand__Group_3__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2741:2: rule__InteractionOperand__Group_3__0__Impl rule__InteractionOperand__Group_3__1
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group_3__0__Impl_in_rule__InteractionOperand__Group_3__05569);
            rule__InteractionOperand__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionOperand__Group_3__1_in_rule__InteractionOperand__Group_3__05572);
            rule__InteractionOperand__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group_3__0"


    // $ANTLR start "rule__InteractionOperand__Group_3__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2748:1: rule__InteractionOperand__Group_3__0__Impl : ( 'covered=' ) ;
    public final void rule__InteractionOperand__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2752:1: ( ( 'covered=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2753:1: ( 'covered=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2753:1: ( 'covered=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2754:1: 'covered='
            {
             before(grammarAccess.getInteractionOperandAccess().getCoveredKeyword_3_0()); 
            match(input,43,FOLLOW_43_in_rule__InteractionOperand__Group_3__0__Impl5600); 
             after(grammarAccess.getInteractionOperandAccess().getCoveredKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group_3__0__Impl"


    // $ANTLR start "rule__InteractionOperand__Group_3__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2767:1: rule__InteractionOperand__Group_3__1 : rule__InteractionOperand__Group_3__1__Impl ;
    public final void rule__InteractionOperand__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2771:1: ( rule__InteractionOperand__Group_3__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2772:2: rule__InteractionOperand__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__InteractionOperand__Group_3__1__Impl_in_rule__InteractionOperand__Group_3__15631);
            rule__InteractionOperand__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group_3__1"


    // $ANTLR start "rule__InteractionOperand__Group_3__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2778:1: rule__InteractionOperand__Group_3__1__Impl : ( ( rule__InteractionOperand__CoveredAssignment_3_1 ) ) ;
    public final void rule__InteractionOperand__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2782:1: ( ( ( rule__InteractionOperand__CoveredAssignment_3_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2783:1: ( ( rule__InteractionOperand__CoveredAssignment_3_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2783:1: ( ( rule__InteractionOperand__CoveredAssignment_3_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2784:1: ( rule__InteractionOperand__CoveredAssignment_3_1 )
            {
             before(grammarAccess.getInteractionOperandAccess().getCoveredAssignment_3_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2785:1: ( rule__InteractionOperand__CoveredAssignment_3_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2785:2: rule__InteractionOperand__CoveredAssignment_3_1
            {
            pushFollow(FOLLOW_rule__InteractionOperand__CoveredAssignment_3_1_in_rule__InteractionOperand__Group_3__1__Impl5658);
            rule__InteractionOperand__CoveredAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionOperandAccess().getCoveredAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__Group_3__1__Impl"


    // $ANTLR start "rule__InteractionConstraint__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2799:1: rule__InteractionConstraint__Group__0 : rule__InteractionConstraint__Group__0__Impl rule__InteractionConstraint__Group__1 ;
    public final void rule__InteractionConstraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2803:1: ( rule__InteractionConstraint__Group__0__Impl rule__InteractionConstraint__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2804:2: rule__InteractionConstraint__Group__0__Impl rule__InteractionConstraint__Group__1
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__0__Impl_in_rule__InteractionConstraint__Group__05692);
            rule__InteractionConstraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionConstraint__Group__1_in_rule__InteractionConstraint__Group__05695);
            rule__InteractionConstraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__0"


    // $ANTLR start "rule__InteractionConstraint__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2811:1: rule__InteractionConstraint__Group__0__Impl : ( 'interactionConstraint' ) ;
    public final void rule__InteractionConstraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2815:1: ( ( 'interactionConstraint' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2816:1: ( 'interactionConstraint' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2816:1: ( 'interactionConstraint' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2817:1: 'interactionConstraint'
            {
             before(grammarAccess.getInteractionConstraintAccess().getInteractionConstraintKeyword_0()); 
            match(input,46,FOLLOW_46_in_rule__InteractionConstraint__Group__0__Impl5723); 
             after(grammarAccess.getInteractionConstraintAccess().getInteractionConstraintKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__0__Impl"


    // $ANTLR start "rule__InteractionConstraint__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2830:1: rule__InteractionConstraint__Group__1 : rule__InteractionConstraint__Group__1__Impl rule__InteractionConstraint__Group__2 ;
    public final void rule__InteractionConstraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2834:1: ( rule__InteractionConstraint__Group__1__Impl rule__InteractionConstraint__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2835:2: rule__InteractionConstraint__Group__1__Impl rule__InteractionConstraint__Group__2
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__1__Impl_in_rule__InteractionConstraint__Group__15754);
            rule__InteractionConstraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionConstraint__Group__2_in_rule__InteractionConstraint__Group__15757);
            rule__InteractionConstraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__1"


    // $ANTLR start "rule__InteractionConstraint__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2842:1: rule__InteractionConstraint__Group__1__Impl : ( ( rule__InteractionConstraint__NameAssignment_1 ) ) ;
    public final void rule__InteractionConstraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2846:1: ( ( ( rule__InteractionConstraint__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2847:1: ( ( rule__InteractionConstraint__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2847:1: ( ( rule__InteractionConstraint__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2848:1: ( rule__InteractionConstraint__NameAssignment_1 )
            {
             before(grammarAccess.getInteractionConstraintAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2849:1: ( rule__InteractionConstraint__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2849:2: rule__InteractionConstraint__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__NameAssignment_1_in_rule__InteractionConstraint__Group__1__Impl5784);
            rule__InteractionConstraint__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionConstraintAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__1__Impl"


    // $ANTLR start "rule__InteractionConstraint__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2859:1: rule__InteractionConstraint__Group__2 : rule__InteractionConstraint__Group__2__Impl rule__InteractionConstraint__Group__3 ;
    public final void rule__InteractionConstraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2863:1: ( rule__InteractionConstraint__Group__2__Impl rule__InteractionConstraint__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2864:2: rule__InteractionConstraint__Group__2__Impl rule__InteractionConstraint__Group__3
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__2__Impl_in_rule__InteractionConstraint__Group__25814);
            rule__InteractionConstraint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionConstraint__Group__3_in_rule__InteractionConstraint__Group__25817);
            rule__InteractionConstraint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__2"


    // $ANTLR start "rule__InteractionConstraint__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2871:1: rule__InteractionConstraint__Group__2__Impl : ( ( rule__InteractionConstraint__MinintAssignment_2 ) ) ;
    public final void rule__InteractionConstraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2875:1: ( ( ( rule__InteractionConstraint__MinintAssignment_2 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2876:1: ( ( rule__InteractionConstraint__MinintAssignment_2 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2876:1: ( ( rule__InteractionConstraint__MinintAssignment_2 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2877:1: ( rule__InteractionConstraint__MinintAssignment_2 )
            {
             before(grammarAccess.getInteractionConstraintAccess().getMinintAssignment_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2878:1: ( rule__InteractionConstraint__MinintAssignment_2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2878:2: rule__InteractionConstraint__MinintAssignment_2
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__MinintAssignment_2_in_rule__InteractionConstraint__Group__2__Impl5844);
            rule__InteractionConstraint__MinintAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getInteractionConstraintAccess().getMinintAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__2__Impl"


    // $ANTLR start "rule__InteractionConstraint__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2888:1: rule__InteractionConstraint__Group__3 : rule__InteractionConstraint__Group__3__Impl rule__InteractionConstraint__Group__4 ;
    public final void rule__InteractionConstraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2892:1: ( rule__InteractionConstraint__Group__3__Impl rule__InteractionConstraint__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2893:2: rule__InteractionConstraint__Group__3__Impl rule__InteractionConstraint__Group__4
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__3__Impl_in_rule__InteractionConstraint__Group__35874);
            rule__InteractionConstraint__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InteractionConstraint__Group__4_in_rule__InteractionConstraint__Group__35877);
            rule__InteractionConstraint__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__3"


    // $ANTLR start "rule__InteractionConstraint__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2900:1: rule__InteractionConstraint__Group__3__Impl : ( ( rule__InteractionConstraint__MaxintAssignment_3 ) ) ;
    public final void rule__InteractionConstraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2904:1: ( ( ( rule__InteractionConstraint__MaxintAssignment_3 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2905:1: ( ( rule__InteractionConstraint__MaxintAssignment_3 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2905:1: ( ( rule__InteractionConstraint__MaxintAssignment_3 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2906:1: ( rule__InteractionConstraint__MaxintAssignment_3 )
            {
             before(grammarAccess.getInteractionConstraintAccess().getMaxintAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2907:1: ( rule__InteractionConstraint__MaxintAssignment_3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2907:2: rule__InteractionConstraint__MaxintAssignment_3
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__MaxintAssignment_3_in_rule__InteractionConstraint__Group__3__Impl5904);
            rule__InteractionConstraint__MaxintAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getInteractionConstraintAccess().getMaxintAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__3__Impl"


    // $ANTLR start "rule__InteractionConstraint__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2917:1: rule__InteractionConstraint__Group__4 : rule__InteractionConstraint__Group__4__Impl ;
    public final void rule__InteractionConstraint__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2921:1: ( rule__InteractionConstraint__Group__4__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2922:2: rule__InteractionConstraint__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__Group__4__Impl_in_rule__InteractionConstraint__Group__45934);
            rule__InteractionConstraint__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__4"


    // $ANTLR start "rule__InteractionConstraint__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2928:1: rule__InteractionConstraint__Group__4__Impl : ( ( rule__InteractionConstraint__SpecificationAssignment_4 ) ) ;
    public final void rule__InteractionConstraint__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2932:1: ( ( ( rule__InteractionConstraint__SpecificationAssignment_4 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2933:1: ( ( rule__InteractionConstraint__SpecificationAssignment_4 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2933:1: ( ( rule__InteractionConstraint__SpecificationAssignment_4 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2934:1: ( rule__InteractionConstraint__SpecificationAssignment_4 )
            {
             before(grammarAccess.getInteractionConstraintAccess().getSpecificationAssignment_4()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2935:1: ( rule__InteractionConstraint__SpecificationAssignment_4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2935:2: rule__InteractionConstraint__SpecificationAssignment_4
            {
            pushFollow(FOLLOW_rule__InteractionConstraint__SpecificationAssignment_4_in_rule__InteractionConstraint__Group__4__Impl5961);
            rule__InteractionConstraint__SpecificationAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getInteractionConstraintAccess().getSpecificationAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__Group__4__Impl"


    // $ANTLR start "rule__Lifeline__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2955:1: rule__Lifeline__Group__0 : rule__Lifeline__Group__0__Impl rule__Lifeline__Group__1 ;
    public final void rule__Lifeline__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2959:1: ( rule__Lifeline__Group__0__Impl rule__Lifeline__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2960:2: rule__Lifeline__Group__0__Impl rule__Lifeline__Group__1
            {
            pushFollow(FOLLOW_rule__Lifeline__Group__0__Impl_in_rule__Lifeline__Group__06001);
            rule__Lifeline__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Lifeline__Group__1_in_rule__Lifeline__Group__06004);
            rule__Lifeline__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__0"


    // $ANTLR start "rule__Lifeline__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2967:1: rule__Lifeline__Group__0__Impl : ( 'lifeline' ) ;
    public final void rule__Lifeline__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2971:1: ( ( 'lifeline' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2972:1: ( 'lifeline' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2972:1: ( 'lifeline' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2973:1: 'lifeline'
            {
             before(grammarAccess.getLifelineAccess().getLifelineKeyword_0()); 
            match(input,47,FOLLOW_47_in_rule__Lifeline__Group__0__Impl6032); 
             after(grammarAccess.getLifelineAccess().getLifelineKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__0__Impl"


    // $ANTLR start "rule__Lifeline__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2986:1: rule__Lifeline__Group__1 : rule__Lifeline__Group__1__Impl rule__Lifeline__Group__2 ;
    public final void rule__Lifeline__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2990:1: ( rule__Lifeline__Group__1__Impl rule__Lifeline__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2991:2: rule__Lifeline__Group__1__Impl rule__Lifeline__Group__2
            {
            pushFollow(FOLLOW_rule__Lifeline__Group__1__Impl_in_rule__Lifeline__Group__16063);
            rule__Lifeline__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Lifeline__Group__2_in_rule__Lifeline__Group__16066);
            rule__Lifeline__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__1"


    // $ANTLR start "rule__Lifeline__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:2998:1: rule__Lifeline__Group__1__Impl : ( ( rule__Lifeline__NameAssignment_1 ) ) ;
    public final void rule__Lifeline__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3002:1: ( ( ( rule__Lifeline__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3003:1: ( ( rule__Lifeline__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3003:1: ( ( rule__Lifeline__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3004:1: ( rule__Lifeline__NameAssignment_1 )
            {
             before(grammarAccess.getLifelineAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3005:1: ( rule__Lifeline__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3005:2: rule__Lifeline__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Lifeline__NameAssignment_1_in_rule__Lifeline__Group__1__Impl6093);
            rule__Lifeline__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLifelineAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__1__Impl"


    // $ANTLR start "rule__Lifeline__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3015:1: rule__Lifeline__Group__2 : rule__Lifeline__Group__2__Impl ;
    public final void rule__Lifeline__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3019:1: ( rule__Lifeline__Group__2__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3020:2: rule__Lifeline__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Lifeline__Group__2__Impl_in_rule__Lifeline__Group__26123);
            rule__Lifeline__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__2"


    // $ANTLR start "rule__Lifeline__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3026:1: rule__Lifeline__Group__2__Impl : ( ( rule__Lifeline__Group_2__0 )? ) ;
    public final void rule__Lifeline__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3030:1: ( ( ( rule__Lifeline__Group_2__0 )? ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3031:1: ( ( rule__Lifeline__Group_2__0 )? )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3031:1: ( ( rule__Lifeline__Group_2__0 )? )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3032:1: ( rule__Lifeline__Group_2__0 )?
            {
             before(grammarAccess.getLifelineAccess().getGroup_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3033:1: ( rule__Lifeline__Group_2__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==48) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3033:2: rule__Lifeline__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Lifeline__Group_2__0_in_rule__Lifeline__Group__2__Impl6150);
                    rule__Lifeline__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLifelineAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group__2__Impl"


    // $ANTLR start "rule__Lifeline__Group_2__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3049:1: rule__Lifeline__Group_2__0 : rule__Lifeline__Group_2__0__Impl rule__Lifeline__Group_2__1 ;
    public final void rule__Lifeline__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3053:1: ( rule__Lifeline__Group_2__0__Impl rule__Lifeline__Group_2__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3054:2: rule__Lifeline__Group_2__0__Impl rule__Lifeline__Group_2__1
            {
            pushFollow(FOLLOW_rule__Lifeline__Group_2__0__Impl_in_rule__Lifeline__Group_2__06187);
            rule__Lifeline__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Lifeline__Group_2__1_in_rule__Lifeline__Group_2__06190);
            rule__Lifeline__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group_2__0"


    // $ANTLR start "rule__Lifeline__Group_2__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3061:1: rule__Lifeline__Group_2__0__Impl : ( 'represents=' ) ;
    public final void rule__Lifeline__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3065:1: ( ( 'represents=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3066:1: ( 'represents=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3066:1: ( 'represents=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3067:1: 'represents='
            {
             before(grammarAccess.getLifelineAccess().getRepresentsKeyword_2_0()); 
            match(input,48,FOLLOW_48_in_rule__Lifeline__Group_2__0__Impl6218); 
             after(grammarAccess.getLifelineAccess().getRepresentsKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group_2__0__Impl"


    // $ANTLR start "rule__Lifeline__Group_2__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3080:1: rule__Lifeline__Group_2__1 : rule__Lifeline__Group_2__1__Impl ;
    public final void rule__Lifeline__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3084:1: ( rule__Lifeline__Group_2__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3085:2: rule__Lifeline__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Lifeline__Group_2__1__Impl_in_rule__Lifeline__Group_2__16249);
            rule__Lifeline__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group_2__1"


    // $ANTLR start "rule__Lifeline__Group_2__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3091:1: rule__Lifeline__Group_2__1__Impl : ( ( rule__Lifeline__RepresentsAssignment_2_1 ) ) ;
    public final void rule__Lifeline__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3095:1: ( ( ( rule__Lifeline__RepresentsAssignment_2_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3096:1: ( ( rule__Lifeline__RepresentsAssignment_2_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3096:1: ( ( rule__Lifeline__RepresentsAssignment_2_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3097:1: ( rule__Lifeline__RepresentsAssignment_2_1 )
            {
             before(grammarAccess.getLifelineAccess().getRepresentsAssignment_2_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3098:1: ( rule__Lifeline__RepresentsAssignment_2_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3098:2: rule__Lifeline__RepresentsAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Lifeline__RepresentsAssignment_2_1_in_rule__Lifeline__Group_2__1__Impl6276);
            rule__Lifeline__RepresentsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLifelineAccess().getRepresentsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__Group_2__1__Impl"


    // $ANTLR start "rule__Message__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3112:1: rule__Message__Group__0 : rule__Message__Group__0__Impl rule__Message__Group__1 ;
    public final void rule__Message__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3116:1: ( rule__Message__Group__0__Impl rule__Message__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3117:2: rule__Message__Group__0__Impl rule__Message__Group__1
            {
            pushFollow(FOLLOW_rule__Message__Group__0__Impl_in_rule__Message__Group__06310);
            rule__Message__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__1_in_rule__Message__Group__06313);
            rule__Message__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__0"


    // $ANTLR start "rule__Message__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3124:1: rule__Message__Group__0__Impl : ( 'message' ) ;
    public final void rule__Message__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3128:1: ( ( 'message' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3129:1: ( 'message' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3129:1: ( 'message' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3130:1: 'message'
            {
             before(grammarAccess.getMessageAccess().getMessageKeyword_0()); 
            match(input,49,FOLLOW_49_in_rule__Message__Group__0__Impl6341); 
             after(grammarAccess.getMessageAccess().getMessageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__0__Impl"


    // $ANTLR start "rule__Message__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3143:1: rule__Message__Group__1 : rule__Message__Group__1__Impl rule__Message__Group__2 ;
    public final void rule__Message__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3147:1: ( rule__Message__Group__1__Impl rule__Message__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3148:2: rule__Message__Group__1__Impl rule__Message__Group__2
            {
            pushFollow(FOLLOW_rule__Message__Group__1__Impl_in_rule__Message__Group__16372);
            rule__Message__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__2_in_rule__Message__Group__16375);
            rule__Message__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__1"


    // $ANTLR start "rule__Message__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3155:1: rule__Message__Group__1__Impl : ( ( rule__Message__MessageSortAssignment_1 ) ) ;
    public final void rule__Message__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3159:1: ( ( ( rule__Message__MessageSortAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3160:1: ( ( rule__Message__MessageSortAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3160:1: ( ( rule__Message__MessageSortAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3161:1: ( rule__Message__MessageSortAssignment_1 )
            {
             before(grammarAccess.getMessageAccess().getMessageSortAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3162:1: ( rule__Message__MessageSortAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3162:2: rule__Message__MessageSortAssignment_1
            {
            pushFollow(FOLLOW_rule__Message__MessageSortAssignment_1_in_rule__Message__Group__1__Impl6402);
            rule__Message__MessageSortAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMessageAccess().getMessageSortAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__1__Impl"


    // $ANTLR start "rule__Message__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3172:1: rule__Message__Group__2 : rule__Message__Group__2__Impl rule__Message__Group__3 ;
    public final void rule__Message__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3176:1: ( rule__Message__Group__2__Impl rule__Message__Group__3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3177:2: rule__Message__Group__2__Impl rule__Message__Group__3
            {
            pushFollow(FOLLOW_rule__Message__Group__2__Impl_in_rule__Message__Group__26432);
            rule__Message__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__3_in_rule__Message__Group__26435);
            rule__Message__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__2"


    // $ANTLR start "rule__Message__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3184:1: rule__Message__Group__2__Impl : ( 'receive=' ) ;
    public final void rule__Message__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3188:1: ( ( 'receive=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3189:1: ( 'receive=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3189:1: ( 'receive=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3190:1: 'receive='
            {
             before(grammarAccess.getMessageAccess().getReceiveKeyword_2()); 
            match(input,50,FOLLOW_50_in_rule__Message__Group__2__Impl6463); 
             after(grammarAccess.getMessageAccess().getReceiveKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__2__Impl"


    // $ANTLR start "rule__Message__Group__3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3203:1: rule__Message__Group__3 : rule__Message__Group__3__Impl rule__Message__Group__4 ;
    public final void rule__Message__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3207:1: ( rule__Message__Group__3__Impl rule__Message__Group__4 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3208:2: rule__Message__Group__3__Impl rule__Message__Group__4
            {
            pushFollow(FOLLOW_rule__Message__Group__3__Impl_in_rule__Message__Group__36494);
            rule__Message__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__4_in_rule__Message__Group__36497);
            rule__Message__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__3"


    // $ANTLR start "rule__Message__Group__3__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3215:1: rule__Message__Group__3__Impl : ( ( rule__Message__ReceiveEventAssignment_3 ) ) ;
    public final void rule__Message__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3219:1: ( ( ( rule__Message__ReceiveEventAssignment_3 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3220:1: ( ( rule__Message__ReceiveEventAssignment_3 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3220:1: ( ( rule__Message__ReceiveEventAssignment_3 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3221:1: ( rule__Message__ReceiveEventAssignment_3 )
            {
             before(grammarAccess.getMessageAccess().getReceiveEventAssignment_3()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3222:1: ( rule__Message__ReceiveEventAssignment_3 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3222:2: rule__Message__ReceiveEventAssignment_3
            {
            pushFollow(FOLLOW_rule__Message__ReceiveEventAssignment_3_in_rule__Message__Group__3__Impl6524);
            rule__Message__ReceiveEventAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getMessageAccess().getReceiveEventAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__3__Impl"


    // $ANTLR start "rule__Message__Group__4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3232:1: rule__Message__Group__4 : rule__Message__Group__4__Impl rule__Message__Group__5 ;
    public final void rule__Message__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3236:1: ( rule__Message__Group__4__Impl rule__Message__Group__5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3237:2: rule__Message__Group__4__Impl rule__Message__Group__5
            {
            pushFollow(FOLLOW_rule__Message__Group__4__Impl_in_rule__Message__Group__46554);
            rule__Message__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__5_in_rule__Message__Group__46557);
            rule__Message__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__4"


    // $ANTLR start "rule__Message__Group__4__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3244:1: rule__Message__Group__4__Impl : ( 'send=' ) ;
    public final void rule__Message__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3248:1: ( ( 'send=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3249:1: ( 'send=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3249:1: ( 'send=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3250:1: 'send='
            {
             before(grammarAccess.getMessageAccess().getSendKeyword_4()); 
            match(input,51,FOLLOW_51_in_rule__Message__Group__4__Impl6585); 
             after(grammarAccess.getMessageAccess().getSendKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__4__Impl"


    // $ANTLR start "rule__Message__Group__5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3263:1: rule__Message__Group__5 : rule__Message__Group__5__Impl rule__Message__Group__6 ;
    public final void rule__Message__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3267:1: ( rule__Message__Group__5__Impl rule__Message__Group__6 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3268:2: rule__Message__Group__5__Impl rule__Message__Group__6
            {
            pushFollow(FOLLOW_rule__Message__Group__5__Impl_in_rule__Message__Group__56616);
            rule__Message__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__6_in_rule__Message__Group__56619);
            rule__Message__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__5"


    // $ANTLR start "rule__Message__Group__5__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3275:1: rule__Message__Group__5__Impl : ( ( rule__Message__SendEventAssignment_5 ) ) ;
    public final void rule__Message__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3279:1: ( ( ( rule__Message__SendEventAssignment_5 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3280:1: ( ( rule__Message__SendEventAssignment_5 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3280:1: ( ( rule__Message__SendEventAssignment_5 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3281:1: ( rule__Message__SendEventAssignment_5 )
            {
             before(grammarAccess.getMessageAccess().getSendEventAssignment_5()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3282:1: ( rule__Message__SendEventAssignment_5 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3282:2: rule__Message__SendEventAssignment_5
            {
            pushFollow(FOLLOW_rule__Message__SendEventAssignment_5_in_rule__Message__Group__5__Impl6646);
            rule__Message__SendEventAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getMessageAccess().getSendEventAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__5__Impl"


    // $ANTLR start "rule__Message__Group__6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3292:1: rule__Message__Group__6 : rule__Message__Group__6__Impl rule__Message__Group__7 ;
    public final void rule__Message__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3296:1: ( rule__Message__Group__6__Impl rule__Message__Group__7 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3297:2: rule__Message__Group__6__Impl rule__Message__Group__7
            {
            pushFollow(FOLLOW_rule__Message__Group__6__Impl_in_rule__Message__Group__66676);
            rule__Message__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Message__Group__7_in_rule__Message__Group__66679);
            rule__Message__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__6"


    // $ANTLR start "rule__Message__Group__6__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3304:1: rule__Message__Group__6__Impl : ( 'signature=' ) ;
    public final void rule__Message__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3308:1: ( ( 'signature=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3309:1: ( 'signature=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3309:1: ( 'signature=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3310:1: 'signature='
            {
             before(grammarAccess.getMessageAccess().getSignatureKeyword_6()); 
            match(input,52,FOLLOW_52_in_rule__Message__Group__6__Impl6707); 
             after(grammarAccess.getMessageAccess().getSignatureKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__6__Impl"


    // $ANTLR start "rule__Message__Group__7"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3323:1: rule__Message__Group__7 : rule__Message__Group__7__Impl ;
    public final void rule__Message__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3327:1: ( rule__Message__Group__7__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3328:2: rule__Message__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Message__Group__7__Impl_in_rule__Message__Group__76738);
            rule__Message__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__7"


    // $ANTLR start "rule__Message__Group__7__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3334:1: rule__Message__Group__7__Impl : ( ( rule__Message__SignatureAssignment_7 ) ) ;
    public final void rule__Message__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3338:1: ( ( ( rule__Message__SignatureAssignment_7 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3339:1: ( ( rule__Message__SignatureAssignment_7 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3339:1: ( ( rule__Message__SignatureAssignment_7 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3340:1: ( rule__Message__SignatureAssignment_7 )
            {
             before(grammarAccess.getMessageAccess().getSignatureAssignment_7()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3341:1: ( rule__Message__SignatureAssignment_7 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3341:2: rule__Message__SignatureAssignment_7
            {
            pushFollow(FOLLOW_rule__Message__SignatureAssignment_7_in_rule__Message__Group__7__Impl6765);
            rule__Message__SignatureAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getMessageAccess().getSignatureAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Group__7__Impl"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3367:1: rule__MessageOccurrenceSpecification__Group__0 : rule__MessageOccurrenceSpecification__Group__0__Impl rule__MessageOccurrenceSpecification__Group__1 ;
    public final void rule__MessageOccurrenceSpecification__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3371:1: ( rule__MessageOccurrenceSpecification__Group__0__Impl rule__MessageOccurrenceSpecification__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3372:2: rule__MessageOccurrenceSpecification__Group__0__Impl rule__MessageOccurrenceSpecification__Group__1
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__0__Impl_in_rule__MessageOccurrenceSpecification__Group__06811);
            rule__MessageOccurrenceSpecification__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__1_in_rule__MessageOccurrenceSpecification__Group__06814);
            rule__MessageOccurrenceSpecification__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__0"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3379:1: rule__MessageOccurrenceSpecification__Group__0__Impl : ( 'messageOccurrenceSpecification' ) ;
    public final void rule__MessageOccurrenceSpecification__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3383:1: ( ( 'messageOccurrenceSpecification' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3384:1: ( 'messageOccurrenceSpecification' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3384:1: ( 'messageOccurrenceSpecification' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3385:1: 'messageOccurrenceSpecification'
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getMessageOccurrenceSpecificationKeyword_0()); 
            match(input,53,FOLLOW_53_in_rule__MessageOccurrenceSpecification__Group__0__Impl6842); 
             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getMessageOccurrenceSpecificationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__0__Impl"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3398:1: rule__MessageOccurrenceSpecification__Group__1 : rule__MessageOccurrenceSpecification__Group__1__Impl rule__MessageOccurrenceSpecification__Group__2 ;
    public final void rule__MessageOccurrenceSpecification__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3402:1: ( rule__MessageOccurrenceSpecification__Group__1__Impl rule__MessageOccurrenceSpecification__Group__2 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3403:2: rule__MessageOccurrenceSpecification__Group__1__Impl rule__MessageOccurrenceSpecification__Group__2
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__1__Impl_in_rule__MessageOccurrenceSpecification__Group__16873);
            rule__MessageOccurrenceSpecification__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__2_in_rule__MessageOccurrenceSpecification__Group__16876);
            rule__MessageOccurrenceSpecification__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__1"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3410:1: rule__MessageOccurrenceSpecification__Group__1__Impl : ( ( rule__MessageOccurrenceSpecification__NameAssignment_1 ) ) ;
    public final void rule__MessageOccurrenceSpecification__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3414:1: ( ( ( rule__MessageOccurrenceSpecification__NameAssignment_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3415:1: ( ( rule__MessageOccurrenceSpecification__NameAssignment_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3415:1: ( ( rule__MessageOccurrenceSpecification__NameAssignment_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3416:1: ( rule__MessageOccurrenceSpecification__NameAssignment_1 )
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getNameAssignment_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3417:1: ( rule__MessageOccurrenceSpecification__NameAssignment_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3417:2: rule__MessageOccurrenceSpecification__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__NameAssignment_1_in_rule__MessageOccurrenceSpecification__Group__1__Impl6903);
            rule__MessageOccurrenceSpecification__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__1__Impl"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3427:1: rule__MessageOccurrenceSpecification__Group__2 : rule__MessageOccurrenceSpecification__Group__2__Impl ;
    public final void rule__MessageOccurrenceSpecification__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3431:1: ( rule__MessageOccurrenceSpecification__Group__2__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3432:2: rule__MessageOccurrenceSpecification__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group__2__Impl_in_rule__MessageOccurrenceSpecification__Group__26933);
            rule__MessageOccurrenceSpecification__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__2"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group__2__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3438:1: rule__MessageOccurrenceSpecification__Group__2__Impl : ( ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) ) ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* ) ) ;
    public final void rule__MessageOccurrenceSpecification__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3442:1: ( ( ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) ) ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3443:1: ( ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) ) ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3443:1: ( ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) ) ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3444:1: ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) ) ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3444:1: ( ( rule__MessageOccurrenceSpecification__Group_2__0 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3445:1: ( rule__MessageOccurrenceSpecification__Group_2__0 )
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3446:1: ( rule__MessageOccurrenceSpecification__Group_2__0 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3446:2: rule__MessageOccurrenceSpecification__Group_2__0
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0_in_rule__MessageOccurrenceSpecification__Group__2__Impl6962);
            rule__MessageOccurrenceSpecification__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup_2()); 

            }

            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3449:1: ( ( rule__MessageOccurrenceSpecification__Group_2__0 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3450:1: ( rule__MessageOccurrenceSpecification__Group_2__0 )*
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup_2()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3451:1: ( rule__MessageOccurrenceSpecification__Group_2__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==43) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3451:2: rule__MessageOccurrenceSpecification__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0_in_rule__MessageOccurrenceSpecification__Group__2__Impl6974);
            	    rule__MessageOccurrenceSpecification__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getGroup_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group__2__Impl"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group_2__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3468:1: rule__MessageOccurrenceSpecification__Group_2__0 : rule__MessageOccurrenceSpecification__Group_2__0__Impl rule__MessageOccurrenceSpecification__Group_2__1 ;
    public final void rule__MessageOccurrenceSpecification__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3472:1: ( rule__MessageOccurrenceSpecification__Group_2__0__Impl rule__MessageOccurrenceSpecification__Group_2__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3473:2: rule__MessageOccurrenceSpecification__Group_2__0__Impl rule__MessageOccurrenceSpecification__Group_2__1
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0__Impl_in_rule__MessageOccurrenceSpecification__Group_2__07013);
            rule__MessageOccurrenceSpecification__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group_2__1_in_rule__MessageOccurrenceSpecification__Group_2__07016);
            rule__MessageOccurrenceSpecification__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group_2__0"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group_2__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3480:1: rule__MessageOccurrenceSpecification__Group_2__0__Impl : ( 'covered=' ) ;
    public final void rule__MessageOccurrenceSpecification__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3484:1: ( ( 'covered=' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3485:1: ( 'covered=' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3485:1: ( 'covered=' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3486:1: 'covered='
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredKeyword_2_0()); 
            match(input,43,FOLLOW_43_in_rule__MessageOccurrenceSpecification__Group_2__0__Impl7044); 
             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group_2__0__Impl"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group_2__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3499:1: rule__MessageOccurrenceSpecification__Group_2__1 : rule__MessageOccurrenceSpecification__Group_2__1__Impl ;
    public final void rule__MessageOccurrenceSpecification__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3503:1: ( rule__MessageOccurrenceSpecification__Group_2__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3504:2: rule__MessageOccurrenceSpecification__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__Group_2__1__Impl_in_rule__MessageOccurrenceSpecification__Group_2__17075);
            rule__MessageOccurrenceSpecification__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group_2__1"


    // $ANTLR start "rule__MessageOccurrenceSpecification__Group_2__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3510:1: rule__MessageOccurrenceSpecification__Group_2__1__Impl : ( ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 ) ) ;
    public final void rule__MessageOccurrenceSpecification__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3514:1: ( ( ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3515:1: ( ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3515:1: ( ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3516:1: ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 )
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredAssignment_2_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3517:1: ( rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3517:2: rule__MessageOccurrenceSpecification__CoveredAssignment_2_1
            {
            pushFollow(FOLLOW_rule__MessageOccurrenceSpecification__CoveredAssignment_2_1_in_rule__MessageOccurrenceSpecification__Group_2__1__Impl7102);
            rule__MessageOccurrenceSpecification__CoveredAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__Group_2__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3531:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3535:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3536:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__07136);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__07139);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3543:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3547:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3548:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3548:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3549:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl7166); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3560:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3564:1: ( rule__QualifiedName__Group__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3565:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__17195);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3571:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3575:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3576:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3576:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3577:1: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3578:1: ( rule__QualifiedName__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==54) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3578:2: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl7222);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3592:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3596:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3597:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__07257);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__07260);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3604:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3608:1: ( ( '.' ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3609:1: ( '.' )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3609:1: ( '.' )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3610:1: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,54,FOLLOW_54_in_rule__QualifiedName__Group_1__0__Impl7288); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3623:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3627:1: ( rule__QualifiedName__Group_1__1__Impl )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3628:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__17319);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3634:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3638:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3639:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3639:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3640:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl7346); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Model__PackagedElementAssignment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3656:1: rule__Model__PackagedElementAssignment : ( rulePackage ) ;
    public final void rule__Model__PackagedElementAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3660:1: ( ( rulePackage ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3661:1: ( rulePackage )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3661:1: ( rulePackage )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3662:1: rulePackage
            {
             before(grammarAccess.getModelAccess().getPackagedElementPackageParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePackage_in_rule__Model__PackagedElementAssignment7384);
            rulePackage();

            state._fsp--;

             after(grammarAccess.getModelAccess().getPackagedElementPackageParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__PackagedElementAssignment"


    // $ANTLR start "rule__Package__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3671:1: rule__Package__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Package__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3675:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3676:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3676:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3677:1: RULE_ID
            {
             before(grammarAccess.getPackageAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Package__NameAssignment_17415); 
             after(grammarAccess.getPackageAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__NameAssignment_1"


    // $ANTLR start "rule__Package__PackagedElementAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3686:1: rule__Package__PackagedElementAssignment_3 : ( rulePackageableElement ) ;
    public final void rule__Package__PackagedElementAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3690:1: ( ( rulePackageableElement ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3691:1: ( rulePackageableElement )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3691:1: ( rulePackageableElement )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3692:1: rulePackageableElement
            {
             before(grammarAccess.getPackageAccess().getPackagedElementPackageableElementParserRuleCall_3_0()); 
            pushFollow(FOLLOW_rulePackageableElement_in_rule__Package__PackagedElementAssignment_37446);
            rulePackageableElement();

            state._fsp--;

             after(grammarAccess.getPackageAccess().getPackagedElementPackageableElementParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package__PackagedElementAssignment_3"


    // $ANTLR start "rule__Class__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3701:1: rule__Class__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Class__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3705:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3706:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3706:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3707:1: RULE_ID
            {
             before(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Class__NameAssignment_17477); 
             after(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__NameAssignment_1"


    // $ANTLR start "rule__Class__OwnedOperationAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3716:1: rule__Class__OwnedOperationAssignment_3 : ( ruleOperation ) ;
    public final void rule__Class__OwnedOperationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3720:1: ( ( ruleOperation ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3721:1: ( ruleOperation )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3721:1: ( ruleOperation )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3722:1: ruleOperation
            {
             before(grammarAccess.getClassAccess().getOwnedOperationOperationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleOperation_in_rule__Class__OwnedOperationAssignment_37508);
            ruleOperation();

            state._fsp--;

             after(grammarAccess.getClassAccess().getOwnedOperationOperationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__OwnedOperationAssignment_3"


    // $ANTLR start "rule__Class__OwnedAttributeAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3731:1: rule__Class__OwnedAttributeAssignment_4 : ( ruleProperty ) ;
    public final void rule__Class__OwnedAttributeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3735:1: ( ( ruleProperty ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3736:1: ( ruleProperty )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3736:1: ( ruleProperty )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3737:1: ruleProperty
            {
             before(grammarAccess.getClassAccess().getOwnedAttributePropertyParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleProperty_in_rule__Class__OwnedAttributeAssignment_47539);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getClassAccess().getOwnedAttributePropertyParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__OwnedAttributeAssignment_4"


    // $ANTLR start "rule__Operation__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3746:1: rule__Operation__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Operation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3750:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3751:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3751:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3752:1: RULE_ID
            {
             before(grammarAccess.getOperationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Operation__NameAssignment_17570); 
             after(grammarAccess.getOperationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__NameAssignment_1"


    // $ANTLR start "rule__Property__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3761:1: rule__Property__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Property__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3765:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3766:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3766:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3767:1: RULE_ID
            {
             before(grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Property__NameAssignment_17601); 
             after(grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__NameAssignment_1"


    // $ANTLR start "rule__Property__TypeAssignment_2_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3776:1: rule__Property__TypeAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Property__TypeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3780:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3781:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3781:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3782:1: ( RULE_ID )
            {
             before(grammarAccess.getPropertyAccess().getTypeClassCrossReference_2_1_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3783:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3784:1: RULE_ID
            {
             before(grammarAccess.getPropertyAccess().getTypeClassIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Property__TypeAssignment_2_17636); 
             after(grammarAccess.getPropertyAccess().getTypeClassIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getPropertyAccess().getTypeClassCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__TypeAssignment_2_1"


    // $ANTLR start "rule__Property__LowerValueAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3795:1: rule__Property__LowerValueAssignment_4 : ( ruleValueSpecification ) ;
    public final void rule__Property__LowerValueAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3799:1: ( ( ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3800:1: ( ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3800:1: ( ruleValueSpecification )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3801:1: ruleValueSpecification
            {
             before(grammarAccess.getPropertyAccess().getLowerValueValueSpecificationParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_rule__Property__LowerValueAssignment_47671);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getLowerValueValueSpecificationParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__LowerValueAssignment_4"


    // $ANTLR start "rule__Property__UpperValueAssignment_6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3810:1: rule__Property__UpperValueAssignment_6 : ( ruleValueSpecification ) ;
    public final void rule__Property__UpperValueAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3814:1: ( ( ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3815:1: ( ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3815:1: ( ruleValueSpecification )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3816:1: ruleValueSpecification
            {
             before(grammarAccess.getPropertyAccess().getUpperValueValueSpecificationParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_rule__Property__UpperValueAssignment_67702);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getUpperValueValueSpecificationParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__UpperValueAssignment_6"


    // $ANTLR start "rule__LiteralUnlimitedNatural__ValueAssignment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3826:1: rule__LiteralUnlimitedNatural__ValueAssignment : ( RULE_INT ) ;
    public final void rule__LiteralUnlimitedNatural__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3830:1: ( ( RULE_INT ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3831:1: ( RULE_INT )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3831:1: ( RULE_INT )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3832:1: RULE_INT
            {
             before(grammarAccess.getLiteralUnlimitedNaturalAccess().getValueINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__LiteralUnlimitedNatural__ValueAssignment7734); 
             after(grammarAccess.getLiteralUnlimitedNaturalAccess().getValueINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralUnlimitedNatural__ValueAssignment"


    // $ANTLR start "rule__LiteralString__ValueAssignment"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3841:1: rule__LiteralString__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__LiteralString__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3845:1: ( ( RULE_STRING ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3846:1: ( RULE_STRING )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3846:1: ( RULE_STRING )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3847:1: RULE_STRING
            {
             before(grammarAccess.getLiteralStringAccess().getValueSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__LiteralString__ValueAssignment7765); 
             after(grammarAccess.getLiteralStringAccess().getValueSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralString__ValueAssignment"


    // $ANTLR start "rule__Collaboration__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3856:1: rule__Collaboration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Collaboration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3860:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3861:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3861:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3862:1: RULE_ID
            {
             before(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Collaboration__NameAssignment_17796); 
             after(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__NameAssignment_1"


    // $ANTLR start "rule__Collaboration__OwnedAttributeAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3871:1: rule__Collaboration__OwnedAttributeAssignment_3 : ( ruleProperty ) ;
    public final void rule__Collaboration__OwnedAttributeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3875:1: ( ( ruleProperty ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3876:1: ( ruleProperty )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3876:1: ( ruleProperty )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3877:1: ruleProperty
            {
             before(grammarAccess.getCollaborationAccess().getOwnedAttributePropertyParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleProperty_in_rule__Collaboration__OwnedAttributeAssignment_37827);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getCollaborationAccess().getOwnedAttributePropertyParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__OwnedAttributeAssignment_3"


    // $ANTLR start "rule__Collaboration__OwnedBehaviorAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3886:1: rule__Collaboration__OwnedBehaviorAssignment_4 : ( ruleInteraction ) ;
    public final void rule__Collaboration__OwnedBehaviorAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3890:1: ( ( ruleInteraction ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3891:1: ( ruleInteraction )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3891:1: ( ruleInteraction )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3892:1: ruleInteraction
            {
             before(grammarAccess.getCollaborationAccess().getOwnedBehaviorInteractionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleInteraction_in_rule__Collaboration__OwnedBehaviorAssignment_47858);
            ruleInteraction();

            state._fsp--;

             after(grammarAccess.getCollaborationAccess().getOwnedBehaviorInteractionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__OwnedBehaviorAssignment_4"


    // $ANTLR start "rule__Interaction__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3901:1: rule__Interaction__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Interaction__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3905:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3906:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3906:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3907:1: RULE_ID
            {
             before(grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Interaction__NameAssignment_17889); 
             after(grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__NameAssignment_1"


    // $ANTLR start "rule__Interaction__OwnedCommentAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3916:1: rule__Interaction__OwnedCommentAssignment_3 : ( ruleComment ) ;
    public final void rule__Interaction__OwnedCommentAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3920:1: ( ( ruleComment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3921:1: ( ruleComment )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3921:1: ( ruleComment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3922:1: ruleComment
            {
             before(grammarAccess.getInteractionAccess().getOwnedCommentCommentParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleComment_in_rule__Interaction__OwnedCommentAssignment_37920);
            ruleComment();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getOwnedCommentCommentParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__OwnedCommentAssignment_3"


    // $ANTLR start "rule__Interaction__LifelineAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3931:1: rule__Interaction__LifelineAssignment_4 : ( ruleLifeline ) ;
    public final void rule__Interaction__LifelineAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3935:1: ( ( ruleLifeline ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3936:1: ( ruleLifeline )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3936:1: ( ruleLifeline )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3937:1: ruleLifeline
            {
             before(grammarAccess.getInteractionAccess().getLifelineLifelineParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleLifeline_in_rule__Interaction__LifelineAssignment_47951);
            ruleLifeline();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getLifelineLifelineParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__LifelineAssignment_4"


    // $ANTLR start "rule__Interaction__MessageAssignment_5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3946:1: rule__Interaction__MessageAssignment_5 : ( ruleMessage ) ;
    public final void rule__Interaction__MessageAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3950:1: ( ( ruleMessage ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3951:1: ( ruleMessage )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3951:1: ( ruleMessage )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3952:1: ruleMessage
            {
             before(grammarAccess.getInteractionAccess().getMessageMessageParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleMessage_in_rule__Interaction__MessageAssignment_57982);
            ruleMessage();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getMessageMessageParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__MessageAssignment_5"


    // $ANTLR start "rule__Interaction__FragmentAssignment_6"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3961:1: rule__Interaction__FragmentAssignment_6 : ( ruleInteractionFragment ) ;
    public final void rule__Interaction__FragmentAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3965:1: ( ( ruleInteractionFragment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3966:1: ( ruleInteractionFragment )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3966:1: ( ruleInteractionFragment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3967:1: ruleInteractionFragment
            {
             before(grammarAccess.getInteractionAccess().getFragmentInteractionFragmentParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleInteractionFragment_in_rule__Interaction__FragmentAssignment_68013);
            ruleInteractionFragment();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getFragmentInteractionFragmentParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__FragmentAssignment_6"


    // $ANTLR start "rule__CombinedFragment__InteractionOperatorAssignment_2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3976:1: rule__CombinedFragment__InteractionOperatorAssignment_2 : ( ruleInteractionOperatorKind ) ;
    public final void rule__CombinedFragment__InteractionOperatorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3980:1: ( ( ruleInteractionOperatorKind ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3981:1: ( ruleInteractionOperatorKind )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3981:1: ( ruleInteractionOperatorKind )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3982:1: ruleInteractionOperatorKind
            {
             before(grammarAccess.getCombinedFragmentAccess().getInteractionOperatorInteractionOperatorKindEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleInteractionOperatorKind_in_rule__CombinedFragment__InteractionOperatorAssignment_28044);
            ruleInteractionOperatorKind();

            state._fsp--;

             after(grammarAccess.getCombinedFragmentAccess().getInteractionOperatorInteractionOperatorKindEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__InteractionOperatorAssignment_2"


    // $ANTLR start "rule__CombinedFragment__CoveredAssignment_3_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3991:1: rule__CombinedFragment__CoveredAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__CombinedFragment__CoveredAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3995:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3996:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3996:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3997:1: ( RULE_ID )
            {
             before(grammarAccess.getCombinedFragmentAccess().getCoveredLifelineCrossReference_3_1_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3998:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:3999:1: RULE_ID
            {
             before(grammarAccess.getCombinedFragmentAccess().getCoveredLifelineIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__CombinedFragment__CoveredAssignment_3_18079); 
             after(grammarAccess.getCombinedFragmentAccess().getCoveredLifelineIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getCombinedFragmentAccess().getCoveredLifelineCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__CoveredAssignment_3_1"


    // $ANTLR start "rule__CombinedFragment__OperandAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4010:1: rule__CombinedFragment__OperandAssignment_4 : ( ruleInteractionOperand ) ;
    public final void rule__CombinedFragment__OperandAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4014:1: ( ( ruleInteractionOperand ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4015:1: ( ruleInteractionOperand )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4015:1: ( ruleInteractionOperand )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4016:1: ruleInteractionOperand
            {
             before(grammarAccess.getCombinedFragmentAccess().getOperandInteractionOperandParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleInteractionOperand_in_rule__CombinedFragment__OperandAssignment_48114);
            ruleInteractionOperand();

            state._fsp--;

             after(grammarAccess.getCombinedFragmentAccess().getOperandInteractionOperandParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CombinedFragment__OperandAssignment_4"


    // $ANTLR start "rule__Comment__BodyAssignment_2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4025:1: rule__Comment__BodyAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Comment__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4029:1: ( ( RULE_STRING ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4030:1: ( RULE_STRING )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4030:1: ( RULE_STRING )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4031:1: RULE_STRING
            {
             before(grammarAccess.getCommentAccess().getBodySTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Comment__BodyAssignment_28145); 
             after(grammarAccess.getCommentAccess().getBodySTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__BodyAssignment_2"


    // $ANTLR start "rule__InteractionOperand__CoveredAssignment_3_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4040:1: rule__InteractionOperand__CoveredAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__InteractionOperand__CoveredAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4044:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4045:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4045:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4046:1: ( RULE_ID )
            {
             before(grammarAccess.getInteractionOperandAccess().getCoveredLifelineCrossReference_3_1_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4047:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4048:1: RULE_ID
            {
             before(grammarAccess.getInteractionOperandAccess().getCoveredLifelineIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__InteractionOperand__CoveredAssignment_3_18180); 
             after(grammarAccess.getInteractionOperandAccess().getCoveredLifelineIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getInteractionOperandAccess().getCoveredLifelineCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__CoveredAssignment_3_1"


    // $ANTLR start "rule__InteractionOperand__GuardAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4059:1: rule__InteractionOperand__GuardAssignment_4 : ( ruleInteractionConstraint ) ;
    public final void rule__InteractionOperand__GuardAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4063:1: ( ( ruleInteractionConstraint ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4064:1: ( ruleInteractionConstraint )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4064:1: ( ruleInteractionConstraint )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4065:1: ruleInteractionConstraint
            {
             before(grammarAccess.getInteractionOperandAccess().getGuardInteractionConstraintParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleInteractionConstraint_in_rule__InteractionOperand__GuardAssignment_48215);
            ruleInteractionConstraint();

            state._fsp--;

             after(grammarAccess.getInteractionOperandAccess().getGuardInteractionConstraintParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__GuardAssignment_4"


    // $ANTLR start "rule__InteractionOperand__FragmentAssignment_5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4074:1: rule__InteractionOperand__FragmentAssignment_5 : ( ruleInteractionFragment ) ;
    public final void rule__InteractionOperand__FragmentAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4078:1: ( ( ruleInteractionFragment ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4079:1: ( ruleInteractionFragment )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4079:1: ( ruleInteractionFragment )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4080:1: ruleInteractionFragment
            {
             before(grammarAccess.getInteractionOperandAccess().getFragmentInteractionFragmentParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleInteractionFragment_in_rule__InteractionOperand__FragmentAssignment_58246);
            ruleInteractionFragment();

            state._fsp--;

             after(grammarAccess.getInteractionOperandAccess().getFragmentInteractionFragmentParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionOperand__FragmentAssignment_5"


    // $ANTLR start "rule__InteractionConstraint__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4089:1: rule__InteractionConstraint__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__InteractionConstraint__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4093:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4094:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4094:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4095:1: RULE_ID
            {
             before(grammarAccess.getInteractionConstraintAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__InteractionConstraint__NameAssignment_18277); 
             after(grammarAccess.getInteractionConstraintAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__NameAssignment_1"


    // $ANTLR start "rule__InteractionConstraint__MinintAssignment_2"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4104:1: rule__InteractionConstraint__MinintAssignment_2 : ( ruleValueSpecification ) ;
    public final void rule__InteractionConstraint__MinintAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4108:1: ( ( ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4109:1: ( ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4109:1: ( ruleValueSpecification )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4110:1: ruleValueSpecification
            {
             before(grammarAccess.getInteractionConstraintAccess().getMinintValueSpecificationParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__MinintAssignment_28308);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getInteractionConstraintAccess().getMinintValueSpecificationParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__MinintAssignment_2"


    // $ANTLR start "rule__InteractionConstraint__MaxintAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4119:1: rule__InteractionConstraint__MaxintAssignment_3 : ( ruleValueSpecification ) ;
    public final void rule__InteractionConstraint__MaxintAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4123:1: ( ( ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4124:1: ( ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4124:1: ( ruleValueSpecification )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4125:1: ruleValueSpecification
            {
             before(grammarAccess.getInteractionConstraintAccess().getMaxintValueSpecificationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__MaxintAssignment_38339);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getInteractionConstraintAccess().getMaxintValueSpecificationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__MaxintAssignment_3"


    // $ANTLR start "rule__InteractionConstraint__SpecificationAssignment_4"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4134:1: rule__InteractionConstraint__SpecificationAssignment_4 : ( ruleValueSpecification ) ;
    public final void rule__InteractionConstraint__SpecificationAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4138:1: ( ( ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4139:1: ( ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4139:1: ( ruleValueSpecification )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4140:1: ruleValueSpecification
            {
             before(grammarAccess.getInteractionConstraintAccess().getSpecificationValueSpecificationParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__SpecificationAssignment_48370);
            ruleValueSpecification();

            state._fsp--;

             after(grammarAccess.getInteractionConstraintAccess().getSpecificationValueSpecificationParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionConstraint__SpecificationAssignment_4"


    // $ANTLR start "rule__Lifeline__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4149:1: rule__Lifeline__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Lifeline__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4153:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4154:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4154:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4155:1: RULE_ID
            {
             before(grammarAccess.getLifelineAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Lifeline__NameAssignment_18401); 
             after(grammarAccess.getLifelineAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__NameAssignment_1"


    // $ANTLR start "rule__Lifeline__RepresentsAssignment_2_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4164:1: rule__Lifeline__RepresentsAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Lifeline__RepresentsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4168:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4169:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4169:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4170:1: ( RULE_ID )
            {
             before(grammarAccess.getLifelineAccess().getRepresentsPropertyCrossReference_2_1_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4171:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4172:1: RULE_ID
            {
             before(grammarAccess.getLifelineAccess().getRepresentsPropertyIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Lifeline__RepresentsAssignment_2_18436); 
             after(grammarAccess.getLifelineAccess().getRepresentsPropertyIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getLifelineAccess().getRepresentsPropertyCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lifeline__RepresentsAssignment_2_1"


    // $ANTLR start "rule__Message__MessageSortAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4183:1: rule__Message__MessageSortAssignment_1 : ( ruleMessageSort ) ;
    public final void rule__Message__MessageSortAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4187:1: ( ( ruleMessageSort ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4188:1: ( ruleMessageSort )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4188:1: ( ruleMessageSort )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4189:1: ruleMessageSort
            {
             before(grammarAccess.getMessageAccess().getMessageSortMessageSortEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleMessageSort_in_rule__Message__MessageSortAssignment_18471);
            ruleMessageSort();

            state._fsp--;

             after(grammarAccess.getMessageAccess().getMessageSortMessageSortEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__MessageSortAssignment_1"


    // $ANTLR start "rule__Message__ReceiveEventAssignment_3"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4198:1: rule__Message__ReceiveEventAssignment_3 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Message__ReceiveEventAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4202:1: ( ( ( ruleQualifiedName ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4203:1: ( ( ruleQualifiedName ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4203:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4204:1: ( ruleQualifiedName )
            {
             before(grammarAccess.getMessageAccess().getReceiveEventMessageOccurrenceSpecificationCrossReference_3_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4205:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4206:1: ruleQualifiedName
            {
             before(grammarAccess.getMessageAccess().getReceiveEventMessageOccurrenceSpecificationQualifiedNameParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__Message__ReceiveEventAssignment_38506);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getMessageAccess().getReceiveEventMessageOccurrenceSpecificationQualifiedNameParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getMessageAccess().getReceiveEventMessageOccurrenceSpecificationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__ReceiveEventAssignment_3"


    // $ANTLR start "rule__Message__SendEventAssignment_5"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4217:1: rule__Message__SendEventAssignment_5 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Message__SendEventAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4221:1: ( ( ( ruleQualifiedName ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4222:1: ( ( ruleQualifiedName ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4222:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4223:1: ( ruleQualifiedName )
            {
             before(grammarAccess.getMessageAccess().getSendEventMessageOccurrenceSpecificationCrossReference_5_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4224:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4225:1: ruleQualifiedName
            {
             before(grammarAccess.getMessageAccess().getSendEventMessageOccurrenceSpecificationQualifiedNameParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__Message__SendEventAssignment_58545);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getMessageAccess().getSendEventMessageOccurrenceSpecificationQualifiedNameParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getMessageAccess().getSendEventMessageOccurrenceSpecificationCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__SendEventAssignment_5"


    // $ANTLR start "rule__Message__SignatureAssignment_7"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4236:1: rule__Message__SignatureAssignment_7 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Message__SignatureAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4240:1: ( ( ( ruleQualifiedName ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4241:1: ( ( ruleQualifiedName ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4241:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4242:1: ( ruleQualifiedName )
            {
             before(grammarAccess.getMessageAccess().getSignatureOperationCrossReference_7_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4243:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4244:1: ruleQualifiedName
            {
             before(grammarAccess.getMessageAccess().getSignatureOperationQualifiedNameParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__Message__SignatureAssignment_78584);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getMessageAccess().getSignatureOperationQualifiedNameParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getMessageAccess().getSignatureOperationCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__SignatureAssignment_7"


    // $ANTLR start "rule__MessageOccurrenceSpecification__NameAssignment_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4255:1: rule__MessageOccurrenceSpecification__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__MessageOccurrenceSpecification__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4259:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4260:1: ( RULE_ID )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4260:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4261:1: RULE_ID
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__MessageOccurrenceSpecification__NameAssignment_18619); 
             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__NameAssignment_1"


    // $ANTLR start "rule__MessageOccurrenceSpecification__CoveredAssignment_2_1"
    // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4270:1: rule__MessageOccurrenceSpecification__CoveredAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__MessageOccurrenceSpecification__CoveredAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4274:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4275:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4275:1: ( ( RULE_ID ) )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4276:1: ( RULE_ID )
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredLifelineCrossReference_2_1_0()); 
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4277:1: ( RULE_ID )
            // ../org.scenariotools.msd.xtext.ui/src-gen/org/scenariotools/msd/xtext/ui/contentassist/antlr/internal/InternalUml.g:4278:1: RULE_ID
            {
             before(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredLifelineIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__MessageOccurrenceSpecification__CoveredAssignment_2_18654); 
             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredLifelineIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredLifelineCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageOccurrenceSpecification__CoveredAssignment_2_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__PackagedElementAssignment_in_ruleModel94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_entryRulePackage121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackage128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__Group__0_in_rulePackage154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackageableElement_in_entryRulePackageableElement181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackageableElement188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PackageableElement__Alternatives_in_rulePackageableElement214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_entryRuleClass241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClass248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__0_in_ruleClass274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOperation_in_entryRuleOperation301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOperation308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Operation__Group__0_in_ruleOperation334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProperty_in_entryRuleProperty361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProperty368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__0_in_ruleProperty394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_entryRuleValueSpecification421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleValueSpecification428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ValueSpecification__Alternatives_in_ruleValueSpecification454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralUnlimitedNatural_in_entryRuleLiteralUnlimitedNatural483 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteralUnlimitedNatural490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LiteralUnlimitedNatural__ValueAssignment_in_ruleLiteralUnlimitedNatural516 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralString_in_entryRuleLiteralString543 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteralString550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LiteralString__ValueAssignment_in_ruleLiteralString576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCollaboration_in_entryRuleCollaboration603 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCollaboration610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__0_in_ruleCollaboration636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteraction_in_entryRuleInteraction663 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteraction670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__0_in_ruleInteraction696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_entryRuleInteractionFragment723 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionFragment730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionFragment__Alternatives_in_ruleInteractionFragment756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCombinedFragment_in_entryRuleCombinedFragment783 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCombinedFragment790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__0_in_ruleCombinedFragment816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComment_in_entryRuleComment843 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComment850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__Group__0_in_ruleComment876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionOperand_in_entryRuleInteractionOperand903 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionOperand910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__0_in_ruleInteractionOperand936 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionConstraint_in_entryRuleInteractionConstraint963 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionConstraint970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__0_in_ruleInteractionConstraint996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLifeline_in_entryRuleLifeline1023 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLifeline1030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__0_in_ruleLifeline1056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage1083 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage1090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__0_in_ruleMessage1116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageOccurrenceSpecification_in_entryRuleMessageOccurrenceSpecification1143 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessageOccurrenceSpecification1150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__0_in_ruleMessageOccurrenceSpecification1176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1203 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName1210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperatorKind__Alternatives_in_ruleInteractionOperatorKind1273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageSort__Alternatives_in_ruleMessageSort1309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_rule__PackageableElement__Alternatives1344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_rule__PackageableElement__Alternatives1361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCollaboration_in_rule__PackageableElement__Alternatives1378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralUnlimitedNatural_in_rule__ValueSpecification__Alternatives1410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralString_in_rule__ValueSpecification__Alternatives1427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageOccurrenceSpecification_in_rule__InteractionFragment__Alternatives1459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCombinedFragment_in_rule__InteractionFragment__Alternatives1476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__InteractionOperatorKind__Alternatives1509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__InteractionOperatorKind__Alternatives1530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__InteractionOperatorKind__Alternatives1551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__InteractionOperatorKind__Alternatives1572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__InteractionOperatorKind__Alternatives1593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__InteractionOperatorKind__Alternatives1614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__InteractionOperatorKind__Alternatives1635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__InteractionOperatorKind__Alternatives1656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__InteractionOperatorKind__Alternatives1677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__InteractionOperatorKind__Alternatives1698 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__InteractionOperatorKind__Alternatives1719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__InteractionOperatorKind__Alternatives1740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__MessageSort__Alternatives1776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__MessageSort__Alternatives1797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__MessageSort__Alternatives1818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__MessageSort__Alternatives1839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__MessageSort__Alternatives1860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__MessageSort__Alternatives1881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__Group__0__Impl_in_rule__Package__Group__01914 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Package__Group__1_in_rule__Package__Group__01917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Package__Group__0__Impl1945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__Group__1__Impl_in_rule__Package__Group__11976 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Package__Group__2_in_rule__Package__Group__11979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__NameAssignment_1_in_rule__Package__Group__1__Impl2006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__Group__2__Impl_in_rule__Package__Group__22036 = new BitSet(new long[]{0x00000101A0000000L});
    public static final BitSet FOLLOW_rule__Package__Group__3_in_rule__Package__Group__22039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Package__Group__2__Impl2067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__Group__3__Impl_in_rule__Package__Group__32098 = new BitSet(new long[]{0x00000101A0000000L});
    public static final BitSet FOLLOW_rule__Package__Group__4_in_rule__Package__Group__32101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package__PackagedElementAssignment_3_in_rule__Package__Group__3__Impl2128 = new BitSet(new long[]{0x0000010120000002L});
    public static final BitSet FOLLOW_rule__Package__Group__4__Impl_in_rule__Package__Group__42159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Package__Group__4__Impl2187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02228 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Class__Group__0__Impl2259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12290 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2320 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22350 = new BitSet(new long[]{0x0000000A80000000L});
    public static final BitSet FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Class__Group__2__Impl2381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32412 = new BitSet(new long[]{0x0000000A80000000L});
    public static final BitSet FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__OwnedOperationAssignment_3_in_rule__Class__Group__3__Impl2442 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42473 = new BitSet(new long[]{0x0000000A80000000L});
    public static final BitSet FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__OwnedAttributeAssignment_4_in_rule__Class__Group__4__Impl2503 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Class__Group__5__Impl2562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Operation__Group__0__Impl_in_rule__Operation__Group__02605 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Operation__Group__1_in_rule__Operation__Group__02608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__Operation__Group__0__Impl2636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Operation__Group__1__Impl_in_rule__Operation__Group__12667 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__Operation__Group__2_in_rule__Operation__Group__12670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Operation__NameAssignment_1_in_rule__Operation__Group__1__Impl2697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Operation__Group__2__Impl_in_rule__Operation__Group__22727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__Operation__Group__2__Impl2755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__0__Impl_in_rule__Property__Group__02792 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Property__Group__1_in_rule__Property__Group__02795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__Property__Group__0__Impl2823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__1__Impl_in_rule__Property__Group__12854 = new BitSet(new long[]{0x0000009000000000L});
    public static final BitSet FOLLOW_rule__Property__Group__2_in_rule__Property__Group__12857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__NameAssignment_1_in_rule__Property__Group__1__Impl2884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__2__Impl_in_rule__Property__Group__22914 = new BitSet(new long[]{0x0000009000000000L});
    public static final BitSet FOLLOW_rule__Property__Group__3_in_rule__Property__Group__22917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group_2__0_in_rule__Property__Group__2__Impl2944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__3__Impl_in_rule__Property__Group__32975 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_rule__Property__Group__4_in_rule__Property__Group__32978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__Property__Group__3__Impl3006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__4__Impl_in_rule__Property__Group__43037 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_rule__Property__Group__5_in_rule__Property__Group__43040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__LowerValueAssignment_4_in_rule__Property__Group__4__Impl3067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__5__Impl_in_rule__Property__Group__53097 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_rule__Property__Group__6_in_rule__Property__Group__53100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Property__Group__5__Impl3128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__6__Impl_in_rule__Property__Group__63159 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__Property__Group__7_in_rule__Property__Group__63162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__UpperValueAssignment_6_in_rule__Property__Group__6__Impl3189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__7__Impl_in_rule__Property__Group__73219 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__Property__Group__8_in_rule__Property__Group__73222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__Property__Group__7__Impl3250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group__8__Impl_in_rule__Property__Group__83281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__Property__Group__8__Impl3309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group_2__0__Impl_in_rule__Property__Group_2__03358 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Property__Group_2__1_in_rule__Property__Group_2__03361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__Property__Group_2__0__Impl3389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__Group_2__1__Impl_in_rule__Property__Group_2__13420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Property__TypeAssignment_2_1_in_rule__Property__Group_2__1__Impl3447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__0__Impl_in_rule__Collaboration__Group__03481 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__1_in_rule__Collaboration__Group__03484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__Collaboration__Group__0__Impl3512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__1__Impl_in_rule__Collaboration__Group__13543 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__2_in_rule__Collaboration__Group__13546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__NameAssignment_1_in_rule__Collaboration__Group__1__Impl3573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__2__Impl_in_rule__Collaboration__Group__23603 = new BitSet(new long[]{0x0000020880000000L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__3_in_rule__Collaboration__Group__23606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Collaboration__Group__2__Impl3634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__3__Impl_in_rule__Collaboration__Group__33665 = new BitSet(new long[]{0x0000020880000000L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__4_in_rule__Collaboration__Group__33668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__OwnedAttributeAssignment_3_in_rule__Collaboration__Group__3__Impl3695 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__4__Impl_in_rule__Collaboration__Group__43726 = new BitSet(new long[]{0x0000020880000000L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__5_in_rule__Collaboration__Group__43729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__OwnedBehaviorAssignment_4_in_rule__Collaboration__Group__4__Impl3756 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_rule__Collaboration__Group__5__Impl_in_rule__Collaboration__Group__53787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Collaboration__Group__5__Impl3815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__0__Impl_in_rule__Interaction__Group__03858 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Interaction__Group__1_in_rule__Interaction__Group__03861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__Interaction__Group__0__Impl3889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__1__Impl_in_rule__Interaction__Group__13920 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__2_in_rule__Interaction__Group__13923 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__NameAssignment_1_in_rule__Interaction__Group__1__Impl3950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__2__Impl_in_rule__Interaction__Group__23980 = new BitSet(new long[]{0x0022940080000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__3_in_rule__Interaction__Group__23983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Interaction__Group__2__Impl4011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__3__Impl_in_rule__Interaction__Group__34042 = new BitSet(new long[]{0x0022940080000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__4_in_rule__Interaction__Group__34045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__OwnedCommentAssignment_3_in_rule__Interaction__Group__3__Impl4072 = new BitSet(new long[]{0x0000100000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__4__Impl_in_rule__Interaction__Group__44103 = new BitSet(new long[]{0x0022940080000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__5_in_rule__Interaction__Group__44106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__LifelineAssignment_4_in_rule__Interaction__Group__4__Impl4133 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__5__Impl_in_rule__Interaction__Group__54164 = new BitSet(new long[]{0x0022940080000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__6_in_rule__Interaction__Group__54167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__MessageAssignment_5_in_rule__Interaction__Group__5__Impl4194 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__6__Impl_in_rule__Interaction__Group__64225 = new BitSet(new long[]{0x0022940080000000L});
    public static final BitSet FOLLOW_rule__Interaction__Group__7_in_rule__Interaction__Group__64228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Interaction__FragmentAssignment_6_in_rule__Interaction__Group__6__Impl4255 = new BitSet(new long[]{0x0020040000000002L});
    public static final BitSet FOLLOW_rule__Interaction__Group__7__Impl_in_rule__Interaction__Group__74286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Interaction__Group__7__Impl4314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__0__Impl_in_rule__CombinedFragment__Group__04361 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__1_in_rule__CombinedFragment__Group__04364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__CombinedFragment__Group__0__Impl4392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__1__Impl_in_rule__CombinedFragment__Group__14423 = new BitSet(new long[]{0x00000000007FF800L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__2_in_rule__CombinedFragment__Group__14426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__CombinedFragment__Group__1__Impl4454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__2__Impl_in_rule__CombinedFragment__Group__24485 = new BitSet(new long[]{0x0000280000000000L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__3_in_rule__CombinedFragment__Group__24488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__InteractionOperatorAssignment_2_in_rule__CombinedFragment__Group__2__Impl4515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__3__Impl_in_rule__CombinedFragment__Group__34545 = new BitSet(new long[]{0x0000280000000000L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__4_in_rule__CombinedFragment__Group__34548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group_3__0_in_rule__CombinedFragment__Group__3__Impl4575 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__4__Impl_in_rule__CombinedFragment__Group__44606 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__5_in_rule__CombinedFragment__Group__44609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__OperandAssignment_4_in_rule__CombinedFragment__Group__4__Impl4638 = new BitSet(new long[]{0x0000280000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__OperandAssignment_4_in_rule__CombinedFragment__Group__4__Impl4650 = new BitSet(new long[]{0x0000280000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group__5__Impl_in_rule__CombinedFragment__Group__54683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__CombinedFragment__Group__5__Impl4711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group_3__0__Impl_in_rule__CombinedFragment__Group_3__04754 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group_3__1_in_rule__CombinedFragment__Group_3__04757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__CombinedFragment__Group_3__0__Impl4785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__Group_3__1__Impl_in_rule__CombinedFragment__Group_3__14816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CombinedFragment__CoveredAssignment_3_1_in_rule__CombinedFragment__Group_3__1__Impl4843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__Group__0__Impl_in_rule__Comment__Group__04877 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Comment__Group__1_in_rule__Comment__Group__04880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__Comment__Group__0__Impl4908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__Group__1__Impl_in_rule__Comment__Group__14939 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Comment__Group__2_in_rule__Comment__Group__14942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Comment__Group__1__Impl4970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__Group__2__Impl_in_rule__Comment__Group__25001 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__Comment__Group__3_in_rule__Comment__Group__25004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__BodyAssignment_2_in_rule__Comment__Group__2__Impl5031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Comment__Group__3__Impl_in_rule__Comment__Group__35061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Comment__Group__3__Impl5089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__0__Impl_in_rule__InteractionOperand__Group__05128 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__1_in_rule__InteractionOperand__Group__05131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rule__InteractionOperand__Group__0__Impl5159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__1__Impl_in_rule__InteractionOperand__Group__15190 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__2_in_rule__InteractionOperand__Group__15193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__2__Impl_in_rule__InteractionOperand__Group__25251 = new BitSet(new long[]{0x00204C0080000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__3_in_rule__InteractionOperand__Group__25254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__InteractionOperand__Group__2__Impl5282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__3__Impl_in_rule__InteractionOperand__Group__35313 = new BitSet(new long[]{0x00204C0080000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__4_in_rule__InteractionOperand__Group__35316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group_3__0_in_rule__InteractionOperand__Group__3__Impl5343 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__4__Impl_in_rule__InteractionOperand__Group__45374 = new BitSet(new long[]{0x00204C0080000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__5_in_rule__InteractionOperand__Group__45377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__GuardAssignment_4_in_rule__InteractionOperand__Group__4__Impl5404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__5__Impl_in_rule__InteractionOperand__Group__55435 = new BitSet(new long[]{0x00204C0080000000L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__6_in_rule__InteractionOperand__Group__55438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__FragmentAssignment_5_in_rule__InteractionOperand__Group__5__Impl5465 = new BitSet(new long[]{0x0020040000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group__6__Impl_in_rule__InteractionOperand__Group__65496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__InteractionOperand__Group__6__Impl5524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group_3__0__Impl_in_rule__InteractionOperand__Group_3__05569 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group_3__1_in_rule__InteractionOperand__Group_3__05572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__InteractionOperand__Group_3__0__Impl5600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__Group_3__1__Impl_in_rule__InteractionOperand__Group_3__15631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionOperand__CoveredAssignment_3_1_in_rule__InteractionOperand__Group_3__1__Impl5658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__0__Impl_in_rule__InteractionConstraint__Group__05692 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__1_in_rule__InteractionConstraint__Group__05695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_rule__InteractionConstraint__Group__0__Impl5723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__1__Impl_in_rule__InteractionConstraint__Group__15754 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__2_in_rule__InteractionConstraint__Group__15757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__NameAssignment_1_in_rule__InteractionConstraint__Group__1__Impl5784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__2__Impl_in_rule__InteractionConstraint__Group__25814 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__3_in_rule__InteractionConstraint__Group__25817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__MinintAssignment_2_in_rule__InteractionConstraint__Group__2__Impl5844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__3__Impl_in_rule__InteractionConstraint__Group__35874 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__4_in_rule__InteractionConstraint__Group__35877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__MaxintAssignment_3_in_rule__InteractionConstraint__Group__3__Impl5904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__Group__4__Impl_in_rule__InteractionConstraint__Group__45934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InteractionConstraint__SpecificationAssignment_4_in_rule__InteractionConstraint__Group__4__Impl5961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__0__Impl_in_rule__Lifeline__Group__06001 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__1_in_rule__Lifeline__Group__06004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule__Lifeline__Group__0__Impl6032 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__1__Impl_in_rule__Lifeline__Group__16063 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__2_in_rule__Lifeline__Group__16066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__NameAssignment_1_in_rule__Lifeline__Group__1__Impl6093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group__2__Impl_in_rule__Lifeline__Group__26123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group_2__0_in_rule__Lifeline__Group__2__Impl6150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group_2__0__Impl_in_rule__Lifeline__Group_2__06187 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Lifeline__Group_2__1_in_rule__Lifeline__Group_2__06190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__Lifeline__Group_2__0__Impl6218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__Group_2__1__Impl_in_rule__Lifeline__Group_2__16249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lifeline__RepresentsAssignment_2_1_in_rule__Lifeline__Group_2__1__Impl6276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__0__Impl_in_rule__Message__Group__06310 = new BitSet(new long[]{0x000000001F800000L});
    public static final BitSet FOLLOW_rule__Message__Group__1_in_rule__Message__Group__06313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rule__Message__Group__0__Impl6341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__1__Impl_in_rule__Message__Group__16372 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_rule__Message__Group__2_in_rule__Message__Group__16375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__MessageSortAssignment_1_in_rule__Message__Group__1__Impl6402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__2__Impl_in_rule__Message__Group__26432 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Message__Group__3_in_rule__Message__Group__26435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rule__Message__Group__2__Impl6463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__3__Impl_in_rule__Message__Group__36494 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_rule__Message__Group__4_in_rule__Message__Group__36497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__ReceiveEventAssignment_3_in_rule__Message__Group__3__Impl6524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__4__Impl_in_rule__Message__Group__46554 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Message__Group__5_in_rule__Message__Group__46557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_rule__Message__Group__4__Impl6585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__5__Impl_in_rule__Message__Group__56616 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_rule__Message__Group__6_in_rule__Message__Group__56619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__SendEventAssignment_5_in_rule__Message__Group__5__Impl6646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__6__Impl_in_rule__Message__Group__66676 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Message__Group__7_in_rule__Message__Group__66679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_rule__Message__Group__6__Impl6707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Group__7__Impl_in_rule__Message__Group__76738 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__SignatureAssignment_7_in_rule__Message__Group__7__Impl6765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__0__Impl_in_rule__MessageOccurrenceSpecification__Group__06811 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__1_in_rule__MessageOccurrenceSpecification__Group__06814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_rule__MessageOccurrenceSpecification__Group__0__Impl6842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__1__Impl_in_rule__MessageOccurrenceSpecification__Group__16873 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__2_in_rule__MessageOccurrenceSpecification__Group__16876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__NameAssignment_1_in_rule__MessageOccurrenceSpecification__Group__1__Impl6903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group__2__Impl_in_rule__MessageOccurrenceSpecification__Group__26933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0_in_rule__MessageOccurrenceSpecification__Group__2__Impl6962 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0_in_rule__MessageOccurrenceSpecification__Group__2__Impl6974 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group_2__0__Impl_in_rule__MessageOccurrenceSpecification__Group_2__07013 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group_2__1_in_rule__MessageOccurrenceSpecification__Group_2__07016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__MessageOccurrenceSpecification__Group_2__0__Impl7044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__Group_2__1__Impl_in_rule__MessageOccurrenceSpecification__Group_2__17075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MessageOccurrenceSpecification__CoveredAssignment_2_1_in_rule__MessageOccurrenceSpecification__Group_2__1__Impl7102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__07136 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__07139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl7166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__17195 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl7222 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__07257 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__07260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_rule__QualifiedName__Group_1__0__Impl7288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__17319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl7346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_rule__Model__PackagedElementAssignment7384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Package__NameAssignment_17415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackageableElement_in_rule__Package__PackagedElementAssignment_37446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Class__NameAssignment_17477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOperation_in_rule__Class__OwnedOperationAssignment_37508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProperty_in_rule__Class__OwnedAttributeAssignment_47539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Operation__NameAssignment_17570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Property__NameAssignment_17601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Property__TypeAssignment_2_17636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_rule__Property__LowerValueAssignment_47671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_rule__Property__UpperValueAssignment_67702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__LiteralUnlimitedNatural__ValueAssignment7734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__LiteralString__ValueAssignment7765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Collaboration__NameAssignment_17796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProperty_in_rule__Collaboration__OwnedAttributeAssignment_37827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteraction_in_rule__Collaboration__OwnedBehaviorAssignment_47858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Interaction__NameAssignment_17889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComment_in_rule__Interaction__OwnedCommentAssignment_37920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLifeline_in_rule__Interaction__LifelineAssignment_47951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_rule__Interaction__MessageAssignment_57982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_rule__Interaction__FragmentAssignment_68013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionOperatorKind_in_rule__CombinedFragment__InteractionOperatorAssignment_28044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__CombinedFragment__CoveredAssignment_3_18079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionOperand_in_rule__CombinedFragment__OperandAssignment_48114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Comment__BodyAssignment_28145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__InteractionOperand__CoveredAssignment_3_18180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionConstraint_in_rule__InteractionOperand__GuardAssignment_48215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_rule__InteractionOperand__FragmentAssignment_58246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__InteractionConstraint__NameAssignment_18277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__MinintAssignment_28308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__MaxintAssignment_38339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_rule__InteractionConstraint__SpecificationAssignment_48370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Lifeline__NameAssignment_18401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Lifeline__RepresentsAssignment_2_18436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageSort_in_rule__Message__MessageSortAssignment_18471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__Message__ReceiveEventAssignment_38506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__Message__SendEventAssignment_58545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__Message__SignatureAssignment_78584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__MessageOccurrenceSpecification__NameAssignment_18619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__MessageOccurrenceSpecification__CoveredAssignment_2_18654 = new BitSet(new long[]{0x0000000000000002L});

}