/**
 */
package org.scenariotools.msd.scenariorunconfiguration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface ScenariorunconfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scenariorunconfiguration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.scenariorunconfiguration/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scenariorunconfiguration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScenariorunconfigurationPackage eINSTANCE = org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl <em>Scenario Run Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl
	 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl#getScenarioRunConfiguration()
	 * @generated
	 */
	int SCENARIO_RUN_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Uml2 Ecore Mapping</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Simulation Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS = 1;

	/**
	 * The feature id for the '<em><b>Static Model Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS = 2;

	/**
	 * The feature id for the '<em><b>Role To EObject Mapping Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER = 3;

	/**
	 * The feature id for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = 4;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH = 5;

	/**
	 * The feature id for the '<em><b>Execution Semantics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS = 6;

	/**
	 * The feature id for the '<em><b>Store Additional MSD Modal Message Event Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA = 7;

	/**
	 * The feature id for the '<em><b>Dynamic Object System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM = 8;

	/**
	 * The feature id for the '<em><b>Only Communicate Via Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS = 9;

	/**
	 * The feature id for the '<em><b>Communication Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS = 10;

	/**
	 * The feature id for the '<em><b>Only Sync Via Communication Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS = 11;

	/**
	 * The feature id for the '<em><b>Strategy Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE = 12;

	/**
	 * The feature id for the '<em><b>Handle Real Time Inconsistencies Conservatively</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY = 13;

	/**
	 * The number of structural features of the '<em>Scenario Run Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_RUN_CONFIGURATION_FEATURE_COUNT = 14;


	/**
	 * The meta object id for the '{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl <em>Communication Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl
	 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl#getCommunicationLink()
	 * @generated
	 */
	int COMMUNICATION_LINK = 1;

	/**
	 * The feature id for the '<em><b>Forbidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_LINK__FORBIDDEN = 0;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_LINK__OPTIONAL = 1;

	/**
	 * The feature id for the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_LINK__SENDER = 2;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_LINK__RECEIVER = 3;

	/**
	 * The number of structural features of the '<em>Communication Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_LINK_FEATURE_COUNT = 4;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration <em>Scenario Run Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario Run Configuration</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration
	 * @generated
	 */
	EClass getScenarioRunConfiguration();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUml2EcoreMapping <em>Uml2 Ecore Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml2 Ecore Mapping</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUml2EcoreMapping()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_Uml2EcoreMapping();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getSimulationRootObjects <em>Simulation Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Simulation Root Objects</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getSimulationRootObjects()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_SimulationRootObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStaticModelRootObjects <em>Static Model Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Static Model Root Objects</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStaticModelRootObjects()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_StaticModelRootObjects();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getRoleToEObjectMappingContainer <em>Role To EObject Mapping Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role To EObject Mapping Container</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getRoleToEObjectMappingContainer()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_RoleToEObjectMappingContainer();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Use Case Specifications To Be Considered</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUseCaseSpecificationsToBeConsidered()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_UseCaseSpecificationsToBeConsidered();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Msd Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getMsdRuntimeStateGraph()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_MsdRuntimeStateGraph();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getExecutionSemantics <em>Execution Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Semantics</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getExecutionSemantics()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_ExecutionSemantics();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isStoreAdditionalMSDModalMessageEventData <em>Store Additional MSD Modal Message Event Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Store Additional MSD Modal Message Event Data</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isStoreAdditionalMSDModalMessageEventData()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_StoreAdditionalMSDModalMessageEventData();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isDynamicObjectSystem <em>Dynamic Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dynamic Object System</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isDynamicObjectSystem()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_DynamicObjectSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlyCommunicateViaLinks <em>Only Communicate Via Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Only Communicate Via Links</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlyCommunicateViaLinks()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_OnlyCommunicateViaLinks();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getCommunicationLinks <em>Communication Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Links</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getCommunicationLinks()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_CommunicationLinks();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlySyncViaCommunicationLinks <em>Only Sync Via Communication Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Only Sync Via Communication Links</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlySyncViaCommunicationLinks()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_OnlySyncViaCommunicationLinks();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStrategyPackage <em>Strategy Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Strategy Package</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStrategyPackage()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EReference getScenarioRunConfiguration_StrategyPackage();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isHandleRealTimeInconsistenciesConservatively <em>Handle Real Time Inconsistencies Conservatively</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Handle Real Time Inconsistencies Conservatively</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isHandleRealTimeInconsistenciesConservatively()
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	EAttribute getScenarioRunConfiguration_HandleRealTimeInconsistenciesConservatively();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink <em>Communication Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Link</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.CommunicationLink
	 * @generated
	 */
	EClass getCommunicationLink();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isForbidden <em>Forbidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Forbidden</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isForbidden()
	 * @see #getCommunicationLink()
	 * @generated
	 */
	EAttribute getCommunicationLink_Forbidden();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isOptional()
	 * @see #getCommunicationLink()
	 * @generated
	 */
	EAttribute getCommunicationLink_Optional();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getSender <em>Sender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sender</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getSender()
	 * @see #getCommunicationLink()
	 * @generated
	 */
	EReference getCommunicationLink_Sender();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getReceiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiver</em>'.
	 * @see org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getReceiver()
	 * @see #getCommunicationLink()
	 * @generated
	 */
	EReference getCommunicationLink_Receiver();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScenariorunconfigurationFactory getScenariorunconfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl <em>Scenario Run Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl
		 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl#getScenarioRunConfiguration()
		 * @generated
		 */
		EClass SCENARIO_RUN_CONFIGURATION = eINSTANCE.getScenarioRunConfiguration();

		/**
		 * The meta object literal for the '<em><b>Uml2 Ecore Mapping</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING = eINSTANCE.getScenarioRunConfiguration_Uml2EcoreMapping();

		/**
		 * The meta object literal for the '<em><b>Simulation Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS = eINSTANCE.getScenarioRunConfiguration_SimulationRootObjects();

		/**
		 * The meta object literal for the '<em><b>Static Model Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS = eINSTANCE.getScenarioRunConfiguration_StaticModelRootObjects();

		/**
		 * The meta object literal for the '<em><b>Role To EObject Mapping Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER = eINSTANCE.getScenarioRunConfiguration_RoleToEObjectMappingContainer();

		/**
		 * The meta object literal for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = eINSTANCE.getScenarioRunConfiguration_UseCaseSpecificationsToBeConsidered();

		/**
		 * The meta object literal for the '<em><b>Msd Runtime State Graph</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getScenarioRunConfiguration_MsdRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Execution Semantics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS = eINSTANCE.getScenarioRunConfiguration_ExecutionSemantics();

		/**
		 * The meta object literal for the '<em><b>Store Additional MSD Modal Message Event Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA = eINSTANCE.getScenarioRunConfiguration_StoreAdditionalMSDModalMessageEventData();

		/**
		 * The meta object literal for the '<em><b>Dynamic Object System</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM = eINSTANCE.getScenarioRunConfiguration_DynamicObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Only Communicate Via Links</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS = eINSTANCE.getScenarioRunConfiguration_OnlyCommunicateViaLinks();

		/**
		 * The meta object literal for the '<em><b>Communication Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS = eINSTANCE.getScenarioRunConfiguration_CommunicationLinks();

		/**
		 * The meta object literal for the '<em><b>Only Sync Via Communication Links</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS = eINSTANCE.getScenarioRunConfiguration_OnlySyncViaCommunicationLinks();

		/**
		 * The meta object literal for the '<em><b>Strategy Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE = eINSTANCE.getScenarioRunConfiguration_StrategyPackage();

		/**
		 * The meta object literal for the '<em><b>Handle Real Time Inconsistencies Conservatively</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY = eINSTANCE.getScenarioRunConfiguration_HandleRealTimeInconsistenciesConservatively();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl <em>Communication Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl
		 * @see org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl#getCommunicationLink()
		 * @generated
		 */
		EClass COMMUNICATION_LINK = eINSTANCE.getCommunicationLink();

		/**
		 * The meta object literal for the '<em><b>Forbidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_LINK__FORBIDDEN = eINSTANCE.getCommunicationLink_Forbidden();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_LINK__OPTIONAL = eINSTANCE.getCommunicationLink_Optional();

		/**
		 * The meta object literal for the '<em><b>Sender</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_LINK__SENDER = eINSTANCE.getCommunicationLink_Sender();

		/**
		 * The meta object literal for the '<em><b>Receiver</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_LINK__RECEIVER = eINSTANCE.getCommunicationLink_Receiver();

	}

} //ScenariorunconfigurationPackage
