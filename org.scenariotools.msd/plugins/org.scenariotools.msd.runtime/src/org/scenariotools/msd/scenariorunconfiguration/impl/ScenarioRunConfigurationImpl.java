/**
 */
package org.scenariotools.msd.scenariorunconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.StateMachine;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scenario Run Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getUml2EcoreMapping <em>Uml2 Ecore Mapping</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getSimulationRootObjects <em>Simulation Root Objects</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getStaticModelRootObjects <em>Static Model Root Objects</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getRoleToEObjectMappingContainer <em>Role To EObject Mapping Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getExecutionSemantics <em>Execution Semantics</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#isStoreAdditionalMSDModalMessageEventData <em>Store Additional MSD Modal Message Event Data</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#isDynamicObjectSystem <em>Dynamic Object System</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#isOnlyCommunicateViaLinks <em>Only Communicate Via Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getCommunicationLinks <em>Communication Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#isOnlySyncViaCommunicationLinks <em>Only Sync Via Communication Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#getStrategyPackage <em>Strategy Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.ScenarioRunConfigurationImpl#isHandleRealTimeInconsistenciesConservatively <em>Handle Real Time Inconsistencies Conservatively</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScenarioRunConfigurationImpl extends RuntimeEObjectImpl implements ScenarioRunConfiguration {
	/**
	 * The cached value of the '{@link #getUml2EcoreMapping() <em>Uml2 Ecore Mapping</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUml2EcoreMapping()
	 * @generated
	 * @ordered
	 */
	protected UmlPackage2EPackage uml2EcoreMapping;

	/**
	 * The cached value of the '{@link #getSimulationRootObjects() <em>Simulation Root Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> simulationRootObjects;

	/**
	 * The cached value of the '{@link #getStaticModelRootObjects() <em>Static Model Root Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticModelRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> staticModelRootObjects;

	/**
	 * The cached value of the '{@link #getRoleToEObjectMappingContainer() <em>Role To EObject Mapping Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleToEObjectMappingContainer()
	 * @generated
	 * @ordered
	 */
	protected RoleToEObjectMappingContainer roleToEObjectMappingContainer;

	/**
	 * The cached value of the '{@link #getUseCaseSpecificationsToBeConsidered() <em>Use Case Specifications To Be Considered</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered;

	/**
	 * The cached value of the '{@link #getMsdRuntimeStateGraph() <em>Msd Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 * @ordered
	 */
	protected MSDRuntimeStateGraph msdRuntimeStateGraph;

	/**
	 * The default value of the '{@link #getExecutionSemantics() <em>Execution Semantics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionSemantics()
	 * @generated
	 * @ordered
	 */
	protected static final ExecutionSemantics EXECUTION_SEMANTICS_EDEFAULT = ExecutionSemantics.PLAY_OUT;

	/**
	 * The cached value of the '{@link #getExecutionSemantics() <em>Execution Semantics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionSemantics()
	 * @generated
	 * @ordered
	 */
	protected ExecutionSemantics executionSemantics = EXECUTION_SEMANTICS_EDEFAULT;

	/**
	 * The default value of the '{@link #isStoreAdditionalMSDModalMessageEventData() <em>Store Additional MSD Modal Message Event Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStoreAdditionalMSDModalMessageEventData()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStoreAdditionalMSDModalMessageEventData() <em>Store Additional MSD Modal Message Event Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStoreAdditionalMSDModalMessageEventData()
	 * @generated
	 * @ordered
	 */
	protected boolean storeAdditionalMSDModalMessageEventData = STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #isDynamicObjectSystem() <em>Dynamic Object System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDynamicObjectSystem()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DYNAMIC_OBJECT_SYSTEM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDynamicObjectSystem() <em>Dynamic Object System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDynamicObjectSystem()
	 * @generated
	 * @ordered
	 */
	protected boolean dynamicObjectSystem = DYNAMIC_OBJECT_SYSTEM_EDEFAULT;

	/**
	 * The default value of the '{@link #isOnlyCommunicateViaLinks() <em>Only Communicate Via Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyCommunicateViaLinks()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ONLY_COMMUNICATE_VIA_LINKS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnlyCommunicateViaLinks() <em>Only Communicate Via Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyCommunicateViaLinks()
	 * @generated
	 * @ordered
	 */
	protected boolean onlyCommunicateViaLinks = ONLY_COMMUNICATE_VIA_LINKS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCommunicationLinks() <em>Communication Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<CommunicationLink> communicationLinks;

	/**
	 * The default value of the '{@link #isOnlySyncViaCommunicationLinks() <em>Only Sync Via Communication Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlySyncViaCommunicationLinks()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ONLY_SYNC_VIA_COMMUNICATION_LINKS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnlySyncViaCommunicationLinks() <em>Only Sync Via Communication Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlySyncViaCommunicationLinks()
	 * @generated
	 * @ordered
	 */
	protected boolean onlySyncViaCommunicationLinks = ONLY_SYNC_VIA_COMMUNICATION_LINKS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStrategyPackage() <em>Strategy Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategyPackage()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package strategyPackage;

	/**
	 * The default value of the '{@link #isHandleRealTimeInconsistenciesConservatively() <em>Handle Real Time Inconsistencies Conservatively</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHandleRealTimeInconsistenciesConservatively()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHandleRealTimeInconsistenciesConservatively() <em>Handle Real Time Inconsistencies Conservatively</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHandleRealTimeInconsistenciesConservatively()
	 * @generated
	 * @ordered
	 */
	protected boolean handleRealTimeInconsistenciesConservatively = HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenarioRunConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenariorunconfigurationPackage.Literals.SCENARIO_RUN_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage getUml2EcoreMapping() {
		if (uml2EcoreMapping != null && uml2EcoreMapping.eIsProxy()) {
			InternalEObject oldUml2EcoreMapping = (InternalEObject)uml2EcoreMapping;
			uml2EcoreMapping = (UmlPackage2EPackage)eResolveProxy(oldUml2EcoreMapping);
			if (uml2EcoreMapping != oldUml2EcoreMapping) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING, oldUml2EcoreMapping, uml2EcoreMapping));
			}
		}
		return uml2EcoreMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage basicGetUml2EcoreMapping() {
		return uml2EcoreMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUml2EcoreMapping(UmlPackage2EPackage newUml2EcoreMapping) {
		UmlPackage2EPackage oldUml2EcoreMapping = uml2EcoreMapping;
		uml2EcoreMapping = newUml2EcoreMapping;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING, oldUml2EcoreMapping, uml2EcoreMapping));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getSimulationRootObjects() {
		if (simulationRootObjects == null) {
			simulationRootObjects = new EObjectResolvingEList<EObject>(EObject.class, this, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS);
		}
		return simulationRootObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getStaticModelRootObjects() {
		if (staticModelRootObjects == null) {
			staticModelRootObjects = new EObjectResolvingEList<EObject>(EObject.class, this, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS);
		}
		return staticModelRootObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleToEObjectMappingContainer getRoleToEObjectMappingContainer() {
		if (roleToEObjectMappingContainer != null && roleToEObjectMappingContainer.eIsProxy()) {
			InternalEObject oldRoleToEObjectMappingContainer = (InternalEObject)roleToEObjectMappingContainer;
			roleToEObjectMappingContainer = (RoleToEObjectMappingContainer)eResolveProxy(oldRoleToEObjectMappingContainer);
			if (roleToEObjectMappingContainer != oldRoleToEObjectMappingContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER, oldRoleToEObjectMappingContainer, roleToEObjectMappingContainer));
			}
		}
		return roleToEObjectMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleToEObjectMappingContainer basicGetRoleToEObjectMappingContainer() {
		return roleToEObjectMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleToEObjectMappingContainer(RoleToEObjectMappingContainer newRoleToEObjectMappingContainer) {
		RoleToEObjectMappingContainer oldRoleToEObjectMappingContainer = roleToEObjectMappingContainer;
		roleToEObjectMappingContainer = newRoleToEObjectMappingContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER, oldRoleToEObjectMappingContainer, roleToEObjectMappingContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getUseCaseSpecificationsToBeConsidered() {
		if (useCaseSpecificationsToBeConsidered == null) {
			useCaseSpecificationsToBeConsidered = new EObjectResolvingEList<org.eclipse.uml2.uml.Package>(org.eclipse.uml2.uml.Package.class, this, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED);
		}
		return useCaseSpecificationsToBeConsidered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(String name) {
		return getUseCaseSpecificationsToBeConsidered(name, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(String name, boolean ignoreCase, EClass eClass) {
		useCaseSpecificationsToBeConsideredLoop: for (org.eclipse.uml2.uml.Package useCaseSpecificationsToBeConsidered : getUseCaseSpecificationsToBeConsidered()) {
			if (eClass != null && !eClass.isInstance(useCaseSpecificationsToBeConsidered))
				continue useCaseSpecificationsToBeConsideredLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(useCaseSpecificationsToBeConsidered.getName()) : name.equals(useCaseSpecificationsToBeConsidered.getName())))
				continue useCaseSpecificationsToBeConsideredLoop;
			return useCaseSpecificationsToBeConsidered;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph getMsdRuntimeStateGraph() {
		if (msdRuntimeStateGraph != null && msdRuntimeStateGraph.eIsProxy()) {
			InternalEObject oldMsdRuntimeStateGraph = (InternalEObject)msdRuntimeStateGraph;
			msdRuntimeStateGraph = (MSDRuntimeStateGraph)eResolveProxy(oldMsdRuntimeStateGraph);
			if (msdRuntimeStateGraph != oldMsdRuntimeStateGraph) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH, oldMsdRuntimeStateGraph, msdRuntimeStateGraph));
			}
		}
		return msdRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph basicGetMsdRuntimeStateGraph() {
		return msdRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsdRuntimeStateGraph(MSDRuntimeStateGraph newMsdRuntimeStateGraph) {
		MSDRuntimeStateGraph oldMsdRuntimeStateGraph = msdRuntimeStateGraph;
		msdRuntimeStateGraph = newMsdRuntimeStateGraph;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH, oldMsdRuntimeStateGraph, msdRuntimeStateGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionSemantics getExecutionSemantics() {
		return executionSemantics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionSemantics(ExecutionSemantics newExecutionSemantics) {
		ExecutionSemantics oldExecutionSemantics = executionSemantics;
		executionSemantics = newExecutionSemantics == null ? EXECUTION_SEMANTICS_EDEFAULT : newExecutionSemantics;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS, oldExecutionSemantics, executionSemantics));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStoreAdditionalMSDModalMessageEventData() {
		return storeAdditionalMSDModalMessageEventData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStoreAdditionalMSDModalMessageEventData(boolean newStoreAdditionalMSDModalMessageEventData) {
		boolean oldStoreAdditionalMSDModalMessageEventData = storeAdditionalMSDModalMessageEventData;
		storeAdditionalMSDModalMessageEventData = newStoreAdditionalMSDModalMessageEventData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA, oldStoreAdditionalMSDModalMessageEventData, storeAdditionalMSDModalMessageEventData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDynamicObjectSystem() {
		return dynamicObjectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicObjectSystem(boolean newDynamicObjectSystem) {
		boolean oldDynamicObjectSystem = dynamicObjectSystem;
		dynamicObjectSystem = newDynamicObjectSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM, oldDynamicObjectSystem, dynamicObjectSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnlyCommunicateViaLinks() {
		return onlyCommunicateViaLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnlyCommunicateViaLinks(boolean newOnlyCommunicateViaLinks) {
		boolean oldOnlyCommunicateViaLinks = onlyCommunicateViaLinks;
		onlyCommunicateViaLinks = newOnlyCommunicateViaLinks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS, oldOnlyCommunicateViaLinks, onlyCommunicateViaLinks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommunicationLink> getCommunicationLinks() {
		if (communicationLinks == null) {
			communicationLinks = new EObjectContainmentEList<CommunicationLink>(CommunicationLink.class, this, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS);
		}
		return communicationLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnlySyncViaCommunicationLinks() {
		return onlySyncViaCommunicationLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnlySyncViaCommunicationLinks(boolean newOnlySyncViaCommunicationLinks) {
		boolean oldOnlySyncViaCommunicationLinks = onlySyncViaCommunicationLinks;
		onlySyncViaCommunicationLinks = newOnlySyncViaCommunicationLinks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS, oldOnlySyncViaCommunicationLinks, onlySyncViaCommunicationLinks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getStrategyPackage() {
		if (strategyPackage != null && strategyPackage.eIsProxy()) {
			InternalEObject oldStrategyPackage = (InternalEObject)strategyPackage;
			strategyPackage = (org.eclipse.uml2.uml.Package)eResolveProxy(oldStrategyPackage);
			if (strategyPackage != oldStrategyPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE, oldStrategyPackage, strategyPackage));
			}
		}
		return strategyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetStrategyPackage() {
		return strategyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategyPackage(org.eclipse.uml2.uml.Package newStrategyPackage) {
		org.eclipse.uml2.uml.Package oldStrategyPackage = strategyPackage;
		strategyPackage = newStrategyPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE, oldStrategyPackage, strategyPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHandleRealTimeInconsistenciesConservatively() {
		return handleRealTimeInconsistenciesConservatively;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandleRealTimeInconsistenciesConservatively(boolean newHandleRealTimeInconsistenciesConservatively) {
		boolean oldHandleRealTimeInconsistenciesConservatively = handleRealTimeInconsistenciesConservatively;
		handleRealTimeInconsistenciesConservatively = newHandleRealTimeInconsistenciesConservatively;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY, oldHandleRealTimeInconsistenciesConservatively, handleRealTimeInconsistenciesConservatively));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				return ((InternalEList<?>)getCommunicationLinks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING:
				if (resolve) return getUml2EcoreMapping();
				return basicGetUml2EcoreMapping();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS:
				return getSimulationRootObjects();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS:
				return getStaticModelRootObjects();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER:
				if (resolve) return getRoleToEObjectMappingContainer();
				return basicGetRoleToEObjectMappingContainer();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				return getUseCaseSpecificationsToBeConsidered();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH:
				if (resolve) return getMsdRuntimeStateGraph();
				return basicGetMsdRuntimeStateGraph();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS:
				return getExecutionSemantics();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA:
				return isStoreAdditionalMSDModalMessageEventData();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM:
				return isDynamicObjectSystem();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS:
				return isOnlyCommunicateViaLinks();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				return getCommunicationLinks();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS:
				return isOnlySyncViaCommunicationLinks();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE:
				if (resolve) return getStrategyPackage();
				return basicGetStrategyPackage();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY:
				return isHandleRealTimeInconsistenciesConservatively();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING:
				setUml2EcoreMapping((UmlPackage2EPackage)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS:
				getSimulationRootObjects().clear();
				getSimulationRootObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS:
				getStaticModelRootObjects().clear();
				getStaticModelRootObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER:
				setRoleToEObjectMappingContainer((RoleToEObjectMappingContainer)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				getUseCaseSpecificationsToBeConsidered().clear();
				getUseCaseSpecificationsToBeConsidered().addAll((Collection<? extends org.eclipse.uml2.uml.Package>)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS:
				setExecutionSemantics((ExecutionSemantics)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA:
				setStoreAdditionalMSDModalMessageEventData((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM:
				setDynamicObjectSystem((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS:
				setOnlyCommunicateViaLinks((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				getCommunicationLinks().clear();
				getCommunicationLinks().addAll((Collection<? extends CommunicationLink>)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS:
				setOnlySyncViaCommunicationLinks((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE:
				setStrategyPackage((org.eclipse.uml2.uml.Package)newValue);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY:
				setHandleRealTimeInconsistenciesConservatively((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING:
				setUml2EcoreMapping((UmlPackage2EPackage)null);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS:
				getSimulationRootObjects().clear();
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS:
				getStaticModelRootObjects().clear();
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER:
				setRoleToEObjectMappingContainer((RoleToEObjectMappingContainer)null);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				getUseCaseSpecificationsToBeConsidered().clear();
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)null);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS:
				setExecutionSemantics(EXECUTION_SEMANTICS_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA:
				setStoreAdditionalMSDModalMessageEventData(STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM:
				setDynamicObjectSystem(DYNAMIC_OBJECT_SYSTEM_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS:
				setOnlyCommunicateViaLinks(ONLY_COMMUNICATE_VIA_LINKS_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				getCommunicationLinks().clear();
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS:
				setOnlySyncViaCommunicationLinks(ONLY_SYNC_VIA_COMMUNICATION_LINKS_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE:
				setStrategyPackage((org.eclipse.uml2.uml.Package)null);
				return;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY:
				setHandleRealTimeInconsistenciesConservatively(HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING:
				return uml2EcoreMapping != null;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS:
				return simulationRootObjects != null && !simulationRootObjects.isEmpty();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS:
				return staticModelRootObjects != null && !staticModelRootObjects.isEmpty();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER:
				return roleToEObjectMappingContainer != null;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				return useCaseSpecificationsToBeConsidered != null && !useCaseSpecificationsToBeConsidered.isEmpty();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH:
				return msdRuntimeStateGraph != null;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS:
				return executionSemantics != EXECUTION_SEMANTICS_EDEFAULT;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA:
				return storeAdditionalMSDModalMessageEventData != STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA_EDEFAULT;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM:
				return dynamicObjectSystem != DYNAMIC_OBJECT_SYSTEM_EDEFAULT;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS:
				return onlyCommunicateViaLinks != ONLY_COMMUNICATE_VIA_LINKS_EDEFAULT;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS:
				return communicationLinks != null && !communicationLinks.isEmpty();
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS:
				return onlySyncViaCommunicationLinks != ONLY_SYNC_VIA_COMMUNICATION_LINKS_EDEFAULT;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE:
				return strategyPackage != null;
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY:
				return handleRealTimeInconsistenciesConservatively != HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executionSemantics: ");
		result.append(executionSemantics);
		result.append(", storeAdditionalMSDModalMessageEventData: ");
		result.append(storeAdditionalMSDModalMessageEventData);
		result.append(", dynamicObjectSystem: ");
		result.append(dynamicObjectSystem);
		result.append(", onlyCommunicateViaLinks: ");
		result.append(onlyCommunicateViaLinks);
		result.append(", onlySyncViaCommunicationLinks: ");
		result.append(onlySyncViaCommunicationLinks);
		result.append(", handleRealTimeInconsistenciesConservatively: ");
		result.append(handleRealTimeInconsistenciesConservatively);
		result.append(')');
		return result.toString();
	}

} //ScenarioRunConfigurationImpl
