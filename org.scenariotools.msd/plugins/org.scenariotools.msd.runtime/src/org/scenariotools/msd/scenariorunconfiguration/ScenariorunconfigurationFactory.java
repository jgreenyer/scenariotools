/**
 */
package org.scenariotools.msd.scenariorunconfiguration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage
 * @generated
 */
public interface ScenariorunconfigurationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScenariorunconfigurationFactory eINSTANCE = org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Scenario Run Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scenario Run Configuration</em>'.
	 * @generated
	 */
	ScenarioRunConfiguration createScenarioRunConfiguration();

	/**
	 * Returns a new object of class '<em>Communication Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Link</em>'.
	 * @generated
	 */
	CommunicationLink createCommunicationLink();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ScenariorunconfigurationPackage getScenariorunconfigurationPackage();

} //ScenariorunconfigurationFactory
