/**
 */
package org.scenariotools.msd.scenariorunconfiguration.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.scenariotools.common.RuntimeEObjectImpl;

import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl#isForbidden <em>Forbidden</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.impl.CommunicationLinkImpl#getReceiver <em>Receiver</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationLinkImpl extends RuntimeEObjectImpl implements CommunicationLink {
	/**
	 * The default value of the '{@link #isForbidden() <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForbidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FORBIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isForbidden() <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForbidden()
	 * @generated
	 * @ordered
	 */
	protected boolean forbidden = FORBIDDEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSender() <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSender()
	 * @generated
	 * @ordered
	 */
	protected EObject sender;

	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected EObject receiver;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenariorunconfigurationPackage.Literals.COMMUNICATION_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isForbidden() {
		return forbidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForbidden(boolean newForbidden) {
		boolean oldForbidden = forbidden;
		forbidden = newForbidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN, oldForbidden, forbidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL, oldOptional, optional));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getSender() {
		if (sender != null && sender.eIsProxy()) {
			InternalEObject oldSender = (InternalEObject)sender;
			sender = eResolveProxy(oldSender);
			if (sender != oldSender) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER, oldSender, sender));
			}
		}
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetSender() {
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSender(EObject newSender) {
		EObject oldSender = sender;
		sender = newSender;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER, oldSender, sender));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getReceiver() {
		if (receiver != null && receiver.eIsProxy()) {
			InternalEObject oldReceiver = (InternalEObject)receiver;
			receiver = eResolveProxy(oldReceiver);
			if (receiver != oldReceiver) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER, oldReceiver, receiver));
			}
		}
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetReceiver() {
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceiver(EObject newReceiver) {
		EObject oldReceiver = receiver;
		receiver = newReceiver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER, oldReceiver, receiver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN:
				return isForbidden();
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL:
				return isOptional();
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER:
				if (resolve) return getSender();
				return basicGetSender();
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER:
				if (resolve) return getReceiver();
				return basicGetReceiver();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN:
				setForbidden((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL:
				setOptional((Boolean)newValue);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER:
				setSender((EObject)newValue);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER:
				setReceiver((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN:
				setForbidden(FORBIDDEN_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL:
				setOptional(OPTIONAL_EDEFAULT);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER:
				setSender((EObject)null);
				return;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER:
				setReceiver((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__FORBIDDEN:
				return forbidden != FORBIDDEN_EDEFAULT;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__OPTIONAL:
				return optional != OPTIONAL_EDEFAULT;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__SENDER:
				return sender != null;
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK__RECEIVER:
				return receiver != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (forbidden: ");
		result.append(forbidden);
		result.append(", optional: ");
		result.append(optional);
		result.append(')');
		return result.toString();
	}

} //CommunicationLinkImpl
