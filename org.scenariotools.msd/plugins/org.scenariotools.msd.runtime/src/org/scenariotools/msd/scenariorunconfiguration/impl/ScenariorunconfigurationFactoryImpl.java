/**
 */
package org.scenariotools.msd.scenariorunconfiguration.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.msd.scenariorunconfiguration.*;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenariorunconfigurationFactoryImpl extends EFactoryImpl implements ScenariorunconfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScenariorunconfigurationFactory init() {
		try {
			ScenariorunconfigurationFactory theScenariorunconfigurationFactory = (ScenariorunconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory(ScenariorunconfigurationPackage.eNS_URI);
			if (theScenariorunconfigurationFactory != null) {
				return theScenariorunconfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScenariorunconfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenariorunconfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScenariorunconfigurationPackage.SCENARIO_RUN_CONFIGURATION: return createScenarioRunConfiguration();
			case ScenariorunconfigurationPackage.COMMUNICATION_LINK: return createCommunicationLink();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfiguration createScenarioRunConfiguration() {
		ScenarioRunConfigurationImpl scenarioRunConfiguration = new ScenarioRunConfigurationImpl();
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationLink createCommunicationLink() {
		CommunicationLinkImpl communicationLink = new CommunicationLinkImpl();
		return communicationLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenariorunconfigurationPackage getScenariorunconfigurationPackage() {
		return (ScenariorunconfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScenariorunconfigurationPackage getPackage() {
		return ScenariorunconfigurationPackage.eINSTANCE;
	}

} //ScenariorunconfigurationFactoryImpl
