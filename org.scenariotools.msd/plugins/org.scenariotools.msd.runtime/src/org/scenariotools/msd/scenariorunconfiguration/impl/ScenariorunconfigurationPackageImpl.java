/**
 */
package org.scenariotools.msd.scenariorunconfiguration.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;
import org.scenariotools.msd.runtime.impl.RuntimePackageImpl;
import org.scenariotools.msd.scenariorunconfiguration.CommunicationLink;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.util.UtilPackage;
import org.scenariotools.msd.util.impl.UtilPackageImpl;
import org.scenariotools.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenariorunconfigurationPackageImpl extends EPackageImpl implements ScenariorunconfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scenarioRunConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationLinkEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScenariorunconfigurationPackageImpl() {
		super(eNS_URI, ScenariorunconfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ScenariorunconfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScenariorunconfigurationPackage init() {
		if (isInited) return (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);

		// Obtain or create and register package
		ScenariorunconfigurationPackageImpl theScenariorunconfigurationPackage = (ScenariorunconfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ScenariorunconfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ScenariorunconfigurationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Roles2eobjectsPackage.eINSTANCE.eClass();
		RuntimePackage.eINSTANCE.eClass();
		Uml2ecorePackage.eINSTANCE.eClass();
		PrimitivemappingsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		RuntimePackageImpl theRuntimePackage_1 = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI) : org.scenariotools.msd.runtime.RuntimePackage.eINSTANCE);
		UtilPackageImpl theUtilPackage = (UtilPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI) instanceof UtilPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI) : UtilPackage.eINSTANCE);

		// Create package meta-data objects
		theScenariorunconfigurationPackage.createPackageContents();
		theRuntimePackage_1.createPackageContents();
		theUtilPackage.createPackageContents();

		// Initialize created meta-data
		theScenariorunconfigurationPackage.initializePackageContents();
		theRuntimePackage_1.initializePackageContents();
		theUtilPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theScenariorunconfigurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScenariorunconfigurationPackage.eNS_URI, theScenariorunconfigurationPackage);
		return theScenariorunconfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScenarioRunConfiguration() {
		return scenarioRunConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_Uml2EcoreMapping() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_SimulationRootObjects() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_StaticModelRootObjects() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_RoleToEObjectMappingContainer() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_UseCaseSpecificationsToBeConsidered() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_MsdRuntimeStateGraph() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_ExecutionSemantics() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_StoreAdditionalMSDModalMessageEventData() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_DynamicObjectSystem() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_OnlyCommunicateViaLinks() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_CommunicationLinks() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_OnlySyncViaCommunicationLinks() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenarioRunConfiguration_StrategyPackage() {
		return (EReference)scenarioRunConfigurationEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenarioRunConfiguration_HandleRealTimeInconsistenciesConservatively() {
		return (EAttribute)scenarioRunConfigurationEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationLink() {
		return communicationLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationLink_Forbidden() {
		return (EAttribute)communicationLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationLink_Optional() {
		return (EAttribute)communicationLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationLink_Sender() {
		return (EReference)communicationLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationLink_Receiver() {
		return (EReference)communicationLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenariorunconfigurationFactory getScenariorunconfigurationFactory() {
		return (ScenariorunconfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scenarioRunConfigurationEClass = createEClass(SCENARIO_RUN_CONFIGURATION);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__UML2_ECORE_MAPPING);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__SIMULATION_ROOT_OBJECTS);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__STATIC_MODEL_ROOT_OBJECTS);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__ROLE_TO_EOBJECT_MAPPING_CONTAINER);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__MSD_RUNTIME_STATE_GRAPH);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__EXECUTION_SEMANTICS);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__STORE_ADDITIONAL_MSD_MODAL_MESSAGE_EVENT_DATA);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__DYNAMIC_OBJECT_SYSTEM);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__ONLY_COMMUNICATE_VIA_LINKS);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__COMMUNICATION_LINKS);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__ONLY_SYNC_VIA_COMMUNICATION_LINKS);
		createEReference(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__STRATEGY_PACKAGE);
		createEAttribute(scenarioRunConfigurationEClass, SCENARIO_RUN_CONFIGURATION__HANDLE_REAL_TIME_INCONSISTENCIES_CONSERVATIVELY);

		communicationLinkEClass = createEClass(COMMUNICATION_LINK);
		createEAttribute(communicationLinkEClass, COMMUNICATION_LINK__FORBIDDEN);
		createEAttribute(communicationLinkEClass, COMMUNICATION_LINK__OPTIONAL);
		createEReference(communicationLinkEClass, COMMUNICATION_LINK__SENDER);
		createEReference(communicationLinkEClass, COMMUNICATION_LINK__RECEIVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Uml2ecorePackage theUml2ecorePackage = (Uml2ecorePackage)EPackage.Registry.INSTANCE.getEPackage(Uml2ecorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		Roles2eobjectsPackage theRoles2eobjectsPackage = (Roles2eobjectsPackage)EPackage.Registry.INSTANCE.getEPackage(Roles2eobjectsPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		org.scenariotools.msd.runtime.RuntimePackage theRuntimePackage_1 = (org.scenariotools.msd.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(scenarioRunConfigurationEClass, ScenarioRunConfiguration.class, "ScenarioRunConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScenarioRunConfiguration_Uml2EcoreMapping(), theUml2ecorePackage.getUmlPackage2EPackage(), null, "uml2EcoreMapping", null, 1, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_SimulationRootObjects(), theEcorePackage.getEObject(), null, "simulationRootObjects", null, 0, -1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_StaticModelRootObjects(), theEcorePackage.getEObject(), null, "staticModelRootObjects", null, 0, -1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_RoleToEObjectMappingContainer(), theRoles2eobjectsPackage.getRoleToEObjectMappingContainer(), null, "roleToEObjectMappingContainer", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_UseCaseSpecificationsToBeConsidered(), theUMLPackage.getPackage(), null, "useCaseSpecificationsToBeConsidered", null, 0, -1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_MsdRuntimeStateGraph(), theRuntimePackage_1.getMSDRuntimeStateGraph(), null, "msdRuntimeStateGraph", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_ExecutionSemantics(), theRuntimePackage_1.getExecutionSemantics(), "executionSemantics", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_StoreAdditionalMSDModalMessageEventData(), ecorePackage.getEBoolean(), "storeAdditionalMSDModalMessageEventData", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_DynamicObjectSystem(), ecorePackage.getEBoolean(), "dynamicObjectSystem", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_OnlyCommunicateViaLinks(), theEcorePackage.getEBoolean(), "onlyCommunicateViaLinks", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_CommunicationLinks(), this.getCommunicationLink(), null, "communicationLinks", null, 0, -1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_OnlySyncViaCommunicationLinks(), theEcorePackage.getEBoolean(), "onlySyncViaCommunicationLinks", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenarioRunConfiguration_StrategyPackage(), theUMLPackage.getPackage(), null, "strategyPackage", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenarioRunConfiguration_HandleRealTimeInconsistenciesConservatively(), theEcorePackage.getEBoolean(), "handleRealTimeInconsistenciesConservatively", null, 0, 1, ScenarioRunConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationLinkEClass, CommunicationLink.class, "CommunicationLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCommunicationLink_Forbidden(), theEcorePackage.getEBoolean(), "forbidden", null, 0, 1, CommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationLink_Optional(), theEcorePackage.getEBoolean(), "optional", "true", 0, 1, CommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationLink_Sender(), theEcorePackage.getEObject(), null, "sender", null, 0, 1, CommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationLink_Receiver(), theEcorePackage.getEObject(), null, "receiver", null, 0, 1, CommunicationLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ScenariorunconfigurationPackageImpl
