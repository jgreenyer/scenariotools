/**
 */
package org.scenariotools.msd.scenariorunconfiguration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.StateMachine;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenario Run Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUml2EcoreMapping <em>Uml2 Ecore Mapping</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getSimulationRootObjects <em>Simulation Root Objects</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStaticModelRootObjects <em>Static Model Root Objects</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getRoleToEObjectMappingContainer <em>Role To EObject Mapping Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getExecutionSemantics <em>Execution Semantics</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isStoreAdditionalMSDModalMessageEventData <em>Store Additional MSD Modal Message Event Data</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isDynamicObjectSystem <em>Dynamic Object System</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlyCommunicateViaLinks <em>Only Communicate Via Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getCommunicationLinks <em>Communication Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlySyncViaCommunicationLinks <em>Only Sync Via Communication Links</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStrategyPackage <em>Strategy Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isHandleRealTimeInconsistenciesConservatively <em>Handle Real Time Inconsistencies Conservatively</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration()
 * @model
 * @generated
 */
public interface ScenarioRunConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Uml2 Ecore Mapping</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml2 Ecore Mapping</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml2 Ecore Mapping</em>' reference.
	 * @see #setUml2EcoreMapping(UmlPackage2EPackage)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_Uml2EcoreMapping()
	 * @model required="true"
	 * @generated
	 */
	UmlPackage2EPackage getUml2EcoreMapping();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getUml2EcoreMapping <em>Uml2 Ecore Mapping</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml2 Ecore Mapping</em>' reference.
	 * @see #getUml2EcoreMapping()
	 * @generated
	 */
	void setUml2EcoreMapping(UmlPackage2EPackage value);

	/**
	 * Returns the value of the '<em><b>Simulation Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Root Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Root Objects</em>' reference list.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_SimulationRootObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getSimulationRootObjects();

	/**
	 * Returns the value of the '<em><b>Static Model Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Model Root Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Model Root Objects</em>' reference list.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_StaticModelRootObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getStaticModelRootObjects();

	/**
	 * Returns the value of the '<em><b>Role To EObject Mapping Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role To EObject Mapping Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role To EObject Mapping Container</em>' reference.
	 * @see #setRoleToEObjectMappingContainer(RoleToEObjectMappingContainer)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_RoleToEObjectMappingContainer()
	 * @model
	 * @generated
	 */
	RoleToEObjectMappingContainer getRoleToEObjectMappingContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getRoleToEObjectMappingContainer <em>Role To EObject Mapping Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role To EObject Mapping Container</em>' reference.
	 * @see #getRoleToEObjectMappingContainer()
	 * @generated
	 */
	void setRoleToEObjectMappingContainer(RoleToEObjectMappingContainer value);

	/**
	 * Returns the value of the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Package}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If the referenced set is not empty, this specifies the use case specifications (->MSDs), which will be considered in the run.
	 * If the set is empty, all use case specifications mapped in the Uml2Ecore mapping will be considered in the run.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Use Case Specifications To Be Considered</em>' reference list.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_UseCaseSpecificationsToBeConsidered()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Package> getUseCaseSpecificationsToBeConsidered();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(String name, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Runtime State Graph</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #setMsdRuntimeStateGraph(MSDRuntimeStateGraph)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_MsdRuntimeStateGraph()
	 * @model
	 * @generated
	 */
	MSDRuntimeStateGraph getMsdRuntimeStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 */
	void setMsdRuntimeStateGraph(MSDRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Execution Semantics</b></em>' attribute.
	 * The literals are from the enumeration {@link org.scenariotools.msd.runtime.ExecutionSemantics}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Semantics</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Semantics</em>' attribute.
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @see #setExecutionSemantics(ExecutionSemantics)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_ExecutionSemantics()
	 * @model
	 * @generated
	 */
	ExecutionSemantics getExecutionSemantics();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getExecutionSemantics <em>Execution Semantics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Semantics</em>' attribute.
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @see #getExecutionSemantics()
	 * @generated
	 */
	void setExecutionSemantics(ExecutionSemantics value);

	/**
	 * Returns the value of the '<em><b>Store Additional MSD Modal Message Event Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specified whether for MSDModalMessageEvents additional information shall be stored, i.e., in which ActiveMSDs the message event is enabled, cold-violating, or safety-violating
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Store Additional MSD Modal Message Event Data</em>' attribute.
	 * @see #setStoreAdditionalMSDModalMessageEventData(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_StoreAdditionalMSDModalMessageEventData()
	 * @model
	 * @generated
	 */
	boolean isStoreAdditionalMSDModalMessageEventData();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isStoreAdditionalMSDModalMessageEventData <em>Store Additional MSD Modal Message Event Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Store Additional MSD Modal Message Event Data</em>' attribute.
	 * @see #isStoreAdditionalMSDModalMessageEventData()
	 * @generated
	 */
	void setStoreAdditionalMSDModalMessageEventData(boolean value);

	/**
	 * Returns the value of the '<em><b>Dynamic Object System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specified whether for MSDModalMessageEvents additional information shall be stored, i.e., in which ActiveMSDs the message event is enabled, cold-violating, or safety-violating
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Dynamic Object System</em>' attribute.
	 * @see #setDynamicObjectSystem(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_DynamicObjectSystem()
	 * @model
	 * @generated
	 */
	boolean isDynamicObjectSystem();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isDynamicObjectSystem <em>Dynamic Object System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Object System</em>' attribute.
	 * @see #isDynamicObjectSystem()
	 * @generated
	 */
	void setDynamicObjectSystem(boolean value);

	/**
	 * Returns the value of the '<em><b>Only Communicate Via Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Only Communicate Via Links</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Only Communicate Via Links</em>' attribute.
	 * @see #setOnlyCommunicateViaLinks(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_OnlyCommunicateViaLinks()
	 * @model
	 * @generated
	 */
	boolean isOnlyCommunicateViaLinks();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlyCommunicateViaLinks <em>Only Communicate Via Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Only Communicate Via Links</em>' attribute.
	 * @see #isOnlyCommunicateViaLinks()
	 * @generated
	 */
	void setOnlyCommunicateViaLinks(boolean value);

	/**
	 * Returns the value of the '<em><b>Communication Links</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Links</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Links</em>' containment reference list.
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_CommunicationLinks()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationLink> getCommunicationLinks();

	/**
	 * Returns the value of the '<em><b>Only Sync Via Communication Links</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Only Sync Via Communication Links</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Only Sync Via Communication Links</em>' attribute.
	 * @see #setOnlySyncViaCommunicationLinks(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_OnlySyncViaCommunicationLinks()
	 * @model
	 * @generated
	 */
	boolean isOnlySyncViaCommunicationLinks();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isOnlySyncViaCommunicationLinks <em>Only Sync Via Communication Links</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Only Sync Via Communication Links</em>' attribute.
	 * @see #isOnlySyncViaCommunicationLinks()
	 * @generated
	 */
	void setOnlySyncViaCommunicationLinks(boolean value);

	/**
	 * Returns the value of the '<em><b>Strategy Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategy Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategy Package</em>' reference.
	 * @see #setStrategyPackage(org.eclipse.uml2.uml.Package)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_StrategyPackage()
	 * @model
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getStrategyPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#getStrategyPackage <em>Strategy Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategy Package</em>' reference.
	 * @see #getStrategyPackage()
	 * @generated
	 */
	void setStrategyPackage(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Handle Real Time Inconsistencies Conservatively</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handle Real Time Inconsistencies Conservatively</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handle Real Time Inconsistencies Conservatively</em>' attribute.
	 * @see #setHandleRealTimeInconsistenciesConservatively(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getScenarioRunConfiguration_HandleRealTimeInconsistenciesConservatively()
	 * @model
	 * @generated
	 */
	boolean isHandleRealTimeInconsistenciesConservatively();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration#isHandleRealTimeInconsistenciesConservatively <em>Handle Real Time Inconsistencies Conservatively</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handle Real Time Inconsistencies Conservatively</em>' attribute.
	 * @see #isHandleRealTimeInconsistenciesConservatively()
	 * @generated
	 */
	void setHandleRealTimeInconsistenciesConservatively(boolean value);

} // ScenarioRunConfiguration
