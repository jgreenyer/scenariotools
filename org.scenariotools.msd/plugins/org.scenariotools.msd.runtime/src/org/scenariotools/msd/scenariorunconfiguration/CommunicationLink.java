/**
 */
package org.scenariotools.msd.scenariorunconfiguration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isForbidden <em>Forbidden</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getSender <em>Sender</em>}</li>
 *   <li>{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getReceiver <em>Receiver</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getCommunicationLink()
 * @model
 * @generated
 */
public interface CommunicationLink extends EObject {
	/**
	 * Returns the value of the '<em><b>Forbidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forbidden</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forbidden</em>' attribute.
	 * @see #setForbidden(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getCommunicationLink_Forbidden()
	 * @model
	 * @generated
	 */
	boolean isForbidden();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isForbidden <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Forbidden</em>' attribute.
	 * @see #isForbidden()
	 * @generated
	 */
	void setForbidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Optional</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optional</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optional</em>' attribute.
	 * @see #setOptional(boolean)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getCommunicationLink_Optional()
	 * @model default="true"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optional</em>' attribute.
	 * @see #isOptional()
	 * @generated
	 */
	void setOptional(boolean value);

	/**
	 * Returns the value of the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sender</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sender</em>' reference.
	 * @see #setSender(EObject)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getCommunicationLink_Sender()
	 * @model
	 * @generated
	 */
	EObject getSender();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getSender <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sender</em>' reference.
	 * @see #getSender()
	 * @generated
	 */
	void setSender(EObject value);

	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiver</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' reference.
	 * @see #setReceiver(EObject)
	 * @see org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage#getCommunicationLink_Receiver()
	 * @model
	 * @generated
	 */
	EObject getReceiver();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.scenariorunconfiguration.CommunicationLink#getReceiver <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiver</em>' reference.
	 * @see #getReceiver()
	 * @generated
	 */
	void setReceiver(EObject value);

} // CommunicationLink
