/**
 */
package org.scenariotools.msd.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Active MSD Progress</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDProgress()
 * @model
 * @generated
 */
public enum ActiveMSDProgress implements Enumerator {
	/**
	 * The '<em><b>Progress</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROGRESS_VALUE
	 * @generated
	 * @ordered
	 */
	PROGRESS(0, "progress", "progress"),

	/**
	 * The '<em><b>No Progress</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_PROGRESS_VALUE
	 * @generated
	 * @ordered
	 */
	NO_PROGRESS(1, "noProgress", "noProgress"),

	/**
	 * The '<em><b>Terminal Cut Reached</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TERMINAL_CUT_REACHED_VALUE
	 * @generated
	 * @ordered
	 */
	TERMINAL_CUT_REACHED(2, "terminalCutReached", "terminalCutReached"),

	/**
	 * The '<em><b>Cold Violation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COLD_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	COLD_VIOLATION(3, "coldViolation", "coldViolation"),

	/**
	 * The '<em><b>Safety Violation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAFETY_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	SAFETY_VIOLATION(4, "safetyViolation", "safetyViolation"), /**
	 * The '<em><b>Timed Cold Violation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIMED_COLD_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	TIMED_COLD_VIOLATION(5, "timedColdViolation", "timedColdViolation"), /**
	 * The '<em><b>Timed Safety Violation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIMED_SAFETY_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	TIMED_SAFETY_VIOLATION(6, "timedSafetyViolation", "timedSafetyViolation");

	/**
	 * The '<em><b>Progress</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Progress</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROGRESS
	 * @model name="progress"
	 * @generated
	 * @ordered
	 */
	public static final int PROGRESS_VALUE = 0;

	/**
	 * The '<em><b>No Progress</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>No Progress</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_PROGRESS
	 * @model name="noProgress"
	 * @generated
	 * @ordered
	 */
	public static final int NO_PROGRESS_VALUE = 1;

	/**
	 * The '<em><b>Terminal Cut Reached</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Terminal Cut Reached</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TERMINAL_CUT_REACHED
	 * @model name="terminalCutReached"
	 * @generated
	 * @ordered
	 */
	public static final int TERMINAL_CUT_REACHED_VALUE = 2;

	/**
	 * The '<em><b>Cold Violation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Cold Violation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COLD_VIOLATION
	 * @model name="coldViolation"
	 * @generated
	 * @ordered
	 */
	public static final int COLD_VIOLATION_VALUE = 3;

	/**
	 * The '<em><b>Safety Violation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Safety Violation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAFETY_VIOLATION
	 * @model name="safetyViolation"
	 * @generated
	 * @ordered
	 */
	public static final int SAFETY_VIOLATION_VALUE = 4;

	/**
	 * The '<em><b>Timed Cold Violation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Timed Cold Violation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIMED_COLD_VIOLATION
	 * @model name="timedColdViolation"
	 * @generated
	 * @ordered
	 */
	public static final int TIMED_COLD_VIOLATION_VALUE = 5;

	/**
	 * The '<em><b>Timed Safety Violation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Timed Safety Violation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIMED_SAFETY_VIOLATION
	 * @model name="timedSafetyViolation"
	 * @generated
	 * @ordered
	 */
	public static final int TIMED_SAFETY_VIOLATION_VALUE = 6;

	/**
	 * An array of all the '<em><b>Active MSD Progress</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ActiveMSDProgress[] VALUES_ARRAY =
		new ActiveMSDProgress[] {
			PROGRESS,
			NO_PROGRESS,
			TERMINAL_CUT_REACHED,
			COLD_VIOLATION,
			SAFETY_VIOLATION,
			TIMED_COLD_VIOLATION,
			TIMED_SAFETY_VIOLATION,
		};

	/**
	 * A public read-only list of all the '<em><b>Active MSD Progress</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ActiveMSDProgress> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Active MSD Progress</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveMSDProgress get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActiveMSDProgress result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Active MSD Progress</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveMSDProgress getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActiveMSDProgress result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Active MSD Progress</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveMSDProgress get(int value) {
		switch (value) {
			case PROGRESS_VALUE: return PROGRESS;
			case NO_PROGRESS_VALUE: return NO_PROGRESS;
			case TERMINAL_CUT_REACHED_VALUE: return TERMINAL_CUT_REACHED;
			case COLD_VIOLATION_VALUE: return COLD_VIOLATION;
			case SAFETY_VIOLATION_VALUE: return SAFETY_VIOLATION;
			case TIMED_COLD_VIOLATION_VALUE: return TIMED_COLD_VIOLATION;
			case TIMED_SAFETY_VIOLATION_VALUE: return TIMED_SAFETY_VIOLATION;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ActiveMSDProgress(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ActiveMSDProgress
