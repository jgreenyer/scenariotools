/**
 */
package org.scenariotools.msd.runtime.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.*;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.Modality;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimePackage
 * @generated
 */
public class RuntimeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RuntimePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RuntimePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeSwitch<Adapter> modelSwitch =
		new RuntimeSwitch<Adapter>() {
			@Override
			public Adapter caseMSDRuntimeStateGraph(MSDRuntimeStateGraph object) {
				return createMSDRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseMSDRuntimeState(MSDRuntimeState object) {
				return createMSDRuntimeStateAdapter();
			}
			@Override
			public Adapter caseElementContainer(ElementContainer object) {
				return createElementContainerAdapter();
			}
			@Override
			public Adapter caseMSDModality(MSDModality object) {
				return createMSDModalityAdapter();
			}
			@Override
			public Adapter caseActiveProcess(ActiveProcess object) {
				return createActiveProcessAdapter();
			}
			@Override
			public Adapter caseActiveMSDCut(ActiveMSDCut object) {
				return createActiveMSDCutAdapter();
			}
			@Override
			public Adapter caseActiveMSDLifelineBindings(ActiveMSDLifelineBindings object) {
				return createActiveMSDLifelineBindingsAdapter();
			}
			@Override
			public Adapter caseActiveMSDVariableValuations(ActiveMSDVariableValuations object) {
				return createActiveMSDVariableValuationsAdapter();
			}
			@Override
			public Adapter caseLifelineToInteractionFragmentMapEntry(Map.Entry<Lifeline, InteractionFragment> object) {
				return createLifelineToInteractionFragmentMapEntryAdapter();
			}
			@Override
			public Adapter caseStringToEObjectMapEntry(Map.Entry<String, EObject> object) {
				return createStringToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseLifelineToEObjectMapEntry(Map.Entry<Lifeline, EObject> object) {
				return createLifelineToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseNamedElementToMessageEventMapEntry(Map.Entry<NamedElement, MessageEvent> object) {
				return createNamedElementToMessageEventMapEntryAdapter();
			}
			@Override
			public Adapter caseMSDModalMessageEvent(MSDModalMessageEvent object) {
				return createMSDModalMessageEventAdapter();
			}
			@Override
			public Adapter caseMSDObjectSystem(MSDObjectSystem object) {
				return createMSDObjectSystemAdapter();
			}
			@Override
			public Adapter caseStringToStringMapEntry(Map.Entry<String, String> object) {
				return createStringToStringMapEntryAdapter();
			}
			@Override
			public Adapter caseStringToIntegerMapEntry(Map.Entry<String, Integer> object) {
				return createStringToIntegerMapEntryAdapter();
			}
			@Override
			public Adapter caseStringToBooleanMapEntry(Map.Entry<String, Boolean> object) {
				return createStringToBooleanMapEntryAdapter();
			}
			@Override
			public Adapter caseMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry(Map.Entry<MSDRuntimeStateKeyWrapper, MSDRuntimeState> object) {
				return createMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveProcessKeyWrapperToActiveProcessMapEntry(Map.Entry<ActiveProcessKeyWrapper, ActiveProcess> object) {
				return createActiveProcessKeyWrapperToActiveProcessMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveStateKeyWrapperToActiveStateMapEntry(Map.Entry<ActiveStateKeyWrapper, ActiveState> object) {
				return createActiveStateKeyWrapperToActiveStateMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry(Map.Entry<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> object) {
				return createActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry(Map.Entry<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> object) {
				return createActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry(Map.Entry<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> object) {
				return createActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryAdapter();
			}
			@Override
			public Adapter caseMessageEventContainer(MessageEventContainer object) {
				return createMessageEventContainerAdapter();
			}
			@Override
			public Adapter caseMessageEventKeyWrapperToMessageEventMapEntry(Map.Entry<MessageEventKeyWrapper, MessageEvent> object) {
				return createMessageEventKeyWrapperToMessageEventMapEntryAdapter();
			}
			@Override
			public Adapter caseObjectSystemKeyWrapperToMSDObjectSystemMapEntry(Map.Entry<ObjectSystemKeyWrapper, MSDObjectSystem> object) {
				return createObjectSystemKeyWrapperToMSDObjectSystemMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveMSS(ActiveMSS object) {
				return createActiveMSSAdapter();
			}
			@Override
			public Adapter caseActiveMSD(ActiveMSD object) {
				return createActiveMSDAdapter();
			}
			@Override
			public Adapter caseActiveMSSRoleBindings(ActiveMSSRoleBindings object) {
				return createActiveMSSRoleBindingsAdapter();
			}
			@Override
			public Adapter casePropertyToEObjectMapEntry(Map.Entry<Property, EObject> object) {
				return createPropertyToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveState(ActiveState object) {
				return createActiveStateAdapter();
			}
			@Override
			public Adapter caseActiveMSSState(ActiveMSSState object) {
				return createActiveMSSStateAdapter();
			}
			@Override
			public Adapter caseEObjectToEOperationMapEntry(Map.Entry<EObject, EList<EOperation>> object) {
				return createEObjectToEOperationMapEntryAdapter();
			}
			@Override
			public Adapter caseEObjectToUMLClassMapEntry(Map.Entry<EObject, EList<org.eclipse.uml2.uml.Class>> object) {
				return createEObjectToUMLClassMapEntryAdapter();
			}
			@Override
			public Adapter caseEClassToEObjectMapEntry(Map.Entry<EClass, EList<EObject>> object) {
				return createEClassToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseStateGraph(StateGraph object) {
				return createStateGraphAdapter();
			}
			@Override
			public Adapter caseRuntimeStateGraph(RuntimeStateGraph object) {
				return createRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseAnnotatableElement(AnnotatableElement object) {
				return createAnnotatableElementAdapter();
			}
			@Override
			public Adapter caseState(State object) {
				return createStateAdapter();
			}
			@Override
			public Adapter caseRuntimeState(RuntimeState object) {
				return createRuntimeStateAdapter();
			}
			@Override
			public Adapter caseModality(Modality object) {
				return createModalityAdapter();
			}
			@Override
			public Adapter caseModalMessageEvent(ModalMessageEvent object) {
				return createModalMessageEventAdapter();
			}
			@Override
			public Adapter caseObjectSystem(ObjectSystem object) {
				return createObjectSystemAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph <em>MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph
	 * @generated
	 */
	public Adapter createMSDRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDRuntimeState <em>MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState
	 * @generated
	 */
	public Adapter createMSDRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ElementContainer
	 * @generated
	 */
	public Adapter createElementContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDModality <em>MSD Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDModality
	 * @generated
	 */
	public Adapter createMSDModalityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveProcess <em>Active Process</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveProcess
	 * @generated
	 */
	public Adapter createActiveProcessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSD <em>Active MSD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSD
	 * @generated
	 */
	public Adapter createActiveMSDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings <em>Active MSS Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSSRoleBindings
	 * @generated
	 */
	public Adapter createActiveMSSRoleBindingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Property To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createPropertyToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveState <em>Active State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveState
	 * @generated
	 */
	public Adapter createActiveStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSSState <em>Active MSS State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSSState
	 * @generated
	 */
	public Adapter createActiveMSSStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSDCut <em>Active MSD Cut</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSDCut
	 * @generated
	 */
	public Adapter createActiveMSDCutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings <em>Active MSD Lifeline Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSDLifelineBindings
	 * @generated
	 */
	public Adapter createActiveMSDLifelineBindingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations <em>Active MSD Variable Valuations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations
	 * @generated
	 */
	public Adapter createActiveMSDVariableValuationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Lifeline To Interaction Fragment Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createLifelineToInteractionFragmentMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Lifeline To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createLifelineToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Named Element To Message Event Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createNamedElementToMessageEventMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent <em>MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent
	 * @generated
	 */
	public Adapter createMSDModalMessageEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MSDObjectSystem <em>MSD Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem
	 * @generated
	 */
	public Adapter createMSDObjectSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To String Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToStringMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Integer Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToIntegerMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Boolean Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToBooleanMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active Process Key Wrapper To Active Process Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveProcessKeyWrapperToActiveProcessMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active State Key Wrapper To Active State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveStateKeyWrapperToActiveStateMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.MessageEventContainer <em>Message Event Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.MessageEventContainer
	 * @generated
	 */
	public Adapter createMessageEventContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Message Event Key Wrapper To Message Event Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createMessageEventKeyWrapperToMessageEventMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Object System Key Wrapper To MSD Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createObjectSystemKeyWrapperToMSDObjectSystemMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.runtime.ActiveMSS <em>Active MSS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.runtime.ActiveMSS
	 * @generated
	 */
	public Adapter createActiveMSSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EObject To EOperation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEObjectToEOperationMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EObject To UML Class Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEObjectToUMLClassMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EClass To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEClassToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.StateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.StateGraph
	 * @generated
	 */
	public Adapter createStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeStateGraph
	 * @generated
	 */
	public Adapter createRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.AnnotatableElement <em>Annotatable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.AnnotatableElement
	 * @generated
	 */
	public Adapter createAnnotatableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	public Adapter createRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.Modality <em>Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.Modality
	 * @generated
	 */
	public Adapter createModalityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.ModalMessageEvent <em>Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.ModalMessageEvent
	 * @generated
	 */
	public Adapter createModalMessageEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.ObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.ObjectSystem
	 * @generated
	 */
	public Adapter createObjectSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RuntimeAdapterFactory
