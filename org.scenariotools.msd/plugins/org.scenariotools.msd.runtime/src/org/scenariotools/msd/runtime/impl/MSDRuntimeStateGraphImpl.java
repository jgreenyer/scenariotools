/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.RuntimeUtil.MSDRuntimeStateCopier;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.impl.RuntimeStateGraphImpl;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MSD Runtime State Graph</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl#getElementContainer <em>Element Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl#getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap <em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDRuntimeStateGraphImpl extends RuntimeStateGraphImpl implements
		MSDRuntimeStateGraph {
	private int maxPassedindex;

	private static Logger logger = Activator.getLogManager().getLogger(
			MSDRuntimeStateGraphImpl.class.getName());

	/**
	 * The cached value of the '{@link #getElementContainer() <em>Element Container</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getElementContainer()
	 * @generated
	 * @ordered
	 */
	protected ElementContainer elementContainer;

	/**
	 * The cached value of the '{@link #getRuntimeUtil() <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuntimeUtil()
	 * @generated
	 * @ordered
	 */
	protected RuntimeUtil runtimeUtil;

	/**
	 * The cached value of the '{@link #getScenarioRunConfiguration() <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 * @ordered
	 */
	protected ScenarioRunConfiguration scenarioRunConfiguration;

	/**
	 * The cached value of the '{@link #getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap() <em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<MSDRuntimeStateKeyWrapper, MSDRuntimeState> msdRuntimeStateKeyWrapperToMSDRuntimeStateMap;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDRuntimeStateGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MSD_RUNTIME_STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainer getElementContainer() {
		return elementContainer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementContainer(
			ElementContainer newElementContainer, NotificationChain msgs) {
		ElementContainer oldElementContainer = elementContainer;
		elementContainer = newElementContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, oldElementContainer, newElementContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementContainer(ElementContainer newElementContainer) {
		if (newElementContainer != elementContainer) {
			NotificationChain msgs = null;
			if (elementContainer != null)
				msgs = ((InternalEObject)elementContainer).eInverseRemove(this, RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH, ElementContainer.class, msgs);
			if (newElementContainer != null)
				msgs = ((InternalEObject)newElementContainer).eInverseAdd(this, RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH, ElementContainer.class, msgs);
			msgs = basicSetElementContainer(newElementContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, newElementContainer, newElementContainer));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil getRuntimeUtil() {
		if (runtimeUtil != null && runtimeUtil.eIsProxy()) {
			InternalEObject oldRuntimeUtil = (InternalEObject)runtimeUtil;
			runtimeUtil = (RuntimeUtil)eResolveProxy(oldRuntimeUtil);
			if (runtimeUtil != oldRuntimeUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
			}
		}
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil basicGetRuntimeUtil() {
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuntimeUtil(RuntimeUtil newRuntimeUtil,
			NotificationChain msgs) {
		RuntimeUtil oldRuntimeUtil = runtimeUtil;
		runtimeUtil = newRuntimeUtil;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, oldRuntimeUtil, newRuntimeUtil);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		if (newRuntimeUtil != runtimeUtil) {
			NotificationChain msgs = null;
			if (runtimeUtil != null)
				msgs = ((InternalEObject)runtimeUtil).eInverseRemove(this, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, RuntimeUtil.class, msgs);
			if (newRuntimeUtil != null)
				msgs = ((InternalEObject)newRuntimeUtil).eInverseAdd(this, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, RuntimeUtil.class, msgs);
			msgs = basicSetRuntimeUtil(newRuntimeUtil, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, newRuntimeUtil, newRuntimeUtil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfiguration getScenarioRunConfiguration() {
		if (scenarioRunConfiguration != null && scenarioRunConfiguration.eIsProxy()) {
			InternalEObject oldScenarioRunConfiguration = (InternalEObject)scenarioRunConfiguration;
			scenarioRunConfiguration = (ScenarioRunConfiguration)eResolveProxy(oldScenarioRunConfiguration);
			if (scenarioRunConfiguration != oldScenarioRunConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
			}
		}
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioRunConfiguration basicGetScenarioRunConfiguration() {
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenarioRunConfiguration(ScenarioRunConfiguration newScenarioRunConfiguration) {
		ScenarioRunConfiguration oldScenarioRunConfiguration = scenarioRunConfiguration;
		scenarioRunConfiguration = newScenarioRunConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<MSDRuntimeStateKeyWrapper, MSDRuntimeState> getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap() {
		if (msdRuntimeStateKeyWrapperToMSDRuntimeStateMap == null) {
			msdRuntimeStateKeyWrapperToMSDRuntimeStateMap = new EcoreEMap<MSDRuntimeStateKeyWrapper,MSDRuntimeState>(RuntimePackage.Literals.MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY, MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl.class, this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP);
		}
		return msdRuntimeStateKeyWrapperToMSDRuntimeStateMap;
	}


	
	protected RuntimeUtil createRuntimeUtil(){
		return UtilFactory.eINSTANCE.createRuntimeUtil();
	}
	
	protected MSDRuntimeState createMSDRuntimeState(){
		return RuntimeFactory.eINSTANCE.createMSDRuntimeState();
	}
	
	protected MSDRuntimeState init(ScenarioRunConfiguration scenarioRunConfiguration, RuntimeUtil runtimeUtil){
		setScenarioRunConfiguration(scenarioRunConfiguration);
		setElementContainer(RuntimeFactory.eINSTANCE.createElementContainer());

		setRuntimeUtil(runtimeUtil);
		
		InteractionFragment finalFragment = UMLFactory.eINSTANCE
				.createOccurrenceSpecification();
		getElementContainer().setFinalFragment(finalFragment);
		getRuntimeUtil().setFinalFragment(finalFragment);

		MSDObjectSystem msdObjectSystem = RuntimeFactory.eINSTANCE
				.createMSDObjectSystem();
//		msdObjectSystem.setRuntimeUtil(getRuntimeUtil());

//		msdObjectSystem.getRootObjects().addAll(
//				getScenarioRunConfiguration().getSimulationRootObjects());

		MSDRuntimeState initialMSDRuntimeState = createMSDRuntimeState();
		initialMSDRuntimeState.setRuntimeUtil(getRuntimeUtil());
		initialMSDRuntimeState.setObjectSystem(msdObjectSystem);
		
		setStartState(initialMSDRuntimeState);

		getRuntimeUtil().init();
//		getRuntimeUtil().init(
//				scenarioRunConfiguration.getUml2EcoreMapping(),
//				scenarioRunConfiguration.getSimulationRootObjects(),
//				scenarioRunConfiguration
//						.getUseCaseSpecificationsToBeConsidered(),
//				scenarioRunConfiguration.getRoleToEObjectMappingContainer());
		
		
		// initialize strategy for counter-play-out or strategy-play-out if any exist
		if(getScenarioRunConfiguration().getStrategyPackage() != null){
			getRuntimeUtil().init(scenarioRunConfiguration.getUml2EcoreMapping(), 
					scenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered(),
					getScenarioRunConfiguration().getStrategyPackage());
		}else{
			getRuntimeUtil().init(scenarioRunConfiguration.getUml2EcoreMapping(), 
					scenarioRunConfiguration.getUseCaseSpecificationsToBeConsidered());
		}
		

		msdObjectSystem.init(scenarioRunConfiguration.getSimulationRootObjects(),
				scenarioRunConfiguration.getRoleToEObjectMappingContainer(),
				getRuntimeUtil());
		
		initializeInitialMSDRuntimeState(initialMSDRuntimeState, scenarioRunConfiguration.getExecutionSemantics());

		// "register" start state.
		getMSDRuntimeState(initialMSDRuntimeState,
				new BasicEList<ActiveProcess>());

		return initialMSDRuntimeState;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public MSDRuntimeState init(
			ScenarioRunConfiguration scenarioRunConfiguration) {
		return init(scenarioRunConfiguration, createRuntimeUtil());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param unchangedActiveMSDsEList
	 * @generated NOT
	 */
	public MSDRuntimeState getMSDRuntimeState(MSDRuntimeState msdRuntimeState,
			EList<ActiveProcess> unchangedActiveMSDsEList) {
		if (logger.isDebugEnabled()) {
			logger.debug("getMSDRuntimeState() called " + msdRuntimeState);
		}
		lookUpStateConstituents(msdRuntimeState, unchangedActiveMSDsEList);

		MSDRuntimeStateKeyWrapper msdRuntimeStateKeyWrapper = createMSDRuntimeStateKeyWrapper(msdRuntimeState);

		MSDRuntimeState registeredState = getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
				.get(msdRuntimeStateKeyWrapper);

		if (registeredState == null) {
			logger.debug("equal state was not already explored, registering and returning copied + modified state.");
			getStates().add(msdRuntimeState);
			maxPassedindex++;
			msdRuntimeState.getStringToStringAnnotationMap().put("passedIndex", maxPassedindex+"");
//			System.out.println("created state "+maxPassedindex);


		    for (ActiveProcess activeMSD : msdRuntimeState.getActiveProcesses()) {
		    	if(activeMSD instanceof ActiveMSD){
					for (Lifeline lifeline : ((ActiveMSD)activeMSD).getLifelineBindings().getLifelineToEObjectMap().keySet()) {
						Assert.isTrue(msdRuntimeState.getObjectSystem().containsEObject(((ActiveMSD)activeMSD).getLifelineBindings().getLifelineToEObjectMap().get(lifeline)));
					}
		    	}
			}

			// creating the map of relevant modal message events for this state:
			msdRuntimeState
					.updateMSDModalMessageEvents(getScenarioRunConfiguration()
							.getExecutionSemantics());

			getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().put(
					msdRuntimeStateKeyWrapper, msdRuntimeState);
			return msdRuntimeState;
		} else {
			logger.debug("equal state was already explored, return registered state.");
			return registeredState;
		}
	}
	
	
	protected void lookUpStateConstituents(MSDRuntimeState msdRuntimeState, EList<ActiveProcess> unchangedActiveMSDsEList){
		MSDObjectSystem lookedUpMSDObjectSystem = getElementContainer()
				.getObjectSystem(
						(MSDObjectSystem) msdRuntimeState.getObjectSystem());
		boolean anotherPreviouslyRegisteredObjectSystemFound = msdRuntimeState
				.getObjectSystem() != lookedUpMSDObjectSystem;
		MSDObjectSystem copiedMSDObjectSystem = (MSDObjectSystem) msdRuntimeState
				.getObjectSystem();
		// set the new object system
		msdRuntimeState.setObjectSystem(lookedUpMSDObjectSystem);

		// if a previously registered object system is used instead of the
		// freshly copied on,
		// the eObjects bound in the lifeline bindings and (possible) variable
		// valuations must be replaced,
		// i.e., the references to the freshly copied eObejects must be changed
		// to point to the corresponding
		// eObjects in the previously registered object system.
		HashMap<EObject, EObject> copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap = null;
		if (anotherPreviouslyRegisteredObjectSystemFound) {

			Assert.isTrue(getScenarioRunConfiguration().isDynamicObjectSystem()); // otherwise
																					// we
																					// cannot
																					// get
																					// here.

			copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap = new HashMap<EObject, EObject>();

			for (int i = 0; i < copiedMSDObjectSystem.getRootObjects().size(); i++) {
				EObject rootObjectInRegisteredObjectSystem = lookedUpMSDObjectSystem
						.getRootObjects().get(i);
				EObject copiedRootObject = copiedMSDObjectSystem
						.getRootObjects().get(i);

				copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
						.put(copiedRootObject,
								rootObjectInRegisteredObjectSystem);

				TreeIterator<EObject> regisgteredObjectSystemTreeIterator = rootObjectInRegisteredObjectSystem
						.eAllContents();
				TreeIterator<EObject> copiedObjectSystemTreeIterator = copiedRootObject
						.eAllContents();

				for (; regisgteredObjectSystemTreeIterator.hasNext();) {
					EObject eObjectInRegisteredObjectSystem = regisgteredObjectSystemTreeIterator
							.next();
					EObject copiedEObject = copiedObjectSystemTreeIterator
							.next();
					// System.out.println("eObjectInRegisteredObjectSystem: " +
					// eObjectInRegisteredObjectSystem);
					// System.out.println("copiedEObject:                   " +
					// copiedEObject);
					copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
							.put(copiedEObject, eObjectInRegisteredObjectSystem);
				}
			}
		}
		
		Set<ActiveProcess> oldProcesses=new HashSet<ActiveProcess>(msdRuntimeState.getActiveProcesses());

		for (ListIterator<ActiveProcess> activeMSDsListIterator = msdRuntimeState
				.getActiveProcesses().listIterator(); activeMSDsListIterator
				.hasNext();) {

			ActiveProcess activeMSD = activeMSDsListIterator.next();

			if (unchangedActiveMSDsEList.contains(activeMSD))
				continue;

			// if another previously registered object system was found, the
			// references to
			// the eObjects for the lifeline bindings and variable valuations is
			// changed here.
			if (anotherPreviouslyRegisteredObjectSystemFound) {
				ActiveMSDVariableValuations activeMSDVariableValuations = activeMSD
						.getVariableValuations();
				if (activeMSD instanceof ActiveMSD) {
					ActiveMSDLifelineBindings activeMSDLifelineBindings = ((ActiveMSD) activeMSD)
							.getLifelineBindings();
					for (Lifeline lifeline : activeMSDLifelineBindings
							.getLifelineToEObjectMap().keySet()) {
						EObject boundCopiedEObject = activeMSDLifelineBindings
								.getLifelineToEObjectMap().get(lifeline);
						EObject correspondingRegisteredEObject = copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
								.get(boundCopiedEObject);
						if (correspondingRegisteredEObject != null) {
							activeMSDLifelineBindings.getLifelineToEObjectMap()
									.put(lifeline,
											correspondingRegisteredEObject);
			    			Assert.isTrue(lookedUpMSDObjectSystem.containsEObject(correspondingRegisteredEObject));
						} else {
							// it could be that multiple active MSDs refer to
							// the same lifeline binding.
							// Then, replacement may already have taken place.

							// This is a test whether the currently referenced
							// eObject refers to one from the previously
							// registered object system.
							Assert.isTrue(copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
									.values().contains(boundCopiedEObject));
						}
					}
				} else if (activeMSD instanceof ActiveMSS) {
					ActiveMSSRoleBindings activeMSSRoleBindings = ((ActiveMSS) activeMSD)
							.getRoleBindings();
					for (Property property : activeMSSRoleBindings
							.getPropertyToEObjectMap().keySet()) {
						EObject boundCopiedEObject = activeMSSRoleBindings
								.getPropertyToEObjectMap().get(property);
						EObject correspondingRegisteredEObject = copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
								.get(boundCopiedEObject);
						if (correspondingRegisteredEObject != null) {
							activeMSSRoleBindings.getPropertyToEObjectMap()
									.put(property,
											correspondingRegisteredEObject);
						} else {
							// it could be that multiple active MSDs refer to
							// the same lifeline binding.
							// Then, replacement may already have taken place.

							// This is a test whether the currently referenced
							// eObject refers to one from the previously
							// registered object system.
							Assert.isTrue(copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
									.values().contains(boundCopiedEObject));
						}
					}
				}
				for (String variableName : activeMSDVariableValuations
						.getVariableNameToEObjectValueMap().keySet()) {
					EObject boundCopiedEObject = activeMSDVariableValuations
							.getVariableNameToEObjectValueMap().get(
									variableName);
					EObject correspondingRegisteredEObject = copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
							.get(boundCopiedEObject);
					if (correspondingRegisteredEObject != null) {
						activeMSDVariableValuations
								.getVariableNameToEObjectValueMap().put(
										variableName,
										correspondingRegisteredEObject);
					} else {
						// it could be that multiple active MSDs refer to the
						// same lifeline binding.
						// Then, replacement may already have taken place.

						// This is a test whether the currently referenced
						// eObject refers to one from the previously registered
						// object system.
						Assert.isTrue(copiedEObjectsToCorrespondingPreviouslyRegisteredEObjectsMap
								.values().contains(boundCopiedEObject));
					}
				}
			}

			ActiveProcess lookedUpNewProcess = getElementContainer()
					.getActiveProcess(activeMSD);

			if (!oldProcesses.contains(lookedUpNewProcess))
				activeMSDsListIterator.set(lookedUpNewProcess);
			else
				activeMSDsListIterator.remove();
		}
	}
	
	protected MSDRuntimeStateKeyWrapper createMSDRuntimeStateKeyWrapper(MSDRuntimeState msdRuntimeState){
		return new MSDRuntimeStateKeyWrapper(
				msdRuntimeState);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				if (elementContainer != null)
					msgs = ((InternalEObject)elementContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, null, msgs);
				return basicSetElementContainer((ElementContainer)otherEnd, msgs);
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				if (runtimeUtil != null)
					msgs = ((InternalEObject)runtimeUtil).eInverseRemove(this, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, RuntimeUtil.class, msgs);
				return basicSetRuntimeUtil((RuntimeUtil)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * Initializes the initial MSDRuntimeState. In this case, it will add to the
	 * MessageEventToModalMessageEventMap the environment events that initialize
	 * MSDs.
	 * 
	 * It can be overridden by subclasses (if they want also other events to be
	 * considered in the initial state)
	 * 
	 * @param initialMSDRuntimeState
	 * @param executionSemantics 
	 */
	protected void initializeInitialMSDRuntimeState(
			MSDRuntimeState initialMSDRuntimeState, ExecutionSemantics executionSemantics) {
	initialMSDRuntimeState.updateMSDModalMessageEvents(executionSemantics);

//		for (MSDModalMessageEvent msdModalMessageEvent : getRuntimeUtil()
//				.getInitializingEnvironmentMSDModalMessageEvents()) {
//			initialMSDRuntimeState.getMessageEventToModalMessageEventMap().put(
//					msdModalMessageEvent.getRepresentedMessageEvent(),
//					msdModalMessageEvent);
//		}
	}
	
	
	protected MSDRuntimeStateCopier createMSDRuntimeStateCopier(){
		return new MSDRuntimeStateCopier();
	}

	@Override
	public Transition generateSuccessor(RuntimeState state, Event event) {
//		System.out.println("event="+event);

//		Assert.isTrue((event instanceof MessageEvent)
//				&& ((MessageEvent) event).isConcrete());

		Transition transition = ((MSDRuntimeState) state)
				.getEventToTransitionMap().get(event);

		if (transition != null)
			return transition;

		if (logger.isDebugEnabled()) {
			logger.debug("generateSuccessor called for event " + event);
			logger.debug("and state: "
					+ RuntimeUtil.Helper
							.getMSDRuntimeStateString((MSDRuntimeState) state));
		}
		MSDRuntimeStateCopier msdRuntimeStateCopier = createMSDRuntimeStateCopier();
		MSDRuntimeState msdRuntimeStateCopy = msdRuntimeStateCopier.copy((MSDRuntimeState) state, event);
		
		if (logger.isDebugEnabled()){
			logger.debug("copied state: " + RuntimeUtil.Helper.getMSDRuntimeStateString(msdRuntimeStateCopy));
		}

		//msdRuntimeStateCopy.updateMSDModalMessageEvents(getScenarioRunConfiguration().getExecutionSemantics());
		msdRuntimeStateCopy.setStateGraph(this);
		
		
		if(((MSDRuntimeStateGraph)state.getStateGraph()).getScenarioRunConfiguration().isDynamicObjectSystem()){
			// check if all message event are the registered ones
			if (event instanceof MessageEvent && logger.isDebugEnabled())
				assertUniquenessOfMessageEvents(msdRuntimeStateCopy, msdRuntimeStateCopier.getCopiedEvent());		
			
			msdRuntimeStateCopy.performStep(msdRuntimeStateCopier.getCopiedEvent());
			
			if (event instanceof MessageEvent && logger.isDebugEnabled())
				assertUniquenessOfMessageEvents(msdRuntimeStateCopy, msdRuntimeStateCopier.getCopiedEvent());		

		}else{
			if (event instanceof MessageEvent && logger.isDebugEnabled())
				assertUniquenessOfMessageEvents(msdRuntimeStateCopy, event);		

			msdRuntimeStateCopy.performStep(event);
			
			if (event instanceof MessageEvent && logger.isDebugEnabled())
				assertUniquenessOfMessageEvents(msdRuntimeStateCopy, event);		

		}
		
		//Assert.isTrue(msdRuntimeStateCopier.getUnchangedActiveMSDsEList().isEmpty());
		MSDRuntimeState successorMSDRuntimeState = getMSDRuntimeState(
				msdRuntimeStateCopy,
				msdRuntimeStateCopier.getUnchangedActiveMSDsEList());
		if(successorMSDRuntimeState!=msdRuntimeStateCopy){
			disposeCopyOfMSDRuntimeState(msdRuntimeStateCopy);
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("copied and modified state: "
					+ RuntimeUtil.Helper
							.getMSDRuntimeStateString(msdRuntimeStateCopy));
		}

		// check if all message event are the registered ones
		if (logger.isDebugEnabled()){
				assertUniquenessOfMessageEvents(successorMSDRuntimeState);
		}
		
//		System.out.println("adding transition for event "+event+" in state "+state+", leading to "+successorMSDRuntimeState);
		transition = StategraphFactory.eINSTANCE.createTransition();
		transition.setEvent(event);
		transition.setSourceState(state);
		transition.setTargetState(successorMSDRuntimeState);

		((MSDRuntimeState) state).getEventToTransitionMap().put(event,
				transition);

		return transition;
	}

	protected void disposeCopyOfMSDRuntimeState(MSDRuntimeState state){
		state.setStateGraph(null);
		state.getIncomingTransition().clear();
		state.getOutgoingTransition().clear();
	}
	
	private void assertUniquenessOfMessageEvents(
			MSDRuntimeState msdRuntimeState) {
		assertUniquenessOfMessageEvents(msdRuntimeState, null);
		
	}

	private void assertUniquenessOfMessageEvents(MSDRuntimeState msdRuntimeState, Event event){
		// check if all message event are the registered ones
		for (ActiveProcess activeMSD : msdRuntimeState.getActiveProcesses()) {
			for (MessageEvent me : activeMSD.getColdForbiddenEvents().values()) {
				MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
				Assert.isTrue(me == me2);
			}
			for (MessageEvent me : activeMSD.getHotForbiddenEvents().values()) {
				MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
				Assert.isTrue(me == me2);
			}
			for (MessageEvent me : activeMSD.getEnabledEvents().values()) {
				MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
				Assert.isTrue(me == me2);
			}
			for (MessageEvent me : activeMSD.getViolatingEvents().values()) {
				MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
				Assert.isTrue(me == me2);
			}
		}
		for (MessageEvent me : msdRuntimeState.getMessageEventToModalMessageEventMap().keySet()) {
			MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
			Assert.isTrue(me == me2);			
		}
		if(event != null){
			MessageEvent me = (MessageEvent) event;
			MessageEvent me2 = ((MSDObjectSystem)msdRuntimeState.getObjectSystem()).getMessageEvent(me.getOperation(), me.getSideEffectOnEStructuralFeature(), me.getSendingObject(), me.getReceivingObject(), RuntimeUtil.Helper.getParameterValue(me));
			Assert.isTrue(me == me2);		
		}
	}
	
	
	@Override
	public EList<Transition> generateAllSuccessors(RuntimeState runtimeState) {
		EList<Transition> allTransitions = new BasicEList<Transition>();
		for (MessageEvent messageEvent : runtimeState.getMessageEventToModalMessageEventMap().keySet()) {
			if (messageEvent.isConcrete()){				
				if(!runtimeState.isSafetyViolationOccurredInAssumptions() && !runtimeState.isSafetyViolationOccurredInRequirements()){
					allTransitions.add(generateSuccessor(runtimeState, messageEvent));
				}
			}
		}
		return allTransitions;
	}

	@Override
	public boolean isEventInAlphabet(Event event) {
		if (event instanceof MessageEvent) {
			MessageEvent messageEvent = (MessageEvent) event;
			for (ObjectSystem objectSystem : getElementContainer()
					.getObjectSystems()) {
				MSDObjectSystem msdObjectSystem = (MSDObjectSystem) objectSystem;
				EList<EOperation> sendableOperations = msdObjectSystem
						.getEObjectToSendableEOperationMap().get(
								messageEvent.getSendingObject());
				EList<EOperation> receivableOperations = msdObjectSystem
						.getEObjectToReceivableEOperationMap().get(
								messageEvent.getReceivingObject());
				if (sendableOperations != null && receivableOperations != null) {
					boolean sendableOperation = sendableOperations
							.contains(messageEvent.getOperation());
					boolean receivableOperation = receivableOperations
							.contains(messageEvent.getOperation());
					if (sendableOperation && receivableOperation)
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return basicSetElementContainer(null, msgs);
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				return basicSetRuntimeUtil(null, msgs);
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP:
				return ((InternalEList<?>)getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return getElementContainer();
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				if (resolve) return getRuntimeUtil();
				return basicGetRuntimeUtil();
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION:
				if (resolve) return getScenarioRunConfiguration();
				return basicGetScenarioRunConfiguration();
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP:
				if (coreType) return getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap();
				else return getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				setElementContainer((ElementContainer)newValue);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)newValue);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((ScenarioRunConfiguration)newValue);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP:
				((EStructuralFeature.Setting)getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				setElementContainer((ElementContainer)null);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)null);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((ScenarioRunConfiguration)null);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP:
				getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return elementContainer != null;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL:
				return runtimeUtil != null;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION:
				return scenarioRunConfiguration != null;
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP:
				return msdRuntimeStateKeyWrapperToMSDRuntimeStateMap != null && !msdRuntimeStateKeyWrapperToMSDRuntimeStateMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // MSDRuntimeStateGraphImpl
