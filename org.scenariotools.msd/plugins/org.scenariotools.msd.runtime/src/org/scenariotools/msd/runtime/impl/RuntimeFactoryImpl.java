/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.SynchronousMessageEvent;
import org.scenariotools.msd.runtime.*;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeFactoryImpl extends EFactoryImpl implements RuntimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RuntimeFactory init() {
		try {
			RuntimeFactory theRuntimeFactory = (RuntimeFactory)EPackage.Registry.INSTANCE.getEFactory(RuntimePackage.eNS_URI);
			if (theRuntimeFactory != null) {
				return theRuntimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RuntimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH: return createMSDRuntimeStateGraph();
			case RuntimePackage.MSD_RUNTIME_STATE: return createMSDRuntimeState();
			case RuntimePackage.ELEMENT_CONTAINER: return createElementContainer();
			case RuntimePackage.MSD_MODALITY: return createMSDModality();
			case RuntimePackage.ACTIVE_PROCESS: return createActiveProcess();
			case RuntimePackage.ACTIVE_MSD_CUT: return createActiveMSDCut();
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS: return createActiveMSDLifelineBindings();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS: return createActiveMSDVariableValuations();
			case RuntimePackage.LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY: return (EObject)createLifelineToInteractionFragmentMapEntry();
			case RuntimePackage.STRING_TO_EOBJECT_MAP_ENTRY: return (EObject)createStringToEObjectMapEntry();
			case RuntimePackage.LIFELINE_TO_EOBJECT_MAP_ENTRY: return (EObject)createLifelineToEObjectMapEntry();
			case RuntimePackage.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY: return (EObject)createNamedElementToMessageEventMapEntry();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT: return createMSDModalMessageEvent();
			case RuntimePackage.MSD_OBJECT_SYSTEM: return createMSDObjectSystem();
			case RuntimePackage.STRING_TO_STRING_MAP_ENTRY: return (EObject)createStringToStringMapEntry();
			case RuntimePackage.STRING_TO_INTEGER_MAP_ENTRY: return (EObject)createStringToIntegerMapEntry();
			case RuntimePackage.STRING_TO_BOOLEAN_MAP_ENTRY: return (EObject)createStringToBooleanMapEntry();
			case RuntimePackage.MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY: return (EObject)createMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry();
			case RuntimePackage.ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY: return (EObject)createActiveProcessKeyWrapperToActiveProcessMapEntry();
			case RuntimePackage.ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY: return (EObject)createActiveStateKeyWrapperToActiveStateMapEntry();
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY: return (EObject)createActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY: return (EObject)createActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry();
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY: return (EObject)createActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry();
			case RuntimePackage.MESSAGE_EVENT_CONTAINER: return createMessageEventContainer();
			case RuntimePackage.MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY: return (EObject)createMessageEventKeyWrapperToMessageEventMapEntry();
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY: return (EObject)createObjectSystemKeyWrapperToMSDObjectSystemMapEntry();
			case RuntimePackage.ACTIVE_MSS: return createActiveMSS();
			case RuntimePackage.ACTIVE_MSD: return createActiveMSD();
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS: return createActiveMSSRoleBindings();
			case RuntimePackage.PROPERTY_TO_EOBJECT_MAP_ENTRY: return (EObject)createPropertyToEObjectMapEntry();
			case RuntimePackage.ACTIVE_STATE: return createActiveState();
			case RuntimePackage.ACTIVE_MSS_STATE: return createActiveMSSState();
			case RuntimePackage.EOBJECT_TO_EOPERATION_MAP_ENTRY: return (EObject)createEObjectToEOperationMapEntry();
			case RuntimePackage.EOBJECT_TO_UML_CLASS_MAP_ENTRY: return (EObject)createEObjectToUMLClassMapEntry();
			case RuntimePackage.ECLASS_TO_EOBJECT_MAP_ENTRY: return (EObject)createEClassToEObjectMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RuntimePackage.EXECUTION_SEMANTICS:
				return createExecutionSemanticsFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_MSD_PROGRESS:
				return createActiveMSDProgressFromString(eDataType, initialValue);
			case RuntimePackage.SEMANTIC_EXCEPTION:
				return createSemanticExceptionFromString(eDataType, initialValue);
			case RuntimePackage.PARSER_EXCEPTION:
				return createParserExceptionFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_STATE_KEY_WRAPPER:
				return createActiveStateKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER:
				return createActiveMSDLifelineBindingsKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER:
				return createActiveMSDVariableValuationsKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_PROCESS_KEY_WRAPPER:
				return createActiveProcessKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.MSD_RUNTIME_STATE_KEY_WRAPPER:
				return createMSDRuntimeStateKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.MESSAGE_EVENT_KEY_WRAPPER:
				return createMessageEventKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER:
				return createObjectSystemKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER:
				return createActiveMSSRoleBindingsKeyWrapperFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RuntimePackage.EXECUTION_SEMANTICS:
				return convertExecutionSemanticsToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_MSD_PROGRESS:
				return convertActiveMSDProgressToString(eDataType, instanceValue);
			case RuntimePackage.SEMANTIC_EXCEPTION:
				return convertSemanticExceptionToString(eDataType, instanceValue);
			case RuntimePackage.PARSER_EXCEPTION:
				return convertParserExceptionToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_STATE_KEY_WRAPPER:
				return convertActiveStateKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER:
				return convertActiveMSDLifelineBindingsKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER:
				return convertActiveMSDVariableValuationsKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_PROCESS_KEY_WRAPPER:
				return convertActiveProcessKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.MSD_RUNTIME_STATE_KEY_WRAPPER:
				return convertMSDRuntimeStateKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.MESSAGE_EVENT_KEY_WRAPPER:
				return convertMessageEventKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER:
				return convertObjectSystemKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER:
				return convertActiveMSSRoleBindingsKeyWrapperToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph createMSDRuntimeStateGraph() {
		MSDRuntimeStateGraphImpl msdRuntimeStateGraph = new MSDRuntimeStateGraphImpl();
		return msdRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeState createMSDRuntimeState() {
		MSDRuntimeStateImpl msdRuntimeState = new MSDRuntimeStateImpl();
		return msdRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainer createElementContainer() {
		ElementContainerImpl elementContainer = new ElementContainerImpl();
		return elementContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDModality createMSDModality() {
		MSDModalityImpl msdModality = new MSDModalityImpl();
		return msdModality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveProcess createActiveProcess() {
		ActiveProcessImpl activeProcess = new ActiveProcessImpl();
		return activeProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSD createActiveMSD() {
		ActiveMSDImpl activeMSD = new ActiveMSDImpl();
		return activeMSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSSRoleBindings createActiveMSSRoleBindings() {
		ActiveMSSRoleBindingsImpl activeMSSRoleBindings = new ActiveMSSRoleBindingsImpl();
		return activeMSSRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Property, EObject> createPropertyToEObjectMapEntry() {
		PropertyToEObjectMapEntryImpl propertyToEObjectMapEntry = new PropertyToEObjectMapEntryImpl();
		return propertyToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveState createActiveState() {
		ActiveStateImpl activeState = new ActiveStateImpl();
		return activeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSSState createActiveMSSState() {
		ActiveMSSStateImpl activeMSSState = new ActiveMSSStateImpl();
		return activeMSSState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDCut createActiveMSDCut() {
		ActiveMSDCutImpl activeMSDCut = new ActiveMSDCutImpl();
		return activeMSDCut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings createActiveMSDLifelineBindings() {
		ActiveMSDLifelineBindingsImpl activeMSDLifelineBindings = new ActiveMSDLifelineBindingsImpl();
		return activeMSDLifelineBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDVariableValuations createActiveMSDVariableValuations() {
		ActiveMSDVariableValuationsImpl activeMSDVariableValuations = new ActiveMSDVariableValuationsImpl();
		return activeMSDVariableValuations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Lifeline, InteractionFragment> createLifelineToInteractionFragmentMapEntry() {
		LifelineToInteractionFragmentMapEntryImpl lifelineToInteractionFragmentMapEntry = new LifelineToInteractionFragmentMapEntryImpl();
		return lifelineToInteractionFragmentMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, EObject> createStringToEObjectMapEntry() {
		StringToEObjectMapEntryImpl stringToEObjectMapEntry = new StringToEObjectMapEntryImpl();
		return stringToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Lifeline, EObject> createLifelineToEObjectMapEntry() {
		LifelineToEObjectMapEntryImpl lifelineToEObjectMapEntry = new LifelineToEObjectMapEntryImpl();
		return lifelineToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<NamedElement, MessageEvent> createNamedElementToMessageEventMapEntry() {
		NamedElementToMessageEventMapEntryImpl namedElementToMessageEventMapEntry = new NamedElementToMessageEventMapEntryImpl();
		return namedElementToMessageEventMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDModalMessageEvent createMSDModalMessageEvent() {
		MSDModalMessageEventImpl msdModalMessageEvent = new MSDModalMessageEventImpl();
		return msdModalMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDObjectSystem createMSDObjectSystem() {
		MSDObjectSystemImpl msdObjectSystem = new MSDObjectSystemImpl();
		return msdObjectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createStringToStringMapEntry() {
		StringToStringMapEntryImpl stringToStringMapEntry = new StringToStringMapEntryImpl();
		return stringToStringMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Integer> createStringToIntegerMapEntry() {
		StringToIntegerMapEntryImpl stringToIntegerMapEntry = new StringToIntegerMapEntryImpl();
		return stringToIntegerMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Boolean> createStringToBooleanMapEntry() {
		StringToBooleanMapEntryImpl stringToBooleanMapEntry = new StringToBooleanMapEntryImpl();
		return stringToBooleanMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<MSDRuntimeStateKeyWrapper, MSDRuntimeState> createMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry() {
		MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry = new MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl();
		return msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveProcessKeyWrapper, ActiveProcess> createActiveProcessKeyWrapperToActiveProcessMapEntry() {
		ActiveProcessKeyWrapperToActiveProcessMapEntryImpl activeProcessKeyWrapperToActiveProcessMapEntry = new ActiveProcessKeyWrapperToActiveProcessMapEntryImpl();
		return activeProcessKeyWrapperToActiveProcessMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveStateKeyWrapper, ActiveState> createActiveStateKeyWrapperToActiveStateMapEntry() {
		ActiveStateKeyWrapperToActiveStateMapEntryImpl activeStateKeyWrapperToActiveStateMapEntry = new ActiveStateKeyWrapperToActiveStateMapEntryImpl();
		return activeStateKeyWrapperToActiveStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> createActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry() {
		ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry = new ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl();
		return activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> createActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry() {
		ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry = new ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl();
		return activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> createActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry() {
		ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry = new ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl();
		return activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventContainer createMessageEventContainer() {
		MessageEventContainerImpl messageEventContainer = new MessageEventContainerImpl();
		return messageEventContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<MessageEventKeyWrapper, MessageEvent> createMessageEventKeyWrapperToMessageEventMapEntry() {
		MessageEventKeyWrapperToMessageEventMapEntryImpl messageEventKeyWrapperToMessageEventMapEntry = new MessageEventKeyWrapperToMessageEventMapEntryImpl();
		return messageEventKeyWrapperToMessageEventMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ObjectSystemKeyWrapper, MSDObjectSystem> createObjectSystemKeyWrapperToMSDObjectSystemMapEntry() {
		ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl objectSystemKeyWrapperToMSDObjectSystemMapEntry = new ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl();
		return objectSystemKeyWrapperToMSDObjectSystemMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSS createActiveMSS() {
		ActiveMSSImpl activeMSS = new ActiveMSSImpl();
		return activeMSS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EObject, EList<EOperation>> createEObjectToEOperationMapEntry() {
		EObjectToEOperationMapEntryImpl eObjectToEOperationMapEntry = new EObjectToEOperationMapEntryImpl();
		return eObjectToEOperationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EObject, EList<org.eclipse.uml2.uml.Class>> createEObjectToUMLClassMapEntry() {
		EObjectToUMLClassMapEntryImpl eObjectToUMLClassMapEntry = new EObjectToUMLClassMapEntryImpl();
		return eObjectToUMLClassMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EClass, EList<EObject>> createEClassToEObjectMapEntry() {
		EClassToEObjectMapEntryImpl eClassToEObjectMapEntry = new EClassToEObjectMapEntryImpl();
		return eClassToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionSemantics createExecutionSemanticsFromString(EDataType eDataType, String initialValue) {
		ExecutionSemantics result = ExecutionSemantics.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertExecutionSemanticsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDProgress createActiveMSDProgressFromString(EDataType eDataType, String initialValue) {
		ActiveMSDProgress result = ActiveMSDProgress.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveMSDProgressToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticException createSemanticExceptionFromString(EDataType eDataType, String initialValue) {
		return (SemanticException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSemanticExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParserException createParserExceptionFromString(EDataType eDataType, String initialValue) {
		return (ParserException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertParserExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveStateKeyWrapper createActiveStateKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveStateKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveStateKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindingsKeyWrapper createActiveMSDLifelineBindingsKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveMSDLifelineBindingsKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveMSDLifelineBindingsKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDVariableValuationsKeyWrapper createActiveMSDVariableValuationsKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveMSDVariableValuationsKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveMSDVariableValuationsKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveProcessKeyWrapper createActiveProcessKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveProcessKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveProcessKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateKeyWrapper createMSDRuntimeStateKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (MSDRuntimeStateKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMSDRuntimeStateKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventKeyWrapper createMessageEventKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (MessageEventKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMessageEventKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSystemKeyWrapper createObjectSystemKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ObjectSystemKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObjectSystemKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSSRoleBindingsKeyWrapper createActiveMSSRoleBindingsKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveMSSRoleBindingsKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveMSSRoleBindingsKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimePackage getRuntimePackage() {
		return (RuntimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RuntimePackage getPackage() {
		return RuntimePackage.eINSTANCE;
	}

} //RuntimeFactoryImpl
