/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MessageEventContainerImpl#getRuntimeMessages <em>Runtime Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MessageEventContainerImpl#getMessageEvents <em>Message Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageEventContainerImpl extends RuntimeEObjectImpl implements MessageEventContainer {
	/**
	 * The cached value of the '{@link #getRuntimeMessages() <em>Runtime Messages</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<RuntimeMessage> runtimeMessages;

	/**
	 * The cached value of the '{@link #getMessageEvents() <em>Message Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> messageEvents;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MESSAGE_EVENT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuntimeMessage> getRuntimeMessages() {
		if (runtimeMessages == null) {
			runtimeMessages = new EObjectContainmentEList<RuntimeMessage>(RuntimeMessage.class, this, RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES);
		}
		return runtimeMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getMessageEvents() {
		if (messageEvents == null) {
			messageEvents = new EObjectContainmentEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS);
		}
		return messageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES:
				return ((InternalEList<?>)getRuntimeMessages()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS:
				return ((InternalEList<?>)getMessageEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES:
				return getRuntimeMessages();
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS:
				return getMessageEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES:
				getRuntimeMessages().clear();
				getRuntimeMessages().addAll((Collection<? extends RuntimeMessage>)newValue);
				return;
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS:
				getMessageEvents().clear();
				getMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES:
				getRuntimeMessages().clear();
				return;
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS:
				getMessageEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES:
				return runtimeMessages != null && !runtimeMessages.isEmpty();
			case RuntimePackage.MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS:
				return messageEvents != null && !messageEvents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MessageEventContainerImpl
