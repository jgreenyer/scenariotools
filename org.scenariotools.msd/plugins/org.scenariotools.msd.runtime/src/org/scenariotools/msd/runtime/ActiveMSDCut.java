/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSD Cut</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDCut#getLifelineToInteractionFragmentMap <em>Lifeline To Interaction Fragment Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDCut()
 * @model
 * @generated
 */
public interface ActiveMSDCut extends ActiveState {
	/**
	 * Returns the value of the '<em><b>Lifeline To Interaction Fragment Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Lifeline},
	 * and the value is of type {@link org.eclipse.uml2.uml.InteractionFragment},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline To Interaction Fragment Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifeline To Interaction Fragment Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDCut_LifelineToInteractionFragmentMap()
	 * @model mapType="org.scenariotools.msd.runtime.LifelineToInteractionFragmentMapEntry<org.eclipse.uml2.uml.Lifeline, org.eclipse.uml2.uml.InteractionFragment>"
	 * @generated
	 */
	EMap<Lifeline, InteractionFragment> getLifelineToInteractionFragmentMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated NOT
	 */
	boolean progressCutLocationOnLifeline(Lifeline lifeline, InteractionFragment currentFragment);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean terminalCutReached();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isMessageEnabled(Message message);

	/**
	 * @generated NOT
	 */
	public boolean isMOSEnabled(MessageOccurrenceSpecification mos, Lifeline lifeline);
} // ActiveMSDCut
