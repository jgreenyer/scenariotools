/**
 */
package org.scenariotools.msd.runtime.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSS Role Bindings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsImpl#getPropertyToEObjectMap <em>Property To EObject Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSSRoleBindingsImpl extends RuntimeEObjectImpl implements ActiveMSSRoleBindings {
	/**
	 * The cached value of the '{@link #getPropertyToEObjectMap() <em>Property To EObject Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyToEObjectMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Property, EObject> propertyToEObjectMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSSRoleBindingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSS_ROLE_BINDINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Property, EObject> getPropertyToEObjectMap() {
		if (propertyToEObjectMap == null) {
			propertyToEObjectMap = new EcoreEMap<Property,EObject>(RuntimePackage.Literals.PROPERTY_TO_EOBJECT_MAP_ENTRY, PropertyToEObjectMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP);
		}
		return propertyToEObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP:
				return ((InternalEList<?>)getPropertyToEObjectMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP:
				if (coreType) return getPropertyToEObjectMap();
				else return getPropertyToEObjectMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP:
				((EStructuralFeature.Setting)getPropertyToEObjectMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP:
				getPropertyToEObjectMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP:
				return propertyToEObjectMap != null && !propertyToEObjectMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActiveMSSRoleBindingsImpl
