/**
 */
package org.scenariotools.msd.runtime.impl;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.NamedElement;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Active Process</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getVariableValuations <em>Variable Valuations</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getColdForbiddenEvents <em>Cold Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getHotForbiddenEvents <em>Hot Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getViolatingEvents <em>Violating Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getEnabledEvents <em>Enabled Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getCurrentState <em>Current State</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getObjectSystem <em>Object System</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveProcessImpl extends RuntimeEObjectImpl implements
		ActiveProcess {
	protected static Logger logger = Activator.getLogManager().getLogger(
			MSDRuntimeStateImpl.class.getName());
	/**
	 * The cached value of the '{@link #getVariableValuations() <em>Variable Valuations</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getVariableValuations()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSDVariableValuations variableValuations;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDObjectSystem getObjectSystem() {
		if (objectSystem != null && objectSystem.eIsProxy()) {
			InternalEObject oldObjectSystem = (InternalEObject)objectSystem;
			objectSystem = (MSDObjectSystem)eResolveProxy(oldObjectSystem);
			if (objectSystem != oldObjectSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
			}
		}
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDObjectSystem basicGetObjectSystem() {
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSystem(MSDObjectSystem newObjectSystem) {
		MSDObjectSystem oldObjectSystem = objectSystem;
		objectSystem = newObjectSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PROCESS__ID, oldId, id));
	}

	/**
	 * The cached value of the '{@link #getColdForbiddenEvents() <em>Cold Forbidden Events</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getColdForbiddenEvents()
	 * @generated
	 * @ordered
	 */
	protected EMap<NamedElement, MessageEvent> coldForbiddenEvents;

	/**
	 * The cached value of the '{@link #getHotForbiddenEvents() <em>Hot Forbidden Events</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getHotForbiddenEvents()
	 * @generated
	 * @ordered
	 */
	protected EMap<NamedElement, MessageEvent> hotForbiddenEvents;

	/**
	 * The cached value of the '{@link #getViolatingEvents() <em>Violating Events</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getViolatingEvents()
	 * @generated
	 * @ordered
	 */
	protected EMap<NamedElement, MessageEvent> violatingEvents;

	/**
	 * The cached value of the '{@link #getEnabledEvents()
	 * <em>Enabled Events</em>}' map. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getEnabledEvents()
	 * @generated
	 * @ordered
	 */
	protected EMap<NamedElement, MessageEvent> enabledEvents;

	/**
	 * The cached value of the '{@link #getRuntimeUtil() <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuntimeUtil()
	 * @generated
	 * @ordered
	 */
	protected RuntimeUtil runtimeUtil;

	/**
	 * The cached value of the '{@link #getCurrentState() <em>Current State</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCurrentState()
	 * @generated
	 * @ordered
	 */
	protected ActiveState currentState;
	/**
	 * The cached value of the '{@link #getObjectSystem() <em>Object System</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getObjectSystem()
	 * @generated
	 * @ordered
	 */
	protected MSDObjectSystem objectSystem;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ActiveProcessImpl() {
		super();
		this.id = EcoreUtil.generateUUID();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_PROCESS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDVariableValuations getVariableValuations() {
		if (variableValuations != null && variableValuations.eIsProxy()) {
			InternalEObject oldVariableValuations = (InternalEObject)variableValuations;
			variableValuations = (ActiveMSDVariableValuations)eResolveProxy(oldVariableValuations);
			if (variableValuations != oldVariableValuations) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS, oldVariableValuations, variableValuations));
			}
		}
		return variableValuations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDVariableValuations basicGetVariableValuations() {
		return variableValuations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableValuations(
			ActiveMSDVariableValuations newVariableValuations) {
		ActiveMSDVariableValuations oldVariableValuations = variableValuations;
		variableValuations = newVariableValuations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS, oldVariableValuations, variableValuations));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<NamedElement, MessageEvent> getColdForbiddenEvents() {
		if (coldForbiddenEvents == null) {
			coldForbiddenEvents = new EcoreEMap<NamedElement,MessageEvent>(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY, NamedElementToMessageEventMapEntryImpl.class, this, RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS);
		}
		return coldForbiddenEvents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<NamedElement, MessageEvent> getHotForbiddenEvents() {
		if (hotForbiddenEvents == null) {
			hotForbiddenEvents = new EcoreEMap<NamedElement,MessageEvent>(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY, NamedElementToMessageEventMapEntryImpl.class, this, RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS);
		}
		return hotForbiddenEvents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<NamedElement, MessageEvent> getViolatingEvents() {
		if (violatingEvents == null) {
			violatingEvents = new EcoreEMap<NamedElement,MessageEvent>(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY, NamedElementToMessageEventMapEntryImpl.class, this, RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS);
		}
		return violatingEvents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<NamedElement, MessageEvent> getEnabledEvents() {
		if (enabledEvents == null) {
			enabledEvents = new EcoreEMap<NamedElement,MessageEvent>(RuntimePackage.Literals.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY, NamedElementToMessageEventMapEntryImpl.class, this, RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS);
		}
		return enabledEvents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil getRuntimeUtil() {
		if (runtimeUtil != null && runtimeUtil.eIsProxy()) {
			InternalEObject oldRuntimeUtil = (InternalEObject)runtimeUtil;
			runtimeUtil = (RuntimeUtil)eResolveProxy(oldRuntimeUtil);
			if (runtimeUtil != oldRuntimeUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
			}
		}
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil basicGetRuntimeUtil() {
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		RuntimeUtil oldRuntimeUtil = runtimeUtil;
		runtimeUtil = newRuntimeUtil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveState getCurrentState() {
		if (currentState != null && currentState.eIsProxy()) {
			InternalEObject oldCurrentState = (InternalEObject)currentState;
			currentState = (ActiveState)eResolveProxy(oldCurrentState);
			if (currentState != oldCurrentState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE, oldCurrentState, currentState));
			}
		}
		return currentState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveState basicGetCurrentState() {
		return currentState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentState(ActiveState newCurrentState) {
		ActiveState oldCurrentState = currentState;
		currentState = newCurrentState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE, oldCurrentState, currentState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isSafetyViolating(MessageEvent messageEvent) {
		// 1) is the event message unifiable with an event that is safety
		// violating in the current cut?
		if (currentState.isHot() && isViolating(messageEvent)) {
			return true;
		}
		if (getHotForbiddenEvents().values().contains(messageEvent))
			return true;

		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isColdViolating(MessageEvent messageEvent) {
		if (!currentState.isHot() && isViolating(messageEvent)) {
			return true;
		}
		if (getColdForbiddenEvents().values().contains(messageEvent))
			return true;

		return false;
	}

	protected boolean isViolating(MessageEvent messageEvent) {

		// is the message event parameter unifiable with some violating event?
		for (MessageEvent violatingEvent : getViolatingEvents().values()) {
			if (RuntimeUtil.Helper.isParameterUnifiable(messageEvent,
					violatingEvent, this)) {
				return true;
			}
		}
		/*
		 * does the is the message event violate some enabled event? it can be
		 * that there are multiple events enabled. it can even be that there are
		 * multiple messages enabled that refer to the same message only with
		 * different parameter values. Let's assume we have enabled m(2) and
		 * m(3) and want to know - is m(4) violating -> yes. - is m(3) violating
		 * -> no. So, what do we do? We iterate over the enabled message events
		 * 1. First we check if there is a message event enabled that is message
		 * unifiable with the candidate violating event. 2. Second, we check if
		 * there is a message event enabled that is parameter unifiable with the
		 * candidate violating event. -> if 1. is TRUE and 2. is FALSE, return
		 * TRUE (-> "yes, there is a violating", there is a message unifiable
		 * event enabled, but no parameter unifiable one.)
		 */
		boolean messageUnifiableEventEnabled = false;
		boolean parameterUnifiableEventEnabled = false;
		for (MessageEvent enabledEvent : getEnabledEvents().values()) {
			if (!messageUnifiableEventEnabled
					&& RuntimeUtil.Helper.isMessageUnifiable(messageEvent,
							enabledEvent)) {
				messageUnifiableEventEnabled = true;
			}
			if (!parameterUnifiableEventEnabled
					&& RuntimeUtil.Helper.isParameterUnifiable(messageEvent,
							enabledEvent, this)) {
				parameterUnifiableEventEnabled = true;
				break; // can break here, both messageUnifiableEventEnabled and
						// parameterUnifiableEventEnabled are true and will not
						// be set to false again.
			}
		}
		return messageUnifiableEventEnabled && !parameterUnifiableEventEnabled;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void calculateRelevantEvents() {

		getEnabledEvents().clear();
		getViolatingEvents().clear();
		getHotForbiddenEvents().clear();
		getColdForbiddenEvents().clear();

		initializeRelevantEventsForActiveProcess();

		if (logger.isDebugEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Relevant events for cut: " + this
						+ " --- the cut is hot? -> "
						+ getCurrentState().isHot());
			}
			for (MessageEvent messageEvent : getEnabledEvents().values()) {
				if (logger.isDebugEnabled()) {
					logger.debug("enabled event: "
							+ RuntimeUtil.Helper
									.getMessageEventString(messageEvent));
				}
			}
			for (MessageEvent messageEvent : getViolatingEvents().values()) {
				if (logger.isDebugEnabled()) {
					logger.debug("violating event: "
							+ RuntimeUtil.Helper
									.getMessageEventString(messageEvent));
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected void initializeRelevantEventsForActiveProcess() {

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveMSDProgress progressEnabledHiddenEvents() {
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isInHotCut() {
		return getCurrentState().isHot();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isInExecutedCut() {
		return getCurrentState().isExecuted();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void assignValue(OCL ocl, String variableName, Object value) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Object assignValueExpression(OCL ocl, String variableName,
			String valueOCLExpression) throws SemanticException,
			ParserException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isRelavant(MessageEvent messageEvent) {
		return (getEnabledEvents().values().contains(messageEvent) || getEnabledEvents().values().contains(messageEvent.getAbstractMessageEvent())
				|| getViolatingEvents().values().contains(messageEvent)
				|| getHotForbiddenEvents().values().contains(messageEvent) || getColdForbiddenEvents()
				.values().contains(messageEvent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
				return ((InternalEList<?>)getColdForbiddenEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
				return ((InternalEList<?>)getHotForbiddenEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
				return ((InternalEList<?>)getViolatingEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				return ((InternalEList<?>)getEnabledEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS:
				if (resolve) return getVariableValuations();
				return basicGetVariableValuations();
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
				if (coreType) return getColdForbiddenEvents();
				else return getColdForbiddenEvents().map();
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
				if (coreType) return getHotForbiddenEvents();
				else return getHotForbiddenEvents().map();
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
				if (coreType) return getViolatingEvents();
				else return getViolatingEvents().map();
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				if (coreType) return getEnabledEvents();
				else return getEnabledEvents().map();
			case RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL:
				if (resolve) return getRuntimeUtil();
				return basicGetRuntimeUtil();
			case RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE:
				if (resolve) return getCurrentState();
				return basicGetCurrentState();
			case RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM:
				if (resolve) return getObjectSystem();
				return basicGetObjectSystem();
			case RuntimePackage.ACTIVE_PROCESS__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS:
				setVariableValuations((ActiveMSDVariableValuations)newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
				((EStructuralFeature.Setting)getColdForbiddenEvents()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
				((EStructuralFeature.Setting)getHotForbiddenEvents()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
				((EStructuralFeature.Setting)getViolatingEvents()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				((EStructuralFeature.Setting)getEnabledEvents()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE:
				setCurrentState((ActiveState)newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM:
				setObjectSystem((MSDObjectSystem)newValue);
				return;
			case RuntimePackage.ACTIVE_PROCESS__ID:
				setId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS:
				setVariableValuations((ActiveMSDVariableValuations)null);
				return;
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
				getColdForbiddenEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
				getHotForbiddenEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
				getViolatingEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				getEnabledEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)null);
				return;
			case RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE:
				setCurrentState((ActiveState)null);
				return;
			case RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM:
				setObjectSystem((MSDObjectSystem)null);
				return;
			case RuntimePackage.ACTIVE_PROCESS__ID:
				setId(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PROCESS__VARIABLE_VALUATIONS:
				return variableValuations != null;
			case RuntimePackage.ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS:
				return coldForbiddenEvents != null && !coldForbiddenEvents.isEmpty();
			case RuntimePackage.ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS:
				return hotForbiddenEvents != null && !hotForbiddenEvents.isEmpty();
			case RuntimePackage.ACTIVE_PROCESS__VIOLATING_EVENTS:
				return violatingEvents != null && !violatingEvents.isEmpty();
			case RuntimePackage.ACTIVE_PROCESS__ENABLED_EVENTS:
				return enabledEvents != null && !enabledEvents.isEmpty();
			case RuntimePackage.ACTIVE_PROCESS__RUNTIME_UTIL:
				return runtimeUtil != null;
			case RuntimePackage.ACTIVE_PROCESS__CURRENT_STATE:
				return currentState != null;
			case RuntimePackage.ACTIVE_PROCESS__OBJECT_SYSTEM:
				return objectSystem != null;
			case RuntimePackage.ACTIVE_PROCESS__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

	public Object getVariableValue(String varName) {
		Object value=null;
		value = getVariableValuations()
				.getVariableNameToEObjectValueMap().get(varName);
		if (value == null)
			value = getVariableValuations()
					.getVariableNameToBooleanValueMap().get(varName);
		if (value == null)
			value = getVariableValuations()
					.getVariableNameToIntegerValueMap().get(varName);
		if (value == null)
			value = getVariableValuations()
					.getVariableNameToStringValueMap().get(varName);
		return value;
	}

	public void assignValueToVariable(String variableName,
			Object value) {
		if (value instanceof EObject){
			getVariableValuations().getVariableNameToEObjectValueMap().put(variableName, (EObject) value);
		}else if (value instanceof String){
			getVariableValuations().getVariableNameToStringValueMap().put(variableName, (String) value);
		}else if (value instanceof Integer){
			getVariableValuations().getVariableNameToIntegerValueMap().put(variableName, (Integer) value);
		}else if (value instanceof Boolean){
			getVariableValuations().getVariableNameToBooleanValueMap().put(variableName, (Boolean) value);
		}
	}
	
	/**
	 * @generated NOT
	 */
	public ActiveProcessKeyWrapper getKeyWrapper(){
		return null;
	}


} // ActiveProcessImpl
