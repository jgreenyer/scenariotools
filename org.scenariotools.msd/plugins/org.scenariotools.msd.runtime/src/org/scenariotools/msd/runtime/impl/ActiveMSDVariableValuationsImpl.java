/**
 */
package org.scenariotools.msd.runtime.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSD Variable Valuations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl#getVariableNameToEObjectValueMap <em>Variable Name To EObject Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl#getVariableNameToStringValueMap <em>Variable Name To String Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl#getVariableNameToIntegerValueMap <em>Variable Name To Integer Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl#getVariableNameToBooleanValueMap <em>Variable Name To Boolean Value Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSDVariableValuationsImpl extends RuntimeEObjectImpl implements ActiveMSDVariableValuations {
	/**
	 * The cached value of the '{@link #getVariableNameToEObjectValueMap() <em>Variable Name To EObject Value Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableNameToEObjectValueMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> variableNameToEObjectValueMap;

	/**
	 * The cached value of the '{@link #getVariableNameToStringValueMap() <em>Variable Name To String Value Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableNameToStringValueMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> variableNameToStringValueMap;

	/**
	 * The cached value of the '{@link #getVariableNameToIntegerValueMap() <em>Variable Name To Integer Value Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableNameToIntegerValueMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Integer> variableNameToIntegerValueMap;

	/**
	 * The cached value of the '{@link #getVariableNameToBooleanValueMap() <em>Variable Name To Boolean Value Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableNameToBooleanValueMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Boolean> variableNameToBooleanValueMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSDVariableValuationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSD_VARIABLE_VALUATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, EObject> getVariableNameToEObjectValueMap() {
		if (variableNameToEObjectValueMap == null) {
			variableNameToEObjectValueMap = new EcoreEMap<String,EObject>(RuntimePackage.Literals.STRING_TO_EOBJECT_MAP_ENTRY, StringToEObjectMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP);
		}
		return variableNameToEObjectValueMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getVariableNameToStringValueMap() {
		if (variableNameToStringValueMap == null) {
			variableNameToStringValueMap = new EcoreEMap<String,String>(RuntimePackage.Literals.STRING_TO_STRING_MAP_ENTRY, StringToStringMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP);
		}
		return variableNameToStringValueMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, Integer> getVariableNameToIntegerValueMap() {
		if (variableNameToIntegerValueMap == null) {
			variableNameToIntegerValueMap = new EcoreEMap<String,Integer>(RuntimePackage.Literals.STRING_TO_INTEGER_MAP_ENTRY, StringToIntegerMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP);
		}
		return variableNameToIntegerValueMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, Boolean> getVariableNameToBooleanValueMap() {
		if (variableNameToBooleanValueMap == null) {
			variableNameToBooleanValueMap = new EcoreEMap<String,Boolean>(RuntimePackage.Literals.STRING_TO_BOOLEAN_MAP_ENTRY, StringToBooleanMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP);
		}
		return variableNameToBooleanValueMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP:
				return ((InternalEList<?>)getVariableNameToEObjectValueMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP:
				return ((InternalEList<?>)getVariableNameToStringValueMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP:
				return ((InternalEList<?>)getVariableNameToIntegerValueMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP:
				return ((InternalEList<?>)getVariableNameToBooleanValueMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP:
				if (coreType) return getVariableNameToEObjectValueMap();
				else return getVariableNameToEObjectValueMap().map();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP:
				if (coreType) return getVariableNameToStringValueMap();
				else return getVariableNameToStringValueMap().map();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP:
				if (coreType) return getVariableNameToIntegerValueMap();
				else return getVariableNameToIntegerValueMap().map();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP:
				if (coreType) return getVariableNameToBooleanValueMap();
				else return getVariableNameToBooleanValueMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP:
				((EStructuralFeature.Setting)getVariableNameToEObjectValueMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP:
				((EStructuralFeature.Setting)getVariableNameToStringValueMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP:
				((EStructuralFeature.Setting)getVariableNameToIntegerValueMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP:
				((EStructuralFeature.Setting)getVariableNameToBooleanValueMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP:
				getVariableNameToEObjectValueMap().clear();
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP:
				getVariableNameToStringValueMap().clear();
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP:
				getVariableNameToIntegerValueMap().clear();
				return;
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP:
				getVariableNameToBooleanValueMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP:
				return variableNameToEObjectValueMap != null && !variableNameToEObjectValueMap.isEmpty();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP:
				return variableNameToStringValueMap != null && !variableNameToStringValueMap.isEmpty();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP:
				return variableNameToIntegerValueMap != null && !variableNameToIntegerValueMap.isEmpty();
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP:
				return variableNameToBooleanValueMap != null && !variableNameToBooleanValueMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActiveMSDVariableValuationsImpl
