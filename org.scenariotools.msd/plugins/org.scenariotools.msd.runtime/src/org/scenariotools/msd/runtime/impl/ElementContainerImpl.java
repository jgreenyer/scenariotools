/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.InteractionFragment;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDCutKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.msd.runtime.plugin.Activator;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Element Container</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveProcesses <em>Active Processes</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSDVariableValuations <em>Active MSD Variable Valuations</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveStates <em>Active States</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSDLifelineBindings <em>Active MSD Lifeline Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSSRoleBindings <em>Active MSS Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveProcessKeyWrapperToActiveProcessMap <em>Active Process Key Wrapper To Active Process Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveStateKeyWrapperToActiveStateMap <em>Active State Key Wrapper To Active State Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry <em>Object System Key Wrapper To MSD Object System Map Entry</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getObjectSystems <em>Object Systems</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getFinalFragment <em>Final Fragment</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveRoleBindings <em>Active Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementContainerImpl extends RuntimeEObjectImpl implements ElementContainer {
	
	private static Logger logger = Activator.getLogManager().getLogger(
			ElementContainerImpl.class.getName());
	
	/**
	 * The cached value of the '{@link #getActiveProcesses()
	 * <em>Active Processes</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveProcesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> activeProcesses;

	/**
	 * The cached value of the '{@link #getActiveMSDVariableValuations() <em>Active MSD Variable Valuations</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActiveMSDVariableValuations()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMSDVariableValuations> activeMSDVariableValuations;

	/**
	 * The cached value of the '{@link #getActiveStates() <em>Active States</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getActiveStates()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveState> activeStates;

	/**
	 * The cached value of the '{@link #getActiveMSDLifelineBindings()
	 * <em>Active MSD Lifeline Bindings</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveMSDLifelineBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMSDLifelineBindings> activeMSDLifelineBindings;

	/**
	 * The cached value of the '{@link #getActiveMSDLifelineBindings()
	 * <em>Active MSD Lifeline Bindings</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveMSDLifelineBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMSSRoleBindings> activeMSSRoleBindings;

	/**
	 * The cached value of the '
	 * {@link #getActiveProcessKeyWrapperToActiveProcessMap()
	 * <em>Active Process Key Wrapper To Active Process Map</em>}' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveProcessKeyWrapperToActiveProcessMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveProcessKeyWrapper, ActiveProcess> activeProcessKeyWrapperToActiveProcessMap;

	/**
	 * The cached value of the '
	 * {@link #getActiveStateKeyWrapperToActiveStateMap()
	 * <em>Active State Key Wrapper To Active State Map</em>}' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveStateKeyWrapperToActiveStateMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveStateKeyWrapper, ActiveState> activeStateKeyWrapperToActiveStateMap;

	/**
	 * The cached value of the '{@link #getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap() <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap;

	/**
	 * The cached value of the '{@link #getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap() <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap;

	/**
	 * The cached value of the '{@link #getObjectSystemKeyWrapperToMSDObjectSystemMapEntry() <em>Object System Key Wrapper To MSD Object System Map Entry</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @generated
	 * @ordered
	 */
	protected EMap<ObjectSystemKeyWrapper, MSDObjectSystem> objectSystemKeyWrapperToMSDObjectSystemMapEntry;

	/**
	 * The cached value of the '{@link #getObjectSystems() <em>Object Systems</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getObjectSystems()
	 * @generated
	 * @ordered
	 */
	protected EList<MSDObjectSystem> objectSystems;

	/**
	 * The cached value of the '{@link #getFinalFragment() <em>Final Fragment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalFragment()
	 * @generated
	 * @ordered
	 */
	protected InteractionFragment finalFragment;

	/**
	 * The cached value of the '{@link #getActiveRoleBindings()
	 * <em>Active Role Bindings</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMSSRoleBindings> activeRoleBindings;

	/**
	 * The cached value of the '{@link #getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap() <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ELEMENT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getActiveProcesses() {
		if (activeProcesses == null) {
			activeProcesses = new EObjectContainmentEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES);
		}
		return activeProcesses;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMSDVariableValuations> getActiveMSDVariableValuations() {
		if (activeMSDVariableValuations == null) {
			activeMSDVariableValuations = new EObjectContainmentEList<ActiveMSDVariableValuations>(ActiveMSDVariableValuations.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS);
		}
		return activeMSDVariableValuations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveState> getActiveStates() {
		if (activeStates == null) {
			activeStates = new EObjectContainmentEList<ActiveState>(ActiveState.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES);
		}
		return activeStates;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMSDLifelineBindings> getActiveMSDLifelineBindings() {
		if (activeMSDLifelineBindings == null) {
			activeMSDLifelineBindings = new EObjectContainmentEList<ActiveMSDLifelineBindings>(ActiveMSDLifelineBindings.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS);
		}
		return activeMSDLifelineBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMSSRoleBindings> getActiveMSSRoleBindings() {
		if (activeMSSRoleBindings == null) {
			activeMSSRoleBindings = new EObjectContainmentEList<ActiveMSSRoleBindings>(ActiveMSSRoleBindings.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS);
		}
		return activeMSSRoleBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveProcessKeyWrapper, ActiveProcess> getActiveProcessKeyWrapperToActiveProcessMap() {
		if (activeProcessKeyWrapperToActiveProcessMap == null) {
			activeProcessKeyWrapperToActiveProcessMap = new EcoreEMap<ActiveProcessKeyWrapper,ActiveProcess>(RuntimePackage.Literals.ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY, ActiveProcessKeyWrapperToActiveProcessMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP);
		}
		return activeProcessKeyWrapperToActiveProcessMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveStateKeyWrapper, ActiveState> getActiveStateKeyWrapperToActiveStateMap() {
		if (activeStateKeyWrapperToActiveStateMap == null) {
			activeStateKeyWrapperToActiveStateMap = new EcoreEMap<ActiveStateKeyWrapper,ActiveState>(RuntimePackage.Literals.ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY, ActiveStateKeyWrapperToActiveStateMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP);
		}
		return activeStateKeyWrapperToActiveStateMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap() {
		if (activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap == null) {
			activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap = new EcoreEMap<ActiveMSDLifelineBindingsKeyWrapper,ActiveMSDLifelineBindings>(RuntimePackage.Literals.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY, ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP);
		}
		return activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap() {
		if (activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap == null) {
			activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap = new EcoreEMap<ActiveMSDVariableValuationsKeyWrapper,ActiveMSDVariableValuations>(RuntimePackage.Literals.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY, ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP);
		}
		return activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ObjectSystemKeyWrapper, MSDObjectSystem> getObjectSystemKeyWrapperToMSDObjectSystemMapEntry() {
		if (objectSystemKeyWrapperToMSDObjectSystemMapEntry == null) {
			objectSystemKeyWrapperToMSDObjectSystemMapEntry = new EcoreEMap<ObjectSystemKeyWrapper,MSDObjectSystem>(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY, ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY);
		}
		return objectSystemKeyWrapperToMSDObjectSystemMapEntry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSDObjectSystem> getObjectSystems() {
		if (objectSystems == null) {
			objectSystems = new EObjectContainmentEList<MSDObjectSystem>(MSDObjectSystem.class, this, RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS);
		}
		return objectSystems;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionFragment getFinalFragment() {
		return finalFragment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFinalFragment(
			InteractionFragment newFinalFragment, NotificationChain msgs) {
		InteractionFragment oldFinalFragment = finalFragment;
		finalFragment = newFinalFragment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT, oldFinalFragment, newFinalFragment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalFragment(InteractionFragment newFinalFragment) {
		if (newFinalFragment != finalFragment) {
			NotificationChain msgs = null;
			if (finalFragment != null)
				msgs = ((InternalEObject)finalFragment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT, null, msgs);
			if (newFinalFragment != null)
				msgs = ((InternalEObject)newFinalFragment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT, null, msgs);
			msgs = basicSetFinalFragment(newFinalFragment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT, newFinalFragment, newFinalFragment));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph getMsdRuntimeStateGraph() {
		if (eContainerFeatureID() != RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH) return null;
		return (MSDRuntimeStateGraph)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMsdRuntimeStateGraph(
			MSDRuntimeStateGraph newMsdRuntimeStateGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMsdRuntimeStateGraph, RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsdRuntimeStateGraph(
			MSDRuntimeStateGraph newMsdRuntimeStateGraph) {
		if (newMsdRuntimeStateGraph != eInternalContainer() || (eContainerFeatureID() != RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH && newMsdRuntimeStateGraph != null)) {
			if (EcoreUtil.isAncestor(this, newMsdRuntimeStateGraph))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMsdRuntimeStateGraph != null)
				msgs = ((InternalEObject)newMsdRuntimeStateGraph).eInverseAdd(this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, MSDRuntimeStateGraph.class, msgs);
			msgs = basicSetMsdRuntimeStateGraph(newMsdRuntimeStateGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH, newMsdRuntimeStateGraph, newMsdRuntimeStateGraph));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMSSRoleBindings> getActiveRoleBindings() {
		if (activeRoleBindings == null) {
			activeRoleBindings = new EObjectContainmentEList<ActiveMSSRoleBindings>(ActiveMSSRoleBindings.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS);
		}
		return activeRoleBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap() {
		if (activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap == null) {
			activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap = new EcoreEMap<ActiveMSSRoleBindingsKeyWrapper,ActiveMSSRoleBindings>(RuntimePackage.Literals.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY, ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP);
		}
		return activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveProcess getActiveProcess(ActiveProcess originalActiveProcess) {
		if (originalActiveProcess instanceof ActiveMSD) {
			return getActiveMSD((ActiveMSD) originalActiveProcess);
		} else if (originalActiveProcess instanceof ActiveMSS) {
			return getActiveMSS((ActiveMSS) originalActiveProcess);
		} else
			throw new IllegalArgumentException();
	}
	
	/**
	 * 
	 * @param originalActiveMSD
	 * @return
	 * 
	 * @generated NOT
	 */
	public ActiveMSD getActiveMSD(ActiveMSD originalActiveMSD) {
		ActiveMSD activeMSD = EcoreUtil.copy(originalActiveMSD);
				
		ActiveMSDVariableValuations lookedUpActiveMSDVariableValuations = getActiveMSDVariableValuations(activeMSD.getVariableValuations());
		activeMSD.setVariableValuations(lookedUpActiveMSDVariableValuations);

		ActiveMSDLifelineBindings lookedUpActiveMSDLifelineBindings = getActiveMSDLifelineBindings(
				activeMSD.getLifelineBindings());
		activeMSD.setLifelineBindings(lookedUpActiveMSDLifelineBindings);
		
		ActiveState lookedUpActiveMSDCut = getActiveState(activeMSD, activeMSD.getCurrentState());
		activeMSD.setCurrentState(lookedUpActiveMSDCut);

		ActiveProcessKeyWrapper activeProcessKeyWrapper = activeMSD.getKeyWrapper();

		ActiveMSD registeredActiveProcess = (ActiveMSD) getActiveProcessKeyWrapperToActiveProcessMap()
				.get(activeProcessKeyWrapper);

		if (registeredActiveProcess == null) {
			logger.debug(
					"equal active MSD was not already explored, registering and returning copied + modified active MSD.");
			getActiveProcessKeyWrapperToActiveProcessMap().put(activeProcessKeyWrapper, activeMSD);
			getActiveProcesses().add(activeMSD);
			return activeMSD;
		} else {
			logger.debug("equal active MSD was already explored, return registered active MSD.");
			return registeredActiveProcess;
		}
	}
	
	/**
	 * 
	 * @param originalActiveMSS
	 * @return
	 * 
	 * @generated NOT
	 */
	public ActiveMSS getActiveMSS(ActiveMSS originalActiveMSS) {
		ActiveMSS activeMSS = EcoreUtil.copy(originalActiveMSS);
		ActiveState lookedUpActiveMSDCut = getActiveState(activeMSS.getCurrentState());
		activeMSS.setCurrentState(lookedUpActiveMSDCut);

		ActiveMSSRoleBindings lookedUpActiveMSSRoleBindings = getActiveMSSRoleBindings(activeMSS.getRoleBindings());
		activeMSS.setRoleBindings(lookedUpActiveMSSRoleBindings);

		ActiveProcessKeyWrapper activeProcessKeyWrapper = activeMSS.getKeyWrapper();

		ActiveMSS registeredActiveProcess = (ActiveMSS) getActiveProcessKeyWrapperToActiveProcessMap()
				.get(activeProcessKeyWrapper);

		if (registeredActiveProcess == null) {
			logger.debug(
					"equal active MSD was not already explored, registering and returning copied + modified active MSD.");
			getActiveProcessKeyWrapperToActiveProcessMap().put(activeProcessKeyWrapper, activeMSS);
			getActiveProcesses().add(activeMSS);
			return activeMSS;
		} else {
			logger.debug("equal active MSD was already explored, return registered active MSD.");
			return registeredActiveProcess;
		}
	}

//	public ActiveProcess getActiveProcess(ActiveProcess originalActiveProcess) {
//		// EList<EObject> activeMSDConstituentsList = new
//		// BasicEList.FastCompare<EObject>();
//		
//		ActiveProcess activeProcess = EcoreUtil.copy(originalActiveProcess);
//		ActiveState lookedUpActiveMSDCut = getActiveState(activeProcess
//				.getCurrentState());
//		activeProcess.setCurrentState(lookedUpActiveMSDCut);
//		ActiveMSDVariableValuations lookedUpActiveMSDVariableValuations = getActiveMSDVariableValuations(activeProcess
//				.getVariableValuations());
//		activeProcess
//				.setVariableValuations(lookedUpActiveMSDVariableValuations);
//
//		ActiveProcessKeyWrapper activeProcessKeyWrapper;
//
//		if (activeProcess instanceof ActiveMSD) {
//			ActiveMSDLifelineBindings lookedUpActiveMSDLifelineBindings = getActiveMSDLifelineBindings(((ActiveMSD) activeProcess)
//					.getLifelineBindings());
//			((ActiveMSD) activeProcess)
//					.setLifelineBindings(lookedUpActiveMSDLifelineBindings);
//		} else if (activeProcess instanceof ActiveMSS) {
//			ActiveMSSRoleBindings lookedUpActiveMSSRoleBindings = getActiveMSSRoleBindings(((ActiveMSS) activeProcess)
//					.getRoleBindings());
//			((ActiveMSS) activeProcess)
//					.setRoleBindings(lookedUpActiveMSSRoleBindings);
//		} else
//			throw new IllegalArgumentException();
//		activeProcessKeyWrapper = activeProcess.getKeyWrapper();
//
//		ActiveProcess registeredActiveProcess = getActiveProcessKeyWrapperToActiveProcessMap()
//				.get(activeProcessKeyWrapper);
//
//		if (registeredActiveProcess == null) {
//			logger.debug("equal active MSD was not already explored, registering and returning copied + modified active MSD.");
//			getActiveProcessKeyWrapperToActiveProcessMap().put(
//					activeProcessKeyWrapper, activeProcess);
//			getActiveProcesses().add(activeProcess);
//			return activeProcess;
//		} else {
//			logger.debug("equal active MSD was already explored, return registered active MSD.");
//			return registeredActiveProcess;
//		}
//
//	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private ActiveMSSRoleBindings getActiveMSSRoleBindings(
			ActiveMSSRoleBindings activeMSSRoleBindings) {

		ActiveMSSRoleBindingsKeyWrapper activeMSSRoleBindingsKeyWrapper = new ActiveMSSRoleBindingsKeyWrapper(
				activeMSSRoleBindings);

		ActiveMSSRoleBindings registeredRoleBindings = getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()
				.get(activeMSSRoleBindingsKeyWrapper);

		if (registeredRoleBindings == null) {
			logger.debug("equal lifeline bindings was not already explored, registering and returning copied + modified lifeline bindings.");
			getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap().put(
					activeMSSRoleBindingsKeyWrapper, activeMSSRoleBindings);
			getActiveMSSRoleBindings().add(activeMSSRoleBindings);
			return activeMSSRoleBindings;
		} else {
			logger.debug("equal lifeline bindings was already explored, return registered lifeline bindings.");
			return registeredRoleBindings;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveState getActiveState(ActiveState activeState) {
		ActiveStateKeyWrapper activeStateKeyWrapper;
		if (activeState instanceof ActiveMSDCut){
			activeStateKeyWrapper = new ActiveMSDCutKeyWrapper(
					(ActiveMSDCut) activeState);
			System.err.println("b�m");
		}
		
		else if (activeState instanceof ActiveMSSState)
			activeStateKeyWrapper = new ActiveMSSStateKeyWrapper(
					(ActiveMSSState) activeState);
		else
			throw new IllegalArgumentException();

		ActiveState registeredState = getActiveStateKeyWrapperToActiveStateMap()
				.get(activeStateKeyWrapper);
//		if (activeState instanceof ActiveMSSState) {
//			System.out.println("activeStateKeyWrapper.hashCode()="
//					+ activeStateKeyWrapper.hashCode());
//			System.out.println("hash code of uml state: "
//					+ ((ActiveMSSState) activeState).getUmlState().hashCode());
//		}
//
//		if (activeState instanceof ActiveMSDCut) {
//			System.out.println("activeStateKeyWrapper.hashCode()="
//					+ activeStateKeyWrapper.hashCode());
//			System.out.println("hash code of interaction fragment map: "
//					+ ((ActiveMSDCut) activeState).getLifelineToInteractionFragmentMap().hashCode());
//		}

		if (registeredState == null) {
			logger.debug("equal cut was not already explored, registering and returning copied + modified cut.");
			getActiveStateKeyWrapperToActiveStateMap().put(
					activeStateKeyWrapper, activeState);
			getActiveStates().add(activeState);
			return activeState;
		} else {
			logger.debug("equal cut was already explored, return registered cut.");
			return registeredState;
		}
	}
	
	@Override
	public ActiveState getActiveState(ActiveMSD activeMSD, ActiveMSDCut activeState) {
		ActiveStateKeyWrapper activeStateKeyWrapper;
		if (activeState instanceof ActiveMSDCut)
			activeStateKeyWrapper = new ActiveMSDCutKeyWrapper(activeMSD,
					(ActiveMSDCut) activeState);
		else if (activeState instanceof ActiveMSSState)
			activeStateKeyWrapper = new ActiveMSSStateKeyWrapper(
					(ActiveMSSState) activeState);
		else
			throw new IllegalArgumentException();

		ActiveState registeredState = getActiveStateKeyWrapperToActiveStateMap()
				.get(activeStateKeyWrapper);
//		if (activeState instanceof ActiveMSSState) {
//			System.out.println("activeStateKeyWrapper.hashCode()="
//					+ activeStateKeyWrapper.hashCode());
//			System.out.println("hash code of uml state: "
//					+ ((ActiveMSSState) activeState).getUmlState().hashCode());
//		}
//
//		if (activeState instanceof ActiveMSDCut) {
//			System.out.println("activeStateKeyWrapper.hashCode()="
//					+ activeStateKeyWrapper.hashCode());
//			System.out.println("hash code of interaction fragment map: "
//					+ ((ActiveMSDCut) activeState).getLifelineToInteractionFragmentMap().hashCode());
//		}

		if (registeredState == null) {
			logger.debug("equal cut was not already explored, registering and returning copied + modified cut.");
			getActiveStateKeyWrapperToActiveStateMap().put(
					activeStateKeyWrapper, activeState);
			getActiveStates().add(activeState);
			return activeState;
		} else {
			logger.debug("equal cut was already explored, return registered cut.");
			return registeredState;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveMSDLifelineBindings getActiveMSDLifelineBindings(
			ActiveMSDLifelineBindings activeMSDLifelineBindings) {

		ActiveMSDLifelineBindingsKeyWrapper activeMSDLifelineBindingsKeyWrapper = new ActiveMSDLifelineBindingsKeyWrapper(
				activeMSDLifelineBindings);

		ActiveMSDLifelineBindings registeredLifelineBindings = getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()
				.get(activeMSDLifelineBindingsKeyWrapper);

		if (registeredLifelineBindings == null) {
			logger.debug("equal lifeline bindings was not already explored, registering and returning copied + modified lifeline bindings.");
			getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()
					.put(activeMSDLifelineBindingsKeyWrapper,
							activeMSDLifelineBindings);
			getActiveMSDLifelineBindings().add(activeMSDLifelineBindings);
			return activeMSDLifelineBindings;
		} else {
			logger.debug("equal lifeline bindings was already explored, return registered lifeline bindings.");
			return registeredLifelineBindings;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveMSDVariableValuations getActiveMSDVariableValuations(
			ActiveMSDVariableValuations activeMSDVariableValuations) {

		ActiveMSDVariableValuationsKeyWrapper activeMSDVariableValuationsKeyWrapper = new ActiveMSDVariableValuationsKeyWrapper(
				activeMSDVariableValuations);

		ActiveMSDVariableValuations registeredActiveMSDVariableValuations = getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()
				.get(activeMSDVariableValuationsKeyWrapper);

		if (registeredActiveMSDVariableValuations == null) {
			logger.debug("equal variable valuations was not already explored, registering and returning copied + modified variable valuations.");
			getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()
					.put(activeMSDVariableValuationsKeyWrapper,
							activeMSDVariableValuations);
			getActiveMSDVariableValuations().add(activeMSDVariableValuations);
			return activeMSDVariableValuations;
		} else {
			logger.debug("equal variable valuations was already explored, return registered variable valuations.");
			return registeredActiveMSDVariableValuations;
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public MSDObjectSystem getObjectSystem(MSDObjectSystem msdObjectSystem) {

		if (!getMsdRuntimeStateGraph().getScenarioRunConfiguration()
				.isDynamicObjectSystem()) {
			if (!getObjectSystems().contains(msdObjectSystem))
				getObjectSystems().add(msdObjectSystem);
			Assert.isTrue(
					getObjectSystems().size() == 1,
					"The scenario run configuration specifies that we are dealing with a static object system, so there must not be more than one object system added to the element container.");
			return msdObjectSystem;
		} else {

			ObjectSystemKeyWrapper objectSystemKeyWrapper = new ObjectSystemKeyWrapper(
					msdObjectSystem.getRootObjects());

			MSDObjectSystem registeredMSDMsdObjectSystem = getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
					.get(objectSystemKeyWrapper);

			if (registeredMSDMsdObjectSystem == null) {
				logger.debug("an identical object system was not registered yet.");
				getObjectSystemKeyWrapperToMSDObjectSystemMapEntry().put(
						objectSystemKeyWrapper, msdObjectSystem);
				getObjectSystems().add(msdObjectSystem);
				for (EObject rootObject : msdObjectSystem.getRootObjects()) {
					//System.out.println("rootObject.eContainer() ? " + rootObject.eContainer());
					//System.out.println("rootObject.eResource() ? " + rootObject.eResource());
					if (rootObject.eContainer() == null
							&& rootObject.eResource() == null) {
						msdObjectSystem.getRootObjectsContained().add(
								rootObject);
					}
				}
				return msdObjectSystem;
			} else {
				logger.debug("an identical object system was already registered.");
				return registeredMSDMsdObjectSystem;
			}
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMsdRuntimeStateGraph((MSDRuntimeStateGraph)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
				return ((InternalEList<?>)getActiveProcesses()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
				return ((InternalEList<?>)getActiveMSDVariableValuations()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
				return ((InternalEList<?>)getActiveStates()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
				return ((InternalEList<?>)getActiveMSDLifelineBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
				return ((InternalEList<?>)getActiveMSSRoleBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
				return ((InternalEList<?>)getActiveProcessKeyWrapperToActiveProcessMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
				return ((InternalEList<?>)getActiveStateKeyWrapperToActiveStateMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
				return ((InternalEList<?>)getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
				return ((InternalEList<?>)getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
				return ((InternalEList<?>)getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return ((InternalEList<?>)getObjectSystems()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
				return basicSetFinalFragment(null, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				return basicSetMsdRuntimeStateGraph(null, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
				return ((InternalEList<?>)getActiveRoleBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				return ((InternalEList<?>)getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				return eInternalContainer().eInverseRemove(this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, MSDRuntimeStateGraph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
				return getActiveProcesses();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
				return getActiveMSDVariableValuations();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
				return getActiveStates();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
				return getActiveMSDLifelineBindings();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
				return getActiveMSSRoleBindings();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
				if (coreType) return getActiveProcessKeyWrapperToActiveProcessMap();
				else return getActiveProcessKeyWrapperToActiveProcessMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
				if (coreType) return getActiveStateKeyWrapperToActiveStateMap();
				else return getActiveStateKeyWrapperToActiveStateMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
				if (coreType) return getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap();
				else return getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
				if (coreType) return getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap();
				else return getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
				if (coreType) return getObjectSystemKeyWrapperToMSDObjectSystemMapEntry();
				else return getObjectSystemKeyWrapperToMSDObjectSystemMapEntry().map();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return getObjectSystems();
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
				return getFinalFragment();
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				return getMsdRuntimeStateGraph();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
				return getActiveRoleBindings();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				if (coreType) return getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap();
				else return getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
				getActiveProcesses().clear();
				getActiveProcesses().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
				getActiveMSDVariableValuations().clear();
				getActiveMSDVariableValuations().addAll((Collection<? extends ActiveMSDVariableValuations>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
				getActiveStates().clear();
				getActiveStates().addAll((Collection<? extends ActiveState>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
				getActiveMSDLifelineBindings().clear();
				getActiveMSDLifelineBindings().addAll((Collection<? extends ActiveMSDLifelineBindings>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
				getActiveMSSRoleBindings().clear();
				getActiveMSSRoleBindings().addAll((Collection<? extends ActiveMSSRoleBindings>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
				((EStructuralFeature.Setting)getActiveProcessKeyWrapperToActiveProcessMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
				((EStructuralFeature.Setting)getActiveStateKeyWrapperToActiveStateMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
				((EStructuralFeature.Setting)getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
				((EStructuralFeature.Setting)getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
				((EStructuralFeature.Setting)getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				getObjectSystems().clear();
				getObjectSystems().addAll((Collection<? extends MSDObjectSystem>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
				setFinalFragment((InteractionFragment)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
				getActiveRoleBindings().clear();
				getActiveRoleBindings().addAll((Collection<? extends ActiveMSSRoleBindings>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				((EStructuralFeature.Setting)getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
				getActiveProcesses().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
				getActiveMSDVariableValuations().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
				getActiveStates().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
				getActiveMSDLifelineBindings().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
				getActiveMSSRoleBindings().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
				getActiveProcessKeyWrapperToActiveProcessMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
				getActiveStateKeyWrapperToActiveStateMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
				getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
				getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
				getObjectSystemKeyWrapperToMSDObjectSystemMapEntry().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				getObjectSystems().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
				setFinalFragment((InteractionFragment)null);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)null);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
				getActiveRoleBindings().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESSES:
				return activeProcesses != null && !activeProcesses.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS:
				return activeMSDVariableValuations != null && !activeMSDVariableValuations.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATES:
				return activeStates != null && !activeStates.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS:
				return activeMSDLifelineBindings != null && !activeMSDLifelineBindings.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS:
				return activeMSSRoleBindings != null && !activeMSSRoleBindings.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP:
				return activeProcessKeyWrapperToActiveProcessMap != null && !activeProcessKeyWrapperToActiveProcessMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP:
				return activeStateKeyWrapperToActiveStateMap != null && !activeStateKeyWrapperToActiveStateMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP:
				return activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap != null && !activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP:
				return activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap != null && !activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY:
				return objectSystemKeyWrapperToMSDObjectSystemMapEntry != null && !objectSystemKeyWrapperToMSDObjectSystemMapEntry.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return objectSystems != null && !objectSystems.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__FINAL_FRAGMENT:
				return finalFragment != null;
			case RuntimePackage.ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH:
				return getMsdRuntimeStateGraph() != null;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS:
				return activeRoleBindings != null && !activeRoleBindings.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP:
				return activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap != null && !activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	

} // ElementContainerImpl
