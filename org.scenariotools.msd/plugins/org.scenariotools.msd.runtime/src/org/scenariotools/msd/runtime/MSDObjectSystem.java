/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.ObjectSystem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Object System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingEnvironmentMSDModalMessageEvents <em>Initializing Environment MSD Modal Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingSystemMSDModalMessageEvents <em>Initializing System MSD Modal Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getAssumptionMSDInitializingMessageEvents <em>Assumption MSD Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRequirementMSDInitializingMessageEvents <em>Requirement MSD Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToSendableEOperationMap <em>EObject To Sendable EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToReceivableEOperationMap <em>EObject To Receivable EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEClassToInstanceEObjectMap <em>EClass To Instance EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectsToUMLClassesItIsInstanceOfMap <em>EObjects To UML Classes It Is Instance Of Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getStaticLifelineToEObjectBindings <em>Static Lifeline To EObject Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getStaticRoleToEObjectBindings <em>Static Role To EObject Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRolesToEObjectsMappingContainer <em>Roles To EObjects Mapping Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventContainer <em>Message Event Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventKeyWrapperToMessageEventMap <em>Message Event Key Wrapper To Message Event Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem()
 * @model
 * @generated
 */
public interface MSDObjectSystem extends ObjectSystem {

	/**
	 * Returns the value of the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Util</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Util</em>' reference.
	 * @see #setRuntimeUtil(RuntimeUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_RuntimeUtil()
	 * @model transient="true"
	 * @generated
	 */
	RuntimeUtil getRuntimeUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRuntimeUtil <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Util</em>' reference.
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	void setRuntimeUtil(RuntimeUtil value);

	/**
	 * Returns the value of the '<em><b>Initializing Environment MSD Modal Message Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.MSDModalMessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initializing Environment MSD Modal Message Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initializing Environment MSD Modal Message Events</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_InitializingEnvironmentMSDModalMessageEvents()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<MSDModalMessageEvent> getInitializingEnvironmentMSDModalMessageEvents();

	/**
	 * Returns the value of the '<em><b>Initializing System MSD Modal Message Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.MSDModalMessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initializing System MSD Modal Message Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initializing System MSD Modal Message Events</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_InitializingSystemMSDModalMessageEvents()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<MSDModalMessageEvent> getInitializingSystemMSDModalMessageEvents();

	/**
	 * Returns the value of the '<em><b>Assumption MSD Initializing Message Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assumption MSD Initializing Message Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assumption MSD Initializing Message Events</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_AssumptionMSDInitializingMessageEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getAssumptionMSDInitializingMessageEvents();

	/**
	 * Returns the value of the '<em><b>Requirement MSD Initializing Message Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement MSD Initializing Message Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement MSD Initializing Message Events</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_RequirementMSDInitializingMessageEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getRequirementMSDInitializingMessageEvents();

	/**
	 * Returns the value of the '<em><b>EObject To Sendable EOperation Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.EOperation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject To Sendable EOperation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject To Sendable EOperation Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_EObjectToSendableEOperationMap()
	 * @model mapType="org.scenariotools.msd.runtime.EObjectToEOperationMapEntry<org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EOperation>" transient="true"
	 * @generated
	 */
	EMap<EObject, EList<EOperation>> getEObjectToSendableEOperationMap();

	/**
	 * Returns the value of the '<em><b>EObject To Receivable EOperation Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.EOperation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject To Receivable EOperation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject To Receivable EOperation Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_EObjectToReceivableEOperationMap()
	 * @model mapType="org.scenariotools.msd.runtime.EObjectToEOperationMapEntry<org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EOperation>" transient="true"
	 * @generated
	 */
	EMap<EObject, EList<EOperation>> getEObjectToReceivableEOperationMap();

	/**
	 * Returns the value of the '<em><b>EClass To Instance EObject Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass To Instance EObject Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass To Instance EObject Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_EClassToInstanceEObjectMap()
	 * @model mapType="org.scenariotools.msd.runtime.EClassToEObjectMapEntry<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EObject>" transient="true"
	 * @generated
	 */
	EMap<EClass, EList<EObject>> getEClassToInstanceEObjectMap();

	/**
	 * Returns the value of the '<em><b>EObjects To UML Classes It Is Instance Of Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Class},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObjects To UML Classes It Is Instance Of Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObjects To UML Classes It Is Instance Of Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_EObjectsToUMLClassesItIsInstanceOfMap()
	 * @model mapType="org.scenariotools.msd.runtime.EObjectToUMLClassMapEntry<org.eclipse.emf.ecore.EObject, org.eclipse.uml2.uml.Class>" transient="true"
	 * @generated
	 */
	EMap<EObject, EList<org.eclipse.uml2.uml.Class>> getEObjectsToUMLClassesItIsInstanceOfMap();

	/**
	 * Returns the value of the '<em><b>Static Lifeline To EObject Bindings</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Lifeline},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Lifeline To EObject Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Lifeline To EObject Bindings</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_StaticLifelineToEObjectBindings()
	 * @model mapType="org.scenariotools.msd.runtime.LifelineToEObjectMapEntry<org.eclipse.uml2.uml.Lifeline, org.eclipse.emf.ecore.EObject>" transient="true"
	 * @generated
	 */
	EMap<Lifeline, EObject> getStaticLifelineToEObjectBindings();

	/**
	 * Returns the value of the '<em><b>Static Role To EObject Bindings</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Property},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Role To EObject Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Role To EObject Bindings</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_StaticRoleToEObjectBindings()
	 * @model mapType="org.scenariotools.msd.runtime.PropertyToEObjectMapEntry<org.eclipse.uml2.uml.Property, org.eclipse.emf.ecore.EObject>" transient="true"
	 * @generated
	 */
	EMap<Property, EObject> getStaticRoleToEObjectBindings();

	/**
	 * Returns the value of the '<em><b>Roles To EObjects Mapping Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles To EObjects Mapping Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles To EObjects Mapping Container</em>' containment reference.
	 * @see #setRolesToEObjectsMappingContainer(RoleToEObjectMappingContainer)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_RolesToEObjectsMappingContainer()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	RoleToEObjectMappingContainer getRolesToEObjectsMappingContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRolesToEObjectsMappingContainer <em>Roles To EObjects Mapping Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roles To EObjects Mapping Container</em>' containment reference.
	 * @see #getRolesToEObjectsMappingContainer()
	 * @generated
	 */
	void setRolesToEObjectsMappingContainer(RoleToEObjectMappingContainer value);

	/**
	 * Returns the value of the '<em><b>Message Event Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Container</em>' containment reference.
	 * @see #setMessageEventContainer(MessageEventContainer)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_MessageEventContainer()
	 * @model containment="true"
	 * @generated
	 */
	MessageEventContainer getMessageEventContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventContainer <em>Message Event Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Event Container</em>' containment reference.
	 * @see #getMessageEventContainer()
	 * @generated
	 */
	void setMessageEventContainer(MessageEventContainer value);

	/**
	 * Returns the value of the '<em><b>Message Event Key Wrapper To Message Event Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper},
	 * and the value is of type {@link org.scenariotools.events.MessageEvent},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Key Wrapper To Message Event Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Key Wrapper To Message Event Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDObjectSystem_MessageEventKeyWrapperToMessageEventMap()
	 * @model mapType="org.scenariotools.msd.runtime.MessageEventKeyWrapperToMessageEventMapEntry<org.scenariotools.msd.runtime.MessageEventKeyWrapper, org.scenariotools.events.MessageEvent>" transient="true"
	 * @generated
	 */
	EMap<MessageEventKeyWrapper, MessageEvent> getMessageEventKeyWrapperToMessageEventMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean lifelineMayBindToEObject(Lifeline lifeline, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean roleMayBindToEObject(Property role, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model objectSystemRootObjectsMany="false"
	 * @generated
	 */
	void init(EList<EObject> objectSystemRootObjects, RoleToEObjectMappingContainer rolesToEObjectsMappingContainer, RuntimeUtil runtimeUtil);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model parameterValueDataType="org.scenariotools.msd.util.PlainJavaObject"
	 * @generated
	 */
	MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model parameterValueDataType="org.scenariotools.msd.util.PlainJavaObject"
	 * @generated
	 */
	MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue, MessageEvent emptyMessageEventInstance);
} // MSDObjectSystem
