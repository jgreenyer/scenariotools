/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MessageEventContainer#getRuntimeMessages <em>Runtime Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MessageEventContainer#getMessageEvents <em>Message Events</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMessageEventContainer()
 * @model
 * @generated
 */
public interface MessageEventContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Runtime Messages</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.events.RuntimeMessage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Messages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Messages</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMessageEventContainer_RuntimeMessages()
	 * @model containment="true"
	 * @generated
	 */
	EList<RuntimeMessage> getRuntimeMessages();

	/**
	 * Returns the value of the '<em><b>Message Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Events</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMessageEventContainer_MessageEvents()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEvent> getMessageEvents();

} // MessageEventContainer
