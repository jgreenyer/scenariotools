/**
 */
package org.scenariotools.msd.runtime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.runtime.impl.ModalityImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSD Modality</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl#isMonitored <em>Monitored</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl#isCold <em>Cold</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl#isInitializing <em>Initializing</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl#isColdViolating <em>Cold Violating</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDModalityImpl extends ModalityImpl implements MSDModality {
	/**
	 * The default value of the '{@link #isMonitored() <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MONITORED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMonitored() <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected boolean monitored = MONITORED_EDEFAULT;

	/**
	 * The default value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected boolean hot = HOT_EDEFAULT;

	/**
	 * The default value of the '{@link #isCold() <em>Cold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCold()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COLD_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCold() <em>Cold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCold()
	 * @generated
	 * @ordered
	 */
	protected boolean cold = COLD_EDEFAULT;

	/**
	 * The default value of the '{@link #isInitializing() <em>Initializing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitializing()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INITIALIZING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInitializing() <em>Initializing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitializing()
	 * @generated
	 * @ordered
	 */
	protected boolean initializing = INITIALIZING_EDEFAULT;

	/**
	 * The default value of the '{@link #isColdViolating() <em>Cold Violating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isColdViolating()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COLD_VIOLATING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isColdViolating() <em>Cold Violating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isColdViolating()
	 * @generated
	 * @ordered
	 */
	protected boolean coldViolating = COLD_VIOLATING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDModalityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MSD_MODALITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMonitored() {
		return monitored;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitored(boolean newMonitored) {
		boolean oldMonitored = monitored;
		monitored = newMonitored;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODALITY__MONITORED, oldMonitored, monitored));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHot() {
		return hot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHot(boolean newHot) {
		boolean oldHot = hot;
		hot = newHot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODALITY__HOT, oldHot, hot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCold() {
		return cold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCold(boolean newCold) {
		boolean oldCold = cold;
		cold = newCold;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODALITY__COLD, oldCold, cold));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInitializing() {
		return initializing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitializing(boolean newInitializing) {
		boolean oldInitializing = initializing;
		initializing = newInitializing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODALITY__INITIALIZING, oldInitializing, initializing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isColdViolating() {
		return coldViolating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColdViolating(boolean newColdViolating) {
		boolean oldColdViolating = coldViolating;
		coldViolating = newColdViolating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODALITY__COLD_VIOLATING, oldColdViolating, coldViolating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MSD_MODALITY__MONITORED:
				return isMonitored();
			case RuntimePackage.MSD_MODALITY__HOT:
				return isHot();
			case RuntimePackage.MSD_MODALITY__COLD:
				return isCold();
			case RuntimePackage.MSD_MODALITY__INITIALIZING:
				return isInitializing();
			case RuntimePackage.MSD_MODALITY__COLD_VIOLATING:
				return isColdViolating();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MSD_MODALITY__MONITORED:
				setMonitored((Boolean)newValue);
				return;
			case RuntimePackage.MSD_MODALITY__HOT:
				setHot((Boolean)newValue);
				return;
			case RuntimePackage.MSD_MODALITY__COLD:
				setCold((Boolean)newValue);
				return;
			case RuntimePackage.MSD_MODALITY__INITIALIZING:
				setInitializing((Boolean)newValue);
				return;
			case RuntimePackage.MSD_MODALITY__COLD_VIOLATING:
				setColdViolating((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_MODALITY__MONITORED:
				setMonitored(MONITORED_EDEFAULT);
				return;
			case RuntimePackage.MSD_MODALITY__HOT:
				setHot(HOT_EDEFAULT);
				return;
			case RuntimePackage.MSD_MODALITY__COLD:
				setCold(COLD_EDEFAULT);
				return;
			case RuntimePackage.MSD_MODALITY__INITIALIZING:
				setInitializing(INITIALIZING_EDEFAULT);
				return;
			case RuntimePackage.MSD_MODALITY__COLD_VIOLATING:
				setColdViolating(COLD_VIOLATING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_MODALITY__MONITORED:
				return monitored != MONITORED_EDEFAULT;
			case RuntimePackage.MSD_MODALITY__HOT:
				return hot != HOT_EDEFAULT;
			case RuntimePackage.MSD_MODALITY__COLD:
				return cold != COLD_EDEFAULT;
			case RuntimePackage.MSD_MODALITY__INITIALIZING:
				return initializing != INITIALIZING_EDEFAULT;
			case RuntimePackage.MSD_MODALITY__COLD_VIOLATING:
				return coldViolating != COLD_VIOLATING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (monitored: ");
		result.append(monitored);
		result.append(", hot: ");
		result.append(hot);
		result.append(", cold: ");
		result.append(cold);
		result.append(", initializing: ");
		result.append(initializing);
		result.append(", coldViolating: ");
		result.append(coldViolating);
		result.append(')');
		return result.toString();
	}

} //MSDModalityImpl
