package org.scenariotools.msd.runtime.keywrapper;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;

public class ActiveMSSRoleBindingsKeyWrapper extends KeyWrapper {
	public ActiveMSSRoleBindingsKeyWrapper(
			ActiveMSSRoleBindings activeMSSRoleBindings) {
		this(activeMSSRoleBindings.getPropertyToEObjectMap());
	}

	public ActiveMSSRoleBindingsKeyWrapper(
			EMap<Property, EObject> propertyToEObjectMap) {
		addSubObject(propertyToEObjectMap);
	}
}
