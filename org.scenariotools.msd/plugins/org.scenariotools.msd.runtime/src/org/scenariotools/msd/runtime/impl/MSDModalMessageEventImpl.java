/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.runtime.impl.ModalMessageEventImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSD Modal Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl#getEnabledInActiveProcess <em>Enabled In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl#getColdViolatingInActiveProcess <em>Cold Violating In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl#getSafetyViolatingInActiveProcess <em>Safety Violating In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl#getParentSymbolicMSDModalMessageEvent <em>Parent Symbolic MSD Modal Message Event</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl#getConcreteMSDModalMessageEvent <em>Concrete MSD Modal Message Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDModalMessageEventImpl extends ModalMessageEventImpl implements MSDModalMessageEvent {
	/**
	 * The cached value of the '{@link #getEnabledInActiveProcess() <em>Enabled In Active Process</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledInActiveProcess()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> enabledInActiveProcess;

	/**
	 * The cached value of the '{@link #getColdViolatingInActiveProcess() <em>Cold Violating In Active Process</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColdViolatingInActiveProcess()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> coldViolatingInActiveProcess;

	/**
	 * The cached value of the '{@link #getSafetyViolatingInActiveProcess() <em>Safety Violating In Active Process</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafetyViolatingInActiveProcess()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> safetyViolatingInActiveProcess;

	/**
	 * The cached value of the '{@link #getParentSymbolicMSDModalMessageEvent() <em>Parent Symbolic MSD Modal Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentSymbolicMSDModalMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected MSDModalMessageEvent parentSymbolicMSDModalMessageEvent;

	/**
	 * The cached value of the '{@link #getConcreteMSDModalMessageEvent() <em>Concrete MSD Modal Message Event</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConcreteMSDModalMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected EList<MSDModalMessageEvent> concreteMSDModalMessageEvent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDModalMessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MSD_MODAL_MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getEnabledInActiveProcess() {
		if (enabledInActiveProcess == null) {
			enabledInActiveProcess = new EObjectResolvingEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS);
		}
		return enabledInActiveProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getColdViolatingInActiveProcess() {
		if (coldViolatingInActiveProcess == null) {
			coldViolatingInActiveProcess = new EObjectResolvingEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS);
		}
		return coldViolatingInActiveProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getSafetyViolatingInActiveProcess() {
		if (safetyViolatingInActiveProcess == null) {
			safetyViolatingInActiveProcess = new EObjectResolvingEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS);
		}
		return safetyViolatingInActiveProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDModalMessageEvent getParentSymbolicMSDModalMessageEvent() {
		if (parentSymbolicMSDModalMessageEvent != null && parentSymbolicMSDModalMessageEvent.eIsProxy()) {
			InternalEObject oldParentSymbolicMSDModalMessageEvent = (InternalEObject)parentSymbolicMSDModalMessageEvent;
			parentSymbolicMSDModalMessageEvent = (MSDModalMessageEvent)eResolveProxy(oldParentSymbolicMSDModalMessageEvent);
			if (parentSymbolicMSDModalMessageEvent != oldParentSymbolicMSDModalMessageEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT, oldParentSymbolicMSDModalMessageEvent, parentSymbolicMSDModalMessageEvent));
			}
		}
		return parentSymbolicMSDModalMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDModalMessageEvent basicGetParentSymbolicMSDModalMessageEvent() {
		return parentSymbolicMSDModalMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentSymbolicMSDModalMessageEvent(MSDModalMessageEvent newParentSymbolicMSDModalMessageEvent, NotificationChain msgs) {
		MSDModalMessageEvent oldParentSymbolicMSDModalMessageEvent = parentSymbolicMSDModalMessageEvent;
		parentSymbolicMSDModalMessageEvent = newParentSymbolicMSDModalMessageEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT, oldParentSymbolicMSDModalMessageEvent, newParentSymbolicMSDModalMessageEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentSymbolicMSDModalMessageEvent(MSDModalMessageEvent newParentSymbolicMSDModalMessageEvent) {
		if (newParentSymbolicMSDModalMessageEvent != parentSymbolicMSDModalMessageEvent) {
			NotificationChain msgs = null;
			if (parentSymbolicMSDModalMessageEvent != null)
				msgs = ((InternalEObject)parentSymbolicMSDModalMessageEvent).eInverseRemove(this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT, MSDModalMessageEvent.class, msgs);
			if (newParentSymbolicMSDModalMessageEvent != null)
				msgs = ((InternalEObject)newParentSymbolicMSDModalMessageEvent).eInverseAdd(this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT, MSDModalMessageEvent.class, msgs);
			msgs = basicSetParentSymbolicMSDModalMessageEvent(newParentSymbolicMSDModalMessageEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT, newParentSymbolicMSDModalMessageEvent, newParentSymbolicMSDModalMessageEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSDModalMessageEvent> getConcreteMSDModalMessageEvent() {
		if (concreteMSDModalMessageEvent == null) {
			concreteMSDModalMessageEvent = new EObjectWithInverseResolvingEList<MSDModalMessageEvent>(MSDModalMessageEvent.class, this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT);
		}
		return concreteMSDModalMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				if (parentSymbolicMSDModalMessageEvent != null)
					msgs = ((InternalEObject)parentSymbolicMSDModalMessageEvent).eInverseRemove(this, RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT, MSDModalMessageEvent.class, msgs);
				return basicSetParentSymbolicMSDModalMessageEvent((MSDModalMessageEvent)otherEnd, msgs);
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConcreteMSDModalMessageEvent()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				return basicSetParentSymbolicMSDModalMessageEvent(null, msgs);
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				return ((InternalEList<?>)getConcreteMSDModalMessageEvent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS:
				return getEnabledInActiveProcess();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS:
				return getColdViolatingInActiveProcess();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS:
				return getSafetyViolatingInActiveProcess();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				if (resolve) return getParentSymbolicMSDModalMessageEvent();
				return basicGetParentSymbolicMSDModalMessageEvent();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				return getConcreteMSDModalMessageEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS:
				getEnabledInActiveProcess().clear();
				getEnabledInActiveProcess().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS:
				getColdViolatingInActiveProcess().clear();
				getColdViolatingInActiveProcess().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS:
				getSafetyViolatingInActiveProcess().clear();
				getSafetyViolatingInActiveProcess().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				setParentSymbolicMSDModalMessageEvent((MSDModalMessageEvent)newValue);
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				getConcreteMSDModalMessageEvent().clear();
				getConcreteMSDModalMessageEvent().addAll((Collection<? extends MSDModalMessageEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS:
				getEnabledInActiveProcess().clear();
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS:
				getColdViolatingInActiveProcess().clear();
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS:
				getSafetyViolatingInActiveProcess().clear();
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				setParentSymbolicMSDModalMessageEvent((MSDModalMessageEvent)null);
				return;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				getConcreteMSDModalMessageEvent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS:
				return enabledInActiveProcess != null && !enabledInActiveProcess.isEmpty();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS:
				return coldViolatingInActiveProcess != null && !coldViolatingInActiveProcess.isEmpty();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS:
				return safetyViolatingInActiveProcess != null && !safetyViolatingInActiveProcess.isEmpty();
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT:
				return parentSymbolicMSDModalMessageEvent != null;
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT:
				return concreteMSDModalMessageEvent != null && !concreteMSDModalMessageEvent.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MSDModalMessageEventImpl
