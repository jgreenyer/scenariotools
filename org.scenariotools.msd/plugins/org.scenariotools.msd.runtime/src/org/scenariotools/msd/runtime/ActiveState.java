/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveState#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveState#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveState#isExecuted <em>Executed</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveState()
 * @model
 * @generated
 */
public interface ActiveState extends EObject {
	/**
	 * Returns the value of the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hot</em>' attribute.
	 * @see #setHot(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveState_Hot()
	 * @model
	 * @generated
	 */
	boolean isHot();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveState#isHot <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hot</em>' attribute.
	 * @see #isHot()
	 * @generated
	 */
	void setHot(boolean value);

	/**
	 * Returns the value of the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Util</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Util</em>' reference.
	 * @see #setRuntimeUtil(RuntimeUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveState_RuntimeUtil()
	 * @model
	 * @generated
	 */
	RuntimeUtil getRuntimeUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveState#getRuntimeUtil <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Util</em>' reference.
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	void setRuntimeUtil(RuntimeUtil value);

	/**
	 * Returns the value of the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executed</em>' attribute.
	 * @see #setExecuted(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveState_Executed()
	 * @model
	 * @generated
	 */
	boolean isExecuted();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveState#isExecuted <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executed</em>' attribute.
	 * @see #isExecuted()
	 * @generated
	 */
	void setExecuted(boolean value);

} // ActiveState
