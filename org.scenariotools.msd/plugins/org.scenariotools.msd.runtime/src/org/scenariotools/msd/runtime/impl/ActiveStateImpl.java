/**
 */
package org.scenariotools.msd.runtime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveStateImpl#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveStateImpl#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveStateImpl#isExecuted <em>Executed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveStateImpl extends RuntimeEObjectImpl implements ActiveState {
	/**
	 * The default value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHot() <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHot()
	 * @generated
	 * @ordered
	 */
	protected boolean hot = HOT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuntimeUtil() <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeUtil()
	 * @generated
	 * @ordered
	 */
	protected RuntimeUtil runtimeUtil;

	/**
	 * The default value of the '{@link #isExecuted() <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecuted()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXECUTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExecuted() <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecuted()
	 * @generated
	 * @ordered
	 */
	protected boolean executed = EXECUTED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHot() {
		return hot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHot(boolean newHot) {
		boolean oldHot = hot;
		hot = newHot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_STATE__HOT, oldHot, hot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil getRuntimeUtil() {
		if (runtimeUtil != null && runtimeUtil.eIsProxy()) {
			InternalEObject oldRuntimeUtil = (InternalEObject)runtimeUtil;
			runtimeUtil = (RuntimeUtil)eResolveProxy(oldRuntimeUtil);
			if (runtimeUtil != oldRuntimeUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
			}
		}
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil basicGetRuntimeUtil() {
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		RuntimeUtil oldRuntimeUtil = runtimeUtil;
		runtimeUtil = newRuntimeUtil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExecuted() {
		return executed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecuted(boolean newExecuted) {
		boolean oldExecuted = executed;
		executed = newExecuted;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_STATE__EXECUTED, oldExecuted, executed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_STATE__HOT:
				return isHot();
			case RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL:
				if (resolve) return getRuntimeUtil();
				return basicGetRuntimeUtil();
			case RuntimePackage.ACTIVE_STATE__EXECUTED:
				return isExecuted();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_STATE__HOT:
				setHot((Boolean)newValue);
				return;
			case RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)newValue);
				return;
			case RuntimePackage.ACTIVE_STATE__EXECUTED:
				setExecuted((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_STATE__HOT:
				setHot(HOT_EDEFAULT);
				return;
			case RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)null);
				return;
			case RuntimePackage.ACTIVE_STATE__EXECUTED:
				setExecuted(EXECUTED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_STATE__HOT:
				return hot != HOT_EDEFAULT;
			case RuntimePackage.ACTIVE_STATE__RUNTIME_UTIL:
				return runtimeUtil != null;
			case RuntimePackage.ACTIVE_STATE__EXECUTED:
				return executed != EXECUTED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hot: ");
		result.append(hot);
		result.append(", executed: ");
		result.append(executed);
		result.append(')');
		return result.toString();
	}

} //ActiveStateImpl
