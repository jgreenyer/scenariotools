/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.runtime/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = org.scenariotools.msd.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl <em>MSD Runtime State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateGraph()
	 * @generated
	 */
	int MSD_RUNTIME_STATE_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__STATES = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__START_STATE = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH__START_STATE;

	/**
	 * The feature id for the '<em><b>Strategy Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__STRATEGY_KIND = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH__STRATEGY_KIND;

	/**
	 * The feature id for the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Key Wrapper To MSD Runtime State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MSD Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_GRAPH_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl <em>MSD Runtime State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeState()
	 * @generated
	 */
	int MSD_RUNTIME_STATE = 1;
	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__OUTGOING_TRANSITION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__INCOMING_TRANSITION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__STATE_GRAPH = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__LABEL = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__LABEL;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ElementContainerImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getElementContainer()
	 * @generated
	 */
	int ELEMENT_CONTAINER = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl <em>MSD Modality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDModalityImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDModality()
	 * @generated
	 */
	int MSD_MODALITY = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl <em>Active Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveProcessImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcess()
	 * @generated
	 */
	int ACTIVE_PROCESS = 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveStateImpl <em>Active State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveStateImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveState()
	 * @generated
	 */
	int ACTIVE_STATE = 30;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDImpl <em>Active MSD</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSD()
	 * @generated
	 */
	int ACTIVE_MSD = 27;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.PropertyToEObjectMapEntryImpl <em>Property To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.PropertyToEObjectMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getPropertyToEObjectMapEntry()
	 * @generated
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY = 29;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDCutImpl <em>Active MSD Cut</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDCutImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDCut()
	 * @generated
	 */
	int ACTIVE_MSD_CUT = 5;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl <em>Active MSD Lifeline Bindings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindings()
	 * @generated
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS = 6;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl <em>Active MSD Variable Valuations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuations()
	 * @generated
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS = 7;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.LifelineToInteractionFragmentMapEntryImpl <em>Lifeline To Interaction Fragment Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.LifelineToInteractionFragmentMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getLifelineToInteractionFragmentMapEntry()
	 * @generated
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY = 8;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.StringToEObjectMapEntryImpl <em>String To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.StringToEObjectMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToEObjectMapEntry()
	 * @generated
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY = 9;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.LifelineToEObjectMapEntryImpl <em>Lifeline To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.LifelineToEObjectMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getLifelineToEObjectMapEntry()
	 * @generated
	 */
	int LIFELINE_TO_EOBJECT_MAP_ENTRY = 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.NamedElementToMessageEventMapEntryImpl <em>Named Element To Message Event Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.NamedElementToMessageEventMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getNamedElementToMessageEventMapEntry()
	 * @generated
	 */
	int NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY = 11;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl <em>MSD Modal Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDModalMessageEvent()
	 * @generated
	 */
	int MSD_MODAL_MESSAGE_EVENT = 12;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl <em>MSD Object System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDObjectSystem()
	 * @generated
	 */
	int MSD_OBJECT_SYSTEM = 13;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.StringToStringMapEntryImpl <em>String To String Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.StringToStringMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToStringMapEntry()
	 * @generated
	 */
	int STRING_TO_STRING_MAP_ENTRY = 14;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.StringToIntegerMapEntryImpl <em>String To Integer Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.StringToIntegerMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToIntegerMapEntry()
	 * @generated
	 */
	int STRING_TO_INTEGER_MAP_ENTRY = 15;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.StringToBooleanMapEntryImpl <em>String To Boolean Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.StringToBooleanMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToBooleanMapEntry()
	 * @generated
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY = 16;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl <em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry()
	 * @generated
	 */
	int MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY = 17;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveProcessKeyWrapperToActiveProcessMapEntryImpl <em>Active Process Key Wrapper To Active Process Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveProcessKeyWrapperToActiveProcessMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcessKeyWrapperToActiveProcessMapEntry()
	 * @generated
	 */
	int ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY = 18;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveStateKeyWrapperToActiveStateMapEntryImpl <em>Active State Key Wrapper To Active State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveStateKeyWrapperToActiveStateMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveStateKeyWrapperToActiveStateMapEntry()
	 * @generated
	 */
	int ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY = 19;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry()
	 * @generated
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY = 20;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry()
	 * @generated
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY = 21;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry()
	 * @generated
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY = 22;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MessageEventContainerImpl <em>Message Event Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MessageEventContainerImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventContainer()
	 * @generated
	 */
	int MESSAGE_EVENT_CONTAINER = 23;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.MessageEventKeyWrapperToMessageEventMapEntryImpl <em>Message Event Key Wrapper To Message Event Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.MessageEventKeyWrapperToMessageEventMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventKeyWrapperToMessageEventMapEntry()
	 * @generated
	 */
	int MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY = 24;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl <em>Object System Key Wrapper To MSD Object System Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY = 25;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSImpl <em>Active MSS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSSImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSS()
	 * @generated
	 */
	int ACTIVE_MSS = 26;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsImpl <em>Active MSS Role Bindings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindings()
	 * @generated
	 */
	int ACTIVE_MSS_ROLE_BINDINGS = 28;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSStateImpl <em>Active MSS State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.ActiveMSSStateImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSState()
	 * @generated
	 */
	int ACTIVE_MSS_STATE = 31;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.EObjectToEOperationMapEntryImpl <em>EObject To EOperation Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.EObjectToEOperationMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEObjectToEOperationMapEntry()
	 * @generated
	 */
	int EOBJECT_TO_EOPERATION_MAP_ENTRY = 32;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.EObjectToUMLClassMapEntryImpl <em>EObject To UML Class Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.EObjectToUMLClassMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEObjectToUMLClassMapEntry()
	 * @generated
	 */
	int EOBJECT_TO_UML_CLASS_MAP_ENTRY = 33;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.impl.EClassToEObjectMapEntryImpl
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
	 * @generated
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY = 34;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.ExecutionSemantics <em>Execution Semantics</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getExecutionSemantics()
	 * @generated
	 */
	int EXECUTION_SEMANTICS = 35;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.runtime.ActiveMSDProgress <em>Active MSD Progress</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.ActiveMSDProgress
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDProgress()
	 * @generated
	 */
	int ACTIVE_MSD_PROGRESS = 36;


	/**
	 * The meta object id for the '<em>Semantic Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ocl.SemanticException
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getSemanticException()
	 * @generated
	 */
	int SEMANTIC_EXCEPTION = 37;

	/**
	 * The meta object id for the '<em>Parser Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ocl.ParserException
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getParserException()
	 * @generated
	 */
	int PARSER_EXCEPTION = 38;


	/**
	 * The meta object id for the '<em>Active State Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveStateKeyWrapper()
	 * @generated
	 */
	int ACTIVE_STATE_KEY_WRAPPER = 39;

	/**
	 * The meta object id for the '<em>Active MSD Lifeline Bindings Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindingsKeyWrapper()
	 * @generated
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER = 40;

	/**
	 * The meta object id for the '<em>Active MSD Variable Valuations Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuationsKeyWrapper()
	 * @generated
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER = 41;


	/**
	 * The meta object id for the '<em>Active Process Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcessKeyWrapper()
	 * @generated
	 */
	int ACTIVE_PROCESS_KEY_WRAPPER = 42;

	/**
	 * The meta object id for the '<em>MSD Runtime State Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateKeyWrapper()
	 * @generated
	 */
	int MSD_RUNTIME_STATE_KEY_WRAPPER = 43;


	/**
	 * The meta object id for the '<em>Message Event Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventKeyWrapper()
	 * @generated
	 */
	int MESSAGE_EVENT_KEY_WRAPPER = 44;

	/**
	 * The meta object id for the '<em>Object System Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER = 45;


	/**
	 * The meta object id for the '<em>Active MSS Role Bindings Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper
	 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindingsKeyWrapper()
	 * @generated
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER = 46;

	int MSD_RUNTIME_STATE__EXCLUDED = 0;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__OBJECT_SYSTEM = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Message Event To Modal Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_MODAL_MESSAGE_EVENT_MAP;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__EVENT_TO_TRANSITION_MAP;

	/**
	 * The feature id for the '<em><b>Active Processes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__ACTIVE_PROCESSES = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__RUNTIME_UTIL = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Active MSD Changed During Perform Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>MSD Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Active Processes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_PROCESSES = 0;

	/**
	 * The feature id for the '<em><b>Active MSD Variable Valuations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS = 1;

	/**
	 * The feature id for the '<em><b>Active States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_STATES = 2;

	/**
	 * The feature id for the '<em><b>Active MSD Lifeline Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS = 3;

	/**
	 * The feature id for the '<em><b>Active MSS Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS = 4;

	/**
	 * The feature id for the '<em><b>Active Process Key Wrapper To Active Process Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP = 5;

	/**
	 * The feature id for the '<em><b>Active State Key Wrapper To Active State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP = 6;

	/**
	 * The feature id for the '<em><b>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP = 7;

	/**
	 * The feature id for the '<em><b>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP = 8;

	/**
	 * The feature id for the '<em><b>Object System Key Wrapper To MSD Object System Map Entry</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY = 9;

	/**
	 * The feature id for the '<em><b>Object Systems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEMS = 10;

	/**
	 * The feature id for the '<em><b>Final Fragment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__FINAL_FRAGMENT = 11;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH = 12;

	/**
	 * The feature id for the '<em><b>Active Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS = 13;

	/**
	 * The feature id for the '<em><b>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP = 14;

	/**
	 * The number of structural features of the '<em>Element Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER_FEATURE_COUNT = 15;

	/**
	 * The feature id for the '<em><b>Safety Violating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__SAFETY_VIOLATING = org.scenariotools.runtime.RuntimePackage.MODALITY__SAFETY_VIOLATING;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__MANDATORY = org.scenariotools.runtime.RuntimePackage.MODALITY__MANDATORY;

	/**
	 * The feature id for the '<em><b>Monitored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__MONITORED = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__HOT = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__COLD = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Initializing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__INITIALIZING = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Cold Violating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY__COLD_VIOLATING = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>MSD Modality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODALITY_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.MODALITY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Variable Valuations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__VARIABLE_VALUATIONS = 0;

	/**
	 * The feature id for the '<em><b>Cold Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS = 1;

	/**
	 * The feature id for the '<em><b>Hot Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS = 2;

	/**
	 * The feature id for the '<em><b>Violating Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__VIOLATING_EVENTS = 3;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__ENABLED_EVENTS = 4;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__RUNTIME_UTIL = 5;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__CURRENT_STATE = 6;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__OBJECT_SYSTEM = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS__ID = 8;

	/**
	 * The number of structural features of the '<em>Active Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS_FEATURE_COUNT = 9;

	/**
	 * The feature id for the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE__HOT = 0;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE__RUNTIME_UTIL = 1;

	/**
	 * The feature id for the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE__EXECUTED = 2;

	/**
	 * The number of structural features of the '<em>Active State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */

	int ACTIVE_STATE_FEATURE_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_CUT__HOT = ACTIVE_STATE__HOT;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_CUT__RUNTIME_UTIL = ACTIVE_STATE__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_CUT__EXECUTED = ACTIVE_STATE__EXECUTED;

	/**
	 * The feature id for the '<em><b>Lifeline To Interaction Fragment Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP = ACTIVE_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active MSD Cut</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_CUT_FEATURE_COUNT = ACTIVE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lifeline To EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP = 0;

	/**
	 * The feature id for the '<em><b>Ignored Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES = 1;

	/**
	 * The number of structural features of the '<em>Active MSD Lifeline Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Variable Name To EObject Value Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP = 0;

	/**
	 * The feature id for the '<em><b>Variable Name To String Value Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP = 1;

	/**
	 * The feature id for the '<em><b>Variable Name To Integer Value Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP = 2;

	/**
	 * The feature id for the '<em><b>Variable Name To Boolean Value Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP = 3;

	/**
	 * The number of structural features of the '<em>Active MSD Variable Valuations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_FEATURE_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Lifeline To Interaction Fragment Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>String To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Lifeline To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Named Element To Message Event Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Represented Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT__REPRESENTED_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Requirements Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT__REQUIREMENTS_MODALITY;

	/**
	 * The feature id for the '<em><b>Assumptions Modality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT__ASSUMPTIONS_MODALITY;

	/**
	 * The feature id for the '<em><b>Enabled In Active Process</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cold Violating In Active Process</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Safety Violating In Active Process</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent Symbolic MSD Modal Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Concrete MSD Modal Message Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>MSD Modal Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_MODAL_MESSAGE_EVENT_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.MODAL_MESSAGE_EVENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__ROOT_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS;

	/**
	 * The feature id for the '<em><b>Controllable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS;

	/**
	 * The feature id for the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS;

	/**
	 * The feature id for the '<em><b>Root Objects Contained</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__ROOT_OBJECTS_CONTAINED = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS_CONTAINED;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__RUNTIME_UTIL = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initializing Environment MSD Modal Message Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initializing System MSD Modal Message Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Assumption MSD Initializing Message Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Requirement MSD Initializing Message Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>EObject To Sendable EOperation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>EObject To Receivable EOperation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>EClass To Instance EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>EObjects To UML Classes It Is Instance Of Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Static Lifeline To EObject Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Static Role To EObject Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Roles To EObjects Mapping Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Message Event Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Message Event Key Wrapper To Message Event Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>MSD Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_OBJECT_SYSTEM_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To String Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_INTEGER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_INTEGER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Integer Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_INTEGER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>String To Boolean Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Active Process Key Wrapper To Active Process Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Active State Key Wrapper To Active State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Runtime Messages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES = 0;

	/**
	 * The feature id for the '<em><b>Message Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS = 1;

	/**
	 * The number of structural features of the '<em>Message Event Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_CONTAINER_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Message Event Key Wrapper To Message Event Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Object System Key Wrapper To MSD Object System Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Variable Valuations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__VARIABLE_VALUATIONS = ACTIVE_PROCESS__VARIABLE_VALUATIONS;

	/**
	 * The feature id for the '<em><b>Cold Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__COLD_FORBIDDEN_EVENTS = ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Hot Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__HOT_FORBIDDEN_EVENTS = ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Violating Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__VIOLATING_EVENTS = ACTIVE_PROCESS__VIOLATING_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__ENABLED_EVENTS = ACTIVE_PROCESS__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__RUNTIME_UTIL = ACTIVE_PROCESS__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__CURRENT_STATE = ACTIVE_PROCESS__CURRENT_STATE;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__OBJECT_SYSTEM = ACTIVE_PROCESS__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__ID = ACTIVE_PROCESS__ID;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__ROLE_BINDINGS = ACTIVE_PROCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS__STATE_MACHINE = ACTIVE_PROCESS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Active MSS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_FEATURE_COUNT = ACTIVE_PROCESS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variable Valuations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__VARIABLE_VALUATIONS = ACTIVE_PROCESS__VARIABLE_VALUATIONS;

	/**
	 * The feature id for the '<em><b>Cold Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__COLD_FORBIDDEN_EVENTS = ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Hot Forbidden Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__HOT_FORBIDDEN_EVENTS = ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Violating Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__VIOLATING_EVENTS = ACTIVE_PROCESS__VIOLATING_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__ENABLED_EVENTS = ACTIVE_PROCESS__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__RUNTIME_UTIL = ACTIVE_PROCESS__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__CURRENT_STATE = ACTIVE_PROCESS__CURRENT_STATE;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__OBJECT_SYSTEM = ACTIVE_PROCESS__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__ID = ACTIVE_PROCESS__ID;

	/**
	 * The feature id for the '<em><b>Lifeline Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__LIFELINE_BINDINGS = ACTIVE_PROCESS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__INTERACTION = ACTIVE_PROCESS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Msd Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD__MSD_UTIL = ACTIVE_PROCESS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Active MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSD_FEATURE_COUNT = ACTIVE_PROCESS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Property To EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP = 0;

	/**
	 * The number of structural features of the '<em>Active MSS Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_ROLE_BINDINGS_FEATURE_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Property To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_STATE__HOT = ACTIVE_STATE__HOT;

	/**
	 * The feature id for the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_STATE__RUNTIME_UTIL = ACTIVE_STATE__RUNTIME_UTIL;

	/**
	 * The feature id for the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_STATE__EXECUTED = ACTIVE_STATE__EXECUTED;

	/**
	 * The feature id for the '<em><b>Uml State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_STATE__UML_STATE = ACTIVE_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active MSS State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MSS_STATE_FEATURE_COUNT = ACTIVE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_EOPERATION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_EOPERATION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EObject To EOperation Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_EOPERATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_UML_CLASS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_UML_CLASS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EObject To UML Class Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_TO_UML_CLASS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>EClass To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph <em>MSD Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph
	 * @generated
	 */
	EClass getMSDRuntimeStateGraph();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Container</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer()
	 * @see #getMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getMSDRuntimeStateGraph_ElementContainer();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil()
	 * @see #getMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getMSDRuntimeStateGraph_RuntimeUtil();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario Run Configuration</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getScenarioRunConfiguration()
	 * @see #getMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getMSDRuntimeStateGraph_ScenarioRunConfiguration();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap <em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
	 * @see #getMSDRuntimeStateGraph()
	 * @generated
	 */
	EReference getMSDRuntimeStateGraph_MsdRuntimeStateKeyWrapperToMSDRuntimeStateMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MSDRuntimeState <em>MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Runtime State</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState
	 * @generated
	 */
	EClass getMSDRuntimeState();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDRuntimeState#getActiveProcesses <em>Active Processes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Active Processes</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState#getActiveProcesses()
	 * @see #getMSDRuntimeState()
	 * @generated
	 */
	EReference getMSDRuntimeState_ActiveProcesses();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.MSDRuntimeState#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState#getRuntimeUtil()
	 * @see #getMSDRuntimeState()
	 * @generated
	 */
	EReference getMSDRuntimeState_RuntimeUtil();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDRuntimeState#getActiveMSDChangedDuringPerformStep <em>Active MSD Changed During Perform Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Active MSD Changed During Perform Step</em>'.
	 * @see org.scenariotools.msd.runtime.MSDRuntimeState#getActiveMSDChangedDuringPerformStep()
	 * @see #getMSDRuntimeState()
	 * @generated
	 */
	EReference getMSDRuntimeState_ActiveMSDChangedDuringPerformStep();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Container</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer
	 * @generated
	 */
	EClass getElementContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveProcesses <em>Active Processes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Processes</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveProcesses()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveProcesses();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuations <em>Active MSD Variable Valuations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active MSD Variable Valuations</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuations()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSDVariableValuations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveStates <em>Active States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active States</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveStates()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveStates();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindings <em>Active MSD Lifeline Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active MSD Lifeline Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindings()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSDLifelineBindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindings <em>Active MSS Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active MSS Role Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindings()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSSRoleBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveProcessKeyWrapperToActiveProcessMap <em>Active Process Key Wrapper To Active Process Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Process Key Wrapper To Active Process Map</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveProcessKeyWrapperToActiveProcessMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveProcessKeyWrapperToActiveProcessMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveStateKeyWrapperToActiveStateMap <em>Active State Key Wrapper To Active State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active State Key Wrapper To Active State Map</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveStateKeyWrapperToActiveStateMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveStateKeyWrapperToActiveStateMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry <em>Object System Key Wrapper To MSD Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object System Key Wrapper To MSD Object System Map Entry</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystemKeyWrapperToMSDObjectSystemMapEntry();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getObjectSystems <em>Object Systems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Systems</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getObjectSystems()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystems();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.msd.runtime.ElementContainer#getFinalFragment <em>Final Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Final Fragment</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getFinalFragment()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_FinalFragment();

	/**
	 * Returns the meta object for the container reference '{@link org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Msd Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_MsdRuntimeStateGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveRoleBindings <em>Active Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Role Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveRoleBindings()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveRoleBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>'.
	 * @see org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MSDModality <em>MSD Modality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Modality</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality
	 * @generated
	 */
	EClass getMSDModality();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.MSDModality#isMonitored <em>Monitored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monitored</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality#isMonitored()
	 * @see #getMSDModality()
	 * @generated
	 */
	EAttribute getMSDModality_Monitored();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.MSDModality#isHot <em>Hot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hot</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality#isHot()
	 * @see #getMSDModality()
	 * @generated
	 */
	EAttribute getMSDModality_Hot();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.MSDModality#isCold <em>Cold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cold</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality#isCold()
	 * @see #getMSDModality()
	 * @generated
	 */
	EAttribute getMSDModality_Cold();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.MSDModality#isInitializing <em>Initializing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initializing</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality#isInitializing()
	 * @see #getMSDModality()
	 * @generated
	 */
	EAttribute getMSDModality_Initializing();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.MSDModality#isColdViolating <em>Cold Violating</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cold Violating</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModality#isColdViolating()
	 * @see #getMSDModality()
	 * @generated
	 */
	EAttribute getMSDModality_ColdViolating();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveProcess <em>Active Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Process</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess
	 * @generated
	 */
	EClass getActiveProcess();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveProcess#getVariableValuations <em>Variable Valuations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable Valuations</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getVariableValuations()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_VariableValuations();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveProcess#getColdForbiddenEvents <em>Cold Forbidden Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Cold Forbidden Events</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getColdForbiddenEvents()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_ColdForbiddenEvents();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveProcess#getHotForbiddenEvents <em>Hot Forbidden Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Hot Forbidden Events</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getHotForbiddenEvents()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_HotForbiddenEvents();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveProcess#getViolatingEvents <em>Violating Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Violating Events</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getViolatingEvents()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_ViolatingEvents();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveProcess#getEnabledEvents <em>Enabled Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Enabled Events</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getEnabledEvents()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_EnabledEvents();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveProcess#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getRuntimeUtil()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_RuntimeUtil();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveProcess#getCurrentState <em>Current State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current State</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getCurrentState()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_CurrentState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveProcess#getObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object System</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getObjectSystem()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EReference getActiveProcess_ObjectSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.ActiveProcess#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveProcess#getId()
	 * @see #getActiveProcess()
	 * @generated
	 */
	EAttribute getActiveProcess_Id();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSD <em>Active MSD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSD
	 * @generated
	 */
	EClass getActiveMSD();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Property To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Property" keyRequired="true"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getPropertyToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToEObjectMapEntry()
	 * @generated
	 */
	EReference getPropertyToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToEObjectMapEntry()
	 * @generated
	 */
	EReference getPropertyToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveState <em>Active State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active State</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveState
	 * @generated
	 */
	EClass getActiveState();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.ActiveState#isHot <em>Hot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hot</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveState#isHot()
	 * @see #getActiveState()
	 * @generated
	 */
	EAttribute getActiveState_Hot();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveState#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveState#getRuntimeUtil()
	 * @see #getActiveState()
	 * @generated
	 */
	EReference getActiveState_RuntimeUtil();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.runtime.ActiveState#isExecuted <em>Executed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Executed</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveState#isExecuted()
	 * @see #getActiveState()
	 * @generated
	 */
	EAttribute getActiveState_Executed();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSSState <em>Active MSS State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSS State</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSSState
	 * @generated
	 */
	EClass getActiveMSSState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSSState#getUmlState <em>Uml State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml State</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSSState#getUmlState()
	 * @see #getActiveMSSState()
	 * @generated
	 */
	EReference getActiveMSSState_UmlState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSD#getLifelineBindings <em>Lifeline Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lifeline Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSD#getLifelineBindings()
	 * @see #getActiveMSD()
	 * @generated
	 */
	EReference getActiveMSD_LifelineBindings();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSD#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interaction</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSD#getInteraction()
	 * @see #getActiveMSD()
	 * @generated
	 */
	EReference getActiveMSD_Interaction();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSD#getMsdUtil <em>Msd Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Msd Util</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSD#getMsdUtil()
	 * @see #getActiveMSD()
	 * @generated
	 */
	EReference getActiveMSD_MsdUtil();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings <em>Active MSS Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSS Role Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSSRoleBindings
	 * @generated
	 */
	EClass getActiveMSSRoleBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings#getPropertyToEObjectMap <em>Property To EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Property To EObject Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSSRoleBindings#getPropertyToEObjectMap()
	 * @see #getActiveMSSRoleBindings()
	 * @generated
	 */
	EReference getActiveMSSRoleBindings_PropertyToEObjectMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSDCut <em>Active MSD Cut</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD Cut</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDCut
	 * @generated
	 */
	EClass getActiveMSDCut();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDCut#getLifelineToInteractionFragmentMap <em>Lifeline To Interaction Fragment Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Lifeline To Interaction Fragment Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDCut#getLifelineToInteractionFragmentMap()
	 * @see #getActiveMSDCut()
	 * @generated
	 */
	EReference getActiveMSDCut_LifelineToInteractionFragmentMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings <em>Active MSD Lifeline Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD Lifeline Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDLifelineBindings
	 * @generated
	 */
	EClass getActiveMSDLifelineBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getLifelineToEObjectMap <em>Lifeline To EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Lifeline To EObject Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getLifelineToEObjectMap()
	 * @see #getActiveMSDLifelineBindings()
	 * @generated
	 */
	EReference getActiveMSDLifelineBindings_LifelineToEObjectMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getIgnoredLifelines <em>Ignored Lifelines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ignored Lifelines</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getIgnoredLifelines()
	 * @see #getActiveMSDLifelineBindings()
	 * @generated
	 */
	EReference getActiveMSDLifelineBindings_IgnoredLifelines();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations <em>Active MSD Variable Valuations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD Variable Valuations</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations
	 * @generated
	 */
	EClass getActiveMSDVariableValuations();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToEObjectValueMap <em>Variable Name To EObject Value Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Name To EObject Value Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToEObjectValueMap()
	 * @see #getActiveMSDVariableValuations()
	 * @generated
	 */
	EReference getActiveMSDVariableValuations_VariableNameToEObjectValueMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToStringValueMap <em>Variable Name To String Value Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Name To String Value Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToStringValueMap()
	 * @see #getActiveMSDVariableValuations()
	 * @generated
	 */
	EReference getActiveMSDVariableValuations_VariableNameToStringValueMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToIntegerValueMap <em>Variable Name To Integer Value Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Name To Integer Value Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToIntegerValueMap()
	 * @see #getActiveMSDVariableValuations()
	 * @generated
	 */
	EReference getActiveMSDVariableValuations_VariableNameToIntegerValueMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToBooleanValueMap <em>Variable Name To Boolean Value Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Name To Boolean Value Map</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToBooleanValueMap()
	 * @see #getActiveMSDVariableValuations()
	 * @generated
	 */
	EReference getActiveMSDVariableValuations_VariableNameToBooleanValueMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Lifeline To Interaction Fragment Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lifeline To Interaction Fragment Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Lifeline" keyRequired="true"
	 *        valueType="org.eclipse.uml2.uml.InteractionFragment"
	 * @generated
	 */
	EClass getLifelineToInteractionFragmentMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToInteractionFragmentMapEntry()
	 * @generated
	 */
	EReference getLifelineToInteractionFragmentMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToInteractionFragmentMapEntry()
	 * @generated
	 */
	EReference getLifelineToInteractionFragmentMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 *        keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 * @generated
	 */
	EClass getStringToEObjectMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToEObjectMapEntry()
	 * @generated
	 */
	EAttribute getStringToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToEObjectMapEntry()
	 * @generated
	 */
	EReference getStringToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Lifeline To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lifeline To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Lifeline" keyRequired="true"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getLifelineToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToEObjectMapEntry()
	 * @generated
	 */
	EReference getLifelineToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToEObjectMapEntry()
	 * @generated
	 */
	EReference getLifelineToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Named Element To Message Event Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element To Message Event Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.NamedElement" keyRequired="true"
	 *        valueType="org.scenariotools.events.MessageEvent"
	 * @generated
	 */
	EClass getNamedElementToMessageEventMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNamedElementToMessageEventMapEntry()
	 * @generated
	 */
	EReference getNamedElementToMessageEventMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNamedElementToMessageEventMapEntry()
	 * @generated
	 */
	EReference getNamedElementToMessageEventMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent <em>MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Modal Message Event</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent
	 * @generated
	 */
	EClass getMSDModalMessageEvent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getEnabledInActiveProcess <em>Enabled In Active Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled In Active Process</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getEnabledInActiveProcess()
	 * @see #getMSDModalMessageEvent()
	 * @generated
	 */
	EReference getMSDModalMessageEvent_EnabledInActiveProcess();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getColdViolatingInActiveProcess <em>Cold Violating In Active Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Cold Violating In Active Process</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getColdViolatingInActiveProcess()
	 * @see #getMSDModalMessageEvent()
	 * @generated
	 */
	EReference getMSDModalMessageEvent_ColdViolatingInActiveProcess();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getSafetyViolatingInActiveProcess <em>Safety Violating In Active Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Safety Violating In Active Process</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getSafetyViolatingInActiveProcess()
	 * @see #getMSDModalMessageEvent()
	 * @generated
	 */
	EReference getMSDModalMessageEvent_SafetyViolatingInActiveProcess();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent <em>Parent Symbolic MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Symbolic MSD Modal Message Event</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent()
	 * @see #getMSDModalMessageEvent()
	 * @generated
	 */
	EReference getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getConcreteMSDModalMessageEvent <em>Concrete MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Concrete MSD Modal Message Event</em>'.
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getConcreteMSDModalMessageEvent()
	 * @see #getMSDModalMessageEvent()
	 * @generated
	 */
	EReference getMSDModalMessageEvent_ConcreteMSDModalMessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MSDObjectSystem <em>MSD Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Object System</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem
	 * @generated
	 */
	EClass getMSDObjectSystem();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getRuntimeUtil()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_RuntimeUtil();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingEnvironmentMSDModalMessageEvents <em>Initializing Environment MSD Modal Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Initializing Environment MSD Modal Message Events</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingEnvironmentMSDModalMessageEvents()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_InitializingEnvironmentMSDModalMessageEvents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingSystemMSDModalMessageEvents <em>Initializing System MSD Modal Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Initializing System MSD Modal Message Events</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getInitializingSystemMSDModalMessageEvents()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_InitializingSystemMSDModalMessageEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getAssumptionMSDInitializingMessageEvents <em>Assumption MSD Initializing Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assumption MSD Initializing Message Events</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getAssumptionMSDInitializingMessageEvents()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_AssumptionMSDInitializingMessageEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRequirementMSDInitializingMessageEvents <em>Requirement MSD Initializing Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Requirement MSD Initializing Message Events</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getRequirementMSDInitializingMessageEvents()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_RequirementMSDInitializingMessageEvents();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToSendableEOperationMap <em>EObject To Sendable EOperation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EObject To Sendable EOperation Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToSendableEOperationMap()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_EObjectToSendableEOperationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToReceivableEOperationMap <em>EObject To Receivable EOperation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EObject To Receivable EOperation Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectToReceivableEOperationMap()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_EObjectToReceivableEOperationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEClassToInstanceEObjectMap <em>EClass To Instance EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EClass To Instance EObject Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getEClassToInstanceEObjectMap()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_EClassToInstanceEObjectMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectsToUMLClassesItIsInstanceOfMap <em>EObjects To UML Classes It Is Instance Of Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EObjects To UML Classes It Is Instance Of Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getEObjectsToUMLClassesItIsInstanceOfMap()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_EObjectsToUMLClassesItIsInstanceOfMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getStaticLifelineToEObjectBindings <em>Static Lifeline To EObject Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static Lifeline To EObject Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getStaticLifelineToEObjectBindings()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_StaticLifelineToEObjectBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getStaticRoleToEObjectBindings <em>Static Role To EObject Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static Role To EObject Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getStaticRoleToEObjectBindings()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_StaticRoleToEObjectBindings();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getRolesToEObjectsMappingContainer <em>Roles To EObjects Mapping Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Roles To EObjects Mapping Container</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getRolesToEObjectsMappingContainer()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_RolesToEObjectsMappingContainer();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventContainer <em>Message Event Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Message Event Container</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventContainer()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_MessageEventContainer();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventKeyWrapperToMessageEventMap <em>Message Event Key Wrapper To Message Event Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Message Event Key Wrapper To Message Event Map</em>'.
	 * @see org.scenariotools.msd.runtime.MSDObjectSystem#getMessageEventKeyWrapperToMessageEventMap()
	 * @see #getMSDObjectSystem()
	 * @generated
	 */
	EReference getMSDObjectSystem_MessageEventKeyWrapperToMessageEventMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To String Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To String Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getStringToStringMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMapEntry()
	 * @generated
	 */
	EAttribute getStringToStringMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMapEntry()
	 * @generated
	 */
	EAttribute getStringToStringMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Integer Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Integer Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getStringToIntegerMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToIntegerMapEntry()
	 * @generated
	 */
	EAttribute getStringToIntegerMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToIntegerMapEntry()
	 * @generated
	 */
	EAttribute getStringToIntegerMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Boolean Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Boolean Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueDataType="org.eclipse.emf.ecore.EBooleanObject"
	 *        keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 * @generated
	 */
	EClass getStringToBooleanMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToBooleanMapEntry()
	 * @generated
	 */
	EAttribute getStringToBooleanMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.MSDRuntimeState"
	 *        keyDataType="org.scenariotools.msd.runtime.MSDRuntimeStateKeyWrapper"
	 * @generated
	 */
	EClass getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry()
	 * @generated
	 */
	EReference getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry()
	 * @generated
	 */
	EAttribute getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Process Key Wrapper To Active Process Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Process Key Wrapper To Active Process Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.ActiveProcess"
	 *        keyDataType="org.scenariotools.msd.runtime.ActiveProcessKeyWrapper"
	 * @generated
	 */
	EClass getActiveProcessKeyWrapperToActiveProcessMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveProcessKeyWrapperToActiveProcessMapEntry()
	 * @generated
	 */
	EReference getActiveProcessKeyWrapperToActiveProcessMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveProcessKeyWrapperToActiveProcessMapEntry()
	 * @generated
	 */
	EAttribute getActiveProcessKeyWrapperToActiveProcessMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active State Key Wrapper To Active State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active State Key Wrapper To Active State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.ActiveState"
	 *        keyDataType="org.scenariotools.msd.runtime.ActiveStateKeyWrapper"
	 * @generated
	 */
	EClass getActiveStateKeyWrapperToActiveStateMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveStateKeyWrapperToActiveStateMapEntry()
	 * @generated
	 */
	EReference getActiveStateKeyWrapperToActiveStateMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveStateKeyWrapperToActiveStateMapEntry()
	 * @generated
	 */
	EAttribute getActiveStateKeyWrapperToActiveStateMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.ActiveMSDLifelineBindings"
	 *        keyDataType="org.scenariotools.msd.runtime.ActiveMSDLifelineBindingsKeyWrapper"
	 * @generated
	 */
	EClass getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry()
	 * @generated
	 */
	EReference getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry()
	 * @generated
	 */
	EAttribute getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.ActiveMSDVariableValuations"
	 *        keyDataType="org.scenariotools.msd.runtime.ActiveMSDVariableValuationsKeyWrapper"
	 * @generated
	 */
	EClass getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry()
	 * @generated
	 */
	EReference getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry()
	 * @generated
	 */
	EAttribute getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.ActiveMSSRoleBindings"
	 *        keyDataType="org.scenariotools.msd.runtime.ActiveMSSRoleBindingsKeyWrapper"
	 * @generated
	 */
	EClass getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry()
	 * @generated
	 */
	EReference getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry()
	 * @generated
	 */
	EAttribute getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.MessageEventContainer <em>Message Event Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Container</em>'.
	 * @see org.scenariotools.msd.runtime.MessageEventContainer
	 * @generated
	 */
	EClass getMessageEventContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.MessageEventContainer#getRuntimeMessages <em>Runtime Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Messages</em>'.
	 * @see org.scenariotools.msd.runtime.MessageEventContainer#getRuntimeMessages()
	 * @see #getMessageEventContainer()
	 * @generated
	 */
	EReference getMessageEventContainer_RuntimeMessages();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.runtime.MessageEventContainer#getMessageEvents <em>Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Events</em>'.
	 * @see org.scenariotools.msd.runtime.MessageEventContainer#getMessageEvents()
	 * @see #getMessageEventContainer()
	 * @generated
	 */
	EReference getMessageEventContainer_MessageEvents();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Message Event Key Wrapper To Message Event Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Key Wrapper To Message Event Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.events.MessageEvent"
	 *        keyDataType="org.scenariotools.msd.runtime.MessageEventKeyWrapper"
	 * @generated
	 */
	EClass getMessageEventKeyWrapperToMessageEventMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventKeyWrapperToMessageEventMapEntry()
	 * @generated
	 */
	EReference getMessageEventKeyWrapperToMessageEventMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventKeyWrapperToMessageEventMapEntry()
	 * @generated
	 */
	EAttribute getMessageEventKeyWrapperToMessageEventMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object System Key Wrapper To MSD Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System Key Wrapper To MSD Object System Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.scenariotools.msd.runtime.MSDObjectSystem"
	 *        keyDataType="org.scenariotools.msd.runtime.ObjectSystemKeyWrapper"
	 * @generated
	 */
	EClass getObjectSystemKeyWrapperToMSDObjectSystemMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @generated
	 */
	EReference getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @generated
	 */
	EAttribute getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.runtime.ActiveMSS <em>Active MSS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active MSS</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSS
	 * @generated
	 */
	EClass getActiveMSS();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSS#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Bindings</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSS#getRoleBindings()
	 * @see #getActiveMSS()
	 * @generated
	 */
	EReference getActiveMSS_RoleBindings();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.runtime.ActiveMSS#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Machine</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSS#getStateMachine()
	 * @see #getActiveMSS()
	 * @generated
	 */
	EReference getActiveMSS_StateMachine();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EObject To EOperation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject To EOperation Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject"
	 *        valueType="org.eclipse.emf.ecore.EOperation" valueMany="true"
	 * @generated
	 */
	EClass getEObjectToEOperationMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToEOperationMapEntry()
	 * @generated
	 */
	EReference getEObjectToEOperationMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToEOperationMapEntry()
	 * @generated
	 */
	EReference getEObjectToEOperationMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EObject To UML Class Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject To UML Class Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject"
	 *        valueType="org.eclipse.uml2.uml.Class" valueMany="true"
	 * @generated
	 */
	EClass getEObjectToUMLClassMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToUMLClassMapEntry()
	 * @generated
	 */
	EReference getEObjectToUMLClassMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEObjectToUMLClassMapEntry()
	 * @generated
	 */
	EReference getEObjectToUMLClassMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.emf.ecore.EObject" valueMany="true"
	 *        keyType="org.eclipse.emf.ecore.EClass" keyRequired="true"
	 * @generated
	 */
	EClass getEClassToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.msd.runtime.ExecutionSemantics <em>Execution Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Execution Semantics</em>'.
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @generated
	 */
	EEnum getExecutionSemantics();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToBooleanMapEntry()
	 * @generated
	 */
	EAttribute getStringToBooleanMapEntry_Value();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.msd.runtime.ActiveMSDProgress <em>Active MSD Progress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Active MSD Progress</em>'.
	 * @see org.scenariotools.msd.runtime.ActiveMSDProgress
	 * @generated
	 */
	EEnum getActiveMSDProgress();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ocl.SemanticException <em>Semantic Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Semantic Exception</em>'.
	 * @see org.eclipse.ocl.SemanticException
	 * @model instanceClass="org.eclipse.ocl.SemanticException"
	 * @generated
	 */
	EDataType getSemanticException();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ocl.ParserException <em>Parser Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Parser Exception</em>'.
	 * @see org.eclipse.ocl.ParserException
	 * @model instanceClass="org.eclipse.ocl.ParserException"
	 * @generated
	 */
	EDataType getParserException();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper <em>Active State Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active State Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper"
	 * @generated
	 */
	EDataType getActiveStateKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper <em>Active MSD Lifeline Bindings Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active MSD Lifeline Bindings Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper"
	 * @generated
	 */
	EDataType getActiveMSDLifelineBindingsKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper <em>Active MSD Variable Valuations Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active MSD Variable Valuations Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper"
	 * @generated
	 */
	EDataType getActiveMSDVariableValuationsKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper <em>Active Process Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Process Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper"
	 * @generated
	 */
	EDataType getActiveProcessKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper <em>MSD Runtime State Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>MSD Runtime State Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper"
	 * @generated
	 */
	EDataType getMSDRuntimeStateKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper <em>Message Event Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Message Event Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper"
	 * @generated
	 */
	EDataType getMessageEventKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper <em>Object System Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Object System Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper"
	 * @generated
	 */
	EDataType getObjectSystemKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper <em>Active MSS Role Bindings Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active MSS Role Bindings Key Wrapper</em>'.
	 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper
	 * @model instanceClass="org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper"
	 * @generated
	 */
	EDataType getActiveMSSRoleBindingsKeyWrapper();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl <em>MSD Runtime State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateGraphImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateGraph()
		 * @generated
		 */
		EClass MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getMSDRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Element Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = eINSTANCE.getMSDRuntimeStateGraph_ElementContainer();

		/**
		 * The meta object literal for the '<em><b>Runtime Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL = eINSTANCE.getMSDRuntimeStateGraph_RuntimeUtil();

		
		
		/**
		 * The meta object literal for the '<em><b>Scenario Run Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION = eINSTANCE.getMSDRuntimeStateGraph_ScenarioRunConfiguration();

		/**
		 * The meta object literal for the '<em><b>Msd Runtime State Key Wrapper To MSD Runtime State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP = eINSTANCE.getMSDRuntimeStateGraph_MsdRuntimeStateKeyWrapperToMSDRuntimeStateMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl <em>MSD Runtime State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeState()
		 * @generated
		 */
		EClass MSD_RUNTIME_STATE = eINSTANCE.getMSDRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Active Processes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE__ACTIVE_PROCESSES = eINSTANCE.getMSDRuntimeState_ActiveProcesses();

		/**
		 * The meta object literal for the '<em><b>Runtime Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE__RUNTIME_UTIL = eINSTANCE.getMSDRuntimeState_RuntimeUtil();

		/**
		 * The meta object literal for the '<em><b>Active MSD Changed During Perform Step</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP = eINSTANCE.getMSDRuntimeState_ActiveMSDChangedDuringPerformStep();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ElementContainerImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getElementContainer()
		 * @generated
		 */
		EClass ELEMENT_CONTAINER = eINSTANCE.getElementContainer();

		/**
		 * The meta object literal for the '<em><b>Active Processes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_PROCESSES = eINSTANCE.getElementContainer_ActiveProcesses();

		/**
		 * The meta object literal for the '<em><b>Active MSD Variable Valuations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS = eINSTANCE.getElementContainer_ActiveMSDVariableValuations();

		/**
		 * The meta object literal for the '<em><b>Active States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_STATES = eINSTANCE.getElementContainer_ActiveStates();

		/**
		 * The meta object literal for the '<em><b>Active MSD Lifeline Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS = eINSTANCE.getElementContainer_ActiveMSDLifelineBindings();

		/**
		 * The meta object literal for the '<em><b>Active MSS Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS = eINSTANCE.getElementContainer_ActiveMSSRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Active Process Key Wrapper To Active Process Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP = eINSTANCE.getElementContainer_ActiveProcessKeyWrapperToActiveProcessMap();

		/**
		 * The meta object literal for the '<em><b>Active State Key Wrapper To Active State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP = eINSTANCE.getElementContainer_ActiveStateKeyWrapperToActiveStateMap();

		/**
		 * The meta object literal for the '<em><b>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP = eINSTANCE.getElementContainer_ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap();

		/**
		 * The meta object literal for the '<em><b>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP = eINSTANCE.getElementContainer_ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap();

		/**
		 * The meta object literal for the '<em><b>Object System Key Wrapper To MSD Object System Map Entry</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY = eINSTANCE.getElementContainer_ObjectSystemKeyWrapperToMSDObjectSystemMapEntry();

		/**
		 * The meta object literal for the '<em><b>Object Systems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEMS = eINSTANCE.getElementContainer_ObjectSystems();

		/**
		 * The meta object literal for the '<em><b>Final Fragment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__FINAL_FRAGMENT = eINSTANCE.getElementContainer_FinalFragment();

		/**
		 * The meta object literal for the '<em><b>Msd Runtime State Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getElementContainer_MsdRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Active Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS = eINSTANCE.getElementContainer_ActiveRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP = eINSTANCE.getElementContainer_ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDModalityImpl <em>MSD Modality</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDModalityImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDModality()
		 * @generated
		 */
		EClass MSD_MODALITY = eINSTANCE.getMSDModality();

		/**
		 * The meta object literal for the '<em><b>Monitored</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_MODALITY__MONITORED = eINSTANCE.getMSDModality_Monitored();

		/**
		 * The meta object literal for the '<em><b>Hot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_MODALITY__HOT = eINSTANCE.getMSDModality_Hot();

		/**
		 * The meta object literal for the '<em><b>Cold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_MODALITY__COLD = eINSTANCE.getMSDModality_Cold();

		/**
		 * The meta object literal for the '<em><b>Initializing</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_MODALITY__INITIALIZING = eINSTANCE.getMSDModality_Initializing();

		/**
		 * The meta object literal for the '<em><b>Cold Violating</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_MODALITY__COLD_VIOLATING = eINSTANCE.getMSDModality_ColdViolating();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveProcessImpl <em>Active Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveProcessImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcess()
		 * @generated
		 */
		EClass ACTIVE_PROCESS = eINSTANCE.getActiveProcess();

		/**
		 * The meta object literal for the '<em><b>Variable Valuations</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__VARIABLE_VALUATIONS = eINSTANCE.getActiveProcess_VariableValuations();

		/**
		 * The meta object literal for the '<em><b>Cold Forbidden Events</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS = eINSTANCE.getActiveProcess_ColdForbiddenEvents();

		/**
		 * The meta object literal for the '<em><b>Hot Forbidden Events</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS = eINSTANCE.getActiveProcess_HotForbiddenEvents();

		/**
		 * The meta object literal for the '<em><b>Violating Events</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__VIOLATING_EVENTS = eINSTANCE.getActiveProcess_ViolatingEvents();

		/**
		 * The meta object literal for the '<em><b>Enabled Events</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__ENABLED_EVENTS = eINSTANCE.getActiveProcess_EnabledEvents();

		/**
		 * The meta object literal for the '<em><b>Runtime Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__RUNTIME_UTIL = eINSTANCE.getActiveProcess_RuntimeUtil();

		/**
		 * The meta object literal for the '<em><b>Current State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__CURRENT_STATE = eINSTANCE.getActiveProcess_CurrentState();

		/**
		 * The meta object literal for the '<em><b>Object System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS__OBJECT_SYSTEM = eINSTANCE.getActiveProcess_ObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_PROCESS__ID = eINSTANCE.getActiveProcess_Id();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDImpl <em>Active MSD</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSD()
		 * @generated
		 */
		EClass ACTIVE_MSD = eINSTANCE.getActiveMSD();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.PropertyToEObjectMapEntryImpl <em>Property To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.PropertyToEObjectMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getPropertyToEObjectMapEntry()
		 * @generated
		 */
		EClass PROPERTY_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getPropertyToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getPropertyToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getPropertyToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveStateImpl <em>Active State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveStateImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveState()
		 * @generated
		 */
		EClass ACTIVE_STATE = eINSTANCE.getActiveState();

		/**
		 * The meta object literal for the '<em><b>Hot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_STATE__HOT = eINSTANCE.getActiveState_Hot();

		/**
		 * The meta object literal for the '<em><b>Runtime Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_STATE__RUNTIME_UTIL = eINSTANCE.getActiveState_RuntimeUtil();

		/**
		 * The meta object literal for the '<em><b>Executed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_STATE__EXECUTED = eINSTANCE.getActiveState_Executed();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSStateImpl <em>Active MSS State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSSStateImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSState()
		 * @generated
		 */
		EClass ACTIVE_MSS_STATE = eINSTANCE.getActiveMSSState();

		/**
		 * The meta object literal for the '<em><b>Uml State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSS_STATE__UML_STATE = eINSTANCE.getActiveMSSState_UmlState();

		/**
		 * The meta object literal for the '<em><b>Lifeline Bindings</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD__LIFELINE_BINDINGS = eINSTANCE.getActiveMSD_LifelineBindings();

		/**
		 * The meta object literal for the '<em><b>Interaction</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD__INTERACTION = eINSTANCE.getActiveMSD_Interaction();

		/**
		 * The meta object literal for the '<em><b>Msd Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD__MSD_UTIL = eINSTANCE.getActiveMSD_MsdUtil();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsImpl <em>Active MSS Role Bindings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindings()
		 * @generated
		 */
		EClass ACTIVE_MSS_ROLE_BINDINGS = eINSTANCE.getActiveMSSRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Property To EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP = eINSTANCE.getActiveMSSRoleBindings_PropertyToEObjectMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDCutImpl <em>Active MSD Cut</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDCutImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDCut()
		 * @generated
		 */
		EClass ACTIVE_MSD_CUT = eINSTANCE.getActiveMSDCut();

		/**
		 * The meta object literal for the '<em><b>Lifeline To Interaction Fragment Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP = eINSTANCE.getActiveMSDCut_LifelineToInteractionFragmentMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl <em>Active MSD Lifeline Bindings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindings()
		 * @generated
		 */
		EClass ACTIVE_MSD_LIFELINE_BINDINGS = eINSTANCE.getActiveMSDLifelineBindings();

		/**
		 * The meta object literal for the '<em><b>Lifeline To EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP = eINSTANCE.getActiveMSDLifelineBindings_LifelineToEObjectMap();

		/**
		 * The meta object literal for the '<em><b>Ignored Lifelines</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES = eINSTANCE.getActiveMSDLifelineBindings_IgnoredLifelines();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl <em>Active MSD Variable Valuations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuations()
		 * @generated
		 */
		EClass ACTIVE_MSD_VARIABLE_VALUATIONS = eINSTANCE.getActiveMSDVariableValuations();

		/**
		 * The meta object literal for the '<em><b>Variable Name To EObject Value Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP = eINSTANCE.getActiveMSDVariableValuations_VariableNameToEObjectValueMap();

		/**
		 * The meta object literal for the '<em><b>Variable Name To String Value Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP = eINSTANCE.getActiveMSDVariableValuations_VariableNameToStringValueMap();

		/**
		 * The meta object literal for the '<em><b>Variable Name To Integer Value Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP = eINSTANCE.getActiveMSDVariableValuations_VariableNameToIntegerValueMap();

		/**
		 * The meta object literal for the '<em><b>Variable Name To Boolean Value Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP = eINSTANCE.getActiveMSDVariableValuations_VariableNameToBooleanValueMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.LifelineToInteractionFragmentMapEntryImpl <em>Lifeline To Interaction Fragment Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.LifelineToInteractionFragmentMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getLifelineToInteractionFragmentMapEntry()
		 * @generated
		 */
		EClass LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY = eINSTANCE.getLifelineToInteractionFragmentMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__KEY = eINSTANCE.getLifelineToInteractionFragmentMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__VALUE = eINSTANCE.getLifelineToInteractionFragmentMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.StringToEObjectMapEntryImpl <em>String To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.StringToEObjectMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToEObjectMapEntry()
		 * @generated
		 */
		EClass STRING_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getStringToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getStringToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStringToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.LifelineToEObjectMapEntryImpl <em>Lifeline To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.LifelineToEObjectMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getLifelineToEObjectMapEntry()
		 * @generated
		 */
		EClass LIFELINE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getLifelineToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getLifelineToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getLifelineToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.NamedElementToMessageEventMapEntryImpl <em>Named Element To Message Event Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.NamedElementToMessageEventMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getNamedElementToMessageEventMapEntry()
		 * @generated
		 */
		EClass NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY = eINSTANCE.getNamedElementToMessageEventMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__KEY = eINSTANCE.getNamedElementToMessageEventMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE = eINSTANCE.getNamedElementToMessageEventMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl <em>MSD Modal Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDModalMessageEventImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDModalMessageEvent()
		 * @generated
		 */
		EClass MSD_MODAL_MESSAGE_EVENT = eINSTANCE.getMSDModalMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Enabled In Active Process</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS = eINSTANCE.getMSDModalMessageEvent_EnabledInActiveProcess();

		/**
		 * The meta object literal for the '<em><b>Cold Violating In Active Process</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS = eINSTANCE.getMSDModalMessageEvent_ColdViolatingInActiveProcess();

		/**
		 * The meta object literal for the '<em><b>Safety Violating In Active Process</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS = eINSTANCE.getMSDModalMessageEvent_SafetyViolatingInActiveProcess();

		/**
		 * The meta object literal for the '<em><b>Parent Symbolic MSD Modal Message Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT = eINSTANCE.getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Concrete MSD Modal Message Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT = eINSTANCE.getMSDModalMessageEvent_ConcreteMSDModalMessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl <em>MSD Object System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDObjectSystem()
		 * @generated
		 */
		EClass MSD_OBJECT_SYSTEM = eINSTANCE.getMSDObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Runtime Util</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__RUNTIME_UTIL = eINSTANCE.getMSDObjectSystem_RuntimeUtil();

		/**
		 * The meta object literal for the '<em><b>Initializing Environment MSD Modal Message Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS = eINSTANCE.getMSDObjectSystem_InitializingEnvironmentMSDModalMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Initializing System MSD Modal Message Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS = eINSTANCE.getMSDObjectSystem_InitializingSystemMSDModalMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Assumption MSD Initializing Message Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS = eINSTANCE.getMSDObjectSystem_AssumptionMSDInitializingMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Requirement MSD Initializing Message Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS = eINSTANCE.getMSDObjectSystem_RequirementMSDInitializingMessageEvents();

		/**
		 * The meta object literal for the '<em><b>EObject To Sendable EOperation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP = eINSTANCE.getMSDObjectSystem_EObjectToSendableEOperationMap();

		/**
		 * The meta object literal for the '<em><b>EObject To Receivable EOperation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP = eINSTANCE.getMSDObjectSystem_EObjectToReceivableEOperationMap();

		/**
		 * The meta object literal for the '<em><b>EClass To Instance EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP = eINSTANCE.getMSDObjectSystem_EClassToInstanceEObjectMap();

		/**
		 * The meta object literal for the '<em><b>EObjects To UML Classes It Is Instance Of Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP = eINSTANCE.getMSDObjectSystem_EObjectsToUMLClassesItIsInstanceOfMap();

		/**
		 * The meta object literal for the '<em><b>Static Lifeline To EObject Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS = eINSTANCE.getMSDObjectSystem_StaticLifelineToEObjectBindings();

		/**
		 * The meta object literal for the '<em><b>Static Role To EObject Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS = eINSTANCE.getMSDObjectSystem_StaticRoleToEObjectBindings();

		/**
		 * The meta object literal for the '<em><b>Roles To EObjects Mapping Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER = eINSTANCE.getMSDObjectSystem_RolesToEObjectsMappingContainer();

		/**
		 * The meta object literal for the '<em><b>Message Event Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER = eINSTANCE.getMSDObjectSystem_MessageEventContainer();

		/**
		 * The meta object literal for the '<em><b>Message Event Key Wrapper To Message Event Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP = eINSTANCE.getMSDObjectSystem_MessageEventKeyWrapperToMessageEventMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.StringToStringMapEntryImpl <em>String To String Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.StringToStringMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToStringMapEntry()
		 * @generated
		 */
		EClass STRING_TO_STRING_MAP_ENTRY = eINSTANCE.getStringToStringMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP_ENTRY__KEY = eINSTANCE.getStringToStringMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP_ENTRY__VALUE = eINSTANCE.getStringToStringMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.StringToIntegerMapEntryImpl <em>String To Integer Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.StringToIntegerMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToIntegerMapEntry()
		 * @generated
		 */
		EClass STRING_TO_INTEGER_MAP_ENTRY = eINSTANCE.getStringToIntegerMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_INTEGER_MAP_ENTRY__KEY = eINSTANCE.getStringToIntegerMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_INTEGER_MAP_ENTRY__VALUE = eINSTANCE.getStringToIntegerMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.StringToBooleanMapEntryImpl <em>String To Boolean Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.StringToBooleanMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getStringToBooleanMapEntry()
		 * @generated
		 */
		EClass STRING_TO_BOOLEAN_MAP_ENTRY = eINSTANCE.getStringToBooleanMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_BOOLEAN_MAP_ENTRY__KEY = eINSTANCE.getStringToBooleanMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl <em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry()
		 * @generated
		 */
		EClass MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY = eINSTANCE.getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__VALUE = eINSTANCE.getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__KEY = eINSTANCE.getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveProcessKeyWrapperToActiveProcessMapEntryImpl <em>Active Process Key Wrapper To Active Process Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveProcessKeyWrapperToActiveProcessMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcessKeyWrapperToActiveProcessMapEntry()
		 * @generated
		 */
		EClass ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY = eINSTANCE.getActiveProcessKeyWrapperToActiveProcessMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__VALUE = eINSTANCE.getActiveProcessKeyWrapperToActiveProcessMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__KEY = eINSTANCE.getActiveProcessKeyWrapperToActiveProcessMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveStateKeyWrapperToActiveStateMapEntryImpl <em>Active State Key Wrapper To Active State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveStateKeyWrapperToActiveStateMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveStateKeyWrapperToActiveStateMapEntry()
		 * @generated
		 */
		EClass ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY = eINSTANCE.getActiveStateKeyWrapperToActiveStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__VALUE = eINSTANCE.getActiveStateKeyWrapperToActiveStateMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__KEY = eINSTANCE.getActiveStateKeyWrapperToActiveStateMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry()
		 * @generated
		 */
		EClass ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY = eINSTANCE.getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE = eINSTANCE.getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY = eINSTANCE.getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry()
		 * @generated
		 */
		EClass ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY = eINSTANCE.getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__VALUE = eINSTANCE.getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__KEY = eINSTANCE.getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry()
		 * @generated
		 */
		EClass ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY = eINSTANCE.getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__VALUE = eINSTANCE.getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__KEY = eINSTANCE.getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MessageEventContainerImpl <em>Message Event Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MessageEventContainerImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventContainer()
		 * @generated
		 */
		EClass MESSAGE_EVENT_CONTAINER = eINSTANCE.getMessageEventContainer();

		/**
		 * The meta object literal for the '<em><b>Runtime Messages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES = eINSTANCE.getMessageEventContainer_RuntimeMessages();

		/**
		 * The meta object literal for the '<em><b>Message Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS = eINSTANCE.getMessageEventContainer_MessageEvents();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.MessageEventKeyWrapperToMessageEventMapEntryImpl <em>Message Event Key Wrapper To Message Event Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.MessageEventKeyWrapperToMessageEventMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventKeyWrapperToMessageEventMapEntry()
		 * @generated
		 */
		EClass MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY = eINSTANCE.getMessageEventKeyWrapperToMessageEventMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE = eINSTANCE.getMessageEventKeyWrapperToMessageEventMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__KEY = eINSTANCE.getMessageEventKeyWrapperToMessageEventMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl <em>Object System Key Wrapper To MSD Object System Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ObjectSystemKeyWrapperToMSDObjectSystemMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
		 * @generated
		 */
		EClass OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY = eINSTANCE.getObjectSystemKeyWrapperToMSDObjectSystemMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__VALUE = eINSTANCE.getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__KEY = eINSTANCE.getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.ActiveMSSImpl <em>Active MSS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.ActiveMSSImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSS()
		 * @generated
		 */
		EClass ACTIVE_MSS = eINSTANCE.getActiveMSS();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSS__ROLE_BINDINGS = eINSTANCE.getActiveMSS_RoleBindings();

		/**
		 * The meta object literal for the '<em><b>State Machine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MSS__STATE_MACHINE = eINSTANCE.getActiveMSS_StateMachine();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.EObjectToEOperationMapEntryImpl <em>EObject To EOperation Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.EObjectToEOperationMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEObjectToEOperationMapEntry()
		 * @generated
		 */
		EClass EOBJECT_TO_EOPERATION_MAP_ENTRY = eINSTANCE.getEObjectToEOperationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_EOPERATION_MAP_ENTRY__KEY = eINSTANCE.getEObjectToEOperationMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_EOPERATION_MAP_ENTRY__VALUE = eINSTANCE.getEObjectToEOperationMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.EObjectToUMLClassMapEntryImpl <em>EObject To UML Class Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.EObjectToUMLClassMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEObjectToUMLClassMapEntry()
		 * @generated
		 */
		EClass EOBJECT_TO_UML_CLASS_MAP_ENTRY = eINSTANCE.getEObjectToUMLClassMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_UML_CLASS_MAP_ENTRY__KEY = eINSTANCE.getEObjectToUMLClassMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_TO_UML_CLASS_MAP_ENTRY__VALUE = eINSTANCE.getEObjectToUMLClassMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.impl.EClassToEObjectMapEntryImpl
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getEClassToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getEClassToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getEClassToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.ExecutionSemantics <em>Execution Semantics</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.ExecutionSemantics
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getExecutionSemantics()
		 * @generated
		 */
		EEnum EXECUTION_SEMANTICS = eINSTANCE.getExecutionSemantics();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_BOOLEAN_MAP_ENTRY__VALUE = eINSTANCE.getStringToBooleanMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.runtime.ActiveMSDProgress <em>Active MSD Progress</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.ActiveMSDProgress
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDProgress()
		 * @generated
		 */
		EEnum ACTIVE_MSD_PROGRESS = eINSTANCE.getActiveMSDProgress();

		/**
		 * The meta object literal for the '<em>Semantic Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ocl.SemanticException
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getSemanticException()
		 * @generated
		 */
		EDataType SEMANTIC_EXCEPTION = eINSTANCE.getSemanticException();

		/**
		 * The meta object literal for the '<em>Parser Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ocl.ParserException
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getParserException()
		 * @generated
		 */
		EDataType PARSER_EXCEPTION = eINSTANCE.getParserException();

		/**
		 * The meta object literal for the '<em>Active State Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveStateKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_STATE_KEY_WRAPPER = eINSTANCE.getActiveStateKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active MSD Lifeline Bindings Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDLifelineBindingsKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER = eINSTANCE.getActiveMSDLifelineBindingsKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active MSD Variable Valuations Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSDVariableValuationsKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER = eINSTANCE.getActiveMSDVariableValuationsKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active Process Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveProcessKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_PROCESS_KEY_WRAPPER = eINSTANCE.getActiveProcessKeyWrapper();

		/**
		 * The meta object literal for the '<em>MSD Runtime State Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMSDRuntimeStateKeyWrapper()
		 * @generated
		 */
		EDataType MSD_RUNTIME_STATE_KEY_WRAPPER = eINSTANCE.getMSDRuntimeStateKeyWrapper();

		/**
		 * The meta object literal for the '<em>Message Event Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getMessageEventKeyWrapper()
		 * @generated
		 */
		EDataType MESSAGE_EVENT_KEY_WRAPPER = eINSTANCE.getMessageEventKeyWrapper();

		/**
		 * The meta object literal for the '<em>Object System Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
		 * @generated
		 */
		EDataType OBJECT_SYSTEM_KEY_WRAPPER = eINSTANCE.getObjectSystemKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active MSS Role Bindings Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper
		 * @see org.scenariotools.msd.runtime.impl.RuntimePackageImpl#getActiveMSSRoleBindingsKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER = eINSTANCE.getActiveMSSRoleBindingsKeyWrapper();

	}

} //RuntimePackage
