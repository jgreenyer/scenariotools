/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSS State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSSState#getUmlState <em>Uml State</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSSState()
 * @model
 * @generated
 */
public interface ActiveMSSState extends ActiveState {
	/**
	 * Returns the value of the '<em><b>Uml State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml State</em>' reference.
	 * @see #setUmlState(State)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSSState_UmlState()
	 * @model
	 * @generated
	 */
	State getUmlState();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveMSSState#getUmlState <em>Uml State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml State</em>' reference.
	 * @see #getUmlState()
	 * @generated
	 */
	void setUmlState(State value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isTransitionEnabled(Transition transition);

} // ActiveMSSState
