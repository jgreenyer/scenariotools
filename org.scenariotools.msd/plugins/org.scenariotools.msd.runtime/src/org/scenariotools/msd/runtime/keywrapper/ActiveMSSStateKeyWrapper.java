package org.scenariotools.msd.runtime.keywrapper;

import org.eclipse.uml2.uml.State;
import org.scenariotools.msd.runtime.ActiveMSSState;

public class ActiveMSSStateKeyWrapper extends ActiveStateKeyWrapper {
	public ActiveMSSStateKeyWrapper(ActiveMSSState activeMSSState) {
		this(activeMSSState.getUmlState());
	}

	public ActiveMSSStateKeyWrapper(State state) {
		addSubObject(state);
	}
}
