/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.util.UtilPackage;
import org.scenariotools.msd.util.impl.UtilPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimePackageImpl extends EPackageImpl implements RuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdRuntimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdRuntimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdModalityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeProcessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSSRoleBindingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSSStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDCutEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDLifelineBindingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDVariableValuationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lifelineToInteractionFragmentMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lifelineToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementToMessageEventMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdModalMessageEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdObjectSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToStringMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToIntegerMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToBooleanMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeProcessKeyWrapperToActiveProcessMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeStateKeyWrapperToActiveStateMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventKeyWrapperToMessageEventMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMSSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectToEOperationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectToUMLClassMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eClassToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum executionSemanticsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum activeMSDProgressEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType semanticExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType parserExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeStateKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeMSDLifelineBindingsKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeMSDVariableValuationsKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeProcessKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType msdRuntimeStateKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType messageEventKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType objectSystemKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeMSSRoleBindingsKeyWrapperEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.runtime.RuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuntimePackageImpl() {
		super(eNS_URI, RuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuntimePackage init() {
		if (isInited) return (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Obtain or create and register package
		RuntimePackageImpl theRuntimePackage = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RuntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Roles2eobjectsPackage.eINSTANCE.eClass();
		org.scenariotools.runtime.RuntimePackage.eINSTANCE.eClass();
		Uml2ecorePackage.eINSTANCE.eClass();
		PrimitivemappingsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ScenariorunconfigurationPackageImpl theScenariorunconfigurationPackage = (ScenariorunconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI) instanceof ScenariorunconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI) : ScenariorunconfigurationPackage.eINSTANCE);
		UtilPackageImpl theUtilPackage = (UtilPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI) instanceof UtilPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI) : UtilPackage.eINSTANCE);

		// Create package meta-data objects
		theRuntimePackage.createPackageContents();
		theScenariorunconfigurationPackage.createPackageContents();
		theUtilPackage.createPackageContents();

		// Initialize created meta-data
		theRuntimePackage.initializePackageContents();
		theScenariorunconfigurationPackage.initializePackageContents();
		theUtilPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuntimePackage.eNS_URI, theRuntimePackage);
		return theRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDRuntimeStateGraph() {
		return msdRuntimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeStateGraph_ElementContainer() {
		return (EReference)msdRuntimeStateGraphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeStateGraph_RuntimeUtil() {
		return (EReference)msdRuntimeStateGraphEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeStateGraph_ScenarioRunConfiguration() {
		return (EReference)msdRuntimeStateGraphEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeStateGraph_MsdRuntimeStateKeyWrapperToMSDRuntimeStateMap() {
		return (EReference)msdRuntimeStateGraphEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDRuntimeState() {
		return msdRuntimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeState_ActiveProcesses() {
		return (EReference)msdRuntimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeState_RuntimeUtil() {
		return (EReference)msdRuntimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeState_ActiveMSDChangedDuringPerformStep() {
		return (EReference)msdRuntimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementContainer() {
		return elementContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveProcesses() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSDVariableValuations() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveStates() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSDLifelineBindings() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSSRoleBindings() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveProcessKeyWrapperToActiveProcessMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveStateKeyWrapperToActiveStateMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ObjectSystemKeyWrapperToMSDObjectSystemMapEntry() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ObjectSystems() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_FinalFragment() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_MsdRuntimeStateGraph() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveRoleBindings() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDModality() {
		return msdModalityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDModality_Monitored() {
		return (EAttribute)msdModalityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDModality_Hot() {
		return (EAttribute)msdModalityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDModality_Cold() {
		return (EAttribute)msdModalityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDModality_Initializing() {
		return (EAttribute)msdModalityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDModality_ColdViolating() {
		return (EAttribute)msdModalityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveProcess() {
		return activeProcessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_VariableValuations() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_ColdForbiddenEvents() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_HotForbiddenEvents() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_ViolatingEvents() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_EnabledEvents() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_RuntimeUtil() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_CurrentState() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcess_ObjectSystem() {
		return (EReference)activeProcessEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveProcess_Id() {
		return (EAttribute)activeProcessEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSD() {
		return activeMSDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyToEObjectMapEntry() {
		return propertyToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyToEObjectMapEntry_Key() {
		return (EReference)propertyToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyToEObjectMapEntry_Value() {
		return (EReference)propertyToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveState() {
		return activeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveState_Hot() {
		return (EAttribute)activeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveState_RuntimeUtil() {
		return (EReference)activeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveState_Executed() {
		return (EAttribute)activeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSSState() {
		return activeMSSStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSSState_UmlState() {
		return (EReference)activeMSSStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSD_LifelineBindings() {
		return (EReference)activeMSDEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSD_Interaction() {
		return (EReference)activeMSDEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSD_MsdUtil() {
		return (EReference)activeMSDEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSSRoleBindings() {
		return activeMSSRoleBindingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSSRoleBindings_PropertyToEObjectMap() {
		return (EReference)activeMSSRoleBindingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSDCut() {
		return activeMSDCutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDCut_LifelineToInteractionFragmentMap() {
		return (EReference)activeMSDCutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSDLifelineBindings() {
		return activeMSDLifelineBindingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDLifelineBindings_LifelineToEObjectMap() {
		return (EReference)activeMSDLifelineBindingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDLifelineBindings_IgnoredLifelines() {
		return (EReference)activeMSDLifelineBindingsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSDVariableValuations() {
		return activeMSDVariableValuationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDVariableValuations_VariableNameToEObjectValueMap() {
		return (EReference)activeMSDVariableValuationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDVariableValuations_VariableNameToStringValueMap() {
		return (EReference)activeMSDVariableValuationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDVariableValuations_VariableNameToIntegerValueMap() {
		return (EReference)activeMSDVariableValuationsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDVariableValuations_VariableNameToBooleanValueMap() {
		return (EReference)activeMSDVariableValuationsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLifelineToInteractionFragmentMapEntry() {
		return lifelineToInteractionFragmentMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToInteractionFragmentMapEntry_Key() {
		return (EReference)lifelineToInteractionFragmentMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToInteractionFragmentMapEntry_Value() {
		return (EReference)lifelineToInteractionFragmentMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToEObjectMapEntry() {
		return stringToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToEObjectMapEntry_Key() {
		return (EAttribute)stringToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToEObjectMapEntry_Value() {
		return (EReference)stringToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLifelineToEObjectMapEntry() {
		return lifelineToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToEObjectMapEntry_Key() {
		return (EReference)lifelineToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToEObjectMapEntry_Value() {
		return (EReference)lifelineToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElementToMessageEventMapEntry() {
		return namedElementToMessageEventMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNamedElementToMessageEventMapEntry_Key() {
		return (EReference)namedElementToMessageEventMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNamedElementToMessageEventMapEntry_Value() {
		return (EReference)namedElementToMessageEventMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDModalMessageEvent() {
		return msdModalMessageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDModalMessageEvent_EnabledInActiveProcess() {
		return (EReference)msdModalMessageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDModalMessageEvent_ColdViolatingInActiveProcess() {
		return (EReference)msdModalMessageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDModalMessageEvent_SafetyViolatingInActiveProcess() {
		return (EReference)msdModalMessageEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent() {
		return (EReference)msdModalMessageEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDModalMessageEvent_ConcreteMSDModalMessageEvent() {
		return (EReference)msdModalMessageEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDObjectSystem() {
		return msdObjectSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_RuntimeUtil() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_InitializingEnvironmentMSDModalMessageEvents() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_InitializingSystemMSDModalMessageEvents() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_AssumptionMSDInitializingMessageEvents() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_RequirementMSDInitializingMessageEvents() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_EObjectToSendableEOperationMap() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_EObjectToReceivableEOperationMap() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_EClassToInstanceEObjectMap() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_EObjectsToUMLClassesItIsInstanceOfMap() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_StaticLifelineToEObjectBindings() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_StaticRoleToEObjectBindings() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_RolesToEObjectsMappingContainer() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_MessageEventContainer() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDObjectSystem_MessageEventKeyWrapperToMessageEventMap() {
		return (EReference)msdObjectSystemEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToStringMapEntry() {
		return stringToStringMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMapEntry_Key() {
		return (EAttribute)stringToStringMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMapEntry_Value() {
		return (EAttribute)stringToStringMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToIntegerMapEntry() {
		return stringToIntegerMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToIntegerMapEntry_Key() {
		return (EAttribute)stringToIntegerMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToIntegerMapEntry_Value() {
		return (EAttribute)stringToIntegerMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToBooleanMapEntry() {
		return stringToBooleanMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToBooleanMapEntry_Key() {
		return (EAttribute)stringToBooleanMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry() {
		return msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Value() {
		return (EReference)msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Key() {
		return (EAttribute)msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveProcessKeyWrapperToActiveProcessMapEntry() {
		return activeProcessKeyWrapperToActiveProcessMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveProcessKeyWrapperToActiveProcessMapEntry_Value() {
		return (EReference)activeProcessKeyWrapperToActiveProcessMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveProcessKeyWrapperToActiveProcessMapEntry_Key() {
		return (EAttribute)activeProcessKeyWrapperToActiveProcessMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveStateKeyWrapperToActiveStateMapEntry() {
		return activeStateKeyWrapperToActiveStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveStateKeyWrapperToActiveStateMapEntry_Value() {
		return (EReference)activeStateKeyWrapperToActiveStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveStateKeyWrapperToActiveStateMapEntry_Key() {
		return (EAttribute)activeStateKeyWrapperToActiveStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry() {
		return activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Value() {
		return (EReference)activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Key() {
		return (EAttribute)activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry() {
		return activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Value() {
		return (EReference)activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Key() {
		return (EAttribute)activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry() {
		return activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Value() {
		return (EReference)activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Key() {
		return (EAttribute)activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventContainer() {
		return messageEventContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventContainer_RuntimeMessages() {
		return (EReference)messageEventContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventContainer_MessageEvents() {
		return (EReference)messageEventContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventKeyWrapperToMessageEventMapEntry() {
		return messageEventKeyWrapperToMessageEventMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventKeyWrapperToMessageEventMapEntry_Value() {
		return (EReference)messageEventKeyWrapperToMessageEventMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEventKeyWrapperToMessageEventMapEntry_Key() {
		return (EAttribute)messageEventKeyWrapperToMessageEventMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSystemKeyWrapperToMSDObjectSystemMapEntry() {
		return objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Value() {
		return (EReference)objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Key() {
		return (EAttribute)objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMSS() {
		return activeMSSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSS_RoleBindings() {
		return (EReference)activeMSSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMSS_StateMachine() {
		return (EReference)activeMSSEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectToEOperationMapEntry() {
		return eObjectToEOperationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToEOperationMapEntry_Key() {
		return (EReference)eObjectToEOperationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToEOperationMapEntry_Value() {
		return (EReference)eObjectToEOperationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectToUMLClassMapEntry() {
		return eObjectToUMLClassMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToUMLClassMapEntry_Key() {
		return (EReference)eObjectToUMLClassMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectToUMLClassMapEntry_Value() {
		return (EReference)eObjectToUMLClassMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEClassToEObjectMapEntry() {
		return eClassToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToEObjectMapEntry_Value() {
		return (EReference)eClassToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToEObjectMapEntry_Key() {
		return (EReference)eClassToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getExecutionSemantics() {
		return executionSemanticsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToBooleanMapEntry_Value() {
		return (EAttribute)stringToBooleanMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActiveMSDProgress() {
		return activeMSDProgressEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSemanticException() {
		return semanticExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getParserException() {
		return parserExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveStateKeyWrapper() {
		return activeStateKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveMSDLifelineBindingsKeyWrapper() {
		return activeMSDLifelineBindingsKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveMSDVariableValuationsKeyWrapper() {
		return activeMSDVariableValuationsKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveProcessKeyWrapper() {
		return activeProcessKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getMSDRuntimeStateKeyWrapper() {
		return msdRuntimeStateKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getMessageEventKeyWrapper() {
		return messageEventKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getObjectSystemKeyWrapper() {
		return objectSystemKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveMSSRoleBindingsKeyWrapper() {
		return activeMSSRoleBindingsKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactory getRuntimeFactory() {
		return (RuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		msdRuntimeStateGraphEClass = createEClass(MSD_RUNTIME_STATE_GRAPH);
		createEReference(msdRuntimeStateGraphEClass, MSD_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER);
		createEReference(msdRuntimeStateGraphEClass, MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL);
		createEReference(msdRuntimeStateGraphEClass, MSD_RUNTIME_STATE_GRAPH__SCENARIO_RUN_CONFIGURATION);
		createEReference(msdRuntimeStateGraphEClass, MSD_RUNTIME_STATE_GRAPH__MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP);

		msdRuntimeStateEClass = createEClass(MSD_RUNTIME_STATE);
		createEReference(msdRuntimeStateEClass, MSD_RUNTIME_STATE__ACTIVE_PROCESSES);
		createEReference(msdRuntimeStateEClass, MSD_RUNTIME_STATE__RUNTIME_UTIL);
		createEReference(msdRuntimeStateEClass, MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP);

		elementContainerEClass = createEClass(ELEMENT_CONTAINER);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_PROCESSES);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_STATES);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__OBJECT_SYSTEMS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__FINAL_FRAGMENT);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__MSD_RUNTIME_STATE_GRAPH);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_ROLE_BINDINGS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP);

		msdModalityEClass = createEClass(MSD_MODALITY);
		createEAttribute(msdModalityEClass, MSD_MODALITY__MONITORED);
		createEAttribute(msdModalityEClass, MSD_MODALITY__HOT);
		createEAttribute(msdModalityEClass, MSD_MODALITY__COLD);
		createEAttribute(msdModalityEClass, MSD_MODALITY__INITIALIZING);
		createEAttribute(msdModalityEClass, MSD_MODALITY__COLD_VIOLATING);

		activeProcessEClass = createEClass(ACTIVE_PROCESS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__VARIABLE_VALUATIONS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__COLD_FORBIDDEN_EVENTS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__HOT_FORBIDDEN_EVENTS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__VIOLATING_EVENTS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__ENABLED_EVENTS);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__RUNTIME_UTIL);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__CURRENT_STATE);
		createEReference(activeProcessEClass, ACTIVE_PROCESS__OBJECT_SYSTEM);
		createEAttribute(activeProcessEClass, ACTIVE_PROCESS__ID);

		activeMSDCutEClass = createEClass(ACTIVE_MSD_CUT);
		createEReference(activeMSDCutEClass, ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP);

		activeMSDLifelineBindingsEClass = createEClass(ACTIVE_MSD_LIFELINE_BINDINGS);
		createEReference(activeMSDLifelineBindingsEClass, ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP);
		createEReference(activeMSDLifelineBindingsEClass, ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES);

		activeMSDVariableValuationsEClass = createEClass(ACTIVE_MSD_VARIABLE_VALUATIONS);
		createEReference(activeMSDVariableValuationsEClass, ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_EOBJECT_VALUE_MAP);
		createEReference(activeMSDVariableValuationsEClass, ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_STRING_VALUE_MAP);
		createEReference(activeMSDVariableValuationsEClass, ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_INTEGER_VALUE_MAP);
		createEReference(activeMSDVariableValuationsEClass, ACTIVE_MSD_VARIABLE_VALUATIONS__VARIABLE_NAME_TO_BOOLEAN_VALUE_MAP);

		lifelineToInteractionFragmentMapEntryEClass = createEClass(LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY);
		createEReference(lifelineToInteractionFragmentMapEntryEClass, LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__KEY);
		createEReference(lifelineToInteractionFragmentMapEntryEClass, LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY__VALUE);

		stringToEObjectMapEntryEClass = createEClass(STRING_TO_EOBJECT_MAP_ENTRY);
		createEReference(stringToEObjectMapEntryEClass, STRING_TO_EOBJECT_MAP_ENTRY__VALUE);
		createEAttribute(stringToEObjectMapEntryEClass, STRING_TO_EOBJECT_MAP_ENTRY__KEY);

		lifelineToEObjectMapEntryEClass = createEClass(LIFELINE_TO_EOBJECT_MAP_ENTRY);
		createEReference(lifelineToEObjectMapEntryEClass, LIFELINE_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(lifelineToEObjectMapEntryEClass, LIFELINE_TO_EOBJECT_MAP_ENTRY__VALUE);

		namedElementToMessageEventMapEntryEClass = createEClass(NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY);
		createEReference(namedElementToMessageEventMapEntryEClass, NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__KEY);
		createEReference(namedElementToMessageEventMapEntryEClass, NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE);

		msdModalMessageEventEClass = createEClass(MSD_MODAL_MESSAGE_EVENT);
		createEReference(msdModalMessageEventEClass, MSD_MODAL_MESSAGE_EVENT__ENABLED_IN_ACTIVE_PROCESS);
		createEReference(msdModalMessageEventEClass, MSD_MODAL_MESSAGE_EVENT__COLD_VIOLATING_IN_ACTIVE_PROCESS);
		createEReference(msdModalMessageEventEClass, MSD_MODAL_MESSAGE_EVENT__SAFETY_VIOLATING_IN_ACTIVE_PROCESS);
		createEReference(msdModalMessageEventEClass, MSD_MODAL_MESSAGE_EVENT__PARENT_SYMBOLIC_MSD_MODAL_MESSAGE_EVENT);
		createEReference(msdModalMessageEventEClass, MSD_MODAL_MESSAGE_EVENT__CONCRETE_MSD_MODAL_MESSAGE_EVENT);

		msdObjectSystemEClass = createEClass(MSD_OBJECT_SYSTEM);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__RUNTIME_UTIL);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER);
		createEReference(msdObjectSystemEClass, MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP);

		stringToStringMapEntryEClass = createEClass(STRING_TO_STRING_MAP_ENTRY);
		createEAttribute(stringToStringMapEntryEClass, STRING_TO_STRING_MAP_ENTRY__KEY);
		createEAttribute(stringToStringMapEntryEClass, STRING_TO_STRING_MAP_ENTRY__VALUE);

		stringToIntegerMapEntryEClass = createEClass(STRING_TO_INTEGER_MAP_ENTRY);
		createEAttribute(stringToIntegerMapEntryEClass, STRING_TO_INTEGER_MAP_ENTRY__KEY);
		createEAttribute(stringToIntegerMapEntryEClass, STRING_TO_INTEGER_MAP_ENTRY__VALUE);

		stringToBooleanMapEntryEClass = createEClass(STRING_TO_BOOLEAN_MAP_ENTRY);
		createEAttribute(stringToBooleanMapEntryEClass, STRING_TO_BOOLEAN_MAP_ENTRY__VALUE);
		createEAttribute(stringToBooleanMapEntryEClass, STRING_TO_BOOLEAN_MAP_ENTRY__KEY);

		msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass = createEClass(MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY);
		createEReference(msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass, MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__VALUE);
		createEAttribute(msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass, MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY__KEY);

		activeProcessKeyWrapperToActiveProcessMapEntryEClass = createEClass(ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY);
		createEReference(activeProcessKeyWrapperToActiveProcessMapEntryEClass, ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__VALUE);
		createEAttribute(activeProcessKeyWrapperToActiveProcessMapEntryEClass, ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY__KEY);

		activeStateKeyWrapperToActiveStateMapEntryEClass = createEClass(ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY);
		createEReference(activeStateKeyWrapperToActiveStateMapEntryEClass, ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__VALUE);
		createEAttribute(activeStateKeyWrapperToActiveStateMapEntryEClass, ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY__KEY);

		activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass = createEClass(ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY);
		createEReference(activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass, ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE);
		createEAttribute(activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass, ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY);

		activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass = createEClass(ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY);
		createEReference(activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass, ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__VALUE);
		createEAttribute(activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass, ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY__KEY);

		activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass = createEClass(ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY);
		createEReference(activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass, ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__VALUE);
		createEAttribute(activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass, ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY__KEY);

		messageEventContainerEClass = createEClass(MESSAGE_EVENT_CONTAINER);
		createEReference(messageEventContainerEClass, MESSAGE_EVENT_CONTAINER__RUNTIME_MESSAGES);
		createEReference(messageEventContainerEClass, MESSAGE_EVENT_CONTAINER__MESSAGE_EVENTS);

		messageEventKeyWrapperToMessageEventMapEntryEClass = createEClass(MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY);
		createEReference(messageEventKeyWrapperToMessageEventMapEntryEClass, MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__VALUE);
		createEAttribute(messageEventKeyWrapperToMessageEventMapEntryEClass, MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY__KEY);

		objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass = createEClass(OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY);
		createEReference(objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__VALUE);
		createEAttribute(objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY__KEY);

		activeMSSEClass = createEClass(ACTIVE_MSS);
		createEReference(activeMSSEClass, ACTIVE_MSS__ROLE_BINDINGS);
		createEReference(activeMSSEClass, ACTIVE_MSS__STATE_MACHINE);

		activeMSDEClass = createEClass(ACTIVE_MSD);
		createEReference(activeMSDEClass, ACTIVE_MSD__LIFELINE_BINDINGS);
		createEReference(activeMSDEClass, ACTIVE_MSD__INTERACTION);
		createEReference(activeMSDEClass, ACTIVE_MSD__MSD_UTIL);

		activeMSSRoleBindingsEClass = createEClass(ACTIVE_MSS_ROLE_BINDINGS);
		createEReference(activeMSSRoleBindingsEClass, ACTIVE_MSS_ROLE_BINDINGS__PROPERTY_TO_EOBJECT_MAP);

		propertyToEObjectMapEntryEClass = createEClass(PROPERTY_TO_EOBJECT_MAP_ENTRY);
		createEReference(propertyToEObjectMapEntryEClass, PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(propertyToEObjectMapEntryEClass, PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE);

		activeStateEClass = createEClass(ACTIVE_STATE);
		createEAttribute(activeStateEClass, ACTIVE_STATE__HOT);
		createEReference(activeStateEClass, ACTIVE_STATE__RUNTIME_UTIL);
		createEAttribute(activeStateEClass, ACTIVE_STATE__EXECUTED);

		activeMSSStateEClass = createEClass(ACTIVE_MSS_STATE);
		createEReference(activeMSSStateEClass, ACTIVE_MSS_STATE__UML_STATE);

		eObjectToEOperationMapEntryEClass = createEClass(EOBJECT_TO_EOPERATION_MAP_ENTRY);
		createEReference(eObjectToEOperationMapEntryEClass, EOBJECT_TO_EOPERATION_MAP_ENTRY__KEY);
		createEReference(eObjectToEOperationMapEntryEClass, EOBJECT_TO_EOPERATION_MAP_ENTRY__VALUE);

		eObjectToUMLClassMapEntryEClass = createEClass(EOBJECT_TO_UML_CLASS_MAP_ENTRY);
		createEReference(eObjectToUMLClassMapEntryEClass, EOBJECT_TO_UML_CLASS_MAP_ENTRY__KEY);
		createEReference(eObjectToUMLClassMapEntryEClass, EOBJECT_TO_UML_CLASS_MAP_ENTRY__VALUE);

		eClassToEObjectMapEntryEClass = createEClass(ECLASS_TO_EOBJECT_MAP_ENTRY);
		createEReference(eClassToEObjectMapEntryEClass, ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE);
		createEReference(eClassToEObjectMapEntryEClass, ECLASS_TO_EOBJECT_MAP_ENTRY__KEY);

		// Create enums
		executionSemanticsEEnum = createEEnum(EXECUTION_SEMANTICS);
		activeMSDProgressEEnum = createEEnum(ACTIVE_MSD_PROGRESS);

		// Create data types
		semanticExceptionEDataType = createEDataType(SEMANTIC_EXCEPTION);
		parserExceptionEDataType = createEDataType(PARSER_EXCEPTION);
		activeStateKeyWrapperEDataType = createEDataType(ACTIVE_STATE_KEY_WRAPPER);
		activeMSDLifelineBindingsKeyWrapperEDataType = createEDataType(ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER);
		activeMSDVariableValuationsKeyWrapperEDataType = createEDataType(ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER);
		activeProcessKeyWrapperEDataType = createEDataType(ACTIVE_PROCESS_KEY_WRAPPER);
		msdRuntimeStateKeyWrapperEDataType = createEDataType(MSD_RUNTIME_STATE_KEY_WRAPPER);
		messageEventKeyWrapperEDataType = createEDataType(MESSAGE_EVENT_KEY_WRAPPER);
		objectSystemKeyWrapperEDataType = createEDataType(OBJECT_SYSTEM_KEY_WRAPPER);
		activeMSSRoleBindingsKeyWrapperEDataType = createEDataType(ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		org.scenariotools.runtime.RuntimePackage theRuntimePackage_1 = (org.scenariotools.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.runtime.RuntimePackage.eNS_URI);
		UtilPackage theUtilPackage = (UtilPackage)EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI);
		ScenariorunconfigurationPackage theScenariorunconfigurationPackage = (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		Roles2eobjectsPackage theRoles2eobjectsPackage = (Roles2eobjectsPackage)EPackage.Registry.INSTANCE.getEPackage(Roles2eobjectsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		msdRuntimeStateGraphEClass.getESuperTypes().add(theRuntimePackage_1.getRuntimeStateGraph());
		msdRuntimeStateEClass.getESuperTypes().add(theRuntimePackage_1.getRuntimeState());
		msdModalityEClass.getESuperTypes().add(theRuntimePackage_1.getModality());
		activeMSDCutEClass.getESuperTypes().add(this.getActiveState());
		msdModalMessageEventEClass.getESuperTypes().add(theRuntimePackage_1.getModalMessageEvent());
		msdObjectSystemEClass.getESuperTypes().add(theRuntimePackage_1.getObjectSystem());
		activeMSSEClass.getESuperTypes().add(this.getActiveProcess());
		activeMSDEClass.getESuperTypes().add(this.getActiveProcess());
		activeMSSStateEClass.getESuperTypes().add(this.getActiveState());

		// Initialize classes and features; add operations and parameters
		initEClass(msdRuntimeStateGraphEClass, MSDRuntimeStateGraph.class, "MSDRuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSDRuntimeStateGraph_ElementContainer(), this.getElementContainer(), this.getElementContainer_MsdRuntimeStateGraph(), "elementContainer", null, 0, 1, MSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDRuntimeStateGraph_RuntimeUtil(), theUtilPackage.getRuntimeUtil(), theUtilPackage.getRuntimeUtil_MsdRuntimeStateGraph(), "runtimeUtil", null, 0, 1, MSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDRuntimeStateGraph_ScenarioRunConfiguration(), theScenariorunconfigurationPackage.getScenarioRunConfiguration(), null, "scenarioRunConfiguration", null, 0, 1, MSDRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDRuntimeStateGraph_MsdRuntimeStateKeyWrapperToMSDRuntimeStateMap(), this.getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry(), null, "msdRuntimeStateKeyWrapperToMSDRuntimeStateMap", null, 0, -1, MSDRuntimeStateGraph.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(msdRuntimeStateGraphEClass, this.getMSDRuntimeState(), "getMSDRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMSDRuntimeState(), "msdRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveProcess(), "unchangedActiveMSDs", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdRuntimeStateGraphEClass, this.getMSDRuntimeState(), "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theScenariorunconfigurationPackage.getScenarioRunConfiguration(), "scenarioRunConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(msdRuntimeStateEClass, MSDRuntimeState.class, "MSDRuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSDRuntimeState_ActiveProcesses(), this.getActiveProcess(), null, "activeProcesses", null, 0, -1, MSDRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDRuntimeState_RuntimeUtil(), theUtilPackage.getRuntimeUtil(), null, "runtimeUtil", null, 0, 1, MSDRuntimeState.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDRuntimeState_ActiveMSDChangedDuringPerformStep(), this.getActiveProcess(), null, "activeMSDChangedDuringPerformStep", null, 0, -1, MSDRuntimeState.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(msdRuntimeStateEClass, null, "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdRuntimeStateEClass, null, "updateMSDModalMessageEvents", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getExecutionSemantics(), "executionSemantics", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(msdRuntimeStateEClass, theEcorePackage.getEBoolean(), "isInExecutedSystemCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(msdRuntimeStateEClass, theEcorePackage.getEBoolean(), "isInExecutedEnvironmentCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(msdRuntimeStateEClass, theEcorePackage.getEBoolean(), "isInHotEnvironmentCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(msdRuntimeStateEClass, theEcorePackage.getEBoolean(), "isInHotSystemCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(elementContainerEClass, ElementContainer.class, "ElementContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getElementContainer_ActiveProcesses(), this.getActiveProcess(), null, "activeProcesses", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSDVariableValuations(), this.getActiveMSDVariableValuations(), null, "activeMSDVariableValuations", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveStates(), this.getActiveState(), null, "activeStates", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSDLifelineBindings(), this.getActiveMSDLifelineBindings(), null, "activeMSDLifelineBindings", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSSRoleBindings(), this.getActiveMSSRoleBindings(), null, "activeMSSRoleBindings", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveProcessKeyWrapperToActiveProcessMap(), this.getActiveProcessKeyWrapperToActiveProcessMapEntry(), null, "activeProcessKeyWrapperToActiveProcessMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveStateKeyWrapperToActiveStateMap(), this.getActiveStateKeyWrapperToActiveStateMapEntry(), null, "activeStateKeyWrapperToActiveStateMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap(), this.getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry(), null, "activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap(), this.getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry(), null, "activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ObjectSystemKeyWrapperToMSDObjectSystemMapEntry(), this.getObjectSystemKeyWrapperToMSDObjectSystemMapEntry(), null, "objectSystemKeyWrapperToMSDObjectSystemMapEntry", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ObjectSystems(), this.getMSDObjectSystem(), null, "objectSystems", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_FinalFragment(), theUMLPackage.getInteractionFragment(), null, "finalFragment", null, 0, 1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_MsdRuntimeStateGraph(), this.getMSDRuntimeStateGraph(), this.getMSDRuntimeStateGraph_ElementContainer(), "msdRuntimeStateGraph", null, 0, 1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveRoleBindings(), this.getActiveMSSRoleBindings(), null, "activeRoleBindings", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap(), this.getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry(), null, "activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(elementContainerEClass, this.getActiveProcess(), "getActiveProcess", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveProcess(), "activeProcess", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(elementContainerEClass, this.getActiveState(), "getActiveState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveState(), "activeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(elementContainerEClass, this.getActiveMSDLifelineBindings(), "getActiveMSDLifelineBindings", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveMSDLifelineBindings(), "activeMSDLifelineBindings", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(elementContainerEClass, this.getActiveMSDVariableValuations(), "getActiveMSDVariableValuations", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveMSDVariableValuations(), "activeMSDVariableValuations", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(elementContainerEClass, this.getMSDObjectSystem(), "getObjectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMSDObjectSystem(), "objectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(msdModalityEClass, MSDModality.class, "MSDModality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMSDModality_Monitored(), theEcorePackage.getEBoolean(), "monitored", null, 0, 1, MSDModality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSDModality_Hot(), theEcorePackage.getEBoolean(), "hot", null, 0, 1, MSDModality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSDModality_Cold(), theEcorePackage.getEBoolean(), "cold", null, 0, 1, MSDModality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSDModality_Initializing(), theEcorePackage.getEBoolean(), "initializing", null, 0, 1, MSDModality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSDModality_ColdViolating(), theEcorePackage.getEBoolean(), "coldViolating", null, 0, 1, MSDModality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeProcessEClass, ActiveProcess.class, "ActiveProcess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveProcess_VariableValuations(), this.getActiveMSDVariableValuations(), null, "variableValuations", null, 0, 1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_ColdForbiddenEvents(), this.getNamedElementToMessageEventMapEntry(), null, "coldForbiddenEvents", null, 0, -1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_HotForbiddenEvents(), this.getNamedElementToMessageEventMapEntry(), null, "hotForbiddenEvents", null, 0, -1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_ViolatingEvents(), this.getNamedElementToMessageEventMapEntry(), null, "violatingEvents", null, 0, -1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_EnabledEvents(), this.getNamedElementToMessageEventMapEntry(), null, "enabledEvents", null, 0, -1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_RuntimeUtil(), theUtilPackage.getRuntimeUtil(), null, "runtimeUtil", null, 0, 1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_CurrentState(), this.getActiveState(), null, "currentState", null, 0, 1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveProcess_ObjectSystem(), this.getMSDObjectSystem(), null, "objectSystem", null, 0, 1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveProcess_Id(), ecorePackage.getEString(), "id", null, 0, 1, ActiveProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(activeProcessEClass, ecorePackage.getEBoolean(), "isSafetyViolating", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(activeProcessEClass, ecorePackage.getEBoolean(), "isColdViolating", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(activeProcessEClass, null, "calculateRelevantEvents", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(activeProcessEClass, null, "assignValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getOCL(), "ocl", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "variableName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getPlainJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(activeProcessEClass, theUtilPackage.getPlainJavaObject(), "assignValueExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getOCL(), "ocl", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "variableName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "valueOCLExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getSemanticException());
		addEException(op, this.getParserException());

		op = addEOperation(activeProcessEClass, ecorePackage.getEBoolean(), "isRelavant", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(activeProcessEClass, this.getActiveMSDProgress(), "progressEnabledHiddenEvents", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(activeProcessEClass, theEcorePackage.getEBoolean(), "isInHotCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(activeProcessEClass, theEcorePackage.getEBoolean(), "isInExecutedCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeMSDCutEClass, ActiveMSDCut.class, "ActiveMSDCut", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSDCut_LifelineToInteractionFragmentMap(), this.getLifelineToInteractionFragmentMapEntry(), null, "lifelineToInteractionFragmentMap", null, 0, -1, ActiveMSDCut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(activeMSDCutEClass, ecorePackage.getEBoolean(), "progressCutLocationOnLifeline", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getLifeline(), "lifeline", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getInteractionFragment(), "currentFragment", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(activeMSDCutEClass, ecorePackage.getEBoolean(), "terminalCutReached", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(activeMSDCutEClass, theEcorePackage.getEBoolean(), "isMessageEnabled", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getMessage(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeMSDLifelineBindingsEClass, ActiveMSDLifelineBindings.class, "ActiveMSDLifelineBindings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSDLifelineBindings_LifelineToEObjectMap(), this.getLifelineToEObjectMapEntry(), null, "lifelineToEObjectMap", null, 0, -1, ActiveMSDLifelineBindings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSDLifelineBindings_IgnoredLifelines(), theUMLPackage.getLifeline(), null, "ignoredLifelines", null, 0, -1, ActiveMSDLifelineBindings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(activeMSDVariableValuationsEClass, ActiveMSDVariableValuations.class, "ActiveMSDVariableValuations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSDVariableValuations_VariableNameToEObjectValueMap(), this.getStringToEObjectMapEntry(), null, "variableNameToEObjectValueMap", null, 0, -1, ActiveMSDVariableValuations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSDVariableValuations_VariableNameToStringValueMap(), this.getStringToStringMapEntry(), null, "variableNameToStringValueMap", null, 0, -1, ActiveMSDVariableValuations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSDVariableValuations_VariableNameToIntegerValueMap(), this.getStringToIntegerMapEntry(), null, "variableNameToIntegerValueMap", null, 0, -1, ActiveMSDVariableValuations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSDVariableValuations_VariableNameToBooleanValueMap(), this.getStringToBooleanMapEntry(), null, "variableNameToBooleanValueMap", null, 0, -1, ActiveMSDVariableValuations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lifelineToInteractionFragmentMapEntryEClass, Map.Entry.class, "LifelineToInteractionFragmentMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLifelineToInteractionFragmentMapEntry_Key(), theUMLPackage.getLifeline(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLifelineToInteractionFragmentMapEntry_Value(), theUMLPackage.getInteractionFragment(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToEObjectMapEntryEClass, Map.Entry.class, "StringToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStringToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToEObjectMapEntry_Key(), theEcorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lifelineToEObjectMapEntryEClass, Map.Entry.class, "LifelineToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLifelineToEObjectMapEntry_Key(), theUMLPackage.getLifeline(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLifelineToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementToMessageEventMapEntryEClass, Map.Entry.class, "NamedElementToMessageEventMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNamedElementToMessageEventMapEntry_Key(), theUMLPackage.getNamedElement(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNamedElementToMessageEventMapEntry_Value(), theEventsPackage.getMessageEvent(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(msdModalMessageEventEClass, MSDModalMessageEvent.class, "MSDModalMessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSDModalMessageEvent_EnabledInActiveProcess(), this.getActiveProcess(), null, "enabledInActiveProcess", null, 0, -1, MSDModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDModalMessageEvent_ColdViolatingInActiveProcess(), this.getActiveProcess(), null, "coldViolatingInActiveProcess", null, 0, -1, MSDModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDModalMessageEvent_SafetyViolatingInActiveProcess(), this.getActiveProcess(), null, "safetyViolatingInActiveProcess", null, 0, -1, MSDModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent(), this.getMSDModalMessageEvent(), this.getMSDModalMessageEvent_ConcreteMSDModalMessageEvent(), "parentSymbolicMSDModalMessageEvent", null, 0, 1, MSDModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDModalMessageEvent_ConcreteMSDModalMessageEvent(), this.getMSDModalMessageEvent(), this.getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent(), "concreteMSDModalMessageEvent", null, 0, -1, MSDModalMessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(msdObjectSystemEClass, MSDObjectSystem.class, "MSDObjectSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSDObjectSystem_RuntimeUtil(), theUtilPackage.getRuntimeUtil(), null, "runtimeUtil", null, 0, 1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_InitializingEnvironmentMSDModalMessageEvents(), this.getMSDModalMessageEvent(), null, "initializingEnvironmentMSDModalMessageEvents", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_InitializingSystemMSDModalMessageEvents(), this.getMSDModalMessageEvent(), null, "initializingSystemMSDModalMessageEvents", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_AssumptionMSDInitializingMessageEvents(), theEventsPackage.getMessageEvent(), null, "assumptionMSDInitializingMessageEvents", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_RequirementMSDInitializingMessageEvents(), theEventsPackage.getMessageEvent(), null, "requirementMSDInitializingMessageEvents", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_EObjectToSendableEOperationMap(), this.getEObjectToEOperationMapEntry(), null, "eObjectToSendableEOperationMap", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_EObjectToReceivableEOperationMap(), this.getEObjectToEOperationMapEntry(), null, "eObjectToReceivableEOperationMap", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_EClassToInstanceEObjectMap(), this.getEClassToEObjectMapEntry(), null, "eClassToInstanceEObjectMap", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_EObjectsToUMLClassesItIsInstanceOfMap(), this.getEObjectToUMLClassMapEntry(), null, "eObjectsToUMLClassesItIsInstanceOfMap", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_StaticLifelineToEObjectBindings(), this.getLifelineToEObjectMapEntry(), null, "staticLifelineToEObjectBindings", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_StaticRoleToEObjectBindings(), this.getPropertyToEObjectMapEntry(), null, "staticRoleToEObjectBindings", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_RolesToEObjectsMappingContainer(), theRoles2eobjectsPackage.getRoleToEObjectMappingContainer(), null, "rolesToEObjectsMappingContainer", null, 0, 1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_MessageEventContainer(), this.getMessageEventContainer(), null, "messageEventContainer", null, 0, 1, MSDObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDObjectSystem_MessageEventKeyWrapperToMessageEventMap(), this.getMessageEventKeyWrapperToMessageEventMapEntry(), null, "messageEventKeyWrapperToMessageEventMap", null, 0, -1, MSDObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(msdObjectSystemEClass, theEcorePackage.getEBoolean(), "lifelineMayBindToEObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getLifeline(), "lifeline", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdObjectSystemEClass, theEcorePackage.getEBoolean(), "roleMayBindToEObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getProperty(), "role", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdObjectSystemEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEEList());
		EGenericType g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "objectSystemRootObjects", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRoles2eobjectsPackage.getRoleToEObjectMappingContainer(), "rolesToEObjectsMappingContainer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getRuntimeUtil(), "runtimeUtil", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(msdObjectSystemEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdObjectSystemEClass, theEventsPackage.getMessageEvent(), "getMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEOperation(), "eOperation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "eStructuralFeature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "sendingObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "receivingObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getPlainJavaObject(), "parameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(msdObjectSystemEClass, theEventsPackage.getMessageEvent(), "getMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEOperation(), "eOperation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEStructuralFeature(), "eStructuralFeature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "sendingObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "receivingObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUtilPackage.getPlainJavaObject(), "parameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "emptyMessageEventInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(stringToStringMapEntryEClass, Map.Entry.class, "StringToStringMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToStringMapEntry_Key(), theEcorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToStringMapEntry_Value(), ecorePackage.getEString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToIntegerMapEntryEClass, Map.Entry.class, "StringToIntegerMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToIntegerMapEntry_Key(), theEcorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToIntegerMapEntry_Value(), theEcorePackage.getEIntegerObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToBooleanMapEntryEClass, Map.Entry.class, "StringToBooleanMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToBooleanMapEntry_Value(), theEcorePackage.getEBooleanObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToBooleanMapEntry_Key(), theEcorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntryEClass, Map.Entry.class, "MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Value(), this.getMSDRuntimeState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry_Key(), this.getMSDRuntimeStateKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeProcessKeyWrapperToActiveProcessMapEntryEClass, Map.Entry.class, "ActiveProcessKeyWrapperToActiveProcessMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveProcessKeyWrapperToActiveProcessMapEntry_Value(), this.getActiveProcess(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveProcessKeyWrapperToActiveProcessMapEntry_Key(), this.getActiveProcessKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeStateKeyWrapperToActiveStateMapEntryEClass, Map.Entry.class, "ActiveStateKeyWrapperToActiveStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveStateKeyWrapperToActiveStateMapEntry_Value(), this.getActiveState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveStateKeyWrapperToActiveStateMapEntry_Key(), this.getActiveStateKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryEClass, Map.Entry.class, "ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Value(), this.getActiveMSDLifelineBindings(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry_Key(), this.getActiveMSDLifelineBindingsKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntryEClass, Map.Entry.class, "ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Value(), this.getActiveMSDVariableValuations(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry_Key(), this.getActiveMSDVariableValuationsKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntryEClass, Map.Entry.class, "ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Value(), this.getActiveMSSRoleBindings(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry_Key(), this.getActiveMSSRoleBindingsKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageEventContainerEClass, MessageEventContainer.class, "MessageEventContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEventContainer_RuntimeMessages(), theEventsPackage.getRuntimeMessage(), null, "runtimeMessages", null, 0, -1, MessageEventContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEventContainer_MessageEvents(), theEventsPackage.getMessageEvent(), null, "messageEvents", null, 0, -1, MessageEventContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageEventKeyWrapperToMessageEventMapEntryEClass, Map.Entry.class, "MessageEventKeyWrapperToMessageEventMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEventKeyWrapperToMessageEventMapEntry_Value(), theEventsPackage.getMessageEvent(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEventKeyWrapperToMessageEventMapEntry_Key(), this.getMessageEventKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectSystemKeyWrapperToMSDObjectSystemMapEntryEClass, Map.Entry.class, "ObjectSystemKeyWrapperToMSDObjectSystemMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Value(), this.getMSDObjectSystem(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObjectSystemKeyWrapperToMSDObjectSystemMapEntry_Key(), this.getObjectSystemKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSSEClass, ActiveMSS.class, "ActiveMSS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSS_RoleBindings(), this.getActiveMSSRoleBindings(), null, "roleBindings", null, 0, 1, ActiveMSS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSS_StateMachine(), theUMLPackage.getStateMachine(), null, "stateMachine", null, 0, 1, ActiveMSS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSDEClass, ActiveMSD.class, "ActiveMSD", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSD_LifelineBindings(), this.getActiveMSDLifelineBindings(), null, "lifelineBindings", null, 0, 1, ActiveMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSD_Interaction(), theUMLPackage.getInteraction(), null, "interaction", null, 0, 1, ActiveMSD.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getActiveMSD_MsdUtil(), theUtilPackage.getMSDUtil(), null, "msdUtil", null, 0, 1, ActiveMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(activeMSDEClass, null, "addLifelineBinding", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getLifeline(), "lifeline", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeMSSRoleBindingsEClass, ActiveMSSRoleBindings.class, "ActiveMSSRoleBindings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSSRoleBindings_PropertyToEObjectMap(), this.getPropertyToEObjectMapEntry(), null, "propertyToEObjectMap", null, 0, -1, ActiveMSSRoleBindings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyToEObjectMapEntryEClass, Map.Entry.class, "PropertyToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertyToEObjectMapEntry_Key(), theUMLPackage.getProperty(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeStateEClass, ActiveState.class, "ActiveState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActiveState_Hot(), ecorePackage.getEBoolean(), "hot", null, 0, 1, ActiveState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveState_RuntimeUtil(), theUtilPackage.getRuntimeUtil(), null, "runtimeUtil", null, 0, 1, ActiveState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveState_Executed(), theEcorePackage.getEBoolean(), "executed", null, 0, 1, ActiveState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMSSStateEClass, ActiveMSSState.class, "ActiveMSSState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMSSState_UmlState(), theUMLPackage.getState(), null, "umlState", null, 0, 1, ActiveMSSState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(activeMSSStateEClass, theEcorePackage.getEBoolean(), "isTransitionEnabled", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getTransition(), "transition", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(eObjectToEOperationMapEntryEClass, Map.Entry.class, "EObjectToEOperationMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectToEOperationMapEntry_Key(), theEcorePackage.getEObject(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEObjectToEOperationMapEntry_Value(), theEcorePackage.getEOperation(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eObjectToUMLClassMapEntryEClass, Map.Entry.class, "EObjectToUMLClassMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectToUMLClassMapEntry_Key(), theEcorePackage.getEObject(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEObjectToUMLClassMapEntry_Value(), theUMLPackage.getClass_(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eClassToEObjectMapEntryEClass, Map.Entry.class, "EClassToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEClassToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEClassToEObjectMapEntry_Key(), theEcorePackage.getEClass(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(executionSemanticsEEnum, ExecutionSemantics.class, "ExecutionSemantics");
		addEEnumLiteral(executionSemanticsEEnum, ExecutionSemantics.PLAY_OUT);
		addEEnumLiteral(executionSemanticsEEnum, ExecutionSemantics.ALWAYS_ALL_EVENTS);
		addEEnumLiteral(executionSemanticsEEnum, ExecutionSemantics.PLAY_OUT_BUT_WAITING_POSSIBLE);

		initEEnum(activeMSDProgressEEnum, ActiveMSDProgress.class, "ActiveMSDProgress");
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.PROGRESS);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.NO_PROGRESS);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.TERMINAL_CUT_REACHED);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.COLD_VIOLATION);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.SAFETY_VIOLATION);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.TIMED_COLD_VIOLATION);
		addEEnumLiteral(activeMSDProgressEEnum, ActiveMSDProgress.TIMED_SAFETY_VIOLATION);

		// Initialize data types
		initEDataType(semanticExceptionEDataType, SemanticException.class, "SemanticException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(parserExceptionEDataType, ParserException.class, "ParserException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeStateKeyWrapperEDataType, ActiveStateKeyWrapper.class, "ActiveStateKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeMSDLifelineBindingsKeyWrapperEDataType, ActiveMSDLifelineBindingsKeyWrapper.class, "ActiveMSDLifelineBindingsKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeMSDVariableValuationsKeyWrapperEDataType, ActiveMSDVariableValuationsKeyWrapper.class, "ActiveMSDVariableValuationsKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeProcessKeyWrapperEDataType, ActiveProcessKeyWrapper.class, "ActiveProcessKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(msdRuntimeStateKeyWrapperEDataType, MSDRuntimeStateKeyWrapper.class, "MSDRuntimeStateKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(messageEventKeyWrapperEDataType, MessageEventKeyWrapper.class, "MessageEventKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(objectSystemKeyWrapperEDataType, ObjectSystemKeyWrapper.class, "ObjectSystemKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeMSSRoleBindingsKeyWrapperEDataType, ActiveMSSRoleBindingsKeyWrapper.class, "ActiveMSSRoleBindingsKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //RuntimePackageImpl
