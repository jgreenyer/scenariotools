/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.msd.util.MSDUtil;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSD#getLifelineBindings <em>Lifeline Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSD#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSD#getMsdUtil <em>Msd Util</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSD()
 * @model
 * @generated
 */
public interface ActiveMSD extends ActiveProcess {
	/**
	 * Returns the value of the '<em><b>Lifeline Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline Bindings</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifeline Bindings</em>' reference.
	 * @see #setLifelineBindings(ActiveMSDLifelineBindings)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSD_LifelineBindings()
	 * @model
	 * @generated
	 */
	ActiveMSDLifelineBindings getLifelineBindings();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveMSD#getLifelineBindings <em>Lifeline Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lifeline Bindings</em>' reference.
	 * @see #getLifelineBindings()
	 * @generated
	 */
	void setLifelineBindings(ActiveMSDLifelineBindings value);

	/**
	 * Returns the value of the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction</em>' reference.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSD_Interaction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Interaction getInteraction();

	/**
	 * Returns the value of the '<em><b>Msd Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Util</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Util</em>' reference.
	 * @see #setMsdUtil(MSDUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSD_MsdUtil()
	 * @model
	 * @generated
	 */
	MSDUtil getMsdUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveMSD#getMsdUtil <em>Msd Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msd Util</em>' reference.
	 * @see #getMsdUtil()
	 * @generated
	 */
	void setMsdUtil(MSDUtil value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addLifelineBinding(Lifeline lifeline, EObject eObject);

	/**
	 * @generated NOT
	 */
	@Override
	public ActiveMSDCut getCurrentState();
	/**
	 * @generated NOT
	 */
	public ActiveMSDProgress progressCombinedFragment(CombinedFragment combinedFragment);

} // ActiveMSD
