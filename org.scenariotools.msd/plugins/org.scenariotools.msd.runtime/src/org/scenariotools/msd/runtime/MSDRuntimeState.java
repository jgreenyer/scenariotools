/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeState;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Runtime State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeState#getActiveProcesses <em>Active Processes</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeState#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeState#getActiveMSDChangedDuringPerformStep <em>Active MSD Changed During Perform Step</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeState()
 * @model
 * @generated
 */
public interface MSDRuntimeState extends RuntimeState {
	/**
	 * Returns the value of the '<em><b>Active Processes</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Processes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Processes</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeState_ActiveProcesses()
	 * @model
	 * @generated
	 */
	EList<ActiveProcess> getActiveProcesses();

	/**
	 * Returns the value of the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Util</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Util</em>' reference.
	 * @see #setRuntimeUtil(RuntimeUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeState_RuntimeUtil()
	 * @model transient="true"
	 * @generated
	 */
	RuntimeUtil getRuntimeUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDRuntimeState#getRuntimeUtil <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Util</em>' reference.
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	void setRuntimeUtil(RuntimeUtil value);

	/**
	 * Returns the value of the '<em><b>Active MSD Changed During Perform Step</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Changed During Perform Step</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Changed During Perform Step</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeState_ActiveMSDChangedDuringPerformStep()
	 * @model transient="true"
	 * @generated
	 */
	EList<ActiveProcess> getActiveMSDChangedDuringPerformStep();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void performStep(Event event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Updates (in fact completely re-calculates) the MSDModalMessageEvents for this MSDRuntimeState.
	 * This method should be called by clients after performStep is called unless the re-calculation is not necessary.
	 * 
	 * 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void updateMSDModalMessageEvents(ExecutionSemantics executionSemantics);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInExecutedSystemCut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInExecutedEnvironmentCut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInHotEnvironmentCut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInHotSystemCut();

	/**
	 * @generated NOT
	 */
	boolean isEnvironmentMessageEvent(MessageEvent representedMessageEvent);

} // MSDRuntimeState
