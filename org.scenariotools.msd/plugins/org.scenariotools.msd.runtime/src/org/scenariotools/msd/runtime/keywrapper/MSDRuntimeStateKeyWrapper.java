package org.scenariotools.msd.runtime.keywrapper;

import java.util.HashSet;

import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDRuntimeState;

public class MSDRuntimeStateKeyWrapper extends KeyWrapper {
	public MSDRuntimeStateKeyWrapper(MSDRuntimeState msdRuntimeState) {
		addSubObject(msdRuntimeState.isSafetyViolationOccurredInAssumptions());
		addSubObject(msdRuntimeState.isSafetyViolationOccurredInRequirements());
		addSubObject(new HashSet<ActiveProcess>(
				msdRuntimeState.getActiveProcesses()));
		addSubObject(msdRuntimeState.getObjectSystem());
	}
}
