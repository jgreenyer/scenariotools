/**
 */
package org.scenariotools.msd.runtime.util;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.SynchronousMessageEvent;
import org.scenariotools.msd.runtime.*;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.ElementContainer;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.Modality;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimePackage
 * @generated
 */
public class RuntimeSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RuntimePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeSwitch() {
		if (modelPackage == null) {
			modelPackage = RuntimePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RuntimePackage.MSD_RUNTIME_STATE_GRAPH: {
				MSDRuntimeStateGraph msdRuntimeStateGraph = (MSDRuntimeStateGraph)theEObject;
				T result = caseMSDRuntimeStateGraph(msdRuntimeStateGraph);
				if (result == null) result = caseRuntimeStateGraph(msdRuntimeStateGraph);
				if (result == null) result = caseStateGraph(msdRuntimeStateGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MSD_RUNTIME_STATE: {
				MSDRuntimeState msdRuntimeState = (MSDRuntimeState)theEObject;
				T result = caseMSDRuntimeState(msdRuntimeState);
				if (result == null) result = caseRuntimeState(msdRuntimeState);
				if (result == null) result = caseState(msdRuntimeState);
				if (result == null) result = caseAnnotatableElement(msdRuntimeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ELEMENT_CONTAINER: {
				ElementContainer elementContainer = (ElementContainer)theEObject;
				T result = caseElementContainer(elementContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MSD_MODALITY: {
				MSDModality msdModality = (MSDModality)theEObject;
				T result = caseMSDModality(msdModality);
				if (result == null) result = caseModality(msdModality);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_PROCESS: {
				ActiveProcess activeProcess = (ActiveProcess)theEObject;
				T result = caseActiveProcess(activeProcess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD_CUT: {
				ActiveMSDCut activeMSDCut = (ActiveMSDCut)theEObject;
				T result = caseActiveMSDCut(activeMSDCut);
				if (result == null) result = caseActiveState(activeMSDCut);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS: {
				ActiveMSDLifelineBindings activeMSDLifelineBindings = (ActiveMSDLifelineBindings)theEObject;
				T result = caseActiveMSDLifelineBindings(activeMSDLifelineBindings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS: {
				ActiveMSDVariableValuations activeMSDVariableValuations = (ActiveMSDVariableValuations)theEObject;
				T result = caseActiveMSDVariableValuations(activeMSDVariableValuations);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Lifeline, InteractionFragment> lifelineToInteractionFragmentMapEntry = (Map.Entry<Lifeline, InteractionFragment>)theEObject;
				T result = caseLifelineToInteractionFragmentMapEntry(lifelineToInteractionFragmentMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STRING_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, EObject> stringToEObjectMapEntry = (Map.Entry<String, EObject>)theEObject;
				T result = caseStringToEObjectMapEntry(stringToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.LIFELINE_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Lifeline, EObject> lifelineToEObjectMapEntry = (Map.Entry<Lifeline, EObject>)theEObject;
				T result = caseLifelineToEObjectMapEntry(lifelineToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.NAMED_ELEMENT_TO_MESSAGE_EVENT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<NamedElement, MessageEvent> namedElementToMessageEventMapEntry = (Map.Entry<NamedElement, MessageEvent>)theEObject;
				T result = caseNamedElementToMessageEventMapEntry(namedElementToMessageEventMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MSD_MODAL_MESSAGE_EVENT: {
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent)theEObject;
				T result = caseMSDModalMessageEvent(msdModalMessageEvent);
				if (result == null) result = caseModalMessageEvent(msdModalMessageEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MSD_OBJECT_SYSTEM: {
				MSDObjectSystem msdObjectSystem = (MSDObjectSystem)theEObject;
				T result = caseMSDObjectSystem(msdObjectSystem);
				if (result == null) result = caseObjectSystem(msdObjectSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STRING_TO_STRING_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, String> stringToStringMapEntry = (Map.Entry<String, String>)theEObject;
				T result = caseStringToStringMapEntry(stringToStringMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STRING_TO_INTEGER_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, Integer> stringToIntegerMapEntry = (Map.Entry<String, Integer>)theEObject;
				T result = caseStringToIntegerMapEntry(stringToIntegerMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STRING_TO_BOOLEAN_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, Boolean> stringToBooleanMapEntry = (Map.Entry<String, Boolean>)theEObject;
				T result = caseStringToBooleanMapEntry(stringToBooleanMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MSD_RUNTIME_STATE_KEY_WRAPPER_TO_MSD_RUNTIME_STATE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<MSDRuntimeStateKeyWrapper, MSDRuntimeState> msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry = (Map.Entry<MSDRuntimeStateKeyWrapper, MSDRuntimeState>)theEObject;
				T result = caseMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry(msdRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_PROCESS_KEY_WRAPPER_TO_ACTIVE_PROCESS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveProcessKeyWrapper, ActiveProcess> activeProcessKeyWrapperToActiveProcessMapEntry = (Map.Entry<ActiveProcessKeyWrapper, ActiveProcess>)theEObject;
				T result = caseActiveProcessKeyWrapperToActiveProcessMapEntry(activeProcessKeyWrapperToActiveProcessMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_STATE_KEY_WRAPPER_TO_ACTIVE_STATE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveStateKeyWrapper, ActiveState> activeStateKeyWrapperToActiveStateMapEntry = (Map.Entry<ActiveStateKeyWrapper, ActiveState>)theEObject;
				T result = caseActiveStateKeyWrapperToActiveStateMapEntry(activeStateKeyWrapperToActiveStateMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry = (Map.Entry<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings>)theEObject;
				T result = caseActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry(activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD_VARIABLE_VALUATIONS_KEY_WRAPPER_TO_ACTIVE_MSD_VARIABLE_VALUATIONS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry = (Map.Entry<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations>)theEObject;
				T result = caseActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry(activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSS_ROLE_BINDINGS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry = (Map.Entry<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings>)theEObject;
				T result = caseActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry(activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MESSAGE_EVENT_CONTAINER: {
				MessageEventContainer messageEventContainer = (MessageEventContainer)theEObject;
				T result = caseMessageEventContainer(messageEventContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<MessageEventKeyWrapper, MessageEvent> messageEventKeyWrapperToMessageEventMapEntry = (Map.Entry<MessageEventKeyWrapper, MessageEvent>)theEObject;
				T result = caseMessageEventKeyWrapperToMessageEventMapEntry(messageEventKeyWrapperToMessageEventMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_MSD_OBJECT_SYSTEM_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ObjectSystemKeyWrapper, MSDObjectSystem> objectSystemKeyWrapperToMSDObjectSystemMapEntry = (Map.Entry<ObjectSystemKeyWrapper, MSDObjectSystem>)theEObject;
				T result = caseObjectSystemKeyWrapperToMSDObjectSystemMapEntry(objectSystemKeyWrapperToMSDObjectSystemMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSS: {
				ActiveMSS activeMSS = (ActiveMSS)theEObject;
				T result = caseActiveMSS(activeMSS);
				if (result == null) result = caseActiveProcess(activeMSS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSD: {
				ActiveMSD activeMSD = (ActiveMSD)theEObject;
				T result = caseActiveMSD(activeMSD);
				if (result == null) result = caseActiveProcess(activeMSD);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSS_ROLE_BINDINGS: {
				ActiveMSSRoleBindings activeMSSRoleBindings = (ActiveMSSRoleBindings)theEObject;
				T result = caseActiveMSSRoleBindings(activeMSSRoleBindings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.PROPERTY_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Property, EObject> propertyToEObjectMapEntry = (Map.Entry<Property, EObject>)theEObject;
				T result = casePropertyToEObjectMapEntry(propertyToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_STATE: {
				ActiveState activeState = (ActiveState)theEObject;
				T result = caseActiveState(activeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MSS_STATE: {
				ActiveMSSState activeMSSState = (ActiveMSSState)theEObject;
				T result = caseActiveMSSState(activeMSSState);
				if (result == null) result = caseActiveState(activeMSSState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.EOBJECT_TO_EOPERATION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, EList<EOperation>> eObjectToEOperationMapEntry = (Map.Entry<EObject, EList<EOperation>>)theEObject;
				T result = caseEObjectToEOperationMapEntry(eObjectToEOperationMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.EOBJECT_TO_UML_CLASS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, EList<org.eclipse.uml2.uml.Class>> eObjectToUMLClassMapEntry = (Map.Entry<EObject, EList<org.eclipse.uml2.uml.Class>>)theEObject;
				T result = caseEObjectToUMLClassMapEntry(eObjectToUMLClassMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ECLASS_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EClass, EList<EObject>> eClassToEObjectMapEntry = (Map.Entry<EClass, EList<EObject>>)theEObject;
				T result = caseEClassToEObjectMapEntry(eClassToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeStateGraph(MSDRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeState(MSDRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementContainer(ElementContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Modality</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Modality</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDModality(MSDModality object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Process</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Process</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveProcess(ActiveProcess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSD(ActiveMSD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSS Role Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSS Role Bindings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSSRoleBindings(ActiveMSSRoleBindings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyToEObjectMapEntry(Map.Entry<Property, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveState(ActiveState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSS State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSS State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSSState(ActiveMSSState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD Cut</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD Cut</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSDCut(ActiveMSDCut object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD Lifeline Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD Lifeline Bindings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSDLifelineBindings(ActiveMSDLifelineBindings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD Variable Valuations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD Variable Valuations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSDVariableValuations(ActiveMSDVariableValuations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lifeline To Interaction Fragment Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lifeline To Interaction Fragment Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLifelineToInteractionFragmentMapEntry(Map.Entry<Lifeline, InteractionFragment> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToEObjectMapEntry(Map.Entry<String, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lifeline To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lifeline To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLifelineToEObjectMapEntry(Map.Entry<Lifeline, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element To Message Event Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element To Message Event Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElementToMessageEventMapEntry(Map.Entry<NamedElement, MessageEvent> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Modal Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Modal Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDModalMessageEvent(MSDModalMessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Object System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Object System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDObjectSystem(MSDObjectSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To String Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To String Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToStringMapEntry(Map.Entry<String, String> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Integer Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Integer Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToIntegerMapEntry(Map.Entry<String, Integer> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Boolean Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Boolean Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToBooleanMapEntry(Map.Entry<String, Boolean> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Runtime State Key Wrapper To MSD Runtime State Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry(Map.Entry<MSDRuntimeStateKeyWrapper, MSDRuntimeState> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Process Key Wrapper To Active Process Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Process Key Wrapper To Active Process Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveProcessKeyWrapperToActiveProcessMapEntry(Map.Entry<ActiveProcessKeyWrapper, ActiveProcess> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active State Key Wrapper To Active State Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active State Key Wrapper To Active State Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveStateKeyWrapperToActiveStateMapEntry(Map.Entry<ActiveStateKeyWrapper, ActiveState> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry(Map.Entry<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry(Map.Entry<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry(Map.Entry<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEventContainer(MessageEventContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event Key Wrapper To Message Event Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event Key Wrapper To Message Event Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEventKeyWrapperToMessageEventMapEntry(Map.Entry<MessageEventKeyWrapper, MessageEvent> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object System Key Wrapper To MSD Object System Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object System Key Wrapper To MSD Object System Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectSystemKeyWrapperToMSDObjectSystemMapEntry(Map.Entry<ObjectSystemKeyWrapper, MSDObjectSystem> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active MSS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active MSS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMSS(ActiveMSS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject To EOperation Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject To EOperation Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEObjectToEOperationMapEntry(Map.Entry<EObject, EList<EOperation>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject To UML Class Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject To UML Class Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEObjectToUMLClassMapEntry(Map.Entry<EObject, EList<org.eclipse.uml2.uml.Class>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassToEObjectMapEntry(Map.Entry<EClass, EList<EObject>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateGraph(StateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeStateGraph(RuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatableElement(AnnotatableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeState(RuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modality</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modality</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModality(Modality object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modal Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modal Message Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModalMessageEvent(ModalMessageEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectSystem(ObjectSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RuntimeSwitch
