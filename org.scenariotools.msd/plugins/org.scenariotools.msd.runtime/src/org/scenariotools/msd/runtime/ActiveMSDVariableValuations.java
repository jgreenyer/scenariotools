/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSD Variable Valuations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToEObjectValueMap <em>Variable Name To EObject Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToStringValueMap <em>Variable Name To String Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToIntegerValueMap <em>Variable Name To Integer Value Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations#getVariableNameToBooleanValueMap <em>Variable Name To Boolean Value Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDVariableValuations()
 * @model
 * @generated
 */
public interface ActiveMSDVariableValuations extends EObject {
	/**
	 * Returns the value of the '<em><b>Variable Name To EObject Value Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name To EObject Value Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name To EObject Value Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDVariableValuations_VariableNameToEObjectValueMap()
	 * @model mapType="org.scenariotools.msd.runtime.StringToEObjectMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<String, EObject> getVariableNameToEObjectValueMap();

	/**
	 * Returns the value of the '<em><b>Variable Name To String Value Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name To String Value Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name To String Value Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDVariableValuations_VariableNameToStringValueMap()
	 * @model mapType="org.scenariotools.msd.runtime.StringToStringMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<String, String> getVariableNameToStringValueMap();

	/**
	 * Returns the value of the '<em><b>Variable Name To Integer Value Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name To Integer Value Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name To Integer Value Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDVariableValuations_VariableNameToIntegerValueMap()
	 * @model mapType="org.scenariotools.msd.runtime.StringToIntegerMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<String, Integer> getVariableNameToIntegerValueMap();

	/**
	 * Returns the value of the '<em><b>Variable Name To Boolean Value Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Boolean},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name To Boolean Value Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name To Boolean Value Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDVariableValuations_VariableNameToBooleanValueMap()
	 * @model mapType="org.scenariotools.msd.runtime.StringToBooleanMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EBooleanObject>"
	 * @generated
	 */
	EMap<String, Boolean> getVariableNameToBooleanValueMap();

} // ActiveMSDVariableValuations
