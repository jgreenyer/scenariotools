/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.uml2.uml.StateMachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSS#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSS#getStateMachine <em>State Machine</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSS()
 * @model
 * @generated
 */
public interface ActiveMSS extends ActiveProcess {
	/**
	 * Returns the value of the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Bindings</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Bindings</em>' reference.
	 * @see #setRoleBindings(ActiveMSSRoleBindings)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSS_RoleBindings()
	 * @model
	 * @generated
	 */
	ActiveMSSRoleBindings getRoleBindings();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveMSS#getRoleBindings <em>Role Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Bindings</em>' reference.
	 * @see #getRoleBindings()
	 * @generated
	 */
	void setRoleBindings(ActiveMSSRoleBindings value);

	/**
	 * Returns the value of the '<em><b>State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Machine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machine</em>' reference.
	 * @see #setStateMachine(StateMachine)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSS_StateMachine()
	 * @model
	 * @generated
	 */
	StateMachine getStateMachine();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveMSS#getStateMachine <em>State Machine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Machine</em>' reference.
	 * @see #getStateMachine()
	 * @generated
	 */
	void setStateMachine(StateMachine value);

	/**
	 * @generated NOT
	 */
	@Override
	public ActiveMSSState getCurrentState();
} // ActiveMSS
