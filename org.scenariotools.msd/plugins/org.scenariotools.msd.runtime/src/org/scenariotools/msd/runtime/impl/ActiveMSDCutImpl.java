/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.List;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSD Cut</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDCutImpl#getLifelineToInteractionFragmentMap <em>Lifeline To Interaction Fragment Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSDCutImpl extends ActiveStateImpl implements ActiveMSDCut {
	/**
	 * The cached value of the '{@link #getLifelineToInteractionFragmentMap() <em>Lifeline To Interaction Fragment Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLifelineToInteractionFragmentMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Lifeline, InteractionFragment> lifelineToInteractionFragmentMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSDCutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSD_CUT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Lifeline, InteractionFragment> getLifelineToInteractionFragmentMap() {
		if (lifelineToInteractionFragmentMap == null) {
			lifelineToInteractionFragmentMap = new EcoreEMap<Lifeline,InteractionFragment>(RuntimePackage.Literals.LIFELINE_TO_INTERACTION_FRAGMENT_MAP_ENTRY, LifelineToInteractionFragmentMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP);
		}
		return lifelineToInteractionFragmentMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isMessageEnabled(Message message) {
		MessageOccurrenceSpecification sendingMOS = RuntimeUtil.Helper.getSendingMessageOccurrenceSpecification(message);
		MessageOccurrenceSpecification receivingMOS = RuntimeUtil.Helper.getReceivingMessageOccurrenceSpecification(message);
		
		boolean isSelfMessage = sendingMOS.getCovereds().iterator().next().equals(receivingMOS.getCovereds().iterator().next());
		if (isSelfMessage)
			return isMOSEnabled(sendingMOS, RuntimeUtil.Helper.getSendingLifeline(message));
		else
			return isMOSEnabled(sendingMOS, RuntimeUtil.Helper.getSendingLifeline(message)) 
				&& isMOSEnabled(receivingMOS, RuntimeUtil.Helper.getReceivingLifeline(message));
	}
	
	/**
	 * @generated NOT
	 */
	public boolean isMOSEnabled(MessageOccurrenceSpecification mos, Lifeline lifeline) {
		// check whether the current fragment is mos
		InteractionFragment fragment = getLifelineToInteractionFragmentMap()
				.get(lifeline);
		if (fragment == null){
			// lifeline not bound yet.
			return false;
		}
		if (isAltCombinedFragment(fragment)) {
			// recursively search all first messages within the fragment
			if (isInitialMOSInAltCombinedFragment(mos, (CombinedFragment) fragment,
					lifeline))
				return true;
		} else // simple fragment -> check is trivial
		if (fragment.equals(mos))
			return true;
		return false;
	}
	
	/**
	 * Recursively checks if mos belongs to a first message in the Alt-CombinedFragment alt.
	 * TODO: Should be moved into msd.util.
	 * @param mos
	 * @param alt
	 * @param lifeline
	 * @return
	 */
	private boolean isInitialMOSInAltCombinedFragment(
			MessageOccurrenceSpecification mos, CombinedFragment alt,
			Lifeline lifeline) {
		//examine all operands with at least one fragment
		for (InteractionOperand operand : alt
				.getOperands()) {
			List<InteractionFragment> fragments=getRuntimeUtil().getInteractionOperandToLifelineToInteractionFragmentsMapMap().get(operand).get(lifeline);
			if (fragments!=null && fragments.size() > 0) {
				InteractionFragment firstOpFragment=fragments.get(0);
				if(isAltCombinedFragment(firstOpFragment)){
					//first operand is again alt fragment -> recursion
					if(isInitialMOSInAltCombinedFragment(mos, (CombinedFragment)firstOpFragment, lifeline))
						return true;
				}
				else if(firstOpFragment instanceof MessageOccurrenceSpecification && ((MessageOccurrenceSpecification)firstOpFragment).equals(mos)){
					//found mos (recursion end)
					return true;
				}
			}
		}
		return false;
	}
	
	private void advanceCutToNextFragment(Lifeline lifeline, InteractionFragment fragment){
		getLifelineToInteractionFragmentMap().put(lifeline,getNextFragment(lifeline, fragment));
	}

	private InteractionFragment getNextFragment(
			Lifeline lifeline, InteractionFragment fragment) {
		InteractionOperand enclosingOperand=fragment.getEnclosingOperand();
		EList<InteractionFragment> sameLevelInteractionFragments=null;
		//current fragment is in enclosing operand
		if(enclosingOperand != null){
			sameLevelInteractionFragments = getRuntimeUtil()
					.getInteractionOperandToLifelineToInteractionFragmentsMapMap().get(enclosingOperand).get(lifeline);
		}
		else {
			// fragment is on top level of interaction
			sameLevelInteractionFragments = getRuntimeUtil()
					.getLifelineToInteractionFragmentsMap().get(lifeline);
		}

		int fragmentIndex = sameLevelInteractionFragments
				.indexOf(fragment);
		if(fragmentIndex + 1 == sameLevelInteractionFragments.size())
			//next fragment must be after current operand -> recursion
			//note: implies enclosingOperand != null
			return getNextFragment(lifeline, (InteractionFragment)enclosingOperand.getOwner());
		else
			//next fragment is within same operand
			return sameLevelInteractionFragments.get(fragmentIndex + 1);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean progressCutLocationOnLifeline(Lifeline lifeline,
			InteractionFragment currentFragment) {
		if (currentFragment == null){
			throw new IllegalArgumentException("There is no cut location defined for lifeline " + RuntimeUtil.Helper.getLifelineName(lifeline) + " in this cut.");
		}
		if (currentFragment.equals(getRuntimeUtil().getFinalFragment()))
			return false;
		else{
			//actual progressing is done here
			advanceCutToNextFragment(lifeline, currentFragment);
			return true;
		}
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean terminalCutReached() {
		for(InteractionFragment interactionFragment : getLifelineToInteractionFragmentMap().values()){
			if (interactionFragment != getRuntimeUtil().getFinalFragment() 
					&& !isNegCombinedFragment(interactionFragment)){
				return false;
			}
		}
		return true;
	}
	
	private boolean isNegCombinedFragment(InteractionFragment interactionFragment){
		if (interactionFragment instanceof CombinedFragment){
			return ((CombinedFragment)interactionFragment).getInteractionOperator() == InteractionOperatorKind.NEG_LITERAL;
		}
		return false;
	}

	private boolean isAltCombinedFragment(InteractionFragment interactionFragment){
		if (interactionFragment instanceof CombinedFragment){
			return ((CombinedFragment)interactionFragment).getInteractionOperator() == InteractionOperatorKind.ALT_LITERAL;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP:
				return ((InternalEList<?>)getLifelineToInteractionFragmentMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP:
				if (coreType) return getLifelineToInteractionFragmentMap();
				else return getLifelineToInteractionFragmentMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP:
				((EStructuralFeature.Setting)getLifelineToInteractionFragmentMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP:
				getLifelineToInteractionFragmentMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_CUT__LIFELINE_TO_INTERACTION_FRAGMENT_MAP:
				return lifelineToInteractionFragmentMap != null && !lifelineToInteractionFragmentMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActiveMSDCutImpl
