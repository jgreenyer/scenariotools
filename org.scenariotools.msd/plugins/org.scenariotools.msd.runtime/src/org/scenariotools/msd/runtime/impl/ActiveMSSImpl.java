/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Transition;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Active MSS</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSSImpl#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSSImpl#getStateMachine <em>State Machine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSSImpl extends ActiveProcessImpl implements ActiveMSS {
	/**
	 * The cached value of the '{@link #getRoleBindings() <em>Role Bindings</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSSRoleBindings roleBindings;

	/**
	 * The cached value of the '{@link #getStateMachine() <em>State Machine</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getStateMachine()
	 * @generated
	 * @ordered
	 */
	protected StateMachine stateMachine;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSSRoleBindings getRoleBindings() {
		if (roleBindings != null && roleBindings.eIsProxy()) {
			InternalEObject oldRoleBindings = (InternalEObject)roleBindings;
			roleBindings = (ActiveMSSRoleBindings)eResolveProxy(oldRoleBindings);
			if (roleBindings != oldRoleBindings) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS, oldRoleBindings, roleBindings));
			}
		}
		return roleBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSSRoleBindings basicGetRoleBindings() {
		return roleBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleBindings(ActiveMSSRoleBindings newRoleBindings) {
		ActiveMSSRoleBindings oldRoleBindings = roleBindings;
		roleBindings = newRoleBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS, oldRoleBindings, roleBindings));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine getStateMachine() {
		if (stateMachine != null && stateMachine.eIsProxy()) {
			InternalEObject oldStateMachine = (InternalEObject)stateMachine;
			stateMachine = (StateMachine)eResolveProxy(oldStateMachine);
			if (stateMachine != oldStateMachine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MSS__STATE_MACHINE, oldStateMachine, stateMachine));
			}
		}
		return stateMachine;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine basicGetStateMachine() {
		return stateMachine;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateMachine(StateMachine newStateMachine) {
		StateMachine oldStateMachine = stateMachine;
		stateMachine = newStateMachine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSS__STATE_MACHINE, oldStateMachine, stateMachine));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS:
				if (resolve) return getRoleBindings();
				return basicGetRoleBindings();
			case RuntimePackage.ACTIVE_MSS__STATE_MACHINE:
				if (resolve) return getStateMachine();
				return basicGetStateMachine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS:
				setRoleBindings((ActiveMSSRoleBindings)newValue);
				return;
			case RuntimePackage.ACTIVE_MSS__STATE_MACHINE:
				setStateMachine((StateMachine)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS:
				setRoleBindings((ActiveMSSRoleBindings)null);
				return;
			case RuntimePackage.ACTIVE_MSS__STATE_MACHINE:
				setStateMachine((StateMachine)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSS__ROLE_BINDINGS:
				return roleBindings != null;
			case RuntimePackage.ACTIVE_MSS__STATE_MACHINE:
				return stateMachine != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ActiveMSSState getCurrentState() {
		return (ActiveMSSState) super.getCurrentState();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected void initializeRelevantEventsForActiveProcess() {
		if (logger.isDebugEnabled()) {
			logger.debug("initializeRelevantEvents for active Process " + this);
		}
		
		StateMachine stateMachine = getStateMachine();

		for (Transition transition : stateMachine.getRegions().get(0)
				.getTransitions()) {
			if(!RuntimeUtil.Helper.isMessageEventTransition(transition))
					continue;

			if (logger.isDebugEnabled()) {
				logger.debug("Creating an MSDModalMessageEvent for the transition "
						+ transition);
			}

			if (getCurrentState().isTransitionEnabled(transition)) {
				MessageEvent enabledMessageEvent = getMessageEvent(transition,
						false);
				if (logger.isDebugEnabled()) {
					logger.debug("Adding enabled message event "
							+ RuntimeUtil.Helper
									.getMessageEventString(enabledMessageEvent)
							+ "for active Process " + this);
				}
				getEnabledEvents().put(transition, enabledMessageEvent);

				// if a message event that is message unifiable with this
				// enabled message event was already identified as a violating
				// event,
				// this means that this event is actually not violating.
				Iterator<Map.Entry<NamedElement, MessageEvent>> violatingMessageEventsMapEntriesIterator = getViolatingEvents()
						.iterator();
				while (violatingMessageEventsMapEntriesIterator.hasNext()) {
					Map.Entry<NamedElement, MessageEvent> mapEntry = violatingMessageEventsMapEntriesIterator
							.next();
					if (RuntimeUtil.Helper.isParameterUnifiable(
							enabledMessageEvent, mapEntry.getValue(), this)
							&& !mapEntry.getValue().equals(
									enabledMessageEvent
											.getAbstractMessageEvent())) {
						violatingMessageEventsMapEntriesIterator.remove();
					}
					/*
					 * if(enabledMessageEvent.getAbstractMessageEvent() !=
					 * null){ if (mapEntry.getValue() ==
					 * enabledMessageEvent.getAbstractMessageEvent()){
					 * violatingMessageEventsMapEntriesIterator.remove(); if
					 * (logger.isDebugEnabled()) logger.debug(
					 * "Previously violating event is not violating because a message event message unifiable with this event is enabled: "
					 * +
					 * RuntimeUtil.Helper.getMessageEventString(enabledMessageEvent
					 * )); } }else{ if (mapEntry.getValue() ==
					 * enabledMessageEvent){
					 * violatingMessageEventsMapEntriesIterator.remove(); if
					 * (logger.isDebugEnabled()) logger.debug(
					 * "Previously violating event is not violating because a message event message unifiable with this event is enabled: "
					 * +
					 * RuntimeUtil.Helper.getMessageEventString(enabledMessageEvent
					 * )); } }
					 */
				}

			} else {
				MessageEvent potentiallyViolatingMessageEvent = getMessageEvent(
						transition, false);

				// this may be if the sending or receiving lifeline is not
				// bound. In this case, we ignore corresponding message events.
				if (potentiallyViolatingMessageEvent == null)
					continue;

				if (logger.isDebugEnabled()) {
					logger.debug("Message event "
							+ RuntimeUtil.Helper
									.getMessageEventString(potentiallyViolatingMessageEvent)
							+ " is potentially a violating message event for this active Process");
				}

				// if a message event that is message unifiable with this
				// potentially violating message event was already identified as
				// an enabled event,
				// the potentially violating event is not violating, and thus we
				// don't add it to the map of violating events
				boolean messageUnifiableEventAlreadyEnabled = false;
				for (MessageEvent enabledMessageEvent : getEnabledEvents()
						.values()) {
					if (RuntimeUtil.Helper.isParameterUnifiable(
							enabledMessageEvent,
							potentiallyViolatingMessageEvent, this))
						messageUnifiableEventAlreadyEnabled = true;
				}
				if (!messageUnifiableEventAlreadyEnabled) {
					if (logger.isDebugEnabled()) {
						logger.debug("Message event "
								+ RuntimeUtil.Helper
										.getMessageEventString(potentiallyViolatingMessageEvent)
								+ " is added as a violating message event for this active MSS, no enabled message event that is unifiable with it was found.");
					}

					getViolatingEvents().put(transition,
							potentiallyViolatingMessageEvent);
				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("Message event "
								+ RuntimeUtil.Helper
										.getMessageEventString(potentiallyViolatingMessageEvent)
								+ " is not added as a violating message event for this active MSS, an enabled message event that is unifiable with it was found.");
					}
				}
			}
		}
		getCurrentState().setHot(isStateHot(getCurrentState().getUmlState()));
		getCurrentState().setExecuted(isStateExecuted(getCurrentState().getUmlState()));
	}

	private boolean isStateExecuted(State state) {
		Stereotype stereotype = state.getAppliedStereotype("Modal::ModalState");
		EnumerationLiteral temperature = (EnumerationLiteral) state.getValue(
				stereotype, "execution");
		return temperature.getName().equals("Execute");
	}

	private MessageEvent getMessageEvent(Transition transition,
			boolean ignoreParameter) {
		if (logger.isDebugEnabled()) {
			logger.debug("Creating message event for transition " + transition
					+ " in the context of the active MSS " + this);
		}

		Property senderRole = RuntimeUtil.Helper.getSender(transition);
		Property receiverRole = RuntimeUtil.Helper.getReceiver(transition);

		EObject sendingObject = getRoleBindings().getPropertyToEObjectMap()
				.get(senderRole);
		EObject receivingObject = getRoleBindings().getPropertyToEObjectMap()
				.get(receiverRole);
		if (sendingObject != null && receivingObject != null) {
			EOperation eOperation = getRuntimeUtil().getOperation(transition);
			EStructuralFeature eStructuralFeature = getRuntimeUtil()
					.getEStructuralFeature(transition);
			if (ignoreParameter) {
				return getObjectSystem()
						.getMessageEvent(eOperation, eStructuralFeature,
								sendingObject, receivingObject, null);
			} else {
				// TODO: support message parameters
				// return
				// getRuntimeUtil().getMsdRuntimeStateGraph().getSynchMessageEvent(eOperation,
				// eStructuralFeature, sendingObject, receivingObject,
				// getRuntimeUtil().getParameterValue(message,
				// getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(this)));
				return getObjectSystem().getMessageEvent(eOperation, eStructuralFeature,
								sendingObject, receivingObject, null);
			}
		} else {
			if (logger.isDebugEnabled()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Cannot create message event for transition "
							+ transition + " in active MSS " + this);
				}
				if (sendingObject != null)
					logger.debug("   --   because the sending role is unbound");
				if (receivingObject != null)
					logger.debug("   --   because the receiving role is unbound");
			}
			return null;
		}
	}

	private boolean isStateHot(State state) {
		Stereotype stereotype = state.getAppliedStereotype("Modal::ModalState");
		EnumerationLiteral temperature = (EnumerationLiteral) state.getValue(
				stereotype, "temperature");
		return temperature.getName().equals("Hot");
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ActiveMSDProgress progressEnabledHiddenEvents() {
		return ActiveMSDProgress.NO_PROGRESS;
	}

	/**
	 * @generated NOT
	 */
	public ActiveProcessKeyWrapper getKeyWrapper(){
		return new ActiveMSSKeyWrapper(this);
	}

} // ActiveMSSImpl
