/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSD Lifeline Bindings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl#getLifelineToEObjectMap <em>Lifeline To EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsImpl#getIgnoredLifelines <em>Ignored Lifelines</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSDLifelineBindingsImpl extends RuntimeEObjectImpl implements ActiveMSDLifelineBindings {
	/**
	 * The cached value of the '{@link #getLifelineToEObjectMap() <em>Lifeline To EObject Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLifelineToEObjectMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Lifeline, EObject> lifelineToEObjectMap;

	/**
	 * The cached value of the '{@link #getIgnoredLifelines() <em>Ignored Lifelines</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIgnoredLifelines()
	 * @generated
	 * @ordered
	 */
	protected EList<Lifeline> ignoredLifelines;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSDLifelineBindingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSD_LIFELINE_BINDINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Lifeline, EObject> getLifelineToEObjectMap() {
		if (lifelineToEObjectMap == null) {
			lifelineToEObjectMap = new EcoreEMap<Lifeline,EObject>(RuntimePackage.Literals.LIFELINE_TO_EOBJECT_MAP_ENTRY, LifelineToEObjectMapEntryImpl.class, this, RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP);
		}
		return lifelineToEObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Lifeline> getIgnoredLifelines() {
		if (ignoredLifelines == null) {
			ignoredLifelines = new EObjectResolvingEList<Lifeline>(Lifeline.class, this, RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES);
		}
		return ignoredLifelines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lifeline getIgnoredLifelines(String name) {
		return getIgnoredLifelines(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lifeline getIgnoredLifelines(String name, boolean ignoreCase) {
		ignoredLifelinesLoop: for (Lifeline ignoredLifelines : getIgnoredLifelines()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(ignoredLifelines.getName()) : name.equals(ignoredLifelines.getName())))
				continue ignoredLifelinesLoop;
			return ignoredLifelines;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP:
				return ((InternalEList<?>)getLifelineToEObjectMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP:
				if (coreType) return getLifelineToEObjectMap();
				else return getLifelineToEObjectMap().map();
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES:
				return getIgnoredLifelines();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP:
				((EStructuralFeature.Setting)getLifelineToEObjectMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES:
				getIgnoredLifelines().clear();
				getIgnoredLifelines().addAll((Collection<? extends Lifeline>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP:
				getLifelineToEObjectMap().clear();
				return;
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES:
				getIgnoredLifelines().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__LIFELINE_TO_EOBJECT_MAP:
				return lifelineToEObjectMap != null && !lifelineToEObjectMap.isEmpty();
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS__IGNORED_LIFELINES:
				return ignoredLifelines != null && !ignoredLifelines.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActiveMSDLifelineBindingsImpl
