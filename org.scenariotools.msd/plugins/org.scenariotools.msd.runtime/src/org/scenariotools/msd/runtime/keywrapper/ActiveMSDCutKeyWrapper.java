package org.scenariotools.msd.runtime.keywrapper;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;

public class ActiveMSDCutKeyWrapper extends ActiveStateKeyWrapper {

	@Deprecated
	public ActiveMSDCutKeyWrapper(ActiveMSDCut activeMSDCut) {
		this(activeMSDCut.getLifelineToInteractionFragmentMap().keySet().iterator().next().getInteraction(),
				activeMSDCut.getLifelineToInteractionFragmentMap());
	}

	@Deprecated
	public ActiveMSDCutKeyWrapper(Interaction interaction,
			EMap<Lifeline, InteractionFragment> lifelineToInteractionFragmentMap) {
		addSubObject(interaction);
		addSubObject(lifelineToInteractionFragmentMap);
	}

	public ActiveMSDCutKeyWrapper(ActiveMSD activeMSD, ActiveMSDCut activeMSDCut) {
		addSubObject(activeMSDCut.getLifelineToInteractionFragmentMap().keySet().iterator().next().getInteraction());
		addSubObject(activeMSDCut.getLifelineToInteractionFragmentMap());

		addSubObject(activeMSD.getLifelineBindings());
		addSubObject(activeMSD.getVariableValuations());
	}	
}
