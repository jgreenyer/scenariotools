package org.scenariotools.msd.runtime.keywrapper;

import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;

public class ActiveMSDVariableValuationsKeyWrapper extends KeyWrapper {
	public ActiveMSDVariableValuationsKeyWrapper(
			ActiveMSDVariableValuations activeMSDVariableValuations) {
		addSubObject(activeMSDVariableValuations
				.getVariableNameToBooleanValueMap());
		addSubObject(activeMSDVariableValuations
				.getVariableNameToIntegerValueMap());
		addSubObject(activeMSDVariableValuations
				.getVariableNameToStringValueMap());
		addSubObject(activeMSDVariableValuations
				.getVariableNameToEObjectValueMap());
	}
}
