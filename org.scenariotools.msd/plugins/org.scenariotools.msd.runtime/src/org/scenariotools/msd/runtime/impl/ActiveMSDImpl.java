/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.ValueSpecification;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDImpl#getLifelineBindings <em>Lifeline Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDImpl#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDImpl#getMsdUtil <em>Msd Util</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSDImpl extends ActiveProcessImpl implements ActiveMSD {

	protected static Logger logger = Activator.getLogManager().getLogger(
			MSDRuntimeStateImpl.class.getName());
	
	/**
	 * The cached value of the '{@link #getLifelineBindings() <em>Lifeline Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLifelineBindings()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSDLifelineBindings lifelineBindings;

	/**
	 * The cached value of the '{@link #getMsdUtil() <em>Msd Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsdUtil()
	 * @generated
	 * @ordered
	 */
	protected MSDUtil msdUtil;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings getLifelineBindings() {
		if (lifelineBindings != null && lifelineBindings.eIsProxy()) {
			InternalEObject oldLifelineBindings = (InternalEObject)lifelineBindings;
			lifelineBindings = (ActiveMSDLifelineBindings)eResolveProxy(oldLifelineBindings);
			if (lifelineBindings != oldLifelineBindings) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS, oldLifelineBindings, lifelineBindings));
			}
		}
		return lifelineBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings basicGetLifelineBindings() {
		return lifelineBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLifelineBindings(ActiveMSDLifelineBindings newLifelineBindings) {
		ActiveMSDLifelineBindings oldLifelineBindings = lifelineBindings;
		lifelineBindings = newLifelineBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS, oldLifelineBindings, lifelineBindings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteraction() {
		Interaction interaction = basicGetInteraction();
		return interaction != null && interaction.eIsProxy() ? (Interaction)eResolveProxy((InternalEObject)interaction) : interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Interaction basicGetInteraction() {
		return getLifelineBindings().getLifelineToEObjectMap().keySet().iterator().next().getInteraction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDUtil getMsdUtil() {
		if (msdUtil != null && msdUtil.eIsProxy()) {
			InternalEObject oldMsdUtil = (InternalEObject)msdUtil;
			msdUtil = (MSDUtil)eResolveProxy(oldMsdUtil);
			if (msdUtil != oldMsdUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MSD__MSD_UTIL, oldMsdUtil, msdUtil));
			}
		}
		return msdUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDUtil basicGetMsdUtil() {
		return msdUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsdUtil(MSDUtil newMsdUtil) {
		MSDUtil oldMsdUtil = msdUtil;
		msdUtil = newMsdUtil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSD__MSD_UTIL, oldMsdUtil, msdUtil));
	}


	
	/**
	 * <!-- begin-user-doc -->
	 * Auxiliary method that 
	 * 1. adds a lifeline binding to the activeMSD and 
	 * 2. puts the first interaction fragment on the lifeline as the currently enabled fragment for this lifeline in the cut
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addLifelineBinding(Lifeline lifeline, EObject eObject) {
		getLifelineBindings().getLifelineToEObjectMap().put(lifeline, eObject);
		EList<InteractionFragment> interactionFragementsOnSendingLifelineList = getRuntimeUtil().getLifelineToInteractionFragmentsMap().get(lifeline);
		getCurrentState().getLifelineToInteractionFragmentMap().put(lifeline, interactionFragementsOnSendingLifelineList.get(0));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveMSDProgress progressEnabledHiddenEvents() {
		
		//1) Identify all the CombinedFragments that are enabled in this cut
		EList<CombinedFragment> enabledCombinedFragments = null;
		ActiveMSDProgress activeMSDProgress = ActiveMSDProgress.PROGRESS;
		boolean isEnabled = false;
		boolean cutProgressed = false;
		for(InteractionFragment currentFragment : getCurrentState().getLifelineToInteractionFragmentMap().values()){
			if (currentFragment instanceof CombinedFragment
					&& ((CombinedFragment) currentFragment)
							.getInteractionOperator() != InteractionOperatorKind.ALT_LITERAL) {
				
				if (enabledCombinedFragments == null)
					 enabledCombinedFragments = new BasicEList<CombinedFragment>();
				
				CombinedFragment candidateEnabledCombinedFragment = (CombinedFragment) currentFragment;
				if(!enabledCombinedFragments.contains(candidateEnabledCombinedFragment)){
					isEnabled=true;
					Iterator<Lifeline> byCombinedFragmentCoveredLifelinesIterator = candidateEnabledCombinedFragment.getCovereds().iterator();
					while(byCombinedFragmentCoveredLifelinesIterator.hasNext() && isEnabled){
						if(getCurrentState().getLifelineToInteractionFragmentMap().get(byCombinedFragmentCoveredLifelinesIterator.next()) != candidateEnabledCombinedFragment)
							isEnabled = false;
					}
					if(isEnabled){
						if(logger.isDebugEnabled()){
							logger.debug("CombinedFragment " + candidateEnabledCombinedFragment.getName() + " is enabled");
						}
						enabledCombinedFragments.add(candidateEnabledCombinedFragment);
						activeMSDProgress = progressCombinedFragment(candidateEnabledCombinedFragment);
						if (activeMSDProgress != ActiveMSDProgress.NO_PROGRESS)
							cutProgressed = true;
					}
				}
			}
		}
		

		if (cutProgressed // only necessary to check if terminal cut was reached when cut changed.
				&& getCurrentState().terminalCutReached()){
			return ActiveMSDProgress.TERMINAL_CUT_REACHED;
		}
		return activeMSDProgress;
	}

	/**
	 *@generated NOT 
	 */
	public ActiveMSDProgress progressCombinedFragment(CombinedFragment combinedFragment){
		//2) For each enabled CombinedFragment, determine whether it is a condition or an assignment.
		//   Then either check the condition or execute the assignment.
		//   Progress the cut beyond the CombinedFragment if it is an assignment or a condition that evaluated to true.
		//   If a condition does not evaluate to true, this is a cold violation (cold condition) or the cut does not progress (hot condition).
		//   A special case are hot conditions with the expression "false" -- if enabled, this leads to a safety violation right away.
		//3) Return true if there is a change in the cut
		if(RuntimeUtil.Helper.isCondition(combinedFragment)){
			if (isConditionTrue(combinedFragment)){
				progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
			}else{
				if(!RuntimeUtil.Helper.isHot(combinedFragment)){
					return ActiveMSDProgress.COLD_VIOLATION;
				}else{
					logger.debug("Cannot progress beyond a hot condition.");
					// if an enabled hot condition contains the expression "false"---which will obviously never become true in the future---interpret this as a safety violation.
					getCurrentState().setHot(true);
					getCurrentState().setExecuted(true);
					return ActiveMSDProgress.NO_PROGRESS;
				}
			}
		}
		else{
			//it is an assignment
			computeAssignment(combinedFragment);
			progressCutBeyondCombinedFragment(getCurrentState(), combinedFragment);
		}
		return ActiveMSDProgress.NO_PROGRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void assignValue(OCL ocl, String variableName, Object value) {
		if (ocl == null)
			ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(this);
		
		boolean reassigningAlreadyCreatedVariable = false;
		
		try {
			ocl.getEvaluationEnvironment().add(variableName, value);
			logger.debug("Adding OCL variable \"" + variableName + "\" with the value: " + value);
		} catch (IllegalArgumentException e) {
			reassigningAlreadyCreatedVariable = true;
			ocl.getEvaluationEnvironment().replace(variableName, value);
			logger.debug("Reassigning OCL variable \"" + variableName + "\" the value: " + value);
			//logger.error("Variable " + variableName + " is already bound");
			//return;
		}

		if (!reassigningAlreadyCreatedVariable){
			Variable variable = EcoreFactory.eINSTANCE.createVariable();
			variable.setName(variableName);
			EClassifier type = null;

			if (type == null) {
				if (value instanceof EObject) {
					type = ((EObject) value).eClass();
				} else if (value instanceof Integer) {
					type = ocl.getEnvironment().getOCLStandardLibrary()
							.getInteger();
				} else if (value instanceof Float || value instanceof Double) {
					type = ocl.getEnvironment().getOCLStandardLibrary().getReal();
				} else if (value instanceof Boolean) {
					type = ocl.getEnvironment().getOCLStandardLibrary()
							.getBoolean();
				}
			}
			variable.setType(type);

			// add Variable to OCL environment
			ocl.getEnvironment().addElement(variableName, variable, true);			
		}


	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @throws ParserException, SemanticException 
	 * @generated NOT
	 */
	public Object assignValueExpression(OCL ocl, String variableName, String valueOCLExpression) throws ParserException, SemanticException {
		if (ocl == null)
			ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(this);
		
		Helper helper = ocl.createOCLHelper();
		
		//In case we want to manipulate the hole object system 
					EObject context = this.getObjectSystem().getRootObjects().get(0);
		
					EClass eClass = context.eClass();
					helper.setContext(eClass);
//	helper.setContext(org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass());

	// Evaluation of Constraint-condition
	OCLExpression expr;
	expr = helper.createQuery(valueOCLExpression);
	Object result = ocl.evaluate(context, expr);		
	assignValue(ocl, variableName, result);
	return result;
	}

	

	protected void computeAssignment(CombinedFragment combinedFragment) {		

		String assignmentExpression = getCombinedFragmentGuardString(combinedFragment);
		if(assignmentExpression == null)
			return;
		
		
		//for each assignment
		// Joel: there will usually just be one...
		
		String variableName = "";
		String valueExpression = "";

		if (!assignmentExpression.contains("=")){
			logger.error("The assignment expression must be of the form \"<variable-name> = <OCL-expression>\"): " + assignmentExpression);
		}else{
			variableName = assignmentExpression.substring(0, assignmentExpression.indexOf("=")).trim();
			if ("".equals(variableName)){
				logger.error("The variable name mut not be empty");
			}
		} 
		logger.debug("assigninf variable: " + variableName);
		valueExpression = assignmentExpression.substring(assignmentExpression.indexOf("=")+1).trim();
		logger.debug("binding assignment value expression: " + valueExpression);
		
		assignValueExpressionResultToVariable(variableName, valueExpression);				
	}

	private void assignValueExpressionResultToVariable(String variableName,
			String valueExpression) {
		OCL oclForCut = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(this);
		try {
			Object value = assignValueExpression(oclForCut, variableName, valueExpression);
			assignValueToVariable(variableName, value);
		} catch (SemanticException e) {
			logger.error(e);
		} catch (ParserException e) {
			logger.error(e);
		}
	}



	
	protected boolean isConditionTrue(CombinedFragment combinedFragment) {

		String conditionString = getCombinedFragmentGuardString(combinedFragment);
		if(conditionString == null)
			return false;

		boolean result = true;
		
        OCL ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(this);
        Helper helper = ocl.createOCLHelper();
        Lifeline lifeline = combinedFragment.getCovereds().get(0);
		EObject context = getLifelineBindings().getLifelineToEObjectMap().get(lifeline);

		EClass eClass = context.eClass();
		helper.setContext(eClass);
        // Evaluation of Constraint-condition
        OCLExpression expr;
        try {
                expr = helper.createQuery(conditionString);
                //TODO @Joel is it correct to use check?
                result= ocl.check(context, expr);
                
        } catch (ParserException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
        if (!result){
                //if one of the expression is false return false (as all the expressions were in AND)
                //TODO @Joel is it correct?
                return false;
        }      			

		return result;		
	}

	protected void progressCutBeyondCombinedFragment(ActiveMSDCut cut, CombinedFragment combinedFragment) {
		for (Lifeline lifeline: combinedFragment.getCovereds()){
			cut.progressCutLocationOnLifeline(lifeline,combinedFragment);
		}
	}
	
	protected String getCombinedFragmentGuardString(CombinedFragment combinedFragment){
		return RuntimeUtil.Helper.getCombinedFragmentGuardString(combinedFragment);
	}
	
	
	protected boolean isMessageEnabled(Message message, MessageEvent event){
		return getCurrentState().isMessageEnabled(message);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Iterates over the in the MSD twice. Once to calculate the enabled events, a second time to calculate the violating events.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void initializeRelevantEventsForActiveProcess(){
		
		if(logger.isDebugEnabled()){
			logger.debug("initializeRelevantEvents for active MSD " + this);
		}
		
		Interaction msd = getInteraction();

		boolean cutIsHot = getCurrentState().isHot();
		boolean cutIsExecuted = getCurrentState().isExecuted();
				
		// First iteration over MSD messages to calculate the enabled events 
		for(Message message : msd.getMessages()){
			
			if(logger.isDebugEnabled()){
				logger.debug("Creating an MSDModalMessageEvent for the diagram message " + message);
			}
			
			MessageEvent event = getMessageEvent(message, false);
			if (isMessageEnabled(message, event)){
				
				
				if(event.getSendingObject() == null){
					logger.error("Sending object of enabled message not defined.");
				}
				if(event.getReceivingObject() == null){
					logger.error("Receiving object of enabled message not defined.");
				}
				
				MessageEvent enabledMessageEvent = event;
				if(logger.isDebugEnabled()){
					logger.debug("Adding enabled message event " + RuntimeUtil.Helper.getMessageEventString(enabledMessageEvent) + "for active MSD " + this);
				}	
				getEnabledEvents().put(message, enabledMessageEvent);

				// if a message event that is message unifiable with this enabled message event was already identified as a violating event,
				// this means that this event is actually not violating.
//				Iterator<Map.Entry<NamedElement, MessageEvent>> violatingMessageEventsMapEntriesIterator = getViolatingEvents().iterator(); 
//				while (violatingMessageEventsMapEntriesIterator.hasNext()) {
//					Map.Entry<NamedElement,MessageEvent> mapEntry = violatingMessageEventsMapEntriesIterator.next();
//					if (RuntimeUtil.Helper.isParameterUnifiable(enabledMessageEvent, mapEntry.getValue()) && !mapEntry.getValue().equals(enabledMessageEvent.getAbstractMessageEvent())){
//						violatingMessageEventsMapEntriesIterator.remove();
//					}
					/*
					if(enabledMessageEvent.getAbstractMessageEvent() != null){
						if (mapEntry.getValue() == enabledMessageEvent.getAbstractMessageEvent()){
							violatingMessageEventsMapEntriesIterator.remove();
							if (logger.isDebugEnabled())
								logger.debug("Previously violating event is not violating because a message event message unifiable with this event is enabled: " + RuntimeUtil.Helper.getMessageEventString(enabledMessageEvent));
						}
					}else{
						if (mapEntry.getValue() == enabledMessageEvent){
							violatingMessageEventsMapEntriesIterator.remove();
							if (logger.isDebugEnabled())
								logger.debug("Previously violating event is not violating because a message event message unifiable with this event is enabled: " + RuntimeUtil.Helper.getMessageEventString(enabledMessageEvent));
						}
					}
					*/
//				}
				
				if (getMsdUtil().getHotMessages().contains(message)){
					cutIsHot = true;
				} else {
					cutIsHot = false;
				}
				if(getMsdUtil().getExecutedMessages().contains(message)){
					cutIsExecuted = true;
				} else {
					cutIsExecuted = false;
				}
			}
		}
		
		// Second iteration over MSD messages to calculate the violating events 
		for(Message message : msd.getMessages()){
			MessageEvent potentiallyViolatingMessageEvent = getMessageEvent(message, false);
			if (!isMessageEnabled(message, potentiallyViolatingMessageEvent)){

				// this may be if the sending or receiving lifeline is not bound. In this case, we ignore corresponding message events.
				if (potentiallyViolatingMessageEvent == null) continue;

				if(logger.isDebugEnabled()){
					logger.debug("Message event " + RuntimeUtil.Helper.getMessageEventString(potentiallyViolatingMessageEvent) + " is potentially a violating message event for this active MSD");
				}

				// if a message event that is message unifiable with this potentially violating message event was already identified as an enabled event, 
				// the potentially violating event is not violating, and thus we don't add it to the map of violating events
				boolean messageUnifiableEventAlreadyEnabled = false;
				for (MessageEvent enabledMessageEvent : getEnabledEvents().values()) {
					
					if (enabledMessageEvent.isConcrete() && potentiallyViolatingMessageEvent.isConcrete()){
						if (RuntimeUtil.Helper
								.isParameterUnifiable(enabledMessageEvent,
										potentiallyViolatingMessageEvent,
										this))
							messageUnifiableEventAlreadyEnabled = true;
					}else{
						// 1. it can be that the enabled message event is not concrete. In this case the enabled message event can be unified with any value set for the parameter.
						// In this case, it is therefore, it is sufficient to check if the enabled message event and the potentially violating message event are message unifiable
						// 2. it can be that the potentially violating message event is not concrete. In this case, every concrete instance of this event is violating unless it 
						// is parameter unifiable with an enabled event. This, however, will be calculated in the MSDRuntimeState.updateAbstractMessageEventForConcreteEvent

						if (RuntimeUtil.Helper.isMessageUnifiable(enabledMessageEvent, potentiallyViolatingMessageEvent))
							messageUnifiableEventAlreadyEnabled = true;
					}
				}
				if(!messageUnifiableEventAlreadyEnabled){
					if(logger.isDebugEnabled()){
						logger.debug("Message event " + RuntimeUtil.Helper.getMessageEventString(potentiallyViolatingMessageEvent) + " is added as a violating message event for this active MSD, no enabled message event that is unifiable with it was found.");
					}
					
					// forbidden message?
					if (getMsdUtil().getHotForbiddenMessages().contains(message)){
						getHotForbiddenEvents().put(message,potentiallyViolatingMessageEvent);
						getViolatingEvents().put(message, potentiallyViolatingMessageEvent);
					}
					else if (getMsdUtil().getColdForbiddenMessages().contains(message))
						getColdForbiddenEvents().put(message,potentiallyViolatingMessageEvent);
					else // regular message in the MSD
						getViolatingEvents().put(message, potentiallyViolatingMessageEvent);
				}
				else{
					if(logger.isDebugEnabled()){
						logger.debug("Message event " + RuntimeUtil.Helper.getMessageEventString(potentiallyViolatingMessageEvent) + " is not added as a violating message event for this active MSD, an enabled message event that is unifiable with it was found.");
					}
				}
			}
		}
		getCurrentState().setHot(cutIsHot);
		getCurrentState().setExecuted(cutIsExecuted);
	}


	protected MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue){
		return getObjectSystem().getMessageEvent(eOperation, eStructuralFeature, sendingObject, receivingObject, parameterValue);
	}

	protected MessageEvent getMessageEvent(Message message, boolean ignoreParameter){
		if(logger.isDebugEnabled()){
			logger.debug("Creating message event for message " + message + " in the context of the active MSD "+ this);
		}

		
		Object argumentValue = ignoreParameter ? null : getRuntimeUtil()
				.getParameterValue(
						message,
						getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(
								this));
		String varName=null;
		if (message.getArguments().size() != 0) {
			ValueSpecification valueSpecification = message.getArguments().get(
					0);
			if (RuntimeUtil.Helper.isVariableReference(valueSpecification)) {
				varName = (String) RuntimeUtil.Helper
						.getReferencedVariable(valueSpecification);
				argumentValue = getVariableValue(varName);
			}
		}
		Lifeline sendingLifeline = RuntimeUtil.Helper.getSendingLifeline(message);
		Lifeline receivingLifeline = RuntimeUtil.Helper.getReceivingLifeline(message);
		EObject sendingObject = getLifelineBindings().getLifelineToEObjectMap().get(sendingLifeline);
		EObject receivingObject = getLifelineBindings().getLifelineToEObjectMap().get(receivingLifeline);
		if (sendingObject != null && receivingObject != null){
			Assert.isTrue(getObjectSystem().containsEObject(sendingObject));
			Assert.isTrue(getObjectSystem().containsEObject(receivingObject));
			EOperation eOperation = getRuntimeUtil().getOperation(message);
			EStructuralFeature eStructuralFeature = getRuntimeUtil().getEStructuralFeature(message);
			MessageEvent event = getMessageEvent(eOperation, eStructuralFeature, sendingObject, receivingObject, argumentValue);
			event.setMessage(message);
			return event;
		}else{
			if (logger.isDebugEnabled()){
				if(logger.isDebugEnabled()){
					logger.debug("Cannot create message event for message " + message + " in active MSD " + this);
				}
				if (sendingObject != null)
					logger.debug("   --   because the sending lifeline is unbound");
				if (receivingObject != null)
					logger.debug("   --   because the receiving lifeline is unbound");
			}
			
			return null;
		}
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS:
				if (resolve) return getLifelineBindings();
				return basicGetLifelineBindings();
			case RuntimePackage.ACTIVE_MSD__INTERACTION:
				if (resolve) return getInteraction();
				return basicGetInteraction();
			case RuntimePackage.ACTIVE_MSD__MSD_UTIL:
				if (resolve) return getMsdUtil();
				return basicGetMsdUtil();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS:
				setLifelineBindings((ActiveMSDLifelineBindings)newValue);
				return;
			case RuntimePackage.ACTIVE_MSD__MSD_UTIL:
				setMsdUtil((MSDUtil)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS:
				setLifelineBindings((ActiveMSDLifelineBindings)null);
				return;
			case RuntimePackage.ACTIVE_MSD__MSD_UTIL:
				setMsdUtil((MSDUtil)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD__LIFELINE_BINDINGS:
				return lifelineBindings != null;
			case RuntimePackage.ACTIVE_MSD__INTERACTION:
				return basicGetInteraction() != null;
			case RuntimePackage.ACTIVE_MSD__MSD_UTIL:
				return msdUtil != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public String toString() {
		String bindingString = "[";
		
		EList<Lifeline> lifelines = getInteraction().getLifelines();
		
		for (Lifeline lifeline : lifelines) {
			bindingString += lifeline.getRepresents().getName() + "->";
			EObject eObjectBoundByLifeline = getLifelineBindings().getLifelineToEObjectMap().get(lifeline);
			if(eObjectBoundByLifeline != null){
				bindingString += RuntimeUtil.Helper.getEObjectName(eObjectBoundByLifeline);
			} else {
				bindingString += "<unbound>";
			}
			bindingString += ", ";
		}
		bindingString = bindingString.substring(0, bindingString.length()-2);
		bindingString += "]";
		
		String returnString = "active ";
		if (RuntimeUtil.Helper.isEnvironmentAssumption(getInteraction())){
			returnString += "assumption ";
		}else{
			returnString += "requirement ";
		}
		
		String cutString = "";
		cutString += " (";

		for (Lifeline lifeline : lifelines) {
			cutString += ":" + RuntimeUtil.Helper.getInteractionFragmentNumberOnLifeline(getCurrentState().getLifelineToInteractionFragmentMap().get(lifeline), lifeline); 

			if (lifelines.get(lifelines.size() - 1) != lifeline) {// not last one
				cutString += ", ";
			} 
		}
		cutString += ")";
		
		return returnString + "MSD: " + getInteraction().getName() + " " + bindingString + cutString;	
	}

	/**
	 * @generated NOT
	 */
	@Override
	public ActiveMSDCut getCurrentState() {
		return (ActiveMSDCut) super.getCurrentState();
	}

	/**
	 * @generated NOT
	 */
	public ActiveProcessKeyWrapper getKeyWrapper(){
		return new ActiveMSDKeyWrapper(this);
	}

} //ActiveMSDImpl
