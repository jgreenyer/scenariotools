/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.runtime.ModalMessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Modal Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getEnabledInActiveProcess <em>Enabled In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getColdViolatingInActiveProcess <em>Cold Violating In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getSafetyViolatingInActiveProcess <em>Safety Violating In Active Process</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent <em>Parent Symbolic MSD Modal Message Event</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getConcreteMSDModalMessageEvent <em>Concrete MSD Modal Message Event</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent()
 * @model
 * @generated
 */
public interface MSDModalMessageEvent extends ModalMessageEvent {
	/**
	 * Returns the value of the '<em><b>Enabled In Active Process</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled In Active Process</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled In Active Process</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent_EnabledInActiveProcess()
	 * @model
	 * @generated
	 */
	EList<ActiveProcess> getEnabledInActiveProcess();

	/**
	 * Returns the value of the '<em><b>Cold Violating In Active Process</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cold Violating In Active Process</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cold Violating In Active Process</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent_ColdViolatingInActiveProcess()
	 * @model
	 * @generated
	 */
	EList<ActiveProcess> getColdViolatingInActiveProcess();

	/**
	 * Returns the value of the '<em><b>Safety Violating In Active Process</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violating In Active Process</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violating In Active Process</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent_SafetyViolatingInActiveProcess()
	 * @model
	 * @generated
	 */
	EList<ActiveProcess> getSafetyViolatingInActiveProcess();

	/**
	 * Returns the value of the '<em><b>Parent Symbolic MSD Modal Message Event</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getConcreteMSDModalMessageEvent <em>Concrete MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Symbolic MSD Modal Message Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Symbolic MSD Modal Message Event</em>' reference.
	 * @see #setParentSymbolicMSDModalMessageEvent(MSDModalMessageEvent)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent_ParentSymbolicMSDModalMessageEvent()
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getConcreteMSDModalMessageEvent
	 * @model opposite="concreteMSDModalMessageEvent"
	 * @generated
	 */
	MSDModalMessageEvent getParentSymbolicMSDModalMessageEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent <em>Parent Symbolic MSD Modal Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Symbolic MSD Modal Message Event</em>' reference.
	 * @see #getParentSymbolicMSDModalMessageEvent()
	 * @generated
	 */
	void setParentSymbolicMSDModalMessageEvent(MSDModalMessageEvent value);

	/**
	 * Returns the value of the '<em><b>Concrete MSD Modal Message Event</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.MSDModalMessageEvent}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent <em>Parent Symbolic MSD Modal Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concrete MSD Modal Message Event</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concrete MSD Modal Message Event</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModalMessageEvent_ConcreteMSDModalMessageEvent()
	 * @see org.scenariotools.msd.runtime.MSDModalMessageEvent#getParentSymbolicMSDModalMessageEvent
	 * @model opposite="parentSymbolicMSDModalMessageEvent"
	 * @generated
	 */
	EList<MSDModalMessageEvent> getConcreteMSDModalMessageEvent();

} // MSDModalMessageEvent
