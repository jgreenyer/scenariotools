/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.InteractionFragment;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper;
import org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveProcesses <em>Active Processes</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuations <em>Active MSD Variable Valuations</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveStates <em>Active States</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindings <em>Active MSD Lifeline Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindings <em>Active MSS Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveProcessKeyWrapperToActiveProcessMap <em>Active Process Key Wrapper To Active Process Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveStateKeyWrapperToActiveStateMap <em>Active State Key Wrapper To Active State Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap <em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap <em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getObjectSystemKeyWrapperToMSDObjectSystemMapEntry <em>Object System Key Wrapper To MSD Object System Map Entry</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getObjectSystems <em>Object Systems</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getFinalFragment <em>Final Fragment</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveRoleBindings <em>Active Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ElementContainer#getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap <em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer()
 * @model
 * @generated
 */
public interface ElementContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Active Processes</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveProcess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Processes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Processes</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveProcesses()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveProcess> getActiveProcesses();

	/**
	 * Returns the value of the '<em><b>Active MSD Variable Valuations</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Variable Valuations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Variable Valuations</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSDVariableValuations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMSDVariableValuations> getActiveMSDVariableValuations();

	/**
	 * Returns the value of the '<em><b>Active States</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active States</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveStates()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveState> getActiveStates();

	/**
	 * Returns the value of the '<em><b>Active MSD Lifeline Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Lifeline Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Lifeline Bindings</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSDLifelineBindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMSDLifelineBindings> getActiveMSDLifelineBindings();

	/**
	 * Returns the value of the '<em><b>Active MSS Role Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSS Role Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSS Role Bindings</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSSRoleBindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMSSRoleBindings> getActiveMSSRoleBindings();

	/**
	 * Returns the value of the '<em><b>Active Process Key Wrapper To Active Process Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.ActiveProcess},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Process Key Wrapper To Active Process Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Process Key Wrapper To Active Process Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveProcessKeyWrapperToActiveProcessMap()
	 * @model mapType="org.scenariotools.msd.runtime.ActiveProcessKeyWrapperToActiveProcessMapEntry<org.scenariotools.msd.runtime.ActiveProcessKeyWrapper, org.scenariotools.msd.runtime.ActiveProcess>" transient="true"
	 * @generated
	 */
	EMap<ActiveProcessKeyWrapper, ActiveProcess> getActiveProcessKeyWrapperToActiveProcessMap();

	/**
	 * Returns the value of the '<em><b>Active State Key Wrapper To Active State Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.ActiveState},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active State Key Wrapper To Active State Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active State Key Wrapper To Active State Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveStateKeyWrapperToActiveStateMap()
	 * @model mapType="org.scenariotools.msd.runtime.ActiveStateKeyWrapperToActiveStateMapEntry<org.scenariotools.msd.runtime.ActiveStateKeyWrapper, org.scenariotools.msd.runtime.ActiveState>" transient="true"
	 * @generated
	 */
	EMap<ActiveStateKeyWrapper, ActiveState> getActiveStateKeyWrapperToActiveStateMap();

	/**
	 * Returns the value of the '<em><b>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap()
	 * @model mapType="org.scenariotools.msd.runtime.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry<org.scenariotools.msd.runtime.ActiveMSDLifelineBindingsKeyWrapper, org.scenariotools.msd.runtime.ActiveMSDLifelineBindings>" transient="true"
	 * @generated
	 */
	EMap<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> getActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap();

	/**
	 * Returns the value of the '<em><b>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.ActiveMSDVariableValuations},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSD Variable Valuations Key Wrapper To Active MSD Variable Valuations Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap()
	 * @model mapType="org.scenariotools.msd.runtime.ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry<org.scenariotools.msd.runtime.ActiveMSDVariableValuationsKeyWrapper, org.scenariotools.msd.runtime.ActiveMSDVariableValuations>" transient="true"
	 * @generated
	 */
	EMap<ActiveMSDVariableValuationsKeyWrapper, ActiveMSDVariableValuations> getActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap();

	/**
	 * Returns the value of the '<em><b>Object System Key Wrapper To MSD Object System Map Entry</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.MSDObjectSystem},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System Key Wrapper To MSD Object System Map Entry</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System Key Wrapper To MSD Object System Map Entry</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ObjectSystemKeyWrapperToMSDObjectSystemMapEntry()
	 * @model mapType="org.scenariotools.msd.runtime.ObjectSystemKeyWrapperToMSDObjectSystemMapEntry<org.scenariotools.msd.runtime.ObjectSystemKeyWrapper, org.scenariotools.msd.runtime.MSDObjectSystem>" transient="true"
	 * @generated
	 */
	EMap<ObjectSystemKeyWrapper, MSDObjectSystem> getObjectSystemKeyWrapperToMSDObjectSystemMapEntry();

	/**
	 * Returns the value of the '<em><b>Object Systems</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.MSDObjectSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Systems</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Systems</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ObjectSystems()
	 * @model containment="true"
	 * @generated
	 */
	EList<MSDObjectSystem> getObjectSystems();

	/**
	 * Returns the value of the '<em><b>Final Fragment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Fragment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Fragment</em>' containment reference.
	 * @see #setFinalFragment(InteractionFragment)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_FinalFragment()
	 * @model containment="true"
	 * @generated
	 */
	InteractionFragment getFinalFragment();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ElementContainer#getFinalFragment <em>Final Fragment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Fragment</em>' containment reference.
	 * @see #getFinalFragment()
	 * @generated
	 */
	void setFinalFragment(InteractionFragment value);

	/**
	 * Returns the value of the '<em><b>Msd Runtime State Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Runtime State Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Runtime State Graph</em>' container reference.
	 * @see #setMsdRuntimeStateGraph(MSDRuntimeStateGraph)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_MsdRuntimeStateGraph()
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer
	 * @model opposite="elementContainer" transient="false"
	 * @generated
	 */
	MSDRuntimeStateGraph getMsdRuntimeStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msd Runtime State Graph</em>' container reference.
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 */
	void setMsdRuntimeStateGraph(MSDRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Active Role Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Role Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Role Bindings</em>' containment reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveRoleBindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMSSRoleBindings> getActiveRoleBindings();

	/**
	 * Returns the value of the '<em><b>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active MSS Role Bindings Key Wrapper To Active MSS Role Bindings Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getElementContainer_ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap()
	 * @model mapType="org.scenariotools.msd.runtime.ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry<org.scenariotools.msd.runtime.ActiveMSSRoleBindingsKeyWrapper, org.scenariotools.msd.runtime.ActiveMSSRoleBindings>" transient="true"
	 * @generated
	 */
	EMap<ActiveMSSRoleBindingsKeyWrapper, ActiveMSSRoleBindings> getActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveProcess getActiveProcess(ActiveProcess activeProcess);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveState getActiveState(ActiveState activeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveMSDLifelineBindings getActiveMSDLifelineBindings(ActiveMSDLifelineBindings activeMSDLifelineBindings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveMSDVariableValuations getActiveMSDVariableValuations(ActiveMSDVariableValuations activeMSDVariableValuations);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	MSDObjectSystem getObjectSystem(MSDObjectSystem objectSystem);

	ActiveState getActiveState(ActiveMSD activeMSDImpl, ActiveMSDCut currentState);

} // ElementContainer
