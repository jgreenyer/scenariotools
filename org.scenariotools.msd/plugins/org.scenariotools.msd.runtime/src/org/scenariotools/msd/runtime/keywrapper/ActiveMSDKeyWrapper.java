package org.scenariotools.msd.runtime.keywrapper;

import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;

public class ActiveMSDKeyWrapper extends ActiveProcessKeyWrapper {
	public ActiveMSDKeyWrapper(ActiveMSD activeMSD) {
		this(activeMSD.getCurrentState(), activeMSD.getLifelineBindings(),
				activeMSD.getVariableValuations());
	}

	public ActiveMSDKeyWrapper(ActiveMSDCut activeMSDCut,
			ActiveMSDLifelineBindings activeMSDLifelineBindings,
			ActiveMSDVariableValuations activeMSDVariableValuations) {
		addSubObject(activeMSDCut);
		addSubObject(activeMSDLifelineBindings);
		addSubObject(activeMSDVariableValuations);
	}
}
