/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSS Role Bindings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSSRoleBindings#getPropertyToEObjectMap <em>Property To EObject Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSSRoleBindings()
 * @model
 * @generated
 */
public interface ActiveMSSRoleBindings extends EObject {
	/**
	 * Returns the value of the '<em><b>Property To EObject Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Property},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property To EObject Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property To EObject Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSSRoleBindings_PropertyToEObjectMap()
	 * @model mapType="org.scenariotools.msd.runtime.PropertyToEObjectMapEntry<org.eclipse.uml2.uml.Property, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<Property, EObject> getPropertyToEObjectMap();

} // ActiveMSSRoleBindings
