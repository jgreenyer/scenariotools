/**
 */
package org.scenariotools.msd.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Execution Semantics</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimePackage#getExecutionSemantics()
 * @model
 * @generated
 */
public enum ExecutionSemantics implements Enumerator {
	/**
	 * The '<em><b>Play Out</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLAY_OUT_VALUE
	 * @generated
	 * @ordered
	 */
	PLAY_OUT(0, "PlayOut", "PlayOut"),

	/**
	 * The '<em><b>Always All Events</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALWAYS_ALL_EVENTS_VALUE
	 * @generated
	 * @ordered
	 */
	ALWAYS_ALL_EVENTS(1, "AlwaysAllEvents", "AlwaysAllRelevantMessageEvents"), /**
	 * The '<em><b>Play Out But Waiting Possible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLAY_OUT_BUT_WAITING_POSSIBLE_VALUE
	 * @generated
	 * @ordered
	 */
	PLAY_OUT_BUT_WAITING_POSSIBLE(2, "PlayOutButWaitingPossible", "PlayOutButWaitingPossibleWithActiveEvents");

	/**
	 * The '<em><b>Play Out</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Play Out</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLAY_OUT
	 * @model name="PlayOut"
	 * @generated
	 * @ordered
	 */
	public static final int PLAY_OUT_VALUE = 0;

	/**
	 * The '<em><b>Always All Events</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Always All Events</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ALWAYS_ALL_EVENTS
	 * @model name="AlwaysAllEvents" literal="AlwaysAllRelevantMessageEvents"
	 * @generated
	 * @ordered
	 */
	public static final int ALWAYS_ALL_EVENTS_VALUE = 1;

	/**
	 * The '<em><b>Play Out But Waiting Possible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Play Out But Waiting Possible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLAY_OUT_BUT_WAITING_POSSIBLE
	 * @model name="PlayOutButWaitingPossible" literal="PlayOutButWaitingPossibleWithActiveEvents"
	 * @generated
	 * @ordered
	 */
	public static final int PLAY_OUT_BUT_WAITING_POSSIBLE_VALUE = 2;

	/**
	 * An array of all the '<em><b>Execution Semantics</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ExecutionSemantics[] VALUES_ARRAY =
		new ExecutionSemantics[] {
			PLAY_OUT,
			ALWAYS_ALL_EVENTS,
			PLAY_OUT_BUT_WAITING_POSSIBLE,
		};

	/**
	 * A public read-only list of all the '<em><b>Execution Semantics</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ExecutionSemantics> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Execution Semantics</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ExecutionSemantics get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExecutionSemantics result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Execution Semantics</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ExecutionSemantics getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExecutionSemantics result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Execution Semantics</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ExecutionSemantics get(int value) {
		switch (value) {
			case PLAY_OUT_VALUE: return PLAY_OUT;
			case ALWAYS_ALL_EVENTS_VALUE: return ALWAYS_ALL_EVENTS;
			case PLAY_OUT_BUT_WAITING_POSSIBLE_VALUE: return PLAY_OUT_BUT_WAITING_POSSIBLE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ExecutionSemantics(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ExecutionSemantics
