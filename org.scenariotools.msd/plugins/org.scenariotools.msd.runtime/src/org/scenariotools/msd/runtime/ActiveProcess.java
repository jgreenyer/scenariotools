/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.NamedElement;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper;
import org.scenariotools.msd.util.RuntimeUtil;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getVariableValuations <em>Variable Valuations</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getColdForbiddenEvents <em>Cold Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getHotForbiddenEvents <em>Hot Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getViolatingEvents <em>Violating Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getEnabledEvents <em>Enabled Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getCurrentState <em>Current State</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getObjectSystem <em>Object System</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveProcess#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess()
 * @model
 * @generated
 */
public interface ActiveProcess extends EObject {
	/**
	 * Returns the value of the '<em><b>Variable Valuations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Valuations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Valuations</em>' reference.
	 * @see #setVariableValuations(ActiveMSDVariableValuations)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_VariableValuations()
	 * @model
	 * @generated
	 */
	ActiveMSDVariableValuations getVariableValuations();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveProcess#getVariableValuations <em>Variable Valuations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable Valuations</em>' reference.
	 * @see #getVariableValuations()
	 * @generated
	 */
	void setVariableValuations(ActiveMSDVariableValuations value);

	/**
	 * Returns the value of the '<em><b>Cold Forbidden Events</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.NamedElement},
	 * and the value is of type {@link org.scenariotools.events.MessageEvent},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cold Forbidden Events</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cold Forbidden Events</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_ColdForbiddenEvents()
	 * @model mapType="org.scenariotools.msd.runtime.NamedElementToMessageEventMapEntry<org.eclipse.uml2.uml.NamedElement, org.scenariotools.events.MessageEvent>"
	 * @generated
	 */
	EMap<NamedElement, MessageEvent> getColdForbiddenEvents();

	/**
	 * Returns the value of the '<em><b>Hot Forbidden Events</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.NamedElement},
	 * and the value is of type {@link org.scenariotools.events.MessageEvent},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * refers to a forbidden message in the diagram, not to be confused with "safety violating"
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Hot Forbidden Events</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_HotForbiddenEvents()
	 * @model mapType="org.scenariotools.msd.runtime.NamedElementToMessageEventMapEntry<org.eclipse.uml2.uml.NamedElement, org.scenariotools.events.MessageEvent>"
	 * @generated
	 */
	EMap<NamedElement, MessageEvent> getHotForbiddenEvents();

	/**
	 * Returns the value of the '<em><b>Violating Events</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.NamedElement},
	 * and the value is of type {@link org.scenariotools.events.MessageEvent},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Violating Events</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Violating Events</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_ViolatingEvents()
	 * @model mapType="org.scenariotools.msd.runtime.NamedElementToMessageEventMapEntry<org.eclipse.uml2.uml.NamedElement, org.scenariotools.events.MessageEvent>"
	 * @generated
	 */
	EMap<NamedElement, MessageEvent> getViolatingEvents();

	/**
	 * Returns the value of the '<em><b>Enabled Events</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.NamedElement},
	 * and the value is of type {@link org.scenariotools.events.MessageEvent},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled Events</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled Events</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_EnabledEvents()
	 * @model mapType="org.scenariotools.msd.runtime.NamedElementToMessageEventMapEntry<org.eclipse.uml2.uml.NamedElement, org.scenariotools.events.MessageEvent>"
	 * @generated
	 */
	EMap<NamedElement, MessageEvent> getEnabledEvents();

	/**
	 * Returns the value of the '<em><b>Runtime Util</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Util</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Util</em>' reference.
	 * @see #setRuntimeUtil(RuntimeUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_RuntimeUtil()
	 * @model
	 * @generated
	 */
	RuntimeUtil getRuntimeUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveProcess#getRuntimeUtil <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Util</em>' reference.
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	void setRuntimeUtil(RuntimeUtil value);

	/**
	 * Returns the value of the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current State</em>' reference.
	 * @see #setCurrentState(ActiveState)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_CurrentState()
	 * @model
	 * @generated
	 */
	ActiveState getCurrentState();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveProcess#getCurrentState <em>Current State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current State</em>' reference.
	 * @see #getCurrentState()
	 * @generated
	 */
	void setCurrentState(ActiveState value);

	/**
	 * Returns the value of the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System</em>' reference.
	 * @see #setObjectSystem(MSDObjectSystem)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_ObjectSystem()
	 * @model
	 * @generated
	 */
	MSDObjectSystem getObjectSystem();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveProcess#getObjectSystem <em>Object System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object System</em>' reference.
	 * @see #getObjectSystem()
	 * @generated
	 */
	void setObjectSystem(MSDObjectSystem value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveProcess_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.ActiveProcess#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isSafetyViolating(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isColdViolating(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calculateRelevantEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveMSDProgress progressEnabledHiddenEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInHotCut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInExecutedCut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oclDataType="org.scenariotools.msd.util.OCL" valueDataType="org.scenariotools.msd.util.PlainJavaObject"
	 * @generated
	 */
	void assignValue(OCL ocl, String variableName, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.msd.util.PlainJavaObject" exceptions="org.scenariotools.msd.runtime.SemanticException org.scenariotools.msd.runtime.ParserException" oclDataType="org.scenariotools.msd.util.OCL"
	 * @generated
	 */
	Object assignValueExpression(OCL ocl, String variableName, String valueOCLExpression) throws SemanticException, ParserException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isRelavant(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated NOT
	 */
	public Object getVariableValue(String varName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated NOT
	 */
	public void assignValueToVariable(String varName, Object value);

	/**
	 * @generated NOT
	 */
	public ActiveProcessKeyWrapper getKeyWrapper();

} // ActiveProcess
