/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.events.util.EventsUtil;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MessageEventContainer;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.RuntimeUtil.Helper;
import org.scenariotools.runtime.impl.ObjectSystemImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSD Object System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getInitializingEnvironmentMSDModalMessageEvents <em>Initializing Environment MSD Modal Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getInitializingSystemMSDModalMessageEvents <em>Initializing System MSD Modal Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getAssumptionMSDInitializingMessageEvents <em>Assumption MSD Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getRequirementMSDInitializingMessageEvents <em>Requirement MSD Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getEObjectToSendableEOperationMap <em>EObject To Sendable EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getEObjectToReceivableEOperationMap <em>EObject To Receivable EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getEClassToInstanceEObjectMap <em>EClass To Instance EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getEObjectsToUMLClassesItIsInstanceOfMap <em>EObjects To UML Classes It Is Instance Of Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getStaticLifelineToEObjectBindings <em>Static Lifeline To EObject Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getStaticRoleToEObjectBindings <em>Static Role To EObject Bindings</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getRolesToEObjectsMappingContainer <em>Roles To EObjects Mapping Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getMessageEventContainer <em>Message Event Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDObjectSystemImpl#getMessageEventKeyWrapperToMessageEventMap <em>Message Event Key Wrapper To Message Event Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDObjectSystemImpl extends ObjectSystemImpl implements MSDObjectSystem {
	
	/**
	 * The cached value of the '{@link #getRuntimeUtil() <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeUtil()
	 * @generated
	 * @ordered
	 */
	protected RuntimeUtil runtimeUtil;
	/**
	 * The cached value of the '{@link #getInitializingEnvironmentMSDModalMessageEvents() <em>Initializing Environment MSD Modal Message Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitializingEnvironmentMSDModalMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MSDModalMessageEvent> initializingEnvironmentMSDModalMessageEvents;
	/**
	 * The cached value of the '{@link #getInitializingSystemMSDModalMessageEvents() <em>Initializing System MSD Modal Message Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitializingSystemMSDModalMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MSDModalMessageEvent> initializingSystemMSDModalMessageEvents;
	/**
	 * The cached value of the '{@link #getAssumptionMSDInitializingMessageEvents() <em>Assumption MSD Initializing Message Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssumptionMSDInitializingMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> assumptionMSDInitializingMessageEvents;
	/**
	 * The cached value of the '{@link #getRequirementMSDInitializingMessageEvents() <em>Requirement MSD Initializing Message Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequirementMSDInitializingMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> requirementMSDInitializingMessageEvents;
	/**
	 * The cached value of the '{@link #getEObjectToSendableEOperationMap() <em>EObject To Sendable EOperation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectToSendableEOperationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<EOperation>> eObjectToSendableEOperationMap;
	/**
	 * The cached value of the '{@link #getEObjectToReceivableEOperationMap() <em>EObject To Receivable EOperation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectToReceivableEOperationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<EOperation>> eObjectToReceivableEOperationMap;
	/**
	 * The cached value of the '{@link #getEClassToInstanceEObjectMap() <em>EClass To Instance EObject Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClassToInstanceEObjectMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, EList<EObject>> eClassToInstanceEObjectMap;
	/**
	 * The cached value of the '{@link #getEObjectsToUMLClassesItIsInstanceOfMap() <em>EObjects To UML Classes It Is Instance Of Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectsToUMLClassesItIsInstanceOfMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<org.eclipse.uml2.uml.Class>> eObjectsToUMLClassesItIsInstanceOfMap;
	/**
	 * The cached value of the '{@link #getStaticLifelineToEObjectBindings() <em>Static Lifeline To EObject Bindings</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticLifelineToEObjectBindings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Lifeline, EObject> staticLifelineToEObjectBindings;
	/**
	 * The cached value of the '{@link #getStaticRoleToEObjectBindings() <em>Static Role To EObject Bindings</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticRoleToEObjectBindings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Property, EObject> staticRoleToEObjectBindings;
	/**
	 * The cached value of the '{@link #getRolesToEObjectsMappingContainer() <em>Roles To EObjects Mapping Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRolesToEObjectsMappingContainer()
	 * @generated
	 * @ordered
	 */
	protected RoleToEObjectMappingContainer rolesToEObjectsMappingContainer;
	/**
	 * The cached value of the '{@link #getMessageEventContainer() <em>Message Event Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventContainer()
	 * @generated
	 * @ordered
	 */
	protected MessageEventContainer messageEventContainer;
	/**
	 * The cached value of the '{@link #getMessageEventKeyWrapperToMessageEventMap() <em>Message Event Key Wrapper To Message Event Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventKeyWrapperToMessageEventMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<MessageEventKeyWrapper, MessageEvent> messageEventKeyWrapperToMessageEventMap;
	private static Logger logger = Activator.getLogManager().getLogger(
			MSDObjectSystemImpl.class.getName());

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDObjectSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MSD_OBJECT_SYSTEM;
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil getRuntimeUtil() {
		if (runtimeUtil != null && runtimeUtil.eIsProxy()) {
			InternalEObject oldRuntimeUtil = (InternalEObject)runtimeUtil;
			runtimeUtil = (RuntimeUtil)eResolveProxy(oldRuntimeUtil);
			if (runtimeUtil != oldRuntimeUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
			}
		}
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil basicGetRuntimeUtil() {
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		RuntimeUtil oldRuntimeUtil = runtimeUtil;
		runtimeUtil = newRuntimeUtil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSDModalMessageEvent> getInitializingEnvironmentMSDModalMessageEvents() {
		if (initializingEnvironmentMSDModalMessageEvents == null) {
			initializingEnvironmentMSDModalMessageEvents = new EObjectContainmentEList<MSDModalMessageEvent>(MSDModalMessageEvent.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS);
		}
		return initializingEnvironmentMSDModalMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSDModalMessageEvent> getInitializingSystemMSDModalMessageEvents() {
		if (initializingSystemMSDModalMessageEvents == null) {
			initializingSystemMSDModalMessageEvents = new EObjectContainmentEList<MSDModalMessageEvent>(MSDModalMessageEvent.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS);
		}
		return initializingSystemMSDModalMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getAssumptionMSDInitializingMessageEvents() {
		if (assumptionMSDInitializingMessageEvents == null) {
			assumptionMSDInitializingMessageEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS);
		}
		return assumptionMSDInitializingMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getRequirementMSDInitializingMessageEvents() {
		if (requirementMSDInitializingMessageEvents == null) {
			requirementMSDInitializingMessageEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS);
		}
		return requirementMSDInitializingMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<EOperation>> getEObjectToSendableEOperationMap() {
		if (eObjectToSendableEOperationMap == null) {
			eObjectToSendableEOperationMap = new EcoreEMap<EObject,EList<EOperation>>(RuntimePackage.Literals.EOBJECT_TO_EOPERATION_MAP_ENTRY, EObjectToEOperationMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP);
		}
		return eObjectToSendableEOperationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<EOperation>> getEObjectToReceivableEOperationMap() {
		if (eObjectToReceivableEOperationMap == null) {
			eObjectToReceivableEOperationMap = new EcoreEMap<EObject,EList<EOperation>>(RuntimePackage.Literals.EOBJECT_TO_EOPERATION_MAP_ENTRY, EObjectToEOperationMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP);
		}
		return eObjectToReceivableEOperationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, EList<EObject>> getEClassToInstanceEObjectMap() {
		if (eClassToInstanceEObjectMap == null) {
			eClassToInstanceEObjectMap = new EcoreEMap<EClass,EList<EObject>>(RuntimePackage.Literals.ECLASS_TO_EOBJECT_MAP_ENTRY, EClassToEObjectMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP);
		}
		return eClassToInstanceEObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<org.eclipse.uml2.uml.Class>> getEObjectsToUMLClassesItIsInstanceOfMap() {
		if (eObjectsToUMLClassesItIsInstanceOfMap == null) {
			eObjectsToUMLClassesItIsInstanceOfMap = new EcoreEMap<EObject,EList<org.eclipse.uml2.uml.Class>>(RuntimePackage.Literals.EOBJECT_TO_UML_CLASS_MAP_ENTRY, EObjectToUMLClassMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP);
		}
		return eObjectsToUMLClassesItIsInstanceOfMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Lifeline, EObject> getStaticLifelineToEObjectBindings() {
		if (staticLifelineToEObjectBindings == null) {
			staticLifelineToEObjectBindings = new EcoreEMap<Lifeline,EObject>(RuntimePackage.Literals.LIFELINE_TO_EOBJECT_MAP_ENTRY, LifelineToEObjectMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS);
		}
		return staticLifelineToEObjectBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Property, EObject> getStaticRoleToEObjectBindings() {
		if (staticRoleToEObjectBindings == null) {
			staticRoleToEObjectBindings = new EcoreEMap<Property,EObject>(RuntimePackage.Literals.PROPERTY_TO_EOBJECT_MAP_ENTRY, PropertyToEObjectMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS);
		}
		return staticRoleToEObjectBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleToEObjectMappingContainer getRolesToEObjectsMappingContainer() {
		return rolesToEObjectsMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRolesToEObjectsMappingContainer(RoleToEObjectMappingContainer newRolesToEObjectsMappingContainer, NotificationChain msgs) {
		RoleToEObjectMappingContainer oldRolesToEObjectsMappingContainer = rolesToEObjectsMappingContainer;
		rolesToEObjectsMappingContainer = newRolesToEObjectsMappingContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER, oldRolesToEObjectsMappingContainer, newRolesToEObjectsMappingContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRolesToEObjectsMappingContainer(RoleToEObjectMappingContainer newRolesToEObjectsMappingContainer) {
		if (newRolesToEObjectsMappingContainer != rolesToEObjectsMappingContainer) {
			NotificationChain msgs = null;
			if (rolesToEObjectsMappingContainer != null)
				msgs = ((InternalEObject)rolesToEObjectsMappingContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER, null, msgs);
			if (newRolesToEObjectsMappingContainer != null)
				msgs = ((InternalEObject)newRolesToEObjectsMappingContainer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER, null, msgs);
			msgs = basicSetRolesToEObjectsMappingContainer(newRolesToEObjectsMappingContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER, newRolesToEObjectsMappingContainer, newRolesToEObjectsMappingContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventContainer getMessageEventContainer() {
		return messageEventContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMessageEventContainer(MessageEventContainer newMessageEventContainer, NotificationChain msgs) {
		MessageEventContainer oldMessageEventContainer = messageEventContainer;
		messageEventContainer = newMessageEventContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER, oldMessageEventContainer, newMessageEventContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageEventContainer(MessageEventContainer newMessageEventContainer) {
		if (newMessageEventContainer != messageEventContainer) {
			NotificationChain msgs = null;
			if (messageEventContainer != null)
				msgs = ((InternalEObject)messageEventContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER, null, msgs);
			if (newMessageEventContainer != null)
				msgs = ((InternalEObject)newMessageEventContainer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER, null, msgs);
			msgs = basicSetMessageEventContainer(newMessageEventContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER, newMessageEventContainer, newMessageEventContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<MessageEventKeyWrapper, MessageEvent> getMessageEventKeyWrapperToMessageEventMap() {
		if (messageEventKeyWrapperToMessageEventMap == null) {
			messageEventKeyWrapperToMessageEventMap = new EcoreEMap<MessageEventKeyWrapper,MessageEvent>(RuntimePackage.Literals.MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP_ENTRY, MessageEventKeyWrapperToMessageEventMapEntryImpl.class, this, RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP);
		}
		return messageEventKeyWrapperToMessageEventMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean lifelineMayBindToEObject(Lifeline lifeline, EObject eObject) {

		// if there is a static binding defined, check if the eObject is the
		// specificied object
		EObject eObjectForStaticBinding = getStaticLifelineToEObjectBindings().get(lifeline);
		if (eObjectForStaticBinding != null) {
			return eObjectForStaticBinding == eObject;
		} else {// otherwise (no static binding) check if the eObject is an
				// instance of the type typing the lifeline's role (property)
			boolean lifelineMayBindToEObject = false;

			org.eclipse.uml2.uml.Class umlClass = (org.eclipse.uml2.uml.Class) ((Property) lifeline
					.getRepresents()).getType();

			if (getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap()
					.get(eObject.eClass()).contains(umlClass)) {
				lifelineMayBindToEObject = true;
			} else {
				for (EClass superClass : eObject.eClass().getEAllSuperTypes()) {
					if (getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap()
							.get(superClass).contains(umlClass)) {
						lifelineMayBindToEObject = true;
						break;
					}
				}
			}

			return lifelineMayBindToEObject;
		}

	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean roleMayBindToEObject(Property role, EObject eObject) {
		// if there is a static binding defined, check if the eObject is the
		// specified object
		EObject eObjectForStaticBinding = getStaticRoleToEObjectBindings().get(role);
		if (eObjectForStaticBinding != null) {
			return eObjectForStaticBinding == eObject;
		} else {// otherwise (no static binding) check if the eObject is an
				// instance of the type typing the role (property)
			org.eclipse.uml2.uml.Class umlClass = (org.eclipse.uml2.uml.Class) role
					.getType();
			return umlClassMatchesEObject(umlClass, eObject);
		}
	}

	private boolean umlClassMatchesEObject(org.eclipse.uml2.uml.Class umlClass,
			EObject eObject) {
		if (getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap()
				.get(eObject.eClass()).contains(umlClass)) {
			return true;
		} else {
			for (EClass superClass : eObject.eClass().getEAllSuperTypes()) {
				if (getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap()
						.get(superClass).contains(umlClass)) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(EList<EObject> objectSystemRootObjects, RoleToEObjectMappingContainer rolesToEObjectsMappingContainer, RuntimeUtil runtimeUtil) {
		getRootObjects().addAll(objectSystemRootObjects);
		setRuntimeUtil(runtimeUtil);
		
		// set the StaticLifelineToEObjectBindings if static role-to-eObject
		// mappings are provided.
		if (rolesToEObjectsMappingContainer != null) {
			setRolesToEObjectsMappingContainer(EcoreUtil.copy(rolesToEObjectsMappingContainer));
			for (Lifeline lifeline : getRuntimeUtil().getLifelineToInteractionFragmentsMap()
					.keySet()) {
				Property role = (Property) lifeline.getRepresents();
				// add entries to the StaticLifelineToEObjectBindings map there
				// are role-to-eObject mappings set.
				for (RoleToEObjectMapping roleToEObjectMapping : getRolesToEObjectsMappingContainer()
						.getRoleToEObjects()) {
					if (roleToEObjectMapping.getRole().equals(role)) {
						logger.debug("Lifeline "
								+ lifeline.getRepresents().getName()
								+ " of MSD "
								+ lifeline.getInteraction().getName()
								+ " is statically bound to object "
								+ roleToEObjectMapping.getEObject());
							getStaticLifelineToEObjectBindings().put(
										lifeline,
										roleToEObjectMapping.getEObject());
							getStaticRoleToEObjectBindings()
									.put(role, roleToEObjectMapping.getEObject());
					}
				}
			}
		}

		init();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init() {
		
		
		setMessageEventContainer(RuntimeFactory.eINSTANCE.createMessageEventContainer());


		// init simulation environment/system class maps
		// init simulation environment/system objects maps
		for (EObject objectSystemRootObject : getRootObjects()) {
			initSimulationModelObjectClassMaps(objectSystemRootObject);
			for (Iterator<EObject> simulationModelObjectsIterator = objectSystemRootObject
					.eAllContents(); simulationModelObjectsIterator.hasNext();) {
				EObject simulationModelObject = simulationModelObjectsIterator
						.next();
				initSimulationModelObjectClassMaps(simulationModelObject);
			}
		}

		
		
		// create
		// InitializingEnvironmentMSDModalMessageEvents and
		// InitializingSystemMSDModalMessageEvents lists

		EList<MessageEvent> initEnvList = new BasicEList<MessageEvent>();
		for (Interaction interaction : getRuntimeUtil().getInteractions()) {

			Assert.isTrue(getRuntimeUtil().getUseCaseSpecificationsToBeConsidered().isEmpty()
					|| getRuntimeUtil().getUseCaseSpecificationsToBeConsidered().contains(interaction
							.eContainer().eContainer()));

			Message firstMessage = RuntimeUtil.Helper
					.getFirstMessageForMSD(interaction);

			// consider only first message sent by the Environment
			EOperation operation = getRuntimeUtil().getOperation(firstMessage);
			EStructuralFeature eStructuralFeature = getRuntimeUtil().getEStructuralFeature(firstMessage);
			org.eclipse.uml2.uml.Class sendingUmlClass = (org.eclipse.uml2.uml.Class) Helper
					.getTypeOfSendingLifeline(firstMessage);
			org.eclipse.uml2.uml.Class receivingUmlClass = (org.eclipse.uml2.uml.Class) Helper
					.getTypeOfReceivingLifeline(firstMessage);

			Lifeline sendingLifeline = Helper.getSendingLifeline(firstMessage);
			Lifeline receivingLifeline = Helper
					.getReceivingLifeline(firstMessage);

			EClass sendingEClass = getRuntimeUtil().getUml2ecoreMapping()
					.getUmlClassToEClassMap().get(sendingUmlClass);
			EClass receivingEClass = getRuntimeUtil().getUml2ecoreMapping()
					.getUmlClassToEClassMap().get(receivingUmlClass);

			logger.debug("firstMessage:      " + firstMessage);
			logger.debug("sendingEClass:     " + sendingEClass);
			logger.debug("receivingEClass:   " + receivingEClass);
			logger.debug("sendingObjects:    "
					+ getEClassToInstanceEObjectMap().get(sendingEClass));
			logger.debug("receivingObjects:  "
					+ getEClassToInstanceEObjectMap().get(receivingEClass));

			for (EObject sendingObject : getEClassToInstanceEObjectMap().get(
					sendingEClass)) {
				// we must also consider this utility method in case there is a
				// static binding prescribed for the lifeline
				if (!lifelineMayBindToEObject(sendingLifeline, sendingObject))
					continue;
				for (EObject receivingObject : getEClassToInstanceEObjectMap()
						.get(receivingEClass)) {
					// we must also consider this utility method in case there
					// is a static binding prescribed for the lifeline
					if (!lifelineMayBindToEObject(receivingLifeline,
							receivingObject))
						continue;

					MessageEvent synchMessageEvent = getMessageEvent(operation,
									eStructuralFeature, sendingObject,
									receivingObject, getRuntimeUtil().getParameterValue(firstMessage));
					if (!initEnvList.contains(synchMessageEvent)) {
						initEnvList.add(synchMessageEvent);
						// creating a new Condensated Event
						MSDModalMessageEvent msdModalMessageEvent = RuntimeFactory.eINSTANCE
								.createMSDModalMessageEvent();
						MSDModality assumptionsMSDModality = RuntimeFactory.eINSTANCE
								.createMSDModality();
						MSDModality requirementsMSDModality = RuntimeFactory.eINSTANCE
								.createMSDModality();
						msdModalMessageEvent
								.setAssumptionsModality(assumptionsMSDModality);
						msdModalMessageEvent
								.setRequirementsModality(requirementsMSDModality);

						// identify if the MSD modal message event is
						// assumption- or requirement-initializing
						if (RuntimeUtil.Helper
								.isEnvironmentAssumption(interaction)) {
							assumptionsMSDModality.setInitializing(true);
							getAssumptionMSDInitializingMessageEvents().add(synchMessageEvent);
						} else {
							requirementsMSDModality.setInitializing(true);
							getRequirementMSDInitializingMessageEvents().add(synchMessageEvent);
						}
						// link the condensatedEvent with the corresponding
						// MessageEvent
						msdModalMessageEvent
								.setRepresentedMessageEvent(synchMessageEvent);

						// put it in the list
						// InitializingEnvironmentMSDModalMessageEvents or
						// InitializingSystemMSDModalMessageEvents
						if (RuntimeUtil.Helper
								.isEnvironmentMessage(firstMessage)) {
							getInitializingEnvironmentMSDModalMessageEvents()
									.add(msdModalMessageEvent);
						} else {
							getInitializingSystemMSDModalMessageEvents().add(
									msdModalMessageEvent);
						}
					}

				}
			}
		}

		for (StateMachine stateMachine : getRuntimeUtil().getStateMachines()) {
			Collection<Transition> firstTransitions = RuntimeUtil.Helper
					.getFirstTransitionsForMSS(stateMachine,getRuntimeUtil());
			for(Transition transition:firstTransitions){

			// consider only first message sent by the Environment
			EOperation operation = getRuntimeUtil().getOperation(transition);
			EStructuralFeature eStructuralFeature = getRuntimeUtil().getEStructuralFeature(transition);
			org.eclipse.uml2.uml.Class sendingUmlClass = (org.eclipse.uml2.uml.Class) Helper.getSender(transition).getType();
			org.eclipse.uml2.uml.Class receivingUmlClass = (org.eclipse.uml2.uml.Class) Helper.getReceiver(transition).getType();

			EClass sendingEClass = getRuntimeUtil().getUml2ecoreMapping()
					.getUmlClassToEClassMap().get(sendingUmlClass);
			EClass receivingEClass = getRuntimeUtil().getUml2ecoreMapping()
					.getUmlClassToEClassMap().get(receivingUmlClass);

			logger.debug("firstTransition:      " + transition);
			logger.debug("sendingEClass:     " + sendingEClass);
			logger.debug("receivingEClass:   " + receivingEClass);
			logger.debug("sendingObjects:    "
					+ getEClassToInstanceEObjectMap().get(sendingEClass));
			logger.debug("receivingObjects:  "
					+ getEClassToInstanceEObjectMap().get(receivingEClass));

			for (EObject sendingObject : getEClassToInstanceEObjectMap().get(
					sendingEClass)) {
				// we must also consider this utility method in case there is a
				// static binding prescribed for the lifeline
				if (!roleMayBindToEObject(Helper.getSender(transition), sendingObject))
					continue;
				for (EObject receivingObject : getEClassToInstanceEObjectMap()
						.get(receivingEClass)) {
					// we must also consider this utility method in case there
					// is a static binding prescribed for the lifeline
					if (!roleMayBindToEObject(Helper.getReceiver(transition),
							receivingObject))
						continue;

					MessageEvent messageEvent = getMessageEvent(operation,
									eStructuralFeature, sendingObject,
									receivingObject, null);
					if (!initEnvList.contains(messageEvent)) {
						initEnvList.add(messageEvent);
						// creating a new Condensated Event
						MSDModalMessageEvent msdModalMessageEvent = RuntimeFactory.eINSTANCE
								.createMSDModalMessageEvent();
						MSDModality assumptionsMSDModality = RuntimeFactory.eINSTANCE
								.createMSDModality();
						MSDModality requirementsMSDModality = RuntimeFactory.eINSTANCE
								.createMSDModality();
						msdModalMessageEvent
								.setAssumptionsModality(assumptionsMSDModality);
						msdModalMessageEvent
								.setRequirementsModality(requirementsMSDModality);

						// identify if the MSD modal message event is
						// assumption- or requirement-initializing
						if (RuntimeUtil.Helper.isEnvironmentAssumption(stateMachine)) {
							assumptionsMSDModality.setInitializing(true);
							getAssumptionMSDInitializingMessageEvents().add(messageEvent);
						} else {
							requirementsMSDModality.setInitializing(true);
							getRequirementMSDInitializingMessageEvents().add(messageEvent);
						}
						// link the condensatedEvent with the corresponding
						// MessageEvent
						msdModalMessageEvent
								.setRepresentedMessageEvent(messageEvent);

						// put it in the list
						// InitializingEnvironmentMSDModalMessageEvents or
						// InitializingSystemMSDModalMessageEvents
						if (RuntimeUtil.Helper
								.isEnvironmentTransition(transition)) {
							getInitializingEnvironmentMSDModalMessageEvents()
									.add(msdModalMessageEvent);
						} else {
							getInitializingSystemMSDModalMessageEvents().add(
									msdModalMessageEvent);
						}
					}
				}
				}
			}
		}

		
		for (Interaction interaction : getRuntimeUtil().getInteractions()) {
			
			for (Message message : interaction.getMessages()) {
				// map objects to operations of messages that it can receive

				for (EObject objectSystemRootObject : getRootObjects()) {
					for (Iterator<EObject> simulationModelObjectsIterator = objectSystemRootObject
							.eAllContents(); simulationModelObjectsIterator
							.hasNext();) {						
						
						EObject eObject = simulationModelObjectsIterator.next();
						addEObjectToOperationMaps(eObject, message);
					}
					addEObjectToOperationMaps(objectSystemRootObject, message);
				}

			}
		}

	}


	protected void addEObjectToOperationMaps(EObject eObject, Message message) {
		Lifeline sendingLifeline = ((MessageOccurrenceSpecification) message
				.getSendEvent()).getCovereds().get(0);
		Lifeline receivingLifeline = ((MessageOccurrenceSpecification) message
				.getReceiveEvent()).getCovereds().get(0);

		EOperation messagesEOperation = getRuntimeUtil().getUml2ecoreMapping()
				.getUmlOperationToEOperationMap().get(
						(Operation) message.getSignature());
		if (lifelineMayBindToEObject(sendingLifeline, eObject)) {
			if (getEObjectToSendableEOperationMap().get(eObject) == null) {
				getEObjectToSendableEOperationMap().put(eObject,
						new BasicEList<EOperation>());
			}
			getEObjectToSendableEOperationMap().get(eObject).add(
					messagesEOperation);
		}
		if (lifelineMayBindToEObject(receivingLifeline, eObject)) {
			if (getEObjectToReceivableEOperationMap().get(eObject) == null) {
				getEObjectToReceivableEOperationMap().put(eObject,
						new BasicEList<EOperation>());
			}
			getEObjectToReceivableEOperationMap().get(eObject).add(
					messagesEOperation);
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue) {
		return  getMessageEvent(eOperation, eStructuralFeature, sendingObject, receivingObject, parameterValue, getRuntimeUtil().createMessageEvent(sendingObject, receivingObject));
	}

	private MessageEvent nonrecursiveGetMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue, MessageEvent emptyMessageEventInstance) {
		Assert.isTrue(containsEObject(sendingObject)
				&& containsEObject(receivingObject));
		
		MessageEventKeyWrapper synchronousMessageEventKeyWrapper = new MessageEventKeyWrapper(sendingObject, eOperation, receivingObject, parameterValue, emptyMessageEventInstance.eClass());
		
		MessageEvent messageEvent = getMessageEventKeyWrapperToMessageEventMap().get(synchronousMessageEventKeyWrapper);
		
		if (messageEvent == null){
			messageEvent = emptyMessageEventInstance;
			RuntimeMessage runtimeMessage = EventsFactory.eINSTANCE.createRuntimeMessage();
			messageEvent.setRuntimeMessage(runtimeMessage);
			
			
			//set containment links			
			getMessageEventContainer().getRuntimeMessages().add(runtimeMessage);
			getMessageEventContainer().getMessageEvents().add(messageEvent);
			
			runtimeMessage.setOperation(eOperation);
			runtimeMessage.setSendingObject(sendingObject);
			if (eStructuralFeature != null)
				runtimeMessage.setSideEffectOnEStructuralFeature(eStructuralFeature);
			runtimeMessage.setReceivingObject(receivingObject);
			if (parameterValue != null){
				if (EventsUtil.isBooleanType(eOperation.getEParameters().get(0).getEType()))
					runtimeMessage.setBooleanParameterValue((Boolean)parameterValue);
				else if (EventsUtil.isIntegerType(eOperation.getEParameters().get(0).getEType()))
					runtimeMessage.setIntegerParameterValue((Integer)parameterValue);
				else if (EventsUtil.isStringType(eOperation.getEParameters().get(0).getEType()))
					runtimeMessage.setStringParameterValue((String)parameterValue);
				else 
					runtimeMessage.setEObjectParameterValue((EObject)parameterValue);
			}
			getMessageEventKeyWrapperToMessageEventMap().put(synchronousMessageEventKeyWrapper, messageEvent);
		}
		
			
		return messageEvent;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public MessageEvent getMessageEvent(EOperation eOperation, EStructuralFeature eStructuralFeature, EObject sendingObject, EObject receivingObject, Object parameterValue, MessageEvent emptyMessageEventInstance) {
		
		MessageEvent messageEvent = nonrecursiveGetMessageEvent(eOperation, eStructuralFeature, sendingObject, receivingObject, parameterValue, emptyMessageEventInstance);
		
		// if it it a concrete and parameterized event, immediately set (and if necessary create) the abstract message event
		if (parameterValue != null
				&& messageEvent.getRuntimeMessage().isConcrete()
				&& messageEvent.getAbstractMessageEvent() == null)
			messageEvent.setAbstractMessageEvent(nonrecursiveGetMessageEvent(
					eOperation,
					eStructuralFeature,
					messageEvent.getRuntimeMessage().getSendingObject(),
					messageEvent.getRuntimeMessage().getReceivingObject(),
					null,
					getRuntimeUtil().createMessageEvent(sendingObject,
							receivingObject)));
		else if (parameterValue == null
				&& !messageEvent.getRuntimeMessage().isConcrete()
				&& messageEvent.getConcreteMessageEvent().size() == 0
				&& eOperation.getEParameters().size() > 0
				&& EventsUtil.isBooleanType(eOperation.getEParameters().get(0)
						.getEType())) {
			messageEvent
					.getConcreteMessageEvent()
					.add(nonrecursiveGetMessageEvent(
							eOperation,
							eStructuralFeature,
							messageEvent.getRuntimeMessage().getSendingObject(),
							messageEvent.getRuntimeMessage()
									.getReceivingObject(),
							true,
							getRuntimeUtil().createMessageEvent(sendingObject,
									receivingObject)));
			messageEvent
					.getConcreteMessageEvent()
					.add(nonrecursiveGetMessageEvent(
							eOperation,
							eStructuralFeature,
							messageEvent.getRuntimeMessage().getSendingObject(),
							messageEvent.getRuntimeMessage()
									.getReceivingObject(),
							false,
							getRuntimeUtil().createMessageEvent(sendingObject,
									receivingObject)));
		}
			
		return messageEvent;
	}

	protected void initSimulationModelObjectClassMaps(
			EObject simulationModelObject) {
		// find all the UML classes to the object's eClass
		// (these are basically all UML classes from the UML model with the same
		// name)
		EList<org.eclipse.uml2.uml.Class> classList = new UniqueEList<org.eclipse.uml2.uml.Class>(
				getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap().get(
						simulationModelObject.eClass()));
		if (getEClassToInstanceEObjectMap().get(simulationModelObject.eClass()) == null) {
			// create an entry in the EClassToInstanceEObjectMap for the eClass
			// of simulationModelObject
			getEClassToInstanceEObjectMap().put(simulationModelObject.eClass(),
					new BasicEList<EObject>());
		}
		// put the simulationModelObject in its corresponding eClass entry of
		// EClassToInstanceEObjectMap
		logger.debug("EClassToInstanceEObjectMap -- mapping "
				+ simulationModelObject.eClass());
		logger.debug("                                   to "
				+ simulationModelObject);
		getEClassToInstanceEObjectMap().get(simulationModelObject.eClass())
				.add(simulationModelObject);
		for (EClass eClass : simulationModelObject.eClass().getEAllSuperTypes()) {
			// furthermore, add all the UML classes that correspond to
			// supertypes of the object's EClass.
			classList.addAll(getRuntimeUtil().getUml2ecoreMapping().getEClassToUMLClassMap()
					.get(eClass));
			// initializing the eClass to instance eObject Map by creating new
			// entries of the corresponding superTypes Class of
			// simulationModelObject
			if (getEClassToInstanceEObjectMap().get(eClass) == null) {
				getEClassToInstanceEObjectMap().put(eClass,
						new BasicEList<EObject>());
			}
			logger.debug("EClassToInstanceEObjectMap -- mapping " + eClass);
			logger.debug("                                   to "
					+ simulationModelObject);
			getEClassToInstanceEObjectMap().get(eClass).add(
					simulationModelObject);
		}

		assert classList != null;
		boolean isEnvironmentClass = false;
		for (org.eclipse.uml2.uml.Class umlClass : classList) {
			if (getRuntimeUtil().getEnvironmentClasses().contains(umlClass))
				isEnvironmentClass = true;
		}
		if (isEnvironmentClass) {
			getUncontrollableObjects().add(
					simulationModelObject);
		} else {
			getControllableObjects().add(
					simulationModelObject);
		}

		if (getEObjectsToUMLClassesItIsInstanceOfMap().get(
				simulationModelObject) == null) {
			getEObjectsToUMLClassesItIsInstanceOfMap().put(
					simulationModelObject,
					new UniqueEList<org.eclipse.uml2.uml.Class>());
		}
		// put all UML classes in the EObjectsToUMLClassesItIsInstanceOfMap
		getEObjectsToUMLClassesItIsInstanceOfMap().get(simulationModelObject)
				.addAll(classList);

		for (org.eclipse.uml2.uml.Class class1 : classList) {
			EList<org.eclipse.uml2.uml.Class> superClasses = class1
					.getSuperClasses();
			getEObjectsToUMLClassesItIsInstanceOfMap().get(
					simulationModelObject).addAll(superClasses);
		}

	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
				return ((InternalEList<?>)getInitializingEnvironmentMSDModalMessageEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
				return ((InternalEList<?>)getInitializingSystemMSDModalMessageEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
				return ((InternalEList<?>)getEObjectToSendableEOperationMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
				return ((InternalEList<?>)getEObjectToReceivableEOperationMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
				return ((InternalEList<?>)getEClassToInstanceEObjectMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
				return ((InternalEList<?>)getEObjectsToUMLClassesItIsInstanceOfMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
				return ((InternalEList<?>)getStaticLifelineToEObjectBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
				return ((InternalEList<?>)getStaticRoleToEObjectBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
				return basicSetRolesToEObjectsMappingContainer(null, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
				return basicSetMessageEventContainer(null, msgs);
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				return ((InternalEList<?>)getMessageEventKeyWrapperToMessageEventMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL:
				if (resolve) return getRuntimeUtil();
				return basicGetRuntimeUtil();
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
				return getInitializingEnvironmentMSDModalMessageEvents();
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
				return getInitializingSystemMSDModalMessageEvents();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS:
				return getAssumptionMSDInitializingMessageEvents();
			case RuntimePackage.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS:
				return getRequirementMSDInitializingMessageEvents();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
				if (coreType) return getEObjectToSendableEOperationMap();
				else return getEObjectToSendableEOperationMap().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
				if (coreType) return getEObjectToReceivableEOperationMap();
				else return getEObjectToReceivableEOperationMap().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
				if (coreType) return getEClassToInstanceEObjectMap();
				else return getEClassToInstanceEObjectMap().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
				if (coreType) return getEObjectsToUMLClassesItIsInstanceOfMap();
				else return getEObjectsToUMLClassesItIsInstanceOfMap().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
				if (coreType) return getStaticLifelineToEObjectBindings();
				else return getStaticLifelineToEObjectBindings().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
				if (coreType) return getStaticRoleToEObjectBindings();
				else return getStaticRoleToEObjectBindings().map();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
				return getRolesToEObjectsMappingContainer();
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
				return getMessageEventContainer();
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				if (coreType) return getMessageEventKeyWrapperToMessageEventMap();
				else return getMessageEventKeyWrapperToMessageEventMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
				getInitializingEnvironmentMSDModalMessageEvents().clear();
				getInitializingEnvironmentMSDModalMessageEvents().addAll((Collection<? extends MSDModalMessageEvent>)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
				getInitializingSystemMSDModalMessageEvents().clear();
				getInitializingSystemMSDModalMessageEvents().addAll((Collection<? extends MSDModalMessageEvent>)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS:
				getAssumptionMSDInitializingMessageEvents().clear();
				getAssumptionMSDInitializingMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS:
				getRequirementMSDInitializingMessageEvents().clear();
				getRequirementMSDInitializingMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
				((EStructuralFeature.Setting)getEObjectToSendableEOperationMap()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
				((EStructuralFeature.Setting)getEObjectToReceivableEOperationMap()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
				((EStructuralFeature.Setting)getEClassToInstanceEObjectMap()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
				((EStructuralFeature.Setting)getEObjectsToUMLClassesItIsInstanceOfMap()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
				((EStructuralFeature.Setting)getStaticLifelineToEObjectBindings()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
				((EStructuralFeature.Setting)getStaticRoleToEObjectBindings()).set(newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
				setRolesToEObjectsMappingContainer((RoleToEObjectMappingContainer)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
				setMessageEventContainer((MessageEventContainer)newValue);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				((EStructuralFeature.Setting)getMessageEventKeyWrapperToMessageEventMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)null);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
				getInitializingEnvironmentMSDModalMessageEvents().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
				getInitializingSystemMSDModalMessageEvents().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS:
				getAssumptionMSDInitializingMessageEvents().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS:
				getRequirementMSDInitializingMessageEvents().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
				getEObjectToSendableEOperationMap().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
				getEObjectToReceivableEOperationMap().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
				getEClassToInstanceEObjectMap().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
				getEObjectsToUMLClassesItIsInstanceOfMap().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
				getStaticLifelineToEObjectBindings().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
				getStaticRoleToEObjectBindings().clear();
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
				setRolesToEObjectsMappingContainer((RoleToEObjectMappingContainer)null);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
				setMessageEventContainer((MessageEventContainer)null);
				return;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				getMessageEventKeyWrapperToMessageEventMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_OBJECT_SYSTEM__RUNTIME_UTIL:
				return runtimeUtil != null;
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_ENVIRONMENT_MSD_MODAL_MESSAGE_EVENTS:
				return initializingEnvironmentMSDModalMessageEvents != null && !initializingEnvironmentMSDModalMessageEvents.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__INITIALIZING_SYSTEM_MSD_MODAL_MESSAGE_EVENTS:
				return initializingSystemMSDModalMessageEvents != null && !initializingSystemMSDModalMessageEvents.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ASSUMPTION_MSD_INITIALIZING_MESSAGE_EVENTS:
				return assumptionMSDInitializingMessageEvents != null && !assumptionMSDInitializingMessageEvents.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__REQUIREMENT_MSD_INITIALIZING_MESSAGE_EVENTS:
				return requirementMSDInitializingMessageEvents != null && !requirementMSDInitializingMessageEvents.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_SENDABLE_EOPERATION_MAP:
				return eObjectToSendableEOperationMap != null && !eObjectToSendableEOperationMap.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECT_TO_RECEIVABLE_EOPERATION_MAP:
				return eObjectToReceivableEOperationMap != null && !eObjectToReceivableEOperationMap.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ECLASS_TO_INSTANCE_EOBJECT_MAP:
				return eClassToInstanceEObjectMap != null && !eClassToInstanceEObjectMap.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__EOBJECTS_TO_UML_CLASSES_IT_IS_INSTANCE_OF_MAP:
				return eObjectsToUMLClassesItIsInstanceOfMap != null && !eObjectsToUMLClassesItIsInstanceOfMap.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_LIFELINE_TO_EOBJECT_BINDINGS:
				return staticLifelineToEObjectBindings != null && !staticLifelineToEObjectBindings.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__STATIC_ROLE_TO_EOBJECT_BINDINGS:
				return staticRoleToEObjectBindings != null && !staticRoleToEObjectBindings.isEmpty();
			case RuntimePackage.MSD_OBJECT_SYSTEM__ROLES_TO_EOBJECTS_MAPPING_CONTAINER:
				return rolesToEObjectsMappingContainer != null;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_CONTAINER:
				return messageEventContainer != null;
			case RuntimePackage.MSD_OBJECT_SYSTEM__MESSAGE_EVENT_KEY_WRAPPER_TO_MESSAGE_EVENT_MAP:
				return messageEventKeyWrapperToMessageEventMap != null && !messageEventKeyWrapperToMessageEventMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	
} //MSDObjectSystemImpl
