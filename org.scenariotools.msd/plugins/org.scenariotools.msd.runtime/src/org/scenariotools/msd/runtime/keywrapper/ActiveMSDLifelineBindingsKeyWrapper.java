package org.scenariotools.msd.runtime.keywrapper;

import java.util.HashSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;

public class ActiveMSDLifelineBindingsKeyWrapper extends KeyWrapper {
	public ActiveMSDLifelineBindingsKeyWrapper(
			ActiveMSDLifelineBindings activeMSDLifelineBindings) {
		this(activeMSDLifelineBindings.getLifelineToEObjectMap().keySet()
				.iterator().next().getInteraction(), activeMSDLifelineBindings
				.getLifelineToEObjectMap(), activeMSDLifelineBindings
				.getIgnoredLifelines());
	}

	public ActiveMSDLifelineBindingsKeyWrapper(Interaction interaction,
			EMap<Lifeline, EObject> lifelineToEObjectMap,
			EList<Lifeline> ignoredLifelines) {
		addSubObject(interaction);
		addSubObject(lifelineToEObjectMap);
		addSubObject(new HashSet<Lifeline>(ignoredLifelines));
	}
}
