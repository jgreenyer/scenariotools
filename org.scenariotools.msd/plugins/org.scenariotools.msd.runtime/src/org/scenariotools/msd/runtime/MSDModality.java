/**
 */
package org.scenariotools.msd.runtime;

import org.scenariotools.runtime.Modality;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Modality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModality#isMonitored <em>Monitored</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModality#isHot <em>Hot</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModality#isCold <em>Cold</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModality#isInitializing <em>Initializing</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDModality#isColdViolating <em>Cold Violating</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality()
 * @model
 * @generated
 */
public interface MSDModality extends Modality {
	/**
	 * Returns the value of the '<em><b>Monitored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monitored</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitored</em>' attribute.
	 * @see #setMonitored(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality_Monitored()
	 * @model
	 * @generated
	 */
	boolean isMonitored();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModality#isMonitored <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitored</em>' attribute.
	 * @see #isMonitored()
	 * @generated
	 */
	void setMonitored(boolean value);

	/**
	 * Returns the value of the '<em><b>Hot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hot</em>' attribute.
	 * @see #setHot(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality_Hot()
	 * @model
	 * @generated
	 */
	boolean isHot();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModality#isHot <em>Hot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hot</em>' attribute.
	 * @see #isHot()
	 * @generated
	 */
	void setHot(boolean value);

	/**
	 * Returns the value of the '<em><b>Cold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cold</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cold</em>' attribute.
	 * @see #setCold(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality_Cold()
	 * @model
	 * @generated
	 */
	boolean isCold();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModality#isCold <em>Cold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cold</em>' attribute.
	 * @see #isCold()
	 * @generated
	 */
	void setCold(boolean value);

	/**
	 * Returns the value of the '<em><b>Initializing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initializing</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initializing</em>' attribute.
	 * @see #setInitializing(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality_Initializing()
	 * @model
	 * @generated
	 */
	boolean isInitializing();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModality#isInitializing <em>Initializing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initializing</em>' attribute.
	 * @see #isInitializing()
	 * @generated
	 */
	void setInitializing(boolean value);

	/**
	 * Returns the value of the '<em><b>Cold Violating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cold Violating</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cold Violating</em>' attribute.
	 * @see #setColdViolating(boolean)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDModality_ColdViolating()
	 * @model
	 * @generated
	 */
	boolean isColdViolating();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDModality#isColdViolating <em>Cold Violating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cold Violating</em>' attribute.
	 * @see #isColdViolating()
	 * @generated
	 */
	void setColdViolating(boolean value);

} // MSDModality
