/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer <em>Element Container</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap <em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeStateGraph()
 * @model
 * @generated
 */
public interface MSDRuntimeStateGraph extends RuntimeStateGraph {
	/**
	 * Returns the value of the '<em><b>Element Container</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Container</em>' containment reference.
	 * @see #setElementContainer(ElementContainer)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeStateGraph_ElementContainer()
	 * @see org.scenariotools.msd.runtime.ElementContainer#getMsdRuntimeStateGraph
	 * @model opposite="msdRuntimeStateGraph" containment="true"
	 * @generated
	 */
	ElementContainer getElementContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getElementContainer <em>Element Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Container</em>' containment reference.
	 * @see #getElementContainer()
	 * @generated
	 */
	void setElementContainer(ElementContainer value);

	/**
	 * Returns the value of the '<em><b>Runtime Util</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Util</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Util</em>' reference.
	 * @see #setRuntimeUtil(RuntimeUtil)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeStateGraph_RuntimeUtil()
	 * @see org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph
	 * @model opposite="msdRuntimeStateGraph"
	 * @generated
	 */
	RuntimeUtil getRuntimeUtil();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Util</em>' reference.
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	void setRuntimeUtil(RuntimeUtil value);

	/**
	 * Returns the value of the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario Run Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #setScenarioRunConfiguration(ScenarioRunConfiguration)
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeStateGraph_ScenarioRunConfiguration()
	 * @model
	 * @generated
	 */
	ScenarioRunConfiguration getScenarioRunConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	void setScenarioRunConfiguration(ScenarioRunConfiguration value);

	/**
	 * Returns the value of the '<em><b>Msd Runtime State Key Wrapper To MSD Runtime State Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper},
	 * and the value is of type {@link org.scenariotools.msd.runtime.MSDRuntimeState},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Runtime State Key Wrapper To MSD Runtime State Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getMSDRuntimeStateGraph_MsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
	 * @model mapType="org.scenariotools.msd.runtime.MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry<org.scenariotools.msd.runtime.MSDRuntimeStateKeyWrapper, org.scenariotools.msd.runtime.MSDRuntimeState>" transient="true"
	 * @generated
	 */
	EMap<MSDRuntimeStateKeyWrapper, MSDRuntimeState> getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * initializes the MSDRuntimeStateGraph and returns the start MSDRuntimeState.
	 * (Leads to the creation of an ElementContainer and the creation+initialization of a RuntimeUtil)
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	MSDRuntimeState init(ScenarioRunConfiguration scenarioRunConfiguration);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unchangedActiveMSDsMany="true"
	 * @generated
	 */
	MSDRuntimeState getMSDRuntimeState(MSDRuntimeState msdRuntimeState, EList<ActiveProcess> unchangedActiveMSDs);

} // MSDRuntimeStateGraph
