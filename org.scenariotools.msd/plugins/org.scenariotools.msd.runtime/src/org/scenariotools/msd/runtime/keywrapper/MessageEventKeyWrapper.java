package org.scenariotools.msd.runtime.keywrapper;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;

public class MessageEventKeyWrapper extends KeyWrapper {
	public MessageEventKeyWrapper(EObject sendingObject,
			EOperation eOperation, EObject receivingObject,
			Object parameterValue, EClass _class) {
		addSubObject(sendingObject);
		addSubObject(eOperation);
		addSubObject(receivingObject);
		addSubObject(parameterValue);
		addSubObject(_class);
	}
}
