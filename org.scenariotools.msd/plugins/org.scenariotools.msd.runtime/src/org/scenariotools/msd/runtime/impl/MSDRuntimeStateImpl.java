/**
 */
package org.scenariotools.msd.runtime.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.ValueSpecification;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.SynchronousMessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.ActiveMSDProgress;
import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.impl.RuntimeStateImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MSD Runtime State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl#getActiveProcesses <em>Active Processes</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl#getRuntimeUtil <em>Runtime Util</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.MSDRuntimeStateImpl#getActiveMSDChangedDuringPerformStep <em>Active MSD Changed During Perform Step</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDRuntimeStateImpl extends RuntimeStateImpl implements
		MSDRuntimeState {

	/**
	 * The cached value of the '{@link #getActiveProcesses() <em>Active Processes</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getActiveProcesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> activeProcesses;

	private static Logger logger = Activator.getLogManager().getLogger(
			MSDRuntimeStateImpl.class.getName());

	/**
	 * The cached value of the '{@link #getRuntimeUtil() <em>Runtime Util</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuntimeUtil()
	 * @generated
	 * @ordered
	 */
	protected RuntimeUtil runtimeUtil;

	/**
	 * The cached value of the '{@link #getActiveMSDChangedDuringPerformStep()
	 * <em>Active MSD Changed During Perform Step</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActiveMSDChangedDuringPerformStep()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveProcess> activeMSDChangedDuringPerformStep;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDRuntimeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MSD_RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getActiveProcesses() {
		if (activeProcesses == null) {
			activeProcesses = new EObjectResolvingEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES);
		}
		return activeProcesses;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil getRuntimeUtil() {
		if (runtimeUtil != null && runtimeUtil.eIsProxy()) {
			InternalEObject oldRuntimeUtil = (InternalEObject)runtimeUtil;
			runtimeUtil = (RuntimeUtil)eResolveProxy(oldRuntimeUtil);
			if (runtimeUtil != oldRuntimeUtil) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
			}
		}
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil basicGetRuntimeUtil() {
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeUtil(RuntimeUtil newRuntimeUtil) {
		RuntimeUtil oldRuntimeUtil = runtimeUtil;
		runtimeUtil = newRuntimeUtil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL, oldRuntimeUtil, runtimeUtil));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveProcess> getActiveMSDChangedDuringPerformStep() {
		if (activeMSDChangedDuringPerformStep == null) {
			activeMSDChangedDuringPerformStep = new EObjectResolvingEList<ActiveProcess>(ActiveProcess.class, this, RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP);
		}
		return activeMSDChangedDuringPerformStep;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void performStep(Event event) {
		if (event instanceof MessageEvent) {
			MessageEvent messageEvent = (MessageEvent) event;
			if (logger.isDebugEnabled()) {
				logger.debug("performStep called with messageEvent: "
						+ RuntimeUtil.Helper
								.getMessageEventString(messageEvent));
			}

			// in this cases it makes no sense to progress at all because the
			// system
			// or the environment has lost for sure.
			if (isSafetyViolationOccurredInAssumptions()
					|| isSafetyViolationOccurredInRequirements()
					&& !hasActiveAssumptionProcesses()) { // this may be a bit
															// too
															// optimistic
//				removeAllProcesses();
				return;
			}

			// boolean messageEventHadSideEffect =
			handleSideEffectOnObjectSystem(messageEvent);

			performStepOnActiveProcesses(messageEvent);

			// if (messageEventHadSideEffect) // then the object system may have
			// changed
			getActiveMSDChangedDuringPerformStep().addAll(getActiveProcesses());
		}
	}

	protected void removeAllProcesses() {
		Iterator<ActiveProcess> processIt = getActiveProcesses().iterator();
		while (processIt.hasNext())
			disposeState(processIt.next(), processIt);
	}

	protected boolean hasActiveAssumptionProcesses() {
		for (ActiveProcess activeProcess : getActiveProcesses()) {
			if (RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess))
				return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> The method first iterates over all active MSDs
	 * and adds (or refreshes the information for) the MSDModalMessageEvents for
	 * the enabled message events. Second, it iterates over all active MSDs
	 * again and adds (or refreshes the information for) the
	 * MSDModalMessageEvents for the violating message events.
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void updateMSDModalMessageEvents(
			ExecutionSemantics executionSemantics) {
		getMessageEventToModalMessageEventMap().clear();

		for (ActiveProcess activeMSD : getActiveProcesses()) {
			// if (getActiveMSDChangedDuringPerformStep().contains(activeMSD)){
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeMSD);
			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
					|| !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;
			activeMSD.calculateRelevantEvents();

			// }
		}

		// terminology:
		// - a message event is "active" if it can be unified with an enabled
		// executed message
		// - the system is "active" if there is an active system message in a
		// requirement MSD.
		// - an event is "relevant" if it leads to a state change.
		// We consider three execution semantics:
		// 1. playout: if the system is active, we consider only active system
		// messages, otherwise we consider all relevant environment events
		// 2. playOutButWaitingPossible: if the system is active, we consider
		// only active system messages AND all relevant environment events,
		// otherwise we consider all relevant environment events.
		// 3. alwaysConsiderAllEvents: we consider all relevant events

		boolean isEnvironmentStep = true;
		boolean playOut = executionSemantics == ExecutionSemantics.PLAY_OUT;
		boolean playOutButWaitingPossible = executionSemantics == ExecutionSemantics.PLAY_OUT_BUT_WAITING_POSSIBLE;
		boolean alwaysConsiderAllEvents = executionSemantics == ExecutionSemantics.ALWAYS_ALL_EVENTS;

		if (playOut || playOutButWaitingPossible) { // first determine if there
													// are active system events
													// in the requirement MSDs
			outerloop: for (ActiveProcess activeProcess : getActiveProcesses()) {
				if (RuntimeUtil.Helper
						.isEnvironmentAssumptionProcess(activeProcess))
					continue;
				for (Map.Entry<NamedElement, MessageEvent> enabledMessageEventsEntry : activeProcess
						.getEnabledEvents().entrySet()) {
					boolean isEnvironment = false;
					boolean isExecuted = false;
					if (activeProcess instanceof ActiveMSD) {
						isEnvironment = ((ActiveMSD) activeProcess)
								.getMsdUtil().getEnvironmentMessages()
								.contains(enabledMessageEventsEntry.getKey());
						isExecuted = ((ActiveMSD) activeProcess).getMsdUtil()
								.getExecutedMessages()
								.contains(enabledMessageEventsEntry.getKey());
					} else if (activeProcess instanceof ActiveMSS) {

						isEnvironment = RuntimeUtil.Helper
								.isEnvironmentTransition((Transition) enabledMessageEventsEntry
										.getKey());
						isExecuted = RuntimeUtil.Helper
								.isExecuted((State) ((Transition) enabledMessageEventsEntry
										.getKey()).getSource());
					}
					if (!isEnvironment // is a system message/transition
							&& isExecuted // is executed
					) {
						isEnvironmentStep = false;
						break outerloop;
					}
				}
			}
		}

		// add initializing system modal message events
		if (alwaysConsiderAllEvents) {
			for (MSDModalMessageEvent initializingMSDModalMessageEvent : ((MSDObjectSystem) getObjectSystem())
					.getInitializingSystemMSDModalMessageEvents()) {

				addInitializingModalMessageEvent(initializingMSDModalMessageEvent);

			}
		}

		// add initializing environment modal message events
		if (alwaysConsiderAllEvents || playOutButWaitingPossible
				|| (playOut && isEnvironmentStep)) {
			for (MSDModalMessageEvent initializingMSDModalMessageEvent : ((MSDObjectSystem) getObjectSystem())
					.getInitializingEnvironmentMSDModalMessageEvents()) {

				addInitializingModalMessageEvent(initializingMSDModalMessageEvent);

			}
		}

		// add/update enabled events
		for (ActiveProcess activeProcess : getActiveProcesses()) {

			boolean interactionIsEnvironmentAssumption = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			if (logger.isDebugEnabled()) {
				logger.debug("active Process: " + activeProcess);
				logger.debug("is environment assumption? "
						+ interactionIsEnvironmentAssumption);
			}

			for (Map.Entry<NamedElement, MessageEvent> enabledMessageEventsEntry : activeProcess
					.getEnabledEvents().entrySet()) {
				if (enabledMessageEventsEntry.getValue() == null) {
					logger.debug("enabled event for "
							+ enabledMessageEventsEntry.getKey() + " is null!");
					continue;
				}

				boolean messageIsHot = false;
				boolean messageIsExecuted = false;
				if (activeProcess instanceof ActiveMSD) {
					messageIsHot = ((ActiveMSD) activeProcess).getMsdUtil()
							.getHotMessages()
							.contains(enabledMessageEventsEntry.getKey());
					messageIsExecuted = ((ActiveMSD) activeProcess)
							.getMsdUtil().getExecutedMessages()
							.contains(enabledMessageEventsEntry.getKey());
				}
				if (activeProcess instanceof ActiveMSS) {
					State currentState = ((ActiveMSS) activeProcess)
							.getCurrentState().getUmlState();
					messageIsHot = RuntimeUtil.Helper.isHot(currentState);
					messageIsExecuted = RuntimeUtil.Helper
							.isExecuted(currentState);
				}

				boolean isEnvironmentMessageEvent = isEnvironmentMessageEvent(enabledMessageEventsEntry
						.getValue());
				MessageEvent messageEvent = enabledMessageEventsEntry
						.getValue();

				if (logger.isDebugEnabled()) {
					logger.debug("enabled event:    "
							+ RuntimeUtil.Helper
									.getMessageEventString(messageEvent));
					logger.debug("message hot?      " + messageIsHot);
					logger.debug("message executed? " + messageIsExecuted);
					logger.debug("is env event?     "
							+ isEnvironmentMessageEvent);
				}

				if (alwaysConsiderAllEvents
						|| (playOutButWaitingPossible && isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& !isEnvironmentStep && !isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& isEnvironmentStep && isEnvironmentMessageEvent)) {
					addOrUpdateMSDModalMessageEventForEnabledMessageEvent(
							messageEvent, messageIsHot, messageIsExecuted,
							interactionIsEnvironmentAssumption, activeProcess);
					if (logger.isDebugEnabled()) {
						logger.debug(RuntimeUtil.Helper
								.getMSDModalMessageEventString((MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
										.get(messageEvent)));
					}
				}
			}

		}

		// add/update violating events
		for (ActiveProcess activeProcess : getActiveProcesses()) {
			boolean interactionIsEnvironmentAssumption = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			if (logger.isDebugEnabled()) {
				logger.debug("active MSD: " + activeProcess);
				logger.debug("is environment assumption? "
						+ interactionIsEnvironmentAssumption);
			}

			for (MessageEvent coldForbiddenmessageEvent : activeProcess
					.getColdForbiddenEvents().values()) {

				boolean isEnvironmentMessageEvent = isEnvironmentMessageEvent(coldForbiddenmessageEvent);

				if (logger.isDebugEnabled()) {
					logger.debug("cold forbidden event: "
							+ RuntimeUtil.Helper
									.getMessageEventString(coldForbiddenmessageEvent));
					logger.debug("is env event?    "
							+ isEnvironmentMessageEvent);
				}

				if (alwaysConsiderAllEvents
						|| (playOutButWaitingPossible && isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& !isEnvironmentStep && !isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& isEnvironmentStep && isEnvironmentMessageEvent)) {
					addOrUpdateCondensatedMessageEventForViolatingMessageEvent(
							coldForbiddenmessageEvent, false,
							interactionIsEnvironmentAssumption, activeProcess,
							true);
				}
			}

			for (MessageEvent violatingMessageEvent : activeProcess
					.getViolatingEvents().values()) {

				boolean isEnvironmentMessageEvent = isEnvironmentMessageEvent(violatingMessageEvent);

				if (logger.isDebugEnabled()) {
					logger.debug("violating event: "
							+ RuntimeUtil.Helper
									.getMessageEventString(violatingMessageEvent));
					logger.debug("cut is hot?      "
							+ activeProcess.getCurrentState().isHot());
					logger.debug("is env event?    "
							+ isEnvironmentMessageEvent);
				}

				if (alwaysConsiderAllEvents
						|| (playOutButWaitingPossible && isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& !isEnvironmentStep && !isEnvironmentMessageEvent)
						|| ((playOut || playOutButWaitingPossible)
								&& isEnvironmentStep && isEnvironmentMessageEvent)) {
					addOrUpdateCondensatedMessageEventForViolatingMessageEvent(
							violatingMessageEvent, activeProcess
									.getCurrentState().isHot(),
							interactionIsEnvironmentAssumption, activeProcess,
							true);
				}
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Relevant message events in the current state: ");
			for (MessageEvent messageEvent : getMessageEventToModalMessageEventMap()
					.keySet()) {
				logger.debug(RuntimeUtil.Helper
						.getMSDModalMessageEventString((MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
								.get(messageEvent)));
			}
		}

		// if in play-out or playOutButWaitingPossible mode and it is the
		// system's turn, remove the following message events:
		// (because they are not considered "active":)
		// 1. all non-parameterized system message events that are not
		// req.-executed.
		// 2. all non-concrete parameterized message events that are not
		// req.-executed.
		// 3. all concrete parameterized message events that are not executed if
		// their parent (non-concrete) message event is not req.-executed.
		if ((playOut || playOutButWaitingPossible) && !isEnvironmentStep) {
			Iterator<Entry<MessageEvent, ModalMessageEvent>> messageEventToModalMessageEventMapEntryIterator = getMessageEventToModalMessageEventMap()
					.entrySet().iterator();
			while (messageEventToModalMessageEventMapEntryIterator.hasNext()) {
				Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry = messageEventToModalMessageEventMapEntryIterator
						.next();
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) messageEventToModalMessageEventMapEntry
						.getValue();
				MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
						.getKey();
//				if (!isEnvironmentMessageEvent(msdModalMessageEvent
//						.getRepresentedMessageEvent())) {
					if (!messageEvent.isParameterized()) {
						if (!msdModalMessageEvent.getRequirementsModality()
								.isMandatory())
							messageEventToModalMessageEventMapEntryIterator
									.remove();
					} else { // message is parameterized
						if (!messageEvent.isConcrete()
								&& !msdModalMessageEvent
										.getRequirementsModality()
										.isMandatory()) {
							// msdModalMessageEvent.getConcreteMSDModalMessageEvent().clear();
							messageEventToModalMessageEventMapEntryIterator
									.remove();
						} else if (messageEvent.isConcrete()
								&& !msdModalMessageEvent
										.getRequirementsModality()
										.isMandatory()
								&& !msdModalMessageEvent
										.getParentSymbolicMSDModalMessageEvent()
										.getRequirementsModality()
										.isMandatory()) {
							// msdModalMessageEvent.setParentSymbolicMSDModalMessageEvent(null);
							messageEventToModalMessageEventMapEntryIterator
									.remove();
						}
					}
//				}
			}
		}

		// remove all environment message events that are non-spontaneous
		// environment reactions and that are not assumption-executed
		Iterator<Entry<MessageEvent, ModalMessageEvent>> messageEventToModalMessageEventMapEntryIterator = getMessageEventToModalMessageEventMap()
				.entrySet().iterator();
		while (messageEventToModalMessageEventMapEntryIterator.hasNext()) {
			Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry = messageEventToModalMessageEventMapEntryIterator
					.next();
			MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) messageEventToModalMessageEventMapEntry
					.getValue();
			MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
					.getKey();
			if (!msdModalMessageEvent.getAssumptionsModality().isMandatory()
					&& getRuntimeUtil()
							.isNonSpontaneousEnvironmentMessageEvent(
									messageEvent))
				messageEventToModalMessageEventMapEntryIterator.remove();
		}

		// // if in play-out or playOutButWaitingPossible mode and it is the
		// system's turn, remove
		// // all system messages if they are non-active.
		// if ((playOut || playOutButWaitingPossible) && !isEnvironmentStep){
		// Iterator<Entry<MessageEvent, ModalMessageEvent>>
		// messageEventToModalMessageEventMapEntryIterator =
		// getMessageEventToModalMessageEventMap().entrySet().iterator();
		// while(messageEventToModalMessageEventMapEntryIterator.hasNext()){
		// Entry<MessageEvent, ModalMessageEvent>
		// messageEventToModalMessageEventMapEntry =
		// messageEventToModalMessageEventMapEntryIterator.next();
		// MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent)
		// messageEventToModalMessageEventMapEntry.getValue();
		// //MessageEvent messageEvent =
		// messageEventToModalMessageEventMapEntry.getKey();
		// if
		// (!getRuntimeUtil().isEnvironmentMessageEvent(messageEventToModalMessageEventMapEntry.getKey())
		// && !msdModalMessageEvent.getRequirementsModality().isMandatory())
		// messageEventToModalMessageEventMapEntryIterator.remove();
		// }
		// }

		// set status of concrete instances of messages also for abstract
		// message

		for (MessageEvent messageEvent : new BasicEList<MessageEvent>(
				getMessageEventToModalMessageEventMap().keySet())) {
			updateAbstractMessageEventForConcreteEvent(messageEvent);
		}

		if (logger.isDebugEnabled()) {
			assertEachMessageEventRepresentedByExactlyOneMSDModalMessageEvent();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isInHotEnvironmentCut() {
		for (ActiveProcess proc : getActiveProcesses())
			if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(proc)
					&& proc.isInHotCut())
				return true;
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isInHotSystemCut() {
		for (ActiveProcess proc : getActiveProcesses())
			if (!RuntimeUtil.Helper.isEnvironmentAssumptionProcess(proc)
					&& proc.isInHotCut())
				return true;
		return false;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isInExecutedEnvironmentCut() {
		for (ActiveProcess proc : getActiveProcesses())
			if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(proc)
					&& proc.isInExecutedCut())
				return true;
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isInExecutedSystemCut() {
		for (ActiveProcess proc : getActiveProcesses())
			if (!RuntimeUtil.Helper.isEnvironmentAssumptionProcess(proc)
					&& proc.isInExecutedCut())
				return true;
		return false;
	}

	private void addInitializingModalMessageEvent(
			MSDModalMessageEvent initializingMSDModalMessageEvent) {
		assertMessageEventIsValidWithRespectToObjectSystem(initializingMSDModalMessageEvent
				.getRepresentedMessageEvent());

		// we need a copy, because we don't want to change the properties of the
		// initializing MSD Modal Message Events
		MSDModalMessageEvent copiedInitializingMSDModalMessageEvent = EcoreUtil
				.copy(initializingMSDModalMessageEvent);
		getMessageEventToModalMessageEventMap()
				.put(copiedInitializingMSDModalMessageEvent
						.getRepresentedMessageEvent(),
						copiedInitializingMSDModalMessageEvent);

		assertMessageEventIsValidWithRespectToObjectSystem(copiedInitializingMSDModalMessageEvent
				.getRepresentedMessageEvent());

		MessageEvent inializingMessageEvent = copiedInitializingMSDModalMessageEvent
				.getRepresentedMessageEvent();
		if (inializingMessageEvent.isParameterized()
				&& inializingMessageEvent.isConcrete()) {
			MessageEvent abstractMessageEvent = ((MSDObjectSystem) getObjectSystem())
					.getMessageEvent(inializingMessageEvent.getOperation(),
							inializingMessageEvent
									.getSideEffectOnEStructuralFeature(),
							inializingMessageEvent.getSendingObject(),
							inializingMessageEvent.getReceivingObject(), null);
			MSDModalMessageEvent parentMSDModalMessageEvent = createNewMSDModalMessageEvent(abstractMessageEvent);
			copiedInitializingMSDModalMessageEvent
					.setParentSymbolicMSDModalMessageEvent(parentMSDModalMessageEvent);
		}
	}

	/**
	 * @generated NOT
	 */
	public boolean isEnvironmentMessageEvent(MessageEvent messageEvent) {
		return ((MSDObjectSystem) getObjectSystem())
				.isEnvironmentMessageEvent(messageEvent);
	}

	private void assertEachMessageEventRepresentedByExactlyOneMSDModalMessageEvent() {
		EList<MSDModalMessageEvent> msdModalMessageEvents = new UniqueEList<MSDModalMessageEvent>();
		for (MessageEvent me : getMessageEventToModalMessageEventMap().keySet()) {
			MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
					.get(me);
			msdModalMessageEvents.add(msdModalMessageEvent);
			if (msdModalMessageEvent.getParentSymbolicMSDModalMessageEvent() != null) {
				Assert.isTrue(getMessageEventToModalMessageEventMap().get(
						msdModalMessageEvent.getRepresentedMessageEvent()) == msdModalMessageEvent);
				msdModalMessageEvents.add(msdModalMessageEvent
						.getParentSymbolicMSDModalMessageEvent());
			}
		}
		logger.debug("messageEvents:         "
				+ getMessageEventToModalMessageEventMap().keySet());
		logger.debug("msdModalMessageEvents: " + msdModalMessageEvents);
		Assert.isTrue(getMessageEventToModalMessageEventMap().keySet().size() == msdModalMessageEvents
				.size());
	}

	protected void assertMessageEventIsValidWithRespectToObjectSystem(
			MessageEvent me) {
		MessageEvent me2 = ((MSDObjectSystem) getObjectSystem())
				.getMessageEvent(me.getOperation(),
						me.getSideEffectOnEStructuralFeature(),
						me.getSendingObject(), me.getReceivingObject(),
						RuntimeUtil.Helper.getParameterValue(me));
		Assert.isTrue(me == me2);
	}

	protected void updateAbstractMessageEventForConcreteEvent(
			MessageEvent concreteEvent) {
		MessageEvent abstractEvent = concreteEvent.getAbstractMessageEvent();
		if (abstractEvent != null) {
			MSDModalMessageEvent concreteModalMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
					.get(concreteEvent);
			MSDModalMessageEvent abstractModalMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
					.get(abstractEvent);
			if (abstractModalMessageEvent == null)
				abstractModalMessageEvent = createNewMSDModalMessageEvent(abstractEvent);
			for (ActiveProcess violatingMSD : concreteModalMessageEvent
					.getSafetyViolatingInActiveProcess())
				if (!abstractModalMessageEvent
						.getSafetyViolatingInActiveProcess().contains(
								violatingMSD))
					abstractModalMessageEvent
							.getSafetyViolatingInActiveProcess().add(
									violatingMSD);
			for (ActiveProcess violatingMSD : concreteModalMessageEvent
					.getColdViolatingInActiveProcess())
				if (!abstractModalMessageEvent
						.getColdViolatingInActiveProcess().contains(
								violatingMSD))
					abstractModalMessageEvent.getColdViolatingInActiveProcess()
							.add(violatingMSD);
			updateAbstractModality(
					(MSDModality) concreteModalMessageEvent
							.getAssumptionsModality(),
					(MSDModality) abstractModalMessageEvent
							.getAssumptionsModality());
			updateAbstractModality(
					(MSDModality) concreteModalMessageEvent
							.getRequirementsModality(),
					(MSDModality) abstractModalMessageEvent
							.getRequirementsModality());
		}
	}

	private void updateAbstractModality(MSDModality concreteModality,
			MSDModality abstractModality) {
		if (concreteModality.isSafetyViolating())
			abstractModality.setSafetyViolating(true);
		if (concreteModality.isColdViolating())
			abstractModality.setColdViolating(true);
		if (concreteModality.isMandatory())
			abstractModality.setMandatory(true);
		if (concreteModality.isMonitored())
			abstractModality.setMonitored(true);
		if (concreteModality.isCold())
			abstractModality.setCold(true);
		if (concreteModality.isHot())
			abstractModality.setHot(true);
		if (concreteModality.isInitializing())
			abstractModality.setInitializing(true);
	}

	/**
	 * adds (or refreshes the information for) the MSDModalMessageEvents for the
	 * enabled message events
	 * 
	 * if an MSDModalMessageEvent for messageEvent does not exist, one is
	 * created (see {@link #createNewMSDModalMessageEvent(MessageEvent)})
	 * 
	 * @param messageEvent
	 * @param isMessageHotInCut
	 * @param isMessageExecutedInCut
	 * @param isEnvironmentAssumption
	 */
	protected void addOrUpdateMSDModalMessageEventForEnabledMessageEvent(
			MessageEvent messageEvent, boolean isMessageHotInCut,
			boolean isMessageExecutedInCut, boolean isEnvironmentAssumption,
			ActiveProcess activeMSDWhereMessageEventIsEnabled) {
		MSDModalMessageEvent msdModalMessageEventForMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
				.get(messageEvent);
		if (msdModalMessageEventForMessageEvent == null)
			msdModalMessageEventForMessageEvent = createNewMSDModalMessageEvent(messageEvent);

		msdModalMessageEventForMessageEvent.getEnabledInActiveProcess().add(
				activeMSDWhereMessageEventIsEnabled);

		updateBasicMSDModalMessageEventAttributesForEnabledMessageEvents(
				msdModalMessageEventForMessageEvent, isMessageHotInCut,
				isMessageExecutedInCut, isEnvironmentAssumption,
				activeMSDWhereMessageEventIsEnabled);
	}

	/**
	 * adds (or refreshes the information for) the MSDModalMessageEvents for the
	 * violating message events.
	 * 
	 * @param messageEvent
	 * @param hot
	 * @param environmentAssumption
	 */
	protected void addOrUpdateCondensatedMessageEventForViolatingMessageEvent(
			MessageEvent messageEvent, boolean isCutHot,
			boolean isEnvironmentAssumption,
			ActiveProcess activeMSDWhereMessageEventIsViolating,
			boolean updateConcreteInstances) {
		MSDModalMessageEvent msdModalMessageEventForMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
				.get(messageEvent);
		if (msdModalMessageEventForMessageEvent == null)
			msdModalMessageEventForMessageEvent = createNewMSDModalMessageEvent(messageEvent);

		updateBasicMSDModalMessageEventAttributesForViolatingMessageEvents(
				msdModalMessageEventForMessageEvent, isCutHot,
				isEnvironmentAssumption, activeMSDWhereMessageEventIsViolating);

		// if abstract message is violating so are all concrete instances
		if (!messageEvent.isConcrete() && updateConcreteInstances) {
			for (MessageEvent concreteMessageEvent : messageEvent
					.getConcreteMessageEvent()) {
				addOrUpdateCondensatedMessageEventForViolatingMessageEvent(
						concreteMessageEvent, isCutHot,
						isEnvironmentAssumption,
						activeMSDWhereMessageEventIsViolating, false);
			}
		}
	}

	/**
	 * updates the basic attributes of an MSDModalMessageEvent representing a
	 * message even that is enabled in an assumption/requirement MSD in a
	 * hot/cold cut.
	 * 
	 * isEnvironmentAssumption && isHot => setAssumptionHot(true)
	 * isEnvironmentAssumption && !isHot => setAssumptionCold(true)
	 * !isEnvironmentAssumption && isHot => setRequirementHot(true)
	 * !isEnvironmentAssumption && !isHot => setRequirementCold(true) and
	 * likewise with monitored
	 * 
	 * Moreover, there are three cases: (1) the enabled message is not
	 * parametrized. (2) the enabled message is parametrized and concrete, i.e.,
	 * a concrete parameter value is specified. (3) the enabled message is
	 * parametrized and symbolic, i.e., no concrete parameter value is
	 * specified.
	 * 
	 * In the these cases, we do the following: (1) the enabled message is not
	 * parametrized: a) update the MSDModalMessageEvent corresponding to the
	 * message event (update that it is cold/hot monitored/executed enabled)
	 * 
	 * (2) the enabled message is parametrized and concrete, i.e., a concrete
	 * parameter value is specified: a) update the MSDModalMessageEvent
	 * corresponding to the message event (update that it is cold/hot
	 * monitored/executed enabled) b) all attributes set for the (parent)
	 * symbolic message event will be set for the concrete (child) message
	 * events. c) all attributes set for the concrete (child) message events
	 * will be set for the (parent) symbolic message event. d) The (parent)
	 * symbolic message event is set to cold/safety violating (depending on the
	 * cut temperature, isMessageHotInCut) (because all other messages, with
	 * other parameter values, will be cold or safety violating) e) All other
	 * concrete instances of the symbolic events (except the current
	 * messageEvent) will be set to the same violating status. One exception are
	 * concrete instances of the symbolic events that come from the same active
	 * MSD.
	 * 
	 * (3) the enabled message is parametrized and symbolic, i.e., no concrete
	 * parameter value is specified. a) update the MSDModalMessageEvent
	 * corresponding to the message event (update that it is cold/hot
	 * monitored/executed enabled) b) so update all concrete instances
	 * 
	 * 
	 * @param msdModalMessageEvent
	 * @param isHot
	 * @param isEnvironmentAssumption
	 */
	private void updateBasicMSDModalMessageEventAttributesForEnabledMessageEvents(
			MSDModalMessageEvent msdModalMessageEvent,
			boolean isMessageHotInCut, boolean isMessageExecutedInCut,
			boolean isEnvironmentAssumption,
			ActiveProcess activeMSDWhereMessageEventIsEnabled) {

		MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent
				.getAssumptionsModality();
		MSDModality requirementsModality = (MSDModality) msdModalMessageEvent
				.getRequirementsModality();

		if (isEnvironmentAssumption) {
			if (isMessageHotInCut)
				assumptionsModality.setHot(true);
			else
				assumptionsModality.setCold(true);
			if (isMessageExecutedInCut)
				assumptionsModality.setMandatory(true);
			else
				assumptionsModality.setMonitored(true);

		} else {
			if (isMessageHotInCut)
				requirementsModality.setHot(true);
			else
				requirementsModality.setCold(true);
			if (isMessageExecutedInCut)
				requirementsModality.setMandatory(true);
			else
				requirementsModality.setMonitored(true);
		}

		// case (3)
		for (MSDModalMessageEvent concreteMSDModalMessageEvent : msdModalMessageEvent
				.getConcreteMSDModalMessageEvent()) {

			concreteMSDModalMessageEvent.getEnabledInActiveProcess().add(
					activeMSDWhereMessageEventIsEnabled);

			updateBasicMSDModalMessageEventAttributesForEnabledMessageEvents(
					concreteMSDModalMessageEvent, isMessageHotInCut,
					isMessageExecutedInCut, isEnvironmentAssumption,
					activeMSDWhereMessageEventIsEnabled); // this is a recursive
															// call, but
															// recursion depth
															// will always be <=
															// 1.
		}

		MSDModalMessageEvent parentSymbolicMSDModalMessageEvent = msdModalMessageEvent
				.getParentSymbolicMSDModalMessageEvent();
		if (parentSymbolicMSDModalMessageEvent != null) {
			// (2)b)
			if (msdModalMessageEvent.getRepresentedMessageEvent()
					.isParameterized()
					&& msdModalMessageEvent.getRepresentedMessageEvent()
							.isConcrete()) {
				// if
				// (parentSymbolicMSDModalMessageEvent.getAssumptionsModality()
				// .isSafetyViolating())
				// assumptionsModality.setSafetyViolating(true);
				// if (((MSDModality) parentSymbolicMSDModalMessageEvent
				// .getAssumptionsModality()).isColdViolating())
				// assumptionsModality.setColdViolating(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getAssumptionsModality()).isInitializing())
					assumptionsModality.setInitializing(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getAssumptionsModality()).isCold())
					assumptionsModality.setCold(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getAssumptionsModality()).isHot())
					assumptionsModality.setHot(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getAssumptionsModality()).isMonitored())
					assumptionsModality.setMonitored(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getAssumptionsModality()).isMandatory())
					assumptionsModality.setMandatory(true);

				// if (parentSymbolicMSDModalMessageEvent
				// .getRequirementsModality().isSafetyViolating())
				// requirementsModality.setSafetyViolating(true);
				// if (((MSDModality) parentSymbolicMSDModalMessageEvent
				// .getRequirementsModality()).isColdViolating())
				// requirementsModality.setColdViolating(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getRequirementsModality()).isInitializing())
					requirementsModality.setInitializing(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getRequirementsModality()).isCold())
					requirementsModality.setCold(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getRequirementsModality()).isHot())
					requirementsModality.setHot(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getRequirementsModality()).isMonitored())
					requirementsModality.setMonitored(true);
				if (((MSDModality) parentSymbolicMSDModalMessageEvent
						.getRequirementsModality()).isMandatory())
					requirementsModality.setMandatory(true);
			}

			// (2)c)/d)
			if (isEnvironmentAssumption) {
				if (isMessageHotInCut) {
					// (2)c)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getAssumptionsModality()).setSafetyViolating(true);
					// (2)d)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getAssumptionsModality()).setHot(true);
				} else {
					// (2)c)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getAssumptionsModality()).setColdViolating(true);
					// (2)d)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getAssumptionsModality()).setCold(true);
				}
				// (2)d)
				if (isMessageExecutedInCut) {
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getAssumptionsModality()).setMandatory(true);
				}
			} else {
				if (isMessageHotInCut) {
					// (2)c)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getRequirementsModality())
							.setSafetyViolating(true);
					// (2)d)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getRequirementsModality()).setHot(true);
				} else {
					// (2)c)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getRequirementsModality()).setColdViolating(true);
					// (2)d)
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getRequirementsModality()).setCold(true);
				}
				// (2)d)
				if (isMessageExecutedInCut) {
					((MSDModality) parentSymbolicMSDModalMessageEvent
							.getRequirementsModality()).setMandatory(true);
				}
			}
			// (2)e)
			for (MSDModalMessageEvent concreteMSDModalMessageEvent : parentSymbolicMSDModalMessageEvent
					.getConcreteMSDModalMessageEvent()) {
				// System.out.println("(1) enabled modal message event:   " +
				// RuntimeUtil.Helper.getMSDModalMessageEventString(msdModalMessageEvent));
				// System.out.println("(2) sister modal message event:    " +
				// RuntimeUtil.Helper.getMSDModalMessageEventString(concreteMSDModalMessageEvent));
				//
				// System.out.println("(3) where is consid event enabled: " +
				// activeMSDWhereMessageEventIsEnabled);
				// System.out.println("(4) where is consid event enabled: " +
				// RuntimeUtil.Helper.getActiveProcessesString(msdModalMessageEvent.getEnabledInActiveProcess()));
				// System.out.println("(5) where is sister event enabled: " +
				// RuntimeUtil.Helper.getActiveProcessesString(concreteMSDModalMessageEvent.getEnabledInActiveProcess()));

				if (concreteMSDModalMessageEvent != msdModalMessageEvent
						// One exception are concrete instances of the symbolic
						// events that come from the same active MSD.
						&& !concreteMSDModalMessageEvent
								.getEnabledInActiveProcess().contains(
										activeMSDWhereMessageEventIsEnabled) && !concreteMSDModalMessageEvent.getParentSymbolicMSDModalMessageEvent()
										.getEnabledInActiveProcess().contains(
												activeMSDWhereMessageEventIsEnabled)) {

					// Set<ActiveProcess> sharedActiveProcessesInWhichEnabled =
					// new
					// HashSet<ActiveProcess>(msdModalMessageEvent.getEnabledInActiveProcess());
					// sharedActiveProcessesInWhichEnabled.retainAll(concreteMSDModalMessageEvent.getEnabledInActiveProcess());
					//
					// boolean concreteMessageEventEnabledInSharedActiveProcess
					// = !sharedActiveProcessesInWhichEnabled.isEmpty();
					//
					// if (!concreteMessageEventEnabledInSharedActiveProcess)
					updateBasicMSDModalMessageEventAttributesForViolatingMessageEvents(
							concreteMSDModalMessageEvent, isMessageHotInCut,
							isEnvironmentAssumption,
							activeMSDWhereMessageEventIsEnabled);
				}
			}
		}
	}

	/**
	 * updates the basic attributes of an MSDModalMessageEvent representing a
	 * message even that is violating an assumption/requirement MSD in a
	 * hot/cold cut.
	 * 
	 * isEnvironmentAssumption && isHot => setAssumptionSafetyViolating(true)
	 * isEnvironmentAssumption && !isHot => setAssumptionColdViolating(true)
	 * !isEnvironmentAssumption && isHot => setRequirementSafetyViolating(true)
	 * !isEnvironmentAssumption && !isHot => setRequirementColdViolating(true)
	 * 
	 * @param msdModalMessageEvent
	 * @param isHot
	 * @param isEnvironmentAssumption
	 */
	private void updateBasicMSDModalMessageEventAttributesForViolatingMessageEvents(
			MSDModalMessageEvent msdModalMessageEvent, boolean isHot,
			boolean isEnvironmentAssumption,
			ActiveProcess activeMSDWhereMessageEventIsViolating) {

		MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent
				.getAssumptionsModality();
		MSDModality requirementsModality = (MSDModality) msdModalMessageEvent
				.getRequirementsModality();

		boolean StoreAdditionalMSDModalMessageEventData = ((MSDRuntimeStateGraph) getStateGraph())
				.getScenarioRunConfiguration()
				.isStoreAdditionalMSDModalMessageEventData();
		// boolean StoreAdditionalMSDModalMessageEventData = true;

		if (isEnvironmentAssumption) {
			if (isHot) {
				assumptionsModality.setSafetyViolating(true);
				if (StoreAdditionalMSDModalMessageEventData)
					msdModalMessageEvent.getSafetyViolatingInActiveProcess()
							.add(activeMSDWhereMessageEventIsViolating);
			} else {
				assumptionsModality.setColdViolating(true);
				if (StoreAdditionalMSDModalMessageEventData)
					msdModalMessageEvent.getColdViolatingInActiveProcess().add(
							activeMSDWhereMessageEventIsViolating);
			}
		} else {
			if (isHot) {
				requirementsModality.setSafetyViolating(true);
				if (StoreAdditionalMSDModalMessageEventData)
					msdModalMessageEvent.getSafetyViolatingInActiveProcess()
							.add(activeMSDWhereMessageEventIsViolating);
			} else {
				requirementsModality.setColdViolating(true);
				if (StoreAdditionalMSDModalMessageEventData)
					msdModalMessageEvent.getColdViolatingInActiveProcess().add(
							activeMSDWhereMessageEventIsViolating);
			}
		}
	}

	/**
	 * Creates a new MSDModalMessageEvent for the #messageEvent.
	 * 
	 * If the message is parameterized and the message is concrete (i.e., the
	 * message event specifies a value for the parameter), then we want to set a
	 * reference to the MSDModalMessageEvent representing the abstract version
	 * of the message event (i.e., the one where no value is specified for the
	 * parameter). This MSDModalMessageEvent may need to be created also.
	 * 
	 * @param messageEvent
	 * @return
	 */
	private MSDModalMessageEvent createNewMSDModalMessageEvent(
			MessageEvent messageEvent) {
		MSDModalMessageEvent msdModalMessageEvent = RuntimeFactory.eINSTANCE
				.createMSDModalMessageEvent();
		msdModalMessageEvent.setAssumptionsModality(RuntimeFactory.eINSTANCE
				.createMSDModality());
		msdModalMessageEvent.setRequirementsModality(RuntimeFactory.eINSTANCE
				.createMSDModality());
		msdModalMessageEvent.setRepresentedMessageEvent(messageEvent);
		if (((MSDObjectSystem) getObjectSystem())
				.getAssumptionMSDInitializingMessageEvents().contains(
						messageEvent))
			((MSDModality) msdModalMessageEvent.getAssumptionsModality())
					.setInitializing(true);
		if (((MSDObjectSystem) getObjectSystem())
				.getRequirementMSDInitializingMessageEvents().contains(
						messageEvent))
			((MSDModality) msdModalMessageEvent.getRequirementsModality())
					.setInitializing(true);

		Assert.isTrue(getMessageEventToModalMessageEventMap().get(messageEvent) == null);
		getMessageEventToModalMessageEventMap().put(messageEvent,
				msdModalMessageEvent);
		if (messageEvent.isParameterized() && messageEvent.isConcrete()) {
			MessageEvent abstractMessageEvent = ((MSDObjectSystem) getObjectSystem())
					.getMessageEvent(messageEvent.getOperation(),
							messageEvent.getSideEffectOnEStructuralFeature(),
							messageEvent.getSendingObject(),
							messageEvent.getReceivingObject(), null);

			MSDModalMessageEvent abstractMSDModalMessageEvent = (MSDModalMessageEvent) getMessageEventToModalMessageEventMap()
					.get(abstractMessageEvent);
			if (abstractMSDModalMessageEvent == null)
				abstractMSDModalMessageEvent = createNewMSDModalMessageEvent(abstractMessageEvent); // this
																									// is
																									// a
																									// recursive
																									// call,
																									// but
																									// recursion
																									// depth
																									// will
																									// always
																									// be
																									// <=
																									// 1.

			msdModalMessageEvent
					.setParentSymbolicMSDModalMessageEvent(abstractMSDModalMessageEvent);
		}
		else {
			for(MessageEvent concreteEvent:messageEvent.getConcreteMessageEvent()){
				MSDModalMessageEvent concreteMSDModalMessageEvent=(MSDModalMessageEvent) getMessageEventToModalMessageEventMap().get(concreteEvent);
				if(concreteMSDModalMessageEvent==null){
					concreteMSDModalMessageEvent=createNewMSDModalMessageEvent(concreteEvent);
				}
				msdModalMessageEvent.getConcreteMSDModalMessageEvent().add(concreteMSDModalMessageEvent);
			}
		}
		return msdModalMessageEvent;
	}

	protected void handleViolations(MessageEvent messageEvent,
			Iterator<ActiveProcess> activeProcessesIterator) {
		logger.debug("1) Check if the event violates an active Process in the current cut: ");
		while (activeProcessesIterator.hasNext()) {
			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			// do not bother if it's an assumption MSD, but a safety violation
			// already occurred in the assumptions
			// or if it's a requirement MSD, but a safety violation already
			// occurred in the requirements
			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
					|| !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			if (activeProcess.getColdForbiddenEvents().containsValue(
					messageEvent)) {
				handleColdViolations(messageEvent, activeProcess,
						activeProcessesIterator);
				continue;
			}
			
			if (activeProcess.isSafetyViolating(messageEvent)) {
				if (isAssumptionMSD) {
					if (logger.isDebugEnabled()) {
						logger.debug("Safety violation occurred in assumption MSD "
								+ activeProcess);
					}
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("Safety violation occurred in requirements MSD "
								+ activeProcess);
					}
					setSafetyViolationOccurredInRequirements(true);
				}
				// we do not return (stop) already here, because we want to
				// check if the safety violation also occurred in other MSDs
				// (to see whether it is a violation in the assumptions or
				// requirements or both)
			}
			if (activeProcess.isColdViolating(messageEvent)) {
				handleColdViolations(messageEvent, activeProcess,
						activeProcessesIterator);
			}
		}

		removeViolatedProcesses();
	}

	protected void removeViolatedProcesses() {
		// if a safety violation occurred in the assumptions or requirements,
		// remove all the assumption resp. requirement MSDs.
		Iterator<ActiveProcess> activeProcessesIterator = getActiveProcesses()
				.iterator();
		while (activeProcessesIterator.hasNext()) {
			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);

			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements()){
				disposeState(activeProcess, activeProcessesIterator);
			}
		}
	}

	/**
	 * Performs the activeMSD-specific changes.
	 * 
	 * @param messageEvent
	 */
	protected void performStepOnActiveProcesses(MessageEvent messageEvent) {
		// 1) Check if the event violates an active Process in the current state
		// using an iterator to avoid a ConcurrentModificationException if
		// handleColdViolations removes a cut from
		// getCoreScenarioLogic().getCurrentState().getCuts().
		Iterator<ActiveProcess> activeProcessesIterator = getActiveProcesses()
				.iterator();
		handleViolations(messageEvent, activeProcessesIterator);

		Set<ActiveProcess> activeProcessesNeedingFurtherProcessing = new HashSet<ActiveProcess>();

		// 2) Progress cut according to the message event (if possible)
		logger.debug("2) Progress cut according to the message event (if possible)");
		activeProcessesIterator = getActiveProcesses().iterator();
		while (activeProcessesIterator.hasNext()) {

			ActiveProcess activeProcess = activeProcessesIterator.next();
			boolean isAssumptionMSD = RuntimeUtil.Helper
					.isEnvironmentAssumptionProcess(activeProcess);
			if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
					|| !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			if (activeProcess instanceof ActiveMSD) {
				// TODO Workaround. If the cut waits in front of a dynamic changing combinedFragment
				// because the first time it was false, we need to check it here in the next round. 
				// It may become true. the method progressEnabledHiddenEvents() checks for all hidden 
				// events, i think we need only to check for hidden events that can dynamically change.
				((ActiveMSD) activeProcess).progressEnabledHiddenEvents();
				if (progressActiveMSD((ActiveMSD) activeProcess, messageEvent,
						activeProcessesIterator)) {
					activeProcessesNeedingFurtherProcessing.add(activeProcess);
				}
			} else if (activeProcess instanceof ActiveMSS) {
				if (progressActiveMSS((ActiveMSS) activeProcess, messageEvent,
						activeProcessesIterator)) {
					activeProcessesNeedingFurtherProcessing.add(activeProcess);
				}
			}
		}

		// 3) Create a new active MSD for all MSDs where this event is
		// represented by the first message
		logger.debug("3a) Create a new active MSD for all MSDs where this event is represented by the first message.");
		activeProcessesNeedingFurtherProcessing
				.addAll(createActiveMSDs(messageEvent));
		logger.debug("3b) Create a new active MSS for all MSSs where this event is represented by the first transition.");

		activeProcessesNeedingFurtherProcessing
				.addAll(createActiveMSSs(messageEvent));

		performPostProcessingForStepOnActiveMSDs(activeProcessesNeedingFurtherProcessing);
	}

	protected void performPostProcessingForStepOnActiveMSDs(
			Set<ActiveProcess> activeProcessesNeedingFurtherProcessing) {
		EList<ActiveProcess> activeProcessesWhereRevelantEventsNeedRecalculation = new BasicEList<ActiveProcess>(
				activeProcessesNeedingFurtherProcessing);
		// 4) Progress hidden events and evaluate lifeline bindings
		logger.debug("4) Progress hidden events and evaluate lifeline bindings");
		while (!activeProcessesNeedingFurtherProcessing.isEmpty()) {
			ActiveProcess activeProcess = activeProcessesNeedingFurtherProcessing
					.iterator().next();
			boolean nextActiveProcessNeedsFurtherProcessing = false;

			// progress a hidden event. If this changed the cut (the method then
			// returns true), the cut needs to be considered in another
			// iteration of this loop.
			ActiveMSDProgress activeMSDProgress = activeProcess
					.progressEnabledHiddenEvents();
			if (activeMSDProgress == ActiveMSDProgress.PROGRESS) {
				nextActiveProcessNeedsFurtherProcessing = true;
			} else if (activeMSDProgress == ActiveMSDProgress.COLD_VIOLATION) {
				handleColdViolations(null, activeProcess, null);
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.SAFETY_VIOLATION) {
				if (RuntimeUtil.Helper
						.isEnvironmentAssumptionProcess(activeProcess)) {
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					setSafetyViolationOccurredInRequirements(true);
				}
				activeProcessesWhereRevelantEventsNeedRecalculation
						.remove(activeProcess);
			} else if (activeMSDProgress == ActiveMSDProgress.NO_PROGRESS) {
				// do nothing.
			} else if (activeMSDProgress == ActiveMSDProgress.TERMINAL_CUT_REACHED) {
				disposeState(activeProcess, null);
			}

			if (activeMSDProgress != ActiveMSDProgress.COLD_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.SAFETY_VIOLATION
					&& activeMSDProgress != ActiveMSDProgress.TERMINAL_CUT_REACHED) {
				if (activeProcess instanceof ActiveMSD) {
					// returns a list, because binding lifelines may lead to a
					// multiplication of cuts if multiple candidate objects for
					// a
					// lifeline binding exist.
					EList<ActiveMSD> activeMSDsWhereAdditionalLifelinesWereBound = evaluateNextLifelineBindingExpression((ActiveMSD) activeProcess);

					// if the returned list is empty...
					if (activeMSDsWhereAdditionalLifelinesWereBound == null
							|| activeMSDsWhereAdditionalLifelinesWereBound
									.isEmpty())
						nextActiveProcessNeedsFurtherProcessing = false;
					else {
						activeProcessesNeedingFurtherProcessing
								.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
						activeProcessesWhereRevelantEventsNeedRecalculation
								.addAll(activeMSDsWhereAdditionalLifelinesWereBound);
						nextActiveProcessNeedsFurtherProcessing = true;
					}
				} else if (activeProcess instanceof ActiveMSS) {
					EList<ActiveMSS> activeMSSsWhereAdditionalRolesWereBound = evaluateNextRoleBindingExpression((ActiveMSS) activeProcess);

					// if the returned list is empty...
					if (activeMSSsWhereAdditionalRolesWereBound == null
							|| activeMSSsWhereAdditionalRolesWereBound
									.isEmpty())
						nextActiveProcessNeedsFurtherProcessing = false;
					else {
						activeProcessesNeedingFurtherProcessing
								.addAll(activeMSSsWhereAdditionalRolesWereBound);
						activeProcessesWhereRevelantEventsNeedRecalculation
								.addAll(activeMSSsWhereAdditionalRolesWereBound);
						nextActiveProcessNeedsFurtherProcessing = true;
					}
				}
			}

			if (!nextActiveProcessNeedsFurtherProcessing)
				activeProcessesNeedingFurtherProcessing.remove(activeProcess);
		}

		// 5) for each cut update relevant events
		// ---> moved to updateMSDModalMessageEvents
		// logger.debug("5) for each cut update relevant events");
		// for (ActiveMSD activeMSD:
		// activeMSDsWhereRevelantEventsNeedRecalculation){
		// boolean isAssumptionMSD =
		// activeMSD.getMsdUtil().isEnvironmentAssumption();
		//
		// if (isAssumptionMSD && isSafetyViolationOccurredInAssumptions()
		// || !isAssumptionMSD && isSafetyViolationOccurredInRequirements())
		// continue;
		//
		// activeMSD.calculateRelevantEvents();
		// }

		getActiveMSDChangedDuringPerformStep().clear();
		getActiveMSDChangedDuringPerformStep().addAll(
				activeProcessesWhereRevelantEventsNeedRecalculation);

		removeViolatedProcesses();
	}

	protected EList<ActiveMSS> evaluateNextRoleBindingExpression(
			ActiveMSS activeMSS) {

		if (logger.isDebugEnabled()) {
			logger.debug("Attempting to evaluate a next role binding expression for active MSS: "
					+ activeMSS);
		}

		EList<Property> unboundRoleList = getUnboundRoles(activeMSS);

		if (unboundRoleList.isEmpty())
			return null; // nothing more to bind

		// a list of states that we return later
		EList<ActiveMSS> activeMSSsWhereAdditionalRolesWereBound = new UniqueEList<ActiveMSS>();

		// iterate over all the unbound roles...
		for (Property unboundRole : unboundRoleList) {

			// if there is a static binding prescribed for the roles, bind
			// the role accordingly
			EObject eObjectPrescribedForRole = activeMSS.getObjectSystem()
					.getStaticRoleToEObjectBindings().get(unboundRole);
			if (eObjectPrescribedForRole != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("There is a static binding prescribed for the role: "
							+ unboundRole);
					logger.debug("   --  binding to eObject: "
							+ eObjectPrescribedForRole);
				}
				activeMSS.getRoleBindings().getPropertyToEObjectMap()
						.put(unboundRole, eObjectPrescribedForRole);
				activeMSSsWhereAdditionalRolesWereBound.add(activeMSS);
			} else {

				// TODO: implement dynamic binding for roles
				throw new IllegalArgumentException();
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("States where additional roles were bound: "
					+ activeMSSsWhereAdditionalRolesWereBound);
		}

		return activeMSSsWhereAdditionalRolesWereBound;
	}

	protected void handleColdViolations(MessageEvent messageEvent,
			ActiveProcess activeMSD,
			Iterator<ActiveProcess> currentStateActiveMSDIteratorToRemoveFrom) {
		disposeState(activeMSD, currentStateActiveMSDIteratorToRemoveFrom);
	}

	protected void disposeState(ActiveProcess activeMSD,
			Iterator<ActiveProcess> currentStateActiveMSDIteratorToRemoveFrom) {
		if (currentStateActiveMSDIteratorToRemoveFrom == null) {
			boolean notAlreadyRemoved = getActiveProcesses().remove(activeMSD);
			if (!notAlreadyRemoved)
				logger.debug("The cut/mss-state was already removed from the current state.");
		} else {
			currentStateActiveMSDIteratorToRemoveFrom.remove();
			getRuntimeUtil().getOclRegistry().disposeOCLForActiveMSD(activeMSD);
		}
	}

	protected boolean handleSideEffectOnObjectSystem(MessageEvent messageEvent) {
		EStructuralFeature eStructuralFeature = messageEvent
				.getSideEffectOnEStructuralFeature();
		if (eStructuralFeature != null) {
			EObject receivingEObject = messageEvent.getReceivingObject();
			try {
				if (messageEvent.isSetBooleanParameterValue()) {
					receivingEObject.eSet(eStructuralFeature,
							messageEvent.isBooleanParameterValue());
					if (logger.isDebugEnabled()) {
						logger.debug("Changed feature "
								+ eStructuralFeature.getName()
								+ " to the value "
								+ messageEvent.isBooleanParameterValue());
					}
					return true;
				} else if (messageEvent.isSetIntegerParameterValue()) {
					receivingEObject.eSet(eStructuralFeature,
							messageEvent.getIntegerParameterValue());
					if (logger.isDebugEnabled()) {
						logger.debug("Changed feature "
								+ eStructuralFeature.getName()
								+ " to the value "
								+ messageEvent.getIntegerParameterValue());
					}
					return true;
				} else if (messageEvent.isSetStringParameterValue()) {
					receivingEObject.eSet(eStructuralFeature,
							messageEvent.getStringParameterValue());
					if (logger.isDebugEnabled()) {
						logger.debug("Changed feature "
								+ eStructuralFeature.getName()
								+ " to the value "
								+ messageEvent.getStringParameterValue());
					}
					return true;
				} else if (messageEvent.isSetEObjectParameterValue()) {
					receivingEObject.eSet(eStructuralFeature,
							messageEvent.getEObjectParameterValue());
					if (logger.isDebugEnabled()) {
						logger.debug("Changed feature "
								+ eStructuralFeature.getName()
								+ " to the value "
								+ messageEvent.getEObjectParameterValue());
					}
					return true;
				} else
					logger.error("If the message has a side-effect on a feature of a receiving object, a parameter value must be set!");
			} catch (IllegalArgumentException e) {
				logger.error("Exception trying to set an object feature based on a SET-Message: "
						+ e);
			}
		}
		return false;
	}

	protected boolean progressActiveMSD(ActiveMSD activeMSD,
			MessageEvent messageEvent,
			Iterator<ActiveProcess> currentStateActiveMSDIteratorToRemoveFrom) {
		if (logger.isDebugEnabled()) {
			logger.debug("Attempting to progress active MSD: " + activeMSD
					+ " with message event: " + messageEvent);
		}

		for (Map.Entry<NamedElement, MessageEvent> messageToEnabledMessageEventMapEntry : activeMSD
				.getEnabledEvents()) {
			Message message = (Message) messageToEnabledMessageEventMapEntry
					.getKey();
			if (RuntimeUtil.Helper.isParameterUnifiable(messageEvent,
					messageToEnabledMessageEventMapEntry.getValue(),
					activeMSD)) {

				// progress cut beyond the enabled message
				Lifeline sendingLifeline = RuntimeUtil.Helper
						.getSendingLifeline(message);

				Lifeline receivingLifeline = RuntimeUtil.Helper
						.getReceivingLifeline(message);

				activeMSD
						.getCurrentState()
						.progressCutLocationOnLifeline(
								sendingLifeline,
								RuntimeUtil.Helper
										.getSendingMessageOccurrenceSpecification(message));
				activeMSD
						.getCurrentState()
						.progressCutLocationOnLifeline(
								receivingLifeline,
								RuntimeUtil.Helper
										.getReceivingMessageOccurrenceSpecification(message));

				if (activeMSD.getCurrentState().terminalCutReached()) {
					if (logger.isDebugEnabled()) {
						logger.debug("Terminal cut of active MSD reached, disposing: "
								+ activeMSD);
					}
					disposeState(activeMSD,
							currentStateActiveMSDIteratorToRemoveFrom);
					return false;
				}
				String varName=null;
				Object argumentValue=null;
				Message diagramMessage=messageToEnabledMessageEventMapEntry.getValue().getMessage();
				if(diagramMessage!=null && diagramMessage.getArguments().size()!=0){
					ValueSpecification valueSpecification = message.getArguments().get(
							0);
					if (RuntimeUtil.Helper.isVariableReference(valueSpecification)) {
						varName = (String) RuntimeUtil.Helper
								.getReferencedVariable(valueSpecification);
						argumentValue = activeMSD.getVariableValue(varName);
					}
				}
				
				//check if argument is a variable, but has no value yet
				if (varName != null && argumentValue == null) {
					activeMSD.assignValueToVariable(varName,
							RuntimeUtil.Helper.getParameterValue(messageEvent));
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug("Cut was progressed: " + activeMSD);
				}

				return true; // the cut was progressed

			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("No progress of active MSD: " + activeMSD
					+ " with message event: " + messageEvent);
		}

		return false; // there was no effect
	}

	protected boolean progressActiveMSS(ActiveMSS activeMSS,
			MessageEvent messageEvent,
			Iterator<ActiveProcess> activeProcessesIteratorToRemoveFrom) {

		if (logger.isDebugEnabled()) {
			logger.debug("Attempting to progress active MSS: " + activeMSS
					+ " with message event: " + messageEvent);
		}

		for (Map.Entry<NamedElement, MessageEvent> transitionToEnabledMessageEventMapEntry : activeMSS
				.getEnabledEvents()) {
			if (RuntimeUtil.Helper.isParameterUnifiable(messageEvent,
					transitionToEnabledMessageEventMapEntry.getValue(), null)) {

				// change current state to target of the transition
				Transition transition = (Transition) transitionToEnabledMessageEventMapEntry
						.getKey();
				// System.out.println("changing uml state of "+activeMSS.getCurrentState()+" from "+activeMSS.getCurrentState().getUmlState()+" to "+transition.getTarget());
				activeMSS.getCurrentState().setUmlState(
						(State) transition.getTarget());

				if (activeMSS.getCurrentState().getUmlState() == getRuntimeUtil()
						.getStateMachineToInitialStateMap().get(
								activeMSS.getStateMachine())) {
					if (logger.isDebugEnabled()) {
						logger.debug("Initial state of MSS reached again, disposing: "
								+ activeMSS);
					}
					disposeState(activeMSS, activeProcessesIteratorToRemoveFrom);
					return false;
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Transition was progressed: " + activeMSS);
				}

				return true; // the cut was progressed

			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("No progress of active MSS: " + activeMSS
					+ " with message event: " + messageEvent);
		}

		return false; // there was no effect
	}

	/**
	 * Evaluates the next binding expressions that can be evaluated for unbound
	 * lifelines and binds the according lifelines. In this process multiple
	 * copies of the cut may be created if a binding value expression evaluates
	 * to a set of objects with more than one element. The method calls itself
	 * recursively after successfully binding a lifeline to try binding the next
	 * unbound lifeline. etc.
	 * 
	 * In more detail: A property represented by the lifeline can have the
	 * following multiplicities: 0..1, 1..1, 0..n, 1..n. The multiplicity
	 * defines whether the lifeline can bind to one or to many objects (upper
	 * bound 1 or n) and whether it must bind to at least one object (lower
	 * bound 1) or it is also okay to bind to no object (lower bound 0).
	 * 
	 * If the binding value expression can be evaluated results to null or an
	 * empty set, but the lower bound is one, this is an error.
	 * 
	 * If the binding value expression can be evaluated and the result is null
	 * or an empty set, and the lower bound is zero, we define that the lifeline
	 * is "ignored". If a lifeline is ignored (which is different from a
	 * lifeline being unbound!), this means that we do not consider messages to
	 * or from the lifeline in the further progress of the cut (we immediately
	 * progress beyond them).
	 * 
	 * If the binding value expression can be evaluated, and the result is a set
	 * of objects with a size greater 1, and the upper bound is one, this is an
	 * error. (If users specify the upper bound of a lifeline/role to be one,
	 * they should see that the value expression does not return a collection!)
	 * 
	 * If the binding value expression can be evaluated, the result is a set of
	 * objects, and the upper bound is n, then multiple copies of the cut are
	 * created, so that for every candidate object a cut exists where the
	 * lifeline is bound to that candidate object.
	 * 
	 * Whenever a lifeline is bound, we also add to the cut a mapping from the
	 * bound lifeline to the first fragment on the lifeline.
	 * 
	 * returns null if - all lifelines in the cut are already bound
	 * 
	 * returns an empty list if - a lifeline is ignored - a violation occurred
	 * 
	 * returns a list with one element if - a lifeline was bound to one element
	 * 
	 * returns a list with more than one element if - copies of cuts where
	 * created.
	 * 
	 * @param activeMSD
	 * @return
	 */
	protected EList<ActiveMSD> evaluateNextLifelineBindingExpression(
			ActiveMSD activeMSD) {

		if (logger.isDebugEnabled()) {
			logger.debug("Attempting to evaluate a next lifeline binding expression for active MSD: "
					+ activeMSD);
		}

		EList<Lifeline> unboundLifelineList = getUnboundLifelines(activeMSD);

		if (unboundLifelineList.isEmpty())
			return null; // nothing more to bind

		Interaction interaction = activeMSD.getInteraction();

		// a list of cuts that we return later
		// we make this a UniqueEList, because recursive calls of this method,
		// where multiple lifelines in this cut may be bound, may add the same
		// cut to the list several times.
		EList<ActiveMSD> activeMSDsWhereAdditionalLifelinesWereBoundReturnList = new UniqueEList<ActiveMSD>();

		// iterate over all the unbound lifelines...
		for (Lifeline unboundLifeline : unboundLifelineList) {
			// if there is a static binding prescribed for the lifeline, bind
			// the lifeline accordingly
			EObject eObjectPrescribedForStaticLifeline = ((MSDObjectSystem) getObjectSystem())
					.getStaticLifelineToEObjectBindings().get(unboundLifeline);
			if (eObjectPrescribedForStaticLifeline != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("There is a static binding prescribed for the lifeline (static lifeline): "
							+ unboundLifeline);
					logger.debug("   --  binding to eObject: "
							+ eObjectPrescribedForStaticLifeline);
				}
				activeMSD.addLifelineBinding(unboundLifeline,
						eObjectPrescribedForStaticLifeline);
				activeMSDsWhereAdditionalLifelinesWereBoundReturnList
						.add(activeMSD);
			} else {
				// NO static binding prescribed for the lifeline
				for (Comment comment : interaction.getOwnedComments()) {
					// ... and find the constraint attached to them
					if (comment.getAnnotatedElements()
							.contains(unboundLifeline)) {
						String lifelineName = RuntimeUtil.Helper
								.getLifelineName(unboundLifeline);
						// dissect the constraint's string into the parts
						// <lifeline-name> = <binding-OCL-expression>
						String bindingAssignmentExpression = comment.getBody();
						String variableName = "";
						if (!bindingAssignmentExpression.contains("=")) {
							logger.error("The binding expression must be of the form \"<lifeline-name> = <binding-OCL-expression>\"): "
									+ bindingAssignmentExpression);
						} else {
							variableName = bindingAssignmentExpression
									.substring(
											0,
											bindingAssignmentExpression
													.indexOf("=")).trim();
							if (!variableName.equals(lifelineName)) {
								logger.error("The binding expression must be of the form \"<lifeline-name> = <binding-OCL-expression>\" where \"<lifeline-name>\" equals the lifeline name (i.e., the name of the role represented by the lifeline) the binding expression is attached to.): "
										+ bindingAssignmentExpression);
							}
						}

						if (logger.isDebugEnabled()) {
							logger.debug("binding assignment variable name: "
									+ variableName);
						}

						String valueExpression = bindingAssignmentExpression
								.substring(
										bindingAssignmentExpression
												.indexOf("=") + 1).trim();

						if (logger.isDebugEnabled()) {
							logger.debug("binding assignment value expression: "
									+ valueExpression);
						}
						// END dissect the constraint's string into the parts
						// <lifeline-name> = <binding-OCL-expression>

						try {
							Object result = activeMSD.assignValueExpression(
									null, variableName, valueExpression);
							if (result == null
									|| (result instanceof Collection<?> && ((Collection<?>) result)
											.isEmpty())) {
								if (logger.isDebugEnabled()) {
									if (result == null) {
										logger.debug("binding value expression evaluated to an null");
									} else {
										logger.debug("binding value expression evaluated to an empty set.");
									}
								}
								if (RuntimeUtil.Helper
										.getLowerboundMultiplicityOfLifeline(unboundLifeline) == 0) {
									// in this case, that means that we can
									// ignore this lifeline.
									activeMSD.getLifelineBindings()
											.getIgnoredLifelines()
											.add(unboundLifeline);

									// here a lifeline binding has changed, thus
									// we add it to the list that we are going
									// to return
									activeMSDsWhereAdditionalLifelinesWereBoundReturnList
											.add(activeMSD);
									// here we recursively call this method and
									// add the result to the list that we are
									// then returning.
									EList<ActiveMSD> furtherAlteredActiveMSDs = evaluateNextLifelineBindingExpression(activeMSD);
									if (furtherAlteredActiveMSDs != null) {
										activeMSDsWhereAdditionalLifelinesWereBoundReturnList
												.addAll(furtherAlteredActiveMSDs);
									}
									return activeMSDsWhereAdditionalLifelinesWereBoundReturnList;
								} else {
									// if the lower bound is not zero, but the
									// result is null or empty, this is an
									// invalid behavior:
									logger.error("The lower bound for the lifeline (role) "
											+ lifelineName
											+ " is not zero, "
											+ "but the binding value expression \""
											+ valueExpression
											+ "\" evaluated to null or an empty set.");
									// handleViolationDependingOnTemperature(cut,
									// null);
								}
							} else if (result instanceof EObject) {
								// if the result is an object, this if fine
								// regardless of the upper and lower bounds of
								// the lifeline (i.e., role).
								EObject eObject = (EObject) result;
								if (!((MSDObjectSystem) getObjectSystem())
										.lifelineMayBindToEObject(
												unboundLifeline, eObject)) {
									// if the lifeline cannot bind the object,
									// this is an invalid case.
									logger.error("The lifeline (role) "
											+ lifelineName
											+ " cannot bind to the object "
											+ RuntimeUtil.Helper
													.getEObjectName(eObject));
								} else {
									if (logger.isDebugEnabled()) {
										logger.debug("Adding lifeline binding for lifeline "
												+ RuntimeUtil.Helper
														.getLifelineName(unboundLifeline)
												+ " to eObject " + eObject);
									}
									activeMSD.addLifelineBinding(
											unboundLifeline, eObject);
									if (logger.isDebugEnabled()) {
										logger.debug("  --  active MSD: "
												+ activeMSD);
									}
									activeMSDsWhereAdditionalLifelinesWereBoundReturnList
											.add(activeMSD);
									EList<ActiveMSD> furtherAlteredActiveMSDs = evaluateNextLifelineBindingExpression(activeMSD);
									if (furtherAlteredActiveMSDs != null) {
										activeMSDsWhereAdditionalLifelinesWereBoundReturnList
												.addAll(furtherAlteredActiveMSDs);
									}
								}
								return activeMSDsWhereAdditionalLifelinesWereBoundReturnList;
							} else if (result instanceof Collection) {
								@SuppressWarnings("unchecked")
								Collection<EObject> collection = (Collection<EObject>) result;
								if (RuntimeUtil.Helper
										.getUpperBoundMultiplicityOfLifeline(unboundLifeline) == 1) {
									// If users specify the upper bound of a
									// lifeline/role to be one, they should see
									// that the value expression does not return
									// a collection!
									if (collection.size() == 1) {
										// in the case where the result is a
										// collection of size 1, we continue,
										// but issue a warning
										logger.warn("The upper bound for the lifeline (role) "
												+ lifelineName
												+ " is not 1, "
												+ "but the binding value expression \""
												+ valueExpression
												+ "\" evaluated to a set. Use e.g. \"...->any()\" in the binding value expression to return only on object");
									} else {
										// in the case where the result is a
										// collection of size >1, we issue and
										// error and return
										logger.error("The upper bound for the lifeline (role) "
												+ lifelineName
												+ " is not 1, "
												+ "but the binding value expression \""
												+ valueExpression
												+ "\" evaluated to a set. Use e.g. \"...->any()\" in the binding value expression to return only on object");
										return activeMSDsWhereAdditionalLifelinesWereBoundReturnList;
									}
								} else { // upper bound != 1
									Iterator<EObject> resultIterator = collection
											.iterator();

									// for the first object in the collection,
									// we add an additional lifeline binding to
									// the current cut.
									EObject firstEObjectInResult = resultIterator
											.next();
									activeMSD.addLifelineBinding(
											unboundLifeline,
											firstEObjectInResult);
									// here a lifeline binding has changed, thus
									// we add it to the list that we are going
									// to return
									activeMSDsWhereAdditionalLifelinesWereBoundReturnList
											.add(activeMSD);
									// here we recursively call this method and
									// add the result to the list that we are
									// then returning.
									EList<ActiveMSD> furtherAlteredActiveMSDs = evaluateNextLifelineBindingExpression(activeMSD);
									if (furtherAlteredActiveMSDs != null) {
										activeMSDsWhereAdditionalLifelinesWereBoundReturnList
												.addAll(furtherAlteredActiveMSDs);
									}

									// for the next objects in the collection,
									// we multiply the cut and add an additional
									// lifeline binding that that multiplied
									// cut.
									while (resultIterator.hasNext()) {
										EObject eObject = (EObject) resultIterator
												.next();
										ActiveMSD multipliedActiveMSD = EcoreUtil
												.copy(activeMSD);
										multipliedActiveMSD.addLifelineBinding(
												unboundLifeline, eObject);
										// here a new cut was created, thus we
										// add it to the list that we are going
										// to return
										activeMSDsWhereAdditionalLifelinesWereBoundReturnList
												.add(multipliedActiveMSD);
										// here we recursively call this method
										// and add the result to the list that
										// we are then returning.
										furtherAlteredActiveMSDs = evaluateNextLifelineBindingExpression(multipliedActiveMSD);
										if (furtherAlteredActiveMSDs != null) {
											activeMSDsWhereAdditionalLifelinesWereBoundReturnList
													.addAll(furtherAlteredActiveMSDs);
										}
									}
									return activeMSDsWhereAdditionalLifelinesWereBoundReturnList;
								}
							} else {
								logger.error("Cannot handle this kind: "
										+ result);
							}

						} catch (SemanticException e) {
							if (!e.getMessage().contains(
									"Unrecognized variable")) {
								// this case means that we are missing a
								// variable or another lifeline binding to
								// succcessfully evaluate the binding
								// expression.
								logger.debug(e);
								// Thus we just continue and look at the next
								// unbound lifeline.
							}else{
								logger.warn("Active copy of " + activeMSD.getInteraction().getName());
								logger.warn("Expression " + bindingAssignmentExpression);

								logger.warn(e);
							}
						} catch (ParserException e) {
							// There is some other problem with parsing the
							// code.
							logger.error(e);
						}

					}
				}

			}

		}

		if (logger.isDebugEnabled()) {
			logger.debug("Cuts where additional lifelines were bound: "
					+ activeMSDsWhereAdditionalLifelinesWereBoundReturnList);
		}

		return activeMSDsWhereAdditionalLifelinesWereBoundReturnList;
	}

	protected EList<Lifeline> getUnboundLifelines(ActiveMSD activeMSD) {
		EList<Lifeline> unboundLifelinesList = new BasicEList<Lifeline>();
		for (Lifeline lifeline : activeMSD.getInteraction().getLifelines()) {
			if (!activeMSD.getLifelineBindings().getLifelineToEObjectMap()
					.keySet().contains(lifeline)
					&& !activeMSD.getLifelineBindings().getIgnoredLifelines()
							.contains(lifeline))
				unboundLifelinesList.add(lifeline);
		}
		return unboundLifelinesList;
	}

	private EList<Property> getUnboundRoles(ActiveMSS activeMSS) {
		EList<Property> unboundRolesList = new BasicEList<Property>();
		Set<Property> allMSSRolesList = new HashSet<Property>();
		for (Transition transition : activeMSS.getStateMachine().getRegions()
				.get(0).getTransitions()) {
			if (RuntimeUtil.Helper.isMessageEventTransition(transition)) {
				allMSSRolesList.add(RuntimeUtil.Helper.getSender(transition));
				allMSSRolesList.add(RuntimeUtil.Helper.getReceiver(transition));
			}
		}
		for (Property role : allMSSRolesList)
			if (activeMSS.getRoleBindings().getPropertyToEObjectMap().get(role) == null)
				unboundRolesList.add(role);
		return unboundRolesList;
	}

	protected ActiveMSD createActiveMSD() {
		return RuntimeFactory.eINSTANCE.createActiveMSD();
	}

	protected ActiveMSDCut createCut() {
		return RuntimeFactory.eINSTANCE.createActiveMSDCut();
	}

	protected EList<ActiveMSD> createActiveMSDs(MessageEvent messageEvent) {
		if (logger.isDebugEnabled()) {
			logger.debug("Creating active MSDs for message event: "
					+ messageEvent);
			Assert.isTrue(getObjectSystem().containsEObject(
					messageEvent.getSendingObject()));
			Assert.isTrue(getObjectSystem().containsEObject(
					messageEvent.getReceivingObject()));
		}

		EList<ActiveMSD> createdActiveMSDs = new BasicEList<ActiveMSD>();

		for (Interaction interaction : getRuntimeUtil().getInteractions()) {

			// do not create new active assumption or requirement MSDs if
			// already a safety violation occurred in the requirements resp.
			// assumptions.
			boolean isAssumptionMSD = getRuntimeUtil()
					.getInteractionToMSDUtilMap().get(interaction)
					.isEnvironmentAssumption();
			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSD
					&& isSafetyViolationOccurredInRequirements())
				continue;

			Message firstMessage = RuntimeUtil.Helper
					.getFirstMessageForMSD(interaction);
			// if firstMessage and messageEvent has the same operation, sending
			// class and receiving class
			if (getRuntimeUtil().getOperation(firstMessage).equals(
					messageEvent.getOperation())
					&& ((MSDObjectSystem) getObjectSystem())
							.lifelineMayBindToEObject(RuntimeUtil.Helper
									.getSendingLifeline(firstMessage),
									messageEvent.getSendingObject())
					&& ((MSDObjectSystem) getObjectSystem())
							.lifelineMayBindToEObject(RuntimeUtil.Helper
									.getReceivingLifeline(firstMessage),
									messageEvent.getReceivingObject())
					&& RuntimeUtil.Helper
							.isDiagramMessageParameterUnifiableWithValue(
									messageEvent, getRuntimeUtil()
											.getParameterValue(firstMessage))) {
				// create the cut and progress it
				ActiveMSD activeMSD = createActiveMSD();
				activeMSD.setRuntimeUtil(getRuntimeUtil());
				activeMSD.setMsdUtil(getRuntimeUtil()
						.getInteractionToMSDUtilMap().get(interaction));
				activeMSD.setObjectSystem((MSDObjectSystem) getObjectSystem());
				ActiveMSDCut cut = createCut();
				cut.setRuntimeUtil(getRuntimeUtil());
				ActiveMSDLifelineBindings lifelineBindings = RuntimeFactory.eINSTANCE
						.createActiveMSDLifelineBindings();
				ActiveMSDVariableValuations variableValuations = RuntimeFactory.eINSTANCE
						.createActiveMSDVariableValuations();
				activeMSD.setCurrentState(cut);
				activeMSD.setLifelineBindings(lifelineBindings);
				activeMSD.setVariableValuations(variableValuations);

				Lifeline sendingLifeline = RuntimeUtil.Helper
						.getSendingLifeline(firstMessage);
				Lifeline receivingLifeline = RuntimeUtil.Helper
						.getReceivingLifeline(firstMessage);

				activeMSD.addLifelineBinding(sendingLifeline,
						messageEvent.getSendingObject());
				activeMSD.addLifelineBinding(receivingLifeline,
						messageEvent.getReceivingObject());

				evaluateNextLifelineBindingExpression(activeMSD);

				// progress the active MSD beyond the first message.
				activeMSD
						.getCurrentState()
						.progressCutLocationOnLifeline(
								sendingLifeline,
								RuntimeUtil.Helper
										.getSendingMessageOccurrenceSpecification(firstMessage));
				if (messageEvent instanceof SynchronousMessageEvent)
					activeMSD
							.getCurrentState()
							.progressCutLocationOnLifeline(
									receivingLifeline,
									RuntimeUtil.Helper
											.getReceivingMessageOccurrenceSpecification(firstMessage));

				// init OCL for active MSD:

				OCL ocl = getRuntimeUtil().getOclRegistry().getOCLForActiveMSD(
						activeMSD);

				for (Map.Entry<Lifeline, EObject> lifelineToEObjectMapEntry : activeMSD
						.getLifelineBindings().getLifelineToEObjectMap()
						.entrySet()) {
					activeMSD.assignValue(ocl,
							RuntimeUtil.Helper
									.getLifelineName(lifelineToEObjectMapEntry
											.getKey()),
							lifelineToEObjectMapEntry.getValue());
				}

				for (Map.Entry<String, EObject> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToEObjectValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, String> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToStringValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Integer> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToIntegerValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				for (Map.Entry<String, Boolean> variableNameToValueMapEntry : activeMSD
						.getVariableValuations()
						.getVariableNameToBooleanValueMap().entrySet()) {
					activeMSD.assignValue(ocl,
							variableNameToValueMapEntry.getKey(),
							variableNameToValueMapEntry.getValue());
				}
				
				

				evaluateNextLifelineBindingExpression(activeMSD);


				
				// progress the active MSD beyond the first message.
				activeMSD
						.getCurrentState()
						.progressCutLocationOnLifeline(
								sendingLifeline,
								RuntimeUtil.Helper
										.getSendingMessageOccurrenceSpecification(firstMessage));
				if(messageEvent instanceof SynchronousMessageEvent)
					activeMSD
							.getCurrentState()
							.progressCutLocationOnLifeline(
									receivingLifeline,
									RuntimeUtil.Helper
											.getReceivingMessageOccurrenceSpecification(firstMessage));


				
				if (logger.isDebugEnabled()) {
					logger.debug("Active MSD created: " + activeMSD);
				}

				// add the cut to the current state
				getActiveProcesses().add(activeMSD);
				createdActiveMSDs.add(activeMSD);
			}
		}
		return createdActiveMSDs;
	}

	protected EList<ActiveMSS> createActiveMSSs(MessageEvent messageEvent) {
		if (logger.isDebugEnabled()) {
			logger.debug("Creating active MSSs for message event: "
					+ messageEvent);
		}

		EList<ActiveMSS> createdActiveMSSs = new BasicEList<ActiveMSS>();

		for (StateMachine stateMachine : getRuntimeUtil().getStateMachines()) {

			// do not create new active assumption or requirement MSSs if
			// already a safety violation occurred in the requirements resp.
			// assumptions.
			boolean isAssumptionMSS = RuntimeUtil.Helper
					.isEnvironmentAssumption(stateMachine);
			if (isSafetyViolationOccurredInAssumptions() || !isAssumptionMSS
					&& isSafetyViolationOccurredInRequirements())
				continue;

			Collection<Transition> firstTransitions = RuntimeUtil.Helper
					.getFirstTransitionsForMSS(stateMachine, getRuntimeUtil());
			for (Transition firstTransition : firstTransitions) {
				Property senderRole = RuntimeUtil.Helper
						.getSender(firstTransition);
				Property receiverRole = RuntimeUtil.Helper
						.getReceiver(firstTransition);

				// if firstTransition and messageEvent has the same operation,
				// sending
				// class and receiving class
				// TODO: support message parameters
				// if(getRuntimeUtil().getOperation(firstTransition)==null)
				// System.out.println();
				if (getRuntimeUtil().getOperation(firstTransition).equals(
						messageEvent.getOperation())
						&& ((MSDObjectSystem) getObjectSystem())
								.roleMayBindToEObject(senderRole,
										messageEvent.getSendingObject())
						&& ((MSDObjectSystem) getObjectSystem())
								.roleMayBindToEObject(receiverRole,
										messageEvent.getReceivingObject())
				/*
				 * && RuntimeUtil.Helper.isParameterEqual( messageEvent,
				 * getRuntimeUtil().getParameterValue( firstTransition))
				 */
				) {
					// create the state and progress it
					ActiveMSS activeMSS = RuntimeFactory.eINSTANCE
							.createActiveMSS();
					activeMSS.setRuntimeUtil(getRuntimeUtil());
					activeMSS.setStateMachine(stateMachine);
					ActiveMSSState state = RuntimeFactory.eINSTANCE
							.createActiveMSSState();
					state.setRuntimeUtil(getRuntimeUtil());
					ActiveMSSRoleBindings roleBindings = RuntimeFactory.eINSTANCE
							.createActiveMSSRoleBindings();
					ActiveMSDVariableValuations variableValuations = RuntimeFactory.eINSTANCE
							.createActiveMSDVariableValuations();
					activeMSS.setCurrentState(state);
					activeMSS.setRoleBindings(roleBindings);
					activeMSS.setVariableValuations(variableValuations);
					activeMSS
							.setObjectSystem((MSDObjectSystem) getObjectSystem());

					activeMSS.getRoleBindings().getPropertyToEObjectMap()
							.put(senderRole, messageEvent.getSendingObject());
					activeMSS
							.getRoleBindings()
							.getPropertyToEObjectMap()
							.put(receiverRole,
									messageEvent.getReceivingObject());
					state.setUmlState((State) firstTransition.getTarget());

					// TODO: support variables
					// init OCL for active MSD:
					// OCL ocl = getRuntimeUtil().getOclRegistry()
					// .getOCLForActiveMSD(activeMSS);
					//
					// for (Map.Entry<Lifeline, EObject>
					// lifelineToEObjectMapEntry : activeMSS
					// .getLifelineBindings().getLifelineToEObjectMap()
					// .entrySet()) {
					// activeMSS.assignValue(ocl, RuntimeUtil.Helper
					// .getLifelineName(lifelineToEObjectMapEntry
					// .getKey()), lifelineToEObjectMapEntry
					// .getValue());
					// }
					//
					// for (Map.Entry<String, EObject>
					// variableNameToValueMapEntry : activeMSS
					// .getVariableValuations()
					// .getVariableNameToEObjectValueMap().entrySet()) {
					// activeMSS.assignValue(ocl,
					// variableNameToValueMapEntry.getKey(),
					// variableNameToValueMapEntry.getValue());
					// }
					// for (Map.Entry<String, String>
					// variableNameToValueMapEntry : activeMSS
					// .getVariableValuations()
					// .getVariableNameToStringValueMap().entrySet()) {
					// activeMSS.assignValue(ocl,
					// variableNameToValueMapEntry.getKey(),
					// variableNameToValueMapEntry.getValue());
					// }
					// for (Map.Entry<String, Integer>
					// variableNameToValueMapEntry : activeMSS
					// .getVariableValuations()
					// .getVariableNameToIntegerValueMap().entrySet()) {
					// activeMSS.assignValue(ocl,
					// variableNameToValueMapEntry.getKey(),
					// variableNameToValueMapEntry.getValue());
					// }
					// for (Map.Entry<String, Boolean>
					// variableNameToValueMapEntry : activeMSS
					// .getVariableValuations()
					// .getVariableNameToBooleanValueMap().entrySet()) {
					// activeMSS.assignValue(ocl,
					// variableNameToValueMapEntry.getKey(),
					// variableNameToValueMapEntry.getValue());
					// }

					if (logger.isDebugEnabled()) {
						logger.debug("Active MSS created: " + activeMSS);
					}

					// TODO: apparently this is necessary, but might be overkill
					ActiveMSS lookedUpNewProcess = (ActiveMSS) getRuntimeUtil()
							.getMsdRuntimeStateGraph().getElementContainer()
							.getActiveProcess(activeMSS);
					boolean duplicateFound = false;
					// TODO: would be faster if activeProcesses was a real set
					for (ActiveProcess oldProcess : getActiveProcesses()) {
						if (oldProcess instanceof ActiveMSS) {
							ActiveMSS lookedUpOldProcess = (ActiveMSS) getRuntimeUtil()
									.getMsdRuntimeStateGraph()
									.getElementContainer()
									.getActiveProcess(oldProcess);
							if (lookedUpNewProcess == lookedUpOldProcess) {
								duplicateFound = true;
								break;
							}
						}
					}

					if (!duplicateFound) {
						getActiveProcesses().add(activeMSS);
						createdActiveMSSs.add(activeMSS);
					}
				}
			}
		}
		return createdActiveMSSs;
	}

	// /**
	// * @param forAssumptions
	// * @return
	// */
	// private boolean hasMandatoryMessageEvents(boolean forAssumptions) {
	// for (Entry<MessageEvent, ModalMessageEvent>
	// messageEventToModalMessageEventMapEntry :
	// getMessageEventToModalMessageEventMap().entrySet()) {
	// ModalMessageEvent modalMessageEvent =
	// messageEventToModalMessageEventMapEntry
	// .getValue();
	// if (forAssumptions) {
	// if (modalMessageEvent.getAssumptionsModality().isMandatory())
	// return true;
	// } else {
	// if (modalMessageEvent.getRequirementsModality().isMandatory())
	// return true;
	// }
	// }
	// return false;
	// }

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES:
				return getActiveProcesses();
			case RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL:
				if (resolve) return getRuntimeUtil();
				return basicGetRuntimeUtil();
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP:
				return getActiveMSDChangedDuringPerformStep();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES:
				getActiveProcesses().clear();
				getActiveProcesses().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)newValue);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP:
				getActiveMSDChangedDuringPerformStep().clear();
				getActiveMSDChangedDuringPerformStep().addAll((Collection<? extends ActiveProcess>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES:
				getActiveProcesses().clear();
				return;
			case RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL:
				setRuntimeUtil((RuntimeUtil)null);
				return;
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP:
				getActiveMSDChangedDuringPerformStep().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_PROCESSES:
				return activeProcesses != null && !activeProcesses.isEmpty();
			case RuntimePackage.MSD_RUNTIME_STATE__RUNTIME_UTIL:
				return runtimeUtil != null;
			case RuntimePackage.MSD_RUNTIME_STATE__ACTIVE_MSD_CHANGED_DURING_PERFORM_STEP:
				return activeMSDChangedDuringPerformStep != null && !activeMSDChangedDuringPerformStep.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String getLabel() {
		String returnString = "";
		returnString = returnString + this.getStringToStringAnnotationMap().get("passedIndex");
		// try{
		// returnString = returnString + ": " +
		// RuntimeUtil.Helper.getMSDRuntimeStateString(this);
		// }catch(NullPointerException e){
		//
		// }
		return returnString;
	}

} // MSDRuntimeStateImpl
