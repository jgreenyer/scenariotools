/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.runtime.RuntimePackage
 * @generated
 */
public interface RuntimeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimeFactory eINSTANCE = org.scenariotools.msd.runtime.impl.RuntimeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MSD Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Runtime State Graph</em>'.
	 * @generated
	 */
	MSDRuntimeStateGraph createMSDRuntimeStateGraph();

	/**
	 * Returns a new object of class '<em>MSD Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Runtime State</em>'.
	 * @generated
	 */
	MSDRuntimeState createMSDRuntimeState();

	/**
	 * Returns a new object of class '<em>Element Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Container</em>'.
	 * @generated
	 */
	ElementContainer createElementContainer();

	/**
	 * Returns a new object of class '<em>MSD Modality</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Modality</em>'.
	 * @generated
	 */
	MSDModality createMSDModality();

	/**
	 * Returns a new object of class '<em>Active Process</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Process</em>'.
	 * @generated
	 */
	ActiveProcess createActiveProcess();

	/**
	 * Returns a new object of class '<em>Active MSD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSD</em>'.
	 * @generated
	 */
	ActiveMSD createActiveMSD();

	/**
	 * Returns a new object of class '<em>Active MSS Role Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSS Role Bindings</em>'.
	 * @generated
	 */
	ActiveMSSRoleBindings createActiveMSSRoleBindings();

	/**
	 * Returns a new object of class '<em>Active State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active State</em>'.
	 * @generated
	 */
	ActiveState createActiveState();

	/**
	 * Returns a new object of class '<em>Active MSS State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSS State</em>'.
	 * @generated
	 */
	ActiveMSSState createActiveMSSState();

	/**
	 * Returns a new object of class '<em>Active MSD Cut</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSD Cut</em>'.
	 * @generated
	 */
	ActiveMSDCut createActiveMSDCut();

	/**
	 * Returns a new object of class '<em>Active MSD Lifeline Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSD Lifeline Bindings</em>'.
	 * @generated
	 */
	ActiveMSDLifelineBindings createActiveMSDLifelineBindings();

	/**
	 * Returns a new object of class '<em>Active MSD Variable Valuations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSD Variable Valuations</em>'.
	 * @generated
	 */
	ActiveMSDVariableValuations createActiveMSDVariableValuations();

	/**
	 * Returns a new object of class '<em>MSD Modal Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Modal Message Event</em>'.
	 * @generated
	 */
	MSDModalMessageEvent createMSDModalMessageEvent();

	/**
	 * Returns a new object of class '<em>MSD Object System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Object System</em>'.
	 * @generated
	 */
	MSDObjectSystem createMSDObjectSystem();

	/**
	 * Returns a new object of class '<em>Message Event Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Event Container</em>'.
	 * @generated
	 */
	MessageEventContainer createMessageEventContainer();

	/**
	 * Returns a new object of class '<em>Active MSS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active MSS</em>'.
	 * @generated
	 */
	ActiveMSS createActiveMSS();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RuntimePackage getRuntimePackage();

} //RuntimeFactory
