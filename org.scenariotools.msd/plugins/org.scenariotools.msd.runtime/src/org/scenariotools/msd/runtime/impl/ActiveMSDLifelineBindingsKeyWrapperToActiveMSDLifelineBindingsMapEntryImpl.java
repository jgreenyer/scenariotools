/**
 */
package org.scenariotools.msd.runtime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveMSDLifelineBindings;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active MSD Lifeline Bindings Key Wrapper To Active MSD Lifeline Bindings Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl#getTypedValue <em>Value</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.impl.ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl#getTypedKey <em>Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl extends RuntimeEObjectImpl implements BasicEMap.Entry<ActiveMSDLifelineBindingsKeyWrapper,ActiveMSDLifelineBindings> {
	/**
	 * The cached value of the '{@link #getTypedValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedValue()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSDLifelineBindings value;

	/**
	 * The default value of the '{@link #getTypedKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected static final ActiveMSDLifelineBindingsKeyWrapper KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypedKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected ActiveMSDLifelineBindingsKeyWrapper key = KEY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings getTypedValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (ActiveMSDLifelineBindings)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings basicGetTypedValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedValue(ActiveMSDLifelineBindings newValue) {
		ActiveMSDLifelineBindings oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindingsKeyWrapper getTypedKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedKey(ActiveMSDLifelineBindingsKeyWrapper newKey) {
		ActiveMSDLifelineBindingsKeyWrapper oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY, oldKey, key));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE:
				if (resolve) return getTypedValue();
				return basicGetTypedValue();
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY:
				return getTypedKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE:
				setTypedValue((ActiveMSDLifelineBindings)newValue);
				return;
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY:
				setTypedKey((ActiveMSDLifelineBindingsKeyWrapper)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE:
				setTypedValue((ActiveMSDLifelineBindings)null);
				return;
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY:
				setTypedKey(KEY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__VALUE:
				return value != null;
			case RuntimePackage.ACTIVE_MSD_LIFELINE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_MSD_LIFELINE_BINDINGS_MAP_ENTRY__KEY:
				return KEY_EDEFAULT == null ? key != null : !KEY_EDEFAULT.equals(key);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (key: ");
		result.append(key);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected int hash = -1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHash() {
		if (hash == -1) {
			Object theKey = getKey();
			hash = (theKey == null ? 0 : theKey.hashCode());
		}
		return hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHash(int hash) {
		this.hash = hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindingsKeyWrapper getKey() {
		return getTypedKey();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(ActiveMSDLifelineBindingsKeyWrapper key) {
		setTypedKey(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings getValue() {
		return getTypedValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMSDLifelineBindings setValue(ActiveMSDLifelineBindings value) {
		ActiveMSDLifelineBindings oldValue = getValue();
		setTypedValue(value);
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EMap<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings> getEMap() {
		EObject container = eContainer();
		return container == null ? null : (EMap<ActiveMSDLifelineBindingsKeyWrapper, ActiveMSDLifelineBindings>)container.eGet(eContainmentFeature());
	}

} //ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntryImpl
