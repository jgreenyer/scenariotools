package org.scenariotools.msd.runtime.keywrapper;

import org.scenariotools.msd.runtime.ActiveMSDVariableValuations;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveMSSRoleBindings;
import org.scenariotools.msd.runtime.ActiveMSSState;

public class ActiveMSSKeyWrapper extends ActiveProcessKeyWrapper {
	public ActiveMSSKeyWrapper(ActiveMSS activeMSS) {
		this(activeMSS.getCurrentState(), activeMSS.getRoleBindings(),
				activeMSS.getVariableValuations());
	}

	public ActiveMSSKeyWrapper(ActiveMSSState activeMSSState,
			ActiveMSSRoleBindings activeRoleBindings,
			ActiveMSDVariableValuations activeMSDVariableValuations) {
		addSubObject(activeMSSState);
		addSubObject(activeRoleBindings);
		addSubObject(activeMSDVariableValuations);
	}
}
