/**
 */
package org.scenariotools.msd.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Lifeline;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active MSD Lifeline Bindings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getLifelineToEObjectMap <em>Lifeline To EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.runtime.ActiveMSDLifelineBindings#getIgnoredLifelines <em>Ignored Lifelines</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDLifelineBindings()
 * @model
 * @generated
 */
public interface ActiveMSDLifelineBindings extends EObject {
	/**
	 * Returns the value of the '<em><b>Lifeline To EObject Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Lifeline},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline To EObject Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifeline To EObject Map</em>' map.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDLifelineBindings_LifelineToEObjectMap()
	 * @model mapType="org.scenariotools.msd.runtime.LifelineToEObjectMapEntry<org.eclipse.uml2.uml.Lifeline, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<Lifeline, EObject> getLifelineToEObjectMap();

	/**
	 * Returns the value of the '<em><b>Ignored Lifelines</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Lifeline}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * depending on the multiplicity of the role, a lifeline may be bound to no object (not to be confused with an unbound lifeline!!)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Ignored Lifelines</em>' reference list.
	 * @see org.scenariotools.msd.runtime.RuntimePackage#getActiveMSDLifelineBindings_IgnoredLifelines()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Lifeline> getIgnoredLifelines();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Lifeline} with the specified '<em><b>Name</b></em>' from the '<em><b>Ignored Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Lifeline} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Lifeline} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getIgnoredLifelines()
	 * @generated
	 */
	Lifeline getIgnoredLifelines(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Lifeline} with the specified '<em><b>Name</b></em>' from the '<em><b>Ignored Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Lifeline} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Lifeline} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getIgnoredLifelines()
	 * @generated
	 */
	Lifeline getIgnoredLifelines(String name, boolean ignoreCase);

} // ActiveMSDLifelineBindings
