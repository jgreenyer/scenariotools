/**
 */
package org.scenariotools.msd.util.impl;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.scenariotools.msd.util.*;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UtilFactoryImpl extends EFactoryImpl implements UtilFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UtilFactory init() {
		try {
			UtilFactory theUtilFactory = (UtilFactory)EPackage.Registry.INSTANCE.getEFactory(UtilPackage.eNS_URI);
			if (theUtilFactory != null) {
				return theUtilFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UtilFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UtilPackage.RUNTIME_UTIL: return createRuntimeUtil();
			case UtilPackage.UML2_ECORE_MAPPING: return createUML2EcoreMapping();
			case UtilPackage.UML_CLASS_TO_ECLASS_MAP_ENTRY: return (EObject)createUMLClassToEClassMapEntry();
			case UtilPackage.ECLASS_TO_UML_CLASS_MAP_ENTRY: return (EObject)createEClassToUMLClassMapEntry();
			case UtilPackage.EOPERATION2_UML_OPERATION_MAP_ENTRY: return (EObject)createEOperation2UMLOperationMapEntry();
			case UtilPackage.UML_OPERATION_TO_EOPERATION_MAP_ENTRY: return (EObject)createUMLOperationToEOperationMapEntry();
			case UtilPackage.EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY: return (EObject)createEParameterToUMLParameterMapEntry();
			case UtilPackage.UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY: return (EObject)createUMLParameterToEParameterMapEntry();
			case UtilPackage.ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY: return (EObject)createEStructuralFeatureToUMLOperationMapEntry();
			case UtilPackage.UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY: return (EObject)createUMLOperationToEStructuralFeatureMapEntry();
			case UtilPackage.STRING_TO_LIFELINE_MAP_ENTRY: return (EObject)createStringToLifelineMapEntry();
			case UtilPackage.FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY: return (EObject)createFirstMessageOperationToInteractionMapEntry();
			case UtilPackage.FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY: return (EObject)createFirstTransitionOperationToStateMachineMapEntry();
			case UtilPackage.LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY: return (EObject)createLifelineToInteractionFragmentsMapEntry();
			case UtilPackage.PACKAGE_TO_COLLABORATION_MAP_ENTRY: return (EObject)createPackageToCollaborationMapEntry();
			case UtilPackage.INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY: return (EObject)createInteractionToStringToLifelineMapMapEntry();
			case UtilPackage.PROPERTY_TO_EOBJECT_MAP_ENTRY: return (EObject)createPropertyToEObjectMapEntry();
			case UtilPackage.OCL_REGISTRY: return createOCLRegistry();
			case UtilPackage.MSD_UTIL: return createMSDUtil();
			case UtilPackage.INTERACTION_TO_MSD_UTIL_MAP_ENTRY: return (EObject)createInteractionToMSDUtilMapEntry();
			case UtilPackage.INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY: return (EObject)createInteractionOperandToLifelineToInteractionFragmentsMapEntry();
			case UtilPackage.STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY: return (EObject)createStateMachineToInitialStateMapEntry();
			case UtilPackage.ID_TO_OCL_MAP_ENTRY: return (EObject)createIDToOCLMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case UtilPackage.PLAIN_JAVA_OBJECT:
				return createPlainJavaObjectFromString(eDataType, initialValue);
			case UtilPackage.OCL:
				return createOCLFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case UtilPackage.PLAIN_JAVA_OBJECT:
				return convertPlainJavaObjectToString(eDataType, instanceValue);
			case UtilPackage.OCL:
				return convertOCLToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeUtil createRuntimeUtil() {
		RuntimeUtilImpl runtimeUtil = new RuntimeUtilImpl();
		return runtimeUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UML2EcoreMapping createUML2EcoreMapping() {
		UML2EcoreMappingImpl uml2EcoreMapping = new UML2EcoreMappingImpl();
		return uml2EcoreMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<org.eclipse.uml2.uml.Class, EClass> createUMLClassToEClassMapEntry() {
		UMLClassToEClassMapEntryImpl umlClassToEClassMapEntry = new UMLClassToEClassMapEntryImpl();
		return umlClassToEClassMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EClass, EList<org.eclipse.uml2.uml.Class>> createEClassToUMLClassMapEntry() {
		EClassToUMLClassMapEntryImpl eClassToUMLClassMapEntry = new EClassToUMLClassMapEntryImpl();
		return eClassToUMLClassMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EOperation, EList<Operation>> createEOperation2UMLOperationMapEntry() {
		EOperation2UMLOperationMapEntryImpl eOperation2UMLOperationMapEntry = new EOperation2UMLOperationMapEntryImpl();
		return eOperation2UMLOperationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Operation, EOperation> createUMLOperationToEOperationMapEntry() {
		UMLOperationToEOperationMapEntryImpl umlOperationToEOperationMapEntry = new UMLOperationToEOperationMapEntryImpl();
		return umlOperationToEOperationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EParameter, EList<Parameter>> createEParameterToUMLParameterMapEntry() {
		EParameterToUMLParameterMapEntryImpl eParameterToUMLParameterMapEntry = new EParameterToUMLParameterMapEntryImpl();
		return eParameterToUMLParameterMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Parameter, EParameter> createUMLParameterToEParameterMapEntry() {
		UMLParameterToEParameterMapEntryImpl umlParameterToEParameterMapEntry = new UMLParameterToEParameterMapEntryImpl();
		return umlParameterToEParameterMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EStructuralFeature, EList<Operation>> createEStructuralFeatureToUMLOperationMapEntry() {
		EStructuralFeatureToUMLOperationMapEntryImpl eStructuralFeatureToUMLOperationMapEntry = new EStructuralFeatureToUMLOperationMapEntryImpl();
		return eStructuralFeatureToUMLOperationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Operation, EStructuralFeature> createUMLOperationToEStructuralFeatureMapEntry() {
		UMLOperationToEStructuralFeatureMapEntryImpl umlOperationToEStructuralFeatureMapEntry = new UMLOperationToEStructuralFeatureMapEntryImpl();
		return umlOperationToEStructuralFeatureMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Lifeline> createStringToLifelineMapEntry() {
		StringToLifelineMapEntryImpl stringToLifelineMapEntry = new StringToLifelineMapEntryImpl();
		return stringToLifelineMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Operation, EList<Interaction>> createFirstMessageOperationToInteractionMapEntry() {
		FirstMessageOperationToInteractionMapEntryImpl firstMessageOperationToInteractionMapEntry = new FirstMessageOperationToInteractionMapEntryImpl();
		return firstMessageOperationToInteractionMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Operation, EList<StateMachine>> createFirstTransitionOperationToStateMachineMapEntry() {
		FirstTransitionOperationToStateMachineMapEntryImpl firstTransitionOperationToStateMachineMapEntry = new FirstTransitionOperationToStateMachineMapEntryImpl();
		return firstTransitionOperationToStateMachineMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Lifeline, EList<InteractionFragment>> createLifelineToInteractionFragmentsMapEntry() {
		LifelineToInteractionFragmentsMapEntryImpl lifelineToInteractionFragmentsMapEntry = new LifelineToInteractionFragmentsMapEntryImpl();
		return lifelineToInteractionFragmentsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<org.eclipse.uml2.uml.Package, EList<Collaboration>> createPackageToCollaborationMapEntry() {
		PackageToCollaborationMapEntryImpl packageToCollaborationMapEntry = new PackageToCollaborationMapEntryImpl();
		return packageToCollaborationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Interaction, EMap<String, Lifeline>> createInteractionToStringToLifelineMapMapEntry() {
		InteractionToStringToLifelineMapMapEntryImpl interactionToStringToLifelineMapMapEntry = new InteractionToStringToLifelineMapMapEntryImpl();
		return interactionToStringToLifelineMapMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Property, EObject> createPropertyToEObjectMapEntry() {
		PropertyToEObjectMapEntryImpl propertyToEObjectMapEntry = new PropertyToEObjectMapEntryImpl();
		return propertyToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLRegistry createOCLRegistry() {
		OCLRegistryImpl oclRegistry = new OCLRegistryImpl();
		return oclRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSDUtil createMSDUtil() {
		MSDUtilImpl msdUtil = new MSDUtilImpl();
		return msdUtil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Interaction, MSDUtil> createInteractionToMSDUtilMapEntry() {
		InteractionToMSDUtilMapEntryImpl interactionToMSDUtilMapEntry = new InteractionToMSDUtilMapEntryImpl();
		return interactionToMSDUtilMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> createInteractionOperandToLifelineToInteractionFragmentsMapEntry() {
		InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl interactionOperandToLifelineToInteractionFragmentsMapEntry = new InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl();
		return interactionOperandToLifelineToInteractionFragmentsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<StateMachine, State> createStateMachineToInitialStateMapEntry() {
		StateMachineToInitialStateMapEntryImpl stateMachineToInitialStateMapEntry = new StateMachineToInitialStateMapEntryImpl();
		return stateMachineToInitialStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, OCL> createIDToOCLMapEntry() {
		IDToOCLMapEntryImpl idToOCLMapEntry = new IDToOCLMapEntryImpl();
		return idToOCLMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createPlainJavaObjectFromString(EDataType eDataType, String initialValue) {
		return super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPlainJavaObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCL createOCLFromString(EDataType eDataType, String initialValue) {
		return (OCL)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOCLToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilPackage getUtilPackage() {
		return (UtilPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UtilPackage getPackage() {
		return UtilPackage.eINSTANCE;
	}

} //UtilFactoryImpl
