/**
 */
package org.scenariotools.msd.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionConstraint;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateInvariant;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.ValueSpecification;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Runtime Util</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getUml2ecoreMapping <em>Uml2ecore Mapping</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getFirstMessageOperationToInteractionMap <em>First Message Operation To Interaction Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getFirstTransitionOperationToStateMachineMap <em>First Transition Operation To State Machine Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionToStringToLifelineMapMap <em>Interaction To String To Lifeline Map Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getLifelineToInteractionFragmentsMap <em>Lifeline To Interaction Fragments Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getSystemClasses <em>System Classes</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getEnvironmentClasses <em>Environment Classes</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getUmlPackages <em>Uml Packages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getPackageToCollaborationsMap <em>Package To Collaborations Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getInteractions <em>Interactions</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getIntegratedPackage <em>Integrated Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getEnvironmentSendableMessages <em>Environment Sendable Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getSystemSendableMessages <em>System Sendable Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getOclRegistry <em>Ocl Registry</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getFinalFragment <em>Final Fragment</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionToMSDUtilMap <em>Interaction To MSD Util Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionOperandToLifelineToInteractionFragmentsMapMap <em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getStateMachines <em>State Machines</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.RuntimeUtil#getStateMachineToInitialStateMap <em>State Machine To Initial State Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil()
 * @model
 * @generated
 */
public interface RuntimeUtil extends EObject {

	/**
	 * Contains static helper methods
	 * 
	 * @author jgreen
	 * 
	 */
	public static final class Helper {
		private static Map<String, Map<String, Integer>> classNameToEObjectToNumberMap = new HashMap<String, Map<String, Integer>>();
		private static Map<String, Integer> classNameToMaxNumberMap = new HashMap<String, Integer>();

		public static void resetObjectNumbers() {
			classNameToMaxNumberMap.clear();
			classNameToEObjectToNumberMap.clear();
		}

		public static boolean isEnvironmentAssumption(Interaction msd) {
			for (Stereotype stereotype : msd.getAppliedStereotypes()) {
				if ("EnvironmentAssumption".equals(stereotype.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isEnvironmentAssumptionProcess(
				ActiveProcess process) {
			if (process instanceof ActiveMSD)
				return isEnvironmentAssumption(((ActiveMSD) process)
						.getInteraction());
			if (process instanceof ActiveMSS)
				return isEnvironmentAssumption(((ActiveMSS) process)
						.getStateMachine());
			return false;
		}

		public static boolean isEnvironmentAssumption(StateMachine mss) {
			for (Stereotype stereotype : mss.getAppliedStereotypes()) {
				if ("EnvironmentAssumptionStateMachine".equals(stereotype
						.getName())) {
					return true;
				}
			}
			return false;
		}

		public static Message getFirstMessageForMSD(Interaction msd) {
			for (InteractionFragment fragment : msd.getFragments()) {
				if (fragment instanceof MessageOccurrenceSpecification) {

					return ((MessageOccurrenceSpecification) fragment)
							.getMessage();
				}
			}
			return null;
		}

		public static Collection<Transition> getFirstTransitionsForMSS(
				StateMachine mss, RuntimeUtil util) {
			State initialState = util.getStateMachineToInitialStateMap().get(
					mss);
			return initialState.getOutgoings();
		}

		public static boolean isMessageUnifiable(
				MessageEvent concreteMessageEvent,
				MessageEvent diagramMessageEvent) {
			return concreteMessageEvent.equals(diagramMessageEvent)
					|| (concreteMessageEvent.getSendingObject().equals(
							diagramMessageEvent.getSendingObject())
							&& concreteMessageEvent.getReceivingObject()
									.equals(diagramMessageEvent
											.getReceivingObject()) && concreteMessageEvent
							.getOperation().equals(
									diagramMessageEvent.getOperation()));
		}

		public static boolean isParameterUnifiable(
				MessageEvent concreteMessageEvent,
				MessageEvent diagramMessageEvent,
				ActiveProcess msd) {

			if (!isMessageUnifiable(concreteMessageEvent, diagramMessageEvent))
				return false;
			else if (!diagramMessageEvent.isParameterized()
					|| !diagramMessageEvent.isConcrete())
				return true;
			else {
				Object argumentValue = null;
				if (diagramMessageEvent.getMessage() != null
						&& diagramMessageEvent.getMessage().getArguments()
								.size() != 0) {
					ValueSpecification valueSpecification = diagramMessageEvent
							.getMessage().getArguments().get(0);
					if (RuntimeUtil.Helper
							.isVariableReference(valueSpecification)) {
						String varName = (String) RuntimeUtil.Helper
								.getReferencedVariable(valueSpecification);
						argumentValue = msd.getVariableValue(varName);

					}
				}
				if (concreteMessageEvent.getEObjectParameterValue() != null) {
					return concreteMessageEvent.getEObjectParameterValue()
							.equals(argumentValue != null ? argumentValue
									: diagramMessageEvent
											.getEObjectParameterValue());
				} else if (concreteMessageEvent.isSetBooleanParameterValue()) {
					return concreteMessageEvent.isBooleanParameterValue() == (argumentValue != null ? (boolean) argumentValue
							: diagramMessageEvent.isBooleanParameterValue());
				} else if (concreteMessageEvent.isSetIntegerParameterValue()) {
					return concreteMessageEvent.getIntegerParameterValue() == (argumentValue != null ? (int) argumentValue
							: diagramMessageEvent.getIntegerParameterValue());
				} else if (concreteMessageEvent.isSetStringParameterValue()) {
					return concreteMessageEvent.getStringParameterValue()
							.equals(argumentValue != null ? argumentValue
									: diagramMessageEvent
											.getStringParameterValue());
				} else {
					Assert.isTrue(
							false,
							"\"concreteMessageEvent\" is not a concrete message event: "
									+ getMessageEventString(concreteMessageEvent));
					return false;
				}
			}
		}

		public static String getMessageEventString(MessageEvent messageEvent) {
			String returnString = messageEvent.getMessageName();
			if (messageEvent.isParameterized()) {
				returnString += "(";
				if (messageEvent.getEObjectParameterValue() != null) {
					returnString += getEObjectName(messageEvent
							.getEObjectParameterValue());
				} else if (messageEvent.isSetBooleanParameterValue()) {
					returnString += messageEvent.isBooleanParameterValue();
				} else if (messageEvent.isSetIntegerParameterValue()) {
					returnString += messageEvent.getIntegerParameterValue();
				} else if (messageEvent.isSetStringParameterValue()) {
					returnString += messageEvent.getStringParameterValue();
				} else {
					returnString += "?";
				}
				returnString += ")";
			} else {
				returnString += "()";
			}
			return returnString + " ["
					+ getEObjectName(messageEvent.getSendingObject()) + "->"
					+ getEObjectName(messageEvent.getReceivingObject()) + "]";
		}

		public static String getMSDModalMessageEventString(
				MSDModalMessageEvent msdModalMessageEvent) {

			MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent
					.getAssumptionsModality();
			MSDModality requirementsModality = (MSDModality) msdModalMessageEvent
					.getRequirementsModality();

			StringBuilder result = new StringBuilder();
			result.append(getMessageEventString(msdModalMessageEvent
					.getRepresentedMessageEvent()) + " -- ");
			result.append("assumptionHot: "
					+ String.valueOf(assumptionsModality.isHot()).toUpperCase()
					+ "; ");
			result.append("assumptionCold: "
					+ String.valueOf(assumptionsModality.isCold())
							.toUpperCase() + "; ");
			result.append("assumptionMonitored: "
					+ String.valueOf(assumptionsModality.isMonitored())
							.toUpperCase() + "; ");
			result.append("assumptionExecuted: "
					+ String.valueOf(assumptionsModality.isMandatory())
							.toUpperCase() + "; ");
			result.append("assumptionSafetyViolating: "
					+ String.valueOf(assumptionsModality.isSafetyViolating())
							.toUpperCase() + "; ");
			result.append("assumptionColdViolating: "
					+ String.valueOf(assumptionsModality.isColdViolating())
							.toUpperCase() + "; ");
			result.append("assumptionInitializing: "
					+ String.valueOf(assumptionsModality.isInitializing())
							.toUpperCase() + "; ");
			result.append("requirementHot: "
					+ String.valueOf(requirementsModality.isHot())
							.toUpperCase() + "; ");
			result.append("requirementCold: "
					+ String.valueOf(requirementsModality.isCold())
							.toUpperCase() + "; ");
			result.append("requirementMonitored: "
					+ String.valueOf(requirementsModality.isMonitored())
							.toUpperCase() + "; ");
			result.append("requirementExecuted: "
					+ String.valueOf(requirementsModality.isMandatory())
							.toUpperCase() + "; ");
			result.append("requirementSafetyViolating: "
					+ String.valueOf(requirementsModality.isSafetyViolating())
							.toUpperCase() + "; ");
			result.append("requirementColdViolating: "
					+ String.valueOf(requirementsModality.isColdViolating())
							.toUpperCase() + "; ");
			result.append("requirementInitializing: "
					+ String.valueOf(requirementsModality.isInitializing())
							.toUpperCase());
			return result.toString();
		}

		public static boolean isStereotypeValue(NamedElement element,
				String stereotypeName, String valueName, String cmpValue) {
			Stereotype modalMessage = null;
			for (Stereotype stereotype : element.getAppliedStereotypes()) {
				if (stereotypeName.equals(stereotype.getName())) {
					modalMessage = stereotype;
					break;
				}
			}

			if (modalMessage != null) {
				EnumerationLiteral enumLiteral = (EnumerationLiteral) element
						.getValue(modalMessage, valueName);
				if (cmpValue.equals(enumLiteral.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isHot(State state) {
			return isStereotypeValue(state, "ModalState", "temperature", "Hot");
		}

		public static boolean isExecuted(State state) {
			return isStereotypeValue(state, "ModalState", "execution",
					"Execute");
		}

		public static boolean isHot(Message message) {
			return isStereotypeValue(message, "ModalMessage", "temperature",
					"Hot");
		}

		public static boolean isExecuted(Message message) {
			return isStereotypeValue(message, "ModalMessage", "execution",
					"Execute");
		}

		public static boolean isForbidden(Message msg) {
			InteractionOperand enclosingOperand = ((MessageOccurrenceSpecification) msg
					.getSendEvent()).getEnclosingOperand();
			if (enclosingOperand != null) {
				if (enclosingOperand.getOwner() instanceof CombinedFragment) {
					CombinedFragment enclosingCombinedFragment = (CombinedFragment) enclosingOperand
							.getOwner();
					if (enclosingCombinedFragment.getInteractionOperator() == InteractionOperatorKind.NEG_LITERAL) {
						return true;
					}
				}
			}
			for (Stereotype stereotype : msg.getAppliedStereotypes()) {
				if ("Forbidden".equals(stereotype.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isAssignment(StateInvariant stateInvariant) {
			for (Stereotype stereotype : stateInvariant.getAppliedStereotypes()) {
				if ("Assignment".equals(stereotype.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isAssignment(CombinedFragment combinedFragment) {
			for (Stereotype stereotype : combinedFragment
					.getAppliedStereotypes()) {
				if ("Assignment".equals(stereotype.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isCondition(CombinedFragment combinedFragment) {
			for (Stereotype stereotype : combinedFragment
					.getAppliedStereotypes()) {
				if ("Condition".equals(stereotype.getName())) {
					return true;
				}
			}
			return false;
		}

		private static boolean hasStereotype(Element element,
				String stereotypeName) {
			return (element.getAppliedStereotype("Modal::" + stereotypeName) != null);
		}

		private static Object getStereotypeValue(Element element,
				String stereotypeName, String propertyName) {
			Stereotype stereotype = element.getAppliedStereotype("Modal::"
					+ stereotypeName);
			return stereotype == null ? null : element.getValue(stereotype,
					propertyName);
		}

		public static boolean isHot(CombinedFragment combinedFragment) {
			for (Stereotype stereotype : combinedFragment
					.getAppliedStereotypes()) {
				EnumerationLiteral enumLiteral = (EnumerationLiteral) combinedFragment
						.getValue(stereotype, "temperature");
				if (enumLiteral != null && "Hot".equals(enumLiteral.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isEnvironmentLifeline(Lifeline lifeline) {
			return isEnvironmentProperty((Property) lifeline.getRepresents());
		}

		public static boolean isEnvironmentMessage(Message message) {
			return isEnvironmentLifeline(((MessageOccurrenceSpecification) message
					.getSendEvent()).getCovereds().get(0));
		}

		public static boolean isEnvironmentTransition(Transition transition) {
			return isEnvironmentProperty(getSender(transition));
		}

		public static Property getSender(Transition transition) {
			Stereotype messageEventTransitionStereotype = transition
					.getAppliedStereotype("Modal::MessageEventTransition");
			return (Property) transition.getValue(
					messageEventTransitionStereotype, "sendingRole");
		}

		public static Property getReceiver(Transition transition) {
			Stereotype messageEventTransitionStereotype = transition
					.getAppliedStereotype("Modal::MessageEventTransition");
			return (Property) transition.getValue(
					messageEventTransitionStereotype, "receivingRole");
		}

		public static boolean isEnvironmentProperty(Property property) {
			Stereotype partKind = null;
			for (Stereotype stereotype : property.getAppliedStereotypes()) {
				if ("SpecificationPart".equals(stereotype.getName())) {
					partKind = stereotype;
					break;
				}
			}

			if (partKind != null) {
				EnumerationLiteral enumLiteral = (EnumerationLiteral) property
						.getValue(partKind, "partKind");
				if ("Environment".equals(enumLiteral.getName())) {
					return true;
				}
			}
			return false;
		}

		public static MessageOccurrenceSpecification getSendingMessageOccurrenceSpecification(
				Message message) {
			return (MessageOccurrenceSpecification) message.getSendEvent();
		}

		public static MessageOccurrenceSpecification getReceivingMessageOccurrenceSpecification(
				Message message) {
			return (MessageOccurrenceSpecification) message.getReceiveEvent();
		}

		public static Lifeline getSendingLifeline(Message message) {
			return (getSendingMessageOccurrenceSpecification(message))
					.getCovereds().get(0);
		}

		public static Lifeline getReceivingLifeline(Message message) {
			return (getReceivingMessageOccurrenceSpecification(message))
					.getCovereds().get(0);
		}

		public static Type getTypeOfSendingLifeline(Message message) {
			return ((Property) getSendingLifeline(message).getRepresents())
					.getType();
		}

		public static Type getTypeOfReceivingLifeline(Message message) {
			return ((Property) getReceivingLifeline(message).getRepresents())
					.getType();
		}

		public static String getEObjectsName(List<EObject> eObjects) {
			String returnString = "";
			for (EObject eObject : eObjects) {
				if (eObjects.get(0) == eObject) {
					returnString += "[" + getEObjectName(eObject);
				} else if (eObjects.get(eObjects.size() - 1) == eObject) {
					returnString += ", " + getEObjectName(eObject) + "]";
				} else {
					returnString += ", " + getEObjectName(eObject);
				}
			}
			return returnString;
		}

		public static String getEObjectsName(List<EObject> eObjects,
				int printMaximum) {
			String returnString = "[";
			int counter = 0;
			for (EObject eObject : eObjects) {
				if (counter == printMaximum) {
					if (printMaximum > 0) {
						return returnString + ", ...]";
					} else {
						return "[...]";
					}
				} else if (eObjects.get(0) == eObject) {
					returnString += getEObjectName(eObject);
				} else if (eObjects.get(eObjects.size() - 1) == eObject) {
					returnString += ", " + getEObjectName(eObject) + "]";
				} else {
					returnString += ", " + getEObjectName(eObject);
				}
				counter++;
			}
			return returnString;
		}

		public static String getEObjectName(EObject eObject) {
			for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
				if ("name".equals(eAttribute.getName())
						&& eAttribute.getEType().getInstanceClass() == String.class) {
					return (String) eObject.eGet(eAttribute);
				}
			}
			String className = eObject.eClass().getName();
			Map<String, Integer> eObjectURIToNumberMap = classNameToEObjectToNumberMap
					.get(className);
			if (eObjectURIToNumberMap == null) {
				eObjectURIToNumberMap = new HashMap<String, Integer>();
				classNameToEObjectToNumberMap.put(className,
						eObjectURIToNumberMap);
			}
			String uriString = EcoreUtil.getURI(eObject).toString();
			Integer objNum = eObjectURIToNumberMap.get(uriString);
			if (objNum == null) {
				Integer maxNum = classNameToMaxNumberMap.get(className);
				if (maxNum == null) {
					maxNum = -1;
				}
				objNum = maxNum + 1;
				classNameToMaxNumberMap.put(className, objNum);
				eObjectURIToNumberMap.put(uriString, objNum);
			}
			return className + (objNum != 0 ? objNum : "");
		}

		public static int getLowerboundMultiplicityOfLifeline(Lifeline lifeline) {
			return getRepresentedProperty(lifeline).getLower();
		}

		private static Property getRepresentedProperty(Lifeline lifeline) {
			ConnectableElement connectableElement = lifeline.getRepresents();
			Assert.isTrue(connectableElement instanceof Property,
					"The lifeline must represent a property.");
			return (Property) connectableElement;
		}

		public static int getUpperBoundMultiplicityOfLifeline(Lifeline lifeline) {
			return getRepresentedProperty(lifeline).getUpper();
		}

		public static String getLifelineName(Lifeline lifeline) {
			return getRepresentedProperty(lifeline).getName();
		}

		public static boolean isSelfMessage(Message message) {
			MessageOccurrenceSpecification sendingMOS = getSendingMessageOccurrenceSpecification(message);
			MessageOccurrenceSpecification receivingMOS = getReceivingMessageOccurrenceSpecification(message);

			return sendingMOS.getCovereds().iterator().next()
					.equals(receivingMOS.getCovereds().iterator().next());
		}

		public static String getMessageParameterExpression(Message message) {
			if (!message.getArguments().isEmpty()) {
				ValueSpecification valueSpecification = message.getArguments()
						.get(0);
				if (valueSpecification instanceof LiteralString) {
					LiteralString literalString = (LiteralString) valueSpecification;
					return literalString.getValue();
				}
			}
			return null;
		}

		public static Object getParameterValue(MessageEvent messageEvent) {
			RuntimeMessage runtimeMessage = messageEvent.getRuntimeMessage();
			if (runtimeMessage.isSetBooleanParameterValue())
				return runtimeMessage.isBooleanParameterValue();
			else if (runtimeMessage.isSetIntegerParameterValue())
				return runtimeMessage.getIntegerParameterValue();
			else if (runtimeMessage.isSetStringParameterValue())
				return runtimeMessage.getStringParameterValue();
			else if (runtimeMessage.isSetEObjectParameterValue())
				return runtimeMessage.getEObjectParameterValue();
			else
				return null;
		}

		public static boolean isValidOCLVariable(String s) {
			return s.matches("[a-zA-Z0-9]*");
		}

		public static String getActiveProcessesString(
				Collection<ActiveProcess> activeProcesses) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder
					.append(activeProcesses.size() + " active Process: \n");
			for (ActiveProcess activeProcess : activeProcesses) {
				stringBuilder.append(activeProcess + "  hashcode: "
						+ activeProcess.hashCode() + "\n");
				if (activeProcess instanceof ActiveMSD) {
					ActiveMSD activeMSD = (ActiveMSD) activeProcess;
					for (Lifeline lifeline : activeMSD.getInteraction()
							.getLifelines()) {
						stringBuilder.append("   "
								+ getLifelineName(lifeline)
								+ " : "
								+ activeMSD.getCurrentState()
										.getLifelineToInteractionFragmentMap()
										.get(lifeline) + "\n");
					}
				}
				if (activeProcess instanceof ActiveMSS) {
					ActiveMSS activeMSS = (ActiveMSS) activeProcess;
					stringBuilder.append(activeMSS.getCurrentState()
							.getUmlState());
				}
			}
			return stringBuilder.toString();
		}

		public static String getMSDRuntimeStateString(
				MSDRuntimeState msdRuntimeState) {
			return getActiveProcessesString(msdRuntimeState
					.getActiveProcesses());
		}

		public static int getInteractionFragmentNumberOnLifeline(
				InteractionFragment interactionFragment, Lifeline lifeline) {
			int fragmentOnLifelineCounter = -1;
			for (InteractionFragment fragment : lifeline.getInteraction()
					.getFragments()) {
				if (fragment.getCovereds().contains(lifeline))
					fragmentOnLifelineCounter++;
				if (interactionFragment == fragment)
					return fragmentOnLifelineCounter;
			}
			return -1;
		}

		// public static boolean isParameterEqual(MessageEvent messageEvent,
		// Object parameterValue) {
		// if (!messageEvent.isParameterized() && parameterValue == null)
		// return true;
		// if (messageEvent.isParameterized() && messageEvent.isConcrete()
		// && parameterValue == null)
		// return false;
		// if (!messageEvent.isConcrete() && parameterValue == null)
		// return true;
		// Assert.isNotNull(parameterValue);
		// if (parameterValue instanceof Boolean)
		// return messageEvent.isBooleanParameterValue() == (Boolean)
		// parameterValue;
		// if (parameterValue instanceof Integer)
		// return messageEvent.getIntegerParameterValue() == (Integer)
		// parameterValue;
		// if (parameterValue instanceof String)
		// return messageEvent.getStringParameterValue() == (String)
		// parameterValue;
		// if (parameterValue instanceof EObject)
		// return messageEvent.getEObjectParameterValue() == (EObject)
		// parameterValue;
		//
		// return false;
		// }

		public static boolean isDiagramMessageParameterUnifiableWithValue(
				MessageEvent messageEvent, Object parameterValue) {
			if (parameterValue == null)
				return true;
			Assert.isNotNull(parameterValue);
			if (parameterValue instanceof Boolean)
				return messageEvent.isBooleanParameterValue() == (Boolean) parameterValue;
			if (parameterValue instanceof Integer)
				return messageEvent.getIntegerParameterValue() == (Integer) parameterValue;
			if (parameterValue instanceof String)
				return messageEvent.getStringParameterValue() == (String) parameterValue;
			if (parameterValue instanceof EObject)
				return messageEvent.getEObjectParameterValue() == (EObject) parameterValue;

			return false;
		}

		public static boolean isNonSpontaneousEnvironmentReaction(
				Operation operation) {
			for (Stereotype stereotype : operation.getAppliedStereotypes()) {
				if ("NonSponteneousEnvironmentReaction".equals(stereotype
						.getName())) {
					return true;
				}
			}
			return false;
		}

		public static boolean isMessageEventTransition(Transition transition) {
			return (transition
					.getAppliedStereotype("Modal::MessageEventTransition") != null);
		}

		public static String getCombinedFragmentGuardString(
				CombinedFragment combinedFragment) {
			if (combinedFragment.getOperands().isEmpty()) {
				return null;
			}
			InteractionConstraint guard = combinedFragment.getOperands().get(0)
					.getGuard();
			if (guard == null) {
				return null;
			}
			ValueSpecification specification = guard.getSpecification();
			if (!(specification instanceof LiteralString)) {
				return null;
			}
			return ((LiteralString) specification).getValue();
		}

		public static boolean isVariableReference(
				ValueSpecification valueSpecification) {
			return hasStereotype(valueSpecification, "VariableReference");
		}

		public static Object getReferencedVariable(
				ValueSpecification valueSpecification) {
			return getStereotypeValue(valueSpecification, "VariableReference",
					"variable");
		}
	}

	public class MSDRuntimeStateCopier {

		EList<ActiveProcess> unchangedActiveProcessesEList;

		public EList<ActiveProcess> getUnchangedActiveMSDsEList() {
			return unchangedActiveProcessesEList;
		}

		MSDRuntimeState copiedMSDRuntimeState;
		Event copiedEvent;
		RuntimeMessage copiedRuntimeMessage;

		public MSDRuntimeState copy(MSDRuntimeState msdRuntimeState, Event event) {

			EList<EObject> objectsToCopy = new BasicEList<EObject>();

			objectsToCopy.add(msdRuntimeState);

			boolean supportDynamicObjectSystem = ((MSDRuntimeStateGraph) msdRuntimeState
					.getStateGraph()).getScenarioRunConfiguration()
					.isDynamicObjectSystem();

			if (supportDynamicObjectSystem) {
				objectsToCopy.add(msdRuntimeState.getObjectSystem());
				for (EObject rootEObject : msdRuntimeState.getObjectSystem()
						.getRootObjects()) {
					if (!msdRuntimeState.getObjectSystem()
							.getRootObjectsContained().contains(rootEObject))
						objectsToCopy.add(rootEObject);
				}
			}

			unchangedActiveProcessesEList = new BasicEList<ActiveProcess>();

			for (ActiveProcess activeProcess : msdRuntimeState
					.getActiveProcesses()) {
//				if (supportDynamicObjectSystem
//						|| !(event instanceof MessageEvent)
//						|| activeProcess.isRelavant((MessageEvent) event)
//						) {
					objectsToCopy.add(activeProcess);
					objectsToCopy.add(activeProcess.getCurrentState());
					if (activeProcess instanceof ActiveMSD)
						objectsToCopy.add(((ActiveMSD) activeProcess)
								.getLifelineBindings());
					if (activeProcess instanceof ActiveMSS) {
						objectsToCopy.add(((ActiveMSS) activeProcess)
								.getRoleBindings());
					}
					objectsToCopy.add(activeProcess.getVariableValuations());
//				} else {
//					unchangedActiveProcessesEList.add(activeProcess);
//				}
			}

			// should be already a registered message event that will be copied
			// anyways.
			// objectsToCopy.add(0, messageEvent);
			// objectsToCopy.add(1, messageEvent.getRuntimeMessage());

			// Collection<EObject> copiedObjects =
			// EcoreUtil.copyAll(objectsToCopy);
			// instead do this to avoid outgoing transitions to get copied.
			Copier copier = new Copier() {
				@Override
				protected void copyContainment(EReference eReference,
						EObject eObject, EObject copyEObject) {
					if (eReference != StategraphPackage.eINSTANCE
							.getState_OutgoingTransition())
						super.copyContainment(eReference, eObject, copyEObject);
				}
			};
			Collection<EObject> copiedObjects = copier.copyAll(objectsToCopy);
			copier.copyReferences();

			// if(event!=null)
			copiedEvent = (Event) copier.get(event);
			copiedMSDRuntimeState = (MSDRuntimeState) copier
					.get(msdRuntimeState);

			// ensure that each process only modifies its own copy of the variable valuations
			// (up to here all processes can point to the same one)
			for (ActiveProcess activeProcess : copiedMSDRuntimeState
					.getActiveProcesses()) {
				activeProcess.setVariableValuations(EcoreUtil.copy(activeProcess.getVariableValuations()));
			}

			if (supportDynamicObjectSystem) {
				for (EObject rootEObject : copiedMSDRuntimeState
						.getObjectSystem().getRootObjects()) {
					if (!copiedMSDRuntimeState.getObjectSystem()
							.getRootObjectsContained().contains(rootEObject))
						copiedMSDRuntimeState.getObjectSystem()
								.getRootObjectsContained().add(rootEObject);
				}

				// update synch message event registry.
				Collection<MessageEvent> registeredMessageEvents = new BasicEList<MessageEvent>(
						((MSDObjectSystem) copiedMSDRuntimeState
								.getObjectSystem())
								.getMessageEventKeyWrapperToMessageEventMap()
								.values());
				((MSDObjectSystem) copiedMSDRuntimeState.getObjectSystem())
						.getMessageEventKeyWrapperToMessageEventMap().clear();
				for (MessageEvent synchronousMessageEvent : registeredMessageEvents) {
					((MSDObjectSystem) copiedMSDRuntimeState.getObjectSystem())
							.getMessageEventKeyWrapperToMessageEventMap()
							.put(new MessageEventKeyWrapper(
									synchronousMessageEvent.getSendingObject(),
									synchronousMessageEvent.getOperation(),
									synchronousMessageEvent
											.getReceivingObject(),
									RuntimeUtil.Helper
											.getParameterValue(synchronousMessageEvent),
									synchronousMessageEvent.eClass()),
									synchronousMessageEvent);
				}

			}

			copiedMSDRuntimeState.getEventToTransitionMap().clear();

			return copiedMSDRuntimeState;
		}

		public MSDRuntimeState getCopiedMSDRuntimeState() {
			return copiedMSDRuntimeState;
		}

		public Event getCopiedEvent() {
			return copiedEvent;
		}

	}

	/**
	 * Returns the value of the '<em><b>Uml2ecore Mapping</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml2ecore Mapping</em>' containment reference
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml2ecore Mapping</em>' containment reference.
	 * @see #setUml2ecoreMapping(UML2EcoreMapping)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_Uml2ecoreMapping()
	 * @model containment="true"
	 * @generated
	 */
	UML2EcoreMapping getUml2ecoreMapping();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getUml2ecoreMapping <em>Uml2ecore Mapping</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml2ecore Mapping</em>' containment reference.
	 * @see #getUml2ecoreMapping()
	 * @generated
	 */
	void setUml2ecoreMapping(UML2EcoreMapping value);

	/**
	 * Returns the value of the '<em><b>Root UML Package To EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root UML Package To EPackage</em>' reference
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root UML Package To EPackage</em>' reference.
	 * @see #setRootUMLPackageToEPackage(UmlPackage2EPackage)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_RootUMLPackageToEPackage()
	 * @model
	 * @generated
	 */
	UmlPackage2EPackage getRootUMLPackageToEPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root UML Package To EPackage</em>' reference.
	 * @see #getRootUMLPackageToEPackage()
	 * @generated
	 */
	void setRootUMLPackageToEPackage(UmlPackage2EPackage value);

	/**
	 * Returns the value of the '
	 * <em><b>First Message Operation To Interaction Map</b></em>' map. The key
	 * is of type {@link org.eclipse.uml2.uml.Operation}, and the value is of
	 * type list of {@link org.eclipse.uml2.uml.Interaction}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '
	 * <em>First Message Operation To Interaction Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>First Message Operation To Interaction Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_FirstMessageOperationToInteractionMap()
	 * @model mapType=
	 *        "org.scenariotools.msd.util.FirstMessageOperationToInteractionMapEntry<org.eclipse.uml2.uml.Operation, org.eclipse.uml2.uml.Interaction>"
	 * @generated
	 */
	EMap<Operation, EList<Interaction>> getFirstMessageOperationToInteractionMap();

	/**
	 * Returns the value of the '
	 * <em><b>First Transition Operation To State Machine Map</b></em>' map. The
	 * key is of type {@link org.eclipse.uml2.uml.Operation}, and the value is
	 * of type list of {@link org.eclipse.uml2.uml.StateMachine}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '
	 * <em>First Transition Operation To State Machine Map</em>' map isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>First Transition Operation To State Machine Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_FirstTransitionOperationToStateMachineMap()
	 * @model mapType=
	 *        "org.scenariotools.msd.util.FirstTransitionOperationToStateMachineMapEntry<org.eclipse.uml2.uml.Operation, org.eclipse.uml2.uml.StateMachine>"
	 * @generated
	 */
	EMap<Operation, EList<StateMachine>> getFirstTransitionOperationToStateMachineMap();

	/**
	 * Returns the value of the '<em><b>Interaction To String To Lifeline Map Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Interaction},
	 * and the value is of type list of {@link java.util.Map.Entry<java.lang.String, org.eclipse.uml2.uml.Lifeline>},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction To String To Lifeline Map Map</em>
	 * ' map isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction To String To Lifeline Map Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_InteractionToStringToLifelineMapMap()
	 * @model mapType="org.scenariotools.msd.util.InteractionToStringToLifelineMapMapEntry<org.eclipse.uml2.uml.Interaction, org.scenariotools.msd.util.StringToLifelineMapEntry>"
	 * @generated
	 */
	EMap<Interaction, EMap<String, Lifeline>> getInteractionToStringToLifelineMapMap();

	/**
	 * Returns the value of the '<em><b>Lifeline To Interaction Fragments Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Lifeline},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.InteractionFragment},
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Lifeline To Interaction Fragments Map</em>'
	 * map isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lifeline To Interaction Fragments Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_LifelineToInteractionFragmentsMap()
	 * @model mapType="org.scenariotools.msd.util.LifelineToInteractionFragmentsMapEntry<org.eclipse.uml2.uml.Lifeline, org.eclipse.uml2.uml.InteractionFragment>"
	 * @generated
	 */
	EMap<Lifeline, EList<InteractionFragment>> getLifelineToInteractionFragmentsMap();

	/**
	 * Returns the value of the '<em><b>System Classes</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Class}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Classes</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>System Classes</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_SystemClasses()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Class> getSystemClasses();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>' from the '<em><b>System Classes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getSystemClasses()
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getSystemClasses(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>' from the '<em><b>System Classes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getSystemClasses()
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getSystemClasses(String name,
			boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Environment Classes</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Classes</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Classes</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_EnvironmentClasses()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Class> getEnvironmentClasses();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>' from the '<em><b>Environment Classes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentClasses()
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getEnvironmentClasses(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>' from the '<em><b>Environment Classes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Class} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Class} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentClasses()
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getEnvironmentClasses(String name,
			boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Uml Packages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Package}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Packages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Uml Packages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_UmlPackages()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Package> getUmlPackages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Uml Packages</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUmlPackages()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUmlPackages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Uml Packages</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUmlPackages()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUmlPackages(String name,
			boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Package To Collaborations Map</b></em>'
	 * map. The key is of type {@link org.eclipse.uml2.uml.Package}, and the
	 * value is of type list of {@link org.eclipse.uml2.uml.Collaboration}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package To Collaborations Map</em>' map isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Package To Collaborations Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_PackageToCollaborationsMap()
	 * @model mapType=
	 *        "org.scenariotools.msd.util.PackageToCollaborationMapEntry<org.eclipse.uml2.uml.Package, org.eclipse.uml2.uml.Collaboration>"
	 * @generated
	 */
	EMap<org.eclipse.uml2.uml.Package, EList<Collaboration>> getPackageToCollaborationsMap();

	/**
	 * Returns the value of the '<em><b>Interactions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Interaction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interactions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interactions</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_Interactions()
	 * @model
	 * @generated
	 */
	EList<Interaction> getInteractions();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Interaction} with the specified '<em><b>Name</b></em>' from the '<em><b>Interactions</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Interaction} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Interaction} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getInteractions()
	 * @generated
	 */
	Interaction getInteractions(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Interaction} with the specified '<em><b>Name</b></em>' from the '<em><b>Interactions</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Interaction} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Interaction} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getInteractions()
	 * @generated
	 */
	Interaction getInteractions(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>Integrated Package</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * this is the package that merges all other packages <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Integrated Package</em>' reference.
	 * @see #setIntegratedPackage(org.eclipse.uml2.uml.Package)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_IntegratedPackage()
	 * @model
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getIntegratedPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getIntegratedPackage <em>Integrated Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Integrated Package</em>' reference.
	 * @see #getIntegratedPackage()
	 * @generated
	 */
	void setIntegratedPackage(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Environment Sendable Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Sendable Messages</em>' reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Sendable Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_EnvironmentSendableMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getEnvironmentSendableMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the
	 * specified '<em><b>Name</b></em>' from the '
	 * <em><b>Environment Sendable Messages</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name
	 *            The '<em><b>Name</b></em>' of the
	 *            {@link org.eclipse.uml2.uml.Message} to retrieve, or
	 *            <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified
	 *         '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentSendableMessages()
	 * @generated
	 */
	Message getEnvironmentSendableMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the
	 * specified '<em><b>Name</b></em>' from the '
	 * <em><b>Environment Sendable Messages</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name
	 *            The '<em><b>Name</b></em>' of the
	 *            {@link org.eclipse.uml2.uml.Message} to retrieve, or
	 *            <code>null</code>.
	 * @param ignoreCase
	 *            Whether to ignore case in {@link java.lang.String}
	 *            comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified
	 *         '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentSendableMessages()
	 * @generated
	 */
	Message getEnvironmentSendableMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>System Sendable Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Sendable Messages</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Sendable Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_SystemSendableMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getSystemSendableMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the
	 * specified '<em><b>Name</b></em>' from the '
	 * <em><b>System Sendable Messages</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name
	 *            The '<em><b>Name</b></em>' of the
	 *            {@link org.eclipse.uml2.uml.Message} to retrieve, or
	 *            <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified
	 *         '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getSystemSendableMessages()
	 * @generated
	 */
	Message getSystemSendableMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the
	 * specified '<em><b>Name</b></em>' from the '
	 * <em><b>System Sendable Messages</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name
	 *            The '<em><b>Name</b></em>' of the
	 *            {@link org.eclipse.uml2.uml.Message} to retrieve, or
	 *            <code>null</code>.
	 * @param ignoreCase
	 *            Whether to ignore case in {@link java.lang.String}
	 *            comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified
	 *         '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getSystemSendableMessages()
	 * @generated
	 */
	Message getSystemSendableMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '
	 * <em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Package}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Case Specifications To Be Considered</em>'
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Use Case Specifications To Be Considered</em>' reference
	 *         list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_UseCaseSpecificationsToBeConsidered()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Package> getUseCaseSpecificationsToBeConsidered();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(
			String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(
			String name, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Ocl Registry</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ocl Registry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ocl Registry</em>' reference.
	 * @see #setOclRegistry(OCLRegistry)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_OclRegistry()
	 * @model
	 * @generated
	 */
	OCLRegistry getOclRegistry();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getOclRegistry <em>Ocl Registry</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Ocl Registry</em>' reference.
	 * @see #getOclRegistry()
	 * @generated
	 */
	void setOclRegistry(OCLRegistry value);

	/**
	 * Returns the value of the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Runtime State Graph</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #setMsdRuntimeStateGraph(MSDRuntimeStateGraph)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_MsdRuntimeStateGraph()
	 * @see org.scenariotools.msd.runtime.MSDRuntimeStateGraph#getRuntimeUtil
	 * @model opposite="runtimeUtil"
	 * @generated
	 */
	MSDRuntimeStateGraph getMsdRuntimeStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 */
	void setMsdRuntimeStateGraph(MSDRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Final Fragment</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Fragment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Final Fragment</em>' reference.
	 * @see #setFinalFragment(InteractionFragment)
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_FinalFragment()
	 * @model
	 * @generated
	 */
	InteractionFragment getFinalFragment();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.RuntimeUtil#getFinalFragment <em>Final Fragment</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Final Fragment</em>' reference.
	 * @see #getFinalFragment()
	 * @generated
	 */
	void setFinalFragment(InteractionFragment value);

	/**
	 * Returns the value of the '<em><b>Interaction To MSD Util Map</b></em>'
	 * map. The key is of type {@link org.eclipse.uml2.uml.Interaction}, and the
	 * value is of type {@link org.scenariotools.msd.util.MSDUtil}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction To MSD Util Map</em>' map isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Interaction To MSD Util Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_InteractionToMSDUtilMap()
	 * @model mapType=
	 *        "org.scenariotools.msd.util.InteractionToMSDUtilMapEntry<org.eclipse.uml2.uml.Interaction, org.scenariotools.msd.util.MSDUtil>"
	 * @generated
	 */
	EMap<Interaction, MSDUtil> getInteractionToMSDUtilMap();

	/**
	 * Returns the value of the '<em><b>Interaction Operand To Lifeline To Interaction Fragments Map Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.InteractionOperand},
	 * and the value is of type list of {@link java.util.Map.Entry<org.eclipse.uml2.uml.Lifeline, org.eclipse.emf.common.util.EList<org.eclipse.uml2.uml.InteractionFragment>>},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '
	 * <em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>
	 * ' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_InteractionOperandToLifelineToInteractionFragmentsMapMap()
	 * @model mapType="org.scenariotools.msd.util.InteractionOperandToLifelineToInteractionFragmentsMapEntry<org.eclipse.uml2.uml.InteractionOperand, org.scenariotools.msd.util.LifelineToInteractionFragmentsMapEntry>"
	 * @generated
	 */
	EMap<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> getInteractionOperandToLifelineToInteractionFragmentsMapMap();

	/**
	 * Returns the value of the '<em><b>State Machines</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.StateMachine}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Machines</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machines</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_StateMachines()
	 * @model
	 * @generated
	 */
	EList<StateMachine> getStateMachines();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.StateMachine} with the specified '<em><b>Name</b></em>' from the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.StateMachine} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.StateMachine} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getStateMachines()
	 * @generated
	 */
	StateMachine getStateMachines(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.StateMachine} with the specified '<em><b>Name</b></em>' from the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.StateMachine} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.StateMachine} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.StateMachine} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getStateMachines()
	 * @generated
	 */
	StateMachine getStateMachines(String name, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>State Machine To Initial State Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.StateMachine},
	 * and the value is of type {@link org.eclipse.uml2.uml.State},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Machine To Initial State Map</em>' map
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machine To Initial State Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getRuntimeUtil_StateMachineToInitialStateMap()
	 * @model mapType="org.scenariotools.msd.util.StateMachineToInitialStateMapEntry<org.eclipse.uml2.uml.StateMachine, org.eclipse.uml2.uml.State>"
	 * @generated
	 */
	EMap<StateMachine, State> getStateMachineToInitialStateMap();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * this operation initializes - the UMLClassToEClassMap - the
	 * EClassToUMLClassMap
	 * 
	 * 
	 * <!-- end-model-doc -->
	 * 
	 * @model useCaseSpecificationsToBeConsideredMany="false"
	 * @generated
	 */
	void init(
			UmlPackage2EPackage rootUmlPackage2EPackage,
			EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EOperation getOperation(Message message);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EOperation getOperation(Transition transition);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature(Message message);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature(Transition transition);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isEnvironmentRuntimeMessage(RuntimeMessage runtimeMessage);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isEnvironmentMessageEvent(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.msd.util.PlainJavaObject"
	 * @generated
	 */
	Object getParameterValue(Message message);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.msd.util.PlainJavaObject" oclForCutDataType="org.scenariotools.msd.util.OCL"
	 * @generated
	 */
	Object getParameterValue(Message message, OCL oclForCut);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	Object getParameterValue(Message message, ObjectSystem objectSystem);
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isNonSpontaneousEnvironmentMessageEvent(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * this operation initializes - the UMLClassToEClassMap - the
	 * EClassToUMLClassMap
	 * 
	 * 
	 * <!-- end-model-doc -->
	 * 
	 * @model useCaseSpecificationsToBeConsideredMany="false"
	 * @generated
	 */
	void init(
			UmlPackage2EPackage rootUmlPackage2EPackage,
			EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered,
			org.eclipse.uml2.uml.Package strategyPackage);

	/**
	 * @generated NOT
	 */
	MessageEvent createMessageEvent(EObject sendingObject,
			EObject receivingObject);
} // RuntimeUtil
