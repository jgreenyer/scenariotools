/**
 */
package org.scenariotools.msd.util;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.util.UtilPackage
 * @generated
 */
public interface UtilFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UtilFactory eINSTANCE = org.scenariotools.msd.util.impl.UtilFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Runtime Util</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Runtime Util</em>'.
	 * @generated
	 */
	RuntimeUtil createRuntimeUtil();

	/**
	 * Returns a new object of class '<em>UML2 Ecore Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UML2 Ecore Mapping</em>'.
	 * @generated
	 */
	UML2EcoreMapping createUML2EcoreMapping();

	/**
	 * Returns a new object of class '<em>OCL Registry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Registry</em>'.
	 * @generated
	 */
	OCLRegistry createOCLRegistry();

	/**
	 * Returns a new object of class '<em>MSD Util</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSD Util</em>'.
	 * @generated
	 */
	MSDUtil createMSDUtil();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UtilPackage getUtilPackage();

} //UtilFactory
