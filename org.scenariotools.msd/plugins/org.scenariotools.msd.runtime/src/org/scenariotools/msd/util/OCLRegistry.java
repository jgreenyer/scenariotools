/**
 */
package org.scenariotools.msd.util;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ecore.OCL;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveMSD;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OCL Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.OCLRegistry#getIdToOCLMap <em>Id To OCL Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.util.UtilPackage#getOCLRegistry()
 * @model
 * @generated
 */
public interface OCLRegistry extends EObject {
	/**
	 * Returns the value of the '<em><b>Id To OCL Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link org.eclipse.ocl.ecore.OCL},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id To OCL Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id To OCL Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getOCLRegistry_IdToOCLMap()
	 * @model mapType="org.scenariotools.msd.util.IDToOCLMapEntry<org.eclipse.emf.ecore.EString, org.scenariotools.msd.util.OCL>"
	 * @generated
	 */
	EMap<String, OCL> getIdToOCLMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void disposeOCLForActiveMSD(ActiveProcess activeMSD);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.msd.util.OCL"
	 * @generated
	 */
	OCL getOCLForActiveMSD(ActiveProcess activeMSD);

} // OCLRegistry
