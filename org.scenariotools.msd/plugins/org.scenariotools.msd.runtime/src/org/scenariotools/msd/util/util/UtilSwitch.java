/**
 */
package org.scenariotools.msd.util.util;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.scenariotools.msd.util.*;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.util.UtilPackage
 * @generated
 */
public class UtilSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UtilPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilSwitch() {
		if (modelPackage == null) {
			modelPackage = UtilPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UtilPackage.RUNTIME_UTIL: {
				RuntimeUtil runtimeUtil = (RuntimeUtil)theEObject;
				T result = caseRuntimeUtil(runtimeUtil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.UML2_ECORE_MAPPING: {
				UML2EcoreMapping uml2EcoreMapping = (UML2EcoreMapping)theEObject;
				T result = caseUML2EcoreMapping(uml2EcoreMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.UML_CLASS_TO_ECLASS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<org.eclipse.uml2.uml.Class, EClass> umlClassToEClassMapEntry = (Map.Entry<org.eclipse.uml2.uml.Class, EClass>)theEObject;
				T result = caseUMLClassToEClassMapEntry(umlClassToEClassMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.ECLASS_TO_UML_CLASS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EClass, EList<org.eclipse.uml2.uml.Class>> eClassToUMLClassMapEntry = (Map.Entry<EClass, EList<org.eclipse.uml2.uml.Class>>)theEObject;
				T result = caseEClassToUMLClassMapEntry(eClassToUMLClassMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.EOPERATION2_UML_OPERATION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EOperation, EList<Operation>> eOperation2UMLOperationMapEntry = (Map.Entry<EOperation, EList<Operation>>)theEObject;
				T result = caseEOperation2UMLOperationMapEntry(eOperation2UMLOperationMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.UML_OPERATION_TO_EOPERATION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Operation, EOperation> umlOperationToEOperationMapEntry = (Map.Entry<Operation, EOperation>)theEObject;
				T result = caseUMLOperationToEOperationMapEntry(umlOperationToEOperationMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EParameter, EList<Parameter>> eParameterToUMLParameterMapEntry = (Map.Entry<EParameter, EList<Parameter>>)theEObject;
				T result = caseEParameterToUMLParameterMapEntry(eParameterToUMLParameterMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Parameter, EParameter> umlParameterToEParameterMapEntry = (Map.Entry<Parameter, EParameter>)theEObject;
				T result = caseUMLParameterToEParameterMapEntry(umlParameterToEParameterMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EStructuralFeature, EList<Operation>> eStructuralFeatureToUMLOperationMapEntry = (Map.Entry<EStructuralFeature, EList<Operation>>)theEObject;
				T result = caseEStructuralFeatureToUMLOperationMapEntry(eStructuralFeatureToUMLOperationMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Operation, EStructuralFeature> umlOperationToEStructuralFeatureMapEntry = (Map.Entry<Operation, EStructuralFeature>)theEObject;
				T result = caseUMLOperationToEStructuralFeatureMapEntry(umlOperationToEStructuralFeatureMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.STRING_TO_LIFELINE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, Lifeline> stringToLifelineMapEntry = (Map.Entry<String, Lifeline>)theEObject;
				T result = caseStringToLifelineMapEntry(stringToLifelineMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Operation, EList<Interaction>> firstMessageOperationToInteractionMapEntry = (Map.Entry<Operation, EList<Interaction>>)theEObject;
				T result = caseFirstMessageOperationToInteractionMapEntry(firstMessageOperationToInteractionMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Operation, EList<StateMachine>> firstTransitionOperationToStateMachineMapEntry = (Map.Entry<Operation, EList<StateMachine>>)theEObject;
				T result = caseFirstTransitionOperationToStateMachineMapEntry(firstTransitionOperationToStateMachineMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Lifeline, EList<InteractionFragment>> lifelineToInteractionFragmentsMapEntry = (Map.Entry<Lifeline, EList<InteractionFragment>>)theEObject;
				T result = caseLifelineToInteractionFragmentsMapEntry(lifelineToInteractionFragmentsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.PACKAGE_TO_COLLABORATION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<org.eclipse.uml2.uml.Package, EList<Collaboration>> packageToCollaborationMapEntry = (Map.Entry<org.eclipse.uml2.uml.Package, EList<Collaboration>>)theEObject;
				T result = casePackageToCollaborationMapEntry(packageToCollaborationMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Interaction, EMap<String, Lifeline>> interactionToStringToLifelineMapMapEntry = (Map.Entry<Interaction, EMap<String, Lifeline>>)theEObject;
				T result = caseInteractionToStringToLifelineMapMapEntry(interactionToStringToLifelineMapMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.PROPERTY_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Property, EObject> propertyToEObjectMapEntry = (Map.Entry<Property, EObject>)theEObject;
				T result = casePropertyToEObjectMapEntry(propertyToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.OCL_REGISTRY: {
				OCLRegistry oclRegistry = (OCLRegistry)theEObject;
				T result = caseOCLRegistry(oclRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.MSD_UTIL: {
				MSDUtil msdUtil = (MSDUtil)theEObject;
				T result = caseMSDUtil(msdUtil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.INTERACTION_TO_MSD_UTIL_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Interaction, MSDUtil> interactionToMSDUtilMapEntry = (Map.Entry<Interaction, MSDUtil>)theEObject;
				T result = caseInteractionToMSDUtilMapEntry(interactionToMSDUtilMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> interactionOperandToLifelineToInteractionFragmentsMapEntry = (Map.Entry<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>>)theEObject;
				T result = caseInteractionOperandToLifelineToInteractionFragmentsMapEntry(interactionOperandToLifelineToInteractionFragmentsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<StateMachine, State> stateMachineToInitialStateMapEntry = (Map.Entry<StateMachine, State>)theEObject;
				T result = caseStateMachineToInitialStateMapEntry(stateMachineToInitialStateMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilPackage.ID_TO_OCL_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, OCL> idToOCLMapEntry = (Map.Entry<String, OCL>)theEObject;
				T result = caseIDToOCLMapEntry(idToOCLMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Runtime Util</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Runtime Util</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeUtil(RuntimeUtil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML2 Ecore Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML2 Ecore Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUML2EcoreMapping(UML2EcoreMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Class To EClass Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Class To EClass Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLClassToEClassMapEntry(Map.Entry<org.eclipse.uml2.uml.Class, EClass> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass To UML Class Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass To UML Class Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassToUMLClassMapEntry(Map.Entry<EClass, EList<org.eclipse.uml2.uml.Class>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EOperation2 UML Operation Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOperation2 UML Operation Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOperation2UMLOperationMapEntry(Map.Entry<EOperation, EList<Operation>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Operation To EOperation Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Operation To EOperation Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLOperationToEOperationMapEntry(Map.Entry<Operation, EOperation> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EParameter To UML Parameter Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EParameter To UML Parameter Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEParameterToUMLParameterMapEntry(Map.Entry<EParameter, EList<Parameter>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Parameter To EParameter Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Parameter To EParameter Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLParameterToEParameterMapEntry(Map.Entry<Parameter, EParameter> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EStructural Feature To UML Operation Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EStructural Feature To UML Operation Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEStructuralFeatureToUMLOperationMapEntry(Map.Entry<EStructuralFeature, EList<Operation>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Operation To EStructural Feature Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Operation To EStructural Feature Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLOperationToEStructuralFeatureMapEntry(Map.Entry<Operation, EStructuralFeature> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Lifeline Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Lifeline Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToLifelineMapEntry(Map.Entry<String, Lifeline> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>First Message Operation To Interaction Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>First Message Operation To Interaction Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFirstMessageOperationToInteractionMapEntry(Map.Entry<Operation, EList<Interaction>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>First Transition Operation To State Machine Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>First Transition Operation To State Machine Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFirstTransitionOperationToStateMachineMapEntry(Map.Entry<Operation, EList<StateMachine>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lifeline To Interaction Fragments Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lifeline To Interaction Fragments Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLifelineToInteractionFragmentsMapEntry(Map.Entry<Lifeline, EList<InteractionFragment>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package To Collaboration Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package To Collaboration Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageToCollaborationMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, EList<Collaboration>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interaction To String To Lifeline Map Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interaction To String To Lifeline Map Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteractionToStringToLifelineMapMapEntry(Map.Entry<Interaction, EMap<String, Lifeline>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyToEObjectMapEntry(Map.Entry<Property, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OCL Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OCL Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOCLRegistry(OCLRegistry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSD Util</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSD Util</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSDUtil(MSDUtil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interaction To MSD Util Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interaction To MSD Util Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteractionToMSDUtilMapEntry(Map.Entry<Interaction, MSDUtil> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteractionOperandToLifelineToInteractionFragmentsMapEntry(Map.Entry<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Machine To Initial State Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Machine To Initial State Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateMachineToInitialStateMapEntry(Map.Entry<StateMachine, State> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ID To OCL Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ID To OCL Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDToOCLMapEntry(Map.Entry<String, OCL> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UtilSwitch
