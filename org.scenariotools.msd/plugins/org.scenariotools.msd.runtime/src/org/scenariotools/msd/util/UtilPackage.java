/**
 */
package org.scenariotools.msd.util;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.util.UtilFactory
 * @model kind="package"
 * @generated
 */
public interface UtilPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "util";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.runtime.util";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "util";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UtilPackage eINSTANCE = org.scenariotools.msd.util.impl.UtilPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl <em>Runtime Util</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.RuntimeUtilImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getRuntimeUtil()
	 * @generated
	 */
	int RUNTIME_UTIL = 0;

	/**
	 * The feature id for the '<em><b>Uml2ecore Mapping</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__UML2ECORE_MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Root UML Package To EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE = 1;

	/**
	 * The feature id for the '<em><b>First Message Operation To Interaction Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP = 2;

	/**
	 * The feature id for the '<em><b>First Transition Operation To State Machine Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP = 3;

	/**
	 * The feature id for the '<em><b>Interaction To String To Lifeline Map Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP = 4;

	/**
	 * The feature id for the '<em><b>Lifeline To Interaction Fragments Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP = 5;

	/**
	 * The feature id for the '<em><b>System Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__SYSTEM_CLASSES = 6;

	/**
	 * The feature id for the '<em><b>Environment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__ENVIRONMENT_CLASSES = 7;

	/**
	 * The feature id for the '<em><b>Uml Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__UML_PACKAGES = 8;

	/**
	 * The feature id for the '<em><b>Package To Collaborations Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP = 9;

	/**
	 * The feature id for the '<em><b>Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__INTERACTIONS = 10;

	/**
	 * The feature id for the '<em><b>Integrated Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__INTEGRATED_PACKAGE = 11;

	/**
	 * The feature id for the '<em><b>Environment Sendable Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES = 12;

	/**
	 * The feature id for the '<em><b>System Sendable Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES = 13;

	/**
	 * The feature id for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = 14;

	/**
	 * The feature id for the '<em><b>Ocl Registry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__OCL_REGISTRY = 15;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH = 16;

	/**
	 * The feature id for the '<em><b>Final Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__FINAL_FRAGMENT = 17;

	/**
	 * The feature id for the '<em><b>Interaction To MSD Util Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP = 18;

	/**
	 * The feature id for the '<em><b>Interaction Operand To Lifeline To Interaction Fragments Map Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP = 19;

	/**
	 * The feature id for the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__STATE_MACHINES = 20;

	/**
	 * The feature id for the '<em><b>State Machine To Initial State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP = 21;

	/**
	 * The number of structural features of the '<em>Runtime Util</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_UTIL_FEATURE_COUNT = 22;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl <em>UML2 Ecore Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.UML2EcoreMappingImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUML2EcoreMapping()
	 * @generated
	 */
	int UML2_ECORE_MAPPING = 1;

	/**
	 * The feature id for the '<em><b>Uml Class To EClass Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP = 0;

	/**
	 * The feature id for the '<em><b>EClass To UML Class Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP = 1;

	/**
	 * The feature id for the '<em><b>EOperation2 UML Operation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP = 2;

	/**
	 * The feature id for the '<em><b>Uml Operation To EOperation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP = 3;

	/**
	 * The feature id for the '<em><b>EParameter To UML Parameter Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP = 4;

	/**
	 * The feature id for the '<em><b>Uml Parameter To EParameter Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP = 5;

	/**
	 * The feature id for the '<em><b>Uml Operation To EStructural Feature Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP = 6;

	/**
	 * The feature id for the '<em><b>EStructural Feature To UML Operation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP = 7;

	/**
	 * The feature id for the '<em><b>Root UML Package To EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE = 8;

	/**
	 * The feature id for the '<em><b>Merged UML Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING__MERGED_UML_PACKAGES = 9;

	/**
	 * The number of structural features of the '<em>UML2 Ecore Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML2_ECORE_MAPPING_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.UMLClassToEClassMapEntryImpl <em>UML Class To EClass Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.UMLClassToEClassMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLClassToEClassMapEntry()
	 * @generated
	 */
	int UML_CLASS_TO_ECLASS_MAP_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_TO_ECLASS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_TO_ECLASS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>UML Class To EClass Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_TO_ECLASS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.EClassToUMLClassMapEntryImpl <em>EClass To UML Class Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.EClassToUMLClassMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEClassToUMLClassMapEntry()
	 * @generated
	 */
	int ECLASS_TO_UML_CLASS_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_UML_CLASS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_UML_CLASS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EClass To UML Class Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_UML_CLASS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.EOperation2UMLOperationMapEntryImpl <em>EOperation2 UML Operation Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.EOperation2UMLOperationMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEOperation2UMLOperationMapEntry()
	 * @generated
	 */
	int EOPERATION2_UML_OPERATION_MAP_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOPERATION2_UML_OPERATION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOPERATION2_UML_OPERATION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EOperation2 UML Operation Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOPERATION2_UML_OPERATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.UMLOperationToEOperationMapEntryImpl <em>UML Operation To EOperation Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.UMLOperationToEOperationMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLOperationToEOperationMapEntry()
	 * @generated
	 */
	int UML_OPERATION_TO_EOPERATION_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_EOPERATION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_EOPERATION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>UML Operation To EOperation Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_EOPERATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.EParameterToUMLParameterMapEntryImpl <em>EParameter To UML Parameter Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.EParameterToUMLParameterMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEParameterToUMLParameterMapEntry()
	 * @generated
	 */
	int EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY = 6;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EParameter To UML Parameter Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.UMLParameterToEParameterMapEntryImpl <em>UML Parameter To EParameter Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.UMLParameterToEParameterMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLParameterToEParameterMapEntry()
	 * @generated
	 */
	int UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY = 7;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>UML Parameter To EParameter Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.EStructuralFeatureToUMLOperationMapEntryImpl <em>EStructural Feature To UML Operation Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.EStructuralFeatureToUMLOperationMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEStructuralFeatureToUMLOperationMapEntry()
	 * @generated
	 */
	int ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EStructural Feature To UML Operation Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.UMLOperationToEStructuralFeatureMapEntryImpl <em>UML Operation To EStructural Feature Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.UMLOperationToEStructuralFeatureMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLOperationToEStructuralFeatureMapEntry()
	 * @generated
	 */
	int UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY = 9;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>UML Operation To EStructural Feature Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.StringToLifelineMapEntryImpl <em>String To Lifeline Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.StringToLifelineMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getStringToLifelineMapEntry()
	 * @generated
	 */
	int STRING_TO_LIFELINE_MAP_ENTRY = 10;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_LIFELINE_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_LIFELINE_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>String To Lifeline Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_LIFELINE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.FirstMessageOperationToInteractionMapEntryImpl <em>First Message Operation To Interaction Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.FirstMessageOperationToInteractionMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getFirstMessageOperationToInteractionMapEntry()
	 * @generated
	 */
	int FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY = 11;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>First Message Operation To Interaction Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.FirstTransitionOperationToStateMachineMapEntryImpl <em>First Transition Operation To State Machine Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.FirstTransitionOperationToStateMachineMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getFirstTransitionOperationToStateMachineMapEntry()
	 * @generated
	 */
	int FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>First Transition Operation To State Machine Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.LifelineToInteractionFragmentsMapEntryImpl <em>Lifeline To Interaction Fragments Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.LifelineToInteractionFragmentsMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY = 13;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Lifeline To Interaction Fragments Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.PackageToCollaborationMapEntryImpl <em>Package To Collaboration Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.PackageToCollaborationMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPackageToCollaborationMapEntry()
	 * @generated
	 */
	int PACKAGE_TO_COLLABORATION_MAP_ENTRY = 14;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_COLLABORATION_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_COLLABORATION_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Package To Collaboration Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_TO_COLLABORATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.InteractionToStringToLifelineMapMapEntryImpl <em>Interaction To String To Lifeline Map Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.InteractionToStringToLifelineMapMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionToStringToLifelineMapMapEntry()
	 * @generated
	 */
	int INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY = 15;

	/**
	 * The feature id for the '<em><b>Value</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Interaction To String To Lifeline Map Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.PropertyToEObjectMapEntryImpl <em>Property To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.PropertyToEObjectMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPropertyToEObjectMapEntry()
	 * @generated
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY = 16;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY = 1;

	/**
	 * The number of structural features of the '<em>Property To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.OCLRegistryImpl <em>OCL Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.OCLRegistryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getOCLRegistry()
	 * @generated
	 */
	int OCL_REGISTRY = 17;

	/**
	 * The feature id for the '<em><b>Id To OCL Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_REGISTRY__ID_TO_OCL_MAP = 0;

	/**
	 * The number of structural features of the '<em>OCL Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_REGISTRY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.MSDUtilImpl <em>MSD Util</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.MSDUtilImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getMSDUtil()
	 * @generated
	 */
	int MSD_UTIL = 18;

	/**
	 * The feature id for the '<em><b>Environment Assumption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__ENVIRONMENT_ASSUMPTION = 0;

	/**
	 * The feature id for the '<em><b>Hot Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__HOT_MESSAGES = 1;

	/**
	 * The feature id for the '<em><b>Executed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__EXECUTED_MESSAGES = 2;

	/**
	 * The feature id for the '<em><b>Environment Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__ENVIRONMENT_MESSAGES = 3;

	/**
	 * The feature id for the '<em><b>Cold Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__COLD_FORBIDDEN_MESSAGES = 4;

	/**
	 * The feature id for the '<em><b>Hot Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL__HOT_FORBIDDEN_MESSAGES = 5;

	/**
	 * The number of structural features of the '<em>MSD Util</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MSD_UTIL_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.InteractionToMSDUtilMapEntryImpl <em>Interaction To MSD Util Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.InteractionToMSDUtilMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionToMSDUtilMapEntry()
	 * @generated
	 */
	int INTERACTION_TO_MSD_UTIL_MAP_ENTRY = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_MSD_UTIL_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_MSD_UTIL_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Interaction To MSD Util Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TO_MSD_UTIL_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl <em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionOperandToLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	int INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY = 20;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.StateMachineToInitialStateMapEntryImpl <em>State Machine To Initial State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.StateMachineToInitialStateMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getStateMachineToInitialStateMapEntry()
	 * @generated
	 */
	int STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY = 21;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State Machine To Initial State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.util.impl.IDToOCLMapEntryImpl <em>ID To OCL Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.util.impl.IDToOCLMapEntryImpl
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getIDToOCLMapEntry()
	 * @generated
	 */
	int ID_TO_OCL_MAP_ENTRY = 22;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_TO_OCL_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_TO_OCL_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>ID To OCL Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_TO_OCL_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '<em>Plain Java Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Object
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPlainJavaObject()
	 * @generated
	 */
	int PLAIN_JAVA_OBJECT = 23;


	/**
	 * The meta object id for the '<em>OCL</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ocl.ecore.OCL
	 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getOCL()
	 * @generated
	 */
	int OCL = 24;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.util.RuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Runtime Util</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil
	 * @generated
	 */
	EClass getRuntimeUtil();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.msd.util.RuntimeUtil#getUml2ecoreMapping <em>Uml2ecore Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Uml2ecore Mapping</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getUml2ecoreMapping()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_Uml2ecoreMapping();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.RuntimeUtil#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root UML Package To EPackage</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getRootUMLPackageToEPackage()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_RootUMLPackageToEPackage();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getFirstMessageOperationToInteractionMap <em>First Message Operation To Interaction Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>First Message Operation To Interaction Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getFirstMessageOperationToInteractionMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_FirstMessageOperationToInteractionMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getFirstTransitionOperationToStateMachineMap <em>First Transition Operation To State Machine Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>First Transition Operation To State Machine Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getFirstTransitionOperationToStateMachineMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_FirstTransitionOperationToStateMachineMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionToStringToLifelineMapMap <em>Interaction To String To Lifeline Map Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Interaction To String To Lifeline Map Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getInteractionToStringToLifelineMapMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_InteractionToStringToLifelineMapMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getLifelineToInteractionFragmentsMap <em>Lifeline To Interaction Fragments Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Lifeline To Interaction Fragments Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getLifelineToInteractionFragmentsMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_LifelineToInteractionFragmentsMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getSystemClasses <em>System Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>System Classes</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getSystemClasses()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_SystemClasses();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getEnvironmentClasses <em>Environment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Environment Classes</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getEnvironmentClasses()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_EnvironmentClasses();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getUmlPackages <em>Uml Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uml Packages</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getUmlPackages()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_UmlPackages();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getPackageToCollaborationsMap <em>Package To Collaborations Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Package To Collaborations Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getPackageToCollaborationsMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_PackageToCollaborationsMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getInteractions <em>Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interactions</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getInteractions()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_Interactions();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.RuntimeUtil#getIntegratedPackage <em>Integrated Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Integrated Package</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getIntegratedPackage()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_IntegratedPackage();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getEnvironmentSendableMessages <em>Environment Sendable Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Environment Sendable Messages</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getEnvironmentSendableMessages()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_EnvironmentSendableMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getSystemSendableMessages <em>System Sendable Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>System Sendable Messages</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getSystemSendableMessages()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_SystemSendableMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Use Case Specifications To Be Considered</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getUseCaseSpecificationsToBeConsidered()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_UseCaseSpecificationsToBeConsidered();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.RuntimeUtil#getOclRegistry <em>Ocl Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ocl Registry</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getOclRegistry()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_OclRegistry();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Msd Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getMsdRuntimeStateGraph()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_MsdRuntimeStateGraph();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.RuntimeUtil#getFinalFragment <em>Final Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Final Fragment</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getFinalFragment()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_FinalFragment();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionToMSDUtilMap <em>Interaction To MSD Util Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Interaction To MSD Util Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getInteractionToMSDUtilMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_InteractionToMSDUtilMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getInteractionOperandToLifelineToInteractionFragmentsMapMap <em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getInteractionOperandToLifelineToInteractionFragmentsMapMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_InteractionOperandToLifelineToInteractionFragmentsMapMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.RuntimeUtil#getStateMachines <em>State Machines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>State Machines</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getStateMachines()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_StateMachines();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.RuntimeUtil#getStateMachineToInitialStateMap <em>State Machine To Initial State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>State Machine To Initial State Map</em>'.
	 * @see org.scenariotools.msd.util.RuntimeUtil#getStateMachineToInitialStateMap()
	 * @see #getRuntimeUtil()
	 * @generated
	 */
	EReference getRuntimeUtil_StateMachineToInitialStateMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.util.UML2EcoreMapping <em>UML2 Ecore Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML2 Ecore Mapping</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping
	 * @generated
	 */
	EClass getUML2EcoreMapping();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlClassToEClassMap <em>Uml Class To EClass Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Uml Class To EClass Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getUmlClassToEClassMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_UmlClassToEClassMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getEClassToUMLClassMap <em>EClass To UML Class Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EClass To UML Class Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getEClassToUMLClassMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_EClassToUMLClassMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getEOperation2UMLOperationMap <em>EOperation2 UML Operation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EOperation2 UML Operation Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getEOperation2UMLOperationMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_EOperation2UMLOperationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEOperationMap <em>Uml Operation To EOperation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Uml Operation To EOperation Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEOperationMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_UmlOperationToEOperationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getEParameterToUMLParameterMap <em>EParameter To UML Parameter Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EParameter To UML Parameter Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getEParameterToUMLParameterMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_EParameterToUMLParameterMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlParameterToEParameterMap <em>Uml Parameter To EParameter Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Uml Parameter To EParameter Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getUmlParameterToEParameterMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_UmlParameterToEParameterMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEStructuralFeatureMap <em>Uml Operation To EStructural Feature Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Uml Operation To EStructural Feature Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEStructuralFeatureMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_UmlOperationToEStructuralFeatureMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.UML2EcoreMapping#getEStructuralFeatureToUMLOperationMap <em>EStructural Feature To UML Operation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EStructural Feature To UML Operation Map</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getEStructuralFeatureToUMLOperationMap()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_EStructuralFeatureToUMLOperationMap();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.util.UML2EcoreMapping#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root UML Package To EPackage</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getRootUMLPackageToEPackage()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_RootUMLPackageToEPackage();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.UML2EcoreMapping#getMergedUMLPackages <em>Merged UML Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Merged UML Packages</em>'.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping#getMergedUMLPackages()
	 * @see #getUML2EcoreMapping()
	 * @generated
	 */
	EReference getUML2EcoreMapping_MergedUMLPackages();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>UML Class To EClass Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Class To EClass Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Class" keyRequired="true"
	 *        valueType="org.eclipse.emf.ecore.EClass"
	 * @generated
	 */
	EClass getUMLClassToEClassMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLClassToEClassMapEntry()
	 * @generated
	 */
	EReference getUMLClassToEClassMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLClassToEClassMapEntry()
	 * @generated
	 */
	EReference getUMLClassToEClassMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To UML Class Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To UML Class Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EClass" keyRequired="true"
	 *        valueType="org.eclipse.uml2.uml.Class" valueMany="true"
	 * @generated
	 */
	EClass getEClassToUMLClassMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToUMLClassMapEntry()
	 * @generated
	 */
	EReference getEClassToUMLClassMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToUMLClassMapEntry()
	 * @generated
	 */
	EReference getEClassToUMLClassMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EOperation2 UML Operation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EOperation2 UML Operation Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EOperation"
	 *        valueType="org.eclipse.uml2.uml.Operation" valueMany="true"
	 * @generated
	 */
	EClass getEOperation2UMLOperationMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEOperation2UMLOperationMapEntry()
	 * @generated
	 */
	EReference getEOperation2UMLOperationMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEOperation2UMLOperationMapEntry()
	 * @generated
	 */
	EReference getEOperation2UMLOperationMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>UML Operation To EOperation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Operation To EOperation Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Operation"
	 *        valueType="org.eclipse.emf.ecore.EOperation"
	 * @generated
	 */
	EClass getUMLOperationToEOperationMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLOperationToEOperationMapEntry()
	 * @generated
	 */
	EReference getUMLOperationToEOperationMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLOperationToEOperationMapEntry()
	 * @generated
	 */
	EReference getUMLOperationToEOperationMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EParameter To UML Parameter Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EParameter To UML Parameter Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EParameter"
	 *        valueType="org.eclipse.uml2.uml.Parameter" valueMany="true"
	 * @generated
	 */
	EClass getEParameterToUMLParameterMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEParameterToUMLParameterMapEntry()
	 * @generated
	 */
	EReference getEParameterToUMLParameterMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEParameterToUMLParameterMapEntry()
	 * @generated
	 */
	EReference getEParameterToUMLParameterMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>UML Parameter To EParameter Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Parameter To EParameter Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Parameter"
	 *        valueType="org.eclipse.emf.ecore.EParameter"
	 * @generated
	 */
	EClass getUMLParameterToEParameterMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLParameterToEParameterMapEntry()
	 * @generated
	 */
	EReference getUMLParameterToEParameterMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLParameterToEParameterMapEntry()
	 * @generated
	 */
	EReference getUMLParameterToEParameterMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EStructural Feature To UML Operation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EStructural Feature To UML Operation Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EStructuralFeature"
	 *        valueType="org.eclipse.uml2.uml.Operation" valueMany="true"
	 * @generated
	 */
	EClass getEStructuralFeatureToUMLOperationMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEStructuralFeatureToUMLOperationMapEntry()
	 * @generated
	 */
	EReference getEStructuralFeatureToUMLOperationMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEStructuralFeatureToUMLOperationMapEntry()
	 * @generated
	 */
	EReference getEStructuralFeatureToUMLOperationMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>UML Operation To EStructural Feature Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Operation To EStructural Feature Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Operation"
	 *        valueType="org.eclipse.emf.ecore.EStructuralFeature"
	 * @generated
	 */
	EClass getUMLOperationToEStructuralFeatureMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLOperationToEStructuralFeatureMapEntry()
	 * @generated
	 */
	EReference getUMLOperationToEStructuralFeatureMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getUMLOperationToEStructuralFeatureMapEntry()
	 * @generated
	 */
	EReference getUMLOperationToEStructuralFeatureMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Lifeline Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Lifeline Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.uml2.uml.Lifeline"
	 *        keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
	 * @generated
	 */
	EClass getStringToLifelineMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToLifelineMapEntry()
	 * @generated
	 */
	EReference getStringToLifelineMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToLifelineMapEntry()
	 * @generated
	 */
	EAttribute getStringToLifelineMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>First Message Operation To Interaction Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>First Message Operation To Interaction Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.uml2.uml.Interaction" valueMany="true"
	 *        keyType="org.eclipse.uml2.uml.Operation" keyRequired="true"
	 * @generated
	 */
	EClass getFirstMessageOperationToInteractionMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFirstMessageOperationToInteractionMapEntry()
	 * @generated
	 */
	EReference getFirstMessageOperationToInteractionMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFirstMessageOperationToInteractionMapEntry()
	 * @generated
	 */
	EReference getFirstMessageOperationToInteractionMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>First Transition Operation To State Machine Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>First Transition Operation To State Machine Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.uml2.uml.StateMachine" valueMany="true"
	 *        keyType="org.eclipse.uml2.uml.Operation" keyRequired="true"
	 * @generated
	 */
	EClass getFirstTransitionOperationToStateMachineMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFirstTransitionOperationToStateMachineMapEntry()
	 * @generated
	 */
	EReference getFirstTransitionOperationToStateMachineMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getFirstTransitionOperationToStateMachineMapEntry()
	 * @generated
	 */
	EReference getFirstTransitionOperationToStateMachineMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Lifeline To Interaction Fragments Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lifeline To Interaction Fragments Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Lifeline" keyRequired="true"
	 *        valueType="org.eclipse.uml2.uml.InteractionFragment" valueMany="true"
	 * @generated
	 */
	EClass getLifelineToInteractionFragmentsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	EReference getLifelineToInteractionFragmentsMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	EReference getLifelineToInteractionFragmentsMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Package To Collaboration Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package To Collaboration Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.uml2.uml.Collaboration" valueMany="true"
	 *        keyType="org.eclipse.uml2.uml.Package" keyRequired="true"
	 * @generated
	 */
	EClass getPackageToCollaborationMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToCollaborationMapEntry()
	 * @generated
	 */
	EReference getPackageToCollaborationMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPackageToCollaborationMapEntry()
	 * @generated
	 */
	EReference getPackageToCollaborationMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Interaction To String To Lifeline Map Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction To String To Lifeline Map Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueMapType="org.scenariotools.msd.util.StringToLifelineMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.uml2.uml.Lifeline>"
	 *        keyType="org.eclipse.uml2.uml.Interaction" keyRequired="true"
	 * @generated
	 */
	EClass getInteractionToStringToLifelineMapMapEntry();

	/**
	 * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionToStringToLifelineMapMapEntry()
	 * @generated
	 */
	EReference getInteractionToStringToLifelineMapMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionToStringToLifelineMapMapEntry()
	 * @generated
	 */
	EReference getInteractionToStringToLifelineMapMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Property To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="value key" 
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 *        keyType="org.eclipse.uml2.uml.Property" keyRequired="true"
	 * @generated
	 */
	EClass getPropertyToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToEObjectMapEntry()
	 * @generated
	 */
	EReference getPropertyToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToEObjectMapEntry()
	 * @generated
	 */
	EReference getPropertyToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.util.OCLRegistry <em>OCL Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Registry</em>'.
	 * @see org.scenariotools.msd.util.OCLRegistry
	 * @generated
	 */
	EClass getOCLRegistry();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.msd.util.OCLRegistry#getIdToOCLMap <em>Id To OCL Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Id To OCL Map</em>'.
	 * @see org.scenariotools.msd.util.OCLRegistry#getIdToOCLMap()
	 * @see #getOCLRegistry()
	 * @generated
	 */
	EReference getOCLRegistry_IdToOCLMap();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.util.MSDUtil <em>MSD Util</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MSD Util</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil
	 * @generated
	 */
	EClass getMSDUtil();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.util.MSDUtil#isEnvironmentAssumption <em>Environment Assumption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Environment Assumption</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#isEnvironmentAssumption()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EAttribute getMSDUtil_EnvironmentAssumption();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.MSDUtil#getHotMessages <em>Hot Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Hot Messages</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#getHotMessages()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EReference getMSDUtil_HotMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.MSDUtil#getExecutedMessages <em>Executed Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Executed Messages</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#getExecutedMessages()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EReference getMSDUtil_ExecutedMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.MSDUtil#getEnvironmentMessages <em>Environment Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Environment Messages</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#getEnvironmentMessages()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EReference getMSDUtil_EnvironmentMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.MSDUtil#getColdForbiddenMessages <em>Cold Forbidden Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Cold Forbidden Messages</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#getColdForbiddenMessages()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EReference getMSDUtil_ColdForbiddenMessages();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.util.MSDUtil#getHotForbiddenMessages <em>Hot Forbidden Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Hot Forbidden Messages</em>'.
	 * @see org.scenariotools.msd.util.MSDUtil#getHotForbiddenMessages()
	 * @see #getMSDUtil()
	 * @generated
	 */
	EReference getMSDUtil_HotForbiddenMessages();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Interaction To MSD Util Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction To MSD Util Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.Interaction"
	 *        valueType="org.scenariotools.msd.util.MSDUtil"
	 * @generated
	 */
	EClass getInteractionToMSDUtilMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionToMSDUtilMapEntry()
	 * @generated
	 */
	EReference getInteractionToMSDUtilMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionToMSDUtilMapEntry()
	 * @generated
	 */
	EReference getInteractionToMSDUtilMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.InteractionOperand" keyRequired="true"
	 *        valueMapType="org.scenariotools.msd.util.LifelineToInteractionFragmentsMapEntry<org.eclipse.uml2.uml.Lifeline, org.eclipse.uml2.uml.InteractionFragment>"
	 * @generated
	 */
	EClass getInteractionOperandToLifelineToInteractionFragmentsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionOperandToLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	EReference getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Key();

	/**
	 * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInteractionOperandToLifelineToInteractionFragmentsMapEntry()
	 * @generated
	 */
	EReference getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State Machine To Initial State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine To Initial State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.uml2.uml.StateMachine" keyRequired="true"
	 *        valueType="org.eclipse.uml2.uml.State"
	 * @generated
	 */
	EClass getStateMachineToInitialStateMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateMachineToInitialStateMapEntry()
	 * @generated
	 */
	EReference getStateMachineToInitialStateMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateMachineToInitialStateMapEntry()
	 * @generated
	 */
	EReference getStateMachineToInitialStateMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>ID To OCL Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ID To OCL Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.scenariotools.msd.util.OCL"
	 * @generated
	 */
	EClass getIDToOCLMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIDToOCLMapEntry()
	 * @generated
	 */
	EAttribute getIDToOCLMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getIDToOCLMapEntry()
	 * @generated
	 */
	EAttribute getIDToOCLMapEntry_Value();

	/**
	 * Returns the meta object for data type '{@link java.lang.Object <em>Plain Java Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Plain Java Object</em>'.
	 * @see java.lang.Object
	 * @model instanceClass="java.lang.Object"
	 * @generated
	 */
	EDataType getPlainJavaObject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ocl.ecore.OCL <em>OCL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>OCL</em>'.
	 * @see org.eclipse.ocl.ecore.OCL
	 * @model instanceClass="org.eclipse.ocl.ecore.OCL"
	 * @generated
	 */
	EDataType getOCL();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UtilFactory getUtilFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl <em>Runtime Util</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.RuntimeUtilImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getRuntimeUtil()
		 * @generated
		 */
		EClass RUNTIME_UTIL = eINSTANCE.getRuntimeUtil();

		/**
		 * The meta object literal for the '<em><b>Uml2ecore Mapping</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__UML2ECORE_MAPPING = eINSTANCE.getRuntimeUtil_Uml2ecoreMapping();

		/**
		 * The meta object literal for the '<em><b>Root UML Package To EPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE = eINSTANCE.getRuntimeUtil_RootUMLPackageToEPackage();

		/**
		 * The meta object literal for the '<em><b>First Message Operation To Interaction Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP = eINSTANCE.getRuntimeUtil_FirstMessageOperationToInteractionMap();

		/**
		 * The meta object literal for the '<em><b>First Transition Operation To State Machine Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP = eINSTANCE.getRuntimeUtil_FirstTransitionOperationToStateMachineMap();

		/**
		 * The meta object literal for the '<em><b>Interaction To String To Lifeline Map Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP = eINSTANCE.getRuntimeUtil_InteractionToStringToLifelineMapMap();

		/**
		 * The meta object literal for the '<em><b>Lifeline To Interaction Fragments Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP = eINSTANCE.getRuntimeUtil_LifelineToInteractionFragmentsMap();

		/**
		 * The meta object literal for the '<em><b>System Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__SYSTEM_CLASSES = eINSTANCE.getRuntimeUtil_SystemClasses();

		/**
		 * The meta object literal for the '<em><b>Environment Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__ENVIRONMENT_CLASSES = eINSTANCE.getRuntimeUtil_EnvironmentClasses();

		/**
		 * The meta object literal for the '<em><b>Uml Packages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__UML_PACKAGES = eINSTANCE.getRuntimeUtil_UmlPackages();

		/**
		 * The meta object literal for the '<em><b>Package To Collaborations Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP = eINSTANCE.getRuntimeUtil_PackageToCollaborationsMap();

		/**
		 * The meta object literal for the '<em><b>Interactions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__INTERACTIONS = eINSTANCE.getRuntimeUtil_Interactions();

		/**
		 * The meta object literal for the '<em><b>Integrated Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__INTEGRATED_PACKAGE = eINSTANCE.getRuntimeUtil_IntegratedPackage();

		/**
		 * The meta object literal for the '<em><b>Environment Sendable Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES = eINSTANCE.getRuntimeUtil_EnvironmentSendableMessages();

		/**
		 * The meta object literal for the '<em><b>System Sendable Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES = eINSTANCE.getRuntimeUtil_SystemSendableMessages();

		/**
		 * The meta object literal for the '<em><b>Use Case Specifications To Be Considered</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED = eINSTANCE.getRuntimeUtil_UseCaseSpecificationsToBeConsidered();

		/**
		 * The meta object literal for the '<em><b>Ocl Registry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__OCL_REGISTRY = eINSTANCE.getRuntimeUtil_OclRegistry();

		/**
		 * The meta object literal for the '<em><b>Msd Runtime State Graph</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getRuntimeUtil_MsdRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Final Fragment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__FINAL_FRAGMENT = eINSTANCE.getRuntimeUtil_FinalFragment();

		/**
		 * The meta object literal for the '<em><b>Interaction To MSD Util Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP = eINSTANCE.getRuntimeUtil_InteractionToMSDUtilMap();

		/**
		 * The meta object literal for the '<em><b>Interaction Operand To Lifeline To Interaction Fragments Map Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP = eINSTANCE.getRuntimeUtil_InteractionOperandToLifelineToInteractionFragmentsMapMap();

		/**
		 * The meta object literal for the '<em><b>State Machines</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__STATE_MACHINES = eINSTANCE.getRuntimeUtil_StateMachines();

		/**
		 * The meta object literal for the '<em><b>State Machine To Initial State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP = eINSTANCE.getRuntimeUtil_StateMachineToInitialStateMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl <em>UML2 Ecore Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.UML2EcoreMappingImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUML2EcoreMapping()
		 * @generated
		 */
		EClass UML2_ECORE_MAPPING = eINSTANCE.getUML2EcoreMapping();

		/**
		 * The meta object literal for the '<em><b>Uml Class To EClass Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP = eINSTANCE.getUML2EcoreMapping_UmlClassToEClassMap();

		/**
		 * The meta object literal for the '<em><b>EClass To UML Class Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP = eINSTANCE.getUML2EcoreMapping_EClassToUMLClassMap();

		/**
		 * The meta object literal for the '<em><b>EOperation2 UML Operation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP = eINSTANCE.getUML2EcoreMapping_EOperation2UMLOperationMap();

		/**
		 * The meta object literal for the '<em><b>Uml Operation To EOperation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP = eINSTANCE.getUML2EcoreMapping_UmlOperationToEOperationMap();

		/**
		 * The meta object literal for the '<em><b>EParameter To UML Parameter Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP = eINSTANCE.getUML2EcoreMapping_EParameterToUMLParameterMap();

		/**
		 * The meta object literal for the '<em><b>Uml Parameter To EParameter Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP = eINSTANCE.getUML2EcoreMapping_UmlParameterToEParameterMap();

		/**
		 * The meta object literal for the '<em><b>Uml Operation To EStructural Feature Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP = eINSTANCE.getUML2EcoreMapping_UmlOperationToEStructuralFeatureMap();

		/**
		 * The meta object literal for the '<em><b>EStructural Feature To UML Operation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP = eINSTANCE.getUML2EcoreMapping_EStructuralFeatureToUMLOperationMap();

		/**
		 * The meta object literal for the '<em><b>Root UML Package To EPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE = eINSTANCE.getUML2EcoreMapping_RootUMLPackageToEPackage();

		/**
		 * The meta object literal for the '<em><b>Merged UML Packages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML2_ECORE_MAPPING__MERGED_UML_PACKAGES = eINSTANCE.getUML2EcoreMapping_MergedUMLPackages();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.UMLClassToEClassMapEntryImpl <em>UML Class To EClass Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.UMLClassToEClassMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLClassToEClassMapEntry()
		 * @generated
		 */
		EClass UML_CLASS_TO_ECLASS_MAP_ENTRY = eINSTANCE.getUMLClassToEClassMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS_TO_ECLASS_MAP_ENTRY__KEY = eINSTANCE.getUMLClassToEClassMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS_TO_ECLASS_MAP_ENTRY__VALUE = eINSTANCE.getUMLClassToEClassMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.EClassToUMLClassMapEntryImpl <em>EClass To UML Class Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.EClassToUMLClassMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEClassToUMLClassMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_UML_CLASS_MAP_ENTRY = eINSTANCE.getEClassToUMLClassMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_UML_CLASS_MAP_ENTRY__KEY = eINSTANCE.getEClassToUMLClassMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_UML_CLASS_MAP_ENTRY__VALUE = eINSTANCE.getEClassToUMLClassMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.EOperation2UMLOperationMapEntryImpl <em>EOperation2 UML Operation Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.EOperation2UMLOperationMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEOperation2UMLOperationMapEntry()
		 * @generated
		 */
		EClass EOPERATION2_UML_OPERATION_MAP_ENTRY = eINSTANCE.getEOperation2UMLOperationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOPERATION2_UML_OPERATION_MAP_ENTRY__KEY = eINSTANCE.getEOperation2UMLOperationMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOPERATION2_UML_OPERATION_MAP_ENTRY__VALUE = eINSTANCE.getEOperation2UMLOperationMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.UMLOperationToEOperationMapEntryImpl <em>UML Operation To EOperation Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.UMLOperationToEOperationMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLOperationToEOperationMapEntry()
		 * @generated
		 */
		EClass UML_OPERATION_TO_EOPERATION_MAP_ENTRY = eINSTANCE.getUMLOperationToEOperationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OPERATION_TO_EOPERATION_MAP_ENTRY__KEY = eINSTANCE.getUMLOperationToEOperationMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OPERATION_TO_EOPERATION_MAP_ENTRY__VALUE = eINSTANCE.getUMLOperationToEOperationMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.EParameterToUMLParameterMapEntryImpl <em>EParameter To UML Parameter Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.EParameterToUMLParameterMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEParameterToUMLParameterMapEntry()
		 * @generated
		 */
		EClass EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY = eINSTANCE.getEParameterToUMLParameterMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__KEY = eINSTANCE.getEParameterToUMLParameterMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__VALUE = eINSTANCE.getEParameterToUMLParameterMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.UMLParameterToEParameterMapEntryImpl <em>UML Parameter To EParameter Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.UMLParameterToEParameterMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLParameterToEParameterMapEntry()
		 * @generated
		 */
		EClass UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY = eINSTANCE.getUMLParameterToEParameterMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__KEY = eINSTANCE.getUMLParameterToEParameterMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__VALUE = eINSTANCE.getUMLParameterToEParameterMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.EStructuralFeatureToUMLOperationMapEntryImpl <em>EStructural Feature To UML Operation Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.EStructuralFeatureToUMLOperationMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getEStructuralFeatureToUMLOperationMapEntry()
		 * @generated
		 */
		EClass ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY = eINSTANCE.getEStructuralFeatureToUMLOperationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__KEY = eINSTANCE.getEStructuralFeatureToUMLOperationMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__VALUE = eINSTANCE.getEStructuralFeatureToUMLOperationMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.UMLOperationToEStructuralFeatureMapEntryImpl <em>UML Operation To EStructural Feature Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.UMLOperationToEStructuralFeatureMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getUMLOperationToEStructuralFeatureMapEntry()
		 * @generated
		 */
		EClass UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY = eINSTANCE.getUMLOperationToEStructuralFeatureMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__KEY = eINSTANCE.getUMLOperationToEStructuralFeatureMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__VALUE = eINSTANCE.getUMLOperationToEStructuralFeatureMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.StringToLifelineMapEntryImpl <em>String To Lifeline Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.StringToLifelineMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getStringToLifelineMapEntry()
		 * @generated
		 */
		EClass STRING_TO_LIFELINE_MAP_ENTRY = eINSTANCE.getStringToLifelineMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_LIFELINE_MAP_ENTRY__VALUE = eINSTANCE.getStringToLifelineMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_LIFELINE_MAP_ENTRY__KEY = eINSTANCE.getStringToLifelineMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.FirstMessageOperationToInteractionMapEntryImpl <em>First Message Operation To Interaction Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.FirstMessageOperationToInteractionMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getFirstMessageOperationToInteractionMapEntry()
		 * @generated
		 */
		EClass FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY = eINSTANCE.getFirstMessageOperationToInteractionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__VALUE = eINSTANCE.getFirstMessageOperationToInteractionMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__KEY = eINSTANCE.getFirstMessageOperationToInteractionMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.FirstTransitionOperationToStateMachineMapEntryImpl <em>First Transition Operation To State Machine Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.FirstTransitionOperationToStateMachineMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getFirstTransitionOperationToStateMachineMapEntry()
		 * @generated
		 */
		EClass FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY = eINSTANCE.getFirstTransitionOperationToStateMachineMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__VALUE = eINSTANCE.getFirstTransitionOperationToStateMachineMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__KEY = eINSTANCE.getFirstTransitionOperationToStateMachineMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.LifelineToInteractionFragmentsMapEntryImpl <em>Lifeline To Interaction Fragments Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.LifelineToInteractionFragmentsMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getLifelineToInteractionFragmentsMapEntry()
		 * @generated
		 */
		EClass LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY = eINSTANCE.getLifelineToInteractionFragmentsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY = eINSTANCE.getLifelineToInteractionFragmentsMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE = eINSTANCE.getLifelineToInteractionFragmentsMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.PackageToCollaborationMapEntryImpl <em>Package To Collaboration Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.PackageToCollaborationMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPackageToCollaborationMapEntry()
		 * @generated
		 */
		EClass PACKAGE_TO_COLLABORATION_MAP_ENTRY = eINSTANCE.getPackageToCollaborationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_COLLABORATION_MAP_ENTRY__VALUE = eINSTANCE.getPackageToCollaborationMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_TO_COLLABORATION_MAP_ENTRY__KEY = eINSTANCE.getPackageToCollaborationMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.InteractionToStringToLifelineMapMapEntryImpl <em>Interaction To String To Lifeline Map Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.InteractionToStringToLifelineMapMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionToStringToLifelineMapMapEntry()
		 * @generated
		 */
		EClass INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY = eINSTANCE.getInteractionToStringToLifelineMapMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__VALUE = eINSTANCE.getInteractionToStringToLifelineMapMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__KEY = eINSTANCE.getInteractionToStringToLifelineMapMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.PropertyToEObjectMapEntryImpl <em>Property To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.PropertyToEObjectMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPropertyToEObjectMapEntry()
		 * @generated
		 */
		EClass PROPERTY_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getPropertyToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getPropertyToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getPropertyToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.OCLRegistryImpl <em>OCL Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.OCLRegistryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getOCLRegistry()
		 * @generated
		 */
		EClass OCL_REGISTRY = eINSTANCE.getOCLRegistry();

		/**
		 * The meta object literal for the '<em><b>Id To OCL Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OCL_REGISTRY__ID_TO_OCL_MAP = eINSTANCE.getOCLRegistry_IdToOCLMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.MSDUtilImpl <em>MSD Util</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.MSDUtilImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getMSDUtil()
		 * @generated
		 */
		EClass MSD_UTIL = eINSTANCE.getMSDUtil();

		/**
		 * The meta object literal for the '<em><b>Environment Assumption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MSD_UTIL__ENVIRONMENT_ASSUMPTION = eINSTANCE.getMSDUtil_EnvironmentAssumption();

		/**
		 * The meta object literal for the '<em><b>Hot Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_UTIL__HOT_MESSAGES = eINSTANCE.getMSDUtil_HotMessages();

		/**
		 * The meta object literal for the '<em><b>Executed Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_UTIL__EXECUTED_MESSAGES = eINSTANCE.getMSDUtil_ExecutedMessages();

		/**
		 * The meta object literal for the '<em><b>Environment Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_UTIL__ENVIRONMENT_MESSAGES = eINSTANCE.getMSDUtil_EnvironmentMessages();

		/**
		 * The meta object literal for the '<em><b>Cold Forbidden Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_UTIL__COLD_FORBIDDEN_MESSAGES = eINSTANCE.getMSDUtil_ColdForbiddenMessages();

		/**
		 * The meta object literal for the '<em><b>Hot Forbidden Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MSD_UTIL__HOT_FORBIDDEN_MESSAGES = eINSTANCE.getMSDUtil_HotForbiddenMessages();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.InteractionToMSDUtilMapEntryImpl <em>Interaction To MSD Util Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.InteractionToMSDUtilMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionToMSDUtilMapEntry()
		 * @generated
		 */
		EClass INTERACTION_TO_MSD_UTIL_MAP_ENTRY = eINSTANCE.getInteractionToMSDUtilMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_TO_MSD_UTIL_MAP_ENTRY__KEY = eINSTANCE.getInteractionToMSDUtilMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_TO_MSD_UTIL_MAP_ENTRY__VALUE = eINSTANCE.getInteractionToMSDUtilMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl <em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getInteractionOperandToLifelineToInteractionFragmentsMapEntry()
		 * @generated
		 */
		EClass INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY = eINSTANCE.getInteractionOperandToLifelineToInteractionFragmentsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY = eINSTANCE.getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE = eINSTANCE.getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.StateMachineToInitialStateMapEntryImpl <em>State Machine To Initial State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.StateMachineToInitialStateMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getStateMachineToInitialStateMapEntry()
		 * @generated
		 */
		EClass STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY = eINSTANCE.getStateMachineToInitialStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__KEY = eINSTANCE.getStateMachineToInitialStateMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__VALUE = eINSTANCE.getStateMachineToInitialStateMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.util.impl.IDToOCLMapEntryImpl <em>ID To OCL Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.util.impl.IDToOCLMapEntryImpl
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getIDToOCLMapEntry()
		 * @generated
		 */
		EClass ID_TO_OCL_MAP_ENTRY = eINSTANCE.getIDToOCLMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_TO_OCL_MAP_ENTRY__KEY = eINSTANCE.getIDToOCLMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_TO_OCL_MAP_ENTRY__VALUE = eINSTANCE.getIDToOCLMapEntry_Value();

		/**
		 * The meta object literal for the '<em>Plain Java Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Object
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getPlainJavaObject()
		 * @generated
		 */
		EDataType PLAIN_JAVA_OBJECT = eINSTANCE.getPlainJavaObject();

		/**
		 * The meta object literal for the '<em>OCL</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ocl.ecore.OCL
		 * @see org.scenariotools.msd.util.impl.UtilPackageImpl#getOCL()
		 * @generated
		 */
		EDataType OCL = eINSTANCE.getOCL();

	}

} //UtilPackage
