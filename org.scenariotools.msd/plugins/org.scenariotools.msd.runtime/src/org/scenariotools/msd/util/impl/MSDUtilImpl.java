/**
 */
package org.scenariotools.msd.util.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.uml2.uml.Message;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSD Util</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#isEnvironmentAssumption <em>Environment Assumption</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#getHotMessages <em>Hot Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#getExecutedMessages <em>Executed Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#getEnvironmentMessages <em>Environment Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#getColdForbiddenMessages <em>Cold Forbidden Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.MSDUtilImpl#getHotForbiddenMessages <em>Hot Forbidden Messages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MSDUtilImpl extends RuntimeEObjectImpl implements MSDUtil {
	/**
	 * The default value of the '{@link #isEnvironmentAssumption() <em>Environment Assumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnvironmentAssumption()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENVIRONMENT_ASSUMPTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEnvironmentAssumption() <em>Environment Assumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnvironmentAssumption()
	 * @generated
	 * @ordered
	 */
	protected boolean environmentAssumption = ENVIRONMENT_ASSUMPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHotMessages() <em>Hot Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHotMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> hotMessages;

	/**
	 * The cached value of the '{@link #getExecutedMessages() <em>Executed Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutedMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> executedMessages;

	/**
	 * The cached value of the '{@link #getEnvironmentMessages() <em>Environment Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironmentMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> environmentMessages;

	/**
	 * The cached value of the '{@link #getColdForbiddenMessages() <em>Cold Forbidden Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColdForbiddenMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> coldForbiddenMessages;

	/**
	 * The cached value of the '{@link #getHotForbiddenMessages() <em>Hot Forbidden Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHotForbiddenMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> hotForbiddenMessages;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSDUtilImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilPackage.Literals.MSD_UTIL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnvironmentAssumption() {
		return environmentAssumption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironmentAssumption(boolean newEnvironmentAssumption) {
		boolean oldEnvironmentAssumption = environmentAssumption;
		environmentAssumption = newEnvironmentAssumption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.MSD_UTIL__ENVIRONMENT_ASSUMPTION, oldEnvironmentAssumption, environmentAssumption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getHotMessages() {
		if (hotMessages == null) {
			hotMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.MSD_UTIL__HOT_MESSAGES);
		}
		return hotMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getHotMessages(String name) {
		return getHotMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getHotMessages(String name, boolean ignoreCase) {
		hotMessagesLoop: for (Message hotMessages : getHotMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(hotMessages.getName()) : name.equals(hotMessages.getName())))
				continue hotMessagesLoop;
			return hotMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getExecutedMessages() {
		if (executedMessages == null) {
			executedMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.MSD_UTIL__EXECUTED_MESSAGES);
		}
		return executedMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getExecutedMessages(String name) {
		return getExecutedMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getExecutedMessages(String name, boolean ignoreCase) {
		executedMessagesLoop: for (Message executedMessages : getExecutedMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(executedMessages.getName()) : name.equals(executedMessages.getName())))
				continue executedMessagesLoop;
			return executedMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getEnvironmentMessages() {
		if (environmentMessages == null) {
			environmentMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.MSD_UTIL__ENVIRONMENT_MESSAGES);
		}
		return environmentMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getEnvironmentMessages(String name) {
		return getEnvironmentMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getEnvironmentMessages(String name, boolean ignoreCase) {
		environmentMessagesLoop: for (Message environmentMessages : getEnvironmentMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(environmentMessages.getName()) : name.equals(environmentMessages.getName())))
				continue environmentMessagesLoop;
			return environmentMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getColdForbiddenMessages() {
		if (coldForbiddenMessages == null) {
			coldForbiddenMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.MSD_UTIL__COLD_FORBIDDEN_MESSAGES);
		}
		return coldForbiddenMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getColdForbiddenMessages(String name) {
		return getColdForbiddenMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getColdForbiddenMessages(String name, boolean ignoreCase) {
		coldForbiddenMessagesLoop: for (Message coldForbiddenMessages : getColdForbiddenMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(coldForbiddenMessages.getName()) : name.equals(coldForbiddenMessages.getName())))
				continue coldForbiddenMessagesLoop;
			return coldForbiddenMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getHotForbiddenMessages() {
		if (hotForbiddenMessages == null) {
			hotForbiddenMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.MSD_UTIL__HOT_FORBIDDEN_MESSAGES);
		}
		return hotForbiddenMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getHotForbiddenMessages(String name) {
		return getHotForbiddenMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getHotForbiddenMessages(String name, boolean ignoreCase) {
		hotForbiddenMessagesLoop: for (Message hotForbiddenMessages : getHotForbiddenMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(hotForbiddenMessages.getName()) : name.equals(hotForbiddenMessages.getName())))
				continue hotForbiddenMessagesLoop;
			return hotForbiddenMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilPackage.MSD_UTIL__ENVIRONMENT_ASSUMPTION:
				return isEnvironmentAssumption();
			case UtilPackage.MSD_UTIL__HOT_MESSAGES:
				return getHotMessages();
			case UtilPackage.MSD_UTIL__EXECUTED_MESSAGES:
				return getExecutedMessages();
			case UtilPackage.MSD_UTIL__ENVIRONMENT_MESSAGES:
				return getEnvironmentMessages();
			case UtilPackage.MSD_UTIL__COLD_FORBIDDEN_MESSAGES:
				return getColdForbiddenMessages();
			case UtilPackage.MSD_UTIL__HOT_FORBIDDEN_MESSAGES:
				return getHotForbiddenMessages();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilPackage.MSD_UTIL__ENVIRONMENT_ASSUMPTION:
				setEnvironmentAssumption((Boolean)newValue);
				return;
			case UtilPackage.MSD_UTIL__HOT_MESSAGES:
				getHotMessages().clear();
				getHotMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.MSD_UTIL__EXECUTED_MESSAGES:
				getExecutedMessages().clear();
				getExecutedMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.MSD_UTIL__ENVIRONMENT_MESSAGES:
				getEnvironmentMessages().clear();
				getEnvironmentMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.MSD_UTIL__COLD_FORBIDDEN_MESSAGES:
				getColdForbiddenMessages().clear();
				getColdForbiddenMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.MSD_UTIL__HOT_FORBIDDEN_MESSAGES:
				getHotForbiddenMessages().clear();
				getHotForbiddenMessages().addAll((Collection<? extends Message>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilPackage.MSD_UTIL__ENVIRONMENT_ASSUMPTION:
				setEnvironmentAssumption(ENVIRONMENT_ASSUMPTION_EDEFAULT);
				return;
			case UtilPackage.MSD_UTIL__HOT_MESSAGES:
				getHotMessages().clear();
				return;
			case UtilPackage.MSD_UTIL__EXECUTED_MESSAGES:
				getExecutedMessages().clear();
				return;
			case UtilPackage.MSD_UTIL__ENVIRONMENT_MESSAGES:
				getEnvironmentMessages().clear();
				return;
			case UtilPackage.MSD_UTIL__COLD_FORBIDDEN_MESSAGES:
				getColdForbiddenMessages().clear();
				return;
			case UtilPackage.MSD_UTIL__HOT_FORBIDDEN_MESSAGES:
				getHotForbiddenMessages().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilPackage.MSD_UTIL__ENVIRONMENT_ASSUMPTION:
				return environmentAssumption != ENVIRONMENT_ASSUMPTION_EDEFAULT;
			case UtilPackage.MSD_UTIL__HOT_MESSAGES:
				return hotMessages != null && !hotMessages.isEmpty();
			case UtilPackage.MSD_UTIL__EXECUTED_MESSAGES:
				return executedMessages != null && !executedMessages.isEmpty();
			case UtilPackage.MSD_UTIL__ENVIRONMENT_MESSAGES:
				return environmentMessages != null && !environmentMessages.isEmpty();
			case UtilPackage.MSD_UTIL__COLD_FORBIDDEN_MESSAGES:
				return coldForbiddenMessages != null && !coldForbiddenMessages.isEmpty();
			case UtilPackage.MSD_UTIL__HOT_FORBIDDEN_MESSAGES:
				return hotForbiddenMessages != null && !hotForbiddenMessages.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (environmentAssumption: ");
		result.append(environmentAssumption);
		result.append(')');
		return result.toString();
	}

} //MSDUtilImpl
