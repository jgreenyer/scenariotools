/**
 */
package org.scenariotools.msd.util;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSD Util</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#isEnvironmentAssumption <em>Environment Assumption</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#getHotMessages <em>Hot Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#getExecutedMessages <em>Executed Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#getEnvironmentMessages <em>Environment Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#getColdForbiddenMessages <em>Cold Forbidden Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.MSDUtil#getHotForbiddenMessages <em>Hot Forbidden Messages</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil()
 * @model
 * @generated
 */
public interface MSDUtil extends EObject {
	/**
	 * Returns the value of the '<em><b>Environment Assumption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Assumption</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Assumption</em>' attribute.
	 * @see #setEnvironmentAssumption(boolean)
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_EnvironmentAssumption()
	 * @model
	 * @generated
	 */
	boolean isEnvironmentAssumption();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.MSDUtil#isEnvironmentAssumption <em>Environment Assumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment Assumption</em>' attribute.
	 * @see #isEnvironmentAssumption()
	 * @generated
	 */
	void setEnvironmentAssumption(boolean value);

	/**
	 * Returns the value of the '<em><b>Hot Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hot Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hot Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_HotMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getHotMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Hot Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getHotMessages()
	 * @generated
	 */
	Message getHotMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Hot Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getHotMessages()
	 * @generated
	 */
	Message getHotMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>Executed Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executed Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executed Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_ExecutedMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getExecutedMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Executed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getExecutedMessages()
	 * @generated
	 */
	Message getExecutedMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Executed Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getExecutedMessages()
	 * @generated
	 */
	Message getExecutedMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>Environment Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_EnvironmentMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getEnvironmentMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Environment Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentMessages()
	 * @generated
	 */
	Message getEnvironmentMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Environment Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getEnvironmentMessages()
	 * @generated
	 */
	Message getEnvironmentMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>Cold Forbidden Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cold Forbidden Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cold Forbidden Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_ColdForbiddenMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getColdForbiddenMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Cold Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getColdForbiddenMessages()
	 * @generated
	 */
	Message getColdForbiddenMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Cold Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getColdForbiddenMessages()
	 * @generated
	 */
	Message getColdForbiddenMessages(String name, boolean ignoreCase);

	/**
	 * Returns the value of the '<em><b>Hot Forbidden Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hot Forbidden Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hot Forbidden Messages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getMSDUtil_HotForbiddenMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getHotForbiddenMessages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Hot Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getHotForbiddenMessages()
	 * @generated
	 */
	Message getHotForbiddenMessages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>' from the '<em><b>Hot Forbidden Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Message} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @return The first {@link org.eclipse.uml2.uml.Message} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getHotForbiddenMessages()
	 * @generated
	 */
	Message getHotForbiddenMessages(String name, boolean ignoreCase);

} // MSDUtil
