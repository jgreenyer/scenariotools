/**
 */
package org.scenariotools.msd.util.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OCL Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.impl.OCLRegistryImpl#getIdToOCLMap <em>Id To OCL Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OCLRegistryImpl extends RuntimeEObjectImpl implements OCLRegistry {

	/**
	 * The cached value of the '{@link #getIdToOCLMap() <em>Id To OCL Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdToOCLMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, OCL> idToOCLMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected OCLRegistryImpl() {
		super();
		
		idToOCLMap = new EcoreEMap<String,OCL>(UtilPackage.Literals.ID_TO_OCL_MAP_ENTRY, IDToOCLMapEntryImpl.class, this, UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilPackage.Literals.OCL_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, OCL> getIdToOCLMap() {
		if (idToOCLMap == null) {
			idToOCLMap = new EcoreEMap<String,OCL>(UtilPackage.Literals.ID_TO_OCL_MAP_ENTRY, IDToOCLMapEntryImpl.class, this, UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP);
		}
		return idToOCLMap;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void disposeOCLForActiveMSD(ActiveProcess activeMSD) {
		idToOCLMap.remove(activeMSD.getId());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public OCL getOCLForActiveMSD(ActiveProcess activeMSD) {
		OCL ocl = idToOCLMap.get(activeMSD.getId());
		if (ocl == null){
			ocl = OCL.newInstance(new EcoreEnvironmentFactory(EPackage.Registry.INSTANCE));

			idToOCLMap.put(activeMSD.getId(), ocl);
		}
		return ocl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP:
				return ((InternalEList<?>)getIdToOCLMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP:
				if (coreType) return getIdToOCLMap();
				else return getIdToOCLMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP:
				((EStructuralFeature.Setting)getIdToOCLMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP:
				getIdToOCLMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilPackage.OCL_REGISTRY__ID_TO_OCL_MAP:
				return idToOCLMap != null && !idToOCLMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OCLRegistryImpl
