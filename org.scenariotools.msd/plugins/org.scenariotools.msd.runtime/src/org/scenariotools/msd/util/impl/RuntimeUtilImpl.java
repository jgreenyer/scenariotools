/**
 */
package org.scenariotools.msd.util.impl;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.SemanticException;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.Vertex;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.RuntimeMessage;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;
import org.scenariotools.runtime.ObjectSystem;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Runtime Util</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getUml2ecoreMapping <em>Uml2ecore Mapping</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getFirstMessageOperationToInteractionMap <em>First Message Operation To Interaction Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getFirstTransitionOperationToStateMachineMap <em>First Transition Operation To State Machine Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getInteractionToStringToLifelineMapMap <em>Interaction To String To Lifeline Map Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getLifelineToInteractionFragmentsMap <em>Lifeline To Interaction Fragments Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getSystemClasses <em>System Classes</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getEnvironmentClasses <em>Environment Classes</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getUmlPackages <em>Uml Packages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getPackageToCollaborationsMap <em>Package To Collaborations Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getInteractions <em>Interactions</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getIntegratedPackage <em>Integrated Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getEnvironmentSendableMessages <em>Environment Sendable Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getSystemSendableMessages <em>System Sendable Messages</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getUseCaseSpecificationsToBeConsidered <em>Use Case Specifications To Be Considered</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getOclRegistry <em>Ocl Registry</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getFinalFragment <em>Final Fragment</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getInteractionToMSDUtilMap <em>Interaction To MSD Util Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getInteractionOperandToLifelineToInteractionFragmentsMapMap <em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getStateMachines <em>State Machines</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.RuntimeUtilImpl#getStateMachineToInitialStateMap <em>State Machine To Initial State Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuntimeUtilImpl extends RuntimeEObjectImpl implements RuntimeUtil {

	private static Logger logger = Activator.getLogManager().getLogger(
			RuntimeUtilImpl.class.getName());

	/**
	 * The cached value of the '{@link #getUml2ecoreMapping() <em>Uml2ecore Mapping</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getUml2ecoreMapping()
	 * @generated
	 * @ordered
	 */
	protected UML2EcoreMapping uml2ecoreMapping;

	/**
	 * The cached value of the '{@link #getRootUMLPackageToEPackage() <em>Root UML Package To EPackage</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getRootUMLPackageToEPackage()
	 * @generated
	 * @ordered
	 */
	protected UmlPackage2EPackage rootUMLPackageToEPackage;

	/**
	 * The cached value of the '
	 * {@link #getFirstMessageOperationToInteractionMap()
	 * <em>First Message Operation To Interaction Map</em>}' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFirstMessageOperationToInteractionMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Operation, EList<Interaction>> firstMessageOperationToInteractionMap;

	/**
	 * The cached value of the '
	 * {@link #getFirstTransitionOperationToStateMachineMap()
	 * <em>First Transition Operation To State Machine Map</em>}' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFirstTransitionOperationToStateMachineMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Operation, EList<StateMachine>> firstTransitionOperationToStateMachineMap;

	/**
	 * The cached value of the '
	 * {@link #getInteractionToStringToLifelineMapMap()
	 * <em>Interaction To String To Lifeline Map Map</em>}' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInteractionToStringToLifelineMapMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Interaction, EMap<String, Lifeline>> interactionToStringToLifelineMapMap;

	/**
	 * The cached value of the '{@link #getLifelineToInteractionFragmentsMap() <em>Lifeline To Interaction Fragments Map</em>}' map.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getLifelineToInteractionFragmentsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Lifeline, EList<InteractionFragment>> lifelineToInteractionFragmentsMap;

	/**
	 * The cached value of the '{@link #getSystemClasses() <em>System Classes</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSystemClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Class> systemClasses;

	/**
	 * The cached value of the '{@link #getEnvironmentClasses() <em>Environment Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironmentClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Class> environmentClasses;

	/**
	 * The cached value of the '{@link #getUmlPackages() <em>Uml Packages</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUmlPackages()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Package> umlPackages;

	/**
	 * The cached value of the '{@link #getPackageToCollaborationsMap() <em>Package To Collaborations Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageToCollaborationsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<org.eclipse.uml2.uml.Package, EList<Collaboration>> packageToCollaborationsMap;

	/**
	 * The cached value of the '{@link #getInteractions() <em>Interactions</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInteractions()
	 * @generated
	 * @ordered
	 */
	protected EList<Interaction> interactions;

	/**
	 * The cached value of the '{@link #getIntegratedPackage() <em>Integrated Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntegratedPackage()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package integratedPackage;

	/**
	 * The cached value of the '{@link #getEnvironmentSendableMessages()
	 * <em>Environment Sendable Messages</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getEnvironmentSendableMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> environmentSendableMessages;

	/**
	 * The cached value of the '{@link #getSystemSendableMessages() <em>System Sendable Messages</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getSystemSendableMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> systemSendableMessages;

	/**
	 * The cached value of the '
	 * {@link #getUseCaseSpecificationsToBeConsidered()
	 * <em>Use Case Specifications To Be Considered</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUseCaseSpecificationsToBeConsidered()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered;

	/**
	 * The cached value of the '{@link #getOclRegistry() <em>Ocl Registry</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOclRegistry()
	 * @generated
	 * @ordered
	 */
	protected OCLRegistry oclRegistry;

	/**
	 * The cached value of the '{@link #getMsdRuntimeStateGraph() <em>Msd Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 * @ordered
	 */
	protected MSDRuntimeStateGraph msdRuntimeStateGraph;

	/**
	 * The cached value of the '{@link #getFinalFragment() <em>Final Fragment</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getFinalFragment()
	 * @generated
	 * @ordered
	 */
	protected InteractionFragment finalFragment;

	/**
	 * The cached value of the '{@link #getInteractionToMSDUtilMap() <em>Interaction To MSD Util Map</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getInteractionToMSDUtilMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Interaction, MSDUtil> interactionToMSDUtilMap;

	/**
	 * The cached value of the '{@link #getInteractionOperandToLifelineToInteractionFragmentsMapMap() <em>Interaction Operand To Lifeline To Interaction Fragments Map Map</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInteractionOperandToLifelineToInteractionFragmentsMapMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> interactionOperandToLifelineToInteractionFragmentsMapMap;

	/**
	 * The cached value of the '{@link #getStateMachines() <em>State Machines</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getStateMachines()
	 * @generated
	 * @ordered
	 */
	protected EList<StateMachine> stateMachines;

	/**
	 * The cached value of the '{@link #getStateMachineToInitialStateMap() <em>State Machine To Initial State Map</em>}' map.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getStateMachineToInitialStateMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<StateMachine, State> stateMachineToInitialStateMap;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeUtilImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilPackage.Literals.RUNTIME_UTIL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UML2EcoreMapping getUml2ecoreMapping() {
		return uml2ecoreMapping;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUml2ecoreMapping(
			UML2EcoreMapping newUml2ecoreMapping, NotificationChain msgs) {
		UML2EcoreMapping oldUml2ecoreMapping = uml2ecoreMapping;
		uml2ecoreMapping = newUml2ecoreMapping;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING, oldUml2ecoreMapping, newUml2ecoreMapping);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setUml2ecoreMapping(UML2EcoreMapping newUml2ecoreMapping) {
		if (newUml2ecoreMapping != uml2ecoreMapping) {
			NotificationChain msgs = null;
			if (uml2ecoreMapping != null)
				msgs = ((InternalEObject)uml2ecoreMapping).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING, null, msgs);
			if (newUml2ecoreMapping != null)
				msgs = ((InternalEObject)newUml2ecoreMapping).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING, null, msgs);
			msgs = basicSetUml2ecoreMapping(newUml2ecoreMapping, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING, newUml2ecoreMapping, newUml2ecoreMapping));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage getRootUMLPackageToEPackage() {
		if (rootUMLPackageToEPackage != null && rootUMLPackageToEPackage.eIsProxy()) {
			InternalEObject oldRootUMLPackageToEPackage = (InternalEObject)rootUMLPackageToEPackage;
			rootUMLPackageToEPackage = (UmlPackage2EPackage)eResolveProxy(oldRootUMLPackageToEPackage);
			if (rootUMLPackageToEPackage != oldRootUMLPackageToEPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE, oldRootUMLPackageToEPackage, rootUMLPackageToEPackage));
			}
		}
		return rootUMLPackageToEPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage basicGetRootUMLPackageToEPackage() {
		return rootUMLPackageToEPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootUMLPackageToEPackage(
			UmlPackage2EPackage newRootUMLPackageToEPackage) {
		UmlPackage2EPackage oldRootUMLPackageToEPackage = rootUMLPackageToEPackage;
		rootUMLPackageToEPackage = newRootUMLPackageToEPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE, oldRootUMLPackageToEPackage, rootUMLPackageToEPackage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Operation, EList<Interaction>> getFirstMessageOperationToInteractionMap() {
		if (firstMessageOperationToInteractionMap == null) {
			firstMessageOperationToInteractionMap = new EcoreEMap<Operation,EList<Interaction>>(UtilPackage.Literals.FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY, FirstMessageOperationToInteractionMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP);
		}
		return firstMessageOperationToInteractionMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Operation, EList<StateMachine>> getFirstTransitionOperationToStateMachineMap() {
		if (firstTransitionOperationToStateMachineMap == null) {
			firstTransitionOperationToStateMachineMap = new EcoreEMap<Operation,EList<StateMachine>>(UtilPackage.Literals.FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY, FirstTransitionOperationToStateMachineMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP);
		}
		return firstTransitionOperationToStateMachineMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Interaction, EMap<String, Lifeline>> getInteractionToStringToLifelineMapMap() {
		if (interactionToStringToLifelineMapMap == null) {
			interactionToStringToLifelineMapMap = new EcoreEMap<Interaction,EMap<String, Lifeline>>(UtilPackage.Literals.INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY, InteractionToStringToLifelineMapMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP);
		}
		return interactionToStringToLifelineMapMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Lifeline, EList<InteractionFragment>> getLifelineToInteractionFragmentsMap() {
		if (lifelineToInteractionFragmentsMap == null) {
			lifelineToInteractionFragmentsMap = new EcoreEMap<Lifeline,EList<InteractionFragment>>(UtilPackage.Literals.LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY, LifelineToInteractionFragmentsMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP);
		}
		return lifelineToInteractionFragmentsMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Class> getSystemClasses() {
		if (systemClasses == null) {
			systemClasses = new EObjectResolvingEList<org.eclipse.uml2.uml.Class>(org.eclipse.uml2.uml.Class.class, this, UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES);
		}
		return systemClasses;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getSystemClasses(String name) {
		return getSystemClasses(name, false, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getSystemClasses(String name,
			boolean ignoreCase, EClass eClass) {
		systemClassesLoop: for (org.eclipse.uml2.uml.Class systemClasses : getSystemClasses()) {
			if (eClass != null && !eClass.isInstance(systemClasses))
				continue systemClassesLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(systemClasses.getName()) : name.equals(systemClasses.getName())))
				continue systemClassesLoop;
			return systemClasses;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Class> getEnvironmentClasses() {
		if (environmentClasses == null) {
			environmentClasses = new EObjectResolvingEList<org.eclipse.uml2.uml.Class>(org.eclipse.uml2.uml.Class.class, this, UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES);
		}
		return environmentClasses;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getEnvironmentClasses(String name) {
		return getEnvironmentClasses(name, false, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getEnvironmentClasses(String name,
			boolean ignoreCase, EClass eClass) {
		environmentClassesLoop: for (org.eclipse.uml2.uml.Class environmentClasses : getEnvironmentClasses()) {
			if (eClass != null && !eClass.isInstance(environmentClasses))
				continue environmentClassesLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(environmentClasses.getName()) : name.equals(environmentClasses.getName())))
				continue environmentClassesLoop;
			return environmentClasses;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getUmlPackages() {
		if (umlPackages == null) {
			umlPackages = new EObjectResolvingEList<org.eclipse.uml2.uml.Package>(org.eclipse.uml2.uml.Package.class, this, UtilPackage.RUNTIME_UTIL__UML_PACKAGES);
		}
		return umlPackages;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUmlPackages(String name) {
		return getUmlPackages(name, false, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUmlPackages(String name,
			boolean ignoreCase, EClass eClass) {
		umlPackagesLoop: for (org.eclipse.uml2.uml.Package umlPackages : getUmlPackages()) {
			if (eClass != null && !eClass.isInstance(umlPackages))
				continue umlPackagesLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(umlPackages.getName()) : name.equals(umlPackages.getName())))
				continue umlPackagesLoop;
			return umlPackages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<org.eclipse.uml2.uml.Package, EList<Collaboration>> getPackageToCollaborationsMap() {
		if (packageToCollaborationsMap == null) {
			packageToCollaborationsMap = new EcoreEMap<org.eclipse.uml2.uml.Package,EList<Collaboration>>(UtilPackage.Literals.PACKAGE_TO_COLLABORATION_MAP_ENTRY, PackageToCollaborationMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP);
		}
		return packageToCollaborationsMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interaction> getInteractions() {
		if (interactions == null) {
			interactions = new EObjectResolvingEList<Interaction>(Interaction.class, this, UtilPackage.RUNTIME_UTIL__INTERACTIONS);
		}
		return interactions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteractions(String name) {
		return getInteractions(name, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteractions(String name, boolean ignoreCase) {
		interactionsLoop: for (Interaction interactions : getInteractions()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(interactions.getName()) : name.equals(interactions.getName())))
				continue interactionsLoop;
			return interactions;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getIntegratedPackage() {
		if (integratedPackage != null && integratedPackage.eIsProxy()) {
			InternalEObject oldIntegratedPackage = (InternalEObject)integratedPackage;
			integratedPackage = (org.eclipse.uml2.uml.Package)eResolveProxy(oldIntegratedPackage);
			if (integratedPackage != oldIntegratedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE, oldIntegratedPackage, integratedPackage));
			}
		}
		return integratedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetIntegratedPackage() {
		return integratedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegratedPackage(
			org.eclipse.uml2.uml.Package newIntegratedPackage) {
		org.eclipse.uml2.uml.Package oldIntegratedPackage = integratedPackage;
		integratedPackage = newIntegratedPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE, oldIntegratedPackage, integratedPackage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getEnvironmentSendableMessages() {
		if (environmentSendableMessages == null) {
			environmentSendableMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES);
		}
		return environmentSendableMessages;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Message getEnvironmentSendableMessages(String name) {
		return getEnvironmentSendableMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Message getEnvironmentSendableMessages(String name,
			boolean ignoreCase) {
		environmentSendableMessagesLoop: for (Message environmentSendableMessages : getEnvironmentSendableMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(environmentSendableMessages.getName()) : name.equals(environmentSendableMessages.getName())))
				continue environmentSendableMessagesLoop;
			return environmentSendableMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getSystemSendableMessages() {
		if (systemSendableMessages == null) {
			systemSendableMessages = new EObjectResolvingEList<Message>(Message.class, this, UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES);
		}
		return systemSendableMessages;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Message getSystemSendableMessages(String name) {
		return getSystemSendableMessages(name, false);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Message getSystemSendableMessages(String name, boolean ignoreCase) {
		systemSendableMessagesLoop: for (Message systemSendableMessages : getSystemSendableMessages()) {
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(systemSendableMessages.getName()) : name.equals(systemSendableMessages.getName())))
				continue systemSendableMessagesLoop;
			return systemSendableMessages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getUseCaseSpecificationsToBeConsidered() {
		if (useCaseSpecificationsToBeConsidered == null) {
			useCaseSpecificationsToBeConsidered = new EObjectResolvingEList<org.eclipse.uml2.uml.Package>(org.eclipse.uml2.uml.Package.class, this, UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED);
		}
		return useCaseSpecificationsToBeConsidered;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(
			String name) {
		return getUseCaseSpecificationsToBeConsidered(name, false, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUseCaseSpecificationsToBeConsidered(
			String name, boolean ignoreCase, EClass eClass) {
		useCaseSpecificationsToBeConsideredLoop: for (org.eclipse.uml2.uml.Package useCaseSpecificationsToBeConsidered : getUseCaseSpecificationsToBeConsidered()) {
			if (eClass != null && !eClass.isInstance(useCaseSpecificationsToBeConsidered))
				continue useCaseSpecificationsToBeConsideredLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(useCaseSpecificationsToBeConsidered.getName()) : name.equals(useCaseSpecificationsToBeConsidered.getName())))
				continue useCaseSpecificationsToBeConsideredLoop;
			return useCaseSpecificationsToBeConsidered;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OCLRegistry getOclRegistry() {
		if (oclRegistry != null && oclRegistry.eIsProxy()) {
			InternalEObject oldOclRegistry = (InternalEObject)oclRegistry;
			oclRegistry = (OCLRegistry)eResolveProxy(oldOclRegistry);
			if (oclRegistry != oldOclRegistry) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.RUNTIME_UTIL__OCL_REGISTRY, oldOclRegistry, oclRegistry));
			}
		}
		return oclRegistry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OCLRegistry basicGetOclRegistry() {
		return oclRegistry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOclRegistry(OCLRegistry newOclRegistry) {
		OCLRegistry oldOclRegistry = oclRegistry;
		oclRegistry = newOclRegistry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__OCL_REGISTRY, oldOclRegistry, oclRegistry));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph getMsdRuntimeStateGraph() {
		if (msdRuntimeStateGraph != null && msdRuntimeStateGraph.eIsProxy()) {
			InternalEObject oldMsdRuntimeStateGraph = (InternalEObject)msdRuntimeStateGraph;
			msdRuntimeStateGraph = (MSDRuntimeStateGraph)eResolveProxy(oldMsdRuntimeStateGraph);
			if (msdRuntimeStateGraph != oldMsdRuntimeStateGraph) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, oldMsdRuntimeStateGraph, msdRuntimeStateGraph));
			}
		}
		return msdRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MSDRuntimeStateGraph basicGetMsdRuntimeStateGraph() {
		return msdRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMsdRuntimeStateGraph(
			MSDRuntimeStateGraph newMsdRuntimeStateGraph, NotificationChain msgs) {
		MSDRuntimeStateGraph oldMsdRuntimeStateGraph = msdRuntimeStateGraph;
		msdRuntimeStateGraph = newMsdRuntimeStateGraph;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, oldMsdRuntimeStateGraph, newMsdRuntimeStateGraph);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsdRuntimeStateGraph(
			MSDRuntimeStateGraph newMsdRuntimeStateGraph) {
		if (newMsdRuntimeStateGraph != msdRuntimeStateGraph) {
			NotificationChain msgs = null;
			if (msdRuntimeStateGraph != null)
				msgs = ((InternalEObject)msdRuntimeStateGraph).eInverseRemove(this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, MSDRuntimeStateGraph.class, msgs);
			if (newMsdRuntimeStateGraph != null)
				msgs = ((InternalEObject)newMsdRuntimeStateGraph).eInverseAdd(this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, MSDRuntimeStateGraph.class, msgs);
			msgs = basicSetMsdRuntimeStateGraph(newMsdRuntimeStateGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH, newMsdRuntimeStateGraph, newMsdRuntimeStateGraph));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionFragment getFinalFragment() {
		if (finalFragment != null && finalFragment.eIsProxy()) {
			InternalEObject oldFinalFragment = (InternalEObject)finalFragment;
			finalFragment = (InteractionFragment)eResolveProxy(oldFinalFragment);
			if (finalFragment != oldFinalFragment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT, oldFinalFragment, finalFragment));
			}
		}
		return finalFragment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionFragment basicGetFinalFragment() {
		return finalFragment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalFragment(InteractionFragment newFinalFragment) {
		InteractionFragment oldFinalFragment = finalFragment;
		finalFragment = newFinalFragment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT, oldFinalFragment, finalFragment));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Interaction, MSDUtil> getInteractionToMSDUtilMap() {
		if (interactionToMSDUtilMap == null) {
			interactionToMSDUtilMap = new EcoreEMap<Interaction,MSDUtil>(UtilPackage.Literals.INTERACTION_TO_MSD_UTIL_MAP_ENTRY, InteractionToMSDUtilMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP);
		}
		return interactionToMSDUtilMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> getInteractionOperandToLifelineToInteractionFragmentsMapMap() {
		if (interactionOperandToLifelineToInteractionFragmentsMapMap == null) {
			interactionOperandToLifelineToInteractionFragmentsMapMap = new EcoreEMap<InteractionOperand,EMap<Lifeline, EList<InteractionFragment>>>(UtilPackage.Literals.INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY, InteractionOperandToLifelineToInteractionFragmentsMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP);
		}
		return interactionOperandToLifelineToInteractionFragmentsMapMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StateMachine> getStateMachines() {
		if (stateMachines == null) {
			stateMachines = new EObjectResolvingEList<StateMachine>(StateMachine.class, this, UtilPackage.RUNTIME_UTIL__STATE_MACHINES);
		}
		return stateMachines;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine getStateMachines(String name) {
		return getStateMachines(name, false, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine getStateMachines(String name, boolean ignoreCase,
			EClass eClass) {
		stateMachinesLoop: for (StateMachine stateMachines : getStateMachines()) {
			if (eClass != null && !eClass.isInstance(stateMachines))
				continue stateMachinesLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(stateMachines.getName()) : name.equals(stateMachines.getName())))
				continue stateMachinesLoop;
			return stateMachines;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<StateMachine, State> getStateMachineToInitialStateMap() {
		if (stateMachineToInitialStateMap == null) {
			stateMachineToInitialStateMap = new EcoreEMap<StateMachine,State>(UtilPackage.Literals.STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY, StateMachineToInitialStateMapEntryImpl.class, this, UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP);
		}
		return stateMachineToInitialStateMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void init() {
		setOclRegistry(UtilFactory.eINSTANCE.createOCLRegistry());
		Helper.resetObjectNumbers();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void init(
			UmlPackage2EPackage rootUmlPackage2EPackage,
			EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered) {

		setRootUMLPackageToEPackage(rootUmlPackage2EPackage);

		getUseCaseSpecificationsToBeConsidered().addAll(
				useCaseSpecificationsToBeConsidered);

		// set integrated package
		setIntegratedPackage(getRootUMLPackageToEPackage().getUmlPackage());

		// create and init UML2Ecore mapping
		setUml2ecoreMapping(UtilFactory.eINSTANCE.createUML2EcoreMapping());
		getUml2ecoreMapping().init(rootUmlPackage2EPackage);

		// init UML element lists/maps
		initUMLPackageDataStructures(getIntegratedPackage());

		// assertion
		for (Interaction interactions : getInteractions()) {
			for (Lifeline lifeline : interactions.getLifelines()) {
				EList<InteractionFragment> fragments = getLifelineToInteractionFragmentsMap()
						.get(lifeline);
				Assert.isTrue(fragments.get(fragments.size() - 1) == getFinalFragment());
			}
		}

		// set the StaticRoleToEObjectBindings if static role-to-eObject
		// mappings are provided.
		// if (getRolesToEObjectsMappingContainer() != null) {
		// for (Lifeline lifeline : getLifelineToInteractionFragmentsMap()
		// .keySet()) {
		// Property role = (Property) lifeline.getRepresents();
		// // add entries to the StaticLifelineToEObjectBindings map there
		// // are role-to-eObject mappings set.
		// for (RoleToEObjectMapping roleToEObjectMapping :
		// getRolesToEObjectsMappingContainer()
		// .getRoleToEObjects()) {
		// if (roleToEObjectMapping.getRole().equals(role)) {
		// logger.debug("Lifeline "
		// + lifeline.getRepresents().getName()
		// + " of MSD "
		// + lifeline.getInteraction().getName()
		// + " is statically bound to object "
		// + roleToEObjectMapping.getEObject());
		// getUml2ecoreMapping().getStaticRoleToEObjectBindings()
		// .put(role, roleToEObjectMapping.getEObject());
		// }
		// }
		// }
		// }

		// determine environment and system classes.
		for (org.eclipse.uml2.uml.Package umlPackage : getPackageToCollaborationsMap()
				.keySet()) {
			for (Collaboration collaboration : getPackageToCollaborationsMap()
					.get(umlPackage)) {
				for (Property role : collaboration.getOwnedAttributes()) {
					if (Helper.isEnvironmentProperty(role)) {
						getEnvironmentClasses().add(
								(org.eclipse.uml2.uml.Class) role.getType());
					} else {
						getSystemClasses().add(
								(org.eclipse.uml2.uml.Class) role.getType());
					}
				}
			}
		}

		// create MSDUtils
		for (Interaction interaction : getInteractions()) {
			MSDUtil msdUtil = UtilFactory.eINSTANCE.createMSDUtil();
			getInteractionToMSDUtilMap().put(interaction, msdUtil);
			msdUtil.setEnvironmentAssumption(Helper
					.isEnvironmentAssumption(interaction));
			for (Message message : interaction.getMessages()) {
				// if it is a forbidden message
				if (Helper.isForbidden(message)) {
					if (Helper.isHot(message))
						msdUtil.getHotForbiddenMessages().add(message);
					else
						msdUtil.getColdForbiddenMessages().add(message);
					continue; // in any case it's anymore a regular hot/cold
								// executed/monitored message in the diagram
				}
				if (Helper.isHot(message))
					msdUtil.getHotMessages().add(message);
				if (Helper.isExecuted(message))
					msdUtil.getExecutedMessages().add(message);
				if (Helper.isEnvironmentMessage(message)) {
					msdUtil.getEnvironmentMessages().add(message);
				}
			}

			// TODO: cache further MSD-specific information in the MSDUtil.
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @author tgutjahr
	 * @generated NOT
	 */
	public void init(
			UmlPackage2EPackage rootUmlPackage2EPackage,
			EList<org.eclipse.uml2.uml.Package> useCaseSpecificationsToBeConsidered,
			org.eclipse.uml2.uml.Package strategyPackage) {
		
		initUMLPackageDataStructures(strategyPackage);
		init(rootUmlPackage2EPackage, useCaseSpecificationsToBeConsidered);		
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EOperation getOperation(Message message) {
		return getUml2ecoreMapping().getUmlOperationToEOperationMap().get(
				message.getSignature());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EStructuralFeature getEStructuralFeature(Message message) {
		return getUml2ecoreMapping().getUmlOperationToEStructuralFeatureMap()
				.get(message.getSignature());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EStructuralFeature getEStructuralFeature(Transition transition) {
		return getUml2ecoreMapping().getUmlOperationToEStructuralFeatureMap()
				.get(getOperationForTransition(transition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnvironmentRuntimeMessage(RuntimeMessage runtimeMessage) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnvironmentMessageEvent(MessageEvent messageEvent) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object getParameterValue(Message message) {
		return getParameterValue(message,
				OCL.newInstance(new EcoreEnvironmentFactory(
						EPackage.Registry.INSTANCE)));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object getParameterValue(Message message, OCL oclForCut) {
		if (!message.getArguments().isEmpty()) {
			ValueSpecification valueSpecification = message.getArguments().get(
					0);

			if (valueSpecification instanceof LiteralString) {
				LiteralString literalString = (LiteralString) valueSpecification;
				try {
					OCL.Helper helper = oclForCut.createOCLHelper();
					helper.setContext(org.eclipse.emf.ecore.EcoreFactory.eINSTANCE
							.createEClass());
					OCLExpression expr = helper.createQuery(literalString
							.getValue());
					return oclForCut.evaluate(null, expr);
				} catch (ParserException e) {
					if (e instanceof SemanticException
							&& e.getMessage().contains("Unrecognized variable"))
						logger.debug(e);
					else
						logger.error(e);
				}
			} else if (valueSpecification instanceof LiteralBoolean) {
				LiteralBoolean literalBoolean = (LiteralBoolean) valueSpecification;
				return new Boolean(literalBoolean.isValue());
			} else if (valueSpecification instanceof LiteralInteger) {
				LiteralInteger literalInteger = (LiteralInteger) valueSpecification;
				return new Integer(literalInteger.getValue());
			}
		}
		return null;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object getParameterValue(Message message, ObjectSystem objectSystem) {

		if (!message.getArguments().isEmpty()) {
			ValueSpecification valueSpecification = message.getArguments().get(
					0);

			if (valueSpecification instanceof LiteralString) {
				LiteralString literalString = (LiteralString) valueSpecification;
				try {
					OCL oclForCut = OCL.newInstance(new EcoreEnvironmentFactory(
							EPackage.Registry.INSTANCE));
					OCL.Helper helper = oclForCut.createOCLHelper();
//					helper.setContext(org.eclipse.emf.ecore.EcoreFactory.eINSTANCE
//							.createEClass());
					EObject context = objectSystem.getRootObjects().get(0);
					
					EClass eClass = context.eClass();
					helper.setContext(eClass);
					OCLExpression expr = helper.createQuery(literalString
							.getValue());
					return oclForCut.evaluate(context, expr);
				} catch (ParserException e) {
					if (e instanceof SemanticException
							&& e.getMessage().contains("Unrecognized variable"))
						logger.debug(e);
					else
						logger.error(e);
				}
			} else if (valueSpecification instanceof LiteralBoolean) {
				LiteralBoolean literalBoolean = (LiteralBoolean) valueSpecification;
				return new Boolean(literalBoolean.isValue());
			} else if (valueSpecification instanceof LiteralInteger) {
				LiteralInteger literalInteger = (LiteralInteger) valueSpecification;
				return new Integer(literalInteger.getValue());
			}
		}
		return null;
	}
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object getParameterValue(Message message, OCL oclForCut, ObjectSystem objectSystem) {
		if (!message.getArguments().isEmpty()) {
			ValueSpecification valueSpecification = message.getArguments().get(
					0);

			if (valueSpecification instanceof LiteralString) {
				LiteralString literalString = (LiteralString) valueSpecification;
				try {
					OCL oclForCutX = OCL.newInstance(new EcoreEnvironmentFactory(
							EPackage.Registry.INSTANCE));
					OCL.Helper helper = oclForCutX.createOCLHelper();
//					helper.setContext(org.eclipse.emf.ecore.EcoreFactory.eINSTANCE
//							.createEClass());
					
//					EObject eObjectBoundByLifeline = getLifelineBindings().getLifelineToEObjectMap().get(lifeline);
					EObject context = ((MessageOccurrenceSpecification) message.getReceiveEvent()).getCovered().getRepresents();
							
//					EObject context = objectSystem.getRootObjects().get(0);
					
					EClass eClass = context.eClass();
					helper.setContext(eClass);
					OCLExpression expr = helper.createQuery(literalString
							.getValue());
					return oclForCutX.evaluate(context, expr);
				} catch (ParserException e) {
					if (e instanceof SemanticException
							&& e.getMessage().contains("Unrecognized variable"))
						logger.debug(e);
					else
						logger.error(e);
				}
			} else if (valueSpecification instanceof LiteralBoolean) {
				LiteralBoolean literalBoolean = (LiteralBoolean) valueSpecification;
				return new Boolean(literalBoolean.isValue());
			} else if (valueSpecification instanceof LiteralInteger) {
				LiteralInteger literalInteger = (LiteralInteger) valueSpecification;
				return new Integer(literalInteger.getValue());
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isNonSpontaneousEnvironmentMessageEvent(
			MessageEvent messageEvent) {
		for (Operation umlOperation : getUml2ecoreMapping()
				.getEOperation2UMLOperationMap().get(
						messageEvent.getOperation())) {
			if (RuntimeUtil.Helper
					.isNonSpontaneousEnvironmentReaction(umlOperation))
				return true;
		}
		return false;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				if (msdRuntimeStateGraph != null)
					msgs = ((InternalEObject)msdRuntimeStateGraph).eInverseRemove(this, RuntimePackage.MSD_RUNTIME_STATE_GRAPH__RUNTIME_UTIL, MSDRuntimeStateGraph.class, msgs);
				return basicSetMsdRuntimeStateGraph((MSDRuntimeStateGraph)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	private void initUMLCollaborationDataStructures(Collaboration collaboration) {
		if (getPackageToCollaborationsMap().get(collaboration.getPackage()) == null) {
			getPackageToCollaborationsMap().put(collaboration.getPackage(),
					new BasicEList<Collaboration>());
		}
		getPackageToCollaborationsMap().get(collaboration.getPackage()).add(
				collaboration);
		for (Behavior behavior : collaboration.getOwnedBehaviors()) {
			if (behavior instanceof Interaction) {
				initUMLInteractionDataStructures((Interaction) behavior);
			} else if (behavior instanceof StateMachine) {
				initUMLStateMachineDataStructures((StateMachine) behavior);
			}
		}
	}

	private void initUMLStateMachineDataStructures(StateMachine statemachine) {
		EList<Region> regions = statemachine.getRegions();
		// we support only one region at the moment
		if (regions.size() == 1) {
			State initialState = null;
			Region region = regions.get(0);
			for (Vertex subvertex : region.getSubvertices()) {
				if (subvertex instanceof Pseudostate) {
					Pseudostate pseudostate = ((Pseudostate) subvertex);
					// we support only one initial state with one outgoing
					// transition
					if (pseudostate.getKind() == PseudostateKind.INITIAL_LITERAL
							&& pseudostate.getOutgoings().size() > 0) {
						initialState = (State) pseudostate.getOutgoings()
								.get(0).getTarget();
						getStateMachineToInitialStateMap().put(statemachine,
								initialState);
					}
				}
			}
			if(initialState!=null){
				initInitalMessagesDataStructures(initialState);
				getStateMachines().add(statemachine);
			}
		}
	}

	private void initInitalMessagesDataStructures(State initialState) {
		for (Transition transition : initialState.getOutgoings()) {
//			System.out.println(transition.getLabel());
			Operation operation = getOperationForTransition(transition);
//			System.out.println(operation);
			// add operations of outgoing transitions of initial state to
			// map
			if (getFirstTransitionOperationToStateMachineMap().get(
					operation) == null) {
				getFirstTransitionOperationToStateMachineMap().put(
						operation, new UniqueEList<StateMachine>());
			}
			getFirstTransitionOperationToStateMachineMap().get(operation)
					.add(initialState.containingStateMachine());
		}
	}

	private Operation getOperationForTransition(Transition transition) {
		return getOperationForString(Helper.getReceiver(transition),
				transition.getLabel());
	}

	/**
	 * @generated NOT
	 */
	public EOperation getOperation(Transition transition) {
		return getUml2ecoreMapping().getUmlOperationToEOperationMap().get(
				getOperationForTransition(transition));
	}

	private Operation getOperationForString(Property receiver,
			String operationString) {
		for (Operation operation : ((org.eclipse.uml2.uml.Class) receiver
				.getType()).getOperations())
			if (operation.getName().equals(operationString))
				return operation;
		return null;
	}

	protected void initUMLInteractionDataStructures(Interaction interaction) {
		getInteractions().add(interaction);

		// extract initial message from MSD:
		// find first messageOccurrenceSpecification

		Message minimalMessage = Helper.getFirstMessageForMSD(interaction);
		Operation representedOperation = (Operation) minimalMessage
				.getSignature();
		if (getFirstMessageOperationToInteractionMap()
				.get(representedOperation) == null) {
			getFirstMessageOperationToInteractionMap().put(
					representedOperation, new UniqueEList<Interaction>());
		}
		getFirstMessageOperationToInteractionMap().get(representedOperation)
				.add(interaction);

		for (Message message : interaction.getMessages()) {
			// map objects to operations of messages that it can receive

			// collect messages the environment can trigger
			if (Helper
					.isEnvironmentLifeline(((MessageOccurrenceSpecification) message
							.getSendEvent()).getCovereds().get(0))) {
				getEnvironmentSendableMessages().add(message);
			}
		}

		addInteractionFragmentsToMaps(interaction.getFragments(),
				getLifelineToInteractionFragmentsMap());
		for (Lifeline lifeline : interaction.getLifelines()) {
			Property property = (Property) lifeline.getRepresents();
			org.eclipse.uml2.uml.Class umlClass = (org.eclipse.uml2.uml.Class) property
					.getType();

			if (Helper.isEnvironmentLifeline(lifeline)) {
				getEnvironmentClasses().add(umlClass);
			} else {
				getSystemClasses().add(umlClass);
			}

			// append a FINAL dummy fragment to the interaction fragments list
			// for interaction's lifeline -- makes it easier later on to
			// progress to and determine the termination of an active MSD.
			getLifelineToInteractionFragmentsMap().get(lifeline).add(
					getFinalFragment());

		}

	}

	private void addInteractionFragmentsToMaps(
			EList<InteractionFragment> fragments,
			EMap<Lifeline, EList<InteractionFragment>> l2IFmap) {
		for (InteractionFragment interactionFragment : fragments) {
			for (Lifeline lifeline : interactionFragment.getCovereds()) {
				if (interactionFragment instanceof CombinedFragment
						&& ((CombinedFragment) interactionFragment)
								.getInteractionOperator() == InteractionOperatorKind.ALT_LITERAL) {
					for (InteractionOperand interactionOperand : ((CombinedFragment) interactionFragment)
							.getOperands()) {
						EMap<Lifeline, EList<InteractionFragment>> operandL2IFmap = getInteractionOperandToLifelineToInteractionFragmentsMapMap()
								.get(interactionOperand);
						if (operandL2IFmap == null) {
							operandL2IFmap = new EcoreEMap<Lifeline, EList<InteractionFragment>>(
									UtilPackage.Literals.LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY,
									LifelineToInteractionFragmentsMapEntryImpl.class,
									this,
									UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP);
							getInteractionOperandToLifelineToInteractionFragmentsMapMap()
									.put(interactionOperand, operandL2IFmap);
						}
						addInteractionFragmentsToMaps(
								interactionOperand.getFragments(),
								operandL2IFmap);
					}
				}
				EList<InteractionFragment> lifelineInteractionFragmentList = l2IFmap
						.get(lifeline);
				if (lifelineInteractionFragmentList == null) {
					lifelineInteractionFragmentList = new BasicEList<InteractionFragment>();
					l2IFmap.put(lifeline, lifelineInteractionFragmentList);
				}
				l2IFmap.get(lifeline).add(interactionFragment);
			}
		}
	}

	protected void initUMLPackageDataStructures(
			org.eclipse.uml2.uml.Package umlPackage) {

		getUmlPackages().add(umlPackage);

		if (getUseCaseSpecificationsToBeConsidered().isEmpty()
				|| getUseCaseSpecificationsToBeConsidered()
						.contains(umlPackage)) {
			for (Element umlElement : umlPackage.getOwnedElements()) {
				if (umlElement instanceof Collaboration) {
					initUMLCollaborationDataStructures((Collaboration) umlElement);
				}
			}
		}

		for (PackageMerge packageMerge : umlPackage.getPackageMerges()) {
			initUMLPackageDataStructures(packageMerge.getMergedPackage());
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
				return basicSetUml2ecoreMapping(null, msgs);
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
				return ((InternalEList<?>)getFirstMessageOperationToInteractionMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
				return ((InternalEList<?>)getFirstTransitionOperationToStateMachineMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
				return ((InternalEList<?>)getInteractionToStringToLifelineMapMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
				return ((InternalEList<?>)getLifelineToInteractionFragmentsMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
				return ((InternalEList<?>)getPackageToCollaborationsMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				return basicSetMsdRuntimeStateGraph(null, msgs);
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
				return ((InternalEList<?>)getInteractionToMSDUtilMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
				return ((InternalEList<?>)getInteractionOperandToLifelineToInteractionFragmentsMapMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				return ((InternalEList<?>)getStateMachineToInitialStateMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
				return getUml2ecoreMapping();
			case UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE:
				if (resolve) return getRootUMLPackageToEPackage();
				return basicGetRootUMLPackageToEPackage();
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
				if (coreType) return getFirstMessageOperationToInteractionMap();
				else return getFirstMessageOperationToInteractionMap().map();
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
				if (coreType) return getFirstTransitionOperationToStateMachineMap();
				else return getFirstTransitionOperationToStateMachineMap().map();
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
				if (coreType) return getInteractionToStringToLifelineMapMap();
				else return getInteractionToStringToLifelineMapMap().map();
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
				if (coreType) return getLifelineToInteractionFragmentsMap();
				else return getLifelineToInteractionFragmentsMap().map();
			case UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES:
				return getSystemClasses();
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES:
				return getEnvironmentClasses();
			case UtilPackage.RUNTIME_UTIL__UML_PACKAGES:
				return getUmlPackages();
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
				if (coreType) return getPackageToCollaborationsMap();
				else return getPackageToCollaborationsMap().map();
			case UtilPackage.RUNTIME_UTIL__INTERACTIONS:
				return getInteractions();
			case UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE:
				if (resolve) return getIntegratedPackage();
				return basicGetIntegratedPackage();
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES:
				return getEnvironmentSendableMessages();
			case UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES:
				return getSystemSendableMessages();
			case UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				return getUseCaseSpecificationsToBeConsidered();
			case UtilPackage.RUNTIME_UTIL__OCL_REGISTRY:
				if (resolve) return getOclRegistry();
				return basicGetOclRegistry();
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				if (resolve) return getMsdRuntimeStateGraph();
				return basicGetMsdRuntimeStateGraph();
			case UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT:
				if (resolve) return getFinalFragment();
				return basicGetFinalFragment();
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
				if (coreType) return getInteractionToMSDUtilMap();
				else return getInteractionToMSDUtilMap().map();
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
				if (coreType) return getInteractionOperandToLifelineToInteractionFragmentsMapMap();
				else return getInteractionOperandToLifelineToInteractionFragmentsMapMap().map();
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINES:
				return getStateMachines();
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				if (coreType) return getStateMachineToInitialStateMap();
				else return getStateMachineToInitialStateMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
				setUml2ecoreMapping((UML2EcoreMapping)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE:
				setRootUMLPackageToEPackage((UmlPackage2EPackage)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
				((EStructuralFeature.Setting)getFirstMessageOperationToInteractionMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
				((EStructuralFeature.Setting)getFirstTransitionOperationToStateMachineMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
				((EStructuralFeature.Setting)getInteractionToStringToLifelineMapMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
				((EStructuralFeature.Setting)getLifelineToInteractionFragmentsMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES:
				getSystemClasses().clear();
				getSystemClasses().addAll((Collection<? extends org.eclipse.uml2.uml.Class>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES:
				getEnvironmentClasses().clear();
				getEnvironmentClasses().addAll((Collection<? extends org.eclipse.uml2.uml.Class>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__UML_PACKAGES:
				getUmlPackages().clear();
				getUmlPackages().addAll((Collection<? extends org.eclipse.uml2.uml.Package>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
				((EStructuralFeature.Setting)getPackageToCollaborationsMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTIONS:
				getInteractions().clear();
				getInteractions().addAll((Collection<? extends Interaction>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE:
				setIntegratedPackage((org.eclipse.uml2.uml.Package)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES:
				getEnvironmentSendableMessages().clear();
				getEnvironmentSendableMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES:
				getSystemSendableMessages().clear();
				getSystemSendableMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				getUseCaseSpecificationsToBeConsidered().clear();
				getUseCaseSpecificationsToBeConsidered().addAll((Collection<? extends org.eclipse.uml2.uml.Package>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__OCL_REGISTRY:
				setOclRegistry((OCLRegistry)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT:
				setFinalFragment((InteractionFragment)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
				((EStructuralFeature.Setting)getInteractionToMSDUtilMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
				((EStructuralFeature.Setting)getInteractionOperandToLifelineToInteractionFragmentsMapMap()).set(newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINES:
				getStateMachines().clear();
				getStateMachines().addAll((Collection<? extends StateMachine>)newValue);
				return;
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				((EStructuralFeature.Setting)getStateMachineToInitialStateMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
				setUml2ecoreMapping((UML2EcoreMapping)null);
				return;
			case UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE:
				setRootUMLPackageToEPackage((UmlPackage2EPackage)null);
				return;
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
				getFirstMessageOperationToInteractionMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
				getFirstTransitionOperationToStateMachineMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
				getInteractionToStringToLifelineMapMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
				getLifelineToInteractionFragmentsMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES:
				getSystemClasses().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES:
				getEnvironmentClasses().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__UML_PACKAGES:
				getUmlPackages().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
				getPackageToCollaborationsMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTIONS:
				getInteractions().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE:
				setIntegratedPackage((org.eclipse.uml2.uml.Package)null);
				return;
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES:
				getEnvironmentSendableMessages().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES:
				getSystemSendableMessages().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				getUseCaseSpecificationsToBeConsidered().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__OCL_REGISTRY:
				setOclRegistry((OCLRegistry)null);
				return;
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				setMsdRuntimeStateGraph((MSDRuntimeStateGraph)null);
				return;
			case UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT:
				setFinalFragment((InteractionFragment)null);
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
				getInteractionToMSDUtilMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
				getInteractionOperandToLifelineToInteractionFragmentsMapMap().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINES:
				getStateMachines().clear();
				return;
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				getStateMachineToInitialStateMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilPackage.RUNTIME_UTIL__UML2ECORE_MAPPING:
				return uml2ecoreMapping != null;
			case UtilPackage.RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE:
				return rootUMLPackageToEPackage != null;
			case UtilPackage.RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP:
				return firstMessageOperationToInteractionMap != null && !firstMessageOperationToInteractionMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP:
				return firstTransitionOperationToStateMachineMap != null && !firstTransitionOperationToStateMachineMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP:
				return interactionToStringToLifelineMapMap != null && !interactionToStringToLifelineMapMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP:
				return lifelineToInteractionFragmentsMap != null && !lifelineToInteractionFragmentsMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__SYSTEM_CLASSES:
				return systemClasses != null && !systemClasses.isEmpty();
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_CLASSES:
				return environmentClasses != null && !environmentClasses.isEmpty();
			case UtilPackage.RUNTIME_UTIL__UML_PACKAGES:
				return umlPackages != null && !umlPackages.isEmpty();
			case UtilPackage.RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP:
				return packageToCollaborationsMap != null && !packageToCollaborationsMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__INTERACTIONS:
				return interactions != null && !interactions.isEmpty();
			case UtilPackage.RUNTIME_UTIL__INTEGRATED_PACKAGE:
				return integratedPackage != null;
			case UtilPackage.RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES:
				return environmentSendableMessages != null && !environmentSendableMessages.isEmpty();
			case UtilPackage.RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES:
				return systemSendableMessages != null && !systemSendableMessages.isEmpty();
			case UtilPackage.RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED:
				return useCaseSpecificationsToBeConsidered != null && !useCaseSpecificationsToBeConsidered.isEmpty();
			case UtilPackage.RUNTIME_UTIL__OCL_REGISTRY:
				return oclRegistry != null;
			case UtilPackage.RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH:
				return msdRuntimeStateGraph != null;
			case UtilPackage.RUNTIME_UTIL__FINAL_FRAGMENT:
				return finalFragment != null;
			case UtilPackage.RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP:
				return interactionToMSDUtilMap != null && !interactionToMSDUtilMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP:
				return interactionOperandToLifelineToInteractionFragmentsMapMap != null && !interactionOperandToLifelineToInteractionFragmentsMapMap.isEmpty();
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINES:
				return stateMachines != null && !stateMachines.isEmpty();
			case UtilPackage.RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP:
				return stateMachineToInitialStateMap != null && !stateMachineToInitialStateMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public MessageEvent createMessageEvent(EObject sendingObject,
			EObject receivingObject) {
		return EventsFactory.eINSTANCE.createSynchronousMessageEvent();
	}

} // RuntimeUtilImpl
