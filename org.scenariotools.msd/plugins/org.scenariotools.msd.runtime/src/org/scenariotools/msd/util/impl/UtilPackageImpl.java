/**
 */
package org.scenariotools.msd.util.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;
import org.scenariotools.msd.runtime.impl.RuntimePackageImpl;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.scenariorunconfiguration.impl.ScenariorunconfigurationPackageImpl;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilFactory;
import org.scenariotools.msd.util.UtilPackage;
import org.scenariotools.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UtilPackageImpl extends EPackageImpl implements UtilPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeUtilEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uml2EcoreMappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlClassToEClassMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eClassToUMLClassMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eOperation2UMLOperationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlOperationToEOperationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eParameterToUMLParameterMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlParameterToEParameterMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eStructuralFeatureToUMLOperationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlOperationToEStructuralFeatureMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToLifelineMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass firstMessageOperationToInteractionMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass firstTransitionOperationToStateMachineMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lifelineToInteractionFragmentsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageToCollaborationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionToStringToLifelineMapMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oclRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msdUtilEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionToMSDUtilMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionOperandToLifelineToInteractionFragmentsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateMachineToInitialStateMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass idToOCLMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType plainJavaObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType oclEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.util.UtilPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UtilPackageImpl() {
		super(eNS_URI, UtilFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UtilPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UtilPackage init() {
		if (isInited) return (UtilPackage)EPackage.Registry.INSTANCE.getEPackage(UtilPackage.eNS_URI);

		// Obtain or create and register package
		UtilPackageImpl theUtilPackage = (UtilPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UtilPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UtilPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Roles2eobjectsPackage.eINSTANCE.eClass();
		RuntimePackage.eINSTANCE.eClass();
		Uml2ecorePackage.eINSTANCE.eClass();
		PrimitivemappingsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		RuntimePackageImpl theRuntimePackage_1 = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI) : org.scenariotools.msd.runtime.RuntimePackage.eINSTANCE);
		ScenariorunconfigurationPackageImpl theScenariorunconfigurationPackage = (ScenariorunconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI) instanceof ScenariorunconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI) : ScenariorunconfigurationPackage.eINSTANCE);

		// Create package meta-data objects
		theUtilPackage.createPackageContents();
		theRuntimePackage_1.createPackageContents();
		theScenariorunconfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theUtilPackage.initializePackageContents();
		theRuntimePackage_1.initializePackageContents();
		theScenariorunconfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUtilPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UtilPackage.eNS_URI, theUtilPackage);
		return theUtilPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeUtil() {
		return runtimeUtilEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_Uml2ecoreMapping() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_RootUMLPackageToEPackage() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_FirstMessageOperationToInteractionMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_FirstTransitionOperationToStateMachineMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_InteractionToStringToLifelineMapMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_LifelineToInteractionFragmentsMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_SystemClasses() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_EnvironmentClasses() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_UmlPackages() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_PackageToCollaborationsMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_Interactions() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_IntegratedPackage() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_EnvironmentSendableMessages() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_SystemSendableMessages() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_UseCaseSpecificationsToBeConsidered() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_OclRegistry() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_MsdRuntimeStateGraph() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_FinalFragment() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_InteractionToMSDUtilMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_InteractionOperandToLifelineToInteractionFragmentsMapMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_StateMachines() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeUtil_StateMachineToInitialStateMap() {
		return (EReference)runtimeUtilEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUML2EcoreMapping() {
		return uml2EcoreMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_UmlClassToEClassMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_EClassToUMLClassMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_EOperation2UMLOperationMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_UmlOperationToEOperationMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_EParameterToUMLParameterMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_UmlParameterToEParameterMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_UmlOperationToEStructuralFeatureMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_EStructuralFeatureToUMLOperationMap() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_RootUMLPackageToEPackage() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUML2EcoreMapping_MergedUMLPackages() {
		return (EReference)uml2EcoreMappingEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLClassToEClassMapEntry() {
		return umlClassToEClassMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLClassToEClassMapEntry_Key() {
		return (EReference)umlClassToEClassMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLClassToEClassMapEntry_Value() {
		return (EReference)umlClassToEClassMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEClassToUMLClassMapEntry() {
		return eClassToUMLClassMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToUMLClassMapEntry_Key() {
		return (EReference)eClassToUMLClassMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToUMLClassMapEntry_Value() {
		return (EReference)eClassToUMLClassMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEOperation2UMLOperationMapEntry() {
		return eOperation2UMLOperationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEOperation2UMLOperationMapEntry_Key() {
		return (EReference)eOperation2UMLOperationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEOperation2UMLOperationMapEntry_Value() {
		return (EReference)eOperation2UMLOperationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLOperationToEOperationMapEntry() {
		return umlOperationToEOperationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOperationToEOperationMapEntry_Key() {
		return (EReference)umlOperationToEOperationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOperationToEOperationMapEntry_Value() {
		return (EReference)umlOperationToEOperationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEParameterToUMLParameterMapEntry() {
		return eParameterToUMLParameterMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEParameterToUMLParameterMapEntry_Key() {
		return (EReference)eParameterToUMLParameterMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEParameterToUMLParameterMapEntry_Value() {
		return (EReference)eParameterToUMLParameterMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLParameterToEParameterMapEntry() {
		return umlParameterToEParameterMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLParameterToEParameterMapEntry_Key() {
		return (EReference)umlParameterToEParameterMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLParameterToEParameterMapEntry_Value() {
		return (EReference)umlParameterToEParameterMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEStructuralFeatureToUMLOperationMapEntry() {
		return eStructuralFeatureToUMLOperationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEStructuralFeatureToUMLOperationMapEntry_Key() {
		return (EReference)eStructuralFeatureToUMLOperationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEStructuralFeatureToUMLOperationMapEntry_Value() {
		return (EReference)eStructuralFeatureToUMLOperationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLOperationToEStructuralFeatureMapEntry() {
		return umlOperationToEStructuralFeatureMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOperationToEStructuralFeatureMapEntry_Key() {
		return (EReference)umlOperationToEStructuralFeatureMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOperationToEStructuralFeatureMapEntry_Value() {
		return (EReference)umlOperationToEStructuralFeatureMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToLifelineMapEntry() {
		return stringToLifelineMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToLifelineMapEntry_Value() {
		return (EReference)stringToLifelineMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToLifelineMapEntry_Key() {
		return (EAttribute)stringToLifelineMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFirstMessageOperationToInteractionMapEntry() {
		return firstMessageOperationToInteractionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstMessageOperationToInteractionMapEntry_Value() {
		return (EReference)firstMessageOperationToInteractionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstMessageOperationToInteractionMapEntry_Key() {
		return (EReference)firstMessageOperationToInteractionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFirstTransitionOperationToStateMachineMapEntry() {
		return firstTransitionOperationToStateMachineMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstTransitionOperationToStateMachineMapEntry_Value() {
		return (EReference)firstTransitionOperationToStateMachineMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstTransitionOperationToStateMachineMapEntry_Key() {
		return (EReference)firstTransitionOperationToStateMachineMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLifelineToInteractionFragmentsMapEntry() {
		return lifelineToInteractionFragmentsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToInteractionFragmentsMapEntry_Key() {
		return (EReference)lifelineToInteractionFragmentsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLifelineToInteractionFragmentsMapEntry_Value() {
		return (EReference)lifelineToInteractionFragmentsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageToCollaborationMapEntry() {
		return packageToCollaborationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToCollaborationMapEntry_Value() {
		return (EReference)packageToCollaborationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPackageToCollaborationMapEntry_Key() {
		return (EReference)packageToCollaborationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionToStringToLifelineMapMapEntry() {
		return interactionToStringToLifelineMapMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionToStringToLifelineMapMapEntry_Value() {
		return (EReference)interactionToStringToLifelineMapMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionToStringToLifelineMapMapEntry_Key() {
		return (EReference)interactionToStringToLifelineMapMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyToEObjectMapEntry() {
		return propertyToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyToEObjectMapEntry_Value() {
		return (EReference)propertyToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyToEObjectMapEntry_Key() {
		return (EReference)propertyToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLRegistry() {
		return oclRegistryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOCLRegistry_IdToOCLMap() {
		return (EReference)oclRegistryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSDUtil() {
		return msdUtilEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSDUtil_EnvironmentAssumption() {
		return (EAttribute)msdUtilEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDUtil_HotMessages() {
		return (EReference)msdUtilEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDUtil_ExecutedMessages() {
		return (EReference)msdUtilEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDUtil_EnvironmentMessages() {
		return (EReference)msdUtilEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDUtil_ColdForbiddenMessages() {
		return (EReference)msdUtilEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSDUtil_HotForbiddenMessages() {
		return (EReference)msdUtilEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionToMSDUtilMapEntry() {
		return interactionToMSDUtilMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionToMSDUtilMapEntry_Key() {
		return (EReference)interactionToMSDUtilMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionToMSDUtilMapEntry_Value() {
		return (EReference)interactionToMSDUtilMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionOperandToLifelineToInteractionFragmentsMapEntry() {
		return interactionOperandToLifelineToInteractionFragmentsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Key() {
		return (EReference)interactionOperandToLifelineToInteractionFragmentsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Value() {
		return (EReference)interactionOperandToLifelineToInteractionFragmentsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateMachineToInitialStateMapEntry() {
		return stateMachineToInitialStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateMachineToInitialStateMapEntry_Key() {
		return (EReference)stateMachineToInitialStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateMachineToInitialStateMapEntry_Value() {
		return (EReference)stateMachineToInitialStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIDToOCLMapEntry() {
		return idToOCLMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIDToOCLMapEntry_Key() {
		return (EAttribute)idToOCLMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIDToOCLMapEntry_Value() {
		return (EAttribute)idToOCLMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPlainJavaObject() {
		return plainJavaObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOCL() {
		return oclEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilFactory getUtilFactory() {
		return (UtilFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		runtimeUtilEClass = createEClass(RUNTIME_UTIL);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__UML2ECORE_MAPPING);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__ROOT_UML_PACKAGE_TO_EPACKAGE);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__LIFELINE_TO_INTERACTION_FRAGMENTS_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__SYSTEM_CLASSES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__ENVIRONMENT_CLASSES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__UML_PACKAGES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__PACKAGE_TO_COLLABORATIONS_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__INTERACTIONS);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__INTEGRATED_PACKAGE);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__ENVIRONMENT_SENDABLE_MESSAGES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__SYSTEM_SENDABLE_MESSAGES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__USE_CASE_SPECIFICATIONS_TO_BE_CONSIDERED);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__OCL_REGISTRY);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__MSD_RUNTIME_STATE_GRAPH);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__FINAL_FRAGMENT);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__INTERACTION_TO_MSD_UTIL_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_MAP);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__STATE_MACHINES);
		createEReference(runtimeUtilEClass, RUNTIME_UTIL__STATE_MACHINE_TO_INITIAL_STATE_MAP);

		uml2EcoreMappingEClass = createEClass(UML2_ECORE_MAPPING);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE);
		createEReference(uml2EcoreMappingEClass, UML2_ECORE_MAPPING__MERGED_UML_PACKAGES);

		umlClassToEClassMapEntryEClass = createEClass(UML_CLASS_TO_ECLASS_MAP_ENTRY);
		createEReference(umlClassToEClassMapEntryEClass, UML_CLASS_TO_ECLASS_MAP_ENTRY__KEY);
		createEReference(umlClassToEClassMapEntryEClass, UML_CLASS_TO_ECLASS_MAP_ENTRY__VALUE);

		eClassToUMLClassMapEntryEClass = createEClass(ECLASS_TO_UML_CLASS_MAP_ENTRY);
		createEReference(eClassToUMLClassMapEntryEClass, ECLASS_TO_UML_CLASS_MAP_ENTRY__KEY);
		createEReference(eClassToUMLClassMapEntryEClass, ECLASS_TO_UML_CLASS_MAP_ENTRY__VALUE);

		eOperation2UMLOperationMapEntryEClass = createEClass(EOPERATION2_UML_OPERATION_MAP_ENTRY);
		createEReference(eOperation2UMLOperationMapEntryEClass, EOPERATION2_UML_OPERATION_MAP_ENTRY__KEY);
		createEReference(eOperation2UMLOperationMapEntryEClass, EOPERATION2_UML_OPERATION_MAP_ENTRY__VALUE);

		umlOperationToEOperationMapEntryEClass = createEClass(UML_OPERATION_TO_EOPERATION_MAP_ENTRY);
		createEReference(umlOperationToEOperationMapEntryEClass, UML_OPERATION_TO_EOPERATION_MAP_ENTRY__KEY);
		createEReference(umlOperationToEOperationMapEntryEClass, UML_OPERATION_TO_EOPERATION_MAP_ENTRY__VALUE);

		eParameterToUMLParameterMapEntryEClass = createEClass(EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY);
		createEReference(eParameterToUMLParameterMapEntryEClass, EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__KEY);
		createEReference(eParameterToUMLParameterMapEntryEClass, EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY__VALUE);

		umlParameterToEParameterMapEntryEClass = createEClass(UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY);
		createEReference(umlParameterToEParameterMapEntryEClass, UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__KEY);
		createEReference(umlParameterToEParameterMapEntryEClass, UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY__VALUE);

		eStructuralFeatureToUMLOperationMapEntryEClass = createEClass(ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY);
		createEReference(eStructuralFeatureToUMLOperationMapEntryEClass, ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__KEY);
		createEReference(eStructuralFeatureToUMLOperationMapEntryEClass, ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY__VALUE);

		umlOperationToEStructuralFeatureMapEntryEClass = createEClass(UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY);
		createEReference(umlOperationToEStructuralFeatureMapEntryEClass, UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__KEY);
		createEReference(umlOperationToEStructuralFeatureMapEntryEClass, UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY__VALUE);

		stringToLifelineMapEntryEClass = createEClass(STRING_TO_LIFELINE_MAP_ENTRY);
		createEReference(stringToLifelineMapEntryEClass, STRING_TO_LIFELINE_MAP_ENTRY__VALUE);
		createEAttribute(stringToLifelineMapEntryEClass, STRING_TO_LIFELINE_MAP_ENTRY__KEY);

		firstMessageOperationToInteractionMapEntryEClass = createEClass(FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY);
		createEReference(firstMessageOperationToInteractionMapEntryEClass, FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__VALUE);
		createEReference(firstMessageOperationToInteractionMapEntryEClass, FIRST_MESSAGE_OPERATION_TO_INTERACTION_MAP_ENTRY__KEY);

		firstTransitionOperationToStateMachineMapEntryEClass = createEClass(FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY);
		createEReference(firstTransitionOperationToStateMachineMapEntryEClass, FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__VALUE);
		createEReference(firstTransitionOperationToStateMachineMapEntryEClass, FIRST_TRANSITION_OPERATION_TO_STATE_MACHINE_MAP_ENTRY__KEY);

		lifelineToInteractionFragmentsMapEntryEClass = createEClass(LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY);
		createEReference(lifelineToInteractionFragmentsMapEntryEClass, LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY);
		createEReference(lifelineToInteractionFragmentsMapEntryEClass, LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE);

		packageToCollaborationMapEntryEClass = createEClass(PACKAGE_TO_COLLABORATION_MAP_ENTRY);
		createEReference(packageToCollaborationMapEntryEClass, PACKAGE_TO_COLLABORATION_MAP_ENTRY__VALUE);
		createEReference(packageToCollaborationMapEntryEClass, PACKAGE_TO_COLLABORATION_MAP_ENTRY__KEY);

		interactionToStringToLifelineMapMapEntryEClass = createEClass(INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY);
		createEReference(interactionToStringToLifelineMapMapEntryEClass, INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__VALUE);
		createEReference(interactionToStringToLifelineMapMapEntryEClass, INTERACTION_TO_STRING_TO_LIFELINE_MAP_MAP_ENTRY__KEY);

		propertyToEObjectMapEntryEClass = createEClass(PROPERTY_TO_EOBJECT_MAP_ENTRY);
		createEReference(propertyToEObjectMapEntryEClass, PROPERTY_TO_EOBJECT_MAP_ENTRY__VALUE);
		createEReference(propertyToEObjectMapEntryEClass, PROPERTY_TO_EOBJECT_MAP_ENTRY__KEY);

		oclRegistryEClass = createEClass(OCL_REGISTRY);
		createEReference(oclRegistryEClass, OCL_REGISTRY__ID_TO_OCL_MAP);

		msdUtilEClass = createEClass(MSD_UTIL);
		createEAttribute(msdUtilEClass, MSD_UTIL__ENVIRONMENT_ASSUMPTION);
		createEReference(msdUtilEClass, MSD_UTIL__HOT_MESSAGES);
		createEReference(msdUtilEClass, MSD_UTIL__EXECUTED_MESSAGES);
		createEReference(msdUtilEClass, MSD_UTIL__ENVIRONMENT_MESSAGES);
		createEReference(msdUtilEClass, MSD_UTIL__COLD_FORBIDDEN_MESSAGES);
		createEReference(msdUtilEClass, MSD_UTIL__HOT_FORBIDDEN_MESSAGES);

		interactionToMSDUtilMapEntryEClass = createEClass(INTERACTION_TO_MSD_UTIL_MAP_ENTRY);
		createEReference(interactionToMSDUtilMapEntryEClass, INTERACTION_TO_MSD_UTIL_MAP_ENTRY__KEY);
		createEReference(interactionToMSDUtilMapEntryEClass, INTERACTION_TO_MSD_UTIL_MAP_ENTRY__VALUE);

		interactionOperandToLifelineToInteractionFragmentsMapEntryEClass = createEClass(INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY);
		createEReference(interactionOperandToLifelineToInteractionFragmentsMapEntryEClass, INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__KEY);
		createEReference(interactionOperandToLifelineToInteractionFragmentsMapEntryEClass, INTERACTION_OPERAND_TO_LIFELINE_TO_INTERACTION_FRAGMENTS_MAP_ENTRY__VALUE);

		stateMachineToInitialStateMapEntryEClass = createEClass(STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY);
		createEReference(stateMachineToInitialStateMapEntryEClass, STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__KEY);
		createEReference(stateMachineToInitialStateMapEntryEClass, STATE_MACHINE_TO_INITIAL_STATE_MAP_ENTRY__VALUE);

		idToOCLMapEntryEClass = createEClass(ID_TO_OCL_MAP_ENTRY);
		createEAttribute(idToOCLMapEntryEClass, ID_TO_OCL_MAP_ENTRY__KEY);
		createEAttribute(idToOCLMapEntryEClass, ID_TO_OCL_MAP_ENTRY__VALUE);

		// Create data types
		plainJavaObjectEDataType = createEDataType(PLAIN_JAVA_OBJECT);
		oclEDataType = createEDataType(OCL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Uml2ecorePackage theUml2ecorePackage = (Uml2ecorePackage)EPackage.Registry.INSTANCE.getEPackage(Uml2ecorePackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		org.scenariotools.msd.runtime.RuntimePackage theRuntimePackage_1 = (org.scenariotools.msd.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(org.scenariotools.msd.runtime.RuntimePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(runtimeUtilEClass, RuntimeUtil.class, "RuntimeUtil", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuntimeUtil_Uml2ecoreMapping(), this.getUML2EcoreMapping(), null, "uml2ecoreMapping", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_RootUMLPackageToEPackage(), theUml2ecorePackage.getUmlPackage2EPackage(), null, "rootUMLPackageToEPackage", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_FirstMessageOperationToInteractionMap(), this.getFirstMessageOperationToInteractionMapEntry(), null, "firstMessageOperationToInteractionMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_FirstTransitionOperationToStateMachineMap(), this.getFirstTransitionOperationToStateMachineMapEntry(), null, "firstTransitionOperationToStateMachineMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_InteractionToStringToLifelineMapMap(), this.getInteractionToStringToLifelineMapMapEntry(), null, "interactionToStringToLifelineMapMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_LifelineToInteractionFragmentsMap(), this.getLifelineToInteractionFragmentsMapEntry(), null, "lifelineToInteractionFragmentsMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_SystemClasses(), theUMLPackage.getClass_(), null, "systemClasses", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_EnvironmentClasses(), theUMLPackage.getClass_(), null, "environmentClasses", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_UmlPackages(), theUMLPackage.getPackage(), null, "umlPackages", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_PackageToCollaborationsMap(), this.getPackageToCollaborationMapEntry(), null, "packageToCollaborationsMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_Interactions(), theUMLPackage.getInteraction(), null, "interactions", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_IntegratedPackage(), theUMLPackage.getPackage(), null, "integratedPackage", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_EnvironmentSendableMessages(), theUMLPackage.getMessage(), null, "environmentSendableMessages", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_SystemSendableMessages(), theUMLPackage.getMessage(), null, "systemSendableMessages", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_UseCaseSpecificationsToBeConsidered(), theUMLPackage.getPackage(), null, "useCaseSpecificationsToBeConsidered", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_OclRegistry(), this.getOCLRegistry(), null, "oclRegistry", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_MsdRuntimeStateGraph(), theRuntimePackage_1.getMSDRuntimeStateGraph(), theRuntimePackage_1.getMSDRuntimeStateGraph_RuntimeUtil(), "msdRuntimeStateGraph", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_FinalFragment(), theUMLPackage.getInteractionFragment(), null, "finalFragment", null, 0, 1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_InteractionToMSDUtilMap(), this.getInteractionToMSDUtilMapEntry(), null, "interactionToMSDUtilMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_InteractionOperandToLifelineToInteractionFragmentsMapMap(), this.getInteractionOperandToLifelineToInteractionFragmentsMapEntry(), null, "interactionOperandToLifelineToInteractionFragmentsMapMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_StateMachines(), theUMLPackage.getStateMachine(), null, "stateMachines", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeUtil_StateMachineToInitialStateMap(), this.getStateMachineToInitialStateMapEntry(), null, "stateMachineToInitialStateMap", null, 0, -1, RuntimeUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(runtimeUtilEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(runtimeUtilEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUml2ecorePackage.getUmlPackage2EPackage(), "rootUmlPackage2EPackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(theEcorePackage.getEEList());
		EGenericType g2 = createEGenericType(theUMLPackage.getPackage());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "useCaseSpecificationsToBeConsidered", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEOperation(), "getOperation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getMessage(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEOperation(), "getOperation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getTransition(), "transition", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEStructuralFeature(), "getEStructuralFeature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getMessage(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEStructuralFeature(), "getEStructuralFeature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getTransition(), "transition", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEBoolean(), "isEnvironmentRuntimeMessage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getRuntimeMessage(), "runtimeMessage", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEBoolean(), "isEnvironmentMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, this.getPlainJavaObject(), "getParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getMessage(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, this.getPlainJavaObject(), "getParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getMessage(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOCL(), "oclForCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, theEcorePackage.getEBoolean(), "isNonSpontaneousEnvironmentMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(runtimeUtilEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUml2ecorePackage.getUmlPackage2EPackage(), "rootUmlPackage2EPackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theEcorePackage.getEEList());
		g2 = createEGenericType(theUMLPackage.getPackage());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "useCaseSpecificationsToBeConsidered", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUMLPackage.getPackage(), "strategyPackage", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(uml2EcoreMappingEClass, UML2EcoreMapping.class, "UML2EcoreMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUML2EcoreMapping_UmlClassToEClassMap(), this.getUMLClassToEClassMapEntry(), null, "umlClassToEClassMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_EClassToUMLClassMap(), this.getEClassToUMLClassMapEntry(), null, "eClassToUMLClassMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_EOperation2UMLOperationMap(), this.getEOperation2UMLOperationMapEntry(), null, "eOperation2UMLOperationMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_UmlOperationToEOperationMap(), this.getUMLOperationToEOperationMapEntry(), null, "umlOperationToEOperationMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_EParameterToUMLParameterMap(), this.getEParameterToUMLParameterMapEntry(), null, "eParameterToUMLParameterMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_UmlParameterToEParameterMap(), this.getUMLParameterToEParameterMapEntry(), null, "umlParameterToEParameterMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_UmlOperationToEStructuralFeatureMap(), this.getUMLOperationToEStructuralFeatureMapEntry(), null, "umlOperationToEStructuralFeatureMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_EStructuralFeatureToUMLOperationMap(), this.getEStructuralFeatureToUMLOperationMapEntry(), null, "eStructuralFeatureToUMLOperationMap", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_RootUMLPackageToEPackage(), theUml2ecorePackage.getUmlPackage2EPackage(), null, "rootUMLPackageToEPackage", null, 0, 1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUML2EcoreMapping_MergedUMLPackages(), theUMLPackage.getPackage(), null, "mergedUMLPackages", null, 0, -1, UML2EcoreMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(uml2EcoreMappingEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUml2ecorePackage.getUmlPackage2EPackage(), "rootUmlPackage2EPackage", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(umlClassToEClassMapEntryEClass, Map.Entry.class, "UMLClassToEClassMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLClassToEClassMapEntry_Key(), theUMLPackage.getClass_(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLClassToEClassMapEntry_Value(), theEcorePackage.getEClass(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eClassToUMLClassMapEntryEClass, Map.Entry.class, "EClassToUMLClassMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEClassToUMLClassMapEntry_Key(), theEcorePackage.getEClass(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEClassToUMLClassMapEntry_Value(), theUMLPackage.getClass_(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eOperation2UMLOperationMapEntryEClass, Map.Entry.class, "EOperation2UMLOperationMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEOperation2UMLOperationMapEntry_Key(), theEcorePackage.getEOperation(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEOperation2UMLOperationMapEntry_Value(), theUMLPackage.getOperation(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlOperationToEOperationMapEntryEClass, Map.Entry.class, "UMLOperationToEOperationMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLOperationToEOperationMapEntry_Key(), theUMLPackage.getOperation(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLOperationToEOperationMapEntry_Value(), theEcorePackage.getEOperation(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eParameterToUMLParameterMapEntryEClass, Map.Entry.class, "EParameterToUMLParameterMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEParameterToUMLParameterMapEntry_Key(), theEcorePackage.getEParameter(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEParameterToUMLParameterMapEntry_Value(), theUMLPackage.getParameter(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlParameterToEParameterMapEntryEClass, Map.Entry.class, "UMLParameterToEParameterMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLParameterToEParameterMapEntry_Key(), theUMLPackage.getParameter(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLParameterToEParameterMapEntry_Value(), theEcorePackage.getEParameter(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eStructuralFeatureToUMLOperationMapEntryEClass, Map.Entry.class, "EStructuralFeatureToUMLOperationMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEStructuralFeatureToUMLOperationMapEntry_Key(), theEcorePackage.getEStructuralFeature(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEStructuralFeatureToUMLOperationMapEntry_Value(), theUMLPackage.getOperation(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlOperationToEStructuralFeatureMapEntryEClass, Map.Entry.class, "UMLOperationToEStructuralFeatureMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLOperationToEStructuralFeatureMapEntry_Key(), theUMLPackage.getOperation(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLOperationToEStructuralFeatureMapEntry_Value(), theEcorePackage.getEStructuralFeature(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToLifelineMapEntryEClass, Map.Entry.class, "StringToLifelineMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStringToLifelineMapEntry_Value(), theUMLPackage.getLifeline(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToLifelineMapEntry_Key(), theEcorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(firstMessageOperationToInteractionMapEntryEClass, Map.Entry.class, "FirstMessageOperationToInteractionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFirstMessageOperationToInteractionMapEntry_Value(), theUMLPackage.getInteraction(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFirstMessageOperationToInteractionMapEntry_Key(), theUMLPackage.getOperation(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(firstTransitionOperationToStateMachineMapEntryEClass, Map.Entry.class, "FirstTransitionOperationToStateMachineMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFirstTransitionOperationToStateMachineMapEntry_Value(), theUMLPackage.getStateMachine(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFirstTransitionOperationToStateMachineMapEntry_Key(), theUMLPackage.getOperation(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lifelineToInteractionFragmentsMapEntryEClass, Map.Entry.class, "LifelineToInteractionFragmentsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLifelineToInteractionFragmentsMapEntry_Key(), theUMLPackage.getLifeline(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLifelineToInteractionFragmentsMapEntry_Value(), theUMLPackage.getInteractionFragment(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(packageToCollaborationMapEntryEClass, Map.Entry.class, "PackageToCollaborationMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPackageToCollaborationMapEntry_Value(), theUMLPackage.getCollaboration(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPackageToCollaborationMapEntry_Key(), theUMLPackage.getPackage(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionToStringToLifelineMapMapEntryEClass, Map.Entry.class, "InteractionToStringToLifelineMapMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionToStringToLifelineMapMapEntry_Value(), this.getStringToLifelineMapEntry(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionToStringToLifelineMapMapEntry_Key(), theUMLPackage.getInteraction(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyToEObjectMapEntryEClass, Map.Entry.class, "PropertyToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertyToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyToEObjectMapEntry_Key(), theUMLPackage.getProperty(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(oclRegistryEClass, OCLRegistry.class, "OCLRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOCLRegistry_IdToOCLMap(), this.getIDToOCLMapEntry(), null, "idToOCLMap", null, 0, -1, OCLRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(oclRegistryEClass, null, "disposeOCLForActiveMSD", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRuntimePackage_1.getActiveProcess(), "activeMSD", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(oclRegistryEClass, this.getOCL(), "getOCLForActiveMSD", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRuntimePackage_1.getActiveProcess(), "activeMSD", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(msdUtilEClass, MSDUtil.class, "MSDUtil", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMSDUtil_EnvironmentAssumption(), theEcorePackage.getEBoolean(), "environmentAssumption", null, 0, 1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDUtil_HotMessages(), theUMLPackage.getMessage(), null, "hotMessages", null, 0, -1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDUtil_ExecutedMessages(), theUMLPackage.getMessage(), null, "executedMessages", null, 0, -1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDUtil_EnvironmentMessages(), theUMLPackage.getMessage(), null, "environmentMessages", null, 0, -1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDUtil_ColdForbiddenMessages(), theUMLPackage.getMessage(), null, "coldForbiddenMessages", null, 0, -1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSDUtil_HotForbiddenMessages(), theUMLPackage.getMessage(), null, "hotForbiddenMessages", null, 0, -1, MSDUtil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionToMSDUtilMapEntryEClass, Map.Entry.class, "InteractionToMSDUtilMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionToMSDUtilMapEntry_Key(), theUMLPackage.getInteraction(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionToMSDUtilMapEntry_Value(), this.getMSDUtil(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionOperandToLifelineToInteractionFragmentsMapEntryEClass, Map.Entry.class, "InteractionOperandToLifelineToInteractionFragmentsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Key(), theUMLPackage.getInteractionOperand(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionOperandToLifelineToInteractionFragmentsMapEntry_Value(), this.getLifelineToInteractionFragmentsMapEntry(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateMachineToInitialStateMapEntryEClass, Map.Entry.class, "StateMachineToInitialStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateMachineToInitialStateMapEntry_Key(), theUMLPackage.getStateMachine(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineToInitialStateMapEntry_Value(), theUMLPackage.getState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(idToOCLMapEntryEClass, Map.Entry.class, "IDToOCLMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIDToOCLMapEntry_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIDToOCLMapEntry_Value(), this.getOCL(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(plainJavaObjectEDataType, Object.class, "PlainJavaObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(oclEDataType, org.eclipse.ocl.ecore.OCL.class, "OCL", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //UtilPackageImpl
