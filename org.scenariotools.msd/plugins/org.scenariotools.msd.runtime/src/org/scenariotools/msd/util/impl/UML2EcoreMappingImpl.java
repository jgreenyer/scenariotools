/**
 */
package org.scenariotools.msd.util.impl;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.msd.runtime.plugin.Activator;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;
import org.scenariotools.msd.uml2ecore.UmlOp2EOp;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UML2 Ecore Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getUmlClassToEClassMap <em>Uml Class To EClass Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getEClassToUMLClassMap <em>EClass To UML Class Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getEOperation2UMLOperationMap <em>EOperation2 UML Operation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getUmlOperationToEOperationMap <em>Uml Operation To EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getEParameterToUMLParameterMap <em>EParameter To UML Parameter Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getUmlParameterToEParameterMap <em>Uml Parameter To EParameter Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getUmlOperationToEStructuralFeatureMap <em>Uml Operation To EStructural Feature Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getEStructuralFeatureToUMLOperationMap <em>EStructural Feature To UML Operation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.impl.UML2EcoreMappingImpl#getMergedUMLPackages <em>Merged UML Packages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UML2EcoreMappingImpl extends RuntimeEObjectImpl implements UML2EcoreMapping {
	
	private static Logger logger = Activator.getLogManager().getLogger(
			UML2EcoreMappingImpl.class.getName());
	
	/**
	 * The cached value of the '{@link #getUmlClassToEClassMap() <em>Uml Class To EClass Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlClassToEClassMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<org.eclipse.uml2.uml.Class, EClass> umlClassToEClassMap;

	/**
	 * The cached value of the '{@link #getEClassToUMLClassMap() <em>EClass To UML Class Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClassToUMLClassMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, EList<org.eclipse.uml2.uml.Class>> eClassToUMLClassMap;

	/**
	 * The cached value of the '{@link #getEOperation2UMLOperationMap() <em>EOperation2 UML Operation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEOperation2UMLOperationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EOperation, EList<Operation>> eOperation2UMLOperationMap;

	/**
	 * The cached value of the '{@link #getUmlOperationToEOperationMap() <em>Uml Operation To EOperation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlOperationToEOperationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Operation, EOperation> umlOperationToEOperationMap;

	/**
	 * The cached value of the '{@link #getEParameterToUMLParameterMap() <em>EParameter To UML Parameter Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEParameterToUMLParameterMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EParameter, EList<Parameter>> eParameterToUMLParameterMap;

	/**
	 * The cached value of the '{@link #getUmlParameterToEParameterMap() <em>Uml Parameter To EParameter Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlParameterToEParameterMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Parameter, EParameter> umlParameterToEParameterMap;

	/**
	 * The cached value of the '{@link #getUmlOperationToEStructuralFeatureMap() <em>Uml Operation To EStructural Feature Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlOperationToEStructuralFeatureMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Operation, EStructuralFeature> umlOperationToEStructuralFeatureMap;

	/**
	 * The cached value of the '{@link #getEStructuralFeatureToUMLOperationMap() <em>EStructural Feature To UML Operation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEStructuralFeatureToUMLOperationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EStructuralFeature, EList<Operation>> eStructuralFeatureToUMLOperationMap;

	/**
	 * The cached value of the '{@link #getRootUMLPackageToEPackage() <em>Root UML Package To EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootUMLPackageToEPackage()
	 * @generated
	 * @ordered
	 */
	protected UmlPackage2EPackage rootUMLPackageToEPackage;

	/**
	 * The cached value of the '{@link #getMergedUMLPackages() <em>Merged UML Packages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedUMLPackages()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.uml2.uml.Package> mergedUMLPackages;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UML2EcoreMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilPackage.Literals.UML2_ECORE_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<org.eclipse.uml2.uml.Class, EClass> getUmlClassToEClassMap() {
		if (umlClassToEClassMap == null) {
			umlClassToEClassMap = new EcoreEMap<org.eclipse.uml2.uml.Class,EClass>(UtilPackage.Literals.UML_CLASS_TO_ECLASS_MAP_ENTRY, UMLClassToEClassMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP);
		}
		return umlClassToEClassMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, EList<org.eclipse.uml2.uml.Class>> getEClassToUMLClassMap() {
		if (eClassToUMLClassMap == null) {
			eClassToUMLClassMap = new EcoreEMap<EClass,EList<org.eclipse.uml2.uml.Class>>(UtilPackage.Literals.ECLASS_TO_UML_CLASS_MAP_ENTRY, EClassToUMLClassMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP);
		}
		return eClassToUMLClassMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EOperation, EList<Operation>> getEOperation2UMLOperationMap() {
		if (eOperation2UMLOperationMap == null) {
			eOperation2UMLOperationMap = new EcoreEMap<EOperation,EList<Operation>>(UtilPackage.Literals.EOPERATION2_UML_OPERATION_MAP_ENTRY, EOperation2UMLOperationMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP);
		}
		return eOperation2UMLOperationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Operation, EOperation> getUmlOperationToEOperationMap() {
		if (umlOperationToEOperationMap == null) {
			umlOperationToEOperationMap = new EcoreEMap<Operation,EOperation>(UtilPackage.Literals.UML_OPERATION_TO_EOPERATION_MAP_ENTRY, UMLOperationToEOperationMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP);
		}
		return umlOperationToEOperationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EParameter, EList<Parameter>> getEParameterToUMLParameterMap() {
		if (eParameterToUMLParameterMap == null) {
			eParameterToUMLParameterMap = new EcoreEMap<EParameter,EList<Parameter>>(UtilPackage.Literals.EPARAMETER_TO_UML_PARAMETER_MAP_ENTRY, EParameterToUMLParameterMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP);
		}
		return eParameterToUMLParameterMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Parameter, EParameter> getUmlParameterToEParameterMap() {
		if (umlParameterToEParameterMap == null) {
			umlParameterToEParameterMap = new EcoreEMap<Parameter,EParameter>(UtilPackage.Literals.UML_PARAMETER_TO_EPARAMETER_MAP_ENTRY, UMLParameterToEParameterMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP);
		}
		return umlParameterToEParameterMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Operation, EStructuralFeature> getUmlOperationToEStructuralFeatureMap() {
		if (umlOperationToEStructuralFeatureMap == null) {
			umlOperationToEStructuralFeatureMap = new EcoreEMap<Operation,EStructuralFeature>(UtilPackage.Literals.UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP_ENTRY, UMLOperationToEStructuralFeatureMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP);
		}
		return umlOperationToEStructuralFeatureMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EStructuralFeature, EList<Operation>> getEStructuralFeatureToUMLOperationMap() {
		if (eStructuralFeatureToUMLOperationMap == null) {
			eStructuralFeatureToUMLOperationMap = new EcoreEMap<EStructuralFeature,EList<Operation>>(UtilPackage.Literals.ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP_ENTRY, EStructuralFeatureToUMLOperationMapEntryImpl.class, this, UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP);
		}
		return eStructuralFeatureToUMLOperationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage getRootUMLPackageToEPackage() {
		if (rootUMLPackageToEPackage != null && rootUMLPackageToEPackage.eIsProxy()) {
			InternalEObject oldRootUMLPackageToEPackage = (InternalEObject)rootUMLPackageToEPackage;
			rootUMLPackageToEPackage = (UmlPackage2EPackage)eResolveProxy(oldRootUMLPackageToEPackage);
			if (rootUMLPackageToEPackage != oldRootUMLPackageToEPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE, oldRootUMLPackageToEPackage, rootUMLPackageToEPackage));
			}
		}
		return rootUMLPackageToEPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage basicGetRootUMLPackageToEPackage() {
		return rootUMLPackageToEPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootUMLPackageToEPackage(UmlPackage2EPackage newRootUMLPackageToEPackage) {
		UmlPackage2EPackage oldRootUMLPackageToEPackage = rootUMLPackageToEPackage;
		rootUMLPackageToEPackage = newRootUMLPackageToEPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE, oldRootUMLPackageToEPackage, rootUMLPackageToEPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getMergedUMLPackages() {
		if (mergedUMLPackages == null) {
			mergedUMLPackages = new EObjectResolvingEList<org.eclipse.uml2.uml.Package>(org.eclipse.uml2.uml.Package.class, this, UtilPackage.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES);
		}
		return mergedUMLPackages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getMergedUMLPackages(String name) {
		return getMergedUMLPackages(name, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getMergedUMLPackages(String name, boolean ignoreCase, EClass eClass) {
		mergedUMLPackagesLoop: for (org.eclipse.uml2.uml.Package mergedUMLPackages : getMergedUMLPackages()) {
			if (eClass != null && !eClass.isInstance(mergedUMLPackages))
				continue mergedUMLPackagesLoop;
			if (name != null && !(ignoreCase ? name.equalsIgnoreCase(mergedUMLPackages.getName()) : name.equals(mergedUMLPackages.getName())))
				continue mergedUMLPackagesLoop;
			return mergedUMLPackages;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(UmlPackage2EPackage umlPackage2EPackage) {
		fillMapping(umlPackage2EPackage);
	}

	private void fillMapping(UmlPackage2EPackage umlPackage2EPackage){
		getMergedUMLPackages().add(umlPackage2EPackage.getUmlPackage());		
		for (EObject correspondenceObject : umlPackage2EPackage.getChildCorresp()) {

			if (correspondenceObject instanceof UmlClass2EClass) {
				UmlClass2EClass u2eClass = (UmlClass2EClass) correspondenceObject;
				logger.debug("UmlClassToEClassMap -- mapping " + u2eClass.getUmlc());
				logger.debug("                            to " + u2eClass.getEc());
				getUmlClassToEClassMap().put(u2eClass.getUmlc(), u2eClass.getEc());
				if (getEClassToUMLClassMap().get(u2eClass.getEc()) == null){
					getEClassToUMLClassMap().put(u2eClass.getEc(), new UniqueEList<org.eclipse.uml2.uml.Class>());
				}
				logger.debug("EClassToUMLClassMap -- mapping " + u2eClass.getEc());
				logger.debug("                            to " + u2eClass.getUmlc());
				getEClassToUMLClassMap().get(u2eClass.getEc()).add(u2eClass.getUmlc());

				for (EObject correspondenceObject2 : u2eClass.getChildCorresp()) {
					if (correspondenceObject2 instanceof UmlOp2EOp) {
						UmlOp2EOp umlOp2EOp = (UmlOp2EOp) correspondenceObject2;
						getUmlOperationToEOperationMap().put(umlOp2EOp.getUmlop(), umlOp2EOp.getEOperation());
						if (getEOperation2UMLOperationMap().get(umlOp2EOp.getEOperation()) == null){
							getEOperation2UMLOperationMap().put(umlOp2EOp.getEOperation(), new BasicEList<Operation>());
						}
						getEOperation2UMLOperationMap().get(umlOp2EOp.getEOperation()).add(umlOp2EOp.getUmlop());


						for (EObject node3 : umlOp2EOp.getChildCorresp()) {
							if (node3 instanceof UMLOpParam2EParam) {
								UMLOpParam2EParam umlOpParam2EParam = (UMLOpParam2EParam) node3;
								getUmlParameterToEParameterMap().put(umlOpParam2EParam.getUmlOpParam(), umlOpParam2EParam.getEParameter());
								if (getEParameterToUMLParameterMap().get(umlOpParam2EParam.getEParameter()) == null){
									getEParameterToUMLParameterMap().put(umlOpParam2EParam.getEParameter(), new BasicEList<Parameter>());
								}
								getEParameterToUMLParameterMap().get(umlOpParam2EParam.getEParameter()).add(umlOpParam2EParam.getUmlOpParam());
							}
						}
					}
					if (correspondenceObject2 instanceof UmlSetOp2EStructFeat) {
						UmlSetOp2EStructFeat umlSetOp2EStructFeat = (UmlSetOp2EStructFeat) correspondenceObject2;
						getUmlOperationToEStructuralFeatureMap().put(umlSetOp2EStructFeat.getUmlop(), umlSetOp2EStructFeat.getEStructuralFeature());
						if (getEStructuralFeatureToUMLOperationMap().get(umlSetOp2EStructFeat.getEStructuralFeature()) == null){
							getEStructuralFeatureToUMLOperationMap().put(umlSetOp2EStructFeat.getEStructuralFeature(), new BasicEList<Operation>());
						}
						getEStructuralFeatureToUMLOperationMap().get(umlSetOp2EStructFeat.getEStructuralFeature()).add(umlSetOp2EStructFeat.getUmlop());
					}
				}
			}

			if (correspondenceObject instanceof UmlPackage2EPackage) {
				fillMapping((UmlPackage2EPackage) correspondenceObject);
			}
		}		
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
				return ((InternalEList<?>)getUmlClassToEClassMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
				return ((InternalEList<?>)getEClassToUMLClassMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
				return ((InternalEList<?>)getEOperation2UMLOperationMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
				return ((InternalEList<?>)getUmlOperationToEOperationMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
				return ((InternalEList<?>)getEParameterToUMLParameterMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
				return ((InternalEList<?>)getUmlParameterToEParameterMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
				return ((InternalEList<?>)getUmlOperationToEStructuralFeatureMap()).basicRemove(otherEnd, msgs);
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				return ((InternalEList<?>)getEStructuralFeatureToUMLOperationMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
				if (coreType) return getUmlClassToEClassMap();
				else return getUmlClassToEClassMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
				if (coreType) return getEClassToUMLClassMap();
				else return getEClassToUMLClassMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
				if (coreType) return getEOperation2UMLOperationMap();
				else return getEOperation2UMLOperationMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
				if (coreType) return getUmlOperationToEOperationMap();
				else return getUmlOperationToEOperationMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
				if (coreType) return getEParameterToUMLParameterMap();
				else return getEParameterToUMLParameterMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
				if (coreType) return getUmlParameterToEParameterMap();
				else return getUmlParameterToEParameterMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
				if (coreType) return getUmlOperationToEStructuralFeatureMap();
				else return getUmlOperationToEStructuralFeatureMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				if (coreType) return getEStructuralFeatureToUMLOperationMap();
				else return getEStructuralFeatureToUMLOperationMap().map();
			case UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE:
				if (resolve) return getRootUMLPackageToEPackage();
				return basicGetRootUMLPackageToEPackage();
			case UtilPackage.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES:
				return getMergedUMLPackages();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
				((EStructuralFeature.Setting)getUmlClassToEClassMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
				((EStructuralFeature.Setting)getEClassToUMLClassMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
				((EStructuralFeature.Setting)getEOperation2UMLOperationMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
				((EStructuralFeature.Setting)getUmlOperationToEOperationMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
				((EStructuralFeature.Setting)getEParameterToUMLParameterMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
				((EStructuralFeature.Setting)getUmlParameterToEParameterMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
				((EStructuralFeature.Setting)getUmlOperationToEStructuralFeatureMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				((EStructuralFeature.Setting)getEStructuralFeatureToUMLOperationMap()).set(newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE:
				setRootUMLPackageToEPackage((UmlPackage2EPackage)newValue);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES:
				getMergedUMLPackages().clear();
				getMergedUMLPackages().addAll((Collection<? extends org.eclipse.uml2.uml.Package>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
				getUmlClassToEClassMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
				getEClassToUMLClassMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
				getEOperation2UMLOperationMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
				getUmlOperationToEOperationMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
				getEParameterToUMLParameterMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
				getUmlParameterToEParameterMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
				getUmlOperationToEStructuralFeatureMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				getEStructuralFeatureToUMLOperationMap().clear();
				return;
			case UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE:
				setRootUMLPackageToEPackage((UmlPackage2EPackage)null);
				return;
			case UtilPackage.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES:
				getMergedUMLPackages().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilPackage.UML2_ECORE_MAPPING__UML_CLASS_TO_ECLASS_MAP:
				return umlClassToEClassMap != null && !umlClassToEClassMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__ECLASS_TO_UML_CLASS_MAP:
				return eClassToUMLClassMap != null && !eClassToUMLClassMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__EOPERATION2_UML_OPERATION_MAP:
				return eOperation2UMLOperationMap != null && !eOperation2UMLOperationMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_EOPERATION_MAP:
				return umlOperationToEOperationMap != null && !umlOperationToEOperationMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__EPARAMETER_TO_UML_PARAMETER_MAP:
				return eParameterToUMLParameterMap != null && !eParameterToUMLParameterMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__UML_PARAMETER_TO_EPARAMETER_MAP:
				return umlParameterToEParameterMap != null && !umlParameterToEParameterMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__UML_OPERATION_TO_ESTRUCTURAL_FEATURE_MAP:
				return umlOperationToEStructuralFeatureMap != null && !umlOperationToEStructuralFeatureMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__ESTRUCTURAL_FEATURE_TO_UML_OPERATION_MAP:
				return eStructuralFeatureToUMLOperationMap != null && !eStructuralFeatureToUMLOperationMap.isEmpty();
			case UtilPackage.UML2_ECORE_MAPPING__ROOT_UML_PACKAGE_TO_EPACKAGE:
				return rootUMLPackageToEPackage != null;
			case UtilPackage.UML2_ECORE_MAPPING__MERGED_UML_PACKAGES:
				return mergedUMLPackages != null && !mergedUMLPackages.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UML2EcoreMappingImpl
