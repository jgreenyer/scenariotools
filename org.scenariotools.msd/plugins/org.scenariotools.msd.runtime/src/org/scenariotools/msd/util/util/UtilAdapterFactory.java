/**
 */
package org.scenariotools.msd.util.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.scenariotools.msd.util.*;
import org.scenariotools.msd.util.MSDUtil;
import org.scenariotools.msd.util.OCLRegistry;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.util.UtilPackage
 * @generated
 */
public class UtilAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UtilPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UtilPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UtilSwitch<Adapter> modelSwitch =
		new UtilSwitch<Adapter>() {
			@Override
			public Adapter caseRuntimeUtil(RuntimeUtil object) {
				return createRuntimeUtilAdapter();
			}
			@Override
			public Adapter caseUML2EcoreMapping(UML2EcoreMapping object) {
				return createUML2EcoreMappingAdapter();
			}
			@Override
			public Adapter caseUMLClassToEClassMapEntry(Map.Entry<org.eclipse.uml2.uml.Class, EClass> object) {
				return createUMLClassToEClassMapEntryAdapter();
			}
			@Override
			public Adapter caseEClassToUMLClassMapEntry(Map.Entry<EClass, EList<org.eclipse.uml2.uml.Class>> object) {
				return createEClassToUMLClassMapEntryAdapter();
			}
			@Override
			public Adapter caseEOperation2UMLOperationMapEntry(Map.Entry<EOperation, EList<Operation>> object) {
				return createEOperation2UMLOperationMapEntryAdapter();
			}
			@Override
			public Adapter caseUMLOperationToEOperationMapEntry(Map.Entry<Operation, EOperation> object) {
				return createUMLOperationToEOperationMapEntryAdapter();
			}
			@Override
			public Adapter caseEParameterToUMLParameterMapEntry(Map.Entry<EParameter, EList<Parameter>> object) {
				return createEParameterToUMLParameterMapEntryAdapter();
			}
			@Override
			public Adapter caseUMLParameterToEParameterMapEntry(Map.Entry<Parameter, EParameter> object) {
				return createUMLParameterToEParameterMapEntryAdapter();
			}
			@Override
			public Adapter caseEStructuralFeatureToUMLOperationMapEntry(Map.Entry<EStructuralFeature, EList<Operation>> object) {
				return createEStructuralFeatureToUMLOperationMapEntryAdapter();
			}
			@Override
			public Adapter caseUMLOperationToEStructuralFeatureMapEntry(Map.Entry<Operation, EStructuralFeature> object) {
				return createUMLOperationToEStructuralFeatureMapEntryAdapter();
			}
			@Override
			public Adapter caseStringToLifelineMapEntry(Map.Entry<String, Lifeline> object) {
				return createStringToLifelineMapEntryAdapter();
			}
			@Override
			public Adapter caseFirstMessageOperationToInteractionMapEntry(Map.Entry<Operation, EList<Interaction>> object) {
				return createFirstMessageOperationToInteractionMapEntryAdapter();
			}
			@Override
			public Adapter caseFirstTransitionOperationToStateMachineMapEntry(Map.Entry<Operation, EList<StateMachine>> object) {
				return createFirstTransitionOperationToStateMachineMapEntryAdapter();
			}
			@Override
			public Adapter caseLifelineToInteractionFragmentsMapEntry(Map.Entry<Lifeline, EList<InteractionFragment>> object) {
				return createLifelineToInteractionFragmentsMapEntryAdapter();
			}
			@Override
			public Adapter casePackageToCollaborationMapEntry(Map.Entry<org.eclipse.uml2.uml.Package, EList<Collaboration>> object) {
				return createPackageToCollaborationMapEntryAdapter();
			}
			@Override
			public Adapter caseInteractionToStringToLifelineMapMapEntry(Map.Entry<Interaction, EMap<String, Lifeline>> object) {
				return createInteractionToStringToLifelineMapMapEntryAdapter();
			}
			@Override
			public Adapter casePropertyToEObjectMapEntry(Map.Entry<Property, EObject> object) {
				return createPropertyToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseOCLRegistry(OCLRegistry object) {
				return createOCLRegistryAdapter();
			}
			@Override
			public Adapter caseMSDUtil(MSDUtil object) {
				return createMSDUtilAdapter();
			}
			@Override
			public Adapter caseInteractionToMSDUtilMapEntry(Map.Entry<Interaction, MSDUtil> object) {
				return createInteractionToMSDUtilMapEntryAdapter();
			}
			@Override
			public Adapter caseInteractionOperandToLifelineToInteractionFragmentsMapEntry(Map.Entry<InteractionOperand, EMap<Lifeline, EList<InteractionFragment>>> object) {
				return createInteractionOperandToLifelineToInteractionFragmentsMapEntryAdapter();
			}
			@Override
			public Adapter caseStateMachineToInitialStateMapEntry(Map.Entry<StateMachine, State> object) {
				return createStateMachineToInitialStateMapEntryAdapter();
			}
			@Override
			public Adapter caseIDToOCLMapEntry(Map.Entry<String, OCL> object) {
				return createIDToOCLMapEntryAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.util.RuntimeUtil <em>Runtime Util</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.util.RuntimeUtil
	 * @generated
	 */
	public Adapter createRuntimeUtilAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.util.UML2EcoreMapping <em>UML2 Ecore Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.util.UML2EcoreMapping
	 * @generated
	 */
	public Adapter createUML2EcoreMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>UML Class To EClass Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createUMLClassToEClassMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EClass To UML Class Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEClassToUMLClassMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EOperation2 UML Operation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEOperation2UMLOperationMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>UML Operation To EOperation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createUMLOperationToEOperationMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EParameter To UML Parameter Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEParameterToUMLParameterMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>UML Parameter To EParameter Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createUMLParameterToEParameterMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EStructural Feature To UML Operation Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEStructuralFeatureToUMLOperationMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>UML Operation To EStructural Feature Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createUMLOperationToEStructuralFeatureMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Lifeline Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToLifelineMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>First Message Operation To Interaction Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createFirstMessageOperationToInteractionMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>First Transition Operation To State Machine Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createFirstTransitionOperationToStateMachineMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Lifeline To Interaction Fragments Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createLifelineToInteractionFragmentsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Package To Collaboration Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createPackageToCollaborationMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Interaction To String To Lifeline Map Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createInteractionToStringToLifelineMapMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Property To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createPropertyToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.util.OCLRegistry <em>OCL Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.util.OCLRegistry
	 * @generated
	 */
	public Adapter createOCLRegistryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.util.MSDUtil <em>MSD Util</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.util.MSDUtil
	 * @generated
	 */
	public Adapter createMSDUtilAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Interaction To MSD Util Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createInteractionToMSDUtilMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Interaction Operand To Lifeline To Interaction Fragments Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createInteractionOperandToLifelineToInteractionFragmentsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>State Machine To Initial State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStateMachineToInitialStateMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>ID To OCL Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createIDToOCLMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //UtilAdapterFactory
