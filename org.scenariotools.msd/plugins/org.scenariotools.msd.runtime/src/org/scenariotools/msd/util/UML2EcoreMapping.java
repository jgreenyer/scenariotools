/**
 */
package org.scenariotools.msd.util;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UML2 Ecore Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlClassToEClassMap <em>Uml Class To EClass Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getEClassToUMLClassMap <em>EClass To UML Class Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getEOperation2UMLOperationMap <em>EOperation2 UML Operation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEOperationMap <em>Uml Operation To EOperation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getEParameterToUMLParameterMap <em>EParameter To UML Parameter Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlParameterToEParameterMap <em>Uml Parameter To EParameter Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getUmlOperationToEStructuralFeatureMap <em>Uml Operation To EStructural Feature Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getEStructuralFeatureToUMLOperationMap <em>EStructural Feature To UML Operation Map</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}</li>
 *   <li>{@link org.scenariotools.msd.util.UML2EcoreMapping#getMergedUMLPackages <em>Merged UML Packages</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping()
 * @model
 * @generated
 */
public interface UML2EcoreMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Uml Class To EClass Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Class},
	 * and the value is of type {@link org.eclipse.emf.ecore.EClass},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Class To EClass Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Class To EClass Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_UmlClassToEClassMap()
	 * @model mapType="org.scenariotools.msd.util.UMLClassToEClassMapEntry<org.eclipse.uml2.uml.Class, org.eclipse.emf.ecore.EClass>"
	 * @generated
	 */
	EMap<org.eclipse.uml2.uml.Class, EClass> getUmlClassToEClassMap();

	/**
	 * Returns the value of the '<em><b>EClass To UML Class Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Class},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass To UML Class Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass To UML Class Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_EClassToUMLClassMap()
	 * @model mapType="org.scenariotools.msd.util.EClassToUMLClassMapEntry<org.eclipse.emf.ecore.EClass, org.eclipse.uml2.uml.Class>"
	 * @generated
	 */
	EMap<EClass, EList<org.eclipse.uml2.uml.Class>> getEClassToUMLClassMap();

	/**
	 * Returns the value of the '<em><b>EOperation2 UML Operation Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EOperation},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Operation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EOperation2 UML Operation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EOperation2 UML Operation Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_EOperation2UMLOperationMap()
	 * @model mapType="org.scenariotools.msd.util.EOperation2UMLOperationMapEntry<org.eclipse.emf.ecore.EOperation, org.eclipse.uml2.uml.Operation>"
	 * @generated
	 */
	EMap<EOperation, EList<Operation>> getEOperation2UMLOperationMap();

	/**
	 * Returns the value of the '<em><b>Uml Operation To EOperation Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Operation},
	 * and the value is of type {@link org.eclipse.emf.ecore.EOperation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Operation To EOperation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Operation To EOperation Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_UmlOperationToEOperationMap()
	 * @model mapType="org.scenariotools.msd.util.UMLOperationToEOperationMapEntry<org.eclipse.uml2.uml.Operation, org.eclipse.emf.ecore.EOperation>"
	 * @generated
	 */
	EMap<Operation, EOperation> getUmlOperationToEOperationMap();

	/**
	 * Returns the value of the '<em><b>EParameter To UML Parameter Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EParameter},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Parameter},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EParameter To UML Parameter Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EParameter To UML Parameter Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_EParameterToUMLParameterMap()
	 * @model mapType="org.scenariotools.msd.util.EParameterToUMLParameterMapEntry<org.eclipse.emf.ecore.EParameter, org.eclipse.uml2.uml.Parameter>"
	 * @generated
	 */
	EMap<EParameter, EList<Parameter>> getEParameterToUMLParameterMap();

	/**
	 * Returns the value of the '<em><b>Uml Parameter To EParameter Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Parameter},
	 * and the value is of type {@link org.eclipse.emf.ecore.EParameter},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Parameter To EParameter Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Parameter To EParameter Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_UmlParameterToEParameterMap()
	 * @model mapType="org.scenariotools.msd.util.UMLParameterToEParameterMapEntry<org.eclipse.uml2.uml.Parameter, org.eclipse.emf.ecore.EParameter>"
	 * @generated
	 */
	EMap<Parameter, EParameter> getUmlParameterToEParameterMap();

	/**
	 * Returns the value of the '<em><b>Uml Operation To EStructural Feature Map</b></em>' map.
	 * The key is of type {@link org.eclipse.uml2.uml.Operation},
	 * and the value is of type {@link org.eclipse.emf.ecore.EStructuralFeature},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Operation To EStructural Feature Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Operation To EStructural Feature Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_UmlOperationToEStructuralFeatureMap()
	 * @model mapType="org.scenariotools.msd.util.UMLOperationToEStructuralFeatureMapEntry<org.eclipse.uml2.uml.Operation, org.eclipse.emf.ecore.EStructuralFeature>"
	 * @generated
	 */
	EMap<Operation, EStructuralFeature> getUmlOperationToEStructuralFeatureMap();

	/**
	 * Returns the value of the '<em><b>EStructural Feature To UML Operation Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EStructuralFeature},
	 * and the value is of type list of {@link org.eclipse.uml2.uml.Operation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EStructural Feature To UML Operation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EStructural Feature To UML Operation Map</em>' map.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_EStructuralFeatureToUMLOperationMap()
	 * @model mapType="org.scenariotools.msd.util.EStructuralFeatureToUMLOperationMapEntry<org.eclipse.emf.ecore.EStructuralFeature, org.eclipse.uml2.uml.Operation>"
	 * @generated
	 */
	EMap<EStructuralFeature, EList<Operation>> getEStructuralFeatureToUMLOperationMap();

	/**
	 * Returns the value of the '<em><b>Root UML Package To EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root UML Package To EPackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root UML Package To EPackage</em>' reference.
	 * @see #setRootUMLPackageToEPackage(UmlPackage2EPackage)
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_RootUMLPackageToEPackage()
	 * @model
	 * @generated
	 */
	UmlPackage2EPackage getRootUMLPackageToEPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.util.UML2EcoreMapping#getRootUMLPackageToEPackage <em>Root UML Package To EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root UML Package To EPackage</em>' reference.
	 * @see #getRootUMLPackageToEPackage()
	 * @generated
	 */
	void setRootUMLPackageToEPackage(UmlPackage2EPackage value);

	/**
	 * Returns the value of the '<em><b>Merged UML Packages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Package}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged UML Packages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged UML Packages</em>' reference list.
	 * @see org.scenariotools.msd.util.UtilPackage#getUML2EcoreMapping_MergedUMLPackages()
	 * @model
	 * @generated
	 */
	EList<org.eclipse.uml2.uml.Package> getMergedUMLPackages();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Merged UML Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getMergedUMLPackages()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getMergedUMLPackages(String name);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>' from the '<em><b>Merged UML Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @param ignoreCase Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass The Ecore class of the {@link org.eclipse.uml2.uml.Package} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.Package} with the specified '<em><b>Name</b></em>', or <code>null</code>.
	 * @see #getMergedUMLPackages()
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getMergedUMLPackages(String name, boolean ignoreCase, EClass eClass);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * this operation initializes
	 * - the UMLClassToEClassMap
	 * - the EClassToUMLClassMap
	 * 
	 * 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void init(UmlPackage2EPackage rootUmlPackage2EPackage);

} // UML2EcoreMapping
