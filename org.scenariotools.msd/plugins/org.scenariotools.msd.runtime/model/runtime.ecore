<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="runtime" nsURI="http://org.scenariotools.msd.runtime/1.0" nsPrefix="runtime">
  <eClassifiers xsi:type="ecore:EClass" name="MSDRuntimeStateGraph" eSuperTypes="../../org.scenariotools.runtime/model/runtime.ecore#//RuntimeStateGraph">
    <eOperations name="getMSDRuntimeState" eType="#//MSDRuntimeState">
      <eParameters name="msdRuntimeState" eType="#//MSDRuntimeState"/>
      <eParameters name="unchangedActiveMSDs" upperBound="-1" eType="#//ActiveProcess"/>
    </eOperations>
    <eOperations name="init" eType="#//MSDRuntimeState">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="initializes the MSDRuntimeStateGraph and returns the start MSDRuntimeState.&#xD;&#xA;(Leads to the creation of an ElementContainer and the creation+initialization of a RuntimeUtil)"/>
      </eAnnotations>
      <eParameters name="scenarioRunConfiguration" eType="ecore:EClass scenariorunconfiguration.ecore#//ScenarioRunConfiguration"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="elementContainer" eType="#//ElementContainer"
        containment="true" eOpposite="#//ElementContainer/msdRuntimeStateGraph"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"
        eOpposite="util.ecore#//RuntimeUtil/msdRuntimeStateGraph"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="scenarioRunConfiguration"
        eType="ecore:EClass scenariorunconfiguration.ecore#//ScenarioRunConfiguration"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="msdRuntimeStateKeyWrapperToMSDRuntimeStateMap"
        upperBound="-1" eType="#//MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry"
        transient="true" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MSDRuntimeState" eSuperTypes="../../org.scenariotools.runtime/model/runtime.ecore#//RuntimeState">
    <eOperations name="performStep">
      <eParameters name="event" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//Event"/>
    </eOperations>
    <eOperations name="updateMSDModalMessageEvents">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Updates (in fact completely re-calculates) the MSDModalMessageEvents for this MSDRuntimeState.&#xD;&#xA;This method should be called by clients after performStep is called unless the re-calculation is not necessary.&#xD;&#xA;&#xD;&#xA;"/>
      </eAnnotations>
      <eParameters name="executionSemantics" eType="#//ExecutionSemantics"/>
    </eOperations>
    <eOperations name="isInExecutedSystemCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eOperations name="isInExecutedEnvironmentCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eOperations name="isInHotEnvironmentCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eOperations name="isInHotSystemCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeProcesses" upperBound="-1"
        eType="#//ActiveProcess"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"
        transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSDChangedDuringPerformStep"
        upperBound="-1" eType="#//ActiveProcess" transient="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ElementContainer">
    <eOperations name="getActiveProcess" eType="#//ActiveProcess">
      <eParameters name="activeProcess" eType="#//ActiveProcess"/>
    </eOperations>
    <eOperations name="getActiveState" eType="#//ActiveState">
      <eParameters name="activeState" eType="#//ActiveState"/>
    </eOperations>
    <eOperations name="getActiveMSDLifelineBindings" eType="#//ActiveMSDLifelineBindings">
      <eParameters name="activeMSDLifelineBindings" eType="#//ActiveMSDLifelineBindings"/>
    </eOperations>
    <eOperations name="getActiveMSDVariableValuations" eType="#//ActiveMSDVariableValuations">
      <eParameters name="activeMSDVariableValuations" eType="#//ActiveMSDVariableValuations"/>
    </eOperations>
    <eOperations name="getObjectSystem" eType="#//MSDObjectSystem">
      <eParameters name="objectSystem" eType="#//MSDObjectSystem"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeProcesses" upperBound="-1"
        eType="#//ActiveProcess" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSDVariableValuations"
        upperBound="-1" eType="#//ActiveMSDVariableValuations" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeStates" upperBound="-1"
        eType="#//ActiveState" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSDLifelineBindings"
        upperBound="-1" eType="#//ActiveMSDLifelineBindings" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSSRoleBindings"
        upperBound="-1" eType="#//ActiveMSSRoleBindings" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeProcessKeyWrapperToActiveProcessMap"
        upperBound="-1" eType="#//ActiveProcessKeyWrapperToActiveProcessMapEntry"
        transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeStateKeyWrapperToActiveStateMap"
        upperBound="-1" eType="#//ActiveStateKeyWrapperToActiveStateMapEntry" transient="true"
        containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMap"
        upperBound="-1" eType="#//ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry"
        transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMap"
        upperBound="-1" eType="#//ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry"
        transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="objectSystemKeyWrapperToMSDObjectSystemMapEntry"
        upperBound="-1" eType="#//ObjectSystemKeyWrapperToMSDObjectSystemMapEntry"
        transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="objectSystems" upperBound="-1"
        eType="#//MSDObjectSystem" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="finalFragment" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//InteractionFragment"
        containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="msdRuntimeStateGraph" eType="#//MSDRuntimeStateGraph"
        eOpposite="#//MSDRuntimeStateGraph/elementContainer"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeRoleBindings" upperBound="-1"
        eType="#//ActiveMSSRoleBindings" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMap"
        upperBound="-1" eType="#//ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry"
        transient="true" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MSDModality" eSuperTypes="../../org.scenariotools.runtime/model/runtime.ecore#//Modality">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="monitored" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="hot" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="cold" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="initializing" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="coldViolating" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveProcess">
    <eOperations name="isSafetyViolating" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eParameters name="messageEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
    </eOperations>
    <eOperations name="isColdViolating" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eParameters name="messageEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
    </eOperations>
    <eOperations name="calculateRelevantEvents"/>
    <eOperations name="assignValue">
      <eParameters name="ocl" eType="ecore:EDataType util.ecore#//OCL"/>
      <eParameters name="variableName" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
      <eParameters name="value" eType="ecore:EDataType util.ecore#//PlainJavaObject"/>
    </eOperations>
    <eOperations name="assignValueExpression" eType="ecore:EDataType util.ecore#//PlainJavaObject"
        eExceptions="#//SemanticException #//ParserException">
      <eParameters name="ocl" eType="ecore:EDataType util.ecore#//OCL"/>
      <eParameters name="variableName" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
      <eParameters name="valueOCLExpression" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </eOperations>
    <eOperations name="isRelavant" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eParameters name="messageEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
    </eOperations>
    <eOperations name="progressEnabledHiddenEvents" eType="#//ActiveMSDProgress"/>
    <eOperations name="isInHotCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eOperations name="isInExecutedCut" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="variableValuations" eType="#//ActiveMSDVariableValuations"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="coldForbiddenEvents" upperBound="-1"
        eType="#//NamedElementToMessageEventMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="hotForbiddenEvents" upperBound="-1"
        eType="#//NamedElementToMessageEventMapEntry" containment="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="refers to a forbidden message in the diagram, not to be confused with &quot;safety violating&quot;"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="violatingEvents" upperBound="-1"
        eType="#//NamedElementToMessageEventMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="enabledEvents" upperBound="-1"
        eType="#//NamedElementToMessageEventMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="currentState" eType="#//ActiveState"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="objectSystem" eType="#//MSDObjectSystem"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="id" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSDCut" eSuperTypes="#//ActiveState">
    <eOperations name="progressCutLocationOnLifeline" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eParameters name="lifeline" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline"/>
      <eParameters name="currentFragment" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//InteractionFragment"/>
    </eOperations>
    <eOperations name="terminalCutReached" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eOperations name="isMessageEnabled" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean">
      <eParameters name="message" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Message"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="lifelineToInteractionFragmentMap"
        upperBound="-1" eType="#//LifelineToInteractionFragmentMapEntry" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSDLifelineBindings">
    <eStructuralFeatures xsi:type="ecore:EReference" name="lifelineToEObjectMap" upperBound="-1"
        eType="#//LifelineToEObjectMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="ignoredLifelines" ordered="false"
        upperBound="-1" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="depending on the multiplicity of the role, a lifeline may be bound to no object (not to be confused with an unbound lifeline!!)"/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSDVariableValuations">
    <eStructuralFeatures xsi:type="ecore:EReference" name="variableNameToEObjectValueMap"
        upperBound="-1" eType="#//StringToEObjectMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="variableNameToStringValueMap"
        upperBound="-1" eType="#//StringToStringMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="variableNameToIntegerValueMap"
        upperBound="-1" eType="#//StringToIntegerMapEntry" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="variableNameToBooleanValueMap"
        upperBound="-1" eType="#//StringToBooleanMapEntry" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="LifelineToInteractionFragmentMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" lowerBound="1" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//InteractionFragment"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="StringToEObjectMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" lowerBound="1" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="LifelineToEObjectMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" lowerBound="1" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="NamedElementToMessageEventMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" lowerBound="1" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//NamedElement"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MSDModalMessageEvent" eSuperTypes="../../org.scenariotools.runtime/model/runtime.ecore#//ModalMessageEvent">
    <eStructuralFeatures xsi:type="ecore:EReference" name="enabledInActiveProcess"
        upperBound="-1" eType="#//ActiveProcess"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="coldViolatingInActiveProcess"
        upperBound="-1" eType="#//ActiveProcess"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="safetyViolatingInActiveProcess"
        upperBound="-1" eType="#//ActiveProcess"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parentSymbolicMSDModalMessageEvent"
        eType="#//MSDModalMessageEvent" eOpposite="#//MSDModalMessageEvent/concreteMSDModalMessageEvent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="concreteMSDModalMessageEvent"
        upperBound="-1" eType="#//MSDModalMessageEvent" eOpposite="#//MSDModalMessageEvent/parentSymbolicMSDModalMessageEvent"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MSDObjectSystem" eSuperTypes="../../org.scenariotools.runtime/model/runtime.ecore#//ObjectSystem">
    <eOperations name="lifelineMayBindToEObject" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean">
      <eParameters name="lifeline" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline"/>
      <eParameters name="eObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    </eOperations>
    <eOperations name="roleMayBindToEObject" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean">
      <eParameters name="role" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Property"/>
      <eParameters name="eObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    </eOperations>
    <eOperations name="init">
      <eParameters name="objectSystemRootObjects">
        <eGenericType eClassifier="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EEList">
          <eTypeArguments eClassifier="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
        </eGenericType>
      </eParameters>
      <eParameters name="rolesToEObjectsMappingContainer" eType="ecore:EClass ../../org.scenariotools.msd.roles2eobjects/model/roles2eobjects.ecore#//RoleToEObjectMappingContainer"/>
      <eParameters name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"/>
    </eOperations>
    <eOperations name="init"/>
    <eOperations name="getMessageEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent">
      <eParameters name="eOperation" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EOperation"/>
      <eParameters name="eStructuralFeature" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EStructuralFeature"/>
      <eParameters name="sendingObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
      <eParameters name="receivingObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
      <eParameters name="parameterValue" eType="ecore:EDataType util.ecore#//PlainJavaObject"/>
    </eOperations>
    <eOperations name="getMessageEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent">
      <eParameters name="eOperation" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EOperation"/>
      <eParameters name="eStructuralFeature" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EStructuralFeature"/>
      <eParameters name="sendingObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
      <eParameters name="receivingObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
      <eParameters name="parameterValue" eType="ecore:EDataType util.ecore#//PlainJavaObject"/>
      <eParameters name="emptyMessageEventInstance" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"
        transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="initializingEnvironmentMSDModalMessageEvents"
        upperBound="-1" eType="#//MSDModalMessageEvent" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="initializingSystemMSDModalMessageEvents"
        upperBound="-1" eType="#//MSDModalMessageEvent" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="assumptionMSDInitializingMessageEvents"
        upperBound="-1" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"
        transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="requirementMSDInitializingMessageEvents"
        upperBound="-1" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"
        transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="eObjectToSendableEOperationMap"
        upperBound="-1" eType="#//EObjectToEOperationMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="eObjectToReceivableEOperationMap"
        upperBound="-1" eType="#//EObjectToEOperationMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="eClassToInstanceEObjectMap"
        upperBound="-1" eType="#//EClassToEObjectMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="eObjectsToUMLClassesItIsInstanceOfMap"
        upperBound="-1" eType="#//EObjectToUMLClassMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="staticLifelineToEObjectBindings"
        upperBound="-1" eType="#//LifelineToEObjectMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="staticRoleToEObjectBindings"
        upperBound="-1" eType="#//PropertyToEObjectMapEntry" transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="rolesToEObjectsMappingContainer"
        eType="ecore:EClass ../../org.scenariotools.msd.roles2eobjects/model/roles2eobjects.ecore#//RoleToEObjectMappingContainer"
        transient="true" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="messageEventContainer"
        eType="#//MessageEventContainer" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="messageEventKeyWrapperToMessageEventMap"
        upperBound="-1" eType="#//MessageEventKeyWrapperToMessageEventMapEntry" transient="true"
        containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="ExecutionSemantics">
    <eLiterals name="PlayOut"/>
    <eLiterals name="AlwaysAllEvents" value="1" literal="AlwaysAllRelevantMessageEvents"/>
    <eLiterals name="PlayOutButWaitingPossible" value="2" literal="PlayOutButWaitingPossibleWithActiveEvents"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="StringToStringMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" lowerBound="1" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="value" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="StringToIntegerMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" lowerBound="1" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="value" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EIntegerObject"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="StringToBooleanMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="value" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBooleanObject"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" lowerBound="1" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="ActiveMSDProgress">
    <eLiterals name="progress"/>
    <eLiterals name="noProgress" value="1"/>
    <eLiterals name="terminalCutReached" value="2"/>
    <eLiterals name="coldViolation" value="3"/>
    <eLiterals name="safetyViolation" value="4"/>
    <eLiterals name="timedColdViolation" value="5" literal="timedColdViolation"/>
    <eLiterals name="timedSafetyViolation" value="6" literal="timedSafetyViolation"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EDataType" name="SemanticException" instanceClassName="org.eclipse.ocl.SemanticException"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ParserException" instanceClassName="org.eclipse.ocl.ParserException"/>
  <eClassifiers xsi:type="ecore:EClass" name="MSDRuntimeStateKeyWrapperToMSDRuntimeStateMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//MSDRuntimeState"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//MSDRuntimeStateKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveProcessKeyWrapperToActiveProcessMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//ActiveProcess"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ActiveProcessKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveStateKeyWrapperToActiveStateMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//ActiveState"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ActiveStateKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSDLifelineBindingsKeyWrapperToActiveMSDLifelineBindingsMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//ActiveMSDLifelineBindings"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ActiveMSDLifelineBindingsKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSDVariableValuationsKeyWrapperToActiveMSDVariableValuationsMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//ActiveMSDVariableValuations"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ActiveMSDVariableValuationsKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSSRoleBindingsKeyWrapperToActiveMSSRoleBindingsMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//ActiveMSSRoleBindings"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ActiveMSSRoleBindingsKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MessageEventContainer">
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeMessages" upperBound="-1"
        eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//RuntimeMessage"
        containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="messageEvents" upperBound="-1"
        eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"
        containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MessageEventKeyWrapperToMessageEventMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//MessageEvent"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//MessageEventKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EDataType" name="ActiveStateKeyWrapper" instanceClassName="org.scenariotools.msd.runtime.keywrapper.ActiveStateKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ActiveMSDLifelineBindingsKeyWrapper"
      instanceClassName="org.scenariotools.msd.runtime.keywrapper.ActiveMSDLifelineBindingsKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ActiveMSDVariableValuationsKeyWrapper"
      instanceClassName="org.scenariotools.msd.runtime.keywrapper.ActiveMSDVariableValuationsKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ActiveProcessKeyWrapper" instanceClassName="org.scenariotools.msd.runtime.keywrapper.ActiveProcessKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="MSDRuntimeStateKeyWrapper" instanceClassName="org.scenariotools.msd.runtime.keywrapper.MSDRuntimeStateKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="MessageEventKeyWrapper" instanceClassName="org.scenariotools.msd.runtime.keywrapper.MessageEventKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ObjectSystemKeyWrapper" instanceClassName="org.scenariotools.msd.runtime.keywrapper.ObjectSystemKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ActiveMSSRoleBindingsKeyWrapper"
      instanceClassName="org.scenariotools.msd.runtime.keywrapper.ActiveMSSRoleBindingsKeyWrapper"/>
  <eClassifiers xsi:type="ecore:EClass" name="ObjectSystemKeyWrapperToMSDObjectSystemMapEntry"
      instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="#//MSDObjectSystem"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="key" eType="#//ObjectSystemKeyWrapper"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSS" eSuperTypes="#//ActiveProcess">
    <eStructuralFeatures xsi:type="ecore:EReference" name="roleBindings" eType="#//ActiveMSSRoleBindings"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="stateMachine" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//StateMachine"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSD" eSuperTypes="#//ActiveProcess">
    <eOperations name="addLifelineBinding">
      <eParameters name="lifeline" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Lifeline"/>
      <eParameters name="eObject" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="lifelineBindings" eType="#//ActiveMSDLifelineBindings"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="interaction" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Interaction"
        changeable="false" volatile="true" transient="true" derived="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="msdUtil" eType="ecore:EClass util.ecore#//MSDUtil"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSSRoleBindings">
    <eStructuralFeatures xsi:type="ecore:EReference" name="propertyToEObjectMap" upperBound="-1"
        eType="#//PropertyToEObjectMapEntry" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="PropertyToEObjectMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" lowerBound="1" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Property"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveState">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="hot" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="runtimeUtil" eType="ecore:EClass util.ecore#//RuntimeUtil"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="executed" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ActiveMSSState" eSuperTypes="#//ActiveState">
    <eOperations name="isTransitionEnabled" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean">
      <eParameters name="transition" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Transition"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="umlState" eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//State"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EObjectToEOperationMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" upperBound="-1"
        eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EOperation"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EObjectToUMLClassMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" upperBound="-1"
        eType="ecore:EClass ../../org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EClassToEObjectMapEntry" instanceClassName="java.util.Map$Entry">
    <eStructuralFeatures xsi:type="ecore:EReference" name="value" upperBound="-1"
        eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="key" lowerBound="1" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EClass"/>
  </eClassifiers>
</ecore:EPackage>
