package org.scenariotools.msd.simulation.ui.historyviews;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSDCut;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.simulation.HistoryAgent;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.debug.ScenarioDebugTarget;
import org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener;
import org.scenariotools.msd.simulation.ui.icons.MSDModalMessageEventsIconProvider;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

public class SimulationGraphView extends ViewPart implements IZoomableWorkbenchPart {
	
	private GraphViewer viewer;
	
	public static String SIMULATION_GRAPH = "org.scenariotools.msd.simulation.ui.historyviews.SimulationGraphView";



	private SimulationManager currentSimulationManager = null;
	private HistoryAgent currentHistoryAgent = null;

	private Object filterObject; // stores an object (Launch,
									// ScenarioDebugTarget, ActiveMSDThread,
									// ObjectThread) that was selected last

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection)
						.getFirstElement();
				if (firstElement instanceof ScenarioDebugTarget) {
					selectionChanged((ScenarioDebugTarget) firstElement);
				}
			}
			SimulationGraphView.this.getViewer().refresh();
		}

		private void selectionChanged(ScenarioDebugTarget scenarioDebugTarget) {
			setFilterObject(scenarioDebugTarget);
			setCurrentSimulationManager(scenarioDebugTarget
					.getSimulationManager());
			setInput( getCurrentSimulationManager()
					.getMsdRuntimeStateGraph().getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().values());
		}

	};
	
	/**
	 * This listener is registered at a current simulation agent to be notified
	 * about a changing MSD runtime state list.
	 */
	private ISimulationTraceChangeListener simulationTraceChangeListener = new ISimulationTraceChangeListener() {
		
		@Override
		public void simulationTraceChanged(
				Collection<MSDRuntimeState> simulationGraph) {
			setInput(simulationGraph);		
		}
	};
	
	
//	private Action doubleClickAction;

	private boolean disabled;
	
	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	public class ViewContentProvider{
				
		public Collection<MSDRuntimeState> getNodes() {
			if (currentSimulationManager == null)
				return new ArrayList<MSDRuntimeState>();
			return getCurrentSimulationManager().getMsdRuntimeStateGraph()
					.getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
					.values();
		}
	}
	
	public class SimulationTraceContentProvider extends ArrayContentProvider
			implements IGraphEntityRelationshipContentProvider {

		@Override
		public Object[] getRelationships(Object source, Object destination) {
			List<Transition> rels = new ArrayList<Transition>();
			if(source instanceof MSDRuntimeState && destination instanceof MSDRuntimeState){
				MSDRuntimeState src = (MSDRuntimeState) source;
				MSDRuntimeState dest = (MSDRuntimeState) destination;
				for(Transition t : src.getOutgoingTransition()){
					if(t.getTargetState() != null && t.getTargetState().equals(dest)){
						rels.add(t);
					}
				}
			}
			return rels.toArray();
		}
	}
	
	public class SimulationTraceLabelProvider extends LabelProvider implements ISelfStyleProvider{
		
		private int stateNumber = 0;
		public boolean showImage = false;
		
		@Override
		public String getText(Object element) {
			if (element instanceof MSDRuntimeState) {
				MSDRuntimeState state = (MSDRuntimeState) element;
				String result =  "" + state.getStringToStringAnnotationMap().get("passedIndex");
				if(state.isSafetyViolationOccurredInRequirements()){
					result += "\nViolation in Requirements";
				}
				if(state.isSafetyViolationOccurredInAssumptions() ){
					result += "\nViolation in Assumptions";
				}
				return result;
			}
			if (element instanceof Transition) {
				Transition transition = (Transition) element;
				Event event = transition.getEvent();
				if (event instanceof MessageEvent) {
					return RuntimeUtil.Helper.getMessageEventString((MessageEvent) event);
				} else {
					return event.toString();
				}
			}
			throw new RuntimeException("Wrong type: "
					+ element.getClass().toString());
		}
		
		@Override
		public Image getImage(Object element) {
			if(!showImage) return super.getImage(element);
			
			if (element instanceof Transition) {
				Transition t = (Transition) element;
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) 
						((MSDRuntimeState) t.getSourceState())
						.getMessageEventToModalMessageEventMap().get(
								t.getEvent());
				return MSDModalMessageEventsIconProvider
						.getImageForCondensatedMessageEvent(
								msdModalMessageEvent, isDisabled());
			}else{
				return super.getImage(element);	
			}
		}

		@Override
		public void selfStyleConnection(Object element,
				GraphConnection connection) {

			if (element instanceof Transition) {
				Transition t = (Transition) element;
				Event event = t.getEvent();
				if (event instanceof MessageEvent) {
					if (((MSDRuntimeState) t.getSourceState()).isEnvironmentMessageEvent((MessageEvent) t.getEvent())) {
						connection.setLineStyle(Graphics.LINE_DASH);
					} else {
						connection.setLineStyle(Graphics.LINE_SOLID);
					}
				}
				connection.setLineWidth(2);
			}
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			if(element instanceof MSDRuntimeState){
				MSDRuntimeState state = (MSDRuntimeState) element;
				if(state.isSafetyViolationOccurredInAssumptions() || state.isSafetyViolationOccurredInRequirements()){
					node.setBorderColor(new Color(Display.getCurrent(), 255, 0, 0));
				}else{
					//node.setBorderColor(new Color(Display.getCurrent(), 0, 255, 0));
				}
				node.setTooltip(new Label(getMsdStateString(state)));
				
				if(state.equals(currentSimulationManager.getCurrentMSDRuntimeState())){
					node.setBackgroundColor(new Color(Display.getCurrent(), 100, 149, 237));
				}else{
					node.setBackgroundColor(new Color(Display.getCurrent(), 220, 220, 220));
				}
			}
			
		}
		
		protected String getStateNumber(RuntimeState state){
			String result = "";
			String passedIndex = state.getStringToStringAnnotationMap().get(
					"passedIndex");
			if (passedIndex != null && !passedIndex.isEmpty())
				result+=passedIndex + "\n";
			
			if (passedIndex == null || passedIndex.isEmpty())
				result = (stateNumber++) + "\n";
			return result;
		}
		
		protected String getMsdStateString(MSDRuntimeState state) {
			StringBuilder stringBuilder = new StringBuilder();
			for (ActiveProcess activeProcess : state.getActiveProcesses()) {
				if (activeProcess instanceof ActiveMSD)
					stringBuilder
							.append(getActiveMsdString((ActiveMSD) activeProcess)
									+ "\n");
				else if (activeProcess instanceof ActiveMSS)
					stringBuilder
							.append(getActiveMssString((ActiveMSS) activeProcess)
									+ "\n");
				else
					stringBuilder.append(activeProcess.getCurrentState() + "\n");
			}
			
			if (state.isSafetyViolationOccurredInAssumptions()) {
				stringBuilder
						.append("- Safety-violation In Assumptions -\n");
			}
			if (state.isSafetyViolationOccurredInRequirements()) {
				stringBuilder
						.append("- Safety-violation In Requirements -\n");
			}
			return stringBuilder.toString();
		}

		protected String getActiveMssString(ActiveMSS amss) {
			String returnString = "";
			if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(amss)) {
				returnString += "A-MSS ";
			} else {
				returnString += "R-MSS ";
			}

			String cutString = "";
			cutString += " (" + amss.getCurrentState().getUmlState().getName()
					+ ")";

			return returnString + amss.getStateMachine().getName() + cutString;
		}

		private String getActiveMsdString(ActiveMSD amsd) {
			String returnString = "";
			if (RuntimeUtil.Helper.isEnvironmentAssumption(amsd.getInteraction())) {
				returnString += "A-MSD ";
			} else {
				returnString += "R-MSD ";
			}

			String cutString = "";
			cutString += " (";

			LinkedList<Lifeline> lifelines = new LinkedList<Lifeline>(amsd
					.getLifelineBindings().getLifelineToEObjectMap().keySet());
			Collections.sort(lifelines, new Comparator<Lifeline>() {
				@Override
				public int compare(Lifeline o1, Lifeline o2) {
					return o1.getRepresents().getName()
							.compareTo(o2.getRepresents().getName());
				}
			});
			int i = 0;
			int numLifelines = lifelines.size();
			for (Lifeline lifeline : lifelines) {
				cutString += lifeline.getRepresents().getName() + ":";
				cutString += RuntimeUtil.Helper
						.getInteractionFragmentNumberOnLifeline(
								((ActiveMSDCut) amsd.getCurrentState())
										.getLifelineToInteractionFragmentMap().get(
												lifeline), lifeline);

				if (i < numLifelines - 1) {// not last one
					cutString += ", ";
				}
				i++;
			}
			cutString += ")";

			return returnString + amsd.getInteraction().getName() + cutString;
		}
	}



	/**
	 * This is a callback that create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		viewer.setContentProvider(new SimulationTraceContentProvider());
		viewer.setLabelProvider(new SimulationTraceLabelProvider());
		ViewContentProvider model = new ViewContentProvider();
		viewer.setInput(model.getNodes());
		LayoutAlgorithm layout = setLayout(1);
		viewer.setLayoutAlgorithm(layout, true);
		viewer.applyLayout();
		fillToolBar();
				
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
				if (obj instanceof MSDRuntimeState) {
					MSDRuntimeState state = (MSDRuntimeState) ((IStructuredSelection) event
							.getSelection()).getFirstElement();
					getCurrentHistoryAgent().setNextState(state);

					Display.getCurrent().asyncExec(new Runnable() {
						@Override
						public void run() {
							SimulationGraphView.this
									.getCurrentHistoryAgent()
									.getSimulationManager()
									.loadStateFromHistoryAgent(
											SimulationGraphView.this
													.getCurrentHistoryAgent());
						}
					});

				}

			}
		});

		// viewer.addSelectionChangedListener(new ISelectionChangedListener() {
		// @Override
		// public void selectionChanged(SelectionChangedEvent event) {
		// System.out.println("Selected: " + event);
		// }});

		makeActions();
		// hookContextMenu();
		// hookDoubleClickAction();
		contributeToActionBars();
		hookSelectionListener();

	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}


	private class ScenarioToolsSpringLayoutAlgorithm extends SpringLayoutAlgorithm {
		public ScenarioToolsSpringLayoutAlgorithm(int s) {
			super(s);
		}

		/**
		 * Minimum distance considered between nodes
		 */
		protected static final double MIN_DISTANCE = 10.0d;
	}

	private LayoutAlgorithm setLayout(int layoutNum) {
		LayoutAlgorithm layout;
		switch (layoutNum) {
			case 0:	layout = new ScenarioToolsSpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					((ScenarioToolsSpringLayoutAlgorithm) layout).setRandom(false);
			//		((SpringLayoutAlgorithm) layout).setSpringLength(1);
			//		((SpringLayoutAlgorithm) layout).setSpringMove(1);
			//		((SpringLayoutAlgorithm) layout).setSpringStrain(1.0d);
			//		((SpringLayoutAlgorithm) layout).setBounds(1000, 1000, 1000, 1000);
					break;	
			case 1:	layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			case 2: layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			default:layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
		}
		
		// layout = new GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// many intersections
		// layout = new HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// not so good

		return layout;

	}
	
	private String getLayoutName(int layoutNum) {
		String layout;
		switch (layoutNum) {
			case 0:	layout = "Spring Layout";
					break;
			case 1:	layout = "Tree Layout";
					break;
			case 2: layout = "Radial Layout";
					break;
			default:layout = "Tree Layout";
					break;
		}
		return layout;
	}
	
	protected int currentLayout = 1;
	protected final int numLayouts = 3;
	
	protected void changeLayout(){
		currentLayout++;
		getViewer().setLayoutAlgorithm(setLayout(currentLayout % numLayouts));
		getViewer().applyLayout();
		changeLayout.setToolTipText("Change Layout. \nCurrent: " 
										+ getLayoutName(currentLayout % numLayouts) 
										+ "\nNext: " 
										+ getLayoutName((currentLayout+1) % numLayouts));
	}

	private void fillToolBar() {
		ZoomContributionViewItem toolbarZoomContributionViewItem = new ZoomContributionViewItem(
				this);
		IActionBars bars = getViewSite().getActionBars();
		bars.getMenuManager().add(toolbarZoomContributionViewItem);

	}

	protected GraphViewer getViewer() {
		return viewer;
	}
	
//	protected void hookContextMenu() {
//		MenuManager menuMgr = new MenuManager("#PopupMenu");
//		menuMgr.setRemoveAllWhenShown(true);
//		menuMgr.addMenuListener(new IMenuListener() {
//			public void menuAboutToShow(IMenuManager manager) {
//				fillContextMenu(manager);
//			}
//		});
//		Menu menu = menuMgr.createContextMenu(viewer.getControl());
//		viewer.getControl().setMenu(menu);
//		getSite().registerContextMenu(menuMgr, viewer);
//	}

	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

//	protected void fillContextMenu(IMenuManager manager) {
//		 manager.add(new Action("Delete"){
//			 @Override
//			 public void run(){
//				 System.out.println("Delete");
//			 }
//			 
//			 @Override
//			 public void runWithEvent(Event event){
//				 System.out.println("Delete " + event.data);
//				 Object obj = event.data;
//				if (obj instanceof MSDRuntimeState) {
//					MSDRuntimeState state = (MSDRuntimeState) event.data;
//				}
//			 }
//		 });
//		// manager.add(pause);
//		// manager.add(new Separator());
//		//drillDownAdapter.addNavigationActions(manager);
//		// Other plug-ins can contribute there actions here
//		//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
//	}

	
	
//	protected void hookDoubleClickAction() {
//		viewer.addDoubleClickListener(new IDoubleClickListener() {
//			public void doubleClick(DoubleClickEvent event) {
//				doubleClickAction.run();
//			}
//		});
//	}


	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		//viewer.getControl().setFocus();
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		getViewer().refresh();
	}
	
	protected int numAncestors = 0;
	
	public void setInput(Collection<MSDRuntimeState> msdRuntimeState){
		
		if(numAncestors == 0){
			Collection<MSDRuntimeState> temp = new HashSet<MSDRuntimeState>();
			temp.addAll(msdRuntimeState);
			getViewer().setInput(temp);
			getViewer().refresh();
			
//			Collection<MSDRuntimeState> newStates = new HashSet<MSDRuntimeState>();
//			newStates.addAll(msdRuntimeState);
//			if(getInput() instanceof Collection<?>)
//				newStates.removeAll(getInput());
//			if(newStates.size() > 1){
//				Collection<MSDRuntimeState> temp = new HashSet<MSDRuntimeState>();
//				temp.addAll(msdRuntimeState);
//				temp.removeAll(newStates);
//				for(MSDRuntimeState state : newStates){
//					temp.add(state);
//					final Collection<MSDRuntimeState> input = temp;
//					Display.getCurrent().asyncExec(new Runnable() {
//						@Override
//						public void run() {
//							SimulationGraphView.this.getViewer().setInput(input);
//							SimulationGraphView.this.getViewer().refresh();
//							SimulationGraphView.this.getViewer().getControl().update();
//						}
//					});
////					temp.add(state);
////					getViewer().setInput(temp);
////					getViewer().refresh();
//				}
//			}else{
//				Collection<MSDRuntimeState> temp = new HashSet<MSDRuntimeState>();
//				temp.addAll(msdRuntimeState);
//				getViewer().setInput(temp);
//			}
		}else{
			//BFS forward and backward
			Collection<MSDRuntimeState> closed = new HashSet<MSDRuntimeState>();
			MSDRuntimeState currentState = currentSimulationManager.getCurrentMSDRuntimeState();
			
			Queue<MSDRuntimeState> succFw = new LinkedList<MSDRuntimeState>();
			Queue<MSDRuntimeState> openFw = new LinkedList<MSDRuntimeState>();
			Queue<MSDRuntimeState> succBw = new LinkedList<MSDRuntimeState>();
			Queue<MSDRuntimeState> openBw = new LinkedList<MSDRuntimeState>();
			openFw.add(currentState);
			openBw.add(currentState);
			if(msdRuntimeState.contains(currentState)){
				for(int i = 0; i < numAncestors; i++){
					//BFS forward
					while(!openFw.isEmpty()){
						closed.add(openFw.peek());
						for(Transition t : openFw.remove().getOutgoingTransition()){
							MSDRuntimeState s = (MSDRuntimeState) t.getTargetState();
							succFw.add(s);
						}
					}
					openFw.addAll(succFw);
					//BFS backward
					while(!openBw.isEmpty()){
						closed.add(openBw.peek());
						for(Transition t : openBw.remove().getIncomingTransition()){
							MSDRuntimeState s = (MSDRuntimeState) t.getSourceState();
							succBw.add(s);
						}
					}
					openBw.addAll(succBw);
				}
			}
			getViewer().setInput((Collection<MSDRuntimeState>)closed);
			getViewer().refresh();
		}
	}
	
	@SuppressWarnings("unchecked")
	protected Collection<MSDRuntimeState> getInput(){
		if (getViewer().getInput() instanceof Collection<?>)
			return (Collection<MSDRuntimeState>) getViewer().getInput();
		else return (Collection<MSDRuntimeState>) new HashSet<MSDRuntimeState>();
	}

	/**
	 * This method is called 1. when the simulation manager notifies the view
	 * (through the {@link IActiveSimulationAgentChangeListener}) about a change
	 * of the current simulation agent. 2. if the {@link
	 * setCurrentSimulationManager(SimulationManager)} is called.
	 * 
	 * @param newUserInteractingSimulationAgent
	 */
	protected void setCurrentHistoryAgent(
			HistoryAgent newHistoryAgent) {
		HistoryAgent oldHistoryAgent = currentHistoryAgent;
		if (oldHistoryAgent != null)
			oldHistoryAgent
					.getRegisteredSimulationTraceChangeListener().remove(
							simulationTraceChangeListener);

		newHistoryAgent
				.getRegisteredSimulationTraceChangeListener().add(
						simulationTraceChangeListener);
		currentHistoryAgent = newHistoryAgent;
		setInput(currentHistoryAgent
				.getSimulationManager().getMsdRuntimeStateGraph().getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().values());
	}

	protected HistoryAgent getCurrentHistoryAgent() {
		return currentHistoryAgent;
	}

	/**
	 * This method is called when the selection in the debug view has changed.
	 * Calling this method results in re-registering the
	 * activeSimulationAgentChangeListener to that simulation manager and
	 * setting the current user-interacting simulation agent (calling {@link
	 * setCurrentUserInteractingSimulationAgent(UserInteractingSimulationAgent
	 * newUserInteractingSimulationAgent)})
	 * 
	 * @param newSimulationManager
	 */
	protected void setCurrentSimulationManager(
			SimulationManager newSimulationManager) {
		SimulationManager oldSimulationManager = this.currentSimulationManager;

		if (oldSimulationManager != null){
//			oldSimulationManager
//					.getRegisteredActiveSimulationAgentChangeListener().remove(
//							activeSimulationAgentChangeListener);
//			oldSimulationManager
//					.getRegisteredStepPerformedListener().remove(
//							stepPerformedListener);
		}

//		newSimulationManager.getRegisteredActiveSimulationAgentChangeListener()
//				.add(activeSimulationAgentChangeListener);

		currentSimulationManager = newSimulationManager;

		if (newSimulationManager.getCurrentHistoryAgent() instanceof HistoryAgent)
			setCurrentHistoryAgent((HistoryAgent) newSimulationManager
					.getCurrentHistoryAgent());

	}

	protected SimulationManager getCurrentSimulationManager() {
		return currentSimulationManager;
	}


	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}

	protected Object getFilterObject() {
		return filterObject;
	}

	protected void setFilterObject(Object filterObject) {
		this.filterObject = filterObject;
	}

	protected void refreshCurrentModelMessageEventsList() {
		if (getCurrentSimulationManager() == null
				|| getCurrentSimulationManager().getMsdRuntimeStateGraph()
						.getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap()
						.values() == null)
			return;

		setInput((Collection<MSDRuntimeState>) getCurrentSimulationManager()
				.getMsdRuntimeStateGraph().getMsdRuntimeStateKeyWrapperToMSDRuntimeStateMap().values());
	}

	protected void toggleShowImages() {
		if (toggleSohwImageOnMessages.isChecked()) {
			((SimulationTraceLabelProvider)getViewer().getLabelProvider()).showImage = true;
			refreshCurrentModelMessageEventsList();
		} else {
			((SimulationTraceLabelProvider)getViewer().getLabelProvider()).showImage = false;
			refreshCurrentModelMessageEventsList();
		}
	}
	
	protected void toggleShowOnlyUpToXAncestors(int count){
		if (toggleShowOnlyUpToXNeighbours.isChecked()) {
			numAncestors = count;
			refreshCurrentModelMessageEventsList();
		} else {
			numAncestors = 0;
			refreshCurrentModelMessageEventsList();
			//Workaround, without the graph concentrate the nodes sometimes after toggle display mode
			Display.getCurrent().asyncExec(new Runnable() {
				@Override
				public void run() {
					SimulationGraphView.this.refreshCurrentModelMessageEventsList();
				}
			});
		}
	}

	Action toggleSohwImageOnMessages;
	Action toggleShowOnlyUpToXNeighbours;
	Action refresh;
	Action changeLayout;
	

	protected void makeActions() {

		toggleSohwImageOnMessages = new Action(
				"Show message icons", Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowImages();
			}
		};
		toggleSohwImageOnMessages
				.setToolTipText("Show Message icons.");
		toggleSohwImageOnMessages.setImageDescriptor(Activator.getImageDescriptor("img/show-message-icons.png"));


		toggleShowOnlyUpToXNeighbours = new Action(
				"Show only successors and antecessors up to a depth of five.", Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowOnlyUpToXAncestors(5);
			}
		};
		toggleShowOnlyUpToXNeighbours
				.setToolTipText("Show only successors and antecessors up to a depth of five.");
		toggleShowOnlyUpToXNeighbours.setImageDescriptor(Activator
				.getImageDescriptor("img/subtree.png"));
		
		refresh = new Action("Refresh Simulation Graph", Action.AS_PUSH_BUTTON){
			public void run(){
				SimulationGraphView.this.refreshCurrentModelMessageEventsList();
			}
		};
		refresh.setToolTipText("Refresh Simulation Graph");
		refresh.setImageDescriptor(Activator
				.getImageDescriptor("img/refresh.png"));
		
		changeLayout = new Action("Change Layout.", Action.AS_PUSH_BUTTON){
			public void run(){
				SimulationGraphView.this.changeLayout();
			}
		};
		changeLayout.setToolTipText("Change Layout. \nCurrent: " + getLayoutName(currentLayout) + "\nNext: " + getLayoutName(currentLayout+1));
		changeLayout.setImageDescriptor(Activator
				.getImageDescriptor("img/change-layout.png"));
	}

	protected void fillLocalPullDown(IMenuManager manager) {
//		super.fillLocalPullDown(manager);

		manager.add(toggleSohwImageOnMessages);
		manager.add(toggleShowOnlyUpToXNeighbours);
		manager.add(refresh);
		manager.add(changeLayout);
	}

	protected void fillLocalToolBar(IToolBarManager manager) {
//		super.fillLocalToolBar(manager);

		manager.add(toggleSohwImageOnMessages);
		manager.add(toggleShowOnlyUpToXNeighbours);
		manager.add(refresh);
		manager.add(changeLayout);
	}

	@Override
	public void dispose() {
		disposeSelectionListener();
		super.dispose();
	}
}
