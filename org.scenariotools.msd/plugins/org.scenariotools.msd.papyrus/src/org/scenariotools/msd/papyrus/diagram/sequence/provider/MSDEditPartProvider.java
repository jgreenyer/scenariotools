package org.scenariotools.msd.papyrus.diagram.sequence.provider;

import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.uml.diagram.sequence.edit.parts.CombinedFragmentEditPart;
import org.eclipse.papyrus.uml.diagram.sequence.edit.parts.Message2EditPart;
import org.eclipse.papyrus.uml.diagram.sequence.part.UMLVisualIDRegistry;
import org.eclipse.papyrus.uml.diagram.sequence.providers.CustomEditPartProvider;
import org.scenariotools.msd.papyrus.diagram.sequence.edit.parts.CombinedFragmentEditPartOverride;
import org.scenariotools.msd.papyrus.diagram.sequence.edit.parts.Message2EditPartOverride;

public class MSDEditPartProvider extends CustomEditPartProvider {
	public MSDEditPartProvider() {
	}

	@Override
	protected IGraphicalEditPart createCustomEditPart(View view) {
		if (UMLVisualIDRegistry.getVisualID(view) == Message2EditPart.VISUAL_ID) {
			return new Message2EditPartOverride(view);
		} else if (UMLVisualIDRegistry.getVisualID(view) == CombinedFragmentEditPart.VISUAL_ID) {
			return new CombinedFragmentEditPartOverride(view);
		}
		return super.createCustomEditPart(view);
	}

	@Override
	protected IGraphicalEditPart createEditPart(View view) {
		return super.createEditPart(view);
	}

}
