package org.scenariotools.msd.papyrus.diagram.sequence.edit.parts;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.uml.diagram.sequence.edit.parts.CustomMessage2EditPart;
import org.eclipse.papyrus.uml.diagram.sequence.figures.MessageAsync;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Stereotype;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.simulation.debug.ScenarioDebugTarget;
import org.scenariotools.msd.simulation.listener.IStepPerformedListener;

public class Message2EditPartOverride extends CustomMessage2EditPart implements IStepPerformedListener {
	private Message msg; 
	private MessageAsync msgFig;

	public Message2EditPartOverride(View view) {
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification) {
		super.handleNotificationEvent(notification);
		refreshVisuals();
	}
	
	protected void refreshVisuals(){
		super.refreshVisuals();
		refreshLineStyle();
	}

	private void refreshLineStyle() {
		this.msg = (Message) this.resolveSemanticElement();
		Stereotype stereotype = msg.getAppliedStereotype("Modal::ModalMessage");
		if (stereotype != null) {
			EnumerationLiteral kind = (EnumerationLiteral) msg.getValue(
					stereotype, "execution");

			if (kind.getName().equals("Execute")) {
				msgFig.setLineStyle(Graphics.LINE_SOLID);
			} else {
				msgFig.setLineStyle(Graphics.LINE_DASH);
			}
		}
	}

	protected void refreshForegroundColor() {
		super.refreshForegroundColor();
		
		this.msg = (Message) this.resolveSemanticElement();
		if (msg == null) return;
		Stereotype stereotype = msg.getAppliedStereotype("Modal::ModalMessage");

		if (stereotype != null) {
			EnumerationLiteral temp = (EnumerationLiteral) msg.getValue(
					stereotype, "temperature");
			
			if (temp.getName().equals("Hot")) {
				msgFig.setForegroundColor(ColorConstants.red);
			} else {
				msgFig.setForegroundColor(ColorConstants.blue);
			}
		}
	}

	protected Connection createConnectionFigure() {
		Connection conFig = super.createConnectionFigure();
		if (conFig instanceof MessageAsync) {
			msgFig = (MessageAsync) conFig;
			msg = (Message) ((Edge) this.getModel()).getElement();
			
			stepPerformed();
			
			//register for changes in the simulation
			ILaunchManager launchManager = DebugPlugin.getDefault()
					.getLaunchManager();
			for(ILaunch launch: launchManager.getLaunches()){
				for(IDebugTarget target:launch.getDebugTargets()){
					if(target instanceof ScenarioDebugTarget){
						ScenarioDebugTarget scenTarget=(ScenarioDebugTarget) target;
						scenTarget.getSimulationManager().getRegisteredStepPerformedListener().add(this);
					}
				}
			}
			
			
		}
		return conFig;
	}
	
	private boolean isMessageEnabled(Message msg){
		ILaunchManager launchManager = DebugPlugin.getDefault()
				.getLaunchManager();
		for(ILaunch launch: launchManager.getLaunches()){
			for(IDebugTarget target:launch.getDebugTargets()){
				if(target instanceof ScenarioDebugTarget){
					ScenarioDebugTarget scenTarget=(ScenarioDebugTarget) target;
					EList<ActiveProcess> processes=scenTarget.getSimulationManager().getCurrentMSDRuntimeState().getActiveProcesses();
					for(ActiveProcess process:processes){
						for(NamedElement element:process.getEnabledEvents().keySet()){
							if(EcoreUtil.equals(element, msg))
								return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void stepPerformed() {
		if(isMessageEnabled(msg)){
			msgFig.setLineWidth(6);
		}
		else{
			msgFig.setLineWidth(2);
		}
	}
}
