package org.scenariotools.msd.papyrus.diagram.sequence.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.uml.diagram.sequence.edit.parts.CombinedFragmentEditPart;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Stereotype;

public class CombinedFragmentEditPartOverride extends CombinedFragmentEditPart {

	public CombinedFragmentEditPartOverride(View view) {
		super(view);
	}

	protected void refreshForegroundColor() {
		super.refreshForegroundColor();

		CombinedFragment combinedFragment = (CombinedFragment) this
				.resolveSemanticElement();
		Stereotype stereotype = combinedFragment
				.getAppliedStereotype("Modal::TimeCondition");
		if (stereotype == null)
			stereotype = combinedFragment
					.getAppliedStereotype("Modal::Condition");

		if (stereotype != null) {
			EnumerationLiteral temp = (EnumerationLiteral) combinedFragment
					.getValue(stereotype, "temperature");

			if (temp.getName().equals("Hot")) {
				setForegroundColor(ColorConstants.red);
			} else {
				setForegroundColor(ColorConstants.blue);
			}
		}
	}

}
