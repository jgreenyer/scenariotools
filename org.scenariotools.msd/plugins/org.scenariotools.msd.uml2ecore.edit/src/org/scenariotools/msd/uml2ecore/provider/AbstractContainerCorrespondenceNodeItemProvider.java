/**
 */
package org.scenariotools.msd.uml2ecore.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode;
import org.scenariotools.msd.uml2ecore.Uml2ecoreFactory;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractContainerCorrespondenceNodeItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractContainerCorrespondenceNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_AbstractContainerCorrespondenceNode_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractContainerCorrespondenceNode.class)) {
			case Uml2ecorePackage.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlPackage2EPackage()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlClass2EClass()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createfirstProp2ERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlAssoz2ERefe()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createsecProp2ERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlGen2EInh()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createFirstProp2SelfERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createSecProp2SelfERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createSelfAss2ERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createDirectedAssoz2ERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlClassProp2EAttr()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createDirectedSelfAss2ERef()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUMLPrimitiveDataTypeToECorePrimitiveDataType()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlMetamodel2EcoreMeta()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUMLSecMetaModel2EcoreMeta()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlOp2EOp()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUMLOpParam2EParam()));

		newChildDescriptors.add
			(createChildParameter
				(Uml2ecorePackage.Literals.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP,
				 Uml2ecoreFactory.eINSTANCE.createUmlSetOp2EStructFeat()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return Uml2ecoreEditPlugin.INSTANCE;
	}

}
