/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Class Prop2 EAttr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getUmlprop <em>Umlprop</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEAttribute <em>EAttribute</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEClassifier <em>EClassifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClassProp2EAttr()
 * @model
 * @generated
 */
public interface UmlClassProp2EAttr extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umlprop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umlprop</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umlprop</em>' reference.
	 * @see #setUmlprop(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClassProp2EAttr_Umlprop()
	 * @model
	 * @generated
	 */
	Property getUmlprop();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getUmlprop <em>Umlprop</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umlprop</em>' reference.
	 * @see #getUmlprop()
	 * @generated
	 */
	void setUmlprop(Property value);

	/**
	 * Returns the value of the '<em><b>EAttribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EAttribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EAttribute</em>' reference.
	 * @see #setEAttribute(EAttribute)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClassProp2EAttr_EAttribute()
	 * @model
	 * @generated
	 */
	EAttribute getEAttribute();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEAttribute <em>EAttribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EAttribute</em>' reference.
	 * @see #getEAttribute()
	 * @generated
	 */
	void setEAttribute(EAttribute value);

	/**
	 * Returns the value of the '<em><b>EClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClassifier</em>' reference.
	 * @see #setEClassifier(EClassifier)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClassProp2EAttr_EClassifier()
	 * @model
	 * @generated
	 */
	EClassifier getEClassifier();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEClassifier <em>EClassifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EClassifier</em>' reference.
	 * @see #getEClassifier()
	 * @generated
	 */
	void setEClassifier(EClassifier value);

} // UmlClassProp2EAttr
