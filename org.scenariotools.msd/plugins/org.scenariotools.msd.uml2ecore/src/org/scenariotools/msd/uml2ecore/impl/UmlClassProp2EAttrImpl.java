/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Class Prop2 EAttr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl#getUmlprop <em>Umlprop</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl#getEAttribute <em>EAttribute</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl#getEClassifier <em>EClassifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlClassProp2EAttrImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlClassProp2EAttr {
	/**
	 * The cached value of the '{@link #getUmlprop() <em>Umlprop</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlprop()
	 * @generated
	 * @ordered
	 */
	protected Property umlprop;

	/**
	 * The cached value of the '{@link #getEAttribute() <em>EAttribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEAttribute()
	 * @generated
	 * @ordered
	 */
	protected EAttribute eAttribute;

	/**
	 * The cached value of the '{@link #getEClassifier() <em>EClassifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier eClassifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlClassProp2EAttrImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_CLASS_PROP2_EATTR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getUmlprop() {
		if (umlprop != null && umlprop.eIsProxy()) {
			InternalEObject oldUmlprop = (InternalEObject)umlprop;
			umlprop = (Property)eResolveProxy(oldUmlprop);
			if (umlprop != oldUmlprop) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP, oldUmlprop, umlprop));
			}
		}
		return umlprop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetUmlprop() {
		return umlprop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlprop(Property newUmlprop) {
		Property oldUmlprop = umlprop;
		umlprop = newUmlprop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP, oldUmlprop, umlprop));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEAttribute() {
		if (eAttribute != null && eAttribute.eIsProxy()) {
			InternalEObject oldEAttribute = (InternalEObject)eAttribute;
			eAttribute = (EAttribute)eResolveProxy(oldEAttribute);
			if (eAttribute != oldEAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE, oldEAttribute, eAttribute));
			}
		}
		return eAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute basicGetEAttribute() {
		return eAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEAttribute(EAttribute newEAttribute) {
		EAttribute oldEAttribute = eAttribute;
		eAttribute = newEAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE, oldEAttribute, eAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getEClassifier() {
		if (eClassifier != null && eClassifier.eIsProxy()) {
			InternalEObject oldEClassifier = (InternalEObject)eClassifier;
			eClassifier = (EClassifier)eResolveProxy(oldEClassifier);
			if (eClassifier != oldEClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER, oldEClassifier, eClassifier));
			}
		}
		return eClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetEClassifier() {
		return eClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEClassifier(EClassifier newEClassifier) {
		EClassifier oldEClassifier = eClassifier;
		eClassifier = newEClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER, oldEClassifier, eClassifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP:
				if (resolve) return getUmlprop();
				return basicGetUmlprop();
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE:
				if (resolve) return getEAttribute();
				return basicGetEAttribute();
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER:
				if (resolve) return getEClassifier();
				return basicGetEClassifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP:
				setUmlprop((Property)newValue);
				return;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE:
				setEAttribute((EAttribute)newValue);
				return;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER:
				setEClassifier((EClassifier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP:
				setUmlprop((Property)null);
				return;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE:
				setEAttribute((EAttribute)null);
				return;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER:
				setEClassifier((EClassifier)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__UMLPROP:
				return umlprop != null;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__EATTRIBUTE:
				return eAttribute != null;
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR__ECLASSIFIER:
				return eClassifier != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlClassProp2EAttrImpl
