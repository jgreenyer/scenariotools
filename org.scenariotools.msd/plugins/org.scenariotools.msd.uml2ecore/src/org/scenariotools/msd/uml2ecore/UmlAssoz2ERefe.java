/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Association;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Assoz2 ERefe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2ERef <em>Ass2 ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2Ref2 <em>Ass2 Ref2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlAssoz2ERefe()
 * @model
 * @generated
 */
public interface UmlAssoz2ERefe extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umla</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umla</em>' reference.
	 * @see #setUmla(Association)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlAssoz2ERefe_Umla()
	 * @model
	 * @generated
	 */
	Association getUmla();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getUmla <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umla</em>' reference.
	 * @see #getUmla()
	 * @generated
	 */
	void setUmla(Association value);

	/**
	 * Returns the value of the '<em><b>Ass2 ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ass2 ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ass2 ERef</em>' reference.
	 * @see #setAss2ERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlAssoz2ERefe_Ass2ERef()
	 * @model
	 * @generated
	 */
	EReference getAss2ERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2ERef <em>Ass2 ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ass2 ERef</em>' reference.
	 * @see #getAss2ERef()
	 * @generated
	 */
	void setAss2ERef(EReference value);

	/**
	 * Returns the value of the '<em><b>Ass2 Ref2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ass2 Ref2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ass2 Ref2</em>' reference.
	 * @see #setAss2Ref2(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlAssoz2ERefe_Ass2Ref2()
	 * @model
	 * @generated
	 */
	EReference getAss2Ref2();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2Ref2 <em>Ass2 Ref2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ass2 Ref2</em>' reference.
	 * @see #getAss2Ref2()
	 * @generated
	 */
	void setAss2Ref2(EReference value);

} // UmlAssoz2ERefe
