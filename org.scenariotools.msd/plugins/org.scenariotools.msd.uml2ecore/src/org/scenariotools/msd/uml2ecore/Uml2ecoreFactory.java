/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage
 * @generated
 */
public interface Uml2ecoreFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Uml2ecoreFactory eINSTANCE = org.scenariotools.msd.uml2ecore.impl.Uml2ecoreFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Uml Package2 EPackage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Package2 EPackage</em>'.
	 * @generated
	 */
	UmlPackage2EPackage createUmlPackage2EPackage();

	/**
	 * Returns a new object of class '<em>Uml Class2 EClass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Class2 EClass</em>'.
	 * @generated
	 */
	UmlClass2EClass createUmlClass2EClass();

	/**
	 * Returns a new object of class '<em>first Prop2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>first Prop2 ERef</em>'.
	 * @generated
	 */
	firstProp2ERef createfirstProp2ERef();

	/**
	 * Returns a new object of class '<em>Uml Assoz2 ERefe</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Assoz2 ERefe</em>'.
	 * @generated
	 */
	UmlAssoz2ERefe createUmlAssoz2ERefe();

	/**
	 * Returns a new object of class '<em>sec Prop2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>sec Prop2 ERef</em>'.
	 * @generated
	 */
	secProp2ERef createsecProp2ERef();

	/**
	 * Returns a new object of class '<em>Uml Gen2 EInh</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Gen2 EInh</em>'.
	 * @generated
	 */
	UmlGen2EInh createUmlGen2EInh();

	/**
	 * Returns a new object of class '<em>First Prop2 Self ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>First Prop2 Self ERef</em>'.
	 * @generated
	 */
	FirstProp2SelfERef createFirstProp2SelfERef();

	/**
	 * Returns a new object of class '<em>Sec Prop2 Self ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sec Prop2 Self ERef</em>'.
	 * @generated
	 */
	SecProp2SelfERef createSecProp2SelfERef();

	/**
	 * Returns a new object of class '<em>Self Ass2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Self Ass2 ERef</em>'.
	 * @generated
	 */
	SelfAss2ERef createSelfAss2ERef();

	/**
	 * Returns a new object of class '<em>Directed Assoz2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Directed Assoz2 ERef</em>'.
	 * @generated
	 */
	DirectedAssoz2ERef createDirectedAssoz2ERef();

	/**
	 * Returns a new object of class '<em>Uml Class Prop2 EAttr</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Class Prop2 EAttr</em>'.
	 * @generated
	 */
	UmlClassProp2EAttr createUmlClassProp2EAttr();

	/**
	 * Returns a new object of class '<em>Directed Self Ass2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Directed Self Ass2 ERef</em>'.
	 * @generated
	 */
	DirectedSelfAss2ERef createDirectedSelfAss2ERef();

	/**
	 * Returns a new object of class '<em>UML Primitive Data Type To ECore Primitive Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UML Primitive Data Type To ECore Primitive Data Type</em>'.
	 * @generated
	 */
	UMLPrimitiveDataTypeToECorePrimitiveDataType createUMLPrimitiveDataTypeToECorePrimitiveDataType();

	/**
	 * Returns a new object of class '<em>Uml Metamodel2 Ecore Meta</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Metamodel2 Ecore Meta</em>'.
	 * @generated
	 */
	UmlMetamodel2EcoreMeta createUmlMetamodel2EcoreMeta();

	/**
	 * Returns a new object of class '<em>UML Sec Meta Model2 Ecore Meta</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UML Sec Meta Model2 Ecore Meta</em>'.
	 * @generated
	 */
	UMLSecMetaModel2EcoreMeta createUMLSecMetaModel2EcoreMeta();

	/**
	 * Returns a new object of class '<em>Uml Op2 EOp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Op2 EOp</em>'.
	 * @generated
	 */
	UmlOp2EOp createUmlOp2EOp();

	/**
	 * Returns a new object of class '<em>UML Op Param2 EParam</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UML Op Param2 EParam</em>'.
	 * @generated
	 */
	UMLOpParam2EParam createUMLOpParam2EParam();

	/**
	 * Returns a new object of class '<em>Uml Set Op2 EStruct Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Set Op2 EStruct Feat</em>'.
	 * @generated
	 */
	UmlSetOp2EStructFeat createUmlSetOp2EStructFeat();

	/**
	 * Returns a new object of class '<em>Uml Enum2 EEnum</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Enum2 EEnum</em>'.
	 * @generated
	 */
	UmlEnum2EEnum createUmlEnum2EEnum();

	/**
	 * Returns a new object of class '<em>Uml Enum Literal2 EEnum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uml Enum Literal2 EEnum Literal</em>'.
	 * @generated
	 */
	UmlEnumLiteral2EEnumLiteral createUmlEnumLiteral2EEnumLiteral();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Uml2ecorePackage getUml2ecorePackage();

} //Uml2ecoreFactory
