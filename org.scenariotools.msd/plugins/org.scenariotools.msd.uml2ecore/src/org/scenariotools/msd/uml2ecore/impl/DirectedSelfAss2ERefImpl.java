/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Directed Self Ass2 ERef</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl#getFirstEnd <em>First End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl#getFirstERef <em>First ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl#getSecondEnd <em>Second End</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DirectedSelfAss2ERefImpl extends AbstractContainerCorrespondenceNodeImpl implements DirectedSelfAss2ERef {
	/**
	 * The cached value of the '{@link #getUmla() <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmla()
	 * @generated
	 * @ordered
	 */
	protected Association umla;

	/**
	 * The cached value of the '{@link #getFirstEnd() <em>First End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstEnd()
	 * @generated
	 * @ordered
	 */
	protected Property firstEnd;

	/**
	 * The cached value of the '{@link #getFirstERef() <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstERef()
	 * @generated
	 * @ordered
	 */
	protected EReference firstERef;

	/**
	 * The cached value of the '{@link #getSecondEnd() <em>Second End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondEnd()
	 * @generated
	 * @ordered
	 */
	protected Property secondEnd;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DirectedSelfAss2ERefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.DIRECTED_SELF_ASS2_EREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getUmla() {
		if (umla != null && umla.eIsProxy()) {
			InternalEObject oldUmla = (InternalEObject)umla;
			umla = (Association)eResolveProxy(oldUmla);
			if (umla != oldUmla) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA, oldUmla, umla));
			}
		}
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetUmla() {
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmla(Association newUmla) {
		Association oldUmla = umla;
		umla = newUmla;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA, oldUmla, umla));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getFirstEnd() {
		if (firstEnd != null && firstEnd.eIsProxy()) {
			InternalEObject oldFirstEnd = (InternalEObject)firstEnd;
			firstEnd = (Property)eResolveProxy(oldFirstEnd);
			if (firstEnd != oldFirstEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END, oldFirstEnd, firstEnd));
			}
		}
		return firstEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetFirstEnd() {
		return firstEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstEnd(Property newFirstEnd) {
		Property oldFirstEnd = firstEnd;
		firstEnd = newFirstEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END, oldFirstEnd, firstEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstERef() {
		if (firstERef != null && firstERef.eIsProxy()) {
			InternalEObject oldFirstERef = (InternalEObject)firstERef;
			firstERef = (EReference)eResolveProxy(oldFirstERef);
			if (firstERef != oldFirstERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF, oldFirstERef, firstERef));
			}
		}
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetFirstERef() {
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstERef(EReference newFirstERef) {
		EReference oldFirstERef = firstERef;
		firstERef = newFirstERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF, oldFirstERef, firstERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getSecondEnd() {
		if (secondEnd != null && secondEnd.eIsProxy()) {
			InternalEObject oldSecondEnd = (InternalEObject)secondEnd;
			secondEnd = (Property)eResolveProxy(oldSecondEnd);
			if (secondEnd != oldSecondEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END, oldSecondEnd, secondEnd));
			}
		}
		return secondEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetSecondEnd() {
		return secondEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecondEnd(Property newSecondEnd) {
		Property oldSecondEnd = secondEnd;
		secondEnd = newSecondEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END, oldSecondEnd, secondEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA:
				if (resolve) return getUmla();
				return basicGetUmla();
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END:
				if (resolve) return getFirstEnd();
				return basicGetFirstEnd();
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF:
				if (resolve) return getFirstERef();
				return basicGetFirstERef();
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END:
				if (resolve) return getSecondEnd();
				return basicGetSecondEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA:
				setUmla((Association)newValue);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END:
				setFirstEnd((Property)newValue);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF:
				setFirstERef((EReference)newValue);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END:
				setSecondEnd((Property)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA:
				setUmla((Association)null);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END:
				setFirstEnd((Property)null);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF:
				setFirstERef((EReference)null);
				return;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END:
				setSecondEnd((Property)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__UMLA:
				return umla != null;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_END:
				return firstEnd != null;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__FIRST_EREF:
				return firstERef != null;
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF__SECOND_END:
				return secondEnd != null;
		}
		return super.eIsSet(featureID);
	}

} //DirectedSelfAss2ERefImpl
