/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Parameter;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UML Op Param2 EParam</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl#getUmlOpParam <em>Uml Op Param</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl#getEParameter <em>EParameter</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UMLOpParam2EParamImpl extends AbstractContainerCorrespondenceNodeImpl implements UMLOpParam2EParam {
	/**
	 * The cached value of the '{@link #getUmlOpParam() <em>Uml Op Param</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlOpParam()
	 * @generated
	 * @ordered
	 */
	protected Parameter umlOpParam;

	/**
	 * The cached value of the '{@link #getEParameter() <em>EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEParameter()
	 * @generated
	 * @ordered
	 */
	protected EParameter eParameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UMLOpParam2EParamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_OP_PARAM2_EPARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter getUmlOpParam() {
		if (umlOpParam != null && umlOpParam.eIsProxy()) {
			InternalEObject oldUmlOpParam = (InternalEObject)umlOpParam;
			umlOpParam = (Parameter)eResolveProxy(oldUmlOpParam);
			if (umlOpParam != oldUmlOpParam) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM, oldUmlOpParam, umlOpParam));
			}
		}
		return umlOpParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter basicGetUmlOpParam() {
		return umlOpParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlOpParam(Parameter newUmlOpParam) {
		Parameter oldUmlOpParam = umlOpParam;
		umlOpParam = newUmlOpParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM, oldUmlOpParam, umlOpParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter getEParameter() {
		if (eParameter != null && eParameter.eIsProxy()) {
			InternalEObject oldEParameter = (InternalEObject)eParameter;
			eParameter = (EParameter)eResolveProxy(oldEParameter);
			if (eParameter != oldEParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER, oldEParameter, eParameter));
			}
		}
		return eParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter basicGetEParameter() {
		return eParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEParameter(EParameter newEParameter) {
		EParameter oldEParameter = eParameter;
		eParameter = newEParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER, oldEParameter, eParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM:
				if (resolve) return getUmlOpParam();
				return basicGetUmlOpParam();
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER:
				if (resolve) return getEParameter();
				return basicGetEParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM:
				setUmlOpParam((Parameter)newValue);
				return;
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER:
				setEParameter((EParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM:
				setUmlOpParam((Parameter)null);
				return;
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER:
				setEParameter((EParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__UML_OP_PARAM:
				return umlOpParam != null;
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM__EPARAMETER:
				return eParameter != null;
		}
		return super.eIsSet(featureID);
	}

} //UMLOpParam2EParamImpl
