/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.uml2ecore.Uml2ecoreFactory
 * @model kind="package"
 * @generated
 */
public interface Uml2ecorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "uml2ecore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.uml2ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.scenariotools.msd.uml2ecore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Uml2ecorePackage eINSTANCE = org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.AbstractContainerCorrespondenceNodeImpl <em>Abstract Container Correspondence Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.AbstractContainerCorrespondenceNodeImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getAbstractContainerCorrespondenceNode()
	 * @generated
	 */
	int ABSTRACT_CONTAINER_CORRESPONDENCE_NODE = 1;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP = 0;

	/**
	 * The number of structural features of the '<em>Abstract Container Correspondence Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl <em>Uml Package2 EPackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlPackage2EPackage()
	 * @generated
	 */
	int UML_PACKAGE2_EPACKAGE = 0;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PACKAGE2_EPACKAGE__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Uml Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PACKAGE2_EPACKAGE__UML_PACKAGE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ecore Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Primitive Mapping Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Uml Package2 EPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PACKAGE2_EPACKAGE_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl <em>Uml Class2 EClass</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlClass2EClass()
	 * @generated
	 */
	int UML_CLASS2_ECLASS = 2;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS2_ECLASS__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umlc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS2_ECLASS__UMLC = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ec</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS2_ECLASS__EC = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Class2 EClass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS2_ECLASS_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl <em>first Prop2 ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getfirstProp2ERef()
	 * @generated
	 */
	int FIRST_PROP2_EREF = 3;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_EREF__FIRST_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Second ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_EREF__SECOND_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>first Prop2 ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl <em>Uml Assoz2 ERefe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlAssoz2ERefe()
	 * @generated
	 */
	int UML_ASSOZ2_EREFE = 4;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ASSOZ2_EREFE__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ASSOZ2_EREFE__UMLA = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ass2 ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ASSOZ2_EREFE__ASS2_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ass2 Ref2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ASSOZ2_EREFE__ASS2_REF2 = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Uml Assoz2 ERefe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ASSOZ2_EREFE_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl <em>sec Prop2 ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getsecProp2ERef()
	 * @generated
	 */
	int SEC_PROP2_EREF = 5;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_EREF__SECOND_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_EREF__FIRST_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>sec Prop2 ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlGen2EInhImpl <em>Uml Gen2 EInh</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlGen2EInhImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlGen2EInh()
	 * @generated
	 */
	int UML_GEN2_EINH = 6;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_GEN2_EINH__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_GEN2_EINH__GENERALIZATION = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_GEN2_EINH__ECLASS = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Gen2 EInh</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_GEN2_EINH_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.FirstProp2SelfERefImpl <em>First Prop2 Self ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.FirstProp2SelfERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getFirstProp2SelfERef()
	 * @generated
	 */
	int FIRST_PROP2_SELF_EREF = 7;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_SELF_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_SELF_EREF__FIRST_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Second ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_SELF_EREF__SECOND_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>First Prop2 Self ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIRST_PROP2_SELF_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.SecProp2SelfERefImpl <em>Sec Prop2 Self ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.SecProp2SelfERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getSecProp2SelfERef()
	 * @generated
	 */
	int SEC_PROP2_SELF_EREF = 8;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_SELF_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_SELF_EREF__SECOND_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_SELF_EREF__FIRST_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sec Prop2 Self ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEC_PROP2_SELF_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl <em>Self Ass2 ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getSelfAss2ERef()
	 * @generated
	 */
	int SELF_ASS2_EREF = 9;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_ASS2_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_ASS2_EREF__UMLA = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_ASS2_EREF__FIRST_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Second ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_ASS2_EREF__SECOND_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Self Ass2 ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_ASS2_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.DirectedAssoz2ERefImpl <em>Directed Assoz2 ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.DirectedAssoz2ERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getDirectedAssoz2ERef()
	 * @generated
	 */
	int DIRECTED_ASSOZ2_EREF = 10;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF__FIRST_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF__UMLA = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF__SECOND_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF__FIRST_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Directed Assoz2 ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_ASSOZ2_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl <em>Uml Class Prop2 EAttr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlClassProp2EAttr()
	 * @generated
	 */
	int UML_CLASS_PROP2_EATTR = 11;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_PROP2_EATTR__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umlprop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_PROP2_EATTR__UMLPROP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EAttribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_PROP2_EATTR__EATTRIBUTE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>EClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_PROP2_EATTR__ECLASSIFIER = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Uml Class Prop2 EAttr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_CLASS_PROP2_EATTR_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl <em>Directed Self Ass2 ERef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getDirectedSelfAss2ERef()
	 * @generated
	 */
	int DIRECTED_SELF_ASS2_EREF = 12;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF__UMLA = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF__FIRST_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF__FIRST_EREF = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF__SECOND_END = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Directed Self Ass2 ERef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTED_SELF_ASS2_EREF_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl <em>UML Primitive Data Type To ECore Primitive Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLPrimitiveDataTypeToECorePrimitiveDataType()
	 * @generated
	 */
	int UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>UML Primitive Data Type To ECore Primitive Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl <em>Uml Metamodel2 Ecore Meta</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlMetamodel2EcoreMeta()
	 * @generated
	 */
	int UML_METAMODEL2_ECORE_META = 14;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_METAMODEL2_ECORE_META__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Uml Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_METAMODEL2_ECORE_META__UML_METAMODEL = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>MEcore Pack</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_METAMODEL2_ECORE_META__MECORE_PACK = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Metamodel2 Ecore Meta</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_METAMODEL2_ECORE_META_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl <em>UML Sec Meta Model2 Ecore Meta</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLSecMetaModel2EcoreMeta()
	 * @generated
	 */
	int UML_SEC_META_MODEL2_ECORE_META = 15;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SEC_META_MODEL2_ECORE_META__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Sec Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SEC_META_MODEL2_ECORE_META__EPACKAGE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>UML Sec Meta Model2 Ecore Meta</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SEC_META_MODEL2_ECORE_META_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlOp2EOpImpl <em>Uml Op2 EOp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlOp2EOpImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlOp2EOp()
	 * @generated
	 */
	int UML_OP2_EOP = 16;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP2_EOP__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umlop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP2_EOP__UMLOP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EOperation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP2_EOP__EOPERATION = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Op2 EOp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP2_EOP_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl <em>UML Op Param2 EParam</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLOpParam2EParam()
	 * @generated
	 */
	int UML_OP_PARAM2_EPARAM = 17;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP_PARAM2_EPARAM__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Uml Op Param</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP_PARAM2_EPARAM__UML_OP_PARAM = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP_PARAM2_EPARAM__EPARAMETER = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>UML Op Param2 EParam</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_OP_PARAM2_EPARAM_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlSetOp2EStructFeatImpl <em>Uml Set Op2 EStruct Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlSetOp2EStructFeatImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlSetOp2EStructFeat()
	 * @generated
	 */
	int UML_SET_OP2_ESTRUCT_FEAT = 18;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SET_OP2_ESTRUCT_FEAT__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umlop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SET_OP2_ESTRUCT_FEAT__UMLOP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SET_OP2_ESTRUCT_FEAT__ESTRUCTURAL_FEATURE = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Set Op2 EStruct Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_SET_OP2_ESTRUCT_FEAT_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl <em>Uml Enum2 EEnum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlEnum2EEnum()
	 * @generated
	 */
	int UML_ENUM2_EENUM = 19;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM2_EENUM__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>Umlenum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM2_EENUM__UMLENUM = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Eenum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM2_EENUM__EENUM = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Enum2 EEnum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM2_EENUM_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl <em>Uml Enum Literal2 EEnum Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl
	 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlEnumLiteral2EEnumLiteral()
	 * @generated
	 */
	int UML_ENUM_LITERAL2_EENUM_LITERAL = 20;

	/**
	 * The feature id for the '<em><b>Child Corresp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM_LITERAL2_EENUM_LITERAL__CHILD_CORRESP = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP;

	/**
	 * The feature id for the '<em><b>UE Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EE Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Uml Enum Literal2 EEnum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UML_ENUM_LITERAL2_EENUM_LITERAL_FEATURE_COUNT = ABSTRACT_CONTAINER_CORRESPONDENCE_NODE_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage <em>Uml Package2 EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Package2 EPackage</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlPackage2EPackage
	 * @generated
	 */
	EClass getUmlPackage2EPackage();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getUmlPackage <em>Uml Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml Package</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getUmlPackage()
	 * @see #getUmlPackage2EPackage()
	 * @generated
	 */
	EReference getUmlPackage2EPackage_UmlPackage();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getEcorePackage <em>Ecore Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ecore Package</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getEcorePackage()
	 * @see #getUmlPackage2EPackage()
	 * @generated
	 */
	EReference getUmlPackage2EPackage_EcorePackage();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getPrimitiveMappingContainer <em>Primitive Mapping Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Primitive Mapping Container</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getPrimitiveMappingContainer()
	 * @see #getUmlPackage2EPackage()
	 * @generated
	 */
	EReference getUmlPackage2EPackage_PrimitiveMappingContainer();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode <em>Abstract Container Correspondence Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Container Correspondence Node</em>'.
	 * @see org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode
	 * @generated
	 */
	EClass getAbstractContainerCorrespondenceNode();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode#getChildCorresp <em>Child Corresp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child Corresp</em>'.
	 * @see org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode#getChildCorresp()
	 * @see #getAbstractContainerCorrespondenceNode()
	 * @generated
	 */
	EReference getAbstractContainerCorrespondenceNode_ChildCorresp();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass <em>Uml Class2 EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Class2 EClass</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClass2EClass
	 * @generated
	 */
	EClass getUmlClass2EClass();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getUmlc <em>Umlc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umlc</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClass2EClass#getUmlc()
	 * @see #getUmlClass2EClass()
	 * @generated
	 */
	EReference getUmlClass2EClass_Umlc();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getEc <em>Ec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ec</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClass2EClass#getEc()
	 * @see #getUmlClass2EClass()
	 * @generated
	 */
	EReference getUmlClass2EClass_Ec();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.firstProp2ERef <em>first Prop2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>first Prop2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.firstProp2ERef
	 * @generated
	 */
	EClass getfirstProp2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.firstProp2ERef#getFirstEnd <em>First End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.firstProp2ERef#getFirstEnd()
	 * @see #getfirstProp2ERef()
	 * @generated
	 */
	EReference getfirstProp2ERef_FirstEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.firstProp2ERef#getSecondERef <em>Second ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.firstProp2ERef#getSecondERef()
	 * @see #getfirstProp2ERef()
	 * @generated
	 */
	EReference getfirstProp2ERef_SecondERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe <em>Uml Assoz2 ERefe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Assoz2 ERefe</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe
	 * @generated
	 */
	EClass getUmlAssoz2ERefe();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getUmla <em>Umla</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umla</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getUmla()
	 * @see #getUmlAssoz2ERefe()
	 * @generated
	 */
	EReference getUmlAssoz2ERefe_Umla();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2ERef <em>Ass2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ass2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2ERef()
	 * @see #getUmlAssoz2ERefe()
	 * @generated
	 */
	EReference getUmlAssoz2ERefe_Ass2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2Ref2 <em>Ass2 Ref2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ass2 Ref2</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe#getAss2Ref2()
	 * @see #getUmlAssoz2ERefe()
	 * @generated
	 */
	EReference getUmlAssoz2ERefe_Ass2Ref2();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.secProp2ERef <em>sec Prop2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>sec Prop2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.secProp2ERef
	 * @generated
	 */
	EClass getsecProp2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getSecondEnd <em>Second End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.secProp2ERef#getSecondEnd()
	 * @see #getsecProp2ERef()
	 * @generated
	 */
	EReference getsecProp2ERef_SecondEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getFirstERef <em>First ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.secProp2ERef#getFirstERef()
	 * @see #getsecProp2ERef()
	 * @generated
	 */
	EReference getsecProp2ERef_FirstERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh <em>Uml Gen2 EInh</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Gen2 EInh</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlGen2EInh
	 * @generated
	 */
	EClass getUmlGen2EInh();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getGeneralization <em>Generalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Generalization</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlGen2EInh#getGeneralization()
	 * @see #getUmlGen2EInh()
	 * @generated
	 */
	EReference getUmlGen2EInh_Generalization();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getEClass <em>EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EClass</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlGen2EInh#getEClass()
	 * @see #getUmlGen2EInh()
	 * @generated
	 */
	EReference getUmlGen2EInh_EClass();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef <em>First Prop2 Self ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>First Prop2 Self ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.FirstProp2SelfERef
	 * @generated
	 */
	EClass getFirstProp2SelfERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getFirstEnd <em>First End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getFirstEnd()
	 * @see #getFirstProp2SelfERef()
	 * @generated
	 */
	EReference getFirstProp2SelfERef_FirstEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getSecondERef <em>Second ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getSecondERef()
	 * @see #getFirstProp2SelfERef()
	 * @generated
	 */
	EReference getFirstProp2SelfERef_SecondERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef <em>Sec Prop2 Self ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sec Prop2 Self ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SecProp2SelfERef
	 * @generated
	 */
	EClass getSecProp2SelfERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getSecondEnd <em>Second End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getSecondEnd()
	 * @see #getSecProp2SelfERef()
	 * @generated
	 */
	EReference getSecProp2SelfERef_SecondEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getFirstERef <em>First ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getFirstERef()
	 * @see #getSecProp2SelfERef()
	 * @generated
	 */
	EReference getSecProp2SelfERef_FirstERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef <em>Self Ass2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Self Ass2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SelfAss2ERef
	 * @generated
	 */
	EClass getSelfAss2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getUmla <em>Umla</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umla</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SelfAss2ERef#getUmla()
	 * @see #getSelfAss2ERef()
	 * @generated
	 */
	EReference getSelfAss2ERef_Umla();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getFirstERef <em>First ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SelfAss2ERef#getFirstERef()
	 * @see #getSelfAss2ERef()
	 * @generated
	 */
	EReference getSelfAss2ERef_FirstERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getSecondERef <em>Second ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.SelfAss2ERef#getSecondERef()
	 * @see #getSelfAss2ERef()
	 * @generated
	 */
	EReference getSelfAss2ERef_SecondERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef <em>Directed Assoz2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Directed Assoz2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef
	 * @generated
	 */
	EClass getDirectedAssoz2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getFirstEnd <em>First End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getFirstEnd()
	 * @see #getDirectedAssoz2ERef()
	 * @generated
	 */
	EReference getDirectedAssoz2ERef_FirstEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getUmla <em>Umla</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umla</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getUmla()
	 * @see #getDirectedAssoz2ERef()
	 * @generated
	 */
	EReference getDirectedAssoz2ERef_Umla();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getSecondEnd <em>Second End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getSecondEnd()
	 * @see #getDirectedAssoz2ERef()
	 * @generated
	 */
	EReference getDirectedAssoz2ERef_SecondEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getFirstERef <em>First ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef#getFirstERef()
	 * @see #getDirectedAssoz2ERef()
	 * @generated
	 */
	EReference getDirectedAssoz2ERef_FirstERef();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr <em>Uml Class Prop2 EAttr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Class Prop2 EAttr</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr
	 * @generated
	 */
	EClass getUmlClassProp2EAttr();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getUmlprop <em>Umlprop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umlprop</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getUmlprop()
	 * @see #getUmlClassProp2EAttr()
	 * @generated
	 */
	EReference getUmlClassProp2EAttr_Umlprop();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEAttribute <em>EAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EAttribute</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEAttribute()
	 * @see #getUmlClassProp2EAttr()
	 * @generated
	 */
	EReference getUmlClassProp2EAttr_EAttribute();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEClassifier <em>EClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EClassifier</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr#getEClassifier()
	 * @see #getUmlClassProp2EAttr()
	 * @generated
	 */
	EReference getUmlClassProp2EAttr_EClassifier();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef <em>Directed Self Ass2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Directed Self Ass2 ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef
	 * @generated
	 */
	EClass getDirectedSelfAss2ERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getUmla <em>Umla</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umla</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getUmla()
	 * @see #getDirectedSelfAss2ERef()
	 * @generated
	 */
	EReference getDirectedSelfAss2ERef_Umla();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstEnd <em>First End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstEnd()
	 * @see #getDirectedSelfAss2ERef()
	 * @generated
	 */
	EReference getDirectedSelfAss2ERef_FirstEnd();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstERef <em>First ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First ERef</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstERef()
	 * @see #getDirectedSelfAss2ERef()
	 * @generated
	 */
	EReference getDirectedSelfAss2ERef_FirstERef();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getSecondEnd <em>Second End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second End</em>'.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getSecondEnd()
	 * @see #getDirectedSelfAss2ERef()
	 * @generated
	 */
	EReference getDirectedSelfAss2ERef_SecondEnd();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType <em>UML Primitive Data Type To ECore Primitive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Primitive Data Type To ECore Primitive Data Type</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType
	 * @generated
	 */
	EClass getUMLPrimitiveDataTypeToECorePrimitiveDataType();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Type</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getDataType()
	 * @see #getUMLPrimitiveDataTypeToECorePrimitiveDataType()
	 * @generated
	 */
	EReference getUMLPrimitiveDataTypeToECorePrimitiveDataType_DataType();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getEDataType <em>EData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EData Type</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getEDataType()
	 * @see #getUMLPrimitiveDataTypeToECorePrimitiveDataType()
	 * @generated
	 */
	EReference getUMLPrimitiveDataTypeToECorePrimitiveDataType_EDataType();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta <em>Uml Metamodel2 Ecore Meta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Metamodel2 Ecore Meta</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta
	 * @generated
	 */
	EClass getUmlMetamodel2EcoreMeta();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getUmlMetamodel <em>Uml Metamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml Metamodel</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getUmlMetamodel()
	 * @see #getUmlMetamodel2EcoreMeta()
	 * @generated
	 */
	EReference getUmlMetamodel2EcoreMeta_UmlMetamodel();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getMEcorePack <em>MEcore Pack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>MEcore Pack</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getMEcorePack()
	 * @see #getUmlMetamodel2EcoreMeta()
	 * @generated
	 */
	EReference getUmlMetamodel2EcoreMeta_MEcorePack();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta <em>UML Sec Meta Model2 Ecore Meta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Sec Meta Model2 Ecore Meta</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta
	 * @generated
	 */
	EClass getUMLSecMetaModel2EcoreMeta();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getSecMetamodel <em>Sec Metamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sec Metamodel</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getSecMetamodel()
	 * @see #getUMLSecMetaModel2EcoreMeta()
	 * @generated
	 */
	EReference getUMLSecMetaModel2EcoreMeta_SecMetamodel();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getEPackage <em>EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EPackage</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getEPackage()
	 * @see #getUMLSecMetaModel2EcoreMeta()
	 * @generated
	 */
	EReference getUMLSecMetaModel2EcoreMeta_EPackage();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp <em>Uml Op2 EOp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Op2 EOp</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlOp2EOp
	 * @generated
	 */
	EClass getUmlOp2EOp();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getUmlop <em>Umlop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umlop</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlOp2EOp#getUmlop()
	 * @see #getUmlOp2EOp()
	 * @generated
	 */
	EReference getUmlOp2EOp_Umlop();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getEOperation <em>EOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EOperation</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlOp2EOp#getEOperation()
	 * @see #getUmlOp2EOp()
	 * @generated
	 */
	EReference getUmlOp2EOp_EOperation();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam <em>UML Op Param2 EParam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UML Op Param2 EParam</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLOpParam2EParam
	 * @generated
	 */
	EClass getUMLOpParam2EParam();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getUmlOpParam <em>Uml Op Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml Op Param</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getUmlOpParam()
	 * @see #getUMLOpParam2EParam()
	 * @generated
	 */
	EReference getUMLOpParam2EParam_UmlOpParam();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getEParameter <em>EParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EParameter</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getEParameter()
	 * @see #getUMLOpParam2EParam()
	 * @generated
	 */
	EReference getUMLOpParam2EParam_EParameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat <em>Uml Set Op2 EStruct Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Set Op2 EStruct Feat</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat
	 * @generated
	 */
	EClass getUmlSetOp2EStructFeat();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getUmlop <em>Umlop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umlop</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getUmlop()
	 * @see #getUmlSetOp2EStructFeat()
	 * @generated
	 */
	EReference getUmlSetOp2EStructFeat_Umlop();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getEStructuralFeature <em>EStructural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EStructural Feature</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getEStructuralFeature()
	 * @see #getUmlSetOp2EStructFeat()
	 * @generated
	 */
	EReference getUmlSetOp2EStructFeat_EStructuralFeature();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum <em>Uml Enum2 EEnum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Enum2 EEnum</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnum2EEnum
	 * @generated
	 */
	EClass getUmlEnum2EEnum();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getUmlenum <em>Umlenum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Umlenum</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getUmlenum()
	 * @see #getUmlEnum2EEnum()
	 * @generated
	 */
	EReference getUmlEnum2EEnum_Umlenum();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getEenum <em>Eenum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Eenum</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getEenum()
	 * @see #getUmlEnum2EEnum()
	 * @generated
	 */
	EReference getUmlEnum2EEnum_Eenum();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral <em>Uml Enum Literal2 EEnum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uml Enum Literal2 EEnum Literal</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral
	 * @generated
	 */
	EClass getUmlEnumLiteral2EEnumLiteral();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getUELiteral <em>UE Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>UE Literal</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getUELiteral()
	 * @see #getUmlEnumLiteral2EEnumLiteral()
	 * @generated
	 */
	EReference getUmlEnumLiteral2EEnumLiteral_UELiteral();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getEELiteral <em>EE Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EE Literal</em>'.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getEELiteral()
	 * @see #getUmlEnumLiteral2EEnumLiteral()
	 * @generated
	 */
	EReference getUmlEnumLiteral2EEnumLiteral_EELiteral();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Uml2ecoreFactory getUml2ecoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl <em>Uml Package2 EPackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlPackage2EPackage()
		 * @generated
		 */
		EClass UML_PACKAGE2_EPACKAGE = eINSTANCE.getUmlPackage2EPackage();

		/**
		 * The meta object literal for the '<em><b>Uml Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PACKAGE2_EPACKAGE__UML_PACKAGE = eINSTANCE.getUmlPackage2EPackage_UmlPackage();

		/**
		 * The meta object literal for the '<em><b>Ecore Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE = eINSTANCE.getUmlPackage2EPackage_EcorePackage();

		/**
		 * The meta object literal for the '<em><b>Primitive Mapping Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER = eINSTANCE.getUmlPackage2EPackage_PrimitiveMappingContainer();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.AbstractContainerCorrespondenceNodeImpl <em>Abstract Container Correspondence Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.AbstractContainerCorrespondenceNodeImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getAbstractContainerCorrespondenceNode()
		 * @generated
		 */
		EClass ABSTRACT_CONTAINER_CORRESPONDENCE_NODE = eINSTANCE.getAbstractContainerCorrespondenceNode();

		/**
		 * The meta object literal for the '<em><b>Child Corresp</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP = eINSTANCE.getAbstractContainerCorrespondenceNode_ChildCorresp();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl <em>Uml Class2 EClass</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlClass2EClass()
		 * @generated
		 */
		EClass UML_CLASS2_ECLASS = eINSTANCE.getUmlClass2EClass();

		/**
		 * The meta object literal for the '<em><b>Umlc</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS2_ECLASS__UMLC = eINSTANCE.getUmlClass2EClass_Umlc();

		/**
		 * The meta object literal for the '<em><b>Ec</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS2_ECLASS__EC = eINSTANCE.getUmlClass2EClass_Ec();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl <em>first Prop2 ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getfirstProp2ERef()
		 * @generated
		 */
		EClass FIRST_PROP2_EREF = eINSTANCE.getfirstProp2ERef();

		/**
		 * The meta object literal for the '<em><b>First End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_PROP2_EREF__FIRST_END = eINSTANCE.getfirstProp2ERef_FirstEnd();

		/**
		 * The meta object literal for the '<em><b>Second ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_PROP2_EREF__SECOND_EREF = eINSTANCE.getfirstProp2ERef_SecondERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl <em>Uml Assoz2 ERefe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlAssoz2ERefe()
		 * @generated
		 */
		EClass UML_ASSOZ2_EREFE = eINSTANCE.getUmlAssoz2ERefe();

		/**
		 * The meta object literal for the '<em><b>Umla</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ASSOZ2_EREFE__UMLA = eINSTANCE.getUmlAssoz2ERefe_Umla();

		/**
		 * The meta object literal for the '<em><b>Ass2 ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ASSOZ2_EREFE__ASS2_EREF = eINSTANCE.getUmlAssoz2ERefe_Ass2ERef();

		/**
		 * The meta object literal for the '<em><b>Ass2 Ref2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ASSOZ2_EREFE__ASS2_REF2 = eINSTANCE.getUmlAssoz2ERefe_Ass2Ref2();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl <em>sec Prop2 ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getsecProp2ERef()
		 * @generated
		 */
		EClass SEC_PROP2_EREF = eINSTANCE.getsecProp2ERef();

		/**
		 * The meta object literal for the '<em><b>Second End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEC_PROP2_EREF__SECOND_END = eINSTANCE.getsecProp2ERef_SecondEnd();

		/**
		 * The meta object literal for the '<em><b>First ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEC_PROP2_EREF__FIRST_EREF = eINSTANCE.getsecProp2ERef_FirstERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlGen2EInhImpl <em>Uml Gen2 EInh</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlGen2EInhImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlGen2EInh()
		 * @generated
		 */
		EClass UML_GEN2_EINH = eINSTANCE.getUmlGen2EInh();

		/**
		 * The meta object literal for the '<em><b>Generalization</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_GEN2_EINH__GENERALIZATION = eINSTANCE.getUmlGen2EInh_Generalization();

		/**
		 * The meta object literal for the '<em><b>EClass</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_GEN2_EINH__ECLASS = eINSTANCE.getUmlGen2EInh_EClass();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.FirstProp2SelfERefImpl <em>First Prop2 Self ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.FirstProp2SelfERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getFirstProp2SelfERef()
		 * @generated
		 */
		EClass FIRST_PROP2_SELF_EREF = eINSTANCE.getFirstProp2SelfERef();

		/**
		 * The meta object literal for the '<em><b>First End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_PROP2_SELF_EREF__FIRST_END = eINSTANCE.getFirstProp2SelfERef_FirstEnd();

		/**
		 * The meta object literal for the '<em><b>Second ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIRST_PROP2_SELF_EREF__SECOND_EREF = eINSTANCE.getFirstProp2SelfERef_SecondERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.SecProp2SelfERefImpl <em>Sec Prop2 Self ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.SecProp2SelfERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getSecProp2SelfERef()
		 * @generated
		 */
		EClass SEC_PROP2_SELF_EREF = eINSTANCE.getSecProp2SelfERef();

		/**
		 * The meta object literal for the '<em><b>Second End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEC_PROP2_SELF_EREF__SECOND_END = eINSTANCE.getSecProp2SelfERef_SecondEnd();

		/**
		 * The meta object literal for the '<em><b>First ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEC_PROP2_SELF_EREF__FIRST_EREF = eINSTANCE.getSecProp2SelfERef_FirstERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl <em>Self Ass2 ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getSelfAss2ERef()
		 * @generated
		 */
		EClass SELF_ASS2_EREF = eINSTANCE.getSelfAss2ERef();

		/**
		 * The meta object literal for the '<em><b>Umla</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELF_ASS2_EREF__UMLA = eINSTANCE.getSelfAss2ERef_Umla();

		/**
		 * The meta object literal for the '<em><b>First ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELF_ASS2_EREF__FIRST_EREF = eINSTANCE.getSelfAss2ERef_FirstERef();

		/**
		 * The meta object literal for the '<em><b>Second ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELF_ASS2_EREF__SECOND_EREF = eINSTANCE.getSelfAss2ERef_SecondERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.DirectedAssoz2ERefImpl <em>Directed Assoz2 ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.DirectedAssoz2ERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getDirectedAssoz2ERef()
		 * @generated
		 */
		EClass DIRECTED_ASSOZ2_EREF = eINSTANCE.getDirectedAssoz2ERef();

		/**
		 * The meta object literal for the '<em><b>First End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_ASSOZ2_EREF__FIRST_END = eINSTANCE.getDirectedAssoz2ERef_FirstEnd();

		/**
		 * The meta object literal for the '<em><b>Umla</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_ASSOZ2_EREF__UMLA = eINSTANCE.getDirectedAssoz2ERef_Umla();

		/**
		 * The meta object literal for the '<em><b>Second End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_ASSOZ2_EREF__SECOND_END = eINSTANCE.getDirectedAssoz2ERef_SecondEnd();

		/**
		 * The meta object literal for the '<em><b>First ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_ASSOZ2_EREF__FIRST_EREF = eINSTANCE.getDirectedAssoz2ERef_FirstERef();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl <em>Uml Class Prop2 EAttr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlClassProp2EAttrImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlClassProp2EAttr()
		 * @generated
		 */
		EClass UML_CLASS_PROP2_EATTR = eINSTANCE.getUmlClassProp2EAttr();

		/**
		 * The meta object literal for the '<em><b>Umlprop</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS_PROP2_EATTR__UMLPROP = eINSTANCE.getUmlClassProp2EAttr_Umlprop();

		/**
		 * The meta object literal for the '<em><b>EAttribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS_PROP2_EATTR__EATTRIBUTE = eINSTANCE.getUmlClassProp2EAttr_EAttribute();

		/**
		 * The meta object literal for the '<em><b>EClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_CLASS_PROP2_EATTR__ECLASSIFIER = eINSTANCE.getUmlClassProp2EAttr_EClassifier();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl <em>Directed Self Ass2 ERef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.DirectedSelfAss2ERefImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getDirectedSelfAss2ERef()
		 * @generated
		 */
		EClass DIRECTED_SELF_ASS2_EREF = eINSTANCE.getDirectedSelfAss2ERef();

		/**
		 * The meta object literal for the '<em><b>Umla</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_SELF_ASS2_EREF__UMLA = eINSTANCE.getDirectedSelfAss2ERef_Umla();

		/**
		 * The meta object literal for the '<em><b>First End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_SELF_ASS2_EREF__FIRST_END = eINSTANCE.getDirectedSelfAss2ERef_FirstEnd();

		/**
		 * The meta object literal for the '<em><b>First ERef</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_SELF_ASS2_EREF__FIRST_EREF = eINSTANCE.getDirectedSelfAss2ERef_FirstERef();

		/**
		 * The meta object literal for the '<em><b>Second End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTED_SELF_ASS2_EREF__SECOND_END = eINSTANCE.getDirectedSelfAss2ERef_SecondEnd();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl <em>UML Primitive Data Type To ECore Primitive Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLPrimitiveDataTypeToECorePrimitiveDataType()
		 * @generated
		 */
		EClass UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE = eINSTANCE.getUMLPrimitiveDataTypeToECorePrimitiveDataType();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE = eINSTANCE.getUMLPrimitiveDataTypeToECorePrimitiveDataType_DataType();

		/**
		 * The meta object literal for the '<em><b>EData Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE = eINSTANCE.getUMLPrimitiveDataTypeToECorePrimitiveDataType_EDataType();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl <em>Uml Metamodel2 Ecore Meta</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlMetamodel2EcoreMeta()
		 * @generated
		 */
		EClass UML_METAMODEL2_ECORE_META = eINSTANCE.getUmlMetamodel2EcoreMeta();

		/**
		 * The meta object literal for the '<em><b>Uml Metamodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_METAMODEL2_ECORE_META__UML_METAMODEL = eINSTANCE.getUmlMetamodel2EcoreMeta_UmlMetamodel();

		/**
		 * The meta object literal for the '<em><b>MEcore Pack</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_METAMODEL2_ECORE_META__MECORE_PACK = eINSTANCE.getUmlMetamodel2EcoreMeta_MEcorePack();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl <em>UML Sec Meta Model2 Ecore Meta</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLSecMetaModel2EcoreMeta()
		 * @generated
		 */
		EClass UML_SEC_META_MODEL2_ECORE_META = eINSTANCE.getUMLSecMetaModel2EcoreMeta();

		/**
		 * The meta object literal for the '<em><b>Sec Metamodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL = eINSTANCE.getUMLSecMetaModel2EcoreMeta_SecMetamodel();

		/**
		 * The meta object literal for the '<em><b>EPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_SEC_META_MODEL2_ECORE_META__EPACKAGE = eINSTANCE.getUMLSecMetaModel2EcoreMeta_EPackage();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlOp2EOpImpl <em>Uml Op2 EOp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlOp2EOpImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlOp2EOp()
		 * @generated
		 */
		EClass UML_OP2_EOP = eINSTANCE.getUmlOp2EOp();

		/**
		 * The meta object literal for the '<em><b>Umlop</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OP2_EOP__UMLOP = eINSTANCE.getUmlOp2EOp_Umlop();

		/**
		 * The meta object literal for the '<em><b>EOperation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OP2_EOP__EOPERATION = eINSTANCE.getUmlOp2EOp_EOperation();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl <em>UML Op Param2 EParam</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UMLOpParam2EParamImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUMLOpParam2EParam()
		 * @generated
		 */
		EClass UML_OP_PARAM2_EPARAM = eINSTANCE.getUMLOpParam2EParam();

		/**
		 * The meta object literal for the '<em><b>Uml Op Param</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OP_PARAM2_EPARAM__UML_OP_PARAM = eINSTANCE.getUMLOpParam2EParam_UmlOpParam();

		/**
		 * The meta object literal for the '<em><b>EParameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_OP_PARAM2_EPARAM__EPARAMETER = eINSTANCE.getUMLOpParam2EParam_EParameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlSetOp2EStructFeatImpl <em>Uml Set Op2 EStruct Feat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlSetOp2EStructFeatImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlSetOp2EStructFeat()
		 * @generated
		 */
		EClass UML_SET_OP2_ESTRUCT_FEAT = eINSTANCE.getUmlSetOp2EStructFeat();

		/**
		 * The meta object literal for the '<em><b>Umlop</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_SET_OP2_ESTRUCT_FEAT__UMLOP = eINSTANCE.getUmlSetOp2EStructFeat_Umlop();

		/**
		 * The meta object literal for the '<em><b>EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_SET_OP2_ESTRUCT_FEAT__ESTRUCTURAL_FEATURE = eINSTANCE.getUmlSetOp2EStructFeat_EStructuralFeature();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl <em>Uml Enum2 EEnum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlEnum2EEnum()
		 * @generated
		 */
		EClass UML_ENUM2_EENUM = eINSTANCE.getUmlEnum2EEnum();

		/**
		 * The meta object literal for the '<em><b>Umlenum</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ENUM2_EENUM__UMLENUM = eINSTANCE.getUmlEnum2EEnum_Umlenum();

		/**
		 * The meta object literal for the '<em><b>Eenum</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ENUM2_EENUM__EENUM = eINSTANCE.getUmlEnum2EEnum_Eenum();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl <em>Uml Enum Literal2 EEnum Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl
		 * @see org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl#getUmlEnumLiteral2EEnumLiteral()
		 * @generated
		 */
		EClass UML_ENUM_LITERAL2_EENUM_LITERAL = eINSTANCE.getUmlEnumLiteral2EEnumLiteral();

		/**
		 * The meta object literal for the '<em><b>UE Literal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL = eINSTANCE.getUmlEnumLiteral2EEnumLiteral_UELiteral();

		/**
		 * The meta object literal for the '<em><b>EE Literal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL = eINSTANCE.getUmlEnumLiteral2EEnumLiteral_EELiteral();

	}

} //Uml2ecorePackage
