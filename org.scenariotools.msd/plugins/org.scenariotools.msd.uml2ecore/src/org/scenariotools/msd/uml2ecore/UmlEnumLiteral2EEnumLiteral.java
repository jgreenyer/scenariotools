/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EEnumLiteral;

import org.eclipse.uml2.uml.EnumerationLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Enum Literal2 EEnum Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getUELiteral <em>UE Literal</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getEELiteral <em>EE Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnumLiteral2EEnumLiteral()
 * @model
 * @generated
 */
public interface UmlEnumLiteral2EEnumLiteral extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>UE Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>UE Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UE Literal</em>' reference.
	 * @see #setUELiteral(EnumerationLiteral)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnumLiteral2EEnumLiteral_UELiteral()
	 * @model
	 * @generated
	 */
	EnumerationLiteral getUELiteral();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getUELiteral <em>UE Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UE Literal</em>' reference.
	 * @see #getUELiteral()
	 * @generated
	 */
	void setUELiteral(EnumerationLiteral value);

	/**
	 * Returns the value of the '<em><b>EE Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EE Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EE Literal</em>' reference.
	 * @see #setEELiteral(EEnumLiteral)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnumLiteral2EEnumLiteral_EELiteral()
	 * @model
	 * @generated
	 */
	EEnumLiteral getEELiteral();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral#getEELiteral <em>EE Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EE Literal</em>' reference.
	 * @see #getEELiteral()
	 * @generated
	 */
	void setEELiteral(EEnumLiteral value);

} // UmlEnumLiteral2EEnumLiteral
