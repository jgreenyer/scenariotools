/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.uml2.uml.DataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UML Primitive Data Type To ECore Primitive Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getEDataType <em>EData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLPrimitiveDataTypeToECorePrimitiveDataType()
 * @model
 * @generated
 */
public interface UMLPrimitiveDataTypeToECorePrimitiveDataType extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' reference.
	 * @see #setDataType(DataType)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLPrimitiveDataTypeToECorePrimitiveDataType_DataType()
	 * @model
	 * @generated
	 */
	DataType getDataType();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getDataType <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' reference.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataType value);

	/**
	 * Returns the value of the '<em><b>EData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EData Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EData Type</em>' reference.
	 * @see #setEDataType(EDataType)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLPrimitiveDataTypeToECorePrimitiveDataType_EDataType()
	 * @model
	 * @generated
	 */
	EDataType getEDataType();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType#getEDataType <em>EData Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EData Type</em>' reference.
	 * @see #getEDataType()
	 * @generated
	 */
	void setEDataType(EDataType value);

} // UMLPrimitiveDataTypeToECorePrimitiveDataType
