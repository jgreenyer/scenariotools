/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.uml2.uml.Generalization;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Gen2 EInh</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getGeneralization <em>Generalization</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getEClass <em>EClass</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlGen2EInh()
 * @model
 * @generated
 */
public interface UmlGen2EInh extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Generalization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalization</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalization</em>' reference.
	 * @see #setGeneralization(Generalization)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlGen2EInh_Generalization()
	 * @model
	 * @generated
	 */
	Generalization getGeneralization();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getGeneralization <em>Generalization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generalization</em>' reference.
	 * @see #getGeneralization()
	 * @generated
	 */
	void setGeneralization(Generalization value);

	/**
	 * Returns the value of the '<em><b>EClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass</em>' reference.
	 * @see #setEClass(EClass)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlGen2EInh_EClass()
	 * @model
	 * @generated
	 */
	EClass getEClass();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh#getEClass <em>EClass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EClass</em>' reference.
	 * @see #getEClass()
	 * @generated
	 */
	void setEClass(EClass value);

} // UmlGen2EInh
