/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;
import org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl;
import org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode;
import org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef;
import org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef;
import org.scenariotools.msd.uml2ecore.FirstProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SecProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SelfAss2ERef;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType;
import org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.Uml2ecoreFactory;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;
import org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr;
import org.scenariotools.msd.uml2ecore.UmlEnum2EEnum;
import org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral;
import org.scenariotools.msd.uml2ecore.UmlGen2EInh;
import org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.UmlOp2EOp;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat;
import org.scenariotools.msd.uml2ecore.firstProp2ERef;
import org.scenariotools.msd.uml2ecore.secProp2ERef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Uml2ecorePackageImpl extends EPackageImpl implements Uml2ecorePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlPackage2EPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractContainerCorrespondenceNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlClass2EClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass firstProp2ERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlAssoz2ERefeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass secProp2ERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlGen2EInhEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass firstProp2SelfERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass secProp2SelfERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selfAss2ERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass directedAssoz2ERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlClassProp2EAttrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass directedSelfAss2ERefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlMetamodel2EcoreMetaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlSecMetaModel2EcoreMetaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlOp2EOpEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlOpParam2EParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlSetOp2EStructFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlEnum2EEnumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umlEnumLiteral2EEnumLiteralEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Uml2ecorePackageImpl() {
		super(eNS_URI, Uml2ecoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Uml2ecorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Uml2ecorePackage init() {
		if (isInited) return (Uml2ecorePackage)EPackage.Registry.INSTANCE.getEPackage(Uml2ecorePackage.eNS_URI);

		// Obtain or create and register package
		Uml2ecorePackageImpl theUml2ecorePackage = (Uml2ecorePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Uml2ecorePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Uml2ecorePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		PrimitivemappingsPackageImpl thePrimitivemappingsPackage = (PrimitivemappingsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PrimitivemappingsPackage.eNS_URI) instanceof PrimitivemappingsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PrimitivemappingsPackage.eNS_URI) : PrimitivemappingsPackage.eINSTANCE);

		// Create package meta-data objects
		theUml2ecorePackage.createPackageContents();
		thePrimitivemappingsPackage.createPackageContents();

		// Initialize created meta-data
		theUml2ecorePackage.initializePackageContents();
		thePrimitivemappingsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUml2ecorePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Uml2ecorePackage.eNS_URI, theUml2ecorePackage);
		return theUml2ecorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlPackage2EPackage() {
		return umlPackage2EPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlPackage2EPackage_UmlPackage() {
		return (EReference)umlPackage2EPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlPackage2EPackage_EcorePackage() {
		return (EReference)umlPackage2EPackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlPackage2EPackage_PrimitiveMappingContainer() {
		return (EReference)umlPackage2EPackageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractContainerCorrespondenceNode() {
		return abstractContainerCorrespondenceNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractContainerCorrespondenceNode_ChildCorresp() {
		return (EReference)abstractContainerCorrespondenceNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlClass2EClass() {
		return umlClass2EClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlClass2EClass_Umlc() {
		return (EReference)umlClass2EClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlClass2EClass_Ec() {
		return (EReference)umlClass2EClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfirstProp2ERef() {
		return firstProp2ERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfirstProp2ERef_FirstEnd() {
		return (EReference)firstProp2ERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfirstProp2ERef_SecondERef() {
		return (EReference)firstProp2ERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlAssoz2ERefe() {
		return umlAssoz2ERefeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlAssoz2ERefe_Umla() {
		return (EReference)umlAssoz2ERefeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlAssoz2ERefe_Ass2ERef() {
		return (EReference)umlAssoz2ERefeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlAssoz2ERefe_Ass2Ref2() {
		return (EReference)umlAssoz2ERefeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsecProp2ERef() {
		return secProp2ERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsecProp2ERef_SecondEnd() {
		return (EReference)secProp2ERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsecProp2ERef_FirstERef() {
		return (EReference)secProp2ERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlGen2EInh() {
		return umlGen2EInhEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlGen2EInh_Generalization() {
		return (EReference)umlGen2EInhEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlGen2EInh_EClass() {
		return (EReference)umlGen2EInhEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFirstProp2SelfERef() {
		return firstProp2SelfERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstProp2SelfERef_FirstEnd() {
		return (EReference)firstProp2SelfERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstProp2SelfERef_SecondERef() {
		return (EReference)firstProp2SelfERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecProp2SelfERef() {
		return secProp2SelfERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecProp2SelfERef_SecondEnd() {
		return (EReference)secProp2SelfERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecProp2SelfERef_FirstERef() {
		return (EReference)secProp2SelfERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSelfAss2ERef() {
		return selfAss2ERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelfAss2ERef_Umla() {
		return (EReference)selfAss2ERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelfAss2ERef_FirstERef() {
		return (EReference)selfAss2ERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelfAss2ERef_SecondERef() {
		return (EReference)selfAss2ERefEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirectedAssoz2ERef() {
		return directedAssoz2ERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedAssoz2ERef_FirstEnd() {
		return (EReference)directedAssoz2ERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedAssoz2ERef_Umla() {
		return (EReference)directedAssoz2ERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedAssoz2ERef_SecondEnd() {
		return (EReference)directedAssoz2ERefEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedAssoz2ERef_FirstERef() {
		return (EReference)directedAssoz2ERefEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlClassProp2EAttr() {
		return umlClassProp2EAttrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlClassProp2EAttr_Umlprop() {
		return (EReference)umlClassProp2EAttrEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlClassProp2EAttr_EAttribute() {
		return (EReference)umlClassProp2EAttrEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlClassProp2EAttr_EClassifier() {
		return (EReference)umlClassProp2EAttrEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirectedSelfAss2ERef() {
		return directedSelfAss2ERefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedSelfAss2ERef_Umla() {
		return (EReference)directedSelfAss2ERefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedSelfAss2ERef_FirstEnd() {
		return (EReference)directedSelfAss2ERefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedSelfAss2ERef_FirstERef() {
		return (EReference)directedSelfAss2ERefEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectedSelfAss2ERef_SecondEnd() {
		return (EReference)directedSelfAss2ERefEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLPrimitiveDataTypeToECorePrimitiveDataType() {
		return umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLPrimitiveDataTypeToECorePrimitiveDataType_DataType() {
		return (EReference)umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLPrimitiveDataTypeToECorePrimitiveDataType_EDataType() {
		return (EReference)umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlMetamodel2EcoreMeta() {
		return umlMetamodel2EcoreMetaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlMetamodel2EcoreMeta_UmlMetamodel() {
		return (EReference)umlMetamodel2EcoreMetaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlMetamodel2EcoreMeta_MEcorePack() {
		return (EReference)umlMetamodel2EcoreMetaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLSecMetaModel2EcoreMeta() {
		return umlSecMetaModel2EcoreMetaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLSecMetaModel2EcoreMeta_SecMetamodel() {
		return (EReference)umlSecMetaModel2EcoreMetaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLSecMetaModel2EcoreMeta_EPackage() {
		return (EReference)umlSecMetaModel2EcoreMetaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlOp2EOp() {
		return umlOp2EOpEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlOp2EOp_Umlop() {
		return (EReference)umlOp2EOpEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlOp2EOp_EOperation() {
		return (EReference)umlOp2EOpEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMLOpParam2EParam() {
		return umlOpParam2EParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOpParam2EParam_UmlOpParam() {
		return (EReference)umlOpParam2EParamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMLOpParam2EParam_EParameter() {
		return (EReference)umlOpParam2EParamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlSetOp2EStructFeat() {
		return umlSetOp2EStructFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlSetOp2EStructFeat_Umlop() {
		return (EReference)umlSetOp2EStructFeatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlSetOp2EStructFeat_EStructuralFeature() {
		return (EReference)umlSetOp2EStructFeatEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlEnum2EEnum() {
		return umlEnum2EEnumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlEnum2EEnum_Umlenum() {
		return (EReference)umlEnum2EEnumEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlEnum2EEnum_Eenum() {
		return (EReference)umlEnum2EEnumEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUmlEnumLiteral2EEnumLiteral() {
		return umlEnumLiteral2EEnumLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlEnumLiteral2EEnumLiteral_UELiteral() {
		return (EReference)umlEnumLiteral2EEnumLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUmlEnumLiteral2EEnumLiteral_EELiteral() {
		return (EReference)umlEnumLiteral2EEnumLiteralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecoreFactory getUml2ecoreFactory() {
		return (Uml2ecoreFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		umlPackage2EPackageEClass = createEClass(UML_PACKAGE2_EPACKAGE);
		createEReference(umlPackage2EPackageEClass, UML_PACKAGE2_EPACKAGE__UML_PACKAGE);
		createEReference(umlPackage2EPackageEClass, UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE);
		createEReference(umlPackage2EPackageEClass, UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER);

		abstractContainerCorrespondenceNodeEClass = createEClass(ABSTRACT_CONTAINER_CORRESPONDENCE_NODE);
		createEReference(abstractContainerCorrespondenceNodeEClass, ABSTRACT_CONTAINER_CORRESPONDENCE_NODE__CHILD_CORRESP);

		umlClass2EClassEClass = createEClass(UML_CLASS2_ECLASS);
		createEReference(umlClass2EClassEClass, UML_CLASS2_ECLASS__UMLC);
		createEReference(umlClass2EClassEClass, UML_CLASS2_ECLASS__EC);

		firstProp2ERefEClass = createEClass(FIRST_PROP2_EREF);
		createEReference(firstProp2ERefEClass, FIRST_PROP2_EREF__FIRST_END);
		createEReference(firstProp2ERefEClass, FIRST_PROP2_EREF__SECOND_EREF);

		umlAssoz2ERefeEClass = createEClass(UML_ASSOZ2_EREFE);
		createEReference(umlAssoz2ERefeEClass, UML_ASSOZ2_EREFE__UMLA);
		createEReference(umlAssoz2ERefeEClass, UML_ASSOZ2_EREFE__ASS2_EREF);
		createEReference(umlAssoz2ERefeEClass, UML_ASSOZ2_EREFE__ASS2_REF2);

		secProp2ERefEClass = createEClass(SEC_PROP2_EREF);
		createEReference(secProp2ERefEClass, SEC_PROP2_EREF__SECOND_END);
		createEReference(secProp2ERefEClass, SEC_PROP2_EREF__FIRST_EREF);

		umlGen2EInhEClass = createEClass(UML_GEN2_EINH);
		createEReference(umlGen2EInhEClass, UML_GEN2_EINH__GENERALIZATION);
		createEReference(umlGen2EInhEClass, UML_GEN2_EINH__ECLASS);

		firstProp2SelfERefEClass = createEClass(FIRST_PROP2_SELF_EREF);
		createEReference(firstProp2SelfERefEClass, FIRST_PROP2_SELF_EREF__FIRST_END);
		createEReference(firstProp2SelfERefEClass, FIRST_PROP2_SELF_EREF__SECOND_EREF);

		secProp2SelfERefEClass = createEClass(SEC_PROP2_SELF_EREF);
		createEReference(secProp2SelfERefEClass, SEC_PROP2_SELF_EREF__SECOND_END);
		createEReference(secProp2SelfERefEClass, SEC_PROP2_SELF_EREF__FIRST_EREF);

		selfAss2ERefEClass = createEClass(SELF_ASS2_EREF);
		createEReference(selfAss2ERefEClass, SELF_ASS2_EREF__UMLA);
		createEReference(selfAss2ERefEClass, SELF_ASS2_EREF__FIRST_EREF);
		createEReference(selfAss2ERefEClass, SELF_ASS2_EREF__SECOND_EREF);

		directedAssoz2ERefEClass = createEClass(DIRECTED_ASSOZ2_EREF);
		createEReference(directedAssoz2ERefEClass, DIRECTED_ASSOZ2_EREF__FIRST_END);
		createEReference(directedAssoz2ERefEClass, DIRECTED_ASSOZ2_EREF__UMLA);
		createEReference(directedAssoz2ERefEClass, DIRECTED_ASSOZ2_EREF__SECOND_END);
		createEReference(directedAssoz2ERefEClass, DIRECTED_ASSOZ2_EREF__FIRST_EREF);

		umlClassProp2EAttrEClass = createEClass(UML_CLASS_PROP2_EATTR);
		createEReference(umlClassProp2EAttrEClass, UML_CLASS_PROP2_EATTR__UMLPROP);
		createEReference(umlClassProp2EAttrEClass, UML_CLASS_PROP2_EATTR__EATTRIBUTE);
		createEReference(umlClassProp2EAttrEClass, UML_CLASS_PROP2_EATTR__ECLASSIFIER);

		directedSelfAss2ERefEClass = createEClass(DIRECTED_SELF_ASS2_EREF);
		createEReference(directedSelfAss2ERefEClass, DIRECTED_SELF_ASS2_EREF__UMLA);
		createEReference(directedSelfAss2ERefEClass, DIRECTED_SELF_ASS2_EREF__FIRST_END);
		createEReference(directedSelfAss2ERefEClass, DIRECTED_SELF_ASS2_EREF__FIRST_EREF);
		createEReference(directedSelfAss2ERefEClass, DIRECTED_SELF_ASS2_EREF__SECOND_END);

		umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass = createEClass(UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE);
		createEReference(umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass, UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE);
		createEReference(umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass, UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE);

		umlMetamodel2EcoreMetaEClass = createEClass(UML_METAMODEL2_ECORE_META);
		createEReference(umlMetamodel2EcoreMetaEClass, UML_METAMODEL2_ECORE_META__UML_METAMODEL);
		createEReference(umlMetamodel2EcoreMetaEClass, UML_METAMODEL2_ECORE_META__MECORE_PACK);

		umlSecMetaModel2EcoreMetaEClass = createEClass(UML_SEC_META_MODEL2_ECORE_META);
		createEReference(umlSecMetaModel2EcoreMetaEClass, UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL);
		createEReference(umlSecMetaModel2EcoreMetaEClass, UML_SEC_META_MODEL2_ECORE_META__EPACKAGE);

		umlOp2EOpEClass = createEClass(UML_OP2_EOP);
		createEReference(umlOp2EOpEClass, UML_OP2_EOP__UMLOP);
		createEReference(umlOp2EOpEClass, UML_OP2_EOP__EOPERATION);

		umlOpParam2EParamEClass = createEClass(UML_OP_PARAM2_EPARAM);
		createEReference(umlOpParam2EParamEClass, UML_OP_PARAM2_EPARAM__UML_OP_PARAM);
		createEReference(umlOpParam2EParamEClass, UML_OP_PARAM2_EPARAM__EPARAMETER);

		umlSetOp2EStructFeatEClass = createEClass(UML_SET_OP2_ESTRUCT_FEAT);
		createEReference(umlSetOp2EStructFeatEClass, UML_SET_OP2_ESTRUCT_FEAT__UMLOP);
		createEReference(umlSetOp2EStructFeatEClass, UML_SET_OP2_ESTRUCT_FEAT__ESTRUCTURAL_FEATURE);

		umlEnum2EEnumEClass = createEClass(UML_ENUM2_EENUM);
		createEReference(umlEnum2EEnumEClass, UML_ENUM2_EENUM__UMLENUM);
		createEReference(umlEnum2EEnumEClass, UML_ENUM2_EENUM__EENUM);

		umlEnumLiteral2EEnumLiteralEClass = createEClass(UML_ENUM_LITERAL2_EENUM_LITERAL);
		createEReference(umlEnumLiteral2EEnumLiteralEClass, UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL);
		createEReference(umlEnumLiteral2EEnumLiteralEClass, UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		PrimitivemappingsPackage thePrimitivemappingsPackage = (PrimitivemappingsPackage)EPackage.Registry.INSTANCE.getEPackage(PrimitivemappingsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		umlPackage2EPackageEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlClass2EClassEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		firstProp2ERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlAssoz2ERefeEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		secProp2ERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlGen2EInhEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		firstProp2SelfERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		secProp2SelfERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		selfAss2ERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		directedAssoz2ERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlClassProp2EAttrEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		directedSelfAss2ERefEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlMetamodel2EcoreMetaEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlSecMetaModel2EcoreMetaEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlOp2EOpEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlOpParam2EParamEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlSetOp2EStructFeatEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlEnum2EEnumEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());
		umlEnumLiteral2EEnumLiteralEClass.getESuperTypes().add(this.getAbstractContainerCorrespondenceNode());

		// Initialize classes and features; add operations and parameters
		initEClass(umlPackage2EPackageEClass, UmlPackage2EPackage.class, "UmlPackage2EPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlPackage2EPackage_UmlPackage(), theUMLPackage.getPackage(), null, "umlPackage", null, 0, 1, UmlPackage2EPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlPackage2EPackage_EcorePackage(), theEcorePackage.getEPackage(), null, "ecorePackage", null, 0, 1, UmlPackage2EPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlPackage2EPackage_PrimitiveMappingContainer(), thePrimitivemappingsPackage.getPrimitiveMappingContainer(), null, "primitiveMappingContainer", null, 0, 1, UmlPackage2EPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractContainerCorrespondenceNodeEClass, AbstractContainerCorrespondenceNode.class, "AbstractContainerCorrespondenceNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractContainerCorrespondenceNode_ChildCorresp(), this.getAbstractContainerCorrespondenceNode(), null, "childCorresp", null, 0, -1, AbstractContainerCorrespondenceNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlClass2EClassEClass, UmlClass2EClass.class, "UmlClass2EClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlClass2EClass_Umlc(), theUMLPackage.getClass_(), null, "umlc", null, 0, 1, UmlClass2EClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlClass2EClass_Ec(), theEcorePackage.getEClass(), null, "ec", null, 0, 1, UmlClass2EClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(firstProp2ERefEClass, firstProp2ERef.class, "firstProp2ERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getfirstProp2ERef_FirstEnd(), theUMLPackage.getProperty(), null, "firstEnd", null, 0, 1, firstProp2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfirstProp2ERef_SecondERef(), theEcorePackage.getEReference(), null, "secondERef", null, 0, 1, firstProp2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlAssoz2ERefeEClass, UmlAssoz2ERefe.class, "UmlAssoz2ERefe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlAssoz2ERefe_Umla(), theUMLPackage.getAssociation(), null, "umla", null, 0, 1, UmlAssoz2ERefe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlAssoz2ERefe_Ass2ERef(), theEcorePackage.getEReference(), null, "Ass2ERef", null, 0, 1, UmlAssoz2ERefe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlAssoz2ERefe_Ass2Ref2(), theEcorePackage.getEReference(), null, "Ass2Ref2", null, 0, 1, UmlAssoz2ERefe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(secProp2ERefEClass, secProp2ERef.class, "secProp2ERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getsecProp2ERef_SecondEnd(), theUMLPackage.getProperty(), null, "secondEnd", null, 0, 1, secProp2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsecProp2ERef_FirstERef(), theEcorePackage.getEReference(), null, "firstERef", null, 0, 1, secProp2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlGen2EInhEClass, UmlGen2EInh.class, "UmlGen2EInh", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlGen2EInh_Generalization(), theUMLPackage.getGeneralization(), null, "Generalization", null, 0, 1, UmlGen2EInh.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlGen2EInh_EClass(), theEcorePackage.getEClass(), null, "EClass", null, 0, 1, UmlGen2EInh.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(firstProp2SelfERefEClass, FirstProp2SelfERef.class, "FirstProp2SelfERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFirstProp2SelfERef_FirstEnd(), theUMLPackage.getProperty(), null, "firstEnd", null, 0, 1, FirstProp2SelfERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFirstProp2SelfERef_SecondERef(), theEcorePackage.getEReference(), null, "secondERef", null, 0, 1, FirstProp2SelfERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(secProp2SelfERefEClass, SecProp2SelfERef.class, "SecProp2SelfERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSecProp2SelfERef_SecondEnd(), theUMLPackage.getProperty(), null, "secondEnd", null, 0, 1, SecProp2SelfERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecProp2SelfERef_FirstERef(), theEcorePackage.getEReference(), null, "firstERef", null, 0, 1, SecProp2SelfERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(selfAss2ERefEClass, SelfAss2ERef.class, "SelfAss2ERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSelfAss2ERef_Umla(), theUMLPackage.getAssociation(), null, "umla", null, 0, 1, SelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSelfAss2ERef_FirstERef(), theEcorePackage.getEReference(), null, "firstERef", null, 0, 1, SelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSelfAss2ERef_SecondERef(), theEcorePackage.getEReference(), null, "secondERef", null, 0, 1, SelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(directedAssoz2ERefEClass, DirectedAssoz2ERef.class, "DirectedAssoz2ERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirectedAssoz2ERef_FirstEnd(), theUMLPackage.getProperty(), null, "firstEnd", null, 0, 1, DirectedAssoz2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedAssoz2ERef_Umla(), theUMLPackage.getAssociation(), null, "umla", null, 0, 1, DirectedAssoz2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedAssoz2ERef_SecondEnd(), theUMLPackage.getProperty(), null, "secondEnd", null, 0, 1, DirectedAssoz2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedAssoz2ERef_FirstERef(), theEcorePackage.getEReference(), null, "firstERef", null, 0, 1, DirectedAssoz2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlClassProp2EAttrEClass, UmlClassProp2EAttr.class, "UmlClassProp2EAttr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlClassProp2EAttr_Umlprop(), theUMLPackage.getProperty(), null, "umlprop", null, 0, 1, UmlClassProp2EAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlClassProp2EAttr_EAttribute(), theEcorePackage.getEAttribute(), null, "EAttribute", null, 0, 1, UmlClassProp2EAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlClassProp2EAttr_EClassifier(), theEcorePackage.getEClassifier(), null, "EClassifier", null, 0, 1, UmlClassProp2EAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(directedSelfAss2ERefEClass, DirectedSelfAss2ERef.class, "DirectedSelfAss2ERef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirectedSelfAss2ERef_Umla(), theUMLPackage.getAssociation(), null, "umla", null, 0, 1, DirectedSelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedSelfAss2ERef_FirstEnd(), theUMLPackage.getProperty(), null, "firstEnd", null, 0, 1, DirectedSelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedSelfAss2ERef_FirstERef(), theEcorePackage.getEReference(), null, "firstERef", null, 0, 1, DirectedSelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDirectedSelfAss2ERef_SecondEnd(), theUMLPackage.getProperty(), null, "secondEnd", null, 0, 1, DirectedSelfAss2ERef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlPrimitiveDataTypeToECorePrimitiveDataTypeEClass, UMLPrimitiveDataTypeToECorePrimitiveDataType.class, "UMLPrimitiveDataTypeToECorePrimitiveDataType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLPrimitiveDataTypeToECorePrimitiveDataType_DataType(), theUMLPackage.getDataType(), null, "DataType", null, 0, 1, UMLPrimitiveDataTypeToECorePrimitiveDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLPrimitiveDataTypeToECorePrimitiveDataType_EDataType(), ecorePackage.getEDataType(), null, "EDataType", null, 0, 1, UMLPrimitiveDataTypeToECorePrimitiveDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlMetamodel2EcoreMetaEClass, UmlMetamodel2EcoreMeta.class, "UmlMetamodel2EcoreMeta", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlMetamodel2EcoreMeta_UmlMetamodel(), theUMLPackage.getModel(), null, "umlMetamodel", null, 0, 1, UmlMetamodel2EcoreMeta.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlMetamodel2EcoreMeta_MEcorePack(), theEcorePackage.getEPackage(), null, "mEcorePack", null, 0, 1, UmlMetamodel2EcoreMeta.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlSecMetaModel2EcoreMetaEClass, UMLSecMetaModel2EcoreMeta.class, "UMLSecMetaModel2EcoreMeta", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLSecMetaModel2EcoreMeta_SecMetamodel(), theUMLPackage.getModel(), null, "secMetamodel", null, 0, 1, UMLSecMetaModel2EcoreMeta.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLSecMetaModel2EcoreMeta_EPackage(), theEcorePackage.getEPackage(), null, "EPackage", null, 0, 1, UMLSecMetaModel2EcoreMeta.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlOp2EOpEClass, UmlOp2EOp.class, "UmlOp2EOp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlOp2EOp_Umlop(), theUMLPackage.getOperation(), null, "umlop", null, 0, 1, UmlOp2EOp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlOp2EOp_EOperation(), theEcorePackage.getEOperation(), null, "EOperation", null, 0, 1, UmlOp2EOp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlOpParam2EParamEClass, UMLOpParam2EParam.class, "UMLOpParam2EParam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMLOpParam2EParam_UmlOpParam(), theUMLPackage.getParameter(), null, "umlOpParam", null, 0, 1, UMLOpParam2EParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMLOpParam2EParam_EParameter(), theEcorePackage.getEParameter(), null, "EParameter", null, 0, 1, UMLOpParam2EParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlSetOp2EStructFeatEClass, UmlSetOp2EStructFeat.class, "UmlSetOp2EStructFeat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlSetOp2EStructFeat_Umlop(), theUMLPackage.getOperation(), null, "umlop", null, 0, 1, UmlSetOp2EStructFeat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlSetOp2EStructFeat_EStructuralFeature(), theEcorePackage.getEStructuralFeature(), null, "EStructuralFeature", null, 0, 1, UmlSetOp2EStructFeat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEClass(umlEnum2EEnumEClass, UmlEnum2EEnum.class, "UmlEnum2EEnum", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlEnum2EEnum_Umlenum(), theUMLPackage.getEnumeration(), null, "umlenum", null, 0, 1, UmlEnum2EEnum.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlEnum2EEnum_Eenum(), ecorePackage.getEEnum(), null, "eenum", null, 0, 1, UmlEnum2EEnum.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umlEnumLiteral2EEnumLiteralEClass, UmlEnumLiteral2EEnumLiteral.class, "UmlEnumLiteral2EEnumLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUmlEnumLiteral2EEnumLiteral_UELiteral(), theUMLPackage.getEnumerationLiteral(), null, "uELiteral", null, 0, 1, UmlEnumLiteral2EEnumLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUmlEnumLiteral2EEnumLiteral_EELiteral(), ecorePackage.getEEnumLiteral(), null, "eELiteral", null, 0, 1, UmlEnumLiteral2EEnumLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Uml2ecorePackageImpl
