/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EParameter;
import org.eclipse.uml2.uml.Parameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UML Op Param2 EParam</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getUmlOpParam <em>Uml Op Param</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getEParameter <em>EParameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLOpParam2EParam()
 * @model
 * @generated
 */
public interface UMLOpParam2EParam extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Uml Op Param</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Op Param</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Op Param</em>' reference.
	 * @see #setUmlOpParam(Parameter)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLOpParam2EParam_UmlOpParam()
	 * @model
	 * @generated
	 */
	Parameter getUmlOpParam();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getUmlOpParam <em>Uml Op Param</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml Op Param</em>' reference.
	 * @see #getUmlOpParam()
	 * @generated
	 */
	void setUmlOpParam(Parameter value);

	/**
	 * Returns the value of the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EParameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EParameter</em>' reference.
	 * @see #setEParameter(EParameter)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLOpParam2EParam_EParameter()
	 * @model
	 * @generated
	 */
	EParameter getEParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam#getEParameter <em>EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EParameter</em>' reference.
	 * @see #getEParameter()
	 * @generated
	 */
	void setEParameter(EParameter value);

} // UMLOpParam2EParam
