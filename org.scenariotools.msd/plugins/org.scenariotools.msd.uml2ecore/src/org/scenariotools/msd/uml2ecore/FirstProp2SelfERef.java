/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>First Prop2 Self ERef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getFirstEnd <em>First End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getSecondERef <em>Second ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getFirstProp2SelfERef()
 * @model
 * @generated
 */
public interface FirstProp2SelfERef extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First End</em>' reference.
	 * @see #setFirstEnd(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getFirstProp2SelfERef_FirstEnd()
	 * @model
	 * @generated
	 */
	Property getFirstEnd();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getFirstEnd <em>First End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First End</em>' reference.
	 * @see #getFirstEnd()
	 * @generated
	 */
	void setFirstEnd(Property value);

	/**
	 * Returns the value of the '<em><b>Second ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second ERef</em>' reference.
	 * @see #setSecondERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getFirstProp2SelfERef_SecondERef()
	 * @model
	 * @generated
	 */
	EReference getSecondERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef#getSecondERef <em>Second ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second ERef</em>' reference.
	 * @see #getSecondERef()
	 * @generated
	 */
	void setSecondERef(EReference value);

} // FirstProp2SelfERef
