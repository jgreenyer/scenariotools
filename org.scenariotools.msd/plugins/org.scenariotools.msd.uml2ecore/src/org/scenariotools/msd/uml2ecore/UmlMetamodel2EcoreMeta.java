/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.uml2.uml.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Metamodel2 Ecore Meta</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getUmlMetamodel <em>Uml Metamodel</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getMEcorePack <em>MEcore Pack</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlMetamodel2EcoreMeta()
 * @model
 * @generated
 */
public interface UmlMetamodel2EcoreMeta extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Uml Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Metamodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Metamodel</em>' reference.
	 * @see #setUmlMetamodel(Model)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlMetamodel2EcoreMeta_UmlMetamodel()
	 * @model
	 * @generated
	 */
	Model getUmlMetamodel();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getUmlMetamodel <em>Uml Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml Metamodel</em>' reference.
	 * @see #getUmlMetamodel()
	 * @generated
	 */
	void setUmlMetamodel(Model value);

	/**
	 * Returns the value of the '<em><b>MEcore Pack</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MEcore Pack</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MEcore Pack</em>' reference.
	 * @see #setMEcorePack(EPackage)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlMetamodel2EcoreMeta_MEcorePack()
	 * @model
	 * @generated
	 */
	EPackage getMEcorePack();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta#getMEcorePack <em>MEcore Pack</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MEcore Pack</em>' reference.
	 * @see #getMEcorePack()
	 * @generated
	 */
	void setMEcorePack(EPackage value);

} // UmlMetamodel2EcoreMeta
