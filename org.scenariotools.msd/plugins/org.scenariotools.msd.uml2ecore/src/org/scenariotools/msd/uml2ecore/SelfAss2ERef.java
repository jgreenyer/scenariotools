/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Association;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Self Ass2 ERef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getFirstERef <em>First ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getSecondERef <em>Second ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSelfAss2ERef()
 * @model
 * @generated
 */
public interface SelfAss2ERef extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umla</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umla</em>' reference.
	 * @see #setUmla(Association)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSelfAss2ERef_Umla()
	 * @model
	 * @generated
	 */
	Association getUmla();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getUmla <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umla</em>' reference.
	 * @see #getUmla()
	 * @generated
	 */
	void setUmla(Association value);

	/**
	 * Returns the value of the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First ERef</em>' reference.
	 * @see #setFirstERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSelfAss2ERef_FirstERef()
	 * @model
	 * @generated
	 */
	EReference getFirstERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getFirstERef <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First ERef</em>' reference.
	 * @see #getFirstERef()
	 * @generated
	 */
	void setFirstERef(EReference value);

	/**
	 * Returns the value of the '<em><b>Second ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second ERef</em>' reference.
	 * @see #setSecondERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSelfAss2ERef_SecondERef()
	 * @model
	 * @generated
	 */
	EReference getSecondERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef#getSecondERef <em>Second ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second ERef</em>' reference.
	 * @see #getSecondERef()
	 * @generated
	 */
	void setSecondERef(EReference value);

} // SelfAss2ERef
