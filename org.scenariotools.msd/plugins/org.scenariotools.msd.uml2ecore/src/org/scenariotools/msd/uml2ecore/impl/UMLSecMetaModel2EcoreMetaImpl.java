/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Model;
import org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UML Sec Meta Model2 Ecore Meta</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl#getSecMetamodel <em>Sec Metamodel</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLSecMetaModel2EcoreMetaImpl#getEPackage <em>EPackage</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UMLSecMetaModel2EcoreMetaImpl extends AbstractContainerCorrespondenceNodeImpl implements UMLSecMetaModel2EcoreMeta {
	/**
	 * The cached value of the '{@link #getSecMetamodel() <em>Sec Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecMetamodel()
	 * @generated
	 * @ordered
	 */
	protected Model secMetamodel;

	/**
	 * The cached value of the '{@link #getEPackage() <em>EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage ePackage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UMLSecMetaModel2EcoreMetaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_SEC_META_MODEL2_ECORE_META;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getSecMetamodel() {
		if (secMetamodel != null && secMetamodel.eIsProxy()) {
			InternalEObject oldSecMetamodel = (InternalEObject)secMetamodel;
			secMetamodel = (Model)eResolveProxy(oldSecMetamodel);
			if (secMetamodel != oldSecMetamodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL, oldSecMetamodel, secMetamodel));
			}
		}
		return secMetamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model basicGetSecMetamodel() {
		return secMetamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecMetamodel(Model newSecMetamodel) {
		Model oldSecMetamodel = secMetamodel;
		secMetamodel = newSecMetamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL, oldSecMetamodel, secMetamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getEPackage() {
		if (ePackage != null && ePackage.eIsProxy()) {
			InternalEObject oldEPackage = (InternalEObject)ePackage;
			ePackage = (EPackage)eResolveProxy(oldEPackage);
			if (ePackage != oldEPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE, oldEPackage, ePackage));
			}
		}
		return ePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetEPackage() {
		return ePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEPackage(EPackage newEPackage) {
		EPackage oldEPackage = ePackage;
		ePackage = newEPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE, oldEPackage, ePackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL:
				if (resolve) return getSecMetamodel();
				return basicGetSecMetamodel();
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE:
				if (resolve) return getEPackage();
				return basicGetEPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL:
				setSecMetamodel((Model)newValue);
				return;
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE:
				setEPackage((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL:
				setSecMetamodel((Model)null);
				return;
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE:
				setEPackage((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__SEC_METAMODEL:
				return secMetamodel != null;
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META__EPACKAGE:
				return ePackage != null;
		}
		return super.eIsSet(featureID);
	}

} //UMLSecMetaModel2EcoreMetaImpl
