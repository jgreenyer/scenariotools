/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EEnum;

import org.eclipse.uml2.uml.Enumeration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Enum2 EEnum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getUmlenum <em>Umlenum</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getEenum <em>Eenum</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnum2EEnum()
 * @model
 * @generated
 */
public interface UmlEnum2EEnum extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umlenum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umlenum</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umlenum</em>' reference.
	 * @see #setUmlenum(Enumeration)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnum2EEnum_Umlenum()
	 * @model
	 * @generated
	 */
	Enumeration getUmlenum();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getUmlenum <em>Umlenum</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umlenum</em>' reference.
	 * @see #getUmlenum()
	 * @generated
	 */
	void setUmlenum(Enumeration value);

	/**
	 * Returns the value of the '<em><b>Eenum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Eenum</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eenum</em>' reference.
	 * @see #setEenum(EEnum)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlEnum2EEnum_Eenum()
	 * @model
	 * @generated
	 */
	EEnum getEenum();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum#getEenum <em>Eenum</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eenum</em>' reference.
	 * @see #getEenum()
	 * @generated
	 */
	void setEenum(EEnum value);

} // UmlEnum2EEnum
