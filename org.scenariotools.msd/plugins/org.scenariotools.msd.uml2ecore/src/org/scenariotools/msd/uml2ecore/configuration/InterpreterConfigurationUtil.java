package org.scenariotools.msd.uml2ecore.configuration;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.uml2.uml.Package;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

public class InterpreterConfigurationUtil {

	public static Configuration getTGGInterpreterConfiguration(Package packageToTransform,
			String containerName, String ecoreFileName, String correspondenceModelFileName){
		
		Configuration configuration = InterpreterconfigurationFactory.eINSTANCE.createConfiguration();
		
		// create domain models
		DomainModel umlDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel umlPrimitiveMappingsDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel uml2ecoreDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel ecoreMetaDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel ecoreDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		
		// create application scenario
		ApplicationScenario applicationScenario = InterpreterconfigurationFactory.eINSTANCE.createApplicationScenario();
		
		// configure application scenario
		applicationScenario.getSourceDomainModel().add(umlDomainModel);
		applicationScenario.getSourceDomainModel().add(umlPrimitiveMappingsDomainModel);
		applicationScenario.getSourceDomainModel().add(ecoreMetaDomainModel);
		applicationScenario.getCorrespondenceModel().add(uml2ecoreDomainModel);
		applicationScenario.getTargetDomainModel().add(ecoreDomainModel);
		applicationScenario.setName("FWD");
		applicationScenario.setMode(ApplicationMode.TRANSFORM);
				
		// add domain models and application scenario to configuration
		configuration.setRecordRuleBindings(false);
		configuration.getApplicationScenario().add(applicationScenario);
		configuration.setActiveApplicationScenario(applicationScenario);
		configuration.getDomainModel().add(umlDomainModel);
		configuration.getDomainModel().add(umlPrimitiveMappingsDomainModel);
		configuration.getDomainModel().add(ecoreMetaDomainModel);
		configuration.getDomainModel().add(uml2ecoreDomainModel);
		configuration.getDomainModel().add(ecoreDomainModel);
		
		
		
		// load and set TGG
		ResourceSet resourceSet = packageToTransform.eResource().getResourceSet();
		Resource tggResource = resourceSet.getResource(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg"), true);
		configuration.setTripleGraphGrammar((TripleGraphGrammar) tggResource.getContents().get(0));
		
		// load and assign typed models to domain models
		TypedModel umlTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_UW7BkMjQEd-jwuhwx8tcwg"), true);
		umlDomainModel.setTypedModel(umlTypedModel);
		TypedModel umlPrimitiveMappingsTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#__QyqcMjREd-xCNR3dJ1ydw"), true);
		umlPrimitiveMappingsDomainModel.setTypedModel(umlPrimitiveMappingsTypedModel);
		TypedModel ecoreMetaTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_kuUt0MjQEd-jwuhwx8tcwg"), true);
		ecoreMetaDomainModel.setTypedModel(ecoreMetaTypedModel);
		TypedModel uml2EcoreTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_nFbIEMjQEd-jwuhwx8tcwg"), true);
		uml2ecoreDomainModel.setTypedModel(uml2EcoreTypedModel);
		TypedModel ecoreTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_eziKYMjQEd-jwuhwx8tcwg"), true);
		ecoreDomainModel.setTypedModel(ecoreTypedModel);
		
		//load and assign root objects to domain models
		EObject primitiveMappingsRootObject = resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/primitivemappings/uml2ecore.primitivemappings#/"), true);
		umlPrimitiveMappingsDomainModel.getRootObject().add(primitiveMappingsRootObject);
		EObject ecoreMetaRootObject = resourceSet.getEObject(URI.createURI("platform:/plugin/org.eclipse.emf.ecore/model/Ecore.ecore#/"), true);
		ecoreMetaDomainModel.getRootObject().add(ecoreMetaRootObject);

		//assign resourceURIs to domain models
		URI containerURI = URI.createPlatformResourceURI(containerName+"/", true);
		URI absoluteEcoreFileNameURI = URI.createPlatformResourceURI(containerName+"/"+ecoreFileName, true);
		URI absoluteCorrespondenceModelFileNameURI = URI.createPlatformResourceURI(containerName+"/"+correspondenceModelFileName, true);
		URI absoluteUMLModelFileNameURI = packageToTransform.eResource().getURI();
		URI relativeEcoreFileNameURI = absoluteEcoreFileNameURI.deresolve(containerURI);
		URI relativeCorrespondenceModelFileNameURI = absoluteCorrespondenceModelFileNameURI.deresolve(containerURI);
		URI relativeUMLModelFileNameURI = absoluteUMLModelFileNameURI.deresolve(containerURI);
		String relativeUMLModelFileNameURIString = relativeUMLModelFileNameURI.toString();
		String relativeCorrespondenceModelFileNameURIString = relativeCorrespondenceModelFileNameURI.toString();
		String relativeEcoreFileNameURIString = relativeEcoreFileNameURI.toString();
		umlDomainModel.getResourceURI().add(relativeUMLModelFileNameURIString);
		uml2ecoreDomainModel.getResourceURI().add(relativeCorrespondenceModelFileNameURIString);
		ecoreDomainModel.getResourceURI().add(relativeEcoreFileNameURIString);
		

		// create initialAxiomBinding		
		RuleBindingContainer ruleBindingContainer = InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer();
		configuration.setRuleBindingContainer(ruleBindingContainer);
		AxiomBinding axiomBinding = InterpreterconfigurationFactory.eINSTANCE.createAxiomBinding();
		ruleBindingContainer.setInitialAxiomBinding(axiomBinding);
		TripleGraphGrammarAxiom tggAxiom = (TripleGraphGrammarAxiom) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_zave8MjQEd-jwuhwx8tcwg"), true);
		Node node1 = (Node) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_rY3BQMjSEd-xCNR3dJ1ydw"), true);
		Node node2 = (Node) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.uml2ecore/uml2ecore-tgg/uml2ecore.tgg#_kcoEYMjREd-xCNR3dJ1ydw"), true);		
		axiomBinding.setTggRule(tggAxiom);
		InitialAxiomNodeBinding initialAxiomNodeBinding1 = InterpreterconfigurationFactory.eINSTANCE.createInitialAxiomNodeBinding();
		InitialAxiomNodeBinding initialAxiomNodeBinding2 = InterpreterconfigurationFactory.eINSTANCE.createInitialAxiomNodeBinding();
		initialAxiomNodeBinding1.setNode(node1);
		initialAxiomNodeBinding2.setNode(node2);
		initialAxiomNodeBinding1.setEObject(primitiveMappingsRootObject);
		initialAxiomNodeBinding2.setEObject(packageToTransform);
		axiomBinding.getInitialAxiomNodeBinding().add(initialAxiomNodeBinding1);
		axiomBinding.getInitialAxiomNodeBinding().add(initialAxiomNodeBinding2);
		
		return configuration;
	}
	
}
