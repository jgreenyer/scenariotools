/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Class2 EClass</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl#getUmlc <em>Umlc</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlClass2EClassImpl#getEc <em>Ec</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlClass2EClassImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlClass2EClass {
	/**
	 * The cached value of the '{@link #getUmlc() <em>Umlc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlc()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class umlc;

	/**
	 * The cached value of the '{@link #getEc() <em>Ec</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEc()
	 * @generated
	 * @ordered
	 */
	protected EClass ec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlClass2EClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_CLASS2_ECLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getUmlc() {
		if (umlc != null && umlc.eIsProxy()) {
			InternalEObject oldUmlc = (InternalEObject)umlc;
			umlc = (org.eclipse.uml2.uml.Class)eResolveProxy(oldUmlc);
			if (umlc != oldUmlc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC, oldUmlc, umlc));
			}
		}
		return umlc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetUmlc() {
		return umlc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlc(org.eclipse.uml2.uml.Class newUmlc) {
		org.eclipse.uml2.uml.Class oldUmlc = umlc;
		umlc = newUmlc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC, oldUmlc, umlc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEc() {
		if (ec != null && ec.eIsProxy()) {
			InternalEObject oldEc = (InternalEObject)ec;
			ec = (EClass)eResolveProxy(oldEc);
			if (ec != oldEc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_CLASS2_ECLASS__EC, oldEc, ec));
			}
		}
		return ec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetEc() {
		return ec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEc(EClass newEc) {
		EClass oldEc = ec;
		ec = newEc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_CLASS2_ECLASS__EC, oldEc, ec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC:
				if (resolve) return getUmlc();
				return basicGetUmlc();
			case Uml2ecorePackage.UML_CLASS2_ECLASS__EC:
				if (resolve) return getEc();
				return basicGetEc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC:
				setUmlc((org.eclipse.uml2.uml.Class)newValue);
				return;
			case Uml2ecorePackage.UML_CLASS2_ECLASS__EC:
				setEc((EClass)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC:
				setUmlc((org.eclipse.uml2.uml.Class)null);
				return;
			case Uml2ecorePackage.UML_CLASS2_ECLASS__EC:
				setEc((EClass)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_CLASS2_ECLASS__UMLC:
				return umlc != null;
			case Uml2ecorePackage.UML_CLASS2_ECLASS__EC:
				return ec != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlClass2EClassImpl
