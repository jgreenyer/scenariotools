/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Package2 EPackage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl#getUmlPackage <em>Uml Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl#getEcorePackage <em>Ecore Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlPackage2EPackageImpl#getPrimitiveMappingContainer <em>Primitive Mapping Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlPackage2EPackageImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlPackage2EPackage {
	/**
	 * The cached value of the '{@link #getUmlPackage() <em>Uml Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlPackage()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package umlPackage;

	/**
	 * The cached value of the '{@link #getEcorePackage() <em>Ecore Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEcorePackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage ecorePackage;

	/**
	 * The cached value of the '{@link #getPrimitiveMappingContainer() <em>Primitive Mapping Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimitiveMappingContainer()
	 * @generated
	 * @ordered
	 */
	protected PrimitiveMappingContainer primitiveMappingContainer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlPackage2EPackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_PACKAGE2_EPACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getUmlPackage() {
		if (umlPackage != null && umlPackage.eIsProxy()) {
			InternalEObject oldUmlPackage = (InternalEObject)umlPackage;
			umlPackage = (org.eclipse.uml2.uml.Package)eResolveProxy(oldUmlPackage);
			if (umlPackage != oldUmlPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE, oldUmlPackage, umlPackage));
			}
		}
		return umlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetUmlPackage() {
		return umlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlPackage(org.eclipse.uml2.uml.Package newUmlPackage) {
		org.eclipse.uml2.uml.Package oldUmlPackage = umlPackage;
		umlPackage = newUmlPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE, oldUmlPackage, umlPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getEcorePackage() {
		if (ecorePackage != null && ecorePackage.eIsProxy()) {
			InternalEObject oldEcorePackage = (InternalEObject)ecorePackage;
			ecorePackage = (EPackage)eResolveProxy(oldEcorePackage);
			if (ecorePackage != oldEcorePackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE, oldEcorePackage, ecorePackage));
			}
		}
		return ecorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetEcorePackage() {
		return ecorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEcorePackage(EPackage newEcorePackage) {
		EPackage oldEcorePackage = ecorePackage;
		ecorePackage = newEcorePackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE, oldEcorePackage, ecorePackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveMappingContainer getPrimitiveMappingContainer() {
		if (primitiveMappingContainer != null && primitiveMappingContainer.eIsProxy()) {
			InternalEObject oldPrimitiveMappingContainer = (InternalEObject)primitiveMappingContainer;
			primitiveMappingContainer = (PrimitiveMappingContainer)eResolveProxy(oldPrimitiveMappingContainer);
			if (primitiveMappingContainer != oldPrimitiveMappingContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER, oldPrimitiveMappingContainer, primitiveMappingContainer));
			}
		}
		return primitiveMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveMappingContainer basicGetPrimitiveMappingContainer() {
		return primitiveMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrimitiveMappingContainer(PrimitiveMappingContainer newPrimitiveMappingContainer) {
		PrimitiveMappingContainer oldPrimitiveMappingContainer = primitiveMappingContainer;
		primitiveMappingContainer = newPrimitiveMappingContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER, oldPrimitiveMappingContainer, primitiveMappingContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE:
				if (resolve) return getUmlPackage();
				return basicGetUmlPackage();
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE:
				if (resolve) return getEcorePackage();
				return basicGetEcorePackage();
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER:
				if (resolve) return getPrimitiveMappingContainer();
				return basicGetPrimitiveMappingContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE:
				setUmlPackage((org.eclipse.uml2.uml.Package)newValue);
				return;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE:
				setEcorePackage((EPackage)newValue);
				return;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER:
				setPrimitiveMappingContainer((PrimitiveMappingContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE:
				setUmlPackage((org.eclipse.uml2.uml.Package)null);
				return;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE:
				setEcorePackage((EPackage)null);
				return;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER:
				setPrimitiveMappingContainer((PrimitiveMappingContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__UML_PACKAGE:
				return umlPackage != null;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__ECORE_PACKAGE:
				return ecorePackage != null;
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE__PRIMITIVE_MAPPING_CONTAINER:
				return primitiveMappingContainer != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlPackage2EPackageImpl
