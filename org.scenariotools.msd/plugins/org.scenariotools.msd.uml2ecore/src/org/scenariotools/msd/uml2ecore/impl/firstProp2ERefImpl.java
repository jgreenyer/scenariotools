/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.firstProp2ERef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>first Prop2 ERef</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl#getFirstEnd <em>First End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.firstProp2ERefImpl#getSecondERef <em>Second ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class firstProp2ERefImpl extends AbstractContainerCorrespondenceNodeImpl implements firstProp2ERef {
	/**
	 * The cached value of the '{@link #getFirstEnd() <em>First End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstEnd()
	 * @generated
	 * @ordered
	 */
	protected Property firstEnd;

	/**
	 * The cached value of the '{@link #getSecondERef() <em>Second ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondERef()
	 * @generated
	 * @ordered
	 */
	protected EReference secondERef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected firstProp2ERefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.FIRST_PROP2_EREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getFirstEnd() {
		if (firstEnd != null && firstEnd.eIsProxy()) {
			InternalEObject oldFirstEnd = (InternalEObject)firstEnd;
			firstEnd = (Property)eResolveProxy(oldFirstEnd);
			if (firstEnd != oldFirstEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END, oldFirstEnd, firstEnd));
			}
		}
		return firstEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetFirstEnd() {
		return firstEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstEnd(Property newFirstEnd) {
		Property oldFirstEnd = firstEnd;
		firstEnd = newFirstEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END, oldFirstEnd, firstEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecondERef() {
		if (secondERef != null && secondERef.eIsProxy()) {
			InternalEObject oldSecondERef = (InternalEObject)secondERef;
			secondERef = (EReference)eResolveProxy(oldSecondERef);
			if (secondERef != oldSecondERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF, oldSecondERef, secondERef));
			}
		}
		return secondERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetSecondERef() {
		return secondERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecondERef(EReference newSecondERef) {
		EReference oldSecondERef = secondERef;
		secondERef = newSecondERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF, oldSecondERef, secondERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END:
				if (resolve) return getFirstEnd();
				return basicGetFirstEnd();
			case Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF:
				if (resolve) return getSecondERef();
				return basicGetSecondERef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END:
				setFirstEnd((Property)newValue);
				return;
			case Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF:
				setSecondERef((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END:
				setFirstEnd((Property)null);
				return;
			case Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF:
				setSecondERef((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.FIRST_PROP2_EREF__FIRST_END:
				return firstEnd != null;
			case Uml2ecorePackage.FIRST_PROP2_EREF__SECOND_EREF:
				return secondERef != null;
		}
		return super.eIsSet(featureID);
	}

} //firstProp2ERefImpl
