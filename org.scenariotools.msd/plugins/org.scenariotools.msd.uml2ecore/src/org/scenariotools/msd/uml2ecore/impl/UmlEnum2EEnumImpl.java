/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Enumeration;

import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlEnum2EEnum;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Enum2 EEnum</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl#getUmlenum <em>Umlenum</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlEnum2EEnumImpl#getEenum <em>Eenum</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlEnum2EEnumImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlEnum2EEnum {
	/**
	 * The cached value of the '{@link #getUmlenum() <em>Umlenum</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlenum()
	 * @generated
	 * @ordered
	 */
	protected Enumeration umlenum;

	/**
	 * The cached value of the '{@link #getEenum() <em>Eenum</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEenum()
	 * @generated
	 * @ordered
	 */
	protected EEnum eenum;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlEnum2EEnumImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_ENUM2_EENUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration getUmlenum() {
		if (umlenum != null && umlenum.eIsProxy()) {
			InternalEObject oldUmlenum = (InternalEObject)umlenum;
			umlenum = (Enumeration)eResolveProxy(oldUmlenum);
			if (umlenum != oldUmlenum) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM, oldUmlenum, umlenum));
			}
		}
		return umlenum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration basicGetUmlenum() {
		return umlenum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlenum(Enumeration newUmlenum) {
		Enumeration oldUmlenum = umlenum;
		umlenum = newUmlenum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM, oldUmlenum, umlenum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEenum() {
		if (eenum != null && eenum.eIsProxy()) {
			InternalEObject oldEenum = (InternalEObject)eenum;
			eenum = (EEnum)eResolveProxy(oldEenum);
			if (eenum != oldEenum) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ENUM2_EENUM__EENUM, oldEenum, eenum));
			}
		}
		return eenum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum basicGetEenum() {
		return eenum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEenum(EEnum newEenum) {
		EEnum oldEenum = eenum;
		eenum = newEenum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ENUM2_EENUM__EENUM, oldEenum, eenum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM:
				if (resolve) return getUmlenum();
				return basicGetUmlenum();
			case Uml2ecorePackage.UML_ENUM2_EENUM__EENUM:
				if (resolve) return getEenum();
				return basicGetEenum();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM:
				setUmlenum((Enumeration)newValue);
				return;
			case Uml2ecorePackage.UML_ENUM2_EENUM__EENUM:
				setEenum((EEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM:
				setUmlenum((Enumeration)null);
				return;
			case Uml2ecorePackage.UML_ENUM2_EENUM__EENUM:
				setEenum((EEnum)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM2_EENUM__UMLENUM:
				return umlenum != null;
			case Uml2ecorePackage.UML_ENUM2_EENUM__EENUM:
				return eenum != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlEnum2EEnumImpl
