/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sec Prop2 Self ERef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getSecondEnd <em>Second End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getFirstERef <em>First ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSecProp2SelfERef()
 * @model
 * @generated
 */
public interface SecProp2SelfERef extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second End</em>' reference.
	 * @see #setSecondEnd(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSecProp2SelfERef_SecondEnd()
	 * @model
	 * @generated
	 */
	Property getSecondEnd();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getSecondEnd <em>Second End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second End</em>' reference.
	 * @see #getSecondEnd()
	 * @generated
	 */
	void setSecondEnd(Property value);

	/**
	 * Returns the value of the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First ERef</em>' reference.
	 * @see #setFirstERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getSecProp2SelfERef_FirstERef()
	 * @model
	 * @generated
	 */
	EReference getFirstERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef#getFirstERef <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First ERef</em>' reference.
	 * @see #getFirstERef()
	 * @generated
	 */
	void setFirstERef(EReference value);

} // SecProp2SelfERef
