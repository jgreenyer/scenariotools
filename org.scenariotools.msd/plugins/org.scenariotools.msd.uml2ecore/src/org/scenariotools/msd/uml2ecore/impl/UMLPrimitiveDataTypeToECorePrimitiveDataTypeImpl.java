/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.DataType;
import org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UML Primitive Data Type To ECore Primitive Data Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl#getEDataType <em>EData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl extends AbstractContainerCorrespondenceNodeImpl implements UMLPrimitiveDataTypeToECorePrimitiveDataType {
	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataType dataType;

	/**
	 * The cached value of the '{@link #getEDataType() <em>EData Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEDataType()
	 * @generated
	 * @ordered
	 */
	protected EDataType eDataType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getDataType() {
		if (dataType != null && dataType.eIsProxy()) {
			InternalEObject oldDataType = (InternalEObject)dataType;
			dataType = (DataType)eResolveProxy(oldDataType);
			if (dataType != oldDataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE, oldDataType, dataType));
			}
		}
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(DataType newDataType) {
		DataType oldDataType = dataType;
		dataType = newDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE, oldDataType, dataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEDataType() {
		if (eDataType != null && eDataType.eIsProxy()) {
			InternalEObject oldEDataType = (InternalEObject)eDataType;
			eDataType = (EDataType)eResolveProxy(oldEDataType);
			if (eDataType != oldEDataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE, oldEDataType, eDataType));
			}
		}
		return eDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType basicGetEDataType() {
		return eDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEDataType(EDataType newEDataType) {
		EDataType oldEDataType = eDataType;
		eDataType = newEDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE, oldEDataType, eDataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE:
				if (resolve) return getDataType();
				return basicGetDataType();
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE:
				if (resolve) return getEDataType();
				return basicGetEDataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE:
				setDataType((DataType)newValue);
				return;
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE:
				setEDataType((EDataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE:
				setDataType((DataType)null);
				return;
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE:
				setEDataType((EDataType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__DATA_TYPE:
				return dataType != null;
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE__EDATA_TYPE:
				return eDataType != null;
		}
		return super.eIsSet(featureID);
	}

} //UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl
