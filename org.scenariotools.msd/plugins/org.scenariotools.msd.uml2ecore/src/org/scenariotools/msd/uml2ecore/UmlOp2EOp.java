/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EOperation;
import org.eclipse.uml2.uml.Operation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Op2 EOp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getUmlop <em>Umlop</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getEOperation <em>EOperation</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlOp2EOp()
 * @model
 * @generated
 */
public interface UmlOp2EOp extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umlop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umlop</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umlop</em>' reference.
	 * @see #setUmlop(Operation)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlOp2EOp_Umlop()
	 * @model
	 * @generated
	 */
	Operation getUmlop();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getUmlop <em>Umlop</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umlop</em>' reference.
	 * @see #getUmlop()
	 * @generated
	 */
	void setUmlop(Operation value);

	/**
	 * Returns the value of the '<em><b>EOperation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EOperation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EOperation</em>' reference.
	 * @see #setEOperation(EOperation)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlOp2EOp_EOperation()
	 * @model
	 * @generated
	 */
	EOperation getEOperation();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp#getEOperation <em>EOperation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EOperation</em>' reference.
	 * @see #getEOperation()
	 * @generated
	 */
	void setEOperation(EOperation value);

} // UmlOp2EOp
