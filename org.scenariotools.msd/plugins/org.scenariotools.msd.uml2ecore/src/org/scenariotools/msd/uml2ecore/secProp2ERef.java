/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>sec Prop2 ERef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getSecondEnd <em>Second End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getFirstERef <em>First ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getsecProp2ERef()
 * @model
 * @generated
 */
public interface secProp2ERef extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second End</em>' reference.
	 * @see #setSecondEnd(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getsecProp2ERef_SecondEnd()
	 * @model
	 * @generated
	 */
	Property getSecondEnd();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getSecondEnd <em>Second End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second End</em>' reference.
	 * @see #getSecondEnd()
	 * @generated
	 */
	void setSecondEnd(Property value);

	/**
	 * Returns the value of the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First ERef</em>' reference.
	 * @see #setFirstERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getsecProp2ERef_FirstERef()
	 * @model
	 * @generated
	 */
	EReference getFirstERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.secProp2ERef#getFirstERef <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First ERef</em>' reference.
	 * @see #getFirstERef()
	 * @generated
	 */
	void setFirstERef(EReference value);

} // secProp2ERef
