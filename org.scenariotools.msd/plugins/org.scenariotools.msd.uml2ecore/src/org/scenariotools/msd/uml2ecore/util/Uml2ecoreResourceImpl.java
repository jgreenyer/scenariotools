/**
 */
package org.scenariotools.msd.uml2ecore.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.uml2ecore.util.Uml2ecoreResourceFactoryImpl
 * @generated
 */
public class Uml2ecoreResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Uml2ecoreResourceImpl(URI uri) {
		super(uri);
	}

} //Uml2ecoreResourceImpl
