/**
 */
package org.scenariotools.msd.uml2ecore.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.scenariotools.msd.uml2ecore.*;
import org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode;
import org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef;
import org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef;
import org.scenariotools.msd.uml2ecore.FirstProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SecProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SelfAss2ERef;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType;
import org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;
import org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr;
import org.scenariotools.msd.uml2ecore.UmlGen2EInh;
import org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.UmlOp2EOp;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat;
import org.scenariotools.msd.uml2ecore.firstProp2ERef;
import org.scenariotools.msd.uml2ecore.secProp2ERef;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage
 * @generated
 */
public class Uml2ecoreSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Uml2ecorePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecoreSwitch() {
		if (modelPackage == null) {
			modelPackage = Uml2ecorePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE: {
				UmlPackage2EPackage umlPackage2EPackage = (UmlPackage2EPackage)theEObject;
				T result = caseUmlPackage2EPackage(umlPackage2EPackage);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlPackage2EPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.ABSTRACT_CONTAINER_CORRESPONDENCE_NODE: {
				AbstractContainerCorrespondenceNode abstractContainerCorrespondenceNode = (AbstractContainerCorrespondenceNode)theEObject;
				T result = caseAbstractContainerCorrespondenceNode(abstractContainerCorrespondenceNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_CLASS2_ECLASS: {
				UmlClass2EClass umlClass2EClass = (UmlClass2EClass)theEObject;
				T result = caseUmlClass2EClass(umlClass2EClass);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlClass2EClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.FIRST_PROP2_EREF: {
				firstProp2ERef firstProp2ERef = (firstProp2ERef)theEObject;
				T result = casefirstProp2ERef(firstProp2ERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(firstProp2ERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_ASSOZ2_EREFE: {
				UmlAssoz2ERefe umlAssoz2ERefe = (UmlAssoz2ERefe)theEObject;
				T result = caseUmlAssoz2ERefe(umlAssoz2ERefe);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlAssoz2ERefe);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.SEC_PROP2_EREF: {
				secProp2ERef secProp2ERef = (secProp2ERef)theEObject;
				T result = casesecProp2ERef(secProp2ERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(secProp2ERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_GEN2_EINH: {
				UmlGen2EInh umlGen2EInh = (UmlGen2EInh)theEObject;
				T result = caseUmlGen2EInh(umlGen2EInh);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlGen2EInh);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.FIRST_PROP2_SELF_EREF: {
				FirstProp2SelfERef firstProp2SelfERef = (FirstProp2SelfERef)theEObject;
				T result = caseFirstProp2SelfERef(firstProp2SelfERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(firstProp2SelfERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.SEC_PROP2_SELF_EREF: {
				SecProp2SelfERef secProp2SelfERef = (SecProp2SelfERef)theEObject;
				T result = caseSecProp2SelfERef(secProp2SelfERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(secProp2SelfERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.SELF_ASS2_EREF: {
				SelfAss2ERef selfAss2ERef = (SelfAss2ERef)theEObject;
				T result = caseSelfAss2ERef(selfAss2ERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(selfAss2ERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.DIRECTED_ASSOZ2_EREF: {
				DirectedAssoz2ERef directedAssoz2ERef = (DirectedAssoz2ERef)theEObject;
				T result = caseDirectedAssoz2ERef(directedAssoz2ERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(directedAssoz2ERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR: {
				UmlClassProp2EAttr umlClassProp2EAttr = (UmlClassProp2EAttr)theEObject;
				T result = caseUmlClassProp2EAttr(umlClassProp2EAttr);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlClassProp2EAttr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF: {
				DirectedSelfAss2ERef directedSelfAss2ERef = (DirectedSelfAss2ERef)theEObject;
				T result = caseDirectedSelfAss2ERef(directedSelfAss2ERef);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(directedSelfAss2ERef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE: {
				UMLPrimitiveDataTypeToECorePrimitiveDataType umlPrimitiveDataTypeToECorePrimitiveDataType = (UMLPrimitiveDataTypeToECorePrimitiveDataType)theEObject;
				T result = caseUMLPrimitiveDataTypeToECorePrimitiveDataType(umlPrimitiveDataTypeToECorePrimitiveDataType);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlPrimitiveDataTypeToECorePrimitiveDataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META: {
				UmlMetamodel2EcoreMeta umlMetamodel2EcoreMeta = (UmlMetamodel2EcoreMeta)theEObject;
				T result = caseUmlMetamodel2EcoreMeta(umlMetamodel2EcoreMeta);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlMetamodel2EcoreMeta);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META: {
				UMLSecMetaModel2EcoreMeta umlSecMetaModel2EcoreMeta = (UMLSecMetaModel2EcoreMeta)theEObject;
				T result = caseUMLSecMetaModel2EcoreMeta(umlSecMetaModel2EcoreMeta);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlSecMetaModel2EcoreMeta);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_OP2_EOP: {
				UmlOp2EOp umlOp2EOp = (UmlOp2EOp)theEObject;
				T result = caseUmlOp2EOp(umlOp2EOp);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlOp2EOp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM: {
				UMLOpParam2EParam umlOpParam2EParam = (UMLOpParam2EParam)theEObject;
				T result = caseUMLOpParam2EParam(umlOpParam2EParam);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlOpParam2EParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_SET_OP2_ESTRUCT_FEAT: {
				UmlSetOp2EStructFeat umlSetOp2EStructFeat = (UmlSetOp2EStructFeat)theEObject;
				T result = caseUmlSetOp2EStructFeat(umlSetOp2EStructFeat);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlSetOp2EStructFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_ENUM2_EENUM: {
				UmlEnum2EEnum umlEnum2EEnum = (UmlEnum2EEnum)theEObject;
				T result = caseUmlEnum2EEnum(umlEnum2EEnum);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlEnum2EEnum);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL: {
				UmlEnumLiteral2EEnumLiteral umlEnumLiteral2EEnumLiteral = (UmlEnumLiteral2EEnumLiteral)theEObject;
				T result = caseUmlEnumLiteral2EEnumLiteral(umlEnumLiteral2EEnumLiteral);
				if (result == null) result = caseAbstractContainerCorrespondenceNode(umlEnumLiteral2EEnumLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Package2 EPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Package2 EPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlPackage2EPackage(UmlPackage2EPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Container Correspondence Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Container Correspondence Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractContainerCorrespondenceNode(AbstractContainerCorrespondenceNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Class2 EClass</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Class2 EClass</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlClass2EClass(UmlClass2EClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>first Prop2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>first Prop2 ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefirstProp2ERef(firstProp2ERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Assoz2 ERefe</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Assoz2 ERefe</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlAssoz2ERefe(UmlAssoz2ERefe object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>sec Prop2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>sec Prop2 ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesecProp2ERef(secProp2ERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Gen2 EInh</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Gen2 EInh</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlGen2EInh(UmlGen2EInh object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>First Prop2 Self ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>First Prop2 Self ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFirstProp2SelfERef(FirstProp2SelfERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sec Prop2 Self ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sec Prop2 Self ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecProp2SelfERef(SecProp2SelfERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Self Ass2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Self Ass2 ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelfAss2ERef(SelfAss2ERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Directed Assoz2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Directed Assoz2 ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDirectedAssoz2ERef(DirectedAssoz2ERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Class Prop2 EAttr</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Class Prop2 EAttr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlClassProp2EAttr(UmlClassProp2EAttr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Directed Self Ass2 ERef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Directed Self Ass2 ERef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDirectedSelfAss2ERef(DirectedSelfAss2ERef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Primitive Data Type To ECore Primitive Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Primitive Data Type To ECore Primitive Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLPrimitiveDataTypeToECorePrimitiveDataType(UMLPrimitiveDataTypeToECorePrimitiveDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Metamodel2 Ecore Meta</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Metamodel2 Ecore Meta</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlMetamodel2EcoreMeta(UmlMetamodel2EcoreMeta object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Sec Meta Model2 Ecore Meta</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Sec Meta Model2 Ecore Meta</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLSecMetaModel2EcoreMeta(UMLSecMetaModel2EcoreMeta object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Op2 EOp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Op2 EOp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlOp2EOp(UmlOp2EOp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UML Op Param2 EParam</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UML Op Param2 EParam</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMLOpParam2EParam(UMLOpParam2EParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Set Op2 EStruct Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Set Op2 EStruct Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlSetOp2EStructFeat(UmlSetOp2EStructFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Enum2 EEnum</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Enum2 EEnum</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlEnum2EEnum(UmlEnum2EEnum object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uml Enum Literal2 EEnum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uml Enum Literal2 EEnum Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUmlEnumLiteral2EEnumLiteral(UmlEnumLiteral2EEnumLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Uml2ecoreSwitch
