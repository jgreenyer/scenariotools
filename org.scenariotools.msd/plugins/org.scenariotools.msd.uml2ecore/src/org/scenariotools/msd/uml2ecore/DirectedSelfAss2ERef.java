/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Directed Self Ass2 ERef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstEnd <em>First End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstERef <em>First ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getSecondEnd <em>Second End</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getDirectedSelfAss2ERef()
 * @model
 * @generated
 */
public interface DirectedSelfAss2ERef extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umla</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umla</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umla</em>' reference.
	 * @see #setUmla(Association)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getDirectedSelfAss2ERef_Umla()
	 * @model
	 * @generated
	 */
	Association getUmla();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getUmla <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umla</em>' reference.
	 * @see #getUmla()
	 * @generated
	 */
	void setUmla(Association value);

	/**
	 * Returns the value of the '<em><b>First End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First End</em>' reference.
	 * @see #setFirstEnd(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getDirectedSelfAss2ERef_FirstEnd()
	 * @model
	 * @generated
	 */
	Property getFirstEnd();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstEnd <em>First End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First End</em>' reference.
	 * @see #getFirstEnd()
	 * @generated
	 */
	void setFirstEnd(Property value);

	/**
	 * Returns the value of the '<em><b>First ERef</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First ERef</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First ERef</em>' reference.
	 * @see #setFirstERef(EReference)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getDirectedSelfAss2ERef_FirstERef()
	 * @model
	 * @generated
	 */
	EReference getFirstERef();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getFirstERef <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First ERef</em>' reference.
	 * @see #getFirstERef()
	 * @generated
	 */
	void setFirstERef(EReference value);

	/**
	 * Returns the value of the '<em><b>Second End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second End</em>' reference.
	 * @see #setSecondEnd(Property)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getDirectedSelfAss2ERef_SecondEnd()
	 * @model
	 * @generated
	 */
	Property getSecondEnd();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef#getSecondEnd <em>Second End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second End</em>' reference.
	 * @see #getSecondEnd()
	 * @generated
	 */
	void setSecondEnd(Property value);

} // DirectedSelfAss2ERef
