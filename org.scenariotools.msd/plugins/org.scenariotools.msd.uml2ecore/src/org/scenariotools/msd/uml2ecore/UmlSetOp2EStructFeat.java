/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.uml.Operation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Set Op2 EStruct Feat</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getUmlop <em>Umlop</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getEStructuralFeature <em>EStructural Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlSetOp2EStructFeat()
 * @model
 * @generated
 */
public interface UmlSetOp2EStructFeat extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umlop</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umlop</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umlop</em>' reference.
	 * @see #setUmlop(Operation)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlSetOp2EStructFeat_Umlop()
	 * @model
	 * @generated
	 */
	Operation getUmlop();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getUmlop <em>Umlop</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umlop</em>' reference.
	 * @see #getUmlop()
	 * @generated
	 */
	void setUmlop(Operation value);

	/**
	 * Returns the value of the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EStructural Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EStructural Feature</em>' reference.
	 * @see #setEStructuralFeature(EStructuralFeature)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlSetOp2EStructFeat_EStructuralFeature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat#getEStructuralFeature <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EStructural Feature</em>' reference.
	 * @see #getEStructuralFeature()
	 * @generated
	 */
	void setEStructuralFeature(EStructuralFeature value);

} // UmlSetOp2EStructFeat
