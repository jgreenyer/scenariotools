/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.secProp2ERef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>sec Prop2 ERef</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl#getSecondEnd <em>Second End</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.secProp2ERefImpl#getFirstERef <em>First ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class secProp2ERefImpl extends AbstractContainerCorrespondenceNodeImpl implements secProp2ERef {
	/**
	 * The cached value of the '{@link #getSecondEnd() <em>Second End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondEnd()
	 * @generated
	 * @ordered
	 */
	protected Property secondEnd;

	/**
	 * The cached value of the '{@link #getFirstERef() <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstERef()
	 * @generated
	 * @ordered
	 */
	protected EReference firstERef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected secProp2ERefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.SEC_PROP2_EREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getSecondEnd() {
		if (secondEnd != null && secondEnd.eIsProxy()) {
			InternalEObject oldSecondEnd = (InternalEObject)secondEnd;
			secondEnd = (Property)eResolveProxy(oldSecondEnd);
			if (secondEnd != oldSecondEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END, oldSecondEnd, secondEnd));
			}
		}
		return secondEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetSecondEnd() {
		return secondEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecondEnd(Property newSecondEnd) {
		Property oldSecondEnd = secondEnd;
		secondEnd = newSecondEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END, oldSecondEnd, secondEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstERef() {
		if (firstERef != null && firstERef.eIsProxy()) {
			InternalEObject oldFirstERef = (InternalEObject)firstERef;
			firstERef = (EReference)eResolveProxy(oldFirstERef);
			if (firstERef != oldFirstERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF, oldFirstERef, firstERef));
			}
		}
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetFirstERef() {
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstERef(EReference newFirstERef) {
		EReference oldFirstERef = firstERef;
		firstERef = newFirstERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF, oldFirstERef, firstERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END:
				if (resolve) return getSecondEnd();
				return basicGetSecondEnd();
			case Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF:
				if (resolve) return getFirstERef();
				return basicGetFirstERef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END:
				setSecondEnd((Property)newValue);
				return;
			case Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF:
				setFirstERef((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END:
				setSecondEnd((Property)null);
				return;
			case Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF:
				setFirstERef((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.SEC_PROP2_EREF__SECOND_END:
				return secondEnd != null;
			case Uml2ecorePackage.SEC_PROP2_EREF__FIRST_EREF:
				return firstERef != null;
		}
		return super.eIsSet(featureID);
	}

} //secProp2ERefImpl
