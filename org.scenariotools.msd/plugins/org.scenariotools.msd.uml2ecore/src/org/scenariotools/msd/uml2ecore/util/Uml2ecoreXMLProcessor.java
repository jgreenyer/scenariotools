/**
 */
package org.scenariotools.msd.uml2ecore.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Uml2ecoreXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecoreXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		Uml2ecorePackage.eINSTANCE.eClass();
	}
	
	/**
	 * Register for "*" and "xml" file extensions the Uml2ecoreResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new Uml2ecoreResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new Uml2ecoreResourceFactoryImpl());
		}
		return registrations;
	}

} //Uml2ecoreXMLProcessor
