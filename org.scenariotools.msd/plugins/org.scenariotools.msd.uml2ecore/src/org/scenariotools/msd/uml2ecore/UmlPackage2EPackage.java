/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EPackage;
import org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Package2 EPackage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getUmlPackage <em>Uml Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getEcorePackage <em>Ecore Package</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getPrimitiveMappingContainer <em>Primitive Mapping Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlPackage2EPackage()
 * @model
 * @generated
 */
public interface UmlPackage2EPackage extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Uml Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Package</em>' reference.
	 * @see #setUmlPackage(org.eclipse.uml2.uml.Package)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlPackage2EPackage_UmlPackage()
	 * @model
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getUmlPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getUmlPackage <em>Uml Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml Package</em>' reference.
	 * @see #getUmlPackage()
	 * @generated
	 */
	void setUmlPackage(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Ecore Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ecore Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ecore Package</em>' reference.
	 * @see #setEcorePackage(EPackage)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlPackage2EPackage_EcorePackage()
	 * @model
	 * @generated
	 */
	EPackage getEcorePackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getEcorePackage <em>Ecore Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ecore Package</em>' reference.
	 * @see #getEcorePackage()
	 * @generated
	 */
	void setEcorePackage(EPackage value);

	/**
	 * Returns the value of the '<em><b>Primitive Mapping Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Mapping Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Mapping Container</em>' reference.
	 * @see #setPrimitiveMappingContainer(PrimitiveMappingContainer)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlPackage2EPackage_PrimitiveMappingContainer()
	 * @model
	 * @generated
	 */
	PrimitiveMappingContainer getPrimitiveMappingContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage#getPrimitiveMappingContainer <em>Primitive Mapping Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Primitive Mapping Container</em>' reference.
	 * @see #getPrimitiveMappingContainer()
	 * @generated
	 */
	void setPrimitiveMappingContainer(PrimitiveMappingContainer value);

} // UmlPackage2EPackage
