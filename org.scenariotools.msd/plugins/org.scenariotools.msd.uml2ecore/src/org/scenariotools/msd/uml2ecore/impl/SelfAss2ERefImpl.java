/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Association;
import org.scenariotools.msd.uml2ecore.SelfAss2ERef;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Self Ass2 ERef</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl#getFirstERef <em>First ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.SelfAss2ERefImpl#getSecondERef <em>Second ERef</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SelfAss2ERefImpl extends AbstractContainerCorrespondenceNodeImpl implements SelfAss2ERef {
	/**
	 * The cached value of the '{@link #getUmla() <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmla()
	 * @generated
	 * @ordered
	 */
	protected Association umla;

	/**
	 * The cached value of the '{@link #getFirstERef() <em>First ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstERef()
	 * @generated
	 * @ordered
	 */
	protected EReference firstERef;

	/**
	 * The cached value of the '{@link #getSecondERef() <em>Second ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondERef()
	 * @generated
	 * @ordered
	 */
	protected EReference secondERef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SelfAss2ERefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.SELF_ASS2_EREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getUmla() {
		if (umla != null && umla.eIsProxy()) {
			InternalEObject oldUmla = (InternalEObject)umla;
			umla = (Association)eResolveProxy(oldUmla);
			if (umla != oldUmla) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.SELF_ASS2_EREF__UMLA, oldUmla, umla));
			}
		}
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetUmla() {
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmla(Association newUmla) {
		Association oldUmla = umla;
		umla = newUmla;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.SELF_ASS2_EREF__UMLA, oldUmla, umla));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFirstERef() {
		if (firstERef != null && firstERef.eIsProxy()) {
			InternalEObject oldFirstERef = (InternalEObject)firstERef;
			firstERef = (EReference)eResolveProxy(oldFirstERef);
			if (firstERef != oldFirstERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF, oldFirstERef, firstERef));
			}
		}
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetFirstERef() {
		return firstERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstERef(EReference newFirstERef) {
		EReference oldFirstERef = firstERef;
		firstERef = newFirstERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF, oldFirstERef, firstERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecondERef() {
		if (secondERef != null && secondERef.eIsProxy()) {
			InternalEObject oldSecondERef = (InternalEObject)secondERef;
			secondERef = (EReference)eResolveProxy(oldSecondERef);
			if (secondERef != oldSecondERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF, oldSecondERef, secondERef));
			}
		}
		return secondERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetSecondERef() {
		return secondERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecondERef(EReference newSecondERef) {
		EReference oldSecondERef = secondERef;
		secondERef = newSecondERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF, oldSecondERef, secondERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.SELF_ASS2_EREF__UMLA:
				if (resolve) return getUmla();
				return basicGetUmla();
			case Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF:
				if (resolve) return getFirstERef();
				return basicGetFirstERef();
			case Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF:
				if (resolve) return getSecondERef();
				return basicGetSecondERef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.SELF_ASS2_EREF__UMLA:
				setUmla((Association)newValue);
				return;
			case Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF:
				setFirstERef((EReference)newValue);
				return;
			case Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF:
				setSecondERef((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.SELF_ASS2_EREF__UMLA:
				setUmla((Association)null);
				return;
			case Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF:
				setFirstERef((EReference)null);
				return;
			case Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF:
				setSecondERef((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.SELF_ASS2_EREF__UMLA:
				return umla != null;
			case Uml2ecorePackage.SELF_ASS2_EREF__FIRST_EREF:
				return firstERef != null;
			case Uml2ecorePackage.SELF_ASS2_EREF__SECOND_EREF:
				return secondERef != null;
		}
		return super.eIsSet(featureID);
	}

} //SelfAss2ERefImpl
