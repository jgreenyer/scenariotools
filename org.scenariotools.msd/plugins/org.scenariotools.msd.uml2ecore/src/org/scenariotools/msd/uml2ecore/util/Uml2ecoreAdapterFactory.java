/**
 */
package org.scenariotools.msd.uml2ecore.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.msd.uml2ecore.*;
import org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode;
import org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef;
import org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef;
import org.scenariotools.msd.uml2ecore.FirstProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SecProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SelfAss2ERef;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType;
import org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;
import org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr;
import org.scenariotools.msd.uml2ecore.UmlGen2EInh;
import org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.UmlOp2EOp;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat;
import org.scenariotools.msd.uml2ecore.firstProp2ERef;
import org.scenariotools.msd.uml2ecore.secProp2ERef;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage
 * @generated
 */
public class Uml2ecoreAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Uml2ecorePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecoreAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Uml2ecorePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Uml2ecoreSwitch<Adapter> modelSwitch =
		new Uml2ecoreSwitch<Adapter>() {
			@Override
			public Adapter caseUmlPackage2EPackage(UmlPackage2EPackage object) {
				return createUmlPackage2EPackageAdapter();
			}
			@Override
			public Adapter caseAbstractContainerCorrespondenceNode(AbstractContainerCorrespondenceNode object) {
				return createAbstractContainerCorrespondenceNodeAdapter();
			}
			@Override
			public Adapter caseUmlClass2EClass(UmlClass2EClass object) {
				return createUmlClass2EClassAdapter();
			}
			@Override
			public Adapter casefirstProp2ERef(firstProp2ERef object) {
				return createfirstProp2ERefAdapter();
			}
			@Override
			public Adapter caseUmlAssoz2ERefe(UmlAssoz2ERefe object) {
				return createUmlAssoz2ERefeAdapter();
			}
			@Override
			public Adapter casesecProp2ERef(secProp2ERef object) {
				return createsecProp2ERefAdapter();
			}
			@Override
			public Adapter caseUmlGen2EInh(UmlGen2EInh object) {
				return createUmlGen2EInhAdapter();
			}
			@Override
			public Adapter caseFirstProp2SelfERef(FirstProp2SelfERef object) {
				return createFirstProp2SelfERefAdapter();
			}
			@Override
			public Adapter caseSecProp2SelfERef(SecProp2SelfERef object) {
				return createSecProp2SelfERefAdapter();
			}
			@Override
			public Adapter caseSelfAss2ERef(SelfAss2ERef object) {
				return createSelfAss2ERefAdapter();
			}
			@Override
			public Adapter caseDirectedAssoz2ERef(DirectedAssoz2ERef object) {
				return createDirectedAssoz2ERefAdapter();
			}
			@Override
			public Adapter caseUmlClassProp2EAttr(UmlClassProp2EAttr object) {
				return createUmlClassProp2EAttrAdapter();
			}
			@Override
			public Adapter caseDirectedSelfAss2ERef(DirectedSelfAss2ERef object) {
				return createDirectedSelfAss2ERefAdapter();
			}
			@Override
			public Adapter caseUMLPrimitiveDataTypeToECorePrimitiveDataType(UMLPrimitiveDataTypeToECorePrimitiveDataType object) {
				return createUMLPrimitiveDataTypeToECorePrimitiveDataTypeAdapter();
			}
			@Override
			public Adapter caseUmlMetamodel2EcoreMeta(UmlMetamodel2EcoreMeta object) {
				return createUmlMetamodel2EcoreMetaAdapter();
			}
			@Override
			public Adapter caseUMLSecMetaModel2EcoreMeta(UMLSecMetaModel2EcoreMeta object) {
				return createUMLSecMetaModel2EcoreMetaAdapter();
			}
			@Override
			public Adapter caseUmlOp2EOp(UmlOp2EOp object) {
				return createUmlOp2EOpAdapter();
			}
			@Override
			public Adapter caseUMLOpParam2EParam(UMLOpParam2EParam object) {
				return createUMLOpParam2EParamAdapter();
			}
			@Override
			public Adapter caseUmlSetOp2EStructFeat(UmlSetOp2EStructFeat object) {
				return createUmlSetOp2EStructFeatAdapter();
			}
			@Override
			public Adapter caseUmlEnum2EEnum(UmlEnum2EEnum object) {
				return createUmlEnum2EEnumAdapter();
			}
			@Override
			public Adapter caseUmlEnumLiteral2EEnumLiteral(UmlEnumLiteral2EEnumLiteral object) {
				return createUmlEnumLiteral2EEnumLiteralAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlPackage2EPackage <em>Uml Package2 EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlPackage2EPackage
	 * @generated
	 */
	public Adapter createUmlPackage2EPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode <em>Abstract Container Correspondence Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.AbstractContainerCorrespondenceNode
	 * @generated
	 */
	public Adapter createAbstractContainerCorrespondenceNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass <em>Uml Class2 EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlClass2EClass
	 * @generated
	 */
	public Adapter createUmlClass2EClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.firstProp2ERef <em>first Prop2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.firstProp2ERef
	 * @generated
	 */
	public Adapter createfirstProp2ERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe <em>Uml Assoz2 ERefe</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe
	 * @generated
	 */
	public Adapter createUmlAssoz2ERefeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.secProp2ERef <em>sec Prop2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.secProp2ERef
	 * @generated
	 */
	public Adapter createsecProp2ERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlGen2EInh <em>Uml Gen2 EInh</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlGen2EInh
	 * @generated
	 */
	public Adapter createUmlGen2EInhAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.FirstProp2SelfERef <em>First Prop2 Self ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.FirstProp2SelfERef
	 * @generated
	 */
	public Adapter createFirstProp2SelfERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.SecProp2SelfERef <em>Sec Prop2 Self ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.SecProp2SelfERef
	 * @generated
	 */
	public Adapter createSecProp2SelfERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.SelfAss2ERef <em>Self Ass2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.SelfAss2ERef
	 * @generated
	 */
	public Adapter createSelfAss2ERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef <em>Directed Assoz2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef
	 * @generated
	 */
	public Adapter createDirectedAssoz2ERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr <em>Uml Class Prop2 EAttr</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr
	 * @generated
	 */
	public Adapter createUmlClassProp2EAttrAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef <em>Directed Self Ass2 ERef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef
	 * @generated
	 */
	public Adapter createDirectedSelfAss2ERefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType <em>UML Primitive Data Type To ECore Primitive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType
	 * @generated
	 */
	public Adapter createUMLPrimitiveDataTypeToECorePrimitiveDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta <em>Uml Metamodel2 Ecore Meta</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta
	 * @generated
	 */
	public Adapter createUmlMetamodel2EcoreMetaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta <em>UML Sec Meta Model2 Ecore Meta</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta
	 * @generated
	 */
	public Adapter createUMLSecMetaModel2EcoreMetaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlOp2EOp <em>Uml Op2 EOp</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlOp2EOp
	 * @generated
	 */
	public Adapter createUmlOp2EOpAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UMLOpParam2EParam <em>UML Op Param2 EParam</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UMLOpParam2EParam
	 * @generated
	 */
	public Adapter createUMLOpParam2EParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat <em>Uml Set Op2 EStruct Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat
	 * @generated
	 */
	public Adapter createUmlSetOp2EStructFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlEnum2EEnum <em>Uml Enum2 EEnum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnum2EEnum
	 * @generated
	 */
	public Adapter createUmlEnum2EEnumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral <em>Uml Enum Literal2 EEnum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral
	 * @generated
	 */
	public Adapter createUmlEnumLiteral2EEnumLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Uml2ecoreAdapterFactory
