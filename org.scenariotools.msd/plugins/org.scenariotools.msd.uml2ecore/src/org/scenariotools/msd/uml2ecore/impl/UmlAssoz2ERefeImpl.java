/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Association;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Assoz2 ERefe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl#getUmla <em>Umla</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl#getAss2ERef <em>Ass2 ERef</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlAssoz2ERefeImpl#getAss2Ref2 <em>Ass2 Ref2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlAssoz2ERefeImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlAssoz2ERefe {
	/**
	 * The cached value of the '{@link #getUmla() <em>Umla</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmla()
	 * @generated
	 * @ordered
	 */
	protected Association umla;

	/**
	 * The cached value of the '{@link #getAss2ERef() <em>Ass2 ERef</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAss2ERef()
	 * @generated
	 * @ordered
	 */
	protected EReference ass2ERef;

	/**
	 * The cached value of the '{@link #getAss2Ref2() <em>Ass2 Ref2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAss2Ref2()
	 * @generated
	 * @ordered
	 */
	protected EReference ass2Ref2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlAssoz2ERefeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_ASSOZ2_EREFE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getUmla() {
		if (umla != null && umla.eIsProxy()) {
			InternalEObject oldUmla = (InternalEObject)umla;
			umla = (Association)eResolveProxy(oldUmla);
			if (umla != oldUmla) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA, oldUmla, umla));
			}
		}
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetUmla() {
		return umla;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmla(Association newUmla) {
		Association oldUmla = umla;
		umla = newUmla;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA, oldUmla, umla));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAss2ERef() {
		if (ass2ERef != null && ass2ERef.eIsProxy()) {
			InternalEObject oldAss2ERef = (InternalEObject)ass2ERef;
			ass2ERef = (EReference)eResolveProxy(oldAss2ERef);
			if (ass2ERef != oldAss2ERef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF, oldAss2ERef, ass2ERef));
			}
		}
		return ass2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetAss2ERef() {
		return ass2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAss2ERef(EReference newAss2ERef) {
		EReference oldAss2ERef = ass2ERef;
		ass2ERef = newAss2ERef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF, oldAss2ERef, ass2ERef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAss2Ref2() {
		if (ass2Ref2 != null && ass2Ref2.eIsProxy()) {
			InternalEObject oldAss2Ref2 = (InternalEObject)ass2Ref2;
			ass2Ref2 = (EReference)eResolveProxy(oldAss2Ref2);
			if (ass2Ref2 != oldAss2Ref2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2, oldAss2Ref2, ass2Ref2));
			}
		}
		return ass2Ref2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetAss2Ref2() {
		return ass2Ref2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAss2Ref2(EReference newAss2Ref2) {
		EReference oldAss2Ref2 = ass2Ref2;
		ass2Ref2 = newAss2Ref2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2, oldAss2Ref2, ass2Ref2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA:
				if (resolve) return getUmla();
				return basicGetUmla();
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF:
				if (resolve) return getAss2ERef();
				return basicGetAss2ERef();
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2:
				if (resolve) return getAss2Ref2();
				return basicGetAss2Ref2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA:
				setUmla((Association)newValue);
				return;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF:
				setAss2ERef((EReference)newValue);
				return;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2:
				setAss2Ref2((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA:
				setUmla((Association)null);
				return;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF:
				setAss2ERef((EReference)null);
				return;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2:
				setAss2Ref2((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__UMLA:
				return umla != null;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_EREF:
				return ass2ERef != null;
			case Uml2ecorePackage.UML_ASSOZ2_EREFE__ASS2_REF2:
				return ass2Ref2 != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlAssoz2ERefeImpl
