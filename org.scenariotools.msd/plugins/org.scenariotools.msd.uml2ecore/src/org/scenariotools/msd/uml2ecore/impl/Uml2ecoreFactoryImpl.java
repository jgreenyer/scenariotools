/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.msd.uml2ecore.*;
import org.scenariotools.msd.uml2ecore.DirectedAssoz2ERef;
import org.scenariotools.msd.uml2ecore.DirectedSelfAss2ERef;
import org.scenariotools.msd.uml2ecore.FirstProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SecProp2SelfERef;
import org.scenariotools.msd.uml2ecore.SelfAss2ERef;
import org.scenariotools.msd.uml2ecore.UMLOpParam2EParam;
import org.scenariotools.msd.uml2ecore.UMLPrimitiveDataTypeToECorePrimitiveDataType;
import org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.Uml2ecoreFactory;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlAssoz2ERefe;
import org.scenariotools.msd.uml2ecore.UmlClass2EClass;
import org.scenariotools.msd.uml2ecore.UmlClassProp2EAttr;
import org.scenariotools.msd.uml2ecore.UmlGen2EInh;
import org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta;
import org.scenariotools.msd.uml2ecore.UmlOp2EOp;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.UmlSetOp2EStructFeat;
import org.scenariotools.msd.uml2ecore.firstProp2ERef;
import org.scenariotools.msd.uml2ecore.secProp2ERef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Uml2ecoreFactoryImpl extends EFactoryImpl implements Uml2ecoreFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Uml2ecoreFactory init() {
		try {
			Uml2ecoreFactory theUml2ecoreFactory = (Uml2ecoreFactory)EPackage.Registry.INSTANCE.getEFactory(Uml2ecorePackage.eNS_URI);
			if (theUml2ecoreFactory != null) {
				return theUml2ecoreFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Uml2ecoreFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecoreFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Uml2ecorePackage.UML_PACKAGE2_EPACKAGE: return createUmlPackage2EPackage();
			case Uml2ecorePackage.UML_CLASS2_ECLASS: return createUmlClass2EClass();
			case Uml2ecorePackage.FIRST_PROP2_EREF: return createfirstProp2ERef();
			case Uml2ecorePackage.UML_ASSOZ2_EREFE: return createUmlAssoz2ERefe();
			case Uml2ecorePackage.SEC_PROP2_EREF: return createsecProp2ERef();
			case Uml2ecorePackage.UML_GEN2_EINH: return createUmlGen2EInh();
			case Uml2ecorePackage.FIRST_PROP2_SELF_EREF: return createFirstProp2SelfERef();
			case Uml2ecorePackage.SEC_PROP2_SELF_EREF: return createSecProp2SelfERef();
			case Uml2ecorePackage.SELF_ASS2_EREF: return createSelfAss2ERef();
			case Uml2ecorePackage.DIRECTED_ASSOZ2_EREF: return createDirectedAssoz2ERef();
			case Uml2ecorePackage.UML_CLASS_PROP2_EATTR: return createUmlClassProp2EAttr();
			case Uml2ecorePackage.DIRECTED_SELF_ASS2_EREF: return createDirectedSelfAss2ERef();
			case Uml2ecorePackage.UML_PRIMITIVE_DATA_TYPE_TO_ECORE_PRIMITIVE_DATA_TYPE: return createUMLPrimitiveDataTypeToECorePrimitiveDataType();
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META: return createUmlMetamodel2EcoreMeta();
			case Uml2ecorePackage.UML_SEC_META_MODEL2_ECORE_META: return createUMLSecMetaModel2EcoreMeta();
			case Uml2ecorePackage.UML_OP2_EOP: return createUmlOp2EOp();
			case Uml2ecorePackage.UML_OP_PARAM2_EPARAM: return createUMLOpParam2EParam();
			case Uml2ecorePackage.UML_SET_OP2_ESTRUCT_FEAT: return createUmlSetOp2EStructFeat();
			case Uml2ecorePackage.UML_ENUM2_EENUM: return createUmlEnum2EEnum();
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL: return createUmlEnumLiteral2EEnumLiteral();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlPackage2EPackage createUmlPackage2EPackage() {
		UmlPackage2EPackageImpl umlPackage2EPackage = new UmlPackage2EPackageImpl();
		return umlPackage2EPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClass2EClass createUmlClass2EClass() {
		UmlClass2EClassImpl umlClass2EClass = new UmlClass2EClassImpl();
		return umlClass2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public firstProp2ERef createfirstProp2ERef() {
		firstProp2ERefImpl firstProp2ERef = new firstProp2ERefImpl();
		return firstProp2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlAssoz2ERefe createUmlAssoz2ERefe() {
		UmlAssoz2ERefeImpl umlAssoz2ERefe = new UmlAssoz2ERefeImpl();
		return umlAssoz2ERefe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public secProp2ERef createsecProp2ERef() {
		secProp2ERefImpl secProp2ERef = new secProp2ERefImpl();
		return secProp2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlGen2EInh createUmlGen2EInh() {
		UmlGen2EInhImpl umlGen2EInh = new UmlGen2EInhImpl();
		return umlGen2EInh;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstProp2SelfERef createFirstProp2SelfERef() {
		FirstProp2SelfERefImpl firstProp2SelfERef = new FirstProp2SelfERefImpl();
		return firstProp2SelfERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecProp2SelfERef createSecProp2SelfERef() {
		SecProp2SelfERefImpl secProp2SelfERef = new SecProp2SelfERefImpl();
		return secProp2SelfERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelfAss2ERef createSelfAss2ERef() {
		SelfAss2ERefImpl selfAss2ERef = new SelfAss2ERefImpl();
		return selfAss2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectedAssoz2ERef createDirectedAssoz2ERef() {
		DirectedAssoz2ERefImpl directedAssoz2ERef = new DirectedAssoz2ERefImpl();
		return directedAssoz2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassProp2EAttr createUmlClassProp2EAttr() {
		UmlClassProp2EAttrImpl umlClassProp2EAttr = new UmlClassProp2EAttrImpl();
		return umlClassProp2EAttr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectedSelfAss2ERef createDirectedSelfAss2ERef() {
		DirectedSelfAss2ERefImpl directedSelfAss2ERef = new DirectedSelfAss2ERefImpl();
		return directedSelfAss2ERef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UMLPrimitiveDataTypeToECorePrimitiveDataType createUMLPrimitiveDataTypeToECorePrimitiveDataType() {
		UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl umlPrimitiveDataTypeToECorePrimitiveDataType = new UMLPrimitiveDataTypeToECorePrimitiveDataTypeImpl();
		return umlPrimitiveDataTypeToECorePrimitiveDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlMetamodel2EcoreMeta createUmlMetamodel2EcoreMeta() {
		UmlMetamodel2EcoreMetaImpl umlMetamodel2EcoreMeta = new UmlMetamodel2EcoreMetaImpl();
		return umlMetamodel2EcoreMeta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UMLSecMetaModel2EcoreMeta createUMLSecMetaModel2EcoreMeta() {
		UMLSecMetaModel2EcoreMetaImpl umlSecMetaModel2EcoreMeta = new UMLSecMetaModel2EcoreMetaImpl();
		return umlSecMetaModel2EcoreMeta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlOp2EOp createUmlOp2EOp() {
		UmlOp2EOpImpl umlOp2EOp = new UmlOp2EOpImpl();
		return umlOp2EOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UMLOpParam2EParam createUMLOpParam2EParam() {
		UMLOpParam2EParamImpl umlOpParam2EParam = new UMLOpParam2EParamImpl();
		return umlOpParam2EParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlSetOp2EStructFeat createUmlSetOp2EStructFeat() {
		UmlSetOp2EStructFeatImpl umlSetOp2EStructFeat = new UmlSetOp2EStructFeatImpl();
		return umlSetOp2EStructFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlEnum2EEnum createUmlEnum2EEnum() {
		UmlEnum2EEnumImpl umlEnum2EEnum = new UmlEnum2EEnumImpl();
		return umlEnum2EEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlEnumLiteral2EEnumLiteral createUmlEnumLiteral2EEnumLiteral() {
		UmlEnumLiteral2EEnumLiteralImpl umlEnumLiteral2EEnumLiteral = new UmlEnumLiteral2EEnumLiteralImpl();
		return umlEnumLiteral2EEnumLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml2ecorePackage getUml2ecorePackage() {
		return (Uml2ecorePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Uml2ecorePackage getPackage() {
		return Uml2ecorePackage.eINSTANCE;
	}

} //Uml2ecoreFactoryImpl
