/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Model;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlMetamodel2EcoreMeta;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Metamodel2 Ecore Meta</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl#getUmlMetamodel <em>Uml Metamodel</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlMetamodel2EcoreMetaImpl#getMEcorePack <em>MEcore Pack</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlMetamodel2EcoreMetaImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlMetamodel2EcoreMeta {
	/**
	 * The cached value of the '{@link #getUmlMetamodel() <em>Uml Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlMetamodel()
	 * @generated
	 * @ordered
	 */
	protected Model umlMetamodel;

	/**
	 * The cached value of the '{@link #getMEcorePack() <em>MEcore Pack</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMEcorePack()
	 * @generated
	 * @ordered
	 */
	protected EPackage mEcorePack;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlMetamodel2EcoreMetaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_METAMODEL2_ECORE_META;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getUmlMetamodel() {
		if (umlMetamodel != null && umlMetamodel.eIsProxy()) {
			InternalEObject oldUmlMetamodel = (InternalEObject)umlMetamodel;
			umlMetamodel = (Model)eResolveProxy(oldUmlMetamodel);
			if (umlMetamodel != oldUmlMetamodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL, oldUmlMetamodel, umlMetamodel));
			}
		}
		return umlMetamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model basicGetUmlMetamodel() {
		return umlMetamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlMetamodel(Model newUmlMetamodel) {
		Model oldUmlMetamodel = umlMetamodel;
		umlMetamodel = newUmlMetamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL, oldUmlMetamodel, umlMetamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getMEcorePack() {
		if (mEcorePack != null && mEcorePack.eIsProxy()) {
			InternalEObject oldMEcorePack = (InternalEObject)mEcorePack;
			mEcorePack = (EPackage)eResolveProxy(oldMEcorePack);
			if (mEcorePack != oldMEcorePack) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK, oldMEcorePack, mEcorePack));
			}
		}
		return mEcorePack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetMEcorePack() {
		return mEcorePack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMEcorePack(EPackage newMEcorePack) {
		EPackage oldMEcorePack = mEcorePack;
		mEcorePack = newMEcorePack;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK, oldMEcorePack, mEcorePack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL:
				if (resolve) return getUmlMetamodel();
				return basicGetUmlMetamodel();
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK:
				if (resolve) return getMEcorePack();
				return basicGetMEcorePack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL:
				setUmlMetamodel((Model)newValue);
				return;
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK:
				setMEcorePack((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL:
				setUmlMetamodel((Model)null);
				return;
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK:
				setMEcorePack((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__UML_METAMODEL:
				return umlMetamodel != null;
			case Uml2ecorePackage.UML_METAMODEL2_ECORE_META__MECORE_PACK:
				return mEcorePack != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlMetamodel2EcoreMetaImpl
