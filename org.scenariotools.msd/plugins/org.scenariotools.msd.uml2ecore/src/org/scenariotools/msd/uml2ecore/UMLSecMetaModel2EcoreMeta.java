/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.uml2.uml.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UML Sec Meta Model2 Ecore Meta</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getSecMetamodel <em>Sec Metamodel</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getEPackage <em>EPackage</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLSecMetaModel2EcoreMeta()
 * @model
 * @generated
 */
public interface UMLSecMetaModel2EcoreMeta extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Sec Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sec Metamodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sec Metamodel</em>' reference.
	 * @see #setSecMetamodel(Model)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLSecMetaModel2EcoreMeta_SecMetamodel()
	 * @model
	 * @generated
	 */
	Model getSecMetamodel();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getSecMetamodel <em>Sec Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sec Metamodel</em>' reference.
	 * @see #getSecMetamodel()
	 * @generated
	 */
	void setSecMetamodel(Model value);

	/**
	 * Returns the value of the '<em><b>EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EPackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EPackage</em>' reference.
	 * @see #setEPackage(EPackage)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUMLSecMetaModel2EcoreMeta_EPackage()
	 * @model
	 * @generated
	 */
	EPackage getEPackage();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UMLSecMetaModel2EcoreMeta#getEPackage <em>EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EPackage</em>' reference.
	 * @see #getEPackage()
	 * @generated
	 */
	void setEPackage(EPackage value);

} // UMLSecMetaModel2EcoreMeta
