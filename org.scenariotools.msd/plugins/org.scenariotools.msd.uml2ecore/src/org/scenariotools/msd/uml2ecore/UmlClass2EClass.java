/**
 */
package org.scenariotools.msd.uml2ecore;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uml Class2 EClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getUmlc <em>Umlc</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getEc <em>Ec</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClass2EClass()
 * @model
 * @generated
 */
public interface UmlClass2EClass extends AbstractContainerCorrespondenceNode {
	/**
	 * Returns the value of the '<em><b>Umlc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Umlc</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Umlc</em>' reference.
	 * @see #setUmlc(org.eclipse.uml2.uml.Class)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClass2EClass_Umlc()
	 * @model
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getUmlc();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getUmlc <em>Umlc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Umlc</em>' reference.
	 * @see #getUmlc()
	 * @generated
	 */
	void setUmlc(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Ec</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ec</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ec</em>' reference.
	 * @see #setEc(EClass)
	 * @see org.scenariotools.msd.uml2ecore.Uml2ecorePackage#getUmlClass2EClass_Ec()
	 * @model
	 * @generated
	 */
	EClass getEc();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.uml2ecore.UmlClass2EClass#getEc <em>Ec</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ec</em>' reference.
	 * @see #getEc()
	 * @generated
	 */
	void setEc(EClass value);

} // UmlClass2EClass
