/**
 */
package org.scenariotools.msd.uml2ecore.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.EnumerationLiteral;

import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.UmlEnumLiteral2EEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uml Enum Literal2 EEnum Literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl#getUELiteral <em>UE Literal</em>}</li>
 *   <li>{@link org.scenariotools.msd.uml2ecore.impl.UmlEnumLiteral2EEnumLiteralImpl#getEELiteral <em>EE Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UmlEnumLiteral2EEnumLiteralImpl extends AbstractContainerCorrespondenceNodeImpl implements UmlEnumLiteral2EEnumLiteral {
	/**
	 * The cached value of the '{@link #getUELiteral() <em>UE Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUELiteral()
	 * @generated
	 * @ordered
	 */
	protected EnumerationLiteral uELiteral;

	/**
	 * The cached value of the '{@link #getEELiteral() <em>EE Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEELiteral()
	 * @generated
	 * @ordered
	 */
	protected EEnumLiteral eELiteral;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlEnumLiteral2EEnumLiteralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Uml2ecorePackage.Literals.UML_ENUM_LITERAL2_EENUM_LITERAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral getUELiteral() {
		if (uELiteral != null && uELiteral.eIsProxy()) {
			InternalEObject oldUELiteral = (InternalEObject)uELiteral;
			uELiteral = (EnumerationLiteral)eResolveProxy(oldUELiteral);
			if (uELiteral != oldUELiteral) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL, oldUELiteral, uELiteral));
			}
		}
		return uELiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral basicGetUELiteral() {
		return uELiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUELiteral(EnumerationLiteral newUELiteral) {
		EnumerationLiteral oldUELiteral = uELiteral;
		uELiteral = newUELiteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL, oldUELiteral, uELiteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral getEELiteral() {
		if (eELiteral != null && eELiteral.eIsProxy()) {
			InternalEObject oldEELiteral = (InternalEObject)eELiteral;
			eELiteral = (EEnumLiteral)eResolveProxy(oldEELiteral);
			if (eELiteral != oldEELiteral) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL, oldEELiteral, eELiteral));
			}
		}
		return eELiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral basicGetEELiteral() {
		return eELiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEELiteral(EEnumLiteral newEELiteral) {
		EEnumLiteral oldEELiteral = eELiteral;
		eELiteral = newEELiteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL, oldEELiteral, eELiteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL:
				if (resolve) return getUELiteral();
				return basicGetUELiteral();
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL:
				if (resolve) return getEELiteral();
				return basicGetEELiteral();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL:
				setUELiteral((EnumerationLiteral)newValue);
				return;
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL:
				setEELiteral((EEnumLiteral)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL:
				setUELiteral((EnumerationLiteral)null);
				return;
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL:
				setEELiteral((EEnumLiteral)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__UE_LITERAL:
				return uELiteral != null;
			case Uml2ecorePackage.UML_ENUM_LITERAL2_EENUM_LITERAL__EE_LITERAL:
				return eELiteral != null;
		}
		return super.eIsSet(featureID);
	}

} //UmlEnumLiteral2EEnumLiteralImpl
