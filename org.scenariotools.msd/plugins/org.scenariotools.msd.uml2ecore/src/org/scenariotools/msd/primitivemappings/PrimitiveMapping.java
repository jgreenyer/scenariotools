/**
 */
package org.scenariotools.msd.primitivemappings;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.PrimitiveType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getUmlPrimitiveDataType <em>Uml Primitive Data Type</em>}</li>
 *   <li>{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getEDataType <em>EData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#getPrimitiveMapping()
 * @model
 * @generated
 */
public interface PrimitiveMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Uml Primitive Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uml Primitive Data Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uml Primitive Data Type</em>' reference.
	 * @see #setUmlPrimitiveDataType(PrimitiveType)
	 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#getPrimitiveMapping_UmlPrimitiveDataType()
	 * @model
	 * @generated
	 */
	PrimitiveType getUmlPrimitiveDataType();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getUmlPrimitiveDataType <em>Uml Primitive Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uml Primitive Data Type</em>' reference.
	 * @see #getUmlPrimitiveDataType()
	 * @generated
	 */
	void setUmlPrimitiveDataType(PrimitiveType value);

	/**
	 * Returns the value of the '<em><b>EData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EData Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EData Type</em>' reference.
	 * @see #setEDataType(EDataType)
	 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#getPrimitiveMapping_EDataType()
	 * @model
	 * @generated
	 */
	EDataType getEDataType();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getEDataType <em>EData Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EData Type</em>' reference.
	 * @see #getEDataType()
	 * @generated
	 */
	void setEDataType(EDataType value);

} // PrimitiveMapping
