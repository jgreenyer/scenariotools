/**
 */
package org.scenariotools.msd.primitivemappings.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.uml2.uml.PrimitiveType;
import org.scenariotools.msd.primitivemappings.PrimitiveMapping;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Primitive Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl#getUmlPrimitiveDataType <em>Uml Primitive Data Type</em>}</li>
 *   <li>{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl#getEDataType <em>EData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PrimitiveMappingImpl extends EObjectImpl implements PrimitiveMapping {
	/**
	 * The cached value of the '{@link #getUmlPrimitiveDataType() <em>Uml Primitive Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmlPrimitiveDataType()
	 * @generated
	 * @ordered
	 */
	protected PrimitiveType umlPrimitiveDataType;

	/**
	 * The cached value of the '{@link #getEDataType() <em>EData Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEDataType()
	 * @generated
	 * @ordered
	 */
	protected EDataType eDataType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrimitiveMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrimitivemappingsPackage.Literals.PRIMITIVE_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveType getUmlPrimitiveDataType() {
		if (umlPrimitiveDataType != null && umlPrimitiveDataType.eIsProxy()) {
			InternalEObject oldUmlPrimitiveDataType = (InternalEObject)umlPrimitiveDataType;
			umlPrimitiveDataType = (PrimitiveType)eResolveProxy(oldUmlPrimitiveDataType);
			if (umlPrimitiveDataType != oldUmlPrimitiveDataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE, oldUmlPrimitiveDataType, umlPrimitiveDataType));
			}
		}
		return umlPrimitiveDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveType basicGetUmlPrimitiveDataType() {
		return umlPrimitiveDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUmlPrimitiveDataType(PrimitiveType newUmlPrimitiveDataType) {
		PrimitiveType oldUmlPrimitiveDataType = umlPrimitiveDataType;
		umlPrimitiveDataType = newUmlPrimitiveDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE, oldUmlPrimitiveDataType, umlPrimitiveDataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEDataType() {
		if (eDataType != null && eDataType.eIsProxy()) {
			InternalEObject oldEDataType = (InternalEObject)eDataType;
			eDataType = (EDataType)eResolveProxy(oldEDataType);
			if (eDataType != oldEDataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE, oldEDataType, eDataType));
			}
		}
		return eDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType basicGetEDataType() {
		return eDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEDataType(EDataType newEDataType) {
		EDataType oldEDataType = eDataType;
		eDataType = newEDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE, oldEDataType, eDataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE:
				if (resolve) return getUmlPrimitiveDataType();
				return basicGetUmlPrimitiveDataType();
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE:
				if (resolve) return getEDataType();
				return basicGetEDataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE:
				setUmlPrimitiveDataType((PrimitiveType)newValue);
				return;
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE:
				setEDataType((EDataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE:
				setUmlPrimitiveDataType((PrimitiveType)null);
				return;
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE:
				setEDataType((EDataType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE:
				return umlPrimitiveDataType != null;
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING__EDATA_TYPE:
				return eDataType != null;
		}
		return super.eIsSet(featureID);
	}

} //PrimitiveMappingImpl
