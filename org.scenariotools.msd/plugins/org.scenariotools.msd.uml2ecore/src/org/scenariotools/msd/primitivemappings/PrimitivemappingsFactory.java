/**
 */
package org.scenariotools.msd.primitivemappings;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage
 * @generated
 */
public interface PrimitivemappingsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PrimitivemappingsFactory eINSTANCE = org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Primitive Mapping Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primitive Mapping Container</em>'.
	 * @generated
	 */
	PrimitiveMappingContainer createPrimitiveMappingContainer();

	/**
	 * Returns a new object of class '<em>Primitive Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primitive Mapping</em>'.
	 * @generated
	 */
	PrimitiveMapping createPrimitiveMapping();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PrimitivemappingsPackage getPrimitivemappingsPackage();

} //PrimitivemappingsFactory
