/**
 */
package org.scenariotools.msd.primitivemappings.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PrimitivemappingsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitivemappingsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		PrimitivemappingsPackage.eINSTANCE.eClass();
	}
	
	/**
	 * Register for "*" and "xml" file extensions the PrimitivemappingsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new PrimitivemappingsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new PrimitivemappingsResourceFactoryImpl());
		}
		return registrations;
	}

} //PrimitivemappingsXMLProcessor
