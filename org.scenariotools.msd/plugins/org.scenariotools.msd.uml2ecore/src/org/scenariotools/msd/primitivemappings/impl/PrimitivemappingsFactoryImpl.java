/**
 */
package org.scenariotools.msd.primitivemappings.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.msd.primitivemappings.*;
import org.scenariotools.msd.primitivemappings.PrimitiveMapping;
import org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsFactory;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PrimitivemappingsFactoryImpl extends EFactoryImpl implements PrimitivemappingsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PrimitivemappingsFactory init() {
		try {
			PrimitivemappingsFactory thePrimitivemappingsFactory = (PrimitivemappingsFactory)EPackage.Registry.INSTANCE.getEFactory("http://org.scenariotools.swt.uml2ecore.primitivemappings"); 
			if (thePrimitivemappingsFactory != null) {
				return thePrimitivemappingsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PrimitivemappingsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitivemappingsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER: return createPrimitiveMappingContainer();
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING: return createPrimitiveMapping();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveMappingContainer createPrimitiveMappingContainer() {
		PrimitiveMappingContainerImpl primitiveMappingContainer = new PrimitiveMappingContainerImpl();
		return primitiveMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveMapping createPrimitiveMapping() {
		PrimitiveMappingImpl primitiveMapping = new PrimitiveMappingImpl();
		return primitiveMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitivemappingsPackage getPrimitivemappingsPackage() {
		return (PrimitivemappingsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PrimitivemappingsPackage getPackage() {
		return PrimitivemappingsPackage.eINSTANCE;
	}

} //PrimitivemappingsFactoryImpl
