/**
 */
package org.scenariotools.msd.primitivemappings;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Mapping Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer#getPrimitiveMapping <em>Primitive Mapping</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#getPrimitiveMappingContainer()
 * @model
 * @generated
 */
public interface PrimitiveMappingContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Primitive Mapping</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.primitivemappings.PrimitiveMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Mapping</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Mapping</em>' containment reference list.
	 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#getPrimitiveMappingContainer_PrimitiveMapping()
	 * @model containment="true"
	 * @generated
	 */
	EList<PrimitiveMapping> getPrimitiveMapping();

} // PrimitiveMappingContainer
