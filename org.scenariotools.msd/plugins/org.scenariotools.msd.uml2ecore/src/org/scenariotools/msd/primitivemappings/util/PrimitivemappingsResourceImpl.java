/**
 */
package org.scenariotools.msd.primitivemappings.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.primitivemappings.util.PrimitivemappingsResourceFactoryImpl
 * @generated
 */
public class PrimitivemappingsResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public PrimitivemappingsResourceImpl(URI uri) {
		super(uri);
	}

} //PrimitivemappingsResourceImpl
