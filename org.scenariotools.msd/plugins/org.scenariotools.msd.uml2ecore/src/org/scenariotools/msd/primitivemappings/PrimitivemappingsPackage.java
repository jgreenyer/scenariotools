/**
 */
package org.scenariotools.msd.primitivemappings;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsFactory
 * @model kind="package"
 * @generated
 */
public interface PrimitivemappingsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "primitivemappings";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.swt.uml2ecore.primitivemappings";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.scenariotools.swt.uml2ecore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PrimitivemappingsPackage eINSTANCE = org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingContainerImpl <em>Primitive Mapping Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingContainerImpl
	 * @see org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl#getPrimitiveMappingContainer()
	 * @generated
	 */
	int PRIMITIVE_MAPPING_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Primitive Mapping</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING = 0;

	/**
	 * The number of structural features of the '<em>Primitive Mapping Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_MAPPING_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl <em>Primitive Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl
	 * @see org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl#getPrimitiveMapping()
	 * @generated
	 */
	int PRIMITIVE_MAPPING = 1;

	/**
	 * The feature id for the '<em><b>Uml Primitive Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE = 0;

	/**
	 * The feature id for the '<em><b>EData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_MAPPING__EDATA_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Primitive Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_MAPPING_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer <em>Primitive Mapping Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Mapping Container</em>'.
	 * @see org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer
	 * @generated
	 */
	EClass getPrimitiveMappingContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer#getPrimitiveMapping <em>Primitive Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Primitive Mapping</em>'.
	 * @see org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer#getPrimitiveMapping()
	 * @see #getPrimitiveMappingContainer()
	 * @generated
	 */
	EReference getPrimitiveMappingContainer_PrimitiveMapping();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping <em>Primitive Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Mapping</em>'.
	 * @see org.scenariotools.msd.primitivemappings.PrimitiveMapping
	 * @generated
	 */
	EClass getPrimitiveMapping();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getUmlPrimitiveDataType <em>Uml Primitive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Uml Primitive Data Type</em>'.
	 * @see org.scenariotools.msd.primitivemappings.PrimitiveMapping#getUmlPrimitiveDataType()
	 * @see #getPrimitiveMapping()
	 * @generated
	 */
	EReference getPrimitiveMapping_UmlPrimitiveDataType();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.primitivemappings.PrimitiveMapping#getEDataType <em>EData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EData Type</em>'.
	 * @see org.scenariotools.msd.primitivemappings.PrimitiveMapping#getEDataType()
	 * @see #getPrimitiveMapping()
	 * @generated
	 */
	EReference getPrimitiveMapping_EDataType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PrimitivemappingsFactory getPrimitivemappingsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingContainerImpl <em>Primitive Mapping Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingContainerImpl
		 * @see org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl#getPrimitiveMappingContainer()
		 * @generated
		 */
		EClass PRIMITIVE_MAPPING_CONTAINER = eINSTANCE.getPrimitiveMappingContainer();

		/**
		 * The meta object literal for the '<em><b>Primitive Mapping</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING = eINSTANCE.getPrimitiveMappingContainer_PrimitiveMapping();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl <em>Primitive Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingImpl
		 * @see org.scenariotools.msd.primitivemappings.impl.PrimitivemappingsPackageImpl#getPrimitiveMapping()
		 * @generated
		 */
		EClass PRIMITIVE_MAPPING = eINSTANCE.getPrimitiveMapping();

		/**
		 * The meta object literal for the '<em><b>Uml Primitive Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE = eINSTANCE.getPrimitiveMapping_UmlPrimitiveDataType();

		/**
		 * The meta object literal for the '<em><b>EData Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_MAPPING__EDATA_TYPE = eINSTANCE.getPrimitiveMapping_EDataType();

	}

} //PrimitivemappingsPackage
