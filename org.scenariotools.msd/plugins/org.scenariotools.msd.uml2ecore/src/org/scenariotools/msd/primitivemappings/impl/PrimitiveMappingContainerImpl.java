/**
 */
package org.scenariotools.msd.primitivemappings.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.msd.primitivemappings.PrimitiveMapping;
import org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Primitive Mapping Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.primitivemappings.impl.PrimitiveMappingContainerImpl#getPrimitiveMapping <em>Primitive Mapping</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PrimitiveMappingContainerImpl extends EObjectImpl implements PrimitiveMappingContainer {
	/**
	 * The cached value of the '{@link #getPrimitiveMapping() <em>Primitive Mapping</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimitiveMapping()
	 * @generated
	 * @ordered
	 */
	protected EList<PrimitiveMapping> primitiveMapping;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrimitiveMappingContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrimitivemappingsPackage.Literals.PRIMITIVE_MAPPING_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrimitiveMapping> getPrimitiveMapping() {
		if (primitiveMapping == null) {
			primitiveMapping = new EObjectContainmentEList<PrimitiveMapping>(PrimitiveMapping.class, this, PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING);
		}
		return primitiveMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING:
				return ((InternalEList<?>)getPrimitiveMapping()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING:
				return getPrimitiveMapping();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING:
				getPrimitiveMapping().clear();
				getPrimitiveMapping().addAll((Collection<? extends PrimitiveMapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING:
				getPrimitiveMapping().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PrimitivemappingsPackage.PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING:
				return primitiveMapping != null && !primitiveMapping.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PrimitiveMappingContainerImpl
