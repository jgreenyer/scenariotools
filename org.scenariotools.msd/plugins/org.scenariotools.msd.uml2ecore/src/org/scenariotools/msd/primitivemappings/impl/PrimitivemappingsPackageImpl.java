/**
 */
package org.scenariotools.msd.primitivemappings.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.msd.primitivemappings.PrimitiveMapping;
import org.scenariotools.msd.primitivemappings.PrimitiveMappingContainer;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsFactory;
import org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage;
import org.scenariotools.msd.uml2ecore.Uml2ecorePackage;
import org.scenariotools.msd.uml2ecore.impl.Uml2ecorePackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PrimitivemappingsPackageImpl extends EPackageImpl implements PrimitivemappingsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveMappingContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveMappingEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.primitivemappings.PrimitivemappingsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PrimitivemappingsPackageImpl() {
		super(eNS_URI, PrimitivemappingsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PrimitivemappingsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PrimitivemappingsPackage init() {
		if (isInited) return (PrimitivemappingsPackage)EPackage.Registry.INSTANCE.getEPackage(PrimitivemappingsPackage.eNS_URI);

		// Obtain or create and register package
		PrimitivemappingsPackageImpl thePrimitivemappingsPackage = (PrimitivemappingsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PrimitivemappingsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PrimitivemappingsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Uml2ecorePackageImpl theUml2ecorePackage = (Uml2ecorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Uml2ecorePackage.eNS_URI) instanceof Uml2ecorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Uml2ecorePackage.eNS_URI) : Uml2ecorePackage.eINSTANCE);

		// Create package meta-data objects
		thePrimitivemappingsPackage.createPackageContents();
		theUml2ecorePackage.createPackageContents();

		// Initialize created meta-data
		thePrimitivemappingsPackage.initializePackageContents();
		theUml2ecorePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePrimitivemappingsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PrimitivemappingsPackage.eNS_URI, thePrimitivemappingsPackage);
		return thePrimitivemappingsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveMappingContainer() {
		return primitiveMappingContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveMappingContainer_PrimitiveMapping() {
		return (EReference)primitiveMappingContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveMapping() {
		return primitiveMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveMapping_UmlPrimitiveDataType() {
		return (EReference)primitiveMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveMapping_EDataType() {
		return (EReference)primitiveMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitivemappingsFactory getPrimitivemappingsFactory() {
		return (PrimitivemappingsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		primitiveMappingContainerEClass = createEClass(PRIMITIVE_MAPPING_CONTAINER);
		createEReference(primitiveMappingContainerEClass, PRIMITIVE_MAPPING_CONTAINER__PRIMITIVE_MAPPING);

		primitiveMappingEClass = createEClass(PRIMITIVE_MAPPING);
		createEReference(primitiveMappingEClass, PRIMITIVE_MAPPING__UML_PRIMITIVE_DATA_TYPE);
		createEReference(primitiveMappingEClass, PRIMITIVE_MAPPING__EDATA_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(primitiveMappingContainerEClass, PrimitiveMappingContainer.class, "PrimitiveMappingContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimitiveMappingContainer_PrimitiveMapping(), this.getPrimitiveMapping(), null, "primitiveMapping", null, 0, -1, PrimitiveMappingContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveMappingEClass, PrimitiveMapping.class, "PrimitiveMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimitiveMapping_UmlPrimitiveDataType(), theUMLPackage.getPrimitiveType(), null, "umlPrimitiveDataType", null, 0, 1, PrimitiveMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimitiveMapping_EDataType(), theEcorePackage.getEDataType(), null, "eDataType", null, 0, 1, PrimitiveMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //PrimitivemappingsPackageImpl
