package org.scenariotools.msd.xtext.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionConstraint;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.scenariotools.msd.xtext.services.UmlGrammarAccess;

@SuppressWarnings("all")
public class UmlSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private UmlGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == UMLPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case UMLPackage.CLASS:
				if(context == grammarAccess.getClassRule() ||
				   context == grammarAccess.getPackageableElementRule()) {
					sequence_Class(context, (org.eclipse.uml2.uml.Class) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.COLLABORATION:
				if(context == grammarAccess.getCollaborationRule() ||
				   context == grammarAccess.getPackageableElementRule()) {
					sequence_Collaboration(context, (Collaboration) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.COMBINED_FRAGMENT:
				if(context == grammarAccess.getCombinedFragmentRule() ||
				   context == grammarAccess.getInteractionFragmentRule()) {
					sequence_CombinedFragment(context, (CombinedFragment) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.COMMENT:
				if(context == grammarAccess.getCommentRule()) {
					sequence_Comment(context, (Comment) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.INTERACTION:
				if(context == grammarAccess.getInteractionRule()) {
					sequence_Interaction(context, (Interaction) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.INTERACTION_CONSTRAINT:
				if(context == grammarAccess.getInteractionConstraintRule()) {
					sequence_InteractionConstraint(context, (InteractionConstraint) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.INTERACTION_OPERAND:
				if(context == grammarAccess.getInteractionOperandRule()) {
					sequence_InteractionOperand(context, (InteractionOperand) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.LIFELINE:
				if(context == grammarAccess.getLifelineRule()) {
					sequence_Lifeline(context, (Lifeline) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.LITERAL_INTEGER:
				if(context == grammarAccess.getLiteralIntegerRule()) {
					sequence_LiteralInteger(context, (LiteralInteger) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.LITERAL_STRING:
				if(context == grammarAccess.getLiteralStringRule() ||
				   context == grammarAccess.getValueSpecificationRule()) {
					sequence_LiteralString(context, (LiteralString) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.LITERAL_UNLIMITED_NATURAL:
				if(context == grammarAccess.getLiteralUnlimitedNaturalRule() ||
				   context == grammarAccess.getValueSpecificationRule()) {
					sequence_LiteralUnlimitedNatural(context, (LiteralUnlimitedNatural) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.MESSAGE:
				if(context == grammarAccess.getMessageRule()) {
					sequence_Message(context, (Message) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.MESSAGE_OCCURRENCE_SPECIFICATION:
				if(context == grammarAccess.getInteractionFragmentRule() ||
				   context == grammarAccess.getMessageOccurrenceSpecificationRule()) {
					sequence_MessageOccurrenceSpecification(context, (MessageOccurrenceSpecification) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.MODEL:
				if(context == grammarAccess.getModelRule()) {
					sequence_Model(context, (Model) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.OPERATION:
				if(context == grammarAccess.getOperationRule()) {
					sequence_Operation(context, (Operation) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.PACKAGE:
				if(context == grammarAccess.getPackageRule() ||
				   context == grammarAccess.getPackageableElementRule()) {
					sequence_Package(context, (org.eclipse.uml2.uml.Package) semanticObject); 
					return; 
				}
				else break;
			case UMLPackage.PROPERTY:
				if(context == grammarAccess.getPropertyRule()) {
					sequence_Property(context, (Property) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID ownedOperation+=Operation* ownedAttribute+=Property*)
	 */
	protected void sequence_Class(EObject context, org.eclipse.uml2.uml.Class semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID ownedAttribute+=Property* ownedBehavior+=Interaction*)
	 */
	protected void sequence_Collaboration(EObject context, Collaboration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (interactionOperator=InteractionOperatorKind covered+=[Lifeline|ID]* operand+=InteractionOperand+)
	 */
	protected void sequence_CombinedFragment(EObject context, CombinedFragment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     body=STRING
	 */
	protected void sequence_Comment(EObject context, Comment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID minint=ValueSpecification maxint=ValueSpecification specification=ValueSpecification)
	 */
	protected void sequence_InteractionConstraint(EObject context, InteractionConstraint semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (covered+=[Lifeline|ID]* guard=InteractionConstraint? fragment+=InteractionFragment*)
	 */
	protected void sequence_InteractionOperand(EObject context, InteractionOperand semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID ownedComment+=Comment* lifeline+=Lifeline* message+=Message* fragment+=InteractionFragment*)
	 */
	protected void sequence_Interaction(EObject context, Interaction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID represents=[Property|ID]?)
	 */
	protected void sequence_Lifeline(EObject context, Lifeline semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=INT
	 */
	protected void sequence_LiteralInteger(EObject context, LiteralInteger semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=STRING
	 */
	protected void sequence_LiteralString(EObject context, LiteralString semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=INT
	 */
	protected void sequence_LiteralUnlimitedNatural(EObject context, LiteralUnlimitedNatural semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID covered+=[Lifeline|ID]+)
	 */
	protected void sequence_MessageOccurrenceSpecification(EObject context, MessageOccurrenceSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         messageSort=MessageSort 
	 *         receiveEvent=[MessageOccurrenceSpecification|QualifiedName] 
	 *         sendEvent=[MessageOccurrenceSpecification|QualifiedName] 
	 *         signature=[Operation|QualifiedName]
	 *     )
	 */
	protected void sequence_Message(EObject context, Message semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     packagedElement+=Package
	 */
	protected void sequence_Model(EObject context, Model semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Operation(EObject context, Operation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID packagedElement+=PackageableElement*)
	 */
	protected void sequence_Package(EObject context, org.eclipse.uml2.uml.Package semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID type=[Class|ID]? lowerValue=ValueSpecification upperValue=ValueSpecification)
	 */
	protected void sequence_Property(EObject context, Property semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
