package org.scenariotools.msd.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.msd.xtext.services.UmlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUmlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'{'", "'}'", "'class'", "'operation'", "';'", "'attribute'", "'type='", "'('", "','", "')'", "'collaboration'", "'ownedBehavior'", "'combinedFragment'", "'covered='", "'comment'", "'interactionOperand'", "'interactionConstraint'", "'lifeline'", "'represents='", "'message'", "'receive='", "'send='", "'signature='", "'messageOccurrenceSpecification'", "'.'", "'assert'", "'sec'", "'alt'", "'opt'", "'break'", "'par'", "'strict'", "'loop'", "'critical'", "'neg'", "'ignore'", "'consider'", "'asynchCall'", "'synchCall'", "'createMessage'", "'deleteMessage'", "'reply'", "'asynchSignal'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUmlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUmlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUmlParser.tokenNames; }
    public String getGrammarFileName() { return "../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g"; }



     	private UmlGrammarAccess grammarAccess;
     	
        public InternalUmlParser(TokenStream input, UmlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected UmlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:68:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:69:2: (iv_ruleModel= ruleModel EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:70:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:77:1: ruleModel returns [EObject current=null] : ( (lv_packagedElement_0_0= rulePackage ) ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_packagedElement_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:80:28: ( ( (lv_packagedElement_0_0= rulePackage ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:81:1: ( (lv_packagedElement_0_0= rulePackage ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:81:1: ( (lv_packagedElement_0_0= rulePackage ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:82:1: (lv_packagedElement_0_0= rulePackage )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:82:1: (lv_packagedElement_0_0= rulePackage )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:83:3: lv_packagedElement_0_0= rulePackage
            {
             
            	        newCompositeNode(grammarAccess.getModelAccess().getPackagedElementPackageParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_rulePackage_in_ruleModel130);
            lv_packagedElement_0_0=rulePackage();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getModelRule());
            	        }
                   		add(
                   			current, 
                   			"packagedElement",
                    		lv_packagedElement_0_0, 
                    		"Package");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRulePackage"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:107:1: entryRulePackage returns [EObject current=null] : iv_rulePackage= rulePackage EOF ;
    public final EObject entryRulePackage() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePackage = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:108:2: (iv_rulePackage= rulePackage EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:109:2: iv_rulePackage= rulePackage EOF
            {
             newCompositeNode(grammarAccess.getPackageRule()); 
            pushFollow(FOLLOW_rulePackage_in_entryRulePackage165);
            iv_rulePackage=rulePackage();

            state._fsp--;

             current =iv_rulePackage; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePackage175); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackage"


    // $ANTLR start "rulePackage"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:116:1: rulePackage returns [EObject current=null] : (otherlv_0= 'package' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_packagedElement_3_0= rulePackageableElement ) )* otherlv_4= '}' ) ;
    public final EObject rulePackage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_packagedElement_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:119:28: ( (otherlv_0= 'package' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_packagedElement_3_0= rulePackageableElement ) )* otherlv_4= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:120:1: (otherlv_0= 'package' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_packagedElement_3_0= rulePackageableElement ) )* otherlv_4= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:120:1: (otherlv_0= 'package' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_packagedElement_3_0= rulePackageableElement ) )* otherlv_4= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:120:3: otherlv_0= 'package' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_packagedElement_3_0= rulePackageableElement ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulePackage212); 

                	newLeafNode(otherlv_0, grammarAccess.getPackageAccess().getPackageKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:124:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:125:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:125:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:126:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePackage229); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPackageAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPackageRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_rulePackage246); 

                	newLeafNode(otherlv_2, grammarAccess.getPackageAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:146:1: ( (lv_packagedElement_3_0= rulePackageableElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11||LA1_0==14||LA1_0==22) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:147:1: (lv_packagedElement_3_0= rulePackageableElement )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:147:1: (lv_packagedElement_3_0= rulePackageableElement )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:148:3: lv_packagedElement_3_0= rulePackageableElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPackageAccess().getPackagedElementPackageableElementParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePackageableElement_in_rulePackage267);
            	    lv_packagedElement_3_0=rulePackageableElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPackageRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"packagedElement",
            	            		lv_packagedElement_3_0, 
            	            		"PackageableElement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_rulePackage280); 

                	newLeafNode(otherlv_4, grammarAccess.getPackageAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackage"


    // $ANTLR start "entryRulePackageableElement"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:176:1: entryRulePackageableElement returns [EObject current=null] : iv_rulePackageableElement= rulePackageableElement EOF ;
    public final EObject entryRulePackageableElement() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePackageableElement = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:177:2: (iv_rulePackageableElement= rulePackageableElement EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:178:2: iv_rulePackageableElement= rulePackageableElement EOF
            {
             newCompositeNode(grammarAccess.getPackageableElementRule()); 
            pushFollow(FOLLOW_rulePackageableElement_in_entryRulePackageableElement316);
            iv_rulePackageableElement=rulePackageableElement();

            state._fsp--;

             current =iv_rulePackageableElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePackageableElement326); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackageableElement"


    // $ANTLR start "rulePackageableElement"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:185:1: rulePackageableElement returns [EObject current=null] : (this_Package_0= rulePackage | this_Class_1= ruleClass | this_Collaboration_2= ruleCollaboration ) ;
    public final EObject rulePackageableElement() throws RecognitionException {
        EObject current = null;

        EObject this_Package_0 = null;

        EObject this_Class_1 = null;

        EObject this_Collaboration_2 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:188:28: ( (this_Package_0= rulePackage | this_Class_1= ruleClass | this_Collaboration_2= ruleCollaboration ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:189:1: (this_Package_0= rulePackage | this_Class_1= ruleClass | this_Collaboration_2= ruleCollaboration )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:189:1: (this_Package_0= rulePackage | this_Class_1= ruleClass | this_Collaboration_2= ruleCollaboration )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 14:
                {
                alt2=2;
                }
                break;
            case 22:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:190:5: this_Package_0= rulePackage
                    {
                     
                            newCompositeNode(grammarAccess.getPackageableElementAccess().getPackageParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePackage_in_rulePackageableElement373);
                    this_Package_0=rulePackage();

                    state._fsp--;

                     
                            current = this_Package_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:200:5: this_Class_1= ruleClass
                    {
                     
                            newCompositeNode(grammarAccess.getPackageableElementAccess().getClassParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleClass_in_rulePackageableElement400);
                    this_Class_1=ruleClass();

                    state._fsp--;

                     
                            current = this_Class_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:210:5: this_Collaboration_2= ruleCollaboration
                    {
                     
                            newCompositeNode(grammarAccess.getPackageableElementAccess().getCollaborationParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleCollaboration_in_rulePackageableElement427);
                    this_Collaboration_2=ruleCollaboration();

                    state._fsp--;

                     
                            current = this_Collaboration_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackageableElement"


    // $ANTLR start "entryRuleClass"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:226:1: entryRuleClass returns [EObject current=null] : iv_ruleClass= ruleClass EOF ;
    public final EObject entryRuleClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClass = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:227:2: (iv_ruleClass= ruleClass EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:228:2: iv_ruleClass= ruleClass EOF
            {
             newCompositeNode(grammarAccess.getClassRule()); 
            pushFollow(FOLLOW_ruleClass_in_entryRuleClass462);
            iv_ruleClass=ruleClass();

            state._fsp--;

             current =iv_ruleClass; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleClass472); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:235:1: ruleClass returns [EObject current=null] : (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedOperation_3_0= ruleOperation ) )* ( (lv_ownedAttribute_4_0= ruleProperty ) )* otherlv_5= '}' ) ;
    public final EObject ruleClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_ownedOperation_3_0 = null;

        EObject lv_ownedAttribute_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:238:28: ( (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedOperation_3_0= ruleOperation ) )* ( (lv_ownedAttribute_4_0= ruleProperty ) )* otherlv_5= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:239:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedOperation_3_0= ruleOperation ) )* ( (lv_ownedAttribute_4_0= ruleProperty ) )* otherlv_5= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:239:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedOperation_3_0= ruleOperation ) )* ( (lv_ownedAttribute_4_0= ruleProperty ) )* otherlv_5= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:239:3: otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedOperation_3_0= ruleOperation ) )* ( (lv_ownedAttribute_4_0= ruleProperty ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleClass509); 

                	newLeafNode(otherlv_0, grammarAccess.getClassAccess().getClassKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:243:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:244:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:244:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:245:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleClass526); 

            			newLeafNode(lv_name_1_0, grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getClassRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleClass543); 

                	newLeafNode(otherlv_2, grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:265:1: ( (lv_ownedOperation_3_0= ruleOperation ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:266:1: (lv_ownedOperation_3_0= ruleOperation )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:266:1: (lv_ownedOperation_3_0= ruleOperation )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:267:3: lv_ownedOperation_3_0= ruleOperation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getClassAccess().getOwnedOperationOperationParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOperation_in_ruleClass564);
            	    lv_ownedOperation_3_0=ruleOperation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedOperation",
            	            		lv_ownedOperation_3_0, 
            	            		"Operation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:283:3: ( (lv_ownedAttribute_4_0= ruleProperty ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:284:1: (lv_ownedAttribute_4_0= ruleProperty )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:284:1: (lv_ownedAttribute_4_0= ruleProperty )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:285:3: lv_ownedAttribute_4_0= ruleProperty
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getClassAccess().getOwnedAttributePropertyParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleProperty_in_ruleClass586);
            	    lv_ownedAttribute_4_0=ruleProperty();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedAttribute",
            	            		lv_ownedAttribute_4_0, 
            	            		"Property");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_ruleClass599); 

                	newLeafNode(otherlv_5, grammarAccess.getClassAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleOperation"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:313:1: entryRuleOperation returns [EObject current=null] : iv_ruleOperation= ruleOperation EOF ;
    public final EObject entryRuleOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperation = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:314:2: (iv_ruleOperation= ruleOperation EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:315:2: iv_ruleOperation= ruleOperation EOF
            {
             newCompositeNode(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_ruleOperation_in_entryRuleOperation635);
            iv_ruleOperation=ruleOperation();

            state._fsp--;

             current =iv_ruleOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOperation645); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:322:1: ruleOperation returns [EObject current=null] : (otherlv_0= 'operation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:325:28: ( (otherlv_0= 'operation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:326:1: (otherlv_0= 'operation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:326:1: (otherlv_0= 'operation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:326:3: otherlv_0= 'operation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleOperation682); 

                	newLeafNode(otherlv_0, grammarAccess.getOperationAccess().getOperationKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:330:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:331:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:331:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:332:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOperation699); 

            			newLeafNode(lv_name_1_0, grammarAccess.getOperationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOperationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleOperation716); 

                	newLeafNode(otherlv_2, grammarAccess.getOperationAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleProperty"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:360:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:361:2: (iv_ruleProperty= ruleProperty EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:362:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_ruleProperty_in_entryRuleProperty752);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProperty762); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:369:1: ruleProperty returns [EObject current=null] : (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '(' ( (lv_lowerValue_5_0= ruleValueSpecification ) ) otherlv_6= ',' ( (lv_upperValue_7_0= ruleValueSpecification ) ) otherlv_8= ')' otherlv_9= ';' ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_lowerValue_5_0 = null;

        EObject lv_upperValue_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:372:28: ( (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '(' ( (lv_lowerValue_5_0= ruleValueSpecification ) ) otherlv_6= ',' ( (lv_upperValue_7_0= ruleValueSpecification ) ) otherlv_8= ')' otherlv_9= ';' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:373:1: (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '(' ( (lv_lowerValue_5_0= ruleValueSpecification ) ) otherlv_6= ',' ( (lv_upperValue_7_0= ruleValueSpecification ) ) otherlv_8= ')' otherlv_9= ';' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:373:1: (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '(' ( (lv_lowerValue_5_0= ruleValueSpecification ) ) otherlv_6= ',' ( (lv_upperValue_7_0= ruleValueSpecification ) ) otherlv_8= ')' otherlv_9= ';' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:373:3: otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '(' ( (lv_lowerValue_5_0= ruleValueSpecification ) ) otherlv_6= ',' ( (lv_upperValue_7_0= ruleValueSpecification ) ) otherlv_8= ')' otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleProperty799); 

                	newLeafNode(otherlv_0, grammarAccess.getPropertyAccess().getAttributeKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:377:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:378:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:378:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:379:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProperty816); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPropertyRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:395:2: (otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:395:4: otherlv_2= 'type=' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleProperty834); 

                        	newLeafNode(otherlv_2, grammarAccess.getPropertyAccess().getTypeKeyword_2_0());
                        
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:399:1: ( (otherlv_3= RULE_ID ) )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:400:1: (otherlv_3= RULE_ID )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:400:1: (otherlv_3= RULE_ID )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:401:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getPropertyRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProperty854); 

                    		newLeafNode(otherlv_3, grammarAccess.getPropertyAccess().getTypeClassCrossReference_2_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,19,FOLLOW_19_in_ruleProperty868); 

                	newLeafNode(otherlv_4, grammarAccess.getPropertyAccess().getLeftParenthesisKeyword_3());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:416:1: ( (lv_lowerValue_5_0= ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:417:1: (lv_lowerValue_5_0= ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:417:1: (lv_lowerValue_5_0= ruleValueSpecification )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:418:3: lv_lowerValue_5_0= ruleValueSpecification
            {
             
            	        newCompositeNode(grammarAccess.getPropertyAccess().getLowerValueValueSpecificationParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleValueSpecification_in_ruleProperty889);
            lv_lowerValue_5_0=ruleValueSpecification();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPropertyRule());
            	        }
                   		set(
                   			current, 
                   			"lowerValue",
                    		lv_lowerValue_5_0, 
                    		"ValueSpecification");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,20,FOLLOW_20_in_ruleProperty901); 

                	newLeafNode(otherlv_6, grammarAccess.getPropertyAccess().getCommaKeyword_5());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:438:1: ( (lv_upperValue_7_0= ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:439:1: (lv_upperValue_7_0= ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:439:1: (lv_upperValue_7_0= ruleValueSpecification )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:440:3: lv_upperValue_7_0= ruleValueSpecification
            {
             
            	        newCompositeNode(grammarAccess.getPropertyAccess().getUpperValueValueSpecificationParserRuleCall_6_0()); 
            	    
            pushFollow(FOLLOW_ruleValueSpecification_in_ruleProperty922);
            lv_upperValue_7_0=ruleValueSpecification();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPropertyRule());
            	        }
                   		set(
                   			current, 
                   			"upperValue",
                    		lv_upperValue_7_0, 
                    		"ValueSpecification");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,21,FOLLOW_21_in_ruleProperty934); 

                	newLeafNode(otherlv_8, grammarAccess.getPropertyAccess().getRightParenthesisKeyword_7());
                
            otherlv_9=(Token)match(input,16,FOLLOW_16_in_ruleProperty946); 

                	newLeafNode(otherlv_9, grammarAccess.getPropertyAccess().getSemicolonKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleValueSpecification"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:472:1: entryRuleValueSpecification returns [EObject current=null] : iv_ruleValueSpecification= ruleValueSpecification EOF ;
    public final EObject entryRuleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueSpecification = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:473:2: (iv_ruleValueSpecification= ruleValueSpecification EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:474:2: iv_ruleValueSpecification= ruleValueSpecification EOF
            {
             newCompositeNode(grammarAccess.getValueSpecificationRule()); 
            pushFollow(FOLLOW_ruleValueSpecification_in_entryRuleValueSpecification982);
            iv_ruleValueSpecification=ruleValueSpecification();

            state._fsp--;

             current =iv_ruleValueSpecification; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleValueSpecification992); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueSpecification"


    // $ANTLR start "ruleValueSpecification"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:481:1: ruleValueSpecification returns [EObject current=null] : (this_LiteralUnlimitedNatural_0= ruleLiteralUnlimitedNatural | this_LiteralString_1= ruleLiteralString ) ;
    public final EObject ruleValueSpecification() throws RecognitionException {
        EObject current = null;

        EObject this_LiteralUnlimitedNatural_0 = null;

        EObject this_LiteralString_1 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:484:28: ( (this_LiteralUnlimitedNatural_0= ruleLiteralUnlimitedNatural | this_LiteralString_1= ruleLiteralString ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:485:1: (this_LiteralUnlimitedNatural_0= ruleLiteralUnlimitedNatural | this_LiteralString_1= ruleLiteralString )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:485:1: (this_LiteralUnlimitedNatural_0= ruleLiteralUnlimitedNatural | this_LiteralString_1= ruleLiteralString )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_INT) ) {
                alt6=1;
            }
            else if ( (LA6_0==RULE_STRING) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:486:5: this_LiteralUnlimitedNatural_0= ruleLiteralUnlimitedNatural
                    {
                     
                            newCompositeNode(grammarAccess.getValueSpecificationAccess().getLiteralUnlimitedNaturalParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleLiteralUnlimitedNatural_in_ruleValueSpecification1039);
                    this_LiteralUnlimitedNatural_0=ruleLiteralUnlimitedNatural();

                    state._fsp--;

                     
                            current = this_LiteralUnlimitedNatural_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:496:5: this_LiteralString_1= ruleLiteralString
                    {
                     
                            newCompositeNode(grammarAccess.getValueSpecificationAccess().getLiteralStringParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleLiteralString_in_ruleValueSpecification1066);
                    this_LiteralString_1=ruleLiteralString();

                    state._fsp--;

                     
                            current = this_LiteralString_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueSpecification"


    // $ANTLR start "entryRuleLiteralUnlimitedNatural"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:514:1: entryRuleLiteralUnlimitedNatural returns [EObject current=null] : iv_ruleLiteralUnlimitedNatural= ruleLiteralUnlimitedNatural EOF ;
    public final EObject entryRuleLiteralUnlimitedNatural() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralUnlimitedNatural = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:515:2: (iv_ruleLiteralUnlimitedNatural= ruleLiteralUnlimitedNatural EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:516:2: iv_ruleLiteralUnlimitedNatural= ruleLiteralUnlimitedNatural EOF
            {
             newCompositeNode(grammarAccess.getLiteralUnlimitedNaturalRule()); 
            pushFollow(FOLLOW_ruleLiteralUnlimitedNatural_in_entryRuleLiteralUnlimitedNatural1103);
            iv_ruleLiteralUnlimitedNatural=ruleLiteralUnlimitedNatural();

            state._fsp--;

             current =iv_ruleLiteralUnlimitedNatural; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteralUnlimitedNatural1113); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralUnlimitedNatural"


    // $ANTLR start "ruleLiteralUnlimitedNatural"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:523:1: ruleLiteralUnlimitedNatural returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleLiteralUnlimitedNatural() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:526:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:527:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:527:1: ( (lv_value_0_0= RULE_INT ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:528:1: (lv_value_0_0= RULE_INT )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:528:1: (lv_value_0_0= RULE_INT )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:529:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleLiteralUnlimitedNatural1154); 

            			newLeafNode(lv_value_0_0, grammarAccess.getLiteralUnlimitedNaturalAccess().getValueINTTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLiteralUnlimitedNaturalRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"INT");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralUnlimitedNatural"


    // $ANTLR start "entryRuleLiteralString"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:553:1: entryRuleLiteralString returns [EObject current=null] : iv_ruleLiteralString= ruleLiteralString EOF ;
    public final EObject entryRuleLiteralString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralString = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:554:2: (iv_ruleLiteralString= ruleLiteralString EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:555:2: iv_ruleLiteralString= ruleLiteralString EOF
            {
             newCompositeNode(grammarAccess.getLiteralStringRule()); 
            pushFollow(FOLLOW_ruleLiteralString_in_entryRuleLiteralString1194);
            iv_ruleLiteralString=ruleLiteralString();

            state._fsp--;

             current =iv_ruleLiteralString; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteralString1204); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralString"


    // $ANTLR start "ruleLiteralString"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:562:1: ruleLiteralString returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleLiteralString() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:565:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:566:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:566:1: ( (lv_value_0_0= RULE_STRING ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:567:1: (lv_value_0_0= RULE_STRING )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:567:1: (lv_value_0_0= RULE_STRING )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:568:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleLiteralString1245); 

            			newLeafNode(lv_value_0_0, grammarAccess.getLiteralStringAccess().getValueSTRINGTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLiteralStringRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"STRING");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralString"


    // $ANTLR start "entryRuleCollaboration"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:592:1: entryRuleCollaboration returns [EObject current=null] : iv_ruleCollaboration= ruleCollaboration EOF ;
    public final EObject entryRuleCollaboration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollaboration = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:593:2: (iv_ruleCollaboration= ruleCollaboration EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:594:2: iv_ruleCollaboration= ruleCollaboration EOF
            {
             newCompositeNode(grammarAccess.getCollaborationRule()); 
            pushFollow(FOLLOW_ruleCollaboration_in_entryRuleCollaboration1285);
            iv_ruleCollaboration=ruleCollaboration();

            state._fsp--;

             current =iv_ruleCollaboration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCollaboration1295); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollaboration"


    // $ANTLR start "ruleCollaboration"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:601:1: ruleCollaboration returns [EObject current=null] : (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedAttribute_3_0= ruleProperty ) )* ( (lv_ownedBehavior_4_0= ruleInteraction ) )* otherlv_5= '}' ) ;
    public final EObject ruleCollaboration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_ownedAttribute_3_0 = null;

        EObject lv_ownedBehavior_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:604:28: ( (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedAttribute_3_0= ruleProperty ) )* ( (lv_ownedBehavior_4_0= ruleInteraction ) )* otherlv_5= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:605:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedAttribute_3_0= ruleProperty ) )* ( (lv_ownedBehavior_4_0= ruleInteraction ) )* otherlv_5= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:605:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedAttribute_3_0= ruleProperty ) )* ( (lv_ownedBehavior_4_0= ruleInteraction ) )* otherlv_5= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:605:3: otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedAttribute_3_0= ruleProperty ) )* ( (lv_ownedBehavior_4_0= ruleInteraction ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleCollaboration1332); 

                	newLeafNode(otherlv_0, grammarAccess.getCollaborationAccess().getCollaborationKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:609:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:610:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:610:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:611:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCollaboration1349); 

            			newLeafNode(lv_name_1_0, grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCollaborationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleCollaboration1366); 

                	newLeafNode(otherlv_2, grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:631:1: ( (lv_ownedAttribute_3_0= ruleProperty ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==17) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:632:1: (lv_ownedAttribute_3_0= ruleProperty )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:632:1: (lv_ownedAttribute_3_0= ruleProperty )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:633:3: lv_ownedAttribute_3_0= ruleProperty
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCollaborationAccess().getOwnedAttributePropertyParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleProperty_in_ruleCollaboration1387);
            	    lv_ownedAttribute_3_0=ruleProperty();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedAttribute",
            	            		lv_ownedAttribute_3_0, 
            	            		"Property");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:649:3: ( (lv_ownedBehavior_4_0= ruleInteraction ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==23) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:650:1: (lv_ownedBehavior_4_0= ruleInteraction )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:650:1: (lv_ownedBehavior_4_0= ruleInteraction )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:651:3: lv_ownedBehavior_4_0= ruleInteraction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCollaborationAccess().getOwnedBehaviorInteractionParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleInteraction_in_ruleCollaboration1409);
            	    lv_ownedBehavior_4_0=ruleInteraction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedBehavior",
            	            		lv_ownedBehavior_4_0, 
            	            		"Interaction");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_ruleCollaboration1422); 

                	newLeafNode(otherlv_5, grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollaboration"


    // $ANTLR start "entryRuleInteraction"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:679:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:680:2: (iv_ruleInteraction= ruleInteraction EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:681:2: iv_ruleInteraction= ruleInteraction EOF
            {
             newCompositeNode(grammarAccess.getInteractionRule()); 
            pushFollow(FOLLOW_ruleInteraction_in_entryRuleInteraction1458);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;

             current =iv_ruleInteraction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteraction1468); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:688:1: ruleInteraction returns [EObject current=null] : (otherlv_0= 'ownedBehavior' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedComment_3_0= ruleComment ) )* ( (lv_lifeline_4_0= ruleLifeline ) )* ( (lv_message_5_0= ruleMessage ) )* ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_7=null;
        EObject lv_ownedComment_3_0 = null;

        EObject lv_lifeline_4_0 = null;

        EObject lv_message_5_0 = null;

        EObject lv_fragment_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:691:28: ( (otherlv_0= 'ownedBehavior' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedComment_3_0= ruleComment ) )* ( (lv_lifeline_4_0= ruleLifeline ) )* ( (lv_message_5_0= ruleMessage ) )* ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:692:1: (otherlv_0= 'ownedBehavior' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedComment_3_0= ruleComment ) )* ( (lv_lifeline_4_0= ruleLifeline ) )* ( (lv_message_5_0= ruleMessage ) )* ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:692:1: (otherlv_0= 'ownedBehavior' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedComment_3_0= ruleComment ) )* ( (lv_lifeline_4_0= ruleLifeline ) )* ( (lv_message_5_0= ruleMessage ) )* ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:692:3: otherlv_0= 'ownedBehavior' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ownedComment_3_0= ruleComment ) )* ( (lv_lifeline_4_0= ruleLifeline ) )* ( (lv_message_5_0= ruleMessage ) )* ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleInteraction1505); 

                	newLeafNode(otherlv_0, grammarAccess.getInteractionAccess().getOwnedBehaviorKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:696:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:697:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:697:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:698:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInteraction1522); 

            			newLeafNode(lv_name_1_0, grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInteractionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleInteraction1539); 

                	newLeafNode(otherlv_2, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:718:1: ( (lv_ownedComment_3_0= ruleComment ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==26) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:719:1: (lv_ownedComment_3_0= ruleComment )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:719:1: (lv_ownedComment_3_0= ruleComment )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:720:3: lv_ownedComment_3_0= ruleComment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInteractionAccess().getOwnedCommentCommentParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleComment_in_ruleInteraction1560);
            	    lv_ownedComment_3_0=ruleComment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedComment",
            	            		lv_ownedComment_3_0, 
            	            		"Comment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:736:3: ( (lv_lifeline_4_0= ruleLifeline ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==29) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:737:1: (lv_lifeline_4_0= ruleLifeline )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:737:1: (lv_lifeline_4_0= ruleLifeline )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:738:3: lv_lifeline_4_0= ruleLifeline
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInteractionAccess().getLifelineLifelineParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleLifeline_in_ruleInteraction1582);
            	    lv_lifeline_4_0=ruleLifeline();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"lifeline",
            	            		lv_lifeline_4_0, 
            	            		"Lifeline");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:754:3: ( (lv_message_5_0= ruleMessage ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==31) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:755:1: (lv_message_5_0= ruleMessage )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:755:1: (lv_message_5_0= ruleMessage )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:756:3: lv_message_5_0= ruleMessage
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInteractionAccess().getMessageMessageParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleMessage_in_ruleInteraction1604);
            	    lv_message_5_0=ruleMessage();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"message",
            	            		lv_message_5_0, 
            	            		"Message");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:772:3: ( (lv_fragment_6_0= ruleInteractionFragment ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==24||LA12_0==35) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:773:1: (lv_fragment_6_0= ruleInteractionFragment )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:773:1: (lv_fragment_6_0= ruleInteractionFragment )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:774:3: lv_fragment_6_0= ruleInteractionFragment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentInteractionFragmentParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleInteractionFragment_in_ruleInteraction1626);
            	    lv_fragment_6_0=ruleInteractionFragment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"fragment",
            	            		lv_fragment_6_0, 
            	            		"InteractionFragment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_13_in_ruleInteraction1639); 

                	newLeafNode(otherlv_7, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleInteractionFragment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:802:1: entryRuleInteractionFragment returns [EObject current=null] : iv_ruleInteractionFragment= ruleInteractionFragment EOF ;
    public final EObject entryRuleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionFragment = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:803:2: (iv_ruleInteractionFragment= ruleInteractionFragment EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:804:2: iv_ruleInteractionFragment= ruleInteractionFragment EOF
            {
             newCompositeNode(grammarAccess.getInteractionFragmentRule()); 
            pushFollow(FOLLOW_ruleInteractionFragment_in_entryRuleInteractionFragment1675);
            iv_ruleInteractionFragment=ruleInteractionFragment();

            state._fsp--;

             current =iv_ruleInteractionFragment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionFragment1685); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:811:1: ruleInteractionFragment returns [EObject current=null] : (this_MessageOccurrenceSpecification_0= ruleMessageOccurrenceSpecification | this_CombinedFragment_1= ruleCombinedFragment ) ;
    public final EObject ruleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_MessageOccurrenceSpecification_0 = null;

        EObject this_CombinedFragment_1 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:814:28: ( (this_MessageOccurrenceSpecification_0= ruleMessageOccurrenceSpecification | this_CombinedFragment_1= ruleCombinedFragment ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:815:1: (this_MessageOccurrenceSpecification_0= ruleMessageOccurrenceSpecification | this_CombinedFragment_1= ruleCombinedFragment )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:815:1: (this_MessageOccurrenceSpecification_0= ruleMessageOccurrenceSpecification | this_CombinedFragment_1= ruleCombinedFragment )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==35) ) {
                alt13=1;
            }
            else if ( (LA13_0==24) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:816:5: this_MessageOccurrenceSpecification_0= ruleMessageOccurrenceSpecification
                    {
                     
                            newCompositeNode(grammarAccess.getInteractionFragmentAccess().getMessageOccurrenceSpecificationParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleMessageOccurrenceSpecification_in_ruleInteractionFragment1732);
                    this_MessageOccurrenceSpecification_0=ruleMessageOccurrenceSpecification();

                    state._fsp--;

                     
                            current = this_MessageOccurrenceSpecification_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:826:5: this_CombinedFragment_1= ruleCombinedFragment
                    {
                     
                            newCompositeNode(grammarAccess.getInteractionFragmentAccess().getCombinedFragmentParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleCombinedFragment_in_ruleInteractionFragment1759);
                    this_CombinedFragment_1=ruleCombinedFragment();

                    state._fsp--;

                     
                            current = this_CombinedFragment_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleCombinedFragment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:842:1: entryRuleCombinedFragment returns [EObject current=null] : iv_ruleCombinedFragment= ruleCombinedFragment EOF ;
    public final EObject entryRuleCombinedFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCombinedFragment = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:843:2: (iv_ruleCombinedFragment= ruleCombinedFragment EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:844:2: iv_ruleCombinedFragment= ruleCombinedFragment EOF
            {
             newCompositeNode(grammarAccess.getCombinedFragmentRule()); 
            pushFollow(FOLLOW_ruleCombinedFragment_in_entryRuleCombinedFragment1794);
            iv_ruleCombinedFragment=ruleCombinedFragment();

            state._fsp--;

             current =iv_ruleCombinedFragment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCombinedFragment1804); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCombinedFragment"


    // $ANTLR start "ruleCombinedFragment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:851:1: ruleCombinedFragment returns [EObject current=null] : (otherlv_0= 'combinedFragment' otherlv_1= '{' ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) ) (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_operand_5_0= ruleInteractionOperand ) )+ otherlv_6= '}' ) ;
    public final EObject ruleCombinedFragment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Enumerator lv_interactionOperator_2_0 = null;

        EObject lv_operand_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:854:28: ( (otherlv_0= 'combinedFragment' otherlv_1= '{' ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) ) (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_operand_5_0= ruleInteractionOperand ) )+ otherlv_6= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:855:1: (otherlv_0= 'combinedFragment' otherlv_1= '{' ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) ) (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_operand_5_0= ruleInteractionOperand ) )+ otherlv_6= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:855:1: (otherlv_0= 'combinedFragment' otherlv_1= '{' ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) ) (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_operand_5_0= ruleInteractionOperand ) )+ otherlv_6= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:855:3: otherlv_0= 'combinedFragment' otherlv_1= '{' ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) ) (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_operand_5_0= ruleInteractionOperand ) )+ otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleCombinedFragment1841); 

                	newLeafNode(otherlv_0, grammarAccess.getCombinedFragmentAccess().getCombinedFragmentKeyword_0());
                
            otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleCombinedFragment1853); 

                	newLeafNode(otherlv_1, grammarAccess.getCombinedFragmentAccess().getLeftCurlyBracketKeyword_1());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:863:1: ( (lv_interactionOperator_2_0= ruleInteractionOperatorKind ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:864:1: (lv_interactionOperator_2_0= ruleInteractionOperatorKind )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:864:1: (lv_interactionOperator_2_0= ruleInteractionOperatorKind )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:865:3: lv_interactionOperator_2_0= ruleInteractionOperatorKind
            {
             
            	        newCompositeNode(grammarAccess.getCombinedFragmentAccess().getInteractionOperatorInteractionOperatorKindEnumRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleInteractionOperatorKind_in_ruleCombinedFragment1874);
            lv_interactionOperator_2_0=ruleInteractionOperatorKind();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCombinedFragmentRule());
            	        }
                   		set(
                   			current, 
                   			"interactionOperator",
                    		lv_interactionOperator_2_0, 
                    		"InteractionOperatorKind");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:881:2: (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==25) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:881:4: otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,25,FOLLOW_25_in_ruleCombinedFragment1887); 

            	        	newLeafNode(otherlv_3, grammarAccess.getCombinedFragmentAccess().getCoveredKeyword_3_0());
            	        
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:885:1: ( (otherlv_4= RULE_ID ) )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:886:1: (otherlv_4= RULE_ID )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:886:1: (otherlv_4= RULE_ID )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:887:3: otherlv_4= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getCombinedFragmentRule());
            	    	        }
            	            
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCombinedFragment1907); 

            	    		newLeafNode(otherlv_4, grammarAccess.getCombinedFragmentAccess().getCoveredLifelineCrossReference_3_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:898:4: ( (lv_operand_5_0= ruleInteractionOperand ) )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==27) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:899:1: (lv_operand_5_0= ruleInteractionOperand )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:899:1: (lv_operand_5_0= ruleInteractionOperand )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:900:3: lv_operand_5_0= ruleInteractionOperand
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCombinedFragmentAccess().getOperandInteractionOperandParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleInteractionOperand_in_ruleCombinedFragment1930);
            	    lv_operand_5_0=ruleInteractionOperand();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCombinedFragmentRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"operand",
            	            		lv_operand_5_0, 
            	            		"InteractionOperand");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);

            otherlv_6=(Token)match(input,13,FOLLOW_13_in_ruleCombinedFragment1943); 

                	newLeafNode(otherlv_6, grammarAccess.getCombinedFragmentAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCombinedFragment"


    // $ANTLR start "entryRuleComment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:928:1: entryRuleComment returns [EObject current=null] : iv_ruleComment= ruleComment EOF ;
    public final EObject entryRuleComment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComment = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:929:2: (iv_ruleComment= ruleComment EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:930:2: iv_ruleComment= ruleComment EOF
            {
             newCompositeNode(grammarAccess.getCommentRule()); 
            pushFollow(FOLLOW_ruleComment_in_entryRuleComment1979);
            iv_ruleComment=ruleComment();

            state._fsp--;

             current =iv_ruleComment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleComment1989); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComment"


    // $ANTLR start "ruleComment"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:937:1: ruleComment returns [EObject current=null] : (otherlv_0= 'comment' otherlv_1= '{' ( (lv_body_2_0= RULE_STRING ) ) otherlv_3= '}' ) ;
    public final EObject ruleComment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_body_2_0=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:940:28: ( (otherlv_0= 'comment' otherlv_1= '{' ( (lv_body_2_0= RULE_STRING ) ) otherlv_3= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:941:1: (otherlv_0= 'comment' otherlv_1= '{' ( (lv_body_2_0= RULE_STRING ) ) otherlv_3= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:941:1: (otherlv_0= 'comment' otherlv_1= '{' ( (lv_body_2_0= RULE_STRING ) ) otherlv_3= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:941:3: otherlv_0= 'comment' otherlv_1= '{' ( (lv_body_2_0= RULE_STRING ) ) otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26_in_ruleComment2026); 

                	newLeafNode(otherlv_0, grammarAccess.getCommentAccess().getCommentKeyword_0());
                
            otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleComment2038); 

                	newLeafNode(otherlv_1, grammarAccess.getCommentAccess().getLeftCurlyBracketKeyword_1());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:949:1: ( (lv_body_2_0= RULE_STRING ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:950:1: (lv_body_2_0= RULE_STRING )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:950:1: (lv_body_2_0= RULE_STRING )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:951:3: lv_body_2_0= RULE_STRING
            {
            lv_body_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleComment2055); 

            			newLeafNode(lv_body_2_0, grammarAccess.getCommentAccess().getBodySTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCommentRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"body",
                    		lv_body_2_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleComment2072); 

                	newLeafNode(otherlv_3, grammarAccess.getCommentAccess().getRightCurlyBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComment"


    // $ANTLR start "entryRuleInteractionOperand"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:979:1: entryRuleInteractionOperand returns [EObject current=null] : iv_ruleInteractionOperand= ruleInteractionOperand EOF ;
    public final EObject entryRuleInteractionOperand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionOperand = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:980:2: (iv_ruleInteractionOperand= ruleInteractionOperand EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:981:2: iv_ruleInteractionOperand= ruleInteractionOperand EOF
            {
             newCompositeNode(grammarAccess.getInteractionOperandRule()); 
            pushFollow(FOLLOW_ruleInteractionOperand_in_entryRuleInteractionOperand2108);
            iv_ruleInteractionOperand=ruleInteractionOperand();

            state._fsp--;

             current =iv_ruleInteractionOperand; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionOperand2118); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionOperand"


    // $ANTLR start "ruleInteractionOperand"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:988:1: ruleInteractionOperand returns [EObject current=null] : (otherlv_0= 'interactionOperand' () otherlv_2= '{' (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_guard_5_0= ruleInteractionConstraint ) )? ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' ) ;
    public final EObject ruleInteractionOperand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        EObject lv_guard_5_0 = null;

        EObject lv_fragment_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:991:28: ( (otherlv_0= 'interactionOperand' () otherlv_2= '{' (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_guard_5_0= ruleInteractionConstraint ) )? ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:992:1: (otherlv_0= 'interactionOperand' () otherlv_2= '{' (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_guard_5_0= ruleInteractionConstraint ) )? ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:992:1: (otherlv_0= 'interactionOperand' () otherlv_2= '{' (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_guard_5_0= ruleInteractionConstraint ) )? ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}' )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:992:3: otherlv_0= 'interactionOperand' () otherlv_2= '{' (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )* ( (lv_guard_5_0= ruleInteractionConstraint ) )? ( (lv_fragment_6_0= ruleInteractionFragment ) )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleInteractionOperand2155); 

                	newLeafNode(otherlv_0, grammarAccess.getInteractionOperandAccess().getInteractionOperandKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:996:1: ()
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:997:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getInteractionOperandAccess().getInteractionOperandAction_1(),
                        current);
                

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleInteractionOperand2176); 

                	newLeafNode(otherlv_2, grammarAccess.getInteractionOperandAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1006:1: (otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==25) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1006:3: otherlv_3= 'covered=' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,25,FOLLOW_25_in_ruleInteractionOperand2189); 

            	        	newLeafNode(otherlv_3, grammarAccess.getInteractionOperandAccess().getCoveredKeyword_3_0());
            	        
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1010:1: ( (otherlv_4= RULE_ID ) )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1011:1: (otherlv_4= RULE_ID )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1011:1: (otherlv_4= RULE_ID )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1012:3: otherlv_4= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getInteractionOperandRule());
            	    	        }
            	            
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInteractionOperand2209); 

            	    		newLeafNode(otherlv_4, grammarAccess.getInteractionOperandAccess().getCoveredLifelineCrossReference_3_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1023:4: ( (lv_guard_5_0= ruleInteractionConstraint ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==28) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1024:1: (lv_guard_5_0= ruleInteractionConstraint )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1024:1: (lv_guard_5_0= ruleInteractionConstraint )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1025:3: lv_guard_5_0= ruleInteractionConstraint
                    {
                     
                    	        newCompositeNode(grammarAccess.getInteractionOperandAccess().getGuardInteractionConstraintParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_ruleInteractionConstraint_in_ruleInteractionOperand2232);
                    lv_guard_5_0=ruleInteractionConstraint();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInteractionOperandRule());
                    	        }
                           		set(
                           			current, 
                           			"guard",
                            		lv_guard_5_0, 
                            		"InteractionConstraint");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1041:3: ( (lv_fragment_6_0= ruleInteractionFragment ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==24||LA18_0==35) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1042:1: (lv_fragment_6_0= ruleInteractionFragment )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1042:1: (lv_fragment_6_0= ruleInteractionFragment )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1043:3: lv_fragment_6_0= ruleInteractionFragment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInteractionOperandAccess().getFragmentInteractionFragmentParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleInteractionFragment_in_ruleInteractionOperand2254);
            	    lv_fragment_6_0=ruleInteractionFragment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInteractionOperandRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"fragment",
            	            		lv_fragment_6_0, 
            	            		"InteractionFragment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_13_in_ruleInteractionOperand2267); 

                	newLeafNode(otherlv_7, grammarAccess.getInteractionOperandAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionOperand"


    // $ANTLR start "entryRuleInteractionConstraint"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1071:1: entryRuleInteractionConstraint returns [EObject current=null] : iv_ruleInteractionConstraint= ruleInteractionConstraint EOF ;
    public final EObject entryRuleInteractionConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionConstraint = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1072:2: (iv_ruleInteractionConstraint= ruleInteractionConstraint EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1073:2: iv_ruleInteractionConstraint= ruleInteractionConstraint EOF
            {
             newCompositeNode(grammarAccess.getInteractionConstraintRule()); 
            pushFollow(FOLLOW_ruleInteractionConstraint_in_entryRuleInteractionConstraint2303);
            iv_ruleInteractionConstraint=ruleInteractionConstraint();

            state._fsp--;

             current =iv_ruleInteractionConstraint; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInteractionConstraint2313); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionConstraint"


    // $ANTLR start "ruleInteractionConstraint"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1080:1: ruleInteractionConstraint returns [EObject current=null] : (otherlv_0= 'interactionConstraint' ( (lv_name_1_0= RULE_ID ) ) ( (lv_minint_2_0= ruleValueSpecification ) ) ( (lv_maxint_3_0= ruleValueSpecification ) ) ( (lv_specification_4_0= ruleValueSpecification ) ) ) ;
    public final EObject ruleInteractionConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_minint_2_0 = null;

        EObject lv_maxint_3_0 = null;

        EObject lv_specification_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1083:28: ( (otherlv_0= 'interactionConstraint' ( (lv_name_1_0= RULE_ID ) ) ( (lv_minint_2_0= ruleValueSpecification ) ) ( (lv_maxint_3_0= ruleValueSpecification ) ) ( (lv_specification_4_0= ruleValueSpecification ) ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1084:1: (otherlv_0= 'interactionConstraint' ( (lv_name_1_0= RULE_ID ) ) ( (lv_minint_2_0= ruleValueSpecification ) ) ( (lv_maxint_3_0= ruleValueSpecification ) ) ( (lv_specification_4_0= ruleValueSpecification ) ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1084:1: (otherlv_0= 'interactionConstraint' ( (lv_name_1_0= RULE_ID ) ) ( (lv_minint_2_0= ruleValueSpecification ) ) ( (lv_maxint_3_0= ruleValueSpecification ) ) ( (lv_specification_4_0= ruleValueSpecification ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1084:3: otherlv_0= 'interactionConstraint' ( (lv_name_1_0= RULE_ID ) ) ( (lv_minint_2_0= ruleValueSpecification ) ) ( (lv_maxint_3_0= ruleValueSpecification ) ) ( (lv_specification_4_0= ruleValueSpecification ) )
            {
            otherlv_0=(Token)match(input,28,FOLLOW_28_in_ruleInteractionConstraint2350); 

                	newLeafNode(otherlv_0, grammarAccess.getInteractionConstraintAccess().getInteractionConstraintKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1088:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1089:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1089:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1090:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInteractionConstraint2367); 

            			newLeafNode(lv_name_1_0, grammarAccess.getInteractionConstraintAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInteractionConstraintRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1106:2: ( (lv_minint_2_0= ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1107:1: (lv_minint_2_0= ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1107:1: (lv_minint_2_0= ruleValueSpecification )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1108:3: lv_minint_2_0= ruleValueSpecification
            {
             
            	        newCompositeNode(grammarAccess.getInteractionConstraintAccess().getMinintValueSpecificationParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2393);
            lv_minint_2_0=ruleValueSpecification();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteractionConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"minint",
                    		lv_minint_2_0, 
                    		"ValueSpecification");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1124:2: ( (lv_maxint_3_0= ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1125:1: (lv_maxint_3_0= ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1125:1: (lv_maxint_3_0= ruleValueSpecification )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1126:3: lv_maxint_3_0= ruleValueSpecification
            {
             
            	        newCompositeNode(grammarAccess.getInteractionConstraintAccess().getMaxintValueSpecificationParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2414);
            lv_maxint_3_0=ruleValueSpecification();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteractionConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"maxint",
                    		lv_maxint_3_0, 
                    		"ValueSpecification");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1142:2: ( (lv_specification_4_0= ruleValueSpecification ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1143:1: (lv_specification_4_0= ruleValueSpecification )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1143:1: (lv_specification_4_0= ruleValueSpecification )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1144:3: lv_specification_4_0= ruleValueSpecification
            {
             
            	        newCompositeNode(grammarAccess.getInteractionConstraintAccess().getSpecificationValueSpecificationParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2435);
            lv_specification_4_0=ruleValueSpecification();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteractionConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"specification",
                    		lv_specification_4_0, 
                    		"ValueSpecification");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionConstraint"


    // $ANTLR start "entryRuleLifeline"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1168:1: entryRuleLifeline returns [EObject current=null] : iv_ruleLifeline= ruleLifeline EOF ;
    public final EObject entryRuleLifeline() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLifeline = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1169:2: (iv_ruleLifeline= ruleLifeline EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1170:2: iv_ruleLifeline= ruleLifeline EOF
            {
             newCompositeNode(grammarAccess.getLifelineRule()); 
            pushFollow(FOLLOW_ruleLifeline_in_entryRuleLifeline2471);
            iv_ruleLifeline=ruleLifeline();

            state._fsp--;

             current =iv_ruleLifeline; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLifeline2481); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLifeline"


    // $ANTLR start "ruleLifeline"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1177:1: ruleLifeline returns [EObject current=null] : (otherlv_0= 'lifeline' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )? ) ;
    public final EObject ruleLifeline() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1180:28: ( (otherlv_0= 'lifeline' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )? ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1181:1: (otherlv_0= 'lifeline' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )? )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1181:1: (otherlv_0= 'lifeline' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )? )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1181:3: otherlv_0= 'lifeline' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )?
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleLifeline2518); 

                	newLeafNode(otherlv_0, grammarAccess.getLifelineAccess().getLifelineKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1185:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1186:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1186:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1187:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLifeline2535); 

            			newLeafNode(lv_name_1_0, grammarAccess.getLifelineAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLifelineRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1203:2: (otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==30) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1203:4: otherlv_2= 'represents=' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,30,FOLLOW_30_in_ruleLifeline2553); 

                        	newLeafNode(otherlv_2, grammarAccess.getLifelineAccess().getRepresentsKeyword_2_0());
                        
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1207:1: ( (otherlv_3= RULE_ID ) )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1208:1: (otherlv_3= RULE_ID )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1208:1: (otherlv_3= RULE_ID )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1209:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getLifelineRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLifeline2573); 

                    		newLeafNode(otherlv_3, grammarAccess.getLifelineAccess().getRepresentsPropertyCrossReference_2_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLifeline"


    // $ANTLR start "entryRuleMessage"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1228:1: entryRuleMessage returns [EObject current=null] : iv_ruleMessage= ruleMessage EOF ;
    public final EObject entryRuleMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessage = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1229:2: (iv_ruleMessage= ruleMessage EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1230:2: iv_ruleMessage= ruleMessage EOF
            {
             newCompositeNode(grammarAccess.getMessageRule()); 
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage2611);
            iv_ruleMessage=ruleMessage();

            state._fsp--;

             current =iv_ruleMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage2621); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1237:1: ruleMessage returns [EObject current=null] : (otherlv_0= 'message' ( (lv_messageSort_1_0= ruleMessageSort ) ) otherlv_2= 'receive=' ( ( ruleQualifiedName ) ) otherlv_4= 'send=' ( ( ruleQualifiedName ) ) otherlv_6= 'signature=' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Enumerator lv_messageSort_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1240:28: ( (otherlv_0= 'message' ( (lv_messageSort_1_0= ruleMessageSort ) ) otherlv_2= 'receive=' ( ( ruleQualifiedName ) ) otherlv_4= 'send=' ( ( ruleQualifiedName ) ) otherlv_6= 'signature=' ( ( ruleQualifiedName ) ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1241:1: (otherlv_0= 'message' ( (lv_messageSort_1_0= ruleMessageSort ) ) otherlv_2= 'receive=' ( ( ruleQualifiedName ) ) otherlv_4= 'send=' ( ( ruleQualifiedName ) ) otherlv_6= 'signature=' ( ( ruleQualifiedName ) ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1241:1: (otherlv_0= 'message' ( (lv_messageSort_1_0= ruleMessageSort ) ) otherlv_2= 'receive=' ( ( ruleQualifiedName ) ) otherlv_4= 'send=' ( ( ruleQualifiedName ) ) otherlv_6= 'signature=' ( ( ruleQualifiedName ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1241:3: otherlv_0= 'message' ( (lv_messageSort_1_0= ruleMessageSort ) ) otherlv_2= 'receive=' ( ( ruleQualifiedName ) ) otherlv_4= 'send=' ( ( ruleQualifiedName ) ) otherlv_6= 'signature=' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,31,FOLLOW_31_in_ruleMessage2658); 

                	newLeafNode(otherlv_0, grammarAccess.getMessageAccess().getMessageKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1245:1: ( (lv_messageSort_1_0= ruleMessageSort ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1246:1: (lv_messageSort_1_0= ruleMessageSort )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1246:1: (lv_messageSort_1_0= ruleMessageSort )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1247:3: lv_messageSort_1_0= ruleMessageSort
            {
             
            	        newCompositeNode(grammarAccess.getMessageAccess().getMessageSortMessageSortEnumRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleMessageSort_in_ruleMessage2679);
            lv_messageSort_1_0=ruleMessageSort();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMessageRule());
            	        }
                   		set(
                   			current, 
                   			"messageSort",
                    		lv_messageSort_1_0, 
                    		"MessageSort");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32_in_ruleMessage2691); 

                	newLeafNode(otherlv_2, grammarAccess.getMessageAccess().getReceiveKeyword_2());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1267:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1268:1: ( ruleQualifiedName )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1268:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1269:3: ruleQualifiedName
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMessageRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getMessageAccess().getReceiveEventMessageOccurrenceSpecificationCrossReference_3_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleMessage2714);
            ruleQualifiedName();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,33,FOLLOW_33_in_ruleMessage2726); 

                	newLeafNode(otherlv_4, grammarAccess.getMessageAccess().getSendKeyword_4());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1286:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1287:1: ( ruleQualifiedName )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1287:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1288:3: ruleQualifiedName
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMessageRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getMessageAccess().getSendEventMessageOccurrenceSpecificationCrossReference_5_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleMessage2749);
            ruleQualifiedName();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,34,FOLLOW_34_in_ruleMessage2761); 

                	newLeafNode(otherlv_6, grammarAccess.getMessageAccess().getSignatureKeyword_6());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1305:1: ( ( ruleQualifiedName ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1306:1: ( ruleQualifiedName )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1306:1: ( ruleQualifiedName )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1307:3: ruleQualifiedName
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMessageRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getMessageAccess().getSignatureOperationCrossReference_7_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleMessage2784);
            ruleQualifiedName();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleMessageOccurrenceSpecification"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1328:1: entryRuleMessageOccurrenceSpecification returns [EObject current=null] : iv_ruleMessageOccurrenceSpecification= ruleMessageOccurrenceSpecification EOF ;
    public final EObject entryRuleMessageOccurrenceSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessageOccurrenceSpecification = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1329:2: (iv_ruleMessageOccurrenceSpecification= ruleMessageOccurrenceSpecification EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1330:2: iv_ruleMessageOccurrenceSpecification= ruleMessageOccurrenceSpecification EOF
            {
             newCompositeNode(grammarAccess.getMessageOccurrenceSpecificationRule()); 
            pushFollow(FOLLOW_ruleMessageOccurrenceSpecification_in_entryRuleMessageOccurrenceSpecification2820);
            iv_ruleMessageOccurrenceSpecification=ruleMessageOccurrenceSpecification();

            state._fsp--;

             current =iv_ruleMessageOccurrenceSpecification; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessageOccurrenceSpecification2830); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessageOccurrenceSpecification"


    // $ANTLR start "ruleMessageOccurrenceSpecification"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1337:1: ruleMessageOccurrenceSpecification returns [EObject current=null] : (otherlv_0= 'messageOccurrenceSpecification' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+ ) ;
    public final EObject ruleMessageOccurrenceSpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1340:28: ( (otherlv_0= 'messageOccurrenceSpecification' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+ ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1341:1: (otherlv_0= 'messageOccurrenceSpecification' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+ )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1341:1: (otherlv_0= 'messageOccurrenceSpecification' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+ )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1341:3: otherlv_0= 'messageOccurrenceSpecification' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleMessageOccurrenceSpecification2867); 

                	newLeafNode(otherlv_0, grammarAccess.getMessageOccurrenceSpecificationAccess().getMessageOccurrenceSpecificationKeyword_0());
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1345:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1346:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1346:1: (lv_name_1_0= RULE_ID )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1347:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMessageOccurrenceSpecification2884); 

            			newLeafNode(lv_name_1_0, grammarAccess.getMessageOccurrenceSpecificationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMessageOccurrenceSpecificationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1363:2: (otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) ) )+
            int cnt20=0;
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==25) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1363:4: otherlv_2= 'covered=' ( (otherlv_3= RULE_ID ) )
            	    {
            	    otherlv_2=(Token)match(input,25,FOLLOW_25_in_ruleMessageOccurrenceSpecification2902); 

            	        	newLeafNode(otherlv_2, grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredKeyword_2_0());
            	        
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1367:1: ( (otherlv_3= RULE_ID ) )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1368:1: (otherlv_3= RULE_ID )
            	    {
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1368:1: (otherlv_3= RULE_ID )
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1369:3: otherlv_3= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getMessageOccurrenceSpecificationRule());
            	    	        }
            	            
            	    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMessageOccurrenceSpecification2922); 

            	    		newLeafNode(otherlv_3, grammarAccess.getMessageOccurrenceSpecificationAccess().getCoveredLifelineCrossReference_2_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt20 >= 1 ) break loop20;
                        EarlyExitException eee =
                            new EarlyExitException(20, input);
                        throw eee;
                }
                cnt20++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageOccurrenceSpecification"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1388:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1389:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1390:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName2961);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName2972); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1397:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1400:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1401:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1401:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1401:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName3012); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
                
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1408:1: (kw= '.' this_ID_2= RULE_ID )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==36) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1409:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,36,FOLLOW_36_in_ruleQualifiedName3031); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName3046); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleInteractionOperatorKind"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1429:1: ruleInteractionOperatorKind returns [Enumerator current=null] : ( (enumLiteral_0= 'assert' ) | (enumLiteral_1= 'sec' ) | (enumLiteral_2= 'alt' ) | (enumLiteral_3= 'opt' ) | (enumLiteral_4= 'break' ) | (enumLiteral_5= 'par' ) | (enumLiteral_6= 'strict' ) | (enumLiteral_7= 'loop' ) | (enumLiteral_8= 'critical' ) | (enumLiteral_9= 'neg' ) | (enumLiteral_10= 'ignore' ) | (enumLiteral_11= 'consider' ) ) ;
    public final Enumerator ruleInteractionOperatorKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;

         enterRule(); 
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1431:28: ( ( (enumLiteral_0= 'assert' ) | (enumLiteral_1= 'sec' ) | (enumLiteral_2= 'alt' ) | (enumLiteral_3= 'opt' ) | (enumLiteral_4= 'break' ) | (enumLiteral_5= 'par' ) | (enumLiteral_6= 'strict' ) | (enumLiteral_7= 'loop' ) | (enumLiteral_8= 'critical' ) | (enumLiteral_9= 'neg' ) | (enumLiteral_10= 'ignore' ) | (enumLiteral_11= 'consider' ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1432:1: ( (enumLiteral_0= 'assert' ) | (enumLiteral_1= 'sec' ) | (enumLiteral_2= 'alt' ) | (enumLiteral_3= 'opt' ) | (enumLiteral_4= 'break' ) | (enumLiteral_5= 'par' ) | (enumLiteral_6= 'strict' ) | (enumLiteral_7= 'loop' ) | (enumLiteral_8= 'critical' ) | (enumLiteral_9= 'neg' ) | (enumLiteral_10= 'ignore' ) | (enumLiteral_11= 'consider' ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1432:1: ( (enumLiteral_0= 'assert' ) | (enumLiteral_1= 'sec' ) | (enumLiteral_2= 'alt' ) | (enumLiteral_3= 'opt' ) | (enumLiteral_4= 'break' ) | (enumLiteral_5= 'par' ) | (enumLiteral_6= 'strict' ) | (enumLiteral_7= 'loop' ) | (enumLiteral_8= 'critical' ) | (enumLiteral_9= 'neg' ) | (enumLiteral_10= 'ignore' ) | (enumLiteral_11= 'consider' ) )
            int alt22=12;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt22=1;
                }
                break;
            case 38:
                {
                alt22=2;
                }
                break;
            case 39:
                {
                alt22=3;
                }
                break;
            case 40:
                {
                alt22=4;
                }
                break;
            case 41:
                {
                alt22=5;
                }
                break;
            case 42:
                {
                alt22=6;
                }
                break;
            case 43:
                {
                alt22=7;
                }
                break;
            case 44:
                {
                alt22=8;
                }
                break;
            case 45:
                {
                alt22=9;
                }
                break;
            case 46:
                {
                alt22=10;
                }
                break;
            case 47:
                {
                alt22=11;
                }
                break;
            case 48:
                {
                alt22=12;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1432:2: (enumLiteral_0= 'assert' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1432:2: (enumLiteral_0= 'assert' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1432:4: enumLiteral_0= 'assert'
                    {
                    enumLiteral_0=(Token)match(input,37,FOLLOW_37_in_ruleInteractionOperatorKind3107); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getAssertEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getInteractionOperatorKindAccess().getAssertEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1438:6: (enumLiteral_1= 'sec' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1438:6: (enumLiteral_1= 'sec' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1438:8: enumLiteral_1= 'sec'
                    {
                    enumLiteral_1=(Token)match(input,38,FOLLOW_38_in_ruleInteractionOperatorKind3124); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getSeqEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getInteractionOperatorKindAccess().getSeqEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1444:6: (enumLiteral_2= 'alt' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1444:6: (enumLiteral_2= 'alt' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1444:8: enumLiteral_2= 'alt'
                    {
                    enumLiteral_2=(Token)match(input,39,FOLLOW_39_in_ruleInteractionOperatorKind3141); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getAltEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getInteractionOperatorKindAccess().getAltEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1450:6: (enumLiteral_3= 'opt' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1450:6: (enumLiteral_3= 'opt' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1450:8: enumLiteral_3= 'opt'
                    {
                    enumLiteral_3=(Token)match(input,40,FOLLOW_40_in_ruleInteractionOperatorKind3158); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getOptEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getInteractionOperatorKindAccess().getOptEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1456:6: (enumLiteral_4= 'break' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1456:6: (enumLiteral_4= 'break' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1456:8: enumLiteral_4= 'break'
                    {
                    enumLiteral_4=(Token)match(input,41,FOLLOW_41_in_ruleInteractionOperatorKind3175); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getBreakEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getInteractionOperatorKindAccess().getBreakEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1462:6: (enumLiteral_5= 'par' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1462:6: (enumLiteral_5= 'par' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1462:8: enumLiteral_5= 'par'
                    {
                    enumLiteral_5=(Token)match(input,42,FOLLOW_42_in_ruleInteractionOperatorKind3192); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getParEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getInteractionOperatorKindAccess().getParEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1468:6: (enumLiteral_6= 'strict' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1468:6: (enumLiteral_6= 'strict' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1468:8: enumLiteral_6= 'strict'
                    {
                    enumLiteral_6=(Token)match(input,43,FOLLOW_43_in_ruleInteractionOperatorKind3209); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getStrictEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getInteractionOperatorKindAccess().getStrictEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1474:6: (enumLiteral_7= 'loop' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1474:6: (enumLiteral_7= 'loop' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1474:8: enumLiteral_7= 'loop'
                    {
                    enumLiteral_7=(Token)match(input,44,FOLLOW_44_in_ruleInteractionOperatorKind3226); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getLoopEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getInteractionOperatorKindAccess().getLoopEnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;
                case 9 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1480:6: (enumLiteral_8= 'critical' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1480:6: (enumLiteral_8= 'critical' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1480:8: enumLiteral_8= 'critical'
                    {
                    enumLiteral_8=(Token)match(input,45,FOLLOW_45_in_ruleInteractionOperatorKind3243); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getCriticalEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_8, grammarAccess.getInteractionOperatorKindAccess().getCriticalEnumLiteralDeclaration_8()); 
                        

                    }


                    }
                    break;
                case 10 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1486:6: (enumLiteral_9= 'neg' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1486:6: (enumLiteral_9= 'neg' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1486:8: enumLiteral_9= 'neg'
                    {
                    enumLiteral_9=(Token)match(input,46,FOLLOW_46_in_ruleInteractionOperatorKind3260); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getNegEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_9, grammarAccess.getInteractionOperatorKindAccess().getNegEnumLiteralDeclaration_9()); 
                        

                    }


                    }
                    break;
                case 11 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1492:6: (enumLiteral_10= 'ignore' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1492:6: (enumLiteral_10= 'ignore' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1492:8: enumLiteral_10= 'ignore'
                    {
                    enumLiteral_10=(Token)match(input,47,FOLLOW_47_in_ruleInteractionOperatorKind3277); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getIgnoreEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_10, grammarAccess.getInteractionOperatorKindAccess().getIgnoreEnumLiteralDeclaration_10()); 
                        

                    }


                    }
                    break;
                case 12 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1498:6: (enumLiteral_11= 'consider' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1498:6: (enumLiteral_11= 'consider' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1498:8: enumLiteral_11= 'consider'
                    {
                    enumLiteral_11=(Token)match(input,48,FOLLOW_48_in_ruleInteractionOperatorKind3294); 

                            current = grammarAccess.getInteractionOperatorKindAccess().getConsiderEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_11, grammarAccess.getInteractionOperatorKindAccess().getConsiderEnumLiteralDeclaration_11()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionOperatorKind"


    // $ANTLR start "ruleMessageSort"
    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1508:1: ruleMessageSort returns [Enumerator current=null] : ( (enumLiteral_0= 'asynchCall' ) | (enumLiteral_1= 'synchCall' ) | (enumLiteral_2= 'createMessage' ) | (enumLiteral_3= 'deleteMessage' ) | (enumLiteral_4= 'reply' ) | (enumLiteral_5= 'asynchSignal' ) ) ;
    public final Enumerator ruleMessageSort() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;

         enterRule(); 
        try {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1510:28: ( ( (enumLiteral_0= 'asynchCall' ) | (enumLiteral_1= 'synchCall' ) | (enumLiteral_2= 'createMessage' ) | (enumLiteral_3= 'deleteMessage' ) | (enumLiteral_4= 'reply' ) | (enumLiteral_5= 'asynchSignal' ) ) )
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1511:1: ( (enumLiteral_0= 'asynchCall' ) | (enumLiteral_1= 'synchCall' ) | (enumLiteral_2= 'createMessage' ) | (enumLiteral_3= 'deleteMessage' ) | (enumLiteral_4= 'reply' ) | (enumLiteral_5= 'asynchSignal' ) )
            {
            // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1511:1: ( (enumLiteral_0= 'asynchCall' ) | (enumLiteral_1= 'synchCall' ) | (enumLiteral_2= 'createMessage' ) | (enumLiteral_3= 'deleteMessage' ) | (enumLiteral_4= 'reply' ) | (enumLiteral_5= 'asynchSignal' ) )
            int alt23=6;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt23=1;
                }
                break;
            case 50:
                {
                alt23=2;
                }
                break;
            case 51:
                {
                alt23=3;
                }
                break;
            case 52:
                {
                alt23=4;
                }
                break;
            case 53:
                {
                alt23=5;
                }
                break;
            case 54:
                {
                alt23=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1511:2: (enumLiteral_0= 'asynchCall' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1511:2: (enumLiteral_0= 'asynchCall' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1511:4: enumLiteral_0= 'asynchCall'
                    {
                    enumLiteral_0=(Token)match(input,49,FOLLOW_49_in_ruleMessageSort3339); 

                            current = grammarAccess.getMessageSortAccess().getAsynchCallEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getMessageSortAccess().getAsynchCallEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1517:6: (enumLiteral_1= 'synchCall' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1517:6: (enumLiteral_1= 'synchCall' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1517:8: enumLiteral_1= 'synchCall'
                    {
                    enumLiteral_1=(Token)match(input,50,FOLLOW_50_in_ruleMessageSort3356); 

                            current = grammarAccess.getMessageSortAccess().getSynchCallEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getMessageSortAccess().getSynchCallEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1523:6: (enumLiteral_2= 'createMessage' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1523:6: (enumLiteral_2= 'createMessage' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1523:8: enumLiteral_2= 'createMessage'
                    {
                    enumLiteral_2=(Token)match(input,51,FOLLOW_51_in_ruleMessageSort3373); 

                            current = grammarAccess.getMessageSortAccess().getCreateMessageEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getMessageSortAccess().getCreateMessageEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1529:6: (enumLiteral_3= 'deleteMessage' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1529:6: (enumLiteral_3= 'deleteMessage' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1529:8: enumLiteral_3= 'deleteMessage'
                    {
                    enumLiteral_3=(Token)match(input,52,FOLLOW_52_in_ruleMessageSort3390); 

                            current = grammarAccess.getMessageSortAccess().getDeleteMessageEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getMessageSortAccess().getDeleteMessageEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1535:6: (enumLiteral_4= 'reply' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1535:6: (enumLiteral_4= 'reply' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1535:8: enumLiteral_4= 'reply'
                    {
                    enumLiteral_4=(Token)match(input,53,FOLLOW_53_in_ruleMessageSort3407); 

                            current = grammarAccess.getMessageSortAccess().getReplyEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getMessageSortAccess().getReplyEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1541:6: (enumLiteral_5= 'asynchSignal' )
                    {
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1541:6: (enumLiteral_5= 'asynchSignal' )
                    // ../org.scenariotools.msd.xtext/src-gen/org/scenariotools/msd/xtext/parser/antlr/internal/InternalUml.g:1541:8: enumLiteral_5= 'asynchSignal'
                    {
                    enumLiteral_5=(Token)match(input,54,FOLLOW_54_in_ruleMessageSort3424); 

                            current = grammarAccess.getMessageSortAccess().getAsynchSignalEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getMessageSortAccess().getAsynchSignalEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageSort"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_ruleModel130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_entryRulePackage165 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackage175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulePackage212 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePackage229 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePackage246 = new BitSet(new long[]{0x0000000000406800L});
    public static final BitSet FOLLOW_rulePackageableElement_in_rulePackage267 = new BitSet(new long[]{0x0000000000406800L});
    public static final BitSet FOLLOW_13_in_rulePackage280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackageableElement_in_entryRulePackageableElement316 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackageableElement326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackage_in_rulePackageableElement373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_rulePackageableElement400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCollaboration_in_rulePackageableElement427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_entryRuleClass462 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClass472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleClass509 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleClass526 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleClass543 = new BitSet(new long[]{0x000000000002A000L});
    public static final BitSet FOLLOW_ruleOperation_in_ruleClass564 = new BitSet(new long[]{0x000000000002A000L});
    public static final BitSet FOLLOW_ruleProperty_in_ruleClass586 = new BitSet(new long[]{0x0000000000022000L});
    public static final BitSet FOLLOW_13_in_ruleClass599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOperation_in_entryRuleOperation635 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOperation645 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleOperation682 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOperation699 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleOperation716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProperty_in_entryRuleProperty752 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProperty762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleProperty799 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProperty816 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_18_in_ruleProperty834 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProperty854 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleProperty868 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_ruleProperty889 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleProperty901 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_ruleProperty922 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleProperty934 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleProperty946 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_entryRuleValueSpecification982 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleValueSpecification992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralUnlimitedNatural_in_ruleValueSpecification1039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralString_in_ruleValueSpecification1066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralUnlimitedNatural_in_entryRuleLiteralUnlimitedNatural1103 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteralUnlimitedNatural1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleLiteralUnlimitedNatural1154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralString_in_entryRuleLiteralString1194 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteralString1204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleLiteralString1245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCollaboration_in_entryRuleCollaboration1285 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCollaboration1295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleCollaboration1332 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCollaboration1349 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleCollaboration1366 = new BitSet(new long[]{0x0000000000822000L});
    public static final BitSet FOLLOW_ruleProperty_in_ruleCollaboration1387 = new BitSet(new long[]{0x0000000000822000L});
    public static final BitSet FOLLOW_ruleInteraction_in_ruleCollaboration1409 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_13_in_ruleCollaboration1422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteraction_in_entryRuleInteraction1458 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteraction1468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleInteraction1505 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInteraction1522 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleInteraction1539 = new BitSet(new long[]{0x00000008A5002000L});
    public static final BitSet FOLLOW_ruleComment_in_ruleInteraction1560 = new BitSet(new long[]{0x00000008A5002000L});
    public static final BitSet FOLLOW_ruleLifeline_in_ruleInteraction1582 = new BitSet(new long[]{0x00000008A1002000L});
    public static final BitSet FOLLOW_ruleMessage_in_ruleInteraction1604 = new BitSet(new long[]{0x0000000881002000L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_ruleInteraction1626 = new BitSet(new long[]{0x0000000801002000L});
    public static final BitSet FOLLOW_13_in_ruleInteraction1639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_entryRuleInteractionFragment1675 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionFragment1685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageOccurrenceSpecification_in_ruleInteractionFragment1732 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCombinedFragment_in_ruleInteractionFragment1759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCombinedFragment_in_entryRuleCombinedFragment1794 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCombinedFragment1804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleCombinedFragment1841 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleCombinedFragment1853 = new BitSet(new long[]{0x0001FFE000000000L});
    public static final BitSet FOLLOW_ruleInteractionOperatorKind_in_ruleCombinedFragment1874 = new BitSet(new long[]{0x000000000A000000L});
    public static final BitSet FOLLOW_25_in_ruleCombinedFragment1887 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCombinedFragment1907 = new BitSet(new long[]{0x000000000A000000L});
    public static final BitSet FOLLOW_ruleInteractionOperand_in_ruleCombinedFragment1930 = new BitSet(new long[]{0x000000000A002000L});
    public static final BitSet FOLLOW_13_in_ruleCombinedFragment1943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComment_in_entryRuleComment1979 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComment1989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleComment2026 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleComment2038 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleComment2055 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleComment2072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionOperand_in_entryRuleInteractionOperand2108 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionOperand2118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleInteractionOperand2155 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleInteractionOperand2176 = new BitSet(new long[]{0x0000000813002000L});
    public static final BitSet FOLLOW_25_in_ruleInteractionOperand2189 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInteractionOperand2209 = new BitSet(new long[]{0x0000000813002000L});
    public static final BitSet FOLLOW_ruleInteractionConstraint_in_ruleInteractionOperand2232 = new BitSet(new long[]{0x0000000801002000L});
    public static final BitSet FOLLOW_ruleInteractionFragment_in_ruleInteractionOperand2254 = new BitSet(new long[]{0x0000000801002000L});
    public static final BitSet FOLLOW_13_in_ruleInteractionOperand2267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInteractionConstraint_in_entryRuleInteractionConstraint2303 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInteractionConstraint2313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleInteractionConstraint2350 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInteractionConstraint2367 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2393 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2414 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_ruleValueSpecification_in_ruleInteractionConstraint2435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLifeline_in_entryRuleLifeline2471 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLifeline2481 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleLifeline2518 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLifeline2535 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_ruleLifeline2553 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLifeline2573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage2611 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage2621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleMessage2658 = new BitSet(new long[]{0x007E000000000000L});
    public static final BitSet FOLLOW_ruleMessageSort_in_ruleMessage2679 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleMessage2691 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleMessage2714 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleMessage2726 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleMessage2749 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_ruleMessage2761 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleMessage2784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageOccurrenceSpecification_in_entryRuleMessageOccurrenceSpecification2820 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessageOccurrenceSpecification2830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleMessageOccurrenceSpecification2867 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMessageOccurrenceSpecification2884 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleMessageOccurrenceSpecification2902 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMessageOccurrenceSpecification2922 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName2961 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName2972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName3012 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_36_in_ruleQualifiedName3031 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName3046 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_37_in_ruleInteractionOperatorKind3107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleInteractionOperatorKind3124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleInteractionOperatorKind3141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleInteractionOperatorKind3158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleInteractionOperatorKind3175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleInteractionOperatorKind3192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleInteractionOperatorKind3209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleInteractionOperatorKind3226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleInteractionOperatorKind3243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleInteractionOperatorKind3260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_ruleInteractionOperatorKind3277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleInteractionOperatorKind3294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleMessageSort3339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleMessageSort3356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleMessageSort3373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleMessageSort3390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleMessageSort3407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleMessageSort3424 = new BitSet(new long[]{0x0000000000000002L});

}