<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="simulation" nsURI="http://org.scenariotools.msd.simulation"
    nsPrefix="simulation">
  <eClassifiers xsi:type="ecore:EClass" name="SimulationManager">
    <eOperations name="performNextStepFromSimulationAgent">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="shall be called by the (current) simulation runtime agent while not terminated"/>
      </eAnnotations>
      <eParameters name="simulationAgent" eType="#//SimulationAgent"/>
    </eOperations>
    <eOperations name="wait">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="shall be called by the (current) simulation runtime agent to indicate the end of its superstep"/>
      </eAnnotations>
      <eParameters name="simulationAgent" eType="#//SimulationAgent"/>
    </eOperations>
    <eOperations name="terminate"/>
    <eOperations name="loadStateFromHistoryAgent">
      <eParameters name="historyAgent" eType="#//HistoryAgent"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="pause" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="terminated" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"
        changeable="false"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="stepDelayMilliseconds"
        eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EInt"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="currentSystemSimulationAgent"
        eType="#//SimulationAgent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="currentHistoryAgent" eType="#//HistoryAgent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="registeredSystemSimulationAgent"
        upperBound="-1" eType="#//SimulationAgent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="registeredHistoryAgent"
        upperBound="-1" eType="#//HistoryAgent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="currentEnvironmentSimulationAgent"
        eType="#//SimulationAgent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="registeredEnvironmentSimulationAgent"
        upperBound="-1" eType="#//SimulationAgent"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="registeredActiveSimulationAgentChangeListener"
        upperBound="-1" eType="#//IActiveSimulationAgentChangeListener" transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activeSimulationAgent"
        eType="#//SimulationAgent"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="registeredStepPerformedListener"
        upperBound="-1" eType="#//IStepPerformedListener" transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="msdRuntimeStateGraph" eType="ecore:EClass ../../org.scenariotools.msd.runtime/model/runtime.ecore#//MSDRuntimeStateGraph"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="currentMSDRuntimeState"
        eType="ecore:EClass ../../org.scenariotools.msd.runtime/model/runtime.ecore#//MSDRuntimeState"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="scenarioRunConfiguration"
        eType="ecore:EClass ../../org.scenariotools.msd.runtime/model/scenariorunconfiguration.ecore#//ScenarioRunConfiguration"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="currentExecutionSemantics"
        eType="ecore:EEnum ../../org.scenariotools.msd.runtime/model/runtime.ecore#//ExecutionSemantics"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="SimulationAgent" abstract="true">
    <eOperations name="init"/>
    <eOperations name="terminate"/>
    <eOperations name="stepMade"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="simulationManager" eType="#//SimulationManager"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="nextEvent" eType="ecore:EClass ../../org.scenariotools.events/model/events.ecore#//Event">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="can be a message event or a wait event"/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="DefaultSystemSimulationAgent" eSuperTypes="#//UserInteractingSimulationAgent"/>
  <eClassifiers xsi:type="ecore:EClass" name="DefaultEnvironmentSimulationAgent" eSuperTypes="#//UserInteractingSimulationAgent"/>
  <eClassifiers xsi:type="ecore:EClass" name="UserInteractingSimulationAgent" eSuperTypes="#//SimulationAgent">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="registeredMSDModalEventListChangeListener"
        upperBound="-1" eType="#//IMSDModalMessageEventListChangeListener" transient="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EDataType" name="IMSDModalMessageEventListChangeListener"
      instanceClassName="org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="IStepPerformedListener" instanceClassName="org.scenariotools.msd.simulation.listener.IStepPerformedListener"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="IActiveSimulationAgentChangeListener"
      instanceClassName="org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="ISimulationTraceChangeListener" instanceClassName="org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EClass" name="HistoryAgent">
    <eOperations name="init"/>
    <eOperations name="terminate"/>
    <eOperations name="stepMade"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="nextState" eType="ecore:EClass ../../org.scenariotools.msd.runtime/model/runtime.ecore#//MSDRuntimeState"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="simulationManager" eType="#//SimulationManager"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="registeredSimulationTraceChangeListener"
        upperBound="-1" eType="#//ISimulationTraceChangeListener" transient="true"/>
  </eClassifiers>
</ecore:EPackage>
