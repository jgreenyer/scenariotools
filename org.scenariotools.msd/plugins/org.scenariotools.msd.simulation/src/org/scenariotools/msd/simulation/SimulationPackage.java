/**
 */
package org.scenariotools.msd.simulation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.simulation.SimulationFactory
 * @model kind="package"
 * @generated
 */
public interface SimulationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "simulation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.simulation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "simulation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SimulationPackage eINSTANCE = org.scenariotools.msd.simulation.impl.SimulationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.SimulationManagerImpl <em>Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.SimulationManagerImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getSimulationManager()
	 * @generated
	 */
	int SIMULATION_MANAGER = 0;

	/**
	 * The feature id for the '<em><b>Pause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__PAUSE = 0;

	/**
	 * The feature id for the '<em><b>Terminated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__TERMINATED = 1;

	/**
	 * The feature id for the '<em><b>Step Delay Milliseconds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS = 2;

	/**
	 * The feature id for the '<em><b>Current System Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT = 3;

	/**
	 * The feature id for the '<em><b>Current History Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__CURRENT_HISTORY_AGENT = 4;

	/**
	 * The feature id for the '<em><b>Registered System Simulation Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT = 5;

	/**
	 * The feature id for the '<em><b>Registered History Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT = 6;

	/**
	 * The feature id for the '<em><b>Current Environment Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT = 7;

	/**
	 * The feature id for the '<em><b>Registered Environment Simulation Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT = 8;

	/**
	 * The feature id for the '<em><b>Registered Active Simulation Agent Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER = 9;

	/**
	 * The feature id for the '<em><b>Active Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT = 10;

	/**
	 * The feature id for the '<em><b>Registered Step Performed Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER = 11;

	/**
	 * The feature id for the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__MSD_RUNTIME_STATE_GRAPH = 12;

	/**
	 * The feature id for the '<em><b>Current MSD Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__CURRENT_MSD_RUNTIME_STATE = 13;

	/**
	 * The feature id for the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION = 14;

	/**
	 * The feature id for the '<em><b>Current Execution Semantics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER__CURRENT_EXECUTION_SEMANTICS = 15;

	/**
	 * The number of structural features of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_MANAGER_FEATURE_COUNT = 16;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.SimulationAgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.SimulationAgentImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getSimulationAgent()
	 * @generated
	 */
	int SIMULATION_AGENT = 1;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_AGENT__SIMULATION_MANAGER = 0;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_AGENT__NEXT_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_AGENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.UserInteractingSimulationAgentImpl <em>User Interacting Simulation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.UserInteractingSimulationAgentImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getUserInteractingSimulationAgent()
	 * @generated
	 */
	int USER_INTERACTING_SIMULATION_AGENT = 4;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INTERACTING_SIMULATION_AGENT__SIMULATION_MANAGER = SIMULATION_AGENT__SIMULATION_MANAGER;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INTERACTING_SIMULATION_AGENT__NEXT_EVENT = SIMULATION_AGENT__NEXT_EVENT;

	/**
	 * The feature id for the '<em><b>Registered MSD Modal Event List Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER = SIMULATION_AGENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Interacting Simulation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INTERACTING_SIMULATION_AGENT_FEATURE_COUNT = SIMULATION_AGENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.DefaultSystemSimulationAgentImpl <em>Default System Simulation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.DefaultSystemSimulationAgentImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getDefaultSystemSimulationAgent()
	 * @generated
	 */
	int DEFAULT_SYSTEM_SIMULATION_AGENT = 2;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_SYSTEM_SIMULATION_AGENT__SIMULATION_MANAGER = USER_INTERACTING_SIMULATION_AGENT__SIMULATION_MANAGER;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_SYSTEM_SIMULATION_AGENT__NEXT_EVENT = USER_INTERACTING_SIMULATION_AGENT__NEXT_EVENT;

	/**
	 * The feature id for the '<em><b>Registered MSD Modal Event List Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_SYSTEM_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER = USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER;

	/**
	 * The number of structural features of the '<em>Default System Simulation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_SYSTEM_SIMULATION_AGENT_FEATURE_COUNT = USER_INTERACTING_SIMULATION_AGENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.DefaultEnvironmentSimulationAgentImpl <em>Default Environment Simulation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.DefaultEnvironmentSimulationAgentImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getDefaultEnvironmentSimulationAgent()
	 * @generated
	 */
	int DEFAULT_ENVIRONMENT_SIMULATION_AGENT = 3;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ENVIRONMENT_SIMULATION_AGENT__SIMULATION_MANAGER = USER_INTERACTING_SIMULATION_AGENT__SIMULATION_MANAGER;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ENVIRONMENT_SIMULATION_AGENT__NEXT_EVENT = USER_INTERACTING_SIMULATION_AGENT__NEXT_EVENT;

	/**
	 * The feature id for the '<em><b>Registered MSD Modal Event List Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ENVIRONMENT_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER = USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER;

	/**
	 * The number of structural features of the '<em>Default Environment Simulation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_ENVIRONMENT_SIMULATION_AGENT_FEATURE_COUNT = USER_INTERACTING_SIMULATION_AGENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.simulation.impl.HistoryAgentImpl <em>History Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.impl.HistoryAgentImpl
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getHistoryAgent()
	 * @generated
	 */
	int HISTORY_AGENT = 5;

	/**
	 * The feature id for the '<em><b>Next State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_AGENT__NEXT_STATE = 0;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_AGENT__SIMULATION_MANAGER = 1;

	/**
	 * The feature id for the '<em><b>Registered Simulation Trace Change Listener</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER = 2;

	/**
	 * The number of structural features of the '<em>History Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_AGENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '<em>IMSD Modal Message Event List Change Listener</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIMSDModalMessageEventListChangeListener()
	 * @generated
	 */
	int IMSD_MODAL_MESSAGE_EVENT_LIST_CHANGE_LISTENER = 6;

	/**
	 * The meta object id for the '<em>IStep Performed Listener</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.listener.IStepPerformedListener
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIStepPerformedListener()
	 * @generated
	 */
	int ISTEP_PERFORMED_LISTENER = 7;

	/**
	 * The meta object id for the '<em>IActive Simulation Agent Change Listener</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIActiveSimulationAgentChangeListener()
	 * @generated
	 */
	int IACTIVE_SIMULATION_AGENT_CHANGE_LISTENER = 8;


	/**
	 * The meta object id for the '<em>ISimulation Trace Change Listener</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener
	 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getISimulationTraceChangeListener()
	 * @generated
	 */
	int ISIMULATION_TRACE_CHANGE_LISTENER = 9;

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.SimulationManager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manager</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager
	 * @generated
	 */
	EClass getSimulationManager();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.simulation.SimulationManager#isPause <em>Pause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pause</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#isPause()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_Pause();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.simulation.SimulationManager#isTerminated <em>Terminated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Terminated</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#isTerminated()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_Terminated();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.simulation.SimulationManager#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Delay Milliseconds</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getStepDelayMilliseconds()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_StepDelayMilliseconds();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current System Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getCurrentSystemSimulationAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_CurrentSystemSimulationAgent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentHistoryAgent <em>Current History Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current History Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getCurrentHistoryAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_CurrentHistoryAgent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredSystemSimulationAgent <em>Registered System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Registered System Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getRegisteredSystemSimulationAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_RegisteredSystemSimulationAgent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredHistoryAgent <em>Registered History Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Registered History Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getRegisteredHistoryAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_RegisteredHistoryAgent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Environment Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getCurrentEnvironmentSimulationAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_CurrentEnvironmentSimulationAgent();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredEnvironmentSimulationAgent <em>Registered Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Registered Environment Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getRegisteredEnvironmentSimulationAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_RegisteredEnvironmentSimulationAgent();

	/**
	 * Returns the meta object for the attribute list '{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredActiveSimulationAgentChangeListener <em>Registered Active Simulation Agent Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Registered Active Simulation Agent Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getRegisteredActiveSimulationAgentChangeListener()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_RegisteredActiveSimulationAgentChangeListener();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getActiveSimulationAgent <em>Active Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getActiveSimulationAgent()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_ActiveSimulationAgent();

	/**
	 * Returns the meta object for the attribute list '{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredStepPerformedListener <em>Registered Step Performed Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Registered Step Performed Listener</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getRegisteredStepPerformedListener()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_RegisteredStepPerformedListener();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Msd Runtime State Graph</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getMsdRuntimeStateGraph()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_MsdRuntimeStateGraph();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentMSDRuntimeState <em>Current MSD Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current MSD Runtime State</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getCurrentMSDRuntimeState()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_CurrentMSDRuntimeState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationManager#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario Run Configuration</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getScenarioRunConfiguration()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EReference getSimulationManager_ScenarioRunConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentExecutionSemantics <em>Current Execution Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Execution Semantics</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationManager#getCurrentExecutionSemantics()
	 * @see #getSimulationManager()
	 * @generated
	 */
	EAttribute getSimulationManager_CurrentExecutionSemantics();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.SimulationAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationAgent
	 * @generated
	 */
	EClass getSimulationAgent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationAgent#getSimulationManager <em>Simulation Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Simulation Manager</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationAgent#getSimulationManager()
	 * @see #getSimulationAgent()
	 * @generated
	 */
	EReference getSimulationAgent_SimulationManager();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.SimulationAgent#getNextEvent <em>Next Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Event</em>'.
	 * @see org.scenariotools.msd.simulation.SimulationAgent#getNextEvent()
	 * @see #getSimulationAgent()
	 * @generated
	 */
	EReference getSimulationAgent_NextEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.DefaultSystemSimulationAgent <em>Default System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default System Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.DefaultSystemSimulationAgent
	 * @generated
	 */
	EClass getDefaultSystemSimulationAgent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent <em>Default Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Environment Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent
	 * @generated
	 */
	EClass getDefaultEnvironmentSimulationAgent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.UserInteractingSimulationAgent <em>User Interacting Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Interacting Simulation Agent</em>'.
	 * @see org.scenariotools.msd.simulation.UserInteractingSimulationAgent
	 * @generated
	 */
	EClass getUserInteractingSimulationAgent();

	/**
	 * Returns the meta object for the attribute list '{@link org.scenariotools.msd.simulation.UserInteractingSimulationAgent#getRegisteredMSDModalEventListChangeListener <em>Registered MSD Modal Event List Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Registered MSD Modal Event List Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.UserInteractingSimulationAgent#getRegisteredMSDModalEventListChangeListener()
	 * @see #getUserInteractingSimulationAgent()
	 * @generated
	 */
	EAttribute getUserInteractingSimulationAgent_RegisteredMSDModalEventListChangeListener();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.simulation.HistoryAgent <em>History Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Agent</em>'.
	 * @see org.scenariotools.msd.simulation.HistoryAgent
	 * @generated
	 */
	EClass getHistoryAgent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.HistoryAgent#getNextState <em>Next State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next State</em>'.
	 * @see org.scenariotools.msd.simulation.HistoryAgent#getNextState()
	 * @see #getHistoryAgent()
	 * @generated
	 */
	EReference getHistoryAgent_NextState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.simulation.HistoryAgent#getSimulationManager <em>Simulation Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Simulation Manager</em>'.
	 * @see org.scenariotools.msd.simulation.HistoryAgent#getSimulationManager()
	 * @see #getHistoryAgent()
	 * @generated
	 */
	EReference getHistoryAgent_SimulationManager();

	/**
	 * Returns the meta object for the attribute list '{@link org.scenariotools.msd.simulation.HistoryAgent#getRegisteredSimulationTraceChangeListener <em>Registered Simulation Trace Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Registered Simulation Trace Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.HistoryAgent#getRegisteredSimulationTraceChangeListener()
	 * @see #getHistoryAgent()
	 * @generated
	 */
	EAttribute getHistoryAgent_RegisteredSimulationTraceChangeListener();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener <em>IMSD Modal Message Event List Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IMSD Modal Message Event List Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener
	 * @model instanceClass="org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener" serializeable="false"
	 * @generated
	 */
	EDataType getIMSDModalMessageEventListChangeListener();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.simulation.listener.IStepPerformedListener <em>IStep Performed Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IStep Performed Listener</em>'.
	 * @see org.scenariotools.msd.simulation.listener.IStepPerformedListener
	 * @model instanceClass="org.scenariotools.msd.simulation.listener.IStepPerformedListener" serializeable="false"
	 * @generated
	 */
	EDataType getIStepPerformedListener();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener <em>IActive Simulation Agent Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IActive Simulation Agent Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener
	 * @model instanceClass="org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener" serializeable="false"
	 * @generated
	 */
	EDataType getIActiveSimulationAgentChangeListener();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener <em>ISimulation Trace Change Listener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ISimulation Trace Change Listener</em>'.
	 * @see org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener
	 * @model instanceClass="org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener" serializeable="false"
	 * @generated
	 */
	EDataType getISimulationTraceChangeListener();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SimulationFactory getSimulationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.SimulationManagerImpl <em>Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.SimulationManagerImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getSimulationManager()
		 * @generated
		 */
		EClass SIMULATION_MANAGER = eINSTANCE.getSimulationManager();

		/**
		 * The meta object literal for the '<em><b>Pause</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__PAUSE = eINSTANCE.getSimulationManager_Pause();

		/**
		 * The meta object literal for the '<em><b>Terminated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__TERMINATED = eINSTANCE.getSimulationManager_Terminated();

		/**
		 * The meta object literal for the '<em><b>Step Delay Milliseconds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS = eINSTANCE.getSimulationManager_StepDelayMilliseconds();

		/**
		 * The meta object literal for the '<em><b>Current System Simulation Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT = eINSTANCE.getSimulationManager_CurrentSystemSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Current History Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__CURRENT_HISTORY_AGENT = eINSTANCE.getSimulationManager_CurrentHistoryAgent();

		/**
		 * The meta object literal for the '<em><b>Registered System Simulation Agent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT = eINSTANCE.getSimulationManager_RegisteredSystemSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Registered History Agent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT = eINSTANCE.getSimulationManager_RegisteredHistoryAgent();

		/**
		 * The meta object literal for the '<em><b>Current Environment Simulation Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT = eINSTANCE.getSimulationManager_CurrentEnvironmentSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Registered Environment Simulation Agent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT = eINSTANCE.getSimulationManager_RegisteredEnvironmentSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Registered Active Simulation Agent Change Listener</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER = eINSTANCE.getSimulationManager_RegisteredActiveSimulationAgentChangeListener();

		/**
		 * The meta object literal for the '<em><b>Active Simulation Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT = eINSTANCE.getSimulationManager_ActiveSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Registered Step Performed Listener</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER = eINSTANCE.getSimulationManager_RegisteredStepPerformedListener();

		/**
		 * The meta object literal for the '<em><b>Msd Runtime State Graph</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__MSD_RUNTIME_STATE_GRAPH = eINSTANCE.getSimulationManager_MsdRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Current MSD Runtime State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__CURRENT_MSD_RUNTIME_STATE = eINSTANCE.getSimulationManager_CurrentMSDRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Scenario Run Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION = eINSTANCE.getSimulationManager_ScenarioRunConfiguration();

		/**
		 * The meta object literal for the '<em><b>Current Execution Semantics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_MANAGER__CURRENT_EXECUTION_SEMANTICS = eINSTANCE.getSimulationManager_CurrentExecutionSemantics();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.SimulationAgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.SimulationAgentImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getSimulationAgent()
		 * @generated
		 */
		EClass SIMULATION_AGENT = eINSTANCE.getSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Simulation Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_AGENT__SIMULATION_MANAGER = eINSTANCE.getSimulationAgent_SimulationManager();

		/**
		 * The meta object literal for the '<em><b>Next Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_AGENT__NEXT_EVENT = eINSTANCE.getSimulationAgent_NextEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.DefaultSystemSimulationAgentImpl <em>Default System Simulation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.DefaultSystemSimulationAgentImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getDefaultSystemSimulationAgent()
		 * @generated
		 */
		EClass DEFAULT_SYSTEM_SIMULATION_AGENT = eINSTANCE.getDefaultSystemSimulationAgent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.DefaultEnvironmentSimulationAgentImpl <em>Default Environment Simulation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.DefaultEnvironmentSimulationAgentImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getDefaultEnvironmentSimulationAgent()
		 * @generated
		 */
		EClass DEFAULT_ENVIRONMENT_SIMULATION_AGENT = eINSTANCE.getDefaultEnvironmentSimulationAgent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.UserInteractingSimulationAgentImpl <em>User Interacting Simulation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.UserInteractingSimulationAgentImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getUserInteractingSimulationAgent()
		 * @generated
		 */
		EClass USER_INTERACTING_SIMULATION_AGENT = eINSTANCE.getUserInteractingSimulationAgent();

		/**
		 * The meta object literal for the '<em><b>Registered MSD Modal Event List Change Listener</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER = eINSTANCE.getUserInteractingSimulationAgent_RegisteredMSDModalEventListChangeListener();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.simulation.impl.HistoryAgentImpl <em>History Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.impl.HistoryAgentImpl
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getHistoryAgent()
		 * @generated
		 */
		EClass HISTORY_AGENT = eINSTANCE.getHistoryAgent();

		/**
		 * The meta object literal for the '<em><b>Next State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_AGENT__NEXT_STATE = eINSTANCE.getHistoryAgent_NextState();

		/**
		 * The meta object literal for the '<em><b>Simulation Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_AGENT__SIMULATION_MANAGER = eINSTANCE.getHistoryAgent_SimulationManager();

		/**
		 * The meta object literal for the '<em><b>Registered Simulation Trace Change Listener</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER = eINSTANCE.getHistoryAgent_RegisteredSimulationTraceChangeListener();

		/**
		 * The meta object literal for the '<em>IMSD Modal Message Event List Change Listener</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIMSDModalMessageEventListChangeListener()
		 * @generated
		 */
		EDataType IMSD_MODAL_MESSAGE_EVENT_LIST_CHANGE_LISTENER = eINSTANCE.getIMSDModalMessageEventListChangeListener();

		/**
		 * The meta object literal for the '<em>IStep Performed Listener</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.listener.IStepPerformedListener
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIStepPerformedListener()
		 * @generated
		 */
		EDataType ISTEP_PERFORMED_LISTENER = eINSTANCE.getIStepPerformedListener();

		/**
		 * The meta object literal for the '<em>IActive Simulation Agent Change Listener</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getIActiveSimulationAgentChangeListener()
		 * @generated
		 */
		EDataType IACTIVE_SIMULATION_AGENT_CHANGE_LISTENER = eINSTANCE.getIActiveSimulationAgentChangeListener();

		/**
		 * The meta object literal for the '<em>ISimulation Trace Change Listener</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener
		 * @see org.scenariotools.msd.simulation.impl.SimulationPackageImpl#getISimulationTraceChangeListener()
		 * @generated
		 */
		EDataType ISIMULATION_TRACE_CHANGE_LISTENER = eINSTANCE.getISimulationTraceChangeListener();

	}

} //SimulationPackage
