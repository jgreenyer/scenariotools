/**
 */
package org.scenariotools.msd.simulation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.msd.simulation.listener.IStepPerformedListener;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#isPause <em>Pause</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#isTerminated <em>Terminated</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentHistoryAgent <em>Current History Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredSystemSimulationAgent <em>Registered System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredHistoryAgent <em>Registered History Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredEnvironmentSimulationAgent <em>Registered Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredActiveSimulationAgentChangeListener <em>Registered Active Simulation Agent Change Listener</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getActiveSimulationAgent <em>Active Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getRegisteredStepPerformedListener <em>Registered Step Performed Listener</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentMSDRuntimeState <em>Current MSD Runtime State</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentExecutionSemantics <em>Current Execution Semantics</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager()
 * @model
 * @generated
 */
public interface SimulationManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Pause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pause</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pause</em>' attribute.
	 * @see #setPause(boolean)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_Pause()
	 * @model
	 * @generated
	 */
	boolean isPause();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#isPause <em>Pause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pause</em>' attribute.
	 * @see #isPause()
	 * @generated
	 */
	void setPause(boolean value);

	/**
	 * Returns the value of the '<em><b>Terminated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated</em>' attribute.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_Terminated()
	 * @model changeable="false"
	 * @generated
	 */
	boolean isTerminated();

	/**
	 * Returns the value of the '<em><b>Step Delay Milliseconds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Delay Milliseconds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Delay Milliseconds</em>' attribute.
	 * @see #setStepDelayMilliseconds(int)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_StepDelayMilliseconds()
	 * @model
	 * @generated
	 */
	int getStepDelayMilliseconds();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Delay Milliseconds</em>' attribute.
	 * @see #getStepDelayMilliseconds()
	 * @generated
	 */
	void setStepDelayMilliseconds(int value);

	/**
	 * Returns the value of the '<em><b>Current System Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current System Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current System Simulation Agent</em>' reference.
	 * @see #setCurrentSystemSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_CurrentSystemSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getCurrentSystemSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current System Simulation Agent</em>' reference.
	 * @see #getCurrentSystemSimulationAgent()
	 * @generated
	 */
	void setCurrentSystemSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Current History Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current History Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current History Agent</em>' reference.
	 * @see #setCurrentHistoryAgent(HistoryAgent)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_CurrentHistoryAgent()
	 * @model
	 * @generated
	 */
	HistoryAgent getCurrentHistoryAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentHistoryAgent <em>Current History Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current History Agent</em>' reference.
	 * @see #getCurrentHistoryAgent()
	 * @generated
	 */
	void setCurrentHistoryAgent(HistoryAgent value);

	/**
	 * Returns the value of the '<em><b>Registered System Simulation Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.SimulationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered System Simulation Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered System Simulation Agent</em>' reference list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_RegisteredSystemSimulationAgent()
	 * @model
	 * @generated
	 */
	EList<SimulationAgent> getRegisteredSystemSimulationAgent();

	/**
	 * Returns the value of the '<em><b>Registered History Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.HistoryAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered History Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered History Agent</em>' reference list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_RegisteredHistoryAgent()
	 * @model
	 * @generated
	 */
	EList<HistoryAgent> getRegisteredHistoryAgent();

	/**
	 * Returns the value of the '<em><b>Current Environment Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Environment Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Environment Simulation Agent</em>' reference.
	 * @see #setCurrentEnvironmentSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_CurrentEnvironmentSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getCurrentEnvironmentSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Environment Simulation Agent</em>' reference.
	 * @see #getCurrentEnvironmentSimulationAgent()
	 * @generated
	 */
	void setCurrentEnvironmentSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Registered Environment Simulation Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.SimulationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Environment Simulation Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Environment Simulation Agent</em>' reference list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_RegisteredEnvironmentSimulationAgent()
	 * @model
	 * @generated
	 */
	EList<SimulationAgent> getRegisteredEnvironmentSimulationAgent();

	/**
	 * Returns the value of the '<em><b>Registered Active Simulation Agent Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Active Simulation Agent Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Active Simulation Agent Change Listener</em>' attribute list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_RegisteredActiveSimulationAgentChangeListener()
	 * @model dataType="org.scenariotools.msd.simulation.IActiveSimulationAgentChangeListener" transient="true"
	 * @generated
	 */
	EList<IActiveSimulationAgentChangeListener> getRegisteredActiveSimulationAgentChangeListener();

	/**
	 * Returns the value of the '<em><b>Active Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Simulation Agent</em>' reference.
	 * @see #setActiveSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_ActiveSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getActiveSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getActiveSimulationAgent <em>Active Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Simulation Agent</em>' reference.
	 * @see #getActiveSimulationAgent()
	 * @generated
	 */
	void setActiveSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Registered Step Performed Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.listener.IStepPerformedListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Step Performed Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Step Performed Listener</em>' attribute list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_RegisteredStepPerformedListener()
	 * @model dataType="org.scenariotools.msd.simulation.IStepPerformedListener" transient="true"
	 * @generated
	 */
	EList<IStepPerformedListener> getRegisteredStepPerformedListener();

	/**
	 * Returns the value of the '<em><b>Msd Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msd Runtime State Graph</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #setMsdRuntimeStateGraph(MSDRuntimeStateGraph)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_MsdRuntimeStateGraph()
	 * @model
	 * @generated
	 */
	MSDRuntimeStateGraph getMsdRuntimeStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getMsdRuntimeStateGraph <em>Msd Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msd Runtime State Graph</em>' reference.
	 * @see #getMsdRuntimeStateGraph()
	 * @generated
	 */
	void setMsdRuntimeStateGraph(MSDRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Current MSD Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current MSD Runtime State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current MSD Runtime State</em>' reference.
	 * @see #setCurrentMSDRuntimeState(MSDRuntimeState)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_CurrentMSDRuntimeState()
	 * @model
	 * @generated
	 */
	MSDRuntimeState getCurrentMSDRuntimeState();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentMSDRuntimeState <em>Current MSD Runtime State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current MSD Runtime State</em>' reference.
	 * @see #getCurrentMSDRuntimeState()
	 * @generated
	 */
	void setCurrentMSDRuntimeState(MSDRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario Run Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #setScenarioRunConfiguration(ScenarioRunConfiguration)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_ScenarioRunConfiguration()
	 * @model
	 * @generated
	 */
	ScenarioRunConfiguration getScenarioRunConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	void setScenarioRunConfiguration(ScenarioRunConfiguration value);

	/**
	 * Returns the value of the '<em><b>Current Execution Semantics</b></em>' attribute.
	 * The literals are from the enumeration {@link org.scenariotools.msd.runtime.ExecutionSemantics}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Execution Semantics</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Execution Semantics</em>' attribute.
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @see #setCurrentExecutionSemantics(ExecutionSemantics)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getSimulationManager_CurrentExecutionSemantics()
	 * @model
	 * @generated
	 */
	ExecutionSemantics getCurrentExecutionSemantics();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.SimulationManager#getCurrentExecutionSemantics <em>Current Execution Semantics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Execution Semantics</em>' attribute.
	 * @see org.scenariotools.msd.runtime.ExecutionSemantics
	 * @see #getCurrentExecutionSemantics()
	 * @generated
	 */
	void setCurrentExecutionSemantics(ExecutionSemantics value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * shall be called by the (current) simulation runtime agent while not terminated
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void performNextStepFromSimulationAgent(SimulationAgent simulationAgent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * shall be called by the (current) simulation runtime agent to indicate the end of its superstep
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void wait(SimulationAgent simulationAgent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void terminate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void loadStateFromHistoryAgent(HistoryAgent historyAgent);

} // SimulationManager
