package org.scenariotools.msd.simulation.debug;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.simulation.HistoryAgent;
import org.scenariotools.msd.simulation.SimulationAgent;
import org.scenariotools.msd.simulation.SimulationFactory;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.listener.IStepPerformedListener;
import org.scenariotools.msd.simulation.plugin.Activator;
import org.scenariotools.msd.util.RuntimeUtil;

public class ScenarioDebugTarget extends ScenarioDebugElement implements
		IDebugTarget {

	private static Logger logger = Activator.getLogManager().getLogger(
			ScenarioDebugTarget.class.getName());
	
	protected ILaunch launch;
	
	protected SimulationManager simulationManager;
	
	protected List<AbstractScenarioThread> activeMSDThreads;
	protected List<ObjectThread> systemObjectThreads;
	protected List<ObjectThread> environmentObjectThreads;
	
	protected IStepPerformedListener stepPerformedListener = new IStepPerformedListener() {
		@Override
		public void stepPerformed() {
			fireChangeEvent(DebugEvent.CONTENT);
		}
	};

	
	protected SimulationManager createSimulationManager(){
		return SimulationFactory.eINSTANCE.createSimulationManager();
	}

	protected MSDRuntimeStateGraph createMSDRuntimeStateGraph(){
		return RuntimeFactory.eINSTANCE.createMSDRuntimeStateGraph();
	}

	public ScenarioDebugTarget(ILaunch launch, ScenarioRunConfiguration scenarioRunConfiguration,
			String configuredEnvironmentSimulationAgentNsURI,
			String configuredEnvironmentSimulationAgentEClassName,
			String configuredSystemSimulationAgentNsURI,
			String configuredSystemSimulationAgentEClassName,
			String delayMilliseconds, 
			boolean startInPauseMode) {
		super(null);
		this.launch = launch;
		
		logger.debug("Creating runtime manager");
		
		setSimulationManager(createSimulationManager());
		
		getSimulationManager().setScenarioRunConfiguration(scenarioRunConfiguration);
		getSimulationManager().setCurrentExecutionSemantics(scenarioRunConfiguration.getExecutionSemantics());
		
		getSimulationManager().setPause(startInPauseMode);
		
		getSimulationManager().setStepDelayMilliseconds(Integer.parseInt(delayMilliseconds));

		SimulationAgent environmentSimulationAgent = loadSimulationAgent(configuredEnvironmentSimulationAgentNsURI, configuredEnvironmentSimulationAgentEClassName);
		environmentSimulationAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredEnvironmentSimulationAgent().add(environmentSimulationAgent);
		getSimulationManager().setCurrentEnvironmentSimulationAgent(environmentSimulationAgent);

		SimulationAgent systemSimulationAgent = loadSimulationAgent(configuredSystemSimulationAgentNsURI, configuredSystemSimulationAgentEClassName);
		systemSimulationAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredSystemSimulationAgent().add(systemSimulationAgent);
		getSimulationManager().setCurrentSystemSimulationAgent(systemSimulationAgent);
		
		MSDRuntimeStateGraph msdRuntimeStateGraph = createMSDRuntimeStateGraph();
		MSDRuntimeState msdRuntimeState = msdRuntimeStateGraph.init(scenarioRunConfiguration);
		
		getSimulationManager().setMsdRuntimeStateGraph(msdRuntimeStateGraph);
		getSimulationManager().setCurrentMSDRuntimeState(msdRuntimeState);
		
		HistoryAgent historyAgent = SimulationFactory.eINSTANCE.createHistoryAgent();
		historyAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredHistoryAgent().add(historyAgent);
		getSimulationManager().setCurrentHistoryAgent(historyAgent);
		
		logger.debug("init current EnvironmentSimulationAgent");
		getSimulationManager().getCurrentEnvironmentSimulationAgent().init();
		
		logger.debug("init current SystemSimulationAgent");
		getSimulationManager().getCurrentSystemSimulationAgent().init();
		
		getSimulationManager().getCurrentHistoryAgent().init();
		
		getSimulationManager().getRegisteredStepPerformedListener().add(stepPerformedListener);
		
		getSimulationManager().setActiveSimulationAgent(environmentSimulationAgent);
		
		logger.debug("Runtime manager is initialized");

	}
	
	
	protected SimulationAgent loadSimulationAgent(String SimulationAgentNsURI, String SimulationAgentEClassName){
		EPackage environmentSimulationAgentPackage = EPackage.Registry.INSTANCE.getEPackage(SimulationAgentNsURI);
		if (environmentSimulationAgentPackage != null) {
			EClass environmentSimulationAgentClass = (EClass) environmentSimulationAgentPackage.getEClassifier(SimulationAgentEClassName);
			if (environmentSimulationAgentClass == null)
				logger.error("The specified class \"" + SimulationAgentEClassName + "\" cannot be found");
			EFactory eFactory = EPackage.Registry.INSTANCE.getEFactory(SimulationAgentNsURI); 
			EObject eObject = eFactory.create(environmentSimulationAgentClass);
			if (eObject != null && eObject instanceof SimulationAgent)
				return (SimulationAgent) eObject;
			else{
				logger.error("Cannot create a runtime agent object from eClass \"" + environmentSimulationAgentClass + "\" from EFactory " + eFactory);
				return null;
			}
		}else{
			logger.error("The specified package with nsURI \"" + SimulationAgentNsURI
					+ "\" cannot be found");
			return null;
		}

	}
	
	
	
	public SimulationManager getSimulationManager() {
		return simulationManager;
	}


	public void setSimulationManager(SimulationManager SimulationManager) {
		this.simulationManager = SimulationManager;
	}

	
	@Override
	public ILaunch getLaunch() {
		return launch;
	}
	
	@Override
	public boolean canTerminate() {
		return !getSimulationManager().isTerminated();
	}

	@Override
	public boolean isTerminated() {
		return getSimulationManager().isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
		getSimulationManager().terminate();
		fireEvent(new DebugEvent(getDebugTarget(), DebugEvent.TERMINATE));
	}

	@Override
	public boolean canResume() {
		return getSimulationManager().isPause() && !getSimulationManager().isTerminated();
	}

	@Override
	public boolean canSuspend() {
		return !getSimulationManager().isPause() && !getSimulationManager().isTerminated();
	}

	@Override
	public boolean isSuspended() {
		return getSimulationManager().isPause();
	}

	@Override
	public void resume() throws DebugException {
		getSimulationManager().setPause(false);
		fireEvent(new DebugEvent(this, DebugEvent.RESUME));
		fireChangeEvent(DebugEvent.CONTENT);
	}

	@Override
	public void suspend() throws DebugException {
		getSimulationManager().setPause(true);
		fireEvent(new DebugEvent(this, DebugEvent.SUSPEND));
		fireChangeEvent(DebugEvent.CONTENT);
	}
	
	@Override
	public void breakpointAdded(IBreakpoint breakpoint) {
		// TODO Auto-generated method stub

	}

	@Override
	public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canDisconnect() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDisconnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean supportsStorageRetrieval() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IMemoryBlock getMemoryBlock(long startAddress, long length)
			throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess getProcess() {
		// TODO Auto-generated method stub
		return null;
	}

	protected List<AbstractScenarioThread> getActiveMSDThreads(){
		if (activeMSDThreads == null)
			activeMSDThreads = new ArrayList<AbstractScenarioThread>();
		return activeMSDThreads;
	}
	
	protected List<ObjectThread> getSystemObjectThreads(){
		if (systemObjectThreads == null)
			systemObjectThreads = new ArrayList<ObjectThread>();
		return systemObjectThreads;
	}

	protected List<ObjectThread> getEnvironmentObjectThreads(){
		if (environmentObjectThreads == null)
			environmentObjectThreads = new ArrayList<ObjectThread>();
		return environmentObjectThreads;
	}

	@Override
	public IThread[] getThreads() throws DebugException {
		refreshActiveMSDThreads();
		refreshEnvironmentObectThreads();
		refreshSystemObectThreads();
		
		List<IThread> threadList = new ArrayList<IThread>();
		threadList.addAll(getActiveMSDThreads());
		threadList.addAll(getEnvironmentObjectThreads());
		threadList.addAll(getSystemObjectThreads());
		
		return threadList.toArray(new IThread[] {});
	}
	
	protected void refreshSystemObectThreads(){
		// 1. remove orphaned ObectThreads
		EList<EObject> eObjectList = getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getControllableObjects();
		EList<EObject> eObjectsWithoutObjectThreads = new BasicEList<EObject>(eObjectList);
		Iterator<ObjectThread> objectThreadIterator =  getSystemObjectThreads().iterator();
		while (objectThreadIterator.hasNext()) {
			ObjectThread objectThread = (ObjectThread) objectThreadIterator
					.next();
			if(!eObjectList.contains(objectThread.getSimulationObject())){
				objectThreadIterator.remove();
			}else{
				eObjectsWithoutObjectThreads.remove(objectThread.getSimulationObject());
			}
			
		}
		
		// 2. create new Objecthreads
		for (EObject eObject : eObjectsWithoutObjectThreads) {
			getSystemObjectThreads().add(new ObjectThread(this, eObject, true));
		}
	}
	
	protected void refreshEnvironmentObectThreads(){
		// 1. remove orphaned ObectThreads
		EList<EObject> eObjectList = getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getUncontrollableObjects();
		EList<EObject> eObjectsWithoutObjectThreads = new BasicEList<EObject>(eObjectList);
		Iterator<ObjectThread> objectThreadIterator =  getEnvironmentObjectThreads().iterator();
		while (objectThreadIterator.hasNext()) {
			ObjectThread objectThread = (ObjectThread) objectThreadIterator
					.next();
			if(!eObjectList.contains(objectThread.getSimulationObject())){
				objectThreadIterator.remove();
			}else{
				eObjectsWithoutObjectThreads.remove(objectThread.getSimulationObject());
			}
			
		}
		
		// 2. create new Objecthreads
		for (EObject eObject : eObjectsWithoutObjectThreads) {
			getEnvironmentObjectThreads().add(new ObjectThread(this, eObject, false));
		}
	}

	
	protected void refreshActiveMSDThreads(){
		// 1. remove orphaned ActiveMSDThreads
		EList<ActiveProcess> currentActiveProcessList = getSimulationManager().getCurrentMSDRuntimeState().getActiveProcesses();
		EList<ActiveProcess> cutsWithoutActiveMSDThreadList = new BasicEList<ActiveProcess>(currentActiveProcessList);
		Iterator<AbstractScenarioThread> activeMSDThreadIterator =  getActiveMSDThreads().iterator();
		while (activeMSDThreadIterator.hasNext()) {
			AbstractScenarioThread activeMSDThread = activeMSDThreadIterator
					.next();
			if(activeMSDThread instanceof ActiveMSDThread){
				if(!currentActiveProcessList.contains(((ActiveMSDThread)activeMSDThread).getActiveProcess())){
					activeMSDThreadIterator.remove();
				}else{
					cutsWithoutActiveMSDThreadList.remove(((ActiveMSDThread)activeMSDThread).getActiveProcess());
				}
			}
			else{
				if(!currentActiveProcessList.contains(((ActiveMSSThread)activeMSDThread).getActiveProcess())){
					activeMSDThreadIterator.remove();
				}else{
					cutsWithoutActiveMSDThreadList.remove(((ActiveMSSThread)activeMSDThread).getActiveProcess());
				}
			}
		}
		
		// 2. create new ActiveMSDThreads
		for (ActiveProcess activeProcess : cutsWithoutActiveMSDThreadList) {
			if(activeProcess instanceof ActiveMSD)
				getActiveMSDThreads().add(new ActiveMSDThread(this, (ActiveMSD)activeProcess));
			else if(activeProcess instanceof ActiveMSS)
				getActiveMSDThreads().add(new ActiveMSSThread(this, (ActiveMSS)activeProcess));
		}
	}


	@Override
	public boolean hasThreads() throws DebugException {
		return true;
	}

	@Override
	public String getName() throws DebugException {
		MSDRuntimeState state=getSimulationManager().getCurrentMSDRuntimeState();
		String violationsString=(state.isSafetyViolationOccurredInRequirements()?"[requirements violated] ":"")+(state.isSafetyViolationOccurredInAssumptions()?"[assumptions violated] ":"");

		return violationsString+"Object System: " + getSimulationRootObjectsName() + " executed with MSD specification (UML Package): " + getSpecificationName();
	}

	protected String getSpecificationName(){
		return getSimulationManager().getMsdRuntimeStateGraph().getScenarioRunConfiguration().getUml2EcoreMapping().getUmlPackage().getName();
	}
	
	private String getSimulationRootObjectsName(){
		try {
			EList<EObject> simulationObjects = new BasicEList<EObject>();
			simulationObjects.addAll(getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getUncontrollableObjects());
			simulationObjects.addAll(getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getControllableObjects());
			String numberOfObjectsString = "(" + (getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getUncontrollableObjects().size() + getSimulationManager().getCurrentMSDRuntimeState().getObjectSystem().getControllableObjects().size()) + " objects)";
			return numberOfObjectsString + " " + RuntimeUtil.Helper.getEObjectsName(simulationObjects, 5);
		} catch (NullPointerException e) {
			return "<no simulation objects>";
		}
	}
	
	@Override
	public IDebugTarget getDebugTarget() {
		return this;
	}
	
	@Override
	public boolean supportsBreakpoint(IBreakpoint breakpoint) {
		// TODO Auto-generated method stub
		return false;
	}
}
