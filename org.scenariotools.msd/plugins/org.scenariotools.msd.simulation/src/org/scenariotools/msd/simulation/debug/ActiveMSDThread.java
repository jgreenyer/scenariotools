package org.scenariotools.msd.simulation.debug;


import org.apache.log4j.Logger;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.simulation.plugin.Activator;
import org.scenariotools.msd.util.RuntimeUtil;

public class ActiveMSDThread extends AbstractScenarioThread {

	private static Logger logger = Activator.getLogManager().getLogger(
			ActiveMSDThread.class.getName());

	protected ActiveMSD activeMSD;
	protected ActiveMSDLifelineBindingStackFrame activeMSDLifelineBindingStackFrame;
	protected ActiveMSDVariableStackFrame activeMSDVariableStackFrame;

	
	public ActiveMSDThread(IDebugTarget target, ActiveMSD activeMSD) {
		super(target);
		if (activeMSD != null)
			this.activeMSD = activeMSD;
	}
	
	public ActiveMSD getActiveProcess(){
		return activeMSD;
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}
	
	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		if (activeMSDLifelineBindingStackFrame == null)
			activeMSDLifelineBindingStackFrame = new ActiveMSDLifelineBindingStackFrame(getDebugTarget(), activeMSD, this);
		if (activeMSDVariableStackFrame == null)
			activeMSDVariableStackFrame = new ActiveMSDVariableStackFrame(getDebugTarget(), activeMSD, this);
		return new IStackFrame[]{activeMSDLifelineBindingStackFrame,activeMSDVariableStackFrame};
	}

	@Override
	public String getName() throws DebugException {
		String bindingString = "[";
		
		EList<Lifeline> lifelines = getActiveProcess().getInteraction().getLifelines();
		
		for (Lifeline lifeline : lifelines) {
			bindingString += lifeline.getRepresents().getName() + "->";
			EObject eObjectBoundByLifeline = getActiveProcess().getLifelineBindings().getLifelineToEObjectMap().get(lifeline);
			if(eObjectBoundByLifeline != null){
				bindingString += RuntimeUtil.Helper.getEObjectName(eObjectBoundByLifeline);
			} else {
				bindingString += "<unbound>";
			}
			bindingString += ", ";
		}
		bindingString = bindingString.substring(0, bindingString.length()-2);
		bindingString += "]";
		
		String returnString = "active ";
		if (RuntimeUtil.Helper.isEnvironmentAssumption(activeMSD.getInteraction())){
			returnString += "assumption ";
		}else{
			returnString += "requirement ";
		}
		
		String cutString = "";
		cutString += " (";

		for (Lifeline lifeline : lifelines) {
			cutString += RuntimeUtil.Helper.getLifelineName(lifeline);
			cutString += ":";
			
			cutString += getLifelinePositionString(lifeline);
			
			if (lifelines.get(lifelines.size() - 1) != lifeline) {// not last one
				cutString += ", ";
			} 
		}
		cutString += ")";
		
		return returnString + "MSD: " + getActiveProcess().getInteraction().getName() + " " + bindingString + cutString;
	}
	
	private String getLifelinePositionString(Lifeline lifeline){
		StringBuilder resultSB = new StringBuilder();
		RuntimeUtil runtimeUtil = ((ScenarioDebugTarget) getDebugTarget())
				.getSimulationManager().getMsdRuntimeStateGraph().getRuntimeUtil();
		
		//assemble string from right to left -> start at innermost fragment
		InteractionFragment currentFragment=activeMSD.getCurrentState().getLifelineToInteractionFragmentMap().get(lifeline);
		
		if (currentFragment == null) return "<unbound>";
		
		InteractionOperand enclosingOperand=currentFragment.getEnclosingOperand();
		EList<InteractionFragment> sortedInteractionFragmentsOnLifeline = null;
		while(enclosingOperand != null){
			//not at top level yet
			CombinedFragment parentFragment = ((CombinedFragment)enclosingOperand.getOwner());

			//determine index of operand and index of current fragment in operand
			int operandIndex=parentFragment.getOperands().indexOf(enclosingOperand);
			sortedInteractionFragmentsOnLifeline = runtimeUtil.getInteractionOperandToLifelineToInteractionFragmentsMapMap().get(enclosingOperand).get(lifeline);
			int fragmentIndex=sortedInteractionFragmentsOnLifeline.indexOf(currentFragment);
			
			resultSB.insert(0, "["+operandIndex+"]."+fragmentIndex);
			
			//next iteration will run on parent level, i.e. on the complex fragment including the operand with the current fragment
			currentFragment=parentFragment;
			enclosingOperand=parentFragment.getEnclosingOperand();
		}
		//now at top level
		sortedInteractionFragmentsOnLifeline = runtimeUtil.getLifelineToInteractionFragmentsMap().get(lifeline);
		int fragmentIndex=sortedInteractionFragmentsOnLifeline.indexOf(currentFragment);
		resultSB.insert(0, fragmentIndex);
		
		return resultSB.toString();
	}
	
}
