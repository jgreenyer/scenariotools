/**
 */
package org.scenariotools.msd.simulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent;
import org.scenariotools.msd.simulation.SimulationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultEnvironmentSimulationAgentImpl extends UserInteractingSimulationAgentImpl implements DefaultEnvironmentSimulationAgent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultEnvironmentSimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimulationPackage.Literals.DEFAULT_ENVIRONMENT_SIMULATION_AGENT;
	}

} //DefaultEnvironmentSimulationAgentImpl
