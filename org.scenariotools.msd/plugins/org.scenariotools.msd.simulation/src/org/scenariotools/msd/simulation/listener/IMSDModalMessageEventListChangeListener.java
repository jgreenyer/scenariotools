package org.scenariotools.msd.simulation.listener;

import java.util.Collection;
import java.util.EventListener;

import org.scenariotools.msd.runtime.MSDModalMessageEvent;

public interface IMSDModalMessageEventListChangeListener extends EventListener {

	public void msdModalMessageEventsChanged(Collection<MSDModalMessageEvent> msdModalMessageEvents);
	
}
