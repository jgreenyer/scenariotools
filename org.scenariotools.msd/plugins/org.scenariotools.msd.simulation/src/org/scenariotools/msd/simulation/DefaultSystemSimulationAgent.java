/**
 */
package org.scenariotools.msd.simulation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.simulation.SimulationPackage#getDefaultSystemSimulationAgent()
 * @model
 * @generated
 */
public interface DefaultSystemSimulationAgent extends UserInteractingSimulationAgent {
} // DefaultSystemSimulationAgent
