package org.scenariotools.msd.simulation.listener;

import java.util.EventListener;

public interface IStepPerformedListener extends EventListener {

	public void stepPerformed();
	
}
