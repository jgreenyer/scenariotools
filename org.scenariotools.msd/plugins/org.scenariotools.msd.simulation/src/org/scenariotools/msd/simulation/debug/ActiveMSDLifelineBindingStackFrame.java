package org.scenariotools.msd.simulation.debug;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Lifeline;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;

public class ActiveMSDLifelineBindingStackFrame extends AbstractActiveMSDStackFrame{

	private List<LifelineBindingVariable> variables; 

	public ActiveMSDLifelineBindingStackFrame(IDebugTarget target, ActiveMSD activeMSD,
			ActiveMSDThread activeMSDThread) {
		super(target, activeMSD, activeMSDThread);
		// TODO Auto-generated constructor stub
	}

	@Override
	public IVariable[] getVariables() throws DebugException {

		refreshVariables();
		
		// TODO Auto-generated method stub
		return variables.toArray(new LifelineBindingVariable[]{});
	}
	
	@Override
	public ActiveMSD getActiveMSD() {
		return (ActiveMSD)super.getActiveMSD();
	}

	protected void refreshVariables() throws DebugException{
		if (variables == null){
			variables = new ArrayList<LifelineBindingVariable>();
		}

		// 1. remove orphaned Variables
		
		Set<Lifeline> currentlyBoundLifelines = getActiveMSD().getLifelineBindings().getLifelineToEObjectMap().keySet(); 
		EList<Lifeline> currentlyBoundLifelinesWithoutRepresentation = new BasicEList<Lifeline>(currentlyBoundLifelines);
		Iterator<LifelineBindingVariable> lifelineVariableIterator = variables.iterator();
		
		while (lifelineVariableIterator.hasNext()) {
			LifelineBindingVariable lifelineBindingVariable = (LifelineBindingVariable) lifelineVariableIterator.next();
			boolean lifelineBindingForVariableExists = false;
			for (Lifeline lifeline : currentlyBoundLifelines) {
				if (getActiveMSD().getLifelineBindings().getLifelineToEObjectMap().get(lifeline) == null) continue;
				if (lifelineBindingVariable.getName() == lifeline.getRepresents().getName()){
					lifelineBindingForVariableExists = true;
					break;
				}
			}
			if (!lifelineBindingForVariableExists){
				lifelineVariableIterator.remove();
			}else{
				currentlyBoundLifelinesWithoutRepresentation.remove(lifelineBindingVariable.getLifeline());
			}
		}

		// 2. create new Variables
		
		for (Lifeline lifeline : currentlyBoundLifelinesWithoutRepresentation) {
			if (getActiveMSD().getLifelineBindings().getLifelineToEObjectMap().get(lifeline) == null) continue;
			variables.add(new LifelineBindingVariable(getDebugTarget(), lifeline, getActiveMSD().getLifelineBindings().getLifelineToEObjectMap().get(lifeline)));
		}
	}

	@Override
	public boolean hasVariables() throws DebugException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getName() throws DebugException {
		return "Lifeline bindings";
	}



	

}
