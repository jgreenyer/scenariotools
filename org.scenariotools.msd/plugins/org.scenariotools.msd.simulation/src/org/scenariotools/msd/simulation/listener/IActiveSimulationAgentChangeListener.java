package org.scenariotools.msd.simulation.listener;

import java.util.EventListener;

import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;

public interface IActiveSimulationAgentChangeListener extends EventListener {

	public void activeSimulationAgentChanged(UserInteractingSimulationAgent userInteractingSimulationAgent);
	
}
