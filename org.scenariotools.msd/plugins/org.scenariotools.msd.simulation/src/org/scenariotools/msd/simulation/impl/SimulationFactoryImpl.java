/**
 */
package org.scenariotools.msd.simulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.msd.simulation.*;
import org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener;
import org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent;
import org.scenariotools.msd.simulation.DefaultSystemSimulationAgent;
import org.scenariotools.msd.simulation.SimulationFactory;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.SimulationPackage;
import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimulationFactoryImpl extends EFactoryImpl implements SimulationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SimulationFactory init() {
		try {
			SimulationFactory theSimulationFactory = (SimulationFactory)EPackage.Registry.INSTANCE.getEFactory(SimulationPackage.eNS_URI);
			if (theSimulationFactory != null) {
				return theSimulationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SimulationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SimulationPackage.SIMULATION_MANAGER: return createSimulationManager();
			case SimulationPackage.DEFAULT_SYSTEM_SIMULATION_AGENT: return createDefaultSystemSimulationAgent();
			case SimulationPackage.DEFAULT_ENVIRONMENT_SIMULATION_AGENT: return createDefaultEnvironmentSimulationAgent();
			case SimulationPackage.USER_INTERACTING_SIMULATION_AGENT: return createUserInteractingSimulationAgent();
			case SimulationPackage.HISTORY_AGENT: return createHistoryAgent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager createSimulationManager() {
		SimulationManagerImpl simulationManager = new SimulationManagerImpl();
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultSystemSimulationAgent createDefaultSystemSimulationAgent() {
		DefaultSystemSimulationAgentImpl defaultSystemSimulationAgent = new DefaultSystemSimulationAgentImpl();
		return defaultSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultEnvironmentSimulationAgent createDefaultEnvironmentSimulationAgent() {
		DefaultEnvironmentSimulationAgentImpl defaultEnvironmentSimulationAgent = new DefaultEnvironmentSimulationAgentImpl();
		return defaultEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserInteractingSimulationAgent createUserInteractingSimulationAgent() {
		UserInteractingSimulationAgentImpl userInteractingSimulationAgent = new UserInteractingSimulationAgentImpl();
		return userInteractingSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryAgent createHistoryAgent() {
		HistoryAgentImpl historyAgent = new HistoryAgentImpl();
		return historyAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationPackage getSimulationPackage() {
		return (SimulationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SimulationPackage getPackage() {
		return SimulationPackage.eINSTANCE;
	}

} //SimulationFactoryImpl
