/**
 */
package org.scenariotools.msd.simulation.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.simulation.util.SimulationResourceFactoryImpl
 * @generated
 */
public class SimulationResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public SimulationResourceImpl(URI uri) {
		super(uri);
	}

} //SimulationResourceImpl
