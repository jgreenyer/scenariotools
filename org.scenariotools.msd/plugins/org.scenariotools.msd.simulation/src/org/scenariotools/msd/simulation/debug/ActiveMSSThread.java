package org.scenariotools.msd.simulation.debug;

import org.apache.log4j.Logger;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.scenariotools.msd.runtime.ActiveMSS;
import org.scenariotools.msd.simulation.plugin.Activator;
import org.scenariotools.msd.util.RuntimeUtil;

public class ActiveMSSThread extends AbstractScenarioThread {

	private static Logger logger = Activator.getLogManager().getLogger(
			ActiveMSSThread.class.getName());

	private ActiveMSS activeMSS;
	private ActiveMSDVariableStackFrame activeMSDVariableStackFrame;

	public ActiveMSSThread(IDebugTarget target, ActiveMSS activeMSS) {
		super(target);
		if (activeMSS != null)
			this.activeMSS = activeMSS;
	}

	public ActiveMSS getActiveProcess() {
		return activeMSS;
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}

	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		if (activeMSDVariableStackFrame == null)
			activeMSDVariableStackFrame = new ActiveMSDVariableStackFrame(getDebugTarget(), activeMSS, this);
		return new IStackFrame[] { activeMSDVariableStackFrame };
	}

	@Override
	public String getName() throws DebugException {
		String bindingString = "";

		String returnString = "active ";
		if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(activeMSS)) {
			returnString += "assumption ";
		} else {
			returnString += "requirement ";
		}

		String cutString = "";
		cutString += " (";
		cutString += activeMSS.getCurrentState().getUmlState();
		cutString += ")";

		return returnString + "MSS: "
				+ getActiveProcess().getStateMachine().getName() + " "
				+ bindingString + cutString;
	}

}
