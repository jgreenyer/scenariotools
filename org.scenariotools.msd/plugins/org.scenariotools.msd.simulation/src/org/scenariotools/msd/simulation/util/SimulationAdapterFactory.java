/**
 */
package org.scenariotools.msd.simulation.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.msd.simulation.*;
import org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent;
import org.scenariotools.msd.simulation.DefaultSystemSimulationAgent;
import org.scenariotools.msd.simulation.SimulationAgent;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.SimulationPackage;
import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.simulation.SimulationPackage
 * @generated
 */
public class SimulationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SimulationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SimulationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationSwitch<Adapter> modelSwitch =
		new SimulationSwitch<Adapter>() {
			@Override
			public Adapter caseSimulationManager(SimulationManager object) {
				return createSimulationManagerAdapter();
			}
			@Override
			public Adapter caseSimulationAgent(SimulationAgent object) {
				return createSimulationAgentAdapter();
			}
			@Override
			public Adapter caseDefaultSystemSimulationAgent(DefaultSystemSimulationAgent object) {
				return createDefaultSystemSimulationAgentAdapter();
			}
			@Override
			public Adapter caseDefaultEnvironmentSimulationAgent(DefaultEnvironmentSimulationAgent object) {
				return createDefaultEnvironmentSimulationAgentAdapter();
			}
			@Override
			public Adapter caseUserInteractingSimulationAgent(UserInteractingSimulationAgent object) {
				return createUserInteractingSimulationAgentAdapter();
			}
			@Override
			public Adapter caseHistoryAgent(HistoryAgent object) {
				return createHistoryAgentAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.SimulationManager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.SimulationManager
	 * @generated
	 */
	public Adapter createSimulationManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.SimulationAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.SimulationAgent
	 * @generated
	 */
	public Adapter createSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.DefaultSystemSimulationAgent <em>Default System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.DefaultSystemSimulationAgent
	 * @generated
	 */
	public Adapter createDefaultSystemSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent <em>Default Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent
	 * @generated
	 */
	public Adapter createDefaultEnvironmentSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.UserInteractingSimulationAgent <em>User Interacting Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.UserInteractingSimulationAgent
	 * @generated
	 */
	public Adapter createUserInteractingSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.msd.simulation.HistoryAgent <em>History Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.msd.simulation.HistoryAgent
	 * @generated
	 */
	public Adapter createHistoryAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SimulationAdapterFactory
