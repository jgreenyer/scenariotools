package org.scenariotools.msd.simulation.debug;

import org.eclipse.debug.core.model.DebugElement;
import org.eclipse.debug.core.model.IDebugTarget;


public class ScenarioDebugElement extends DebugElement {

	public ScenarioDebugElement(IDebugTarget target) {
		super(target);
	}

	@Override
	public String getModelIdentifier() {
		// TODO Auto-generated method stub
		return "org.scenariotools.msd.simulation";
	}
	

}
