package org.scenariotools.msd.simulation.listener;

import java.util.Collection;
import java.util.EventListener;
import org.scenariotools.msd.runtime.MSDRuntimeState;

public interface ISimulationTraceChangeListener extends EventListener {

	public void simulationTraceChanged(Collection<MSDRuntimeState> simulationTrace);
	
}