/**
 */
package org.scenariotools.msd.simulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.msd.simulation.DefaultSystemSimulationAgent;
import org.scenariotools.msd.simulation.SimulationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultSystemSimulationAgentImpl extends UserInteractingSimulationAgentImpl implements DefaultSystemSimulationAgent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultSystemSimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimulationPackage.Literals.DEFAULT_SYSTEM_SIMULATION_AGENT;
	}

} //DefaultSystemSimulationAgentImpl
