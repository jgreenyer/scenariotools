package org.scenariotools.msd.simulation.debug;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.msd.runtime.ActiveProcess;

public class ActiveMSDVariableStackFrame extends AbstractActiveMSDStackFrame{

	private List<ActiveMSDVariable> variables;

	public ActiveMSDVariableStackFrame(IDebugTarget target, ActiveProcess activeMSD,
			AbstractScenarioThread activeMSDThread) {
		super(target, activeMSD, activeMSDThread);
		// TODO Auto-generated constructor stub
	}

	@Override
	public IVariable[] getVariables() throws DebugException {

		refreshVariables();
		
		return variables.toArray(new ActiveMSDVariable[]{});
	}
	
	
	protected void refreshVariables() throws DebugException{
		if (variables == null){
			variables = new ArrayList<ActiveMSDVariable>();
		}

		// 1. remove orphaned Variables
		
		EList<String> currentActiveMSDVariables = new BasicEList<String>();
		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().keySet());
		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().keySet());
		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().keySet());
		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().keySet());
		
				
		EList<String> currentActiveMSDVariablesWithoutRepresentation = new BasicEList<String>(currentActiveMSDVariables);
		Iterator<ActiveMSDVariable> activeMSDVariableIterator = variables.iterator();
		
		while (activeMSDVariableIterator.hasNext()) {
			ActiveMSDVariable activeMSDVariable = activeMSDVariableIterator.next();
			boolean activeMSDVariableForVariableExists = false;
			for (String varName : currentActiveMSDVariables) {
				if (getBindingForVariableName(varName) == null) continue;
				if (activeMSDVariable.getName() == varName){
					activeMSDVariableForVariableExists = true;
					break;
				}
			}
			if (!activeMSDVariableForVariableExists){
				activeMSDVariableIterator.remove();
			}else{
				currentActiveMSDVariablesWithoutRepresentation.remove(activeMSDVariable.getName());
			}
		}

		// 2. create new Variables
		
		for (String varName : currentActiveMSDVariablesWithoutRepresentation) {
			Object bindingForVariable = getBindingForVariableName(varName);
			if (bindingForVariable == null) continue;
			variables.add(new ActiveMSDVariable(getDebugTarget(), varName, bindingForVariable));
		}
	}
	
	private Object getBindingForVariableName(String varName){
		if (getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().get(varName); 
		else if (getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().get(varName); 
		else if (getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().get(varName); 
		else if (getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().get(varName); 
		else return null;
	}

	
	@Override
	public boolean hasVariables() throws DebugException {
		refreshVariables();
		return !variables.isEmpty();
	}

	@Override
	public String getName() throws DebugException {
		return "Diagram variables";
	}

	

}
