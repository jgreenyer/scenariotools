/**
 */
package org.scenariotools.msd.simulation.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.simulation.SimulationPackage;
import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;
import org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Interacting Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.simulation.impl.UserInteractingSimulationAgentImpl#getRegisteredMSDModalEventListChangeListener <em>Registered MSD Modal Event List Change Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UserInteractingSimulationAgentImpl extends SimulationAgentImpl implements UserInteractingSimulationAgent {
	/**
	 * The cached value of the '{@link #getRegisteredMSDModalEventListChangeListener() <em>Registered MSD Modal Event List Change Listener</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredMSDModalEventListChangeListener()
	 * @generated
	 * @ordered
	 */
	protected EList<IMSDModalMessageEventListChangeListener> registeredMSDModalEventListChangeListener;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserInteractingSimulationAgentImpl() {
		super();
	}
	
	
	@Override
	public void stepMade() {
		for (IMSDModalMessageEventListChangeListener msdModalMessageEventListChangeListener : getRegisteredMSDModalEventListChangeListener()) {
			msdModalMessageEventListChangeListener.msdModalMessageEventsChanged((Collection<MSDModalMessageEvent>)(Collection<?>)getSimulationManager().getCurrentMSDRuntimeState().getMessageEventToModalMessageEventMap().values());
		}
	}

	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimulationPackage.Literals.USER_INTERACTING_SIMULATION_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IMSDModalMessageEventListChangeListener> getRegisteredMSDModalEventListChangeListener() {
		if (registeredMSDModalEventListChangeListener == null) {
			registeredMSDModalEventListChangeListener = new EDataTypeUniqueEList<IMSDModalMessageEventListChangeListener>(IMSDModalMessageEventListChangeListener.class, this, SimulationPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER);
		}
		return registeredMSDModalEventListChangeListener;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimulationPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER:
				return getRegisteredMSDModalEventListChangeListener();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimulationPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER:
				getRegisteredMSDModalEventListChangeListener().clear();
				getRegisteredMSDModalEventListChangeListener().addAll((Collection<? extends IMSDModalMessageEventListChangeListener>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimulationPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER:
				getRegisteredMSDModalEventListChangeListener().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimulationPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER:
				return registeredMSDModalEventListChangeListener != null && !registeredMSDModalEventListChangeListener.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (registeredMSDModalEventListChangeListener: ");
		result.append(registeredMSDModalEventListChangeListener);
		result.append(')');
		return result.toString();
	}

} //UserInteractingSimulationAgentImpl
