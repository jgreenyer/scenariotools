/**
 */
package org.scenariotools.msd.simulation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Image;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.simulation.HistoryAgent#getNextState <em>Next State</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.HistoryAgent#getSimulationManager <em>Simulation Manager</em>}</li>
 *   <li>{@link org.scenariotools.msd.simulation.HistoryAgent#getRegisteredSimulationTraceChangeListener <em>Registered Simulation Trace Change Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.simulation.SimulationPackage#getHistoryAgent()
 * @model
 * @generated
 */
public interface HistoryAgent extends EObject {
	/**
	 * Returns the value of the '<em><b>Next State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next State</em>' reference.
	 * @see #setNextState(MSDRuntimeState)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getHistoryAgent_NextState()
	 * @model
	 * @generated
	 */
	MSDRuntimeState getNextState();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.HistoryAgent#getNextState <em>Next State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next State</em>' reference.
	 * @see #getNextState()
	 * @generated
	 */
	void setNextState(MSDRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Manager</em>' reference.
	 * @see #setSimulationManager(SimulationManager)
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getHistoryAgent_SimulationManager()
	 * @model
	 * @generated
	 */
	SimulationManager getSimulationManager();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.simulation.HistoryAgent#getSimulationManager <em>Simulation Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Manager</em>' reference.
	 * @see #getSimulationManager()
	 * @generated
	 */
	void setSimulationManager(SimulationManager value);

	/**
	 * Returns the value of the '<em><b>Registered Simulation Trace Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Simulation Trace Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Simulation Trace Change Listener</em>' attribute list.
	 * @see org.scenariotools.msd.simulation.SimulationPackage#getHistoryAgent_RegisteredSimulationTraceChangeListener()
	 * @model dataType="org.scenariotools.msd.simulation.ISimulationTraceChangeListener" transient="true"
	 * @generated
	 */
	EList<ISimulationTraceChangeListener> getRegisteredSimulationTraceChangeListener();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void terminate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void stepMade();

} // HistoryAgent
