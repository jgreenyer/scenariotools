package org.scenariotools.msd.simulation.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.msd.util.RuntimeUtil;

public class ObjectThread extends AbstractScenarioThread {

	private EObject simulationObject;
	private boolean isSystemObject;
	private ObjectStackFrame objectStackFrame;
	
	public ObjectThread(IDebugTarget target, EObject simulationObject, boolean isSystemObject) {
		super(target);
		this.simulationObject = simulationObject;
		this.isSystemObject = isSystemObject;
		this.objectStackFrame = new ObjectStackFrame(target, this, simulationObject);
	}

	public EObject getSimulationObject(){
		return simulationObject;
	}
	
	@Override
	public String getName() throws DebugException {
		if (isSystemObject)
			return "Sys Object: " + RuntimeUtil.Helper.getEObjectName(getSimulationObject());
		else
			return "Env Object: " + RuntimeUtil.Helper.getEObjectName(getSimulationObject());
	}
	
	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		return new IStackFrame[]{objectStackFrame};
	}
	
	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}
}
