/**
 */
package org.scenariotools.msd.simulation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.msd.simulation.SimulationPackage#getDefaultEnvironmentSimulationAgent()
 * @model
 * @generated
 */
public interface DefaultEnvironmentSimulationAgent extends UserInteractingSimulationAgent {
} // DefaultEnvironmentSimulationAgent
