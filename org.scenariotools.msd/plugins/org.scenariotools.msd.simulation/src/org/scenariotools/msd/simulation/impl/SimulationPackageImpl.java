/**
 */
package org.scenariotools.msd.simulation.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.msd.runtime.RuntimePackage;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationPackage;
import org.scenariotools.msd.simulation.DefaultEnvironmentSimulationAgent;
import org.scenariotools.msd.simulation.DefaultSystemSimulationAgent;
import org.scenariotools.msd.simulation.HistoryAgent;
import org.scenariotools.msd.simulation.SimulationAgent;
import org.scenariotools.msd.simulation.SimulationFactory;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.SimulationPackage;
import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;
import org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener;
import org.scenariotools.msd.simulation.listener.ISimulationTraceChangeListener;
import org.scenariotools.msd.simulation.listener.IStepPerformedListener;
import org.scenariotools.msd.util.UtilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimulationPackageImpl extends EPackageImpl implements SimulationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultSystemSimulationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultEnvironmentSimulationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userInteractingSimulationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historyAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType imsdModalMessageEventListChangeListenerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iStepPerformedListenerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iActiveSimulationAgentChangeListenerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iSimulationTraceChangeListenerEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.simulation.SimulationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SimulationPackageImpl() {
		super(eNS_URI, SimulationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SimulationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SimulationPackage init() {
		if (isInited) return (SimulationPackage)EPackage.Registry.INSTANCE.getEPackage(SimulationPackage.eNS_URI);

		// Obtain or create and register package
		SimulationPackageImpl theSimulationPackage = (SimulationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SimulationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SimulationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();
		ScenariorunconfigurationPackage.eINSTANCE.eClass();
		UtilPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSimulationPackage.createPackageContents();

		// Initialize created meta-data
		theSimulationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSimulationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SimulationPackage.eNS_URI, theSimulationPackage);
		return theSimulationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulationManager() {
		return simulationManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_Pause() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_Terminated() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_StepDelayMilliseconds() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_CurrentSystemSimulationAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_CurrentHistoryAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_RegisteredSystemSimulationAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_RegisteredHistoryAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_CurrentEnvironmentSimulationAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_RegisteredEnvironmentSimulationAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_RegisteredActiveSimulationAgentChangeListener() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_ActiveSimulationAgent() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_RegisteredStepPerformedListener() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_MsdRuntimeStateGraph() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_CurrentMSDRuntimeState() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationManager_ScenarioRunConfiguration() {
		return (EReference)simulationManagerEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationManager_CurrentExecutionSemantics() {
		return (EAttribute)simulationManagerEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulationAgent() {
		return simulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationAgent_SimulationManager() {
		return (EReference)simulationAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationAgent_NextEvent() {
		return (EReference)simulationAgentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultSystemSimulationAgent() {
		return defaultSystemSimulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDefaultEnvironmentSimulationAgent() {
		return defaultEnvironmentSimulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserInteractingSimulationAgent() {
		return userInteractingSimulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserInteractingSimulationAgent_RegisteredMSDModalEventListChangeListener() {
		return (EAttribute)userInteractingSimulationAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoryAgent() {
		return historyAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryAgent_NextState() {
		return (EReference)historyAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryAgent_SimulationManager() {
		return (EReference)historyAgentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryAgent_RegisteredSimulationTraceChangeListener() {
		return (EAttribute)historyAgentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIMSDModalMessageEventListChangeListener() {
		return imsdModalMessageEventListChangeListenerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIStepPerformedListener() {
		return iStepPerformedListenerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIActiveSimulationAgentChangeListener() {
		return iActiveSimulationAgentChangeListenerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getISimulationTraceChangeListener() {
		return iSimulationTraceChangeListenerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationFactory getSimulationFactory() {
		return (SimulationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		simulationManagerEClass = createEClass(SIMULATION_MANAGER);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__PAUSE);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__TERMINATED);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__CURRENT_HISTORY_AGENT);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__MSD_RUNTIME_STATE_GRAPH);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__CURRENT_MSD_RUNTIME_STATE);
		createEReference(simulationManagerEClass, SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION);
		createEAttribute(simulationManagerEClass, SIMULATION_MANAGER__CURRENT_EXECUTION_SEMANTICS);

		simulationAgentEClass = createEClass(SIMULATION_AGENT);
		createEReference(simulationAgentEClass, SIMULATION_AGENT__SIMULATION_MANAGER);
		createEReference(simulationAgentEClass, SIMULATION_AGENT__NEXT_EVENT);

		defaultSystemSimulationAgentEClass = createEClass(DEFAULT_SYSTEM_SIMULATION_AGENT);

		defaultEnvironmentSimulationAgentEClass = createEClass(DEFAULT_ENVIRONMENT_SIMULATION_AGENT);

		userInteractingSimulationAgentEClass = createEClass(USER_INTERACTING_SIMULATION_AGENT);
		createEAttribute(userInteractingSimulationAgentEClass, USER_INTERACTING_SIMULATION_AGENT__REGISTERED_MSD_MODAL_EVENT_LIST_CHANGE_LISTENER);

		historyAgentEClass = createEClass(HISTORY_AGENT);
		createEReference(historyAgentEClass, HISTORY_AGENT__NEXT_STATE);
		createEReference(historyAgentEClass, HISTORY_AGENT__SIMULATION_MANAGER);
		createEAttribute(historyAgentEClass, HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER);

		// Create data types
		imsdModalMessageEventListChangeListenerEDataType = createEDataType(IMSD_MODAL_MESSAGE_EVENT_LIST_CHANGE_LISTENER);
		iStepPerformedListenerEDataType = createEDataType(ISTEP_PERFORMED_LISTENER);
		iActiveSimulationAgentChangeListenerEDataType = createEDataType(IACTIVE_SIMULATION_AGENT_CHANGE_LISTENER);
		iSimulationTraceChangeListenerEDataType = createEDataType(ISIMULATION_TRACE_CHANGE_LISTENER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);
		ScenariorunconfigurationPackage theScenariorunconfigurationPackage = (ScenariorunconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ScenariorunconfigurationPackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		defaultSystemSimulationAgentEClass.getESuperTypes().add(this.getUserInteractingSimulationAgent());
		defaultEnvironmentSimulationAgentEClass.getESuperTypes().add(this.getUserInteractingSimulationAgent());
		userInteractingSimulationAgentEClass.getESuperTypes().add(this.getSimulationAgent());

		// Initialize classes and features; add operations and parameters
		initEClass(simulationManagerEClass, SimulationManager.class, "SimulationManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimulationManager_Pause(), ecorePackage.getEBoolean(), "pause", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationManager_Terminated(), ecorePackage.getEBoolean(), "terminated", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationManager_StepDelayMilliseconds(), theEcorePackage.getEInt(), "stepDelayMilliseconds", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_CurrentSystemSimulationAgent(), this.getSimulationAgent(), null, "currentSystemSimulationAgent", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_CurrentHistoryAgent(), this.getHistoryAgent(), null, "currentHistoryAgent", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_RegisteredSystemSimulationAgent(), this.getSimulationAgent(), null, "registeredSystemSimulationAgent", null, 0, -1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_RegisteredHistoryAgent(), this.getHistoryAgent(), null, "registeredHistoryAgent", null, 0, -1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_CurrentEnvironmentSimulationAgent(), this.getSimulationAgent(), null, "currentEnvironmentSimulationAgent", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_RegisteredEnvironmentSimulationAgent(), this.getSimulationAgent(), null, "registeredEnvironmentSimulationAgent", null, 0, -1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationManager_RegisteredActiveSimulationAgentChangeListener(), this.getIActiveSimulationAgentChangeListener(), "registeredActiveSimulationAgentChangeListener", null, 0, -1, SimulationManager.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_ActiveSimulationAgent(), this.getSimulationAgent(), null, "activeSimulationAgent", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationManager_RegisteredStepPerformedListener(), this.getIStepPerformedListener(), "registeredStepPerformedListener", null, 0, -1, SimulationManager.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_MsdRuntimeStateGraph(), theRuntimePackage.getMSDRuntimeStateGraph(), null, "msdRuntimeStateGraph", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_CurrentMSDRuntimeState(), theRuntimePackage.getMSDRuntimeState(), null, "currentMSDRuntimeState", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationManager_ScenarioRunConfiguration(), theScenariorunconfigurationPackage.getScenarioRunConfiguration(), null, "scenarioRunConfiguration", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationManager_CurrentExecutionSemantics(), theRuntimePackage.getExecutionSemantics(), "currentExecutionSemantics", null, 0, 1, SimulationManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(simulationManagerEClass, null, "performNextStepFromSimulationAgent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimulationAgent(), "simulationAgent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simulationManagerEClass, null, "wait", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimulationAgent(), "simulationAgent", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(simulationManagerEClass, null, "terminate", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simulationManagerEClass, null, "loadStateFromHistoryAgent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getHistoryAgent(), "historyAgent", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(simulationAgentEClass, SimulationAgent.class, "SimulationAgent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimulationAgent_SimulationManager(), this.getSimulationManager(), null, "simulationManager", null, 0, 1, SimulationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationAgent_NextEvent(), theEventsPackage.getEvent(), null, "nextEvent", null, 0, 1, SimulationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(simulationAgentEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(simulationAgentEClass, null, "terminate", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(simulationAgentEClass, null, "stepMade", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(defaultSystemSimulationAgentEClass, DefaultSystemSimulationAgent.class, "DefaultSystemSimulationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(defaultEnvironmentSimulationAgentEClass, DefaultEnvironmentSimulationAgent.class, "DefaultEnvironmentSimulationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(userInteractingSimulationAgentEClass, UserInteractingSimulationAgent.class, "UserInteractingSimulationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserInteractingSimulationAgent_RegisteredMSDModalEventListChangeListener(), this.getIMSDModalMessageEventListChangeListener(), "registeredMSDModalEventListChangeListener", null, 0, -1, UserInteractingSimulationAgent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(historyAgentEClass, HistoryAgent.class, "HistoryAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHistoryAgent_NextState(), theRuntimePackage.getMSDRuntimeState(), null, "nextState", null, 0, 1, HistoryAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryAgent_SimulationManager(), this.getSimulationManager(), null, "simulationManager", null, 0, 1, HistoryAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryAgent_RegisteredSimulationTraceChangeListener(), this.getISimulationTraceChangeListener(), "registeredSimulationTraceChangeListener", null, 0, -1, HistoryAgent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(historyAgentEClass, null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(historyAgentEClass, null, "terminate", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(historyAgentEClass, null, "stepMade", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(imsdModalMessageEventListChangeListenerEDataType, IMSDModalMessageEventListChangeListener.class, "IMSDModalMessageEventListChangeListener", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iStepPerformedListenerEDataType, IStepPerformedListener.class, "IStepPerformedListener", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iActiveSimulationAgentChangeListenerEDataType, IActiveSimulationAgentChangeListener.class, "IActiveSimulationAgentChangeListener", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iSimulationTraceChangeListenerEDataType, ISimulationTraceChangeListener.class, "ISimulationTraceChangeListener", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //SimulationPackageImpl
