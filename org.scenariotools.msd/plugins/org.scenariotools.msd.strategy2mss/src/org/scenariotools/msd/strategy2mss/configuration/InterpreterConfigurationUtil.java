package org.scenariotools.msd.strategy2mss.configuration;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.runtime.RuntimeStateGraph;

import de.upb.swt.qvt.qvtbase.TypedModel;
import de.upb.swt.qvt.tgg.Node;
import de.upb.swt.qvt.tgg.TripleGraphGrammar;
import de.upb.swt.qvt.tgg.TripleGraphGrammarAxiom;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationMode;
import de.upb.swt.qvt.tgg.interpreterconfiguration.ApplicationScenario;
import de.upb.swt.qvt.tgg.interpreterconfiguration.AxiomBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InitialAxiomNodeBinding;
import de.upb.swt.qvt.tgg.interpreterconfiguration.InterpreterconfigurationFactory;
import de.upb.swt.qvt.tgg.interpreterconfiguration.RuleBindingContainer;

public class InterpreterConfigurationUtil {

	@SuppressWarnings("deprecation")
	public static Configuration getTGGInterpreterConfiguration(
							RuntimeStateGraph strategyToTransform,
							IFile scenariorunconfigurationSource){
		
		Configuration configuration = InterpreterconfigurationFactory.eINSTANCE.createConfiguration();
		
		//load ScenarioRunConfiguration Source
		ResourceSet resourceSetSRC = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSetSRC
				.getResource(URI.createPlatformResourceURI(scenariorunconfigurationSource.getFullPath().toString(), true), true);
		ScenarioRunConfiguration scenariorunconfiguration = (ScenarioRunConfiguration) scenarioRunConfigurationResource.getContents().get(0);
		
		// create domain models
		DomainModel umlDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel msdruntimeDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel strategy2mssDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel scenarioRunConfigurationSourceDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		DomainModel scenarioRunConfigurationTargetDomainModel = InterpreterconfigurationFactory.eINSTANCE.createDomainModel();
		
		// create application scenario
		ApplicationScenario applicationScenario = InterpreterconfigurationFactory.eINSTANCE.createApplicationScenario();
		
		// configure application scenario
		applicationScenario.getSourceDomainModel().add(msdruntimeDomainModel);
		applicationScenario.getSourceDomainModel().add(scenarioRunConfigurationSourceDomainModel);
		applicationScenario.getCorrespondenceModel().add(strategy2mssDomainModel);
		applicationScenario.getTargetDomainModel().add(umlDomainModel);
		applicationScenario.getTargetDomainModel().add(scenarioRunConfigurationTargetDomainModel);
		applicationScenario.setName("FWD");
		applicationScenario.setMode(ApplicationMode.TRANSFORM);
				
		// add domain models and application scenario to configuration
		configuration.setRecordRuleBindings(false);
		configuration.getApplicationScenario().add(applicationScenario);
		configuration.setActiveApplicationScenario(applicationScenario);
		configuration.getDomainModel().add(umlDomainModel);
		configuration.getDomainModel().add(msdruntimeDomainModel);
		configuration.getDomainModel().add(strategy2mssDomainModel);
		configuration.getDomainModel().add(scenarioRunConfigurationSourceDomainModel);
		configuration.getDomainModel().add(scenarioRunConfigurationTargetDomainModel);
		
		
		
		// load and set TGG
		ResourceSet resourceSet = strategyToTransform.eResource().getResourceSet();
		Resource tggResource = resourceSet.getResource(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#"), true);
		configuration.setTripleGraphGrammar((TripleGraphGrammar) tggResource.getContents().get(0));
		
		// load and assign typed models to domain models
		TypedModel umlTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_G2670PjkEeKatslYRBpL-Q"), true);
		umlDomainModel.setTypedModel(umlTypedModel);
		TypedModel msdruntimeTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_JFz1QPjkEeKatslYRBpL-Q"), true);
		msdruntimeDomainModel.setTypedModel(msdruntimeTypedModel);
		TypedModel strategy2mssTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_K_PcEPjkEeKatslYRBpL-Q"), true);
		strategy2mssDomainModel.setTypedModel(strategy2mssTypedModel);
		TypedModel scenarioRunConfigurationSourceTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_cA6NUPjkEeKatslYRBpL-Q"), true);
		scenarioRunConfigurationSourceDomainModel.setTypedModel(scenarioRunConfigurationSourceTypedModel);
		TypedModel scenarioRunConfigurationTargetTypedModel = (TypedModel) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_dquqMFOOEeO885WdQaJ99g"), true);
		scenarioRunConfigurationTargetDomainModel.setTypedModel(scenarioRunConfigurationTargetTypedModel);

		//assign resourceURIs to domain models
		// -- container is the parent directory from the scenariorunconfiguration source file
		URI containerURI = URI.createPlatformResourceURI(
				scenariorunconfigurationSource.getFullPath().toString()
				.substring(0, scenariorunconfigurationSource.getFullPath().toString().lastIndexOf("/") + 1), true);
		String containerString = containerURI.toString();
		String baseName = URI.createURI(scenariorunconfigurationSource.getFullPath().toString(), true).trimFileExtension().lastSegment();
		
		// -- target domain
		URI absoluteScenarioRunConfigurationTargetURI = URI.createURI(containerString + baseName + "Controller", true)
				.appendFileExtension("scenariorunconfiguration");
		String relativeScenarioRunConfigurationTargetString = absoluteScenarioRunConfigurationTargetURI.deresolve(containerURI).toString();
		scenarioRunConfigurationTargetDomainModel.getResourceURI().add(relativeScenarioRunConfigurationTargetString);
				
		URI absoluteUMLModelFileNameURI = strategyToTransform.eResource().getURI()
				.trimFileExtension().appendFileExtension("uml");
		URI relativeUMLModelFileNameURI = absoluteUMLModelFileNameURI.deresolve(containerURI);
		String relativeUMLModelFileNameURIString = relativeUMLModelFileNameURI.toString();
		umlDomainModel.getResourceURI().add(relativeUMLModelFileNameURIString);
		
		// -- corresponding domain
		URI absoluteCorrespondingModelFileNameURI = URI.createURI(containerString + baseName + "Controller", true)
				.appendFileExtension("corr.xmi");
		URI relativeCorrespondingModelFileNameURI = absoluteCorrespondingModelFileNameURI.deresolve(containerURI);
		String relativeCorrespondingModelFileNameURIString = relativeCorrespondingModelFileNameURI.toString();
		strategy2mssDomainModel.getResourceURI().add(relativeCorrespondingModelFileNameURIString);
		
		// -- source domain
		scenarioRunConfigurationSourceDomainModel.getRootObject().add(scenariorunconfiguration);
		msdruntimeDomainModel.getRootObject().add(strategyToTransform);
		
		// create initialAxiomBinding		
		RuleBindingContainer ruleBindingContainer = InterpreterconfigurationFactory.eINSTANCE.createRuleBindingContainer();
		configuration.setRuleBindingContainer(ruleBindingContainer);
		AxiomBinding axiomBinding = InterpreterconfigurationFactory.eINSTANCE.createAxiomBinding();
		ruleBindingContainer.setInitialAxiomBinding(axiomBinding);
		TripleGraphGrammarAxiom tggAxiom = (TripleGraphGrammarAxiom) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_Bi-ggPjlEeKatslYRBpL-Q"), true);
		Node node1 = (Node) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_X33pcPjmEeKatslYRBpL-Q"), true);//src Source 
		Node node2 = (Node) resourceSet.getEObject(URI.createURI("platform:/plugin/org.scenariotools.msd.strategy2mss/strategy2mss-tgg/strategy2mss.tgg#_TOFa4PjmEeKatslYRBpL-Q"), true);//stategraph		
		axiomBinding.setTggRule(tggAxiom);
		InitialAxiomNodeBinding initialAxiomNodeBinding1 = InterpreterconfigurationFactory.eINSTANCE.createInitialAxiomNodeBinding();
		InitialAxiomNodeBinding initialAxiomNodeBinding2 = InterpreterconfigurationFactory.eINSTANCE.createInitialAxiomNodeBinding();
		initialAxiomNodeBinding1.setNode(node1);
		initialAxiomNodeBinding2.setNode(node2);
		initialAxiomNodeBinding1.setEObject(scenariorunconfiguration);
		initialAxiomNodeBinding2.setEObject(strategyToTransform);
		axiomBinding.getInitialAxiomNodeBinding().add(initialAxiomNodeBinding1);
		axiomBinding.getInitialAxiomNodeBinding().add(initialAxiomNodeBinding2);
		
		return configuration;
	}
	
}


