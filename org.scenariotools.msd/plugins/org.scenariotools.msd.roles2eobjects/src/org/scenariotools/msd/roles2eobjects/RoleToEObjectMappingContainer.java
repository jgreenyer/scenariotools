/**
 */
package org.scenariotools.msd.roles2eobjects;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role To EObject Mapping Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer#getRoleToEObjects <em>Role To EObjects</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#getRoleToEObjectMappingContainer()
 * @model
 * @generated
 */
public interface RoleToEObjectMappingContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Role To EObjects</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role To EObjects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role To EObjects</em>' containment reference list.
	 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#getRoleToEObjectMappingContainer_RoleToEObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<RoleToEObjectMapping> getRoleToEObjects();

} // RoleToEObjectMappingContainer
