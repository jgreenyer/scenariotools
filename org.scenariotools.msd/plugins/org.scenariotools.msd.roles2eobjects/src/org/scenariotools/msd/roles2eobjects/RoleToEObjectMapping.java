/**
 */
package org.scenariotools.msd.roles2eobjects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role To EObject Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getEObject <em>EObject</em>}</li>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getRole <em>Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#getRoleToEObjectMapping()
 * @model
 * @generated
 */
public interface RoleToEObjectMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject</em>' reference.
	 * @see #setEObject(EObject)
	 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#getRoleToEObjectMapping_EObject()
	 * @model
	 * @generated
	 */
	EObject getEObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getEObject <em>EObject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EObject</em>' reference.
	 * @see #getEObject()
	 * @generated
	 */
	void setEObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Property)
	 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#getRoleToEObjectMapping_Role()
	 * @model
	 * @generated
	 */
	Property getRole();

	/**
	 * Sets the value of the '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Property value);

} // RoleToEObjectMapping
