/**
 */
package org.scenariotools.msd.roles2eobjects.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.roles2eobjects.util.Roles2eobjectsResourceFactoryImpl
 * @generated
 */
public class Roles2eobjectsResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Roles2eobjectsResourceImpl(URI uri) {
		super(uri);
	}

} //Roles2eobjectsResourceImpl
