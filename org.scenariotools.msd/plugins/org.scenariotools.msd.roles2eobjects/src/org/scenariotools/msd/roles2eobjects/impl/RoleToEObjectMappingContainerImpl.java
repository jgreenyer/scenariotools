/**
 */
package org.scenariotools.msd.roles2eobjects.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role To EObject Mapping Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingContainerImpl#getRoleToEObjects <em>Role To EObjects</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RoleToEObjectMappingContainerImpl extends EObjectImpl implements RoleToEObjectMappingContainer {
	/**
	 * The cached value of the '{@link #getRoleToEObjects() <em>Role To EObjects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleToEObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<RoleToEObjectMapping> roleToEObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleToEObjectMappingContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Roles2eobjectsPackage.Literals.ROLE_TO_EOBJECT_MAPPING_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoleToEObjectMapping> getRoleToEObjects() {
		if (roleToEObjects == null) {
			roleToEObjects = new EObjectContainmentEList<RoleToEObjectMapping>(RoleToEObjectMapping.class, this, Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS);
		}
		return roleToEObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS:
				return ((InternalEList<?>)getRoleToEObjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS:
				return getRoleToEObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS:
				getRoleToEObjects().clear();
				getRoleToEObjects().addAll((Collection<? extends RoleToEObjectMapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS:
				getRoleToEObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS:
				return roleToEObjects != null && !roleToEObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RoleToEObjectMappingContainerImpl
