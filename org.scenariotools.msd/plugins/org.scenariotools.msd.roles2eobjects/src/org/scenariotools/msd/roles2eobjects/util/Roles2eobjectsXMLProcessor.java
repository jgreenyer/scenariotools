/**
 */
package org.scenariotools.msd.roles2eobjects.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Roles2eobjectsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Roles2eobjectsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		Roles2eobjectsPackage.eINSTANCE.eClass();
	}
	
	/**
	 * Register for "*" and "xml" file extensions the Roles2eobjectsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new Roles2eobjectsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new Roles2eobjectsResourceFactoryImpl());
		}
		return registrations;
	}

} //Roles2eobjectsXMLProcessor
