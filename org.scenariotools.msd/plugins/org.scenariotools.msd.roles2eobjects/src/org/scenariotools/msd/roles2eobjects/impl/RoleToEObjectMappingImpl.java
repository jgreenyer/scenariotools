/**
 */
package org.scenariotools.msd.roles2eobjects.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role To EObject Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl#getEObject <em>EObject</em>}</li>
 *   <li>{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl#getRole <em>Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RoleToEObjectMappingImpl extends EObjectImpl implements RoleToEObjectMapping {
	/**
	 * The cached value of the '{@link #getEObject() <em>EObject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObject()
	 * @generated
	 * @ordered
	 */
	protected EObject eObject;

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected Property role;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleToEObjectMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Roles2eobjectsPackage.Literals.ROLE_TO_EOBJECT_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getEObject() {
		if (eObject != null && eObject.eIsProxy()) {
			InternalEObject oldEObject = (InternalEObject)eObject;
			eObject = eResolveProxy(oldEObject);
			if (eObject != oldEObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT, oldEObject, eObject));
			}
		}
		return eObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetEObject() {
		return eObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEObject(EObject newEObject) {
		EObject oldEObject = eObject;
		eObject = newEObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT, oldEObject, eObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getRole() {
		if (role != null && role.eIsProxy()) {
			InternalEObject oldRole = (InternalEObject)role;
			role = (Property)eResolveProxy(oldRole);
			if (role != oldRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE, oldRole, role));
			}
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(Property newRole) {
		Property oldRole = role;
		role = newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT:
				if (resolve) return getEObject();
				return basicGetEObject();
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE:
				if (resolve) return getRole();
				return basicGetRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT:
				setEObject((EObject)newValue);
				return;
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE:
				setRole((Property)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT:
				setEObject((EObject)null);
				return;
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE:
				setRole((Property)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__EOBJECT:
				return eObject != null;
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING__ROLE:
				return role != null;
		}
		return super.eIsSet(featureID);
	}

} //RoleToEObjectMappingImpl
