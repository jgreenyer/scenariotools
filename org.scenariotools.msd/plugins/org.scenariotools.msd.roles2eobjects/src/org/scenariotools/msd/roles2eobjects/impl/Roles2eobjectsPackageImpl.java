/**
 */
package org.scenariotools.msd.roles2eobjects.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsFactory;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Roles2eobjectsPackageImpl extends EPackageImpl implements Roles2eobjectsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleToEObjectMappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleToEObjectMappingContainerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Roles2eobjectsPackageImpl() {
		super(eNS_URI, Roles2eobjectsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Roles2eobjectsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Roles2eobjectsPackage init() {
		if (isInited) return (Roles2eobjectsPackage)EPackage.Registry.INSTANCE.getEPackage(Roles2eobjectsPackage.eNS_URI);

		// Obtain or create and register package
		Roles2eobjectsPackageImpl theRoles2eobjectsPackage = (Roles2eobjectsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Roles2eobjectsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Roles2eobjectsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRoles2eobjectsPackage.createPackageContents();

		// Initialize created meta-data
		theRoles2eobjectsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoles2eobjectsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Roles2eobjectsPackage.eNS_URI, theRoles2eobjectsPackage);
		return theRoles2eobjectsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleToEObjectMapping() {
		return roleToEObjectMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleToEObjectMapping_EObject() {
		return (EReference)roleToEObjectMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleToEObjectMapping_Role() {
		return (EReference)roleToEObjectMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleToEObjectMappingContainer() {
		return roleToEObjectMappingContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleToEObjectMappingContainer_RoleToEObjects() {
		return (EReference)roleToEObjectMappingContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Roles2eobjectsFactory getRoles2eobjectsFactory() {
		return (Roles2eobjectsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roleToEObjectMappingEClass = createEClass(ROLE_TO_EOBJECT_MAPPING);
		createEReference(roleToEObjectMappingEClass, ROLE_TO_EOBJECT_MAPPING__EOBJECT);
		createEReference(roleToEObjectMappingEClass, ROLE_TO_EOBJECT_MAPPING__ROLE);

		roleToEObjectMappingContainerEClass = createEClass(ROLE_TO_EOBJECT_MAPPING_CONTAINER);
		createEReference(roleToEObjectMappingContainerEClass, ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(roleToEObjectMappingEClass, RoleToEObjectMapping.class, "RoleToEObjectMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleToEObjectMapping_EObject(), theEcorePackage.getEObject(), null, "eObject", null, 0, 1, RoleToEObjectMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRoleToEObjectMapping_Role(), theUMLPackage.getProperty(), null, "role", null, 0, 1, RoleToEObjectMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleToEObjectMappingContainerEClass, RoleToEObjectMappingContainer.class, "RoleToEObjectMappingContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleToEObjectMappingContainer_RoleToEObjects(), this.getRoleToEObjectMapping(), null, "roleToEObjects", null, 0, -1, RoleToEObjectMappingContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Roles2eobjectsPackageImpl
