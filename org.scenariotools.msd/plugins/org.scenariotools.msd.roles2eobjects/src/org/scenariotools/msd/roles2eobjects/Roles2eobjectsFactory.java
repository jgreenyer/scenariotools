/**
 */
package org.scenariotools.msd.roles2eobjects;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage
 * @generated
 */
public interface Roles2eobjectsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Roles2eobjectsFactory eINSTANCE = org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Role To EObject Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role To EObject Mapping</em>'.
	 * @generated
	 */
	RoleToEObjectMapping createRoleToEObjectMapping();

	/**
	 * Returns a new object of class '<em>Role To EObject Mapping Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role To EObject Mapping Container</em>'.
	 * @generated
	 */
	RoleToEObjectMappingContainer createRoleToEObjectMappingContainer();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Roles2eobjectsPackage getRoles2eobjectsPackage();

} //Roles2eobjectsFactory
