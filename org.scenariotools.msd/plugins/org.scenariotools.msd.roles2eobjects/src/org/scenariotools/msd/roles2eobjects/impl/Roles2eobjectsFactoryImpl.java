/**
 */
package org.scenariotools.msd.roles2eobjects.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.msd.roles2eobjects.*;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsFactory;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Roles2eobjectsFactoryImpl extends EFactoryImpl implements Roles2eobjectsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Roles2eobjectsFactory init() {
		try {
			Roles2eobjectsFactory theRoles2eobjectsFactory = (Roles2eobjectsFactory)EPackage.Registry.INSTANCE.getEFactory("http://org.scenariotools.msd.roles2eobjects/1.0"); 
			if (theRoles2eobjectsFactory != null) {
				return theRoles2eobjectsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Roles2eobjectsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Roles2eobjectsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING: return createRoleToEObjectMapping();
			case Roles2eobjectsPackage.ROLE_TO_EOBJECT_MAPPING_CONTAINER: return createRoleToEObjectMappingContainer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleToEObjectMapping createRoleToEObjectMapping() {
		RoleToEObjectMappingImpl roleToEObjectMapping = new RoleToEObjectMappingImpl();
		return roleToEObjectMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleToEObjectMappingContainer createRoleToEObjectMappingContainer() {
		RoleToEObjectMappingContainerImpl roleToEObjectMappingContainer = new RoleToEObjectMappingContainerImpl();
		return roleToEObjectMappingContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Roles2eobjectsPackage getRoles2eobjectsPackage() {
		return (Roles2eobjectsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Roles2eobjectsPackage getPackage() {
		return Roles2eobjectsPackage.eINSTANCE;
	}

} //Roles2eobjectsFactoryImpl
