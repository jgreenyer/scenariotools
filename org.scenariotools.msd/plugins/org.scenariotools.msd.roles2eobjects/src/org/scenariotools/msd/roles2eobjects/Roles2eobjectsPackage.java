/**
 */
package org.scenariotools.msd.roles2eobjects;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.msd.roles2eobjects.Roles2eobjectsFactory
 * @model kind="package"
 * @generated
 */
public interface Roles2eobjectsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "roles2eobjects";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.msd.roles2eobjects/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "roles2eobjects";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Roles2eobjectsPackage eINSTANCE = org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl <em>Role To EObject Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl
	 * @see org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsPackageImpl#getRoleToEObjectMapping()
	 * @generated
	 */
	int ROLE_TO_EOBJECT_MAPPING = 0;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAPPING__EOBJECT = 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAPPING__ROLE = 1;

	/**
	 * The number of structural features of the '<em>Role To EObject Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAPPING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingContainerImpl <em>Role To EObject Mapping Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingContainerImpl
	 * @see org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsPackageImpl#getRoleToEObjectMappingContainer()
	 * @generated
	 */
	int ROLE_TO_EOBJECT_MAPPING_CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Role To EObjects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS = 0;

	/**
	 * The number of structural features of the '<em>Role To EObject Mapping Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAPPING_CONTAINER_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping <em>Role To EObject Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role To EObject Mapping</em>'.
	 * @see org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping
	 * @generated
	 */
	EClass getRoleToEObjectMapping();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getEObject <em>EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject</em>'.
	 * @see org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getEObject()
	 * @see #getRoleToEObjectMapping()
	 * @generated
	 */
	EReference getRoleToEObjectMapping_EObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping#getRole()
	 * @see #getRoleToEObjectMapping()
	 * @generated
	 */
	EReference getRoleToEObjectMapping_Role();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer <em>Role To EObject Mapping Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role To EObject Mapping Container</em>'.
	 * @see org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer
	 * @generated
	 */
	EClass getRoleToEObjectMappingContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer#getRoleToEObjects <em>Role To EObjects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role To EObjects</em>'.
	 * @see org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer#getRoleToEObjects()
	 * @see #getRoleToEObjectMappingContainer()
	 * @generated
	 */
	EReference getRoleToEObjectMappingContainer_RoleToEObjects();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Roles2eobjectsFactory getRoles2eobjectsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl <em>Role To EObject Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingImpl
		 * @see org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsPackageImpl#getRoleToEObjectMapping()
		 * @generated
		 */
		EClass ROLE_TO_EOBJECT_MAPPING = eINSTANCE.getRoleToEObjectMapping();

		/**
		 * The meta object literal for the '<em><b>EObject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAPPING__EOBJECT = eINSTANCE.getRoleToEObjectMapping_EObject();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAPPING__ROLE = eINSTANCE.getRoleToEObjectMapping_Role();

		/**
		 * The meta object literal for the '{@link org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingContainerImpl <em>Role To EObject Mapping Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.msd.roles2eobjects.impl.RoleToEObjectMappingContainerImpl
		 * @see org.scenariotools.msd.roles2eobjects.impl.Roles2eobjectsPackageImpl#getRoleToEObjectMappingContainer()
		 * @generated
		 */
		EClass ROLE_TO_EOBJECT_MAPPING_CONTAINER = eINSTANCE.getRoleToEObjectMappingContainer();

		/**
		 * The meta object literal for the '<em><b>Role To EObjects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAPPING_CONTAINER__ROLE_TO_EOBJECTS = eINSTANCE.getRoleToEObjectMappingContainer_RoleToEObjects();

	}

} //Roles2eobjectsPackage
