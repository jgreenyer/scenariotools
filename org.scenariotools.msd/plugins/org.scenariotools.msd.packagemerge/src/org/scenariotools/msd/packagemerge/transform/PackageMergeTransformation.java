package org.scenariotools.msd.packagemerge.transform;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.resource.UMLResource;

public class PackageMergeTransformation {

	public Package execute(Model model, String packageName, String containerName, String mergedModelName) {

		// create URI for the transformation file
		URI transformationURI = URI
				.createPlatformPluginURI("/org.scenariotools.msd.packagemerge/transforms/MSDSpecMerge.qvto", true);
		// create executor and execution context
		TransformationExecutor executor = new TransformationExecutor(transformationURI);
		ExecutionContextImpl context = new ExecutionContextImpl();

		context.setConfigProperty("selectedPackageName", packageName);
		// prepare the input
		List<Model> inObjects = Arrays.asList(model);

		// create input extend containing the BlockModel
		// create empty output extend
		ModelExtent input = new BasicModelExtent(inObjects);
		ModelExtent mergedModel = new BasicModelExtent();

		// execute transformation
		ExecutionDiagnostic result = executor.execute(context, input, mergedModel);

		// this will report if the transformation performed correctly
		if (result.getSeverity() != ExecutionDiagnostic.OK) {
			String message = "QVT-O ERROR on rule transformation. Message was: " + System.getProperty("line.separator")
					+ result.getMessage();
			System.out.println(message);
		}
		
		Package mergedPackage = (Package) mergedModel.getContents().get(0);

		saveEMFModel(model.eResource().getURI(), mergedModel.getContents(), containerName, mergedModelName, "uml");
		return mergedPackage;
	}

//	/**
//	 * Executes the transformation and returns the result. The result is also
//	 * stored to a file.
//	 * 
//	 * @return The transformed EPackage
//	 */
//	public void execute(URI modelURI, String packageName, String mergedModelName, String targetModelName,
//			String traceFileName) {
//		targetModelName = removeFileExtension(targetModelName);
//		traceFileName = removeFileExtension(traceFileName);
//		Model model = (Model) loadEMFModel(modelURI);
//
//		execute(model, packageName, mergedModelName);
//	}

	private String removeFileExtension(String fileName) {
		if (fileName.contains(".")) {
			fileName = fileName.substring(0, fileName.indexOf("."));
		}
		return fileName;
	}

	protected void saveEMFModel(URI modelURI, EObject model, String fileName, String fileExtension) {
		URI uri = modelURI.trimFileExtension().trimSegments(1).appendSegment(fileName);
		if (!fileName.endsWith(fileExtension))
			uri.appendFileExtension(fileExtension);

		ResourceSet rs = new ResourceSetImpl();

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());

		Resource r = rs.createResource(uri);

		r.getContents().add(model);

		try {
			r.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void saveEMFModel(URI modelURI, List<EObject> model, String containerName, String fileName, String fileExtension) {
		URI uri = URI.createPlatformResourceURI(containerName + "/" + fileName, true);
		if (!fileName.endsWith(fileExtension))
			uri.appendFileExtension(fileExtension);

		ResourceSet rs = new ResourceSetImpl();
		Map extensionToFactoryMap = rs.getResourceFactoryRegistry().getExtensionToFactoryMap();
		extensionToFactoryMap.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
		extensionToFactoryMap.put("ecore", new EcoreResourceFactoryImpl());
		extensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(fileExtension, new XMIResourceFactoryImpl());

		Resource r = rs.createResource(uri);

		r.getContents().addAll(model);

		try {
			r.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private EObject loadEMFModel(URI uri) {

		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.getResource(uri, true);

		try {
			r.load(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return r.getContents().get(0);
	}

}
