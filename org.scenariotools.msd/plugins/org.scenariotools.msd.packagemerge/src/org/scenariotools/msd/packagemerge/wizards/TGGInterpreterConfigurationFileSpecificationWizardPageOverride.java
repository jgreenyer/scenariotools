package org.scenariotools.msd.packagemerge.wizards;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.uml2.uml.Package;
import org.scenariotools.msd.simulation.ui.wizards.AbstractScenarioConfigurationWizard;
import org.scenariotools.msd.simulation.ui.wizards.TGGInterpreterConfigurationFileSpecificationWizardPage;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one
 * (interpreterconfiguration).
 */

public class TGGInterpreterConfigurationFileSpecificationWizardPageOverride
		extends TGGInterpreterConfigurationFileSpecificationWizardPage {

	protected Text mergedModelText;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param resourceSet
	 * @param scenarioConfigurationWizard
	 * 
	 * @param pageName
	 */
	public TGGInterpreterConfigurationFileSpecificationWizardPageOverride(
			AbstractScenarioConfigurationWizard scenarioConfigurationWizard) {
		super(scenarioConfigurationWizard);
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;

		Label containerLabel = new Label(container, SWT.NULL);
		containerLabel.setText("&Container:");

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);

		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		containerText.setText("");
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button browseContainerButton = new Button(container, SWT.PUSH);
		browseContainerButton.setText("Browse...");
		browseContainerButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseContainer();
			}
		});

		Label tggInterpreterConfigurationfileNameLabel = new Label(container, SWT.NULL);
		tggInterpreterConfigurationfileNameLabel.setText("&Interpreter configuration file name:");

		interpreterConfigurationfileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		Button setDefaultInterpreterConfigurationfileTextButton = new Button(container, SWT.PUSH);
		setDefaultInterpreterConfigurationfileTextButton.setText("Set Default");
		setDefaultInterpreterConfigurationfileTextButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setDefaultInterpreterConfigurationfileText();
			}
		});
		interpreterConfigurationfileText.setLayoutData(gd);
		interpreterConfigurationfileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Label mergedModelFileNameLabel = new Label(container, SWT.NULL);
		mergedModelFileNameLabel.setText("&Merged UML model file name:");

		mergedModelText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		mergedModelText.setLayoutData(gd);
		Button setDefaultMergedModelFileTextButton = new Button(container, SWT.PUSH);
		setDefaultMergedModelFileTextButton.setText("Set Default");
		setDefaultMergedModelFileTextButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setMergedModelFileText();
			}
		});
		mergedModelText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Label eCoreFileNameLabel = new Label(container, SWT.NULL);
		eCoreFileNameLabel.setText("&ECore model file name:");

		ecoreFileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		ecoreFileText.setLayoutData(gd);
		Button setDefaultECoreFileTextButton = new Button(container, SWT.PUSH);
		setDefaultECoreFileTextButton.setText("Set Default");
		setDefaultECoreFileTextButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setDefaultECoreFileText();
			}
		});
		ecoreFileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Label correspondenceModelFileNameLabel = new Label(container, SWT.NULL);
		correspondenceModelFileNameLabel.setText("&Correspondence model file name:");

		correspondenceModelFileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		Button setDefaultCorrespondenceModelFileTextButton = new Button(container, SWT.PUSH);
		setDefaultCorrespondenceModelFileTextButton.setText("Set Default");
		setDefaultCorrespondenceModelFileTextButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setDefaultCorrespondenceModelFileText();
			}
		});
		correspondenceModelFileText.setLayoutData(gd);
		correspondenceModelFileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		dialogChanged();
		setControl(container);
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {

		IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(getContainerName()));

		if (getContainerName().length() == 0) {
			updateStatus("File container must be specified");
			return;
		}
		if (container == null || (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("File container must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}

		String interpreterConfigurationFileName = getInterpreterConfigurationFileName();

		if (interpreterConfigurationFileName.length() == 0) {
			updateStatus("Interpreter configuration file name must be specified");
			return;
		}
		if (isValidFileName(interpreterConfigurationFileName)) {
			updateStatus("Interpreter configuration file name must be valid");
			return;
		}
		int dotLoc = interpreterConfigurationFileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = interpreterConfigurationFileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("interpreterconfiguration") == false) {
				updateStatus("File extension must be \"interpreterconfiguration\"");
				return;
			}
		}

		String ecoreFileName = getECoreFileName();

		if (ecoreFileName.length() == 0) {
			updateStatus("ECore file name must be specified");
			return;
		}
		if (isValidFileName(ecoreFileName)) {
			updateStatus("ECore file name must be valid");
			return;
		}
		dotLoc = ecoreFileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = ecoreFileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("ecore") == false) {
				updateStatus("File extension must be \"ecore\"");
				return;
			}
		}

		String correspondenceModelFileName = getCorrespondenceModelFileName();

		if (correspondenceModelFileName.length() == 0) {
			updateStatus("Correspondence model file name must be specified");
			return;
		}
		if (isValidFileName(correspondenceModelFileName)) {
			updateStatus("Correspondence model file name must be valid");
			return;
		}
		dotLoc = correspondenceModelFileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = correspondenceModelFileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("xmi") == false) {
				updateStatus("File extension must be \"xmi\"");
				return;
			}
		}

		updateStatus(null);
	}

	public String getMergedModelFileName() {
		return mergedModelText.getText();
	}

	protected void setMergedModelFileText() {
		mergedModelText.setText(getDefaultFileName() + "_merged.uml");
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			if (mergedModelText.getText() == "")
				setMergedModelFileText();
		}
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
}