package org.scenariotools.msd.packagemerge.transform;


import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.internal.qvt.oml.InternalTransformationExecutor;
import org.eclipse.m2m.internal.qvt.oml.trace.Trace;

@SuppressWarnings("restriction")
public class InternalTransformationExecutorOverride extends
		InternalTransformationExecutor {

	
	private Trace trace; 
	private boolean store_trace = true;
	private String tracePath;


	public InternalTransformationExecutorOverride(URI uri) {
		super(uri);
	}

	public InternalTransformationExecutorOverride(URI uri, Registry registry) {
		super(uri, registry);
	}

	@Override
	protected void handleExecutionTraces(Trace traces) {
		setTrace(traces);
	}

	
	protected void saveEMFModel(String path, EObject model, String modeltype){
	    ResourceSet rs = new ResourceSetImpl();    

	    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;	    
	    Map<String, Object> m = reg.getExtensionToFactoryMap();
	    m.put(modeltype, new XMIResourceFactoryImpl());

	    Resource r = rs.createResource(URI.createFileURI(path));
	   
	    r.getContents().add(model);

	    try {
			r.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isStore_trace() {
		return store_trace;
	}

	public void setStore_trace(boolean store_trace) {
		this.store_trace = store_trace;
	}

	public String getTracePath() {
		return tracePath;
	}

	public void setTracePath(String tracePath) {
		this.tracePath = tracePath;
	}

	public Trace getTrace() {
		return trace;
	}

	public void setTrace(Trace trace) {
		this.trace = trace;
	}
	

}
