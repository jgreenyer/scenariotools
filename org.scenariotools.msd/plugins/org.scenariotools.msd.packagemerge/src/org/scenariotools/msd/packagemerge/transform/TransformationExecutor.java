package org.scenariotools.msd.packagemerge.transform;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.m2m.internal.qvt.oml.trace.Trace;
import org.eclipse.m2m.qvt.oml.ExecutionContext;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;

/**
 * A utility class that enables to execute existing transformation in the
 * specified execution context.
 * 
 * @since 2.0
 * @see org.eclipse.m2m.qvt.oml.TransformationExecutor
 */
@SuppressWarnings("restriction")
public final class TransformationExecutor {

	private InternalTransformationExecutorOverride fExector;
	
	/**
	 * Constructs the executor for the given transformation URI.
	 * <p>
	 * No attempt to resolve and load the transformation is done at this step
	 * 
	 * @param uri
	 *            the URI of an existing transformation
	 */
	public TransformationExecutor(URI uri) {
		fExector = new InternalTransformationExecutorOverride(uri);
	}
	
	/**
	 * Constructs the executor for the given transformation URI.
	 * <p>
	 * No attempt to resolve and load the transformation is done at this step
	 * 
	 * @param uri
	 *            the URI of an existing transformation
	 * @param registry
	 *            a package registry of meta-models to be referenced by the
	 *            executed transformation
	 * @since 3.0
	 */
	public TransformationExecutor(URI uri, EPackage.Registry registry) {
		fExector = new InternalTransformationExecutorOverride(uri, registry);
	}
			
	/**
	 * Attempts to load the transformation referred by this executor and checks
	 * if it is valid for execution.
	 * <p>
	 * <b>Remark:</b></br> Only the first performs the actual transformation
	 * loading, subsequent calls to this method will return the existing
	 * diagnostic.
	 * 
	 * @return the diagnostic indicating possible problems of the load action
	 */
	public Diagnostic loadTransformation() {
		return fExector.loadTransformation(new NullProgressMonitor());
	}

	/**
	 * Executes the transformation referred by this executor using the given
	 * model parameters and execution context.
	 * 
	 * @param executionContext
	 *            the context object keeping the execution environment details
	 * @param modelParameters
	 *            the actual model arguments to the transformation
	 * 
	 * @return the diagnostic object indicating the execution result status,
	 *         also keeping the details of possible problems
	 * @throws IllegalArgumentException
	 *             if the context or any of the model parameters is
	 *             <code>null</code>
	 */
	public ExecutionDiagnostic execute(ExecutionContext executionContext,
			ModelExtent... modelParameters) {
		return fExector.execute(executionContext, modelParameters);
	}
	
	/**
	 * Sets if the trace should be stored
	 * @param storeTrace True, iff the trace should be stored
	 */
	public void setStoreTrace(boolean storeTrace) {
		fExector.setStore_trace(storeTrace);
	}
	
	/**
	 * 
	 * @return the trace model of the executed transformation 
	 */
	public Trace getTrace() {
		return fExector.getTrace();
	}
}

