package org.scenariotools.msd.simulation.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

public class PerformTransformationUponFinishSelectionWizardPage extends
		WizardPage {

	private Button performTGGTransformationCheckButton;
	private Button initializeEObjectSystemCheckButton;
	private Label initializeEObjectSystemCheckLabel;

	private Button createScenarioRunConfigurationCheckButton;
	private Label createScenarioRunConfigurationLabel;

	private Button createRoleToEObjectMappingCheckButton;
	private Label createRoleToEObjectMappingLabel;

	
	private boolean performTGGTransformation;
	private boolean initializeEObjectSystem;
	private boolean createScenarioRunConfiguration;
	private boolean createRoleToEObjectMapping;
	private AbstractScenarioConfigurationWizard scenarioConfigurationWizard;
	
	public PerformTransformationUponFinishSelectionWizardPage(AbstractScenarioConfigurationWizard scenarioConfigurationWizard) {
		super("wizardpage");
		this.setScenarioConfigurationWizard(scenarioConfigurationWizard);
		setTitle("Perform TGG Transformation after finish?");
		setTitle("Specify whether the configured transformation shall be run after completing the wizard and wheter an instance system shall be created from a collaboration diagram contained in the UML package.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
	    GridLayout layout = new GridLayout();
	    container.setLayout(layout);
	    layout.numColumns = 2;
	    
	    Label performTGGTransformationCheckLabel = new Label(container, SWT.NONE);
	    performTGGTransformationCheckLabel.setText("Perform TGG Transformation upon finish");
	    performTGGTransformationCheckButton = new Button(container, SWT.CHECK);
	    GridData gd = new GridData(GridData.END | GridData.FILL_HORIZONTAL);
	    performTGGTransformationCheckButton.setSelection(false);
	    performTGGTransformationCheckButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handlePerformTGGTransformationChecked();
			}

		});
	    performTGGTransformationCheckButton.setLayoutData(gd);

	    
	    initializeEObjectSystemCheckLabel = new Label(container, SWT.NONE);
	    initializeEObjectSystemCheckLabel.setText("Initialize EObject system from collaboration structure");
	    initializeEObjectSystemCheckButton = new Button(container, SWT.CHECK);
	    initializeEObjectSystemCheckLabel.setEnabled(false);
	    initializeEObjectSystemCheckButton.setSelection(false);
	    initializeEObjectSystemCheckButton.setEnabled(false);
	    initializeEObjectSystemCheckButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleInitializeEObjectSystemChecked();
			}
		});
	    initializeEObjectSystemCheckButton.setLayoutData(gd);

	    
	    createRoleToEObjectMappingLabel = new Label(container, SWT.NONE);
	    createRoleToEObjectMappingLabel.setText("Create a role-to-eobject mapping (for a static MSD interpretation)");
	    createRoleToEObjectMappingCheckButton = new Button(container, SWT.CHECK);
	    createRoleToEObjectMappingLabel.setEnabled(false);
	    createRoleToEObjectMappingCheckButton.setSelection(false);
	    createRoleToEObjectMappingCheckButton.setEnabled(false);
	    createRoleToEObjectMappingCheckButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleCreateRoleToEObjectMappingChecked();
			}
		});
	    createRoleToEObjectMappingCheckButton.setLayoutData(gd);
	    
	    createScenarioRunConfigurationLabel = new Label(container, SWT.NONE);
	    createScenarioRunConfigurationLabel.setText("Create scenario run configuration &(NOTE: You may need to manually configure the use case specifications to be considered in the run)");
	    createScenarioRunConfigurationCheckButton = new Button(container, SWT.CHECK);
	    createScenarioRunConfigurationLabel.setEnabled(false);
	    createScenarioRunConfigurationCheckButton.setSelection(false);
	    createScenarioRunConfigurationCheckButton.setEnabled(false);
	    createScenarioRunConfigurationCheckButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleCreateScenarioRunConfigurationChecked();
			}
		});
	    createScenarioRunConfigurationCheckButton.setLayoutData(gd);

		dialogChanged();
		setControl(container);
	}

	private void handlePerformTGGTransformationChecked() {
		initializeEObjectSystemCheckButton.setEnabled(performTGGTransformationCheckButton.getSelection());
		initializeEObjectSystemCheckLabel.setEnabled(performTGGTransformationCheckButton.getSelection());
		if (!performTGGTransformationCheckButton.getSelection())
			initializeEObjectSystemCheckButton.setSelection(false);
		
		performTGGTransformation = performTGGTransformationCheckButton.getSelection();
		
		handleInitializeEObjectSystemChecked();
	}
	
	private void handleInitializeEObjectSystemChecked() {

		initializeEObjectSystem = initializeEObjectSystemCheckButton.getSelection();

		createRoleToEObjectMappingCheckButton.setEnabled(initializeEObjectSystemCheckButton.getSelection());
		createRoleToEObjectMappingLabel.setEnabled(initializeEObjectSystemCheckButton.getSelection());
		
		createScenarioRunConfigurationCheckButton.setEnabled(initializeEObjectSystemCheckButton.getSelection());
		createScenarioRunConfigurationLabel.setEnabled(initializeEObjectSystemCheckButton.getSelection());
		
		if (!initializeEObjectSystemCheckButton.getSelection()){
			createRoleToEObjectMapping = false;
			createScenarioRunConfiguration = false;
			createRoleToEObjectMappingCheckButton.setSelection(false);
			createScenarioRunConfigurationCheckButton.setSelection(false);
			getScenarioConfigurationWizard().setCreateObjectSystemFromCollaboration(false);
		}
		if (initializeEObjectSystemCheckButton.getSelection()){
			createRoleToEObjectMapping = true;
			createScenarioRunConfiguration = true;
			createRoleToEObjectMappingCheckButton.setSelection(true);
			createScenarioRunConfigurationCheckButton.setSelection(true);
			getScenarioConfigurationWizard().setCreateObjectSystemFromCollaboration(true);
		}

		
	}


	private void handleCreateRoleToEObjectMappingChecked() {
		createRoleToEObjectMapping = createRoleToEObjectMappingCheckButton.getSelection();
	}

	private void handleCreateScenarioRunConfigurationChecked() {
		createScenarioRunConfiguration = createScenarioRunConfigurationCheckButton.getSelection();
	}	
	
	private void dialogChanged() {
		if(initializeEObjectSystemCheckButton.getSelection()){
			org.eclipse.uml2.uml.Package umlPackageSelectedForTransformation = ((AbstractScenarioConfigurationWizard)getWizard()).getUmlPackageSelectedForTransformation();
			Collaboration collaboration = null;
			for (org.eclipse.uml2.uml.Element element : umlPackageSelectedForTransformation.getOwnedElements()) {
				if (element instanceof Collaboration){
					collaboration = (Collaboration) element;
					break;
				}
			}
			if (collaboration == null){
				updateStatus("The UML package selected for transformation does not contain any collaboration that could act as template for creating an instance system.");
				return;
			}else{
				for (Property role : collaboration.getOwnedAttributes()) {
					Type roleType = role.getType();
					if(!(roleType instanceof Class)){
						updateStatus("A role is not typed via a class: " + role);
						return;
					}else{
						Class roleTypeClass = (Class) roleType;
						if (roleTypeClass.isAbstract()){
							updateStatus("Cannot create an instance from role that is types by an abstract class: " + role);
							return;
						}
					}
				}
			}
		}
		updateStatus(null);
	}
	
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}


	public boolean isPerformTGGTransformation() {
		return performTGGTransformation;
	}
	
	public boolean isInitializeEObjectSystem() {
		return initializeEObjectSystem;
	}

	public boolean isCreateRoleToEObjectMapping() {
		return createRoleToEObjectMapping;
	}
	
	public boolean isCreateScenarioRunConfiguration() {
		return createScenarioRunConfiguration;
	}

	public AbstractScenarioConfigurationWizard getScenarioConfigurationWizard() {
		return scenarioConfigurationWizard;
	}

	public void setScenarioConfigurationWizard(
			AbstractScenarioConfigurationWizard scenarioConfigurationWizard) {
		this.scenarioConfigurationWizard = scenarioConfigurationWizard;
	}
}
