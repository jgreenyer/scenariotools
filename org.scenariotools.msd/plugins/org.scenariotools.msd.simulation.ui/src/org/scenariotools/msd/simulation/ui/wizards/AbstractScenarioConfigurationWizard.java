package org.scenariotools.msd.simulation.ui.wizards;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.uml2.uml.Collaboration;

public abstract class AbstractScenarioConfigurationWizard extends Wizard implements INewWizard {

	private org.eclipse.uml2.uml.Package rootUMLPackage;

	private org.eclipse.uml2.uml.Package umlPackageSelectedForTransformation;

	private Collaboration umlCollaborationSelectedForObjectSystemCreation;

	private ResourceSet resourceSet;
	
	private ISelection selection;
	
	private boolean createObjectSystemFromCollaboration;

	
	@Override
	public boolean performFinish() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
		resourceSet = new ResourceSetImpl();
	}
	
	public org.eclipse.uml2.uml.Package getRootUMLPackage() {
		return rootUMLPackage;
	}
	public void setRootUMLPackage(org.eclipse.uml2.uml.Package rootUMLPackage) {
		this.rootUMLPackage = rootUMLPackage;
	}

	public ISelection getSelection() {
		return selection;
	}

	public ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	public org.eclipse.uml2.uml.Package getUmlPackageSelectedForTransformation() {
		return umlPackageSelectedForTransformation;
	}
	public void setUmlPackageSelectedForTransformation(
			org.eclipse.uml2.uml.Package umlPackageSelectedForTransformation) {
		this.umlPackageSelectedForTransformation = umlPackageSelectedForTransformation;
	}


	public Collaboration getUmlCollaborationSelectedForObjectSystemCreation() {
		return umlCollaborationSelectedForObjectSystemCreation;
	}


	public void setUmlCollaborationSelectedForObjectSystemCreation(
			Collaboration umlCollaborationSelectedForTransformation) {
		this.umlCollaborationSelectedForObjectSystemCreation = umlCollaborationSelectedForTransformation;
	}


	public boolean isCreateObjectSystemFromCollaboration() {
		return createObjectSystemFromCollaboration;
	}


	public void setCreateObjectSystemFromCollaboration(
			boolean createObjectSystemFromCollaboration) {
		this.createObjectSystemFromCollaboration = createObjectSystemFromCollaboration;
	}
}
