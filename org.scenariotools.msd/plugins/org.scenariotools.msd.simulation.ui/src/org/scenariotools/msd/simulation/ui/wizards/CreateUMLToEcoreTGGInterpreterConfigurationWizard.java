package org.scenariotools.msd.simulation.ui.wizards;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMapping;
import org.scenariotools.msd.roles2eobjects.RoleToEObjectMappingContainer;
import org.scenariotools.msd.roles2eobjects.Roles2eobjectsFactory;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.msd.scenariorunconfiguration.ScenariorunconfigurationFactory;
import org.scenariotools.msd.uml2ecore.UmlPackage2EPackage;
import org.scenariotools.msd.uml2ecore.configuration.InterpreterConfigurationUtil;
import org.scenariotools.msd.util.UML2EcoreMapping;
import org.scenariotools.msd.util.UtilFactory;

import de.upb.swt.qvt.tgg.interpreter.Interpreter;
import de.upb.swt.qvt.tgg.interpreter.InterpreterPackage;
import de.upb.swt.qvt.tgg.interpreterconfiguration.Configuration;
import de.upb.swt.qvt.tgg.interpreterconfiguration.DomainModel;
import de.upb.swt.qvt.tgg.interpreterconfiguration.util.InterpreterconfigurationUtil;

/**
 * This wizard creates and stores a TGG-Interpreter configuration file for a UML-to-Ecore
 * transformation of certain packages in a UML model file.
 * The wizard allows the user to specify the UML file path, the container where to create
 * the interpreter configuration file, and the file name.
 */

public class CreateUMLToEcoreTGGInterpreterConfigurationWizard extends AbstractScenarioConfigurationWizard {

	private UMLFileSelectionWizardPage umlFileSelectionWizardPage;
	private UMLPackageSelectionPage umlPackageSelectionPage;
	private TGGInterpreterConfigurationFileSpecificationWizardPage tggInterpreterConfigurationFileSpecificationWizardPage;
	private PerformTransformationUponFinishSelectionWizardPage performTransformationUponFinishSelectionWizardPage;
	private UMLCollaborationSelectionPage umlCollaborationSelectionPage;
	
	

	/**
	 * Constructor for CreateUMLToEcoreTGGInterpreterConfigurationWizard.
	 */
	public CreateUMLToEcoreTGGInterpreterConfigurationWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		umlFileSelectionWizardPage = new UMLFileSelectionWizardPage(getSelection(), getResourceSet(), this);
		addPage(umlFileSelectionWizardPage);
		umlPackageSelectionPage = new UMLPackageSelectionPage(this);
		addPage(umlPackageSelectionPage);
		tggInterpreterConfigurationFileSpecificationWizardPage = new TGGInterpreterConfigurationFileSpecificationWizardPage(this);
		addPage(tggInterpreterConfigurationFileSpecificationWizardPage);
		performTransformationUponFinishSelectionWizardPage = new PerformTransformationUponFinishSelectionWizardPage(this);
		addPage(performTransformationUponFinishSelectionWizardPage);
		umlCollaborationSelectionPage = new UMLCollaborationSelectionPage(this);
		addPage(umlCollaborationSelectionPage);
	}
	
	@Override
	public void setCreateObjectSystemFromCollaboration(
			boolean createObjectSystemFromCollaboration) {
		super.setCreateObjectSystemFromCollaboration(createObjectSystemFromCollaboration);
		umlCollaborationSelectionPage.validatePage();
		getContainer().updateButtons();
	}
	
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page == performTransformationUponFinishSelectionWizardPage && !isCreateObjectSystemFromCollaboration())
			return null;
		else
			return super.getNextPage(page);
	}
	
	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String containerName = tggInterpreterConfigurationFileSpecificationWizardPage.getContainerName();
		final String configurationFileName = tggInterpreterConfigurationFileSpecificationWizardPage.getInterpreterConfigurationFileName();
		
		final String ecoreFileName = tggInterpreterConfigurationFileSpecificationWizardPage.getECoreFileName();

		final String correspondenceModelFileName = tggInterpreterConfigurationFileSpecificationWizardPage.getCorrespondenceModelFileName();
		
		final org.eclipse.uml2.uml.Package umlPackageSelectedForTransformation = getUmlPackageSelectedForTransformation();
		final Collaboration umlCollaborationSelectedForObjectSystemCreation = getUmlCollaborationSelectedForObjectSystemCreation();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(containerName, configurationFileName, ecoreFileName, correspondenceModelFileName, umlPackageSelectedForTransformation, umlCollaborationSelectedForObjectSystemCreation, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	
	
	@Override
	public boolean canFinish() {
		if (performTransformationUponFinishSelectionWizardPage.isInitializeEObjectSystem()
				&& getUmlCollaborationSelectedForObjectSystemCreation() == null)
			return false;
		else
			return super.canFinish();
	}
	
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 * @param umlPackageSelectedForTransformation 
	 * @param correspondenceModelFileName 
	 * @param ecoreFileName 
	 * @param umlCollaborationSelectedForObjectSystemCreation 
	 */

	private void doFinish(
		String containerName,
		String configurationFileName,
		String ecoreFileName, 
		String correspondenceModelFileName, 
		Package umlPackageSelectedForTransformation, 
		Collaboration umlCollaborationSelectedForObjectSystemCreation, IProgressMonitor monitor)
		throws CoreException {
		

		Configuration tggInterpreterConfiguration = InterpreterConfigurationUtil.getTGGInterpreterConfiguration(getUmlPackageSelectedForTransformation(), 
				containerName, ecoreFileName, correspondenceModelFileName);

		
		monitor.worked(1);
		
		ResourceSet resourceSet = umlPackageSelectedForTransformation.eResource().getResourceSet();
		
		Resource configurationResource = resourceSet.createResource(URI.createPlatformResourceURI(containerName+"/"+configurationFileName, true));
		configurationResource.getContents().add(tggInterpreterConfiguration);
		try {
			configurationResource.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// optionally perform the transformation...
		if (performTransformationUponFinishSelectionWizardPage.isPerformTGGTransformation()){

			final Configuration reloadedTGGInterpreterConfiguration = InterpreterconfigurationUtil.loadInterpreterConfiguration(configurationResource.getURI());
			
			Interpreter interpreter = InterpreterPackage.eINSTANCE.getInterpreterFactory().createInterpreter();
			interpreter.setConfiguration(reloadedTGGInterpreterConfiguration);
			interpreter.initializeConfiguration();
			IStatus interpreterStatus = interpreter.performTransformation(monitor);
			
			//System.out.println(interpreterStatus);
			
			interpreter.storeResult();

			
			// optionally create instance system...
			if (interpreterStatus.isOK() && performTransformationUponFinishSelectionWizardPage.isInitializeEObjectSystem()){
				
				// First create a UML2EcoreMapping
				DomainModel correspondenceDomainModel = reloadedTGGInterpreterConfiguration.getActiveApplicationScenario().getCorrespondenceModel().get(0);
				UmlPackage2EPackage rootUmlPackage2EPackage = (UmlPackage2EPackage) correspondenceDomainModel.getRootObject().get(0);
				UML2EcoreMapping uml2EcoreMapping = UtilFactory.eINSTANCE.createUML2EcoreMapping();
				uml2EcoreMapping.init(rootUmlPackage2EPackage);
				
				
				EList<EObject> eObjectList = new BasicEList<EObject>();
				
				Package rootUMLPackage = rootUmlPackage2EPackage.getUmlPackage();
				if (umlCollaborationSelectedForObjectSystemCreation == null)
					throw new CoreException(new Status(IStatus.ERROR, "org.scenariotools.msd.runtime", "No collaboration provided."));
				if (umlCollaborationSelectedForObjectSystemCreation.getOwnedAttributes().isEmpty())
					throw new CoreException(new Status(IStatus.ERROR, "org.scenariotools.msd.runtime", "Collaboration does not contain any roles."));
				Map<String, EObject> roleNameToEObjectMap = new HashMap<String, EObject>();
				
				// this is necessary because the TGG-Interpreter will re-load the UML model, so the collaborations provided by the wizard
				// will be another loaded instance of conceptually the same collaboration.
				URI umlResourceURI = umlCollaborationSelectedForObjectSystemCreation.eResource().getURI();
				URI umlCollaborationSelectedForObjectSystemCreationURI = umlResourceURI.appendFragment(umlCollaborationSelectedForObjectSystemCreation.eResource().getURIFragment(umlCollaborationSelectedForObjectSystemCreation));
				umlCollaborationSelectedForObjectSystemCreation.eResource().getURIFragment(umlCollaborationSelectedForObjectSystemCreation);
				umlCollaborationSelectedForObjectSystemCreation = (Collaboration) rootUmlPackage2EPackage.eResource().getResourceSet().getEObject(umlCollaborationSelectedForObjectSystemCreationURI, true);
				
				for (Property role : umlCollaborationSelectedForObjectSystemCreation.getOwnedAttributes()) {
					org.eclipse.uml2.uml.Class roleTypeClass = (Class) role.getType();
					EClass correspondingRoleTypeEClass = uml2EcoreMapping.getUmlClassToEClassMap().get(roleTypeClass);
					EFactory eFactory = correspondingRoleTypeEClass.getEPackage().getEFactoryInstance();
					EObject eObject = eFactory.create(correspondingRoleTypeEClass);
					
					eObjectList.add(eObject);							
					
					if (role.getName() == null || !role.getName().isEmpty()){
						roleNameToEObjectMap.put(role.getName(), eObject);
						for (EAttribute eAttribute : correspondingRoleTypeEClass.getEAllAttributes()) {
							if ("name".equals(eAttribute.getName())
									&& eAttribute.getEType().getInstanceClass() == String.class
									) {
								eObject.eSet(eAttribute, role.getName());
							}
						}
					}
				}
				
				// if possible, create a container object for the eObjects
				EClass containerClass = null;
				Map<EObject, EReference> eObjectToContainmentReferenceMap = new HashMap<EObject, EReference>();
				for (EClassifier eClassifier : eObjectList.get(0).eClass().getEPackage().getEClassifiers()) {
					if (eClassifier instanceof EClass){
						eObjectToContainmentReferenceMap = new HashMap<EObject, EReference>();
						containerClass = (EClass) eClassifier;
						for (EReference containmentReference : containerClass.getEAllContainments()) {
							for (EObject eObject : eObjectList) {
								EClassifier containmentReferenceType = containmentReference.getEType();
								EList<EClass> eObjectsEClassEAllSuperTypes = eObject.eClass().getEAllSuperTypes();
								EClassifier eObjectsEClass = eObject.eClass();
								if (eObjectsEClass == containmentReferenceType
										|| eObjectsEClassEAllSuperTypes.contains(containmentReferenceType))
									eObjectToContainmentReferenceMap.put(eObject, containmentReference);
							}
						}
						if(eObjectToContainmentReferenceMap.keySet().containsAll(eObjectList))
							break;
						else{
							containerClass = null;
						}
					}
				}
				EObject containerObject = null;
				if (containerClass != null){
					containerObject = containerClass.getEPackage().getEFactoryInstance().create(containerClass);
					for (Entry<EObject, EReference> eObjectToContainmentReferenceMapEntry : eObjectToContainmentReferenceMap.entrySet()) {
						EReference containmentReference = eObjectToContainmentReferenceMapEntry.getValue();
						EObject eObject = eObjectToContainmentReferenceMapEntry.getKey();
						if (containmentReference.isMany()){
							((EList) containerObject.eGet(containmentReference)).add(eObject);
						}else{
							containerObject.eSet(containmentReference, eObject);
						}
					}
				}
				
				// serialize instance model
				Map options = new HashMap();
				options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
				Resource instanceModelResouce  = resourceSet.createResource(URI.createPlatformResourceURI(containerName+"/"+ecoreFileName.replace(".ecore", ".xmi"), true));
				if (containerObject != null){
					instanceModelResouce.getContents().add(containerObject);
				}else{
					instanceModelResouce.getContents().addAll(eObjectList);	
				}
				try {
					instanceModelResouce.save(options);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				RoleToEObjectMappingContainer roleToEObjectMappingContainer = null;
				
				// optionally create role-to-eobject mapping...
				if(performTransformationUponFinishSelectionWizardPage.isCreateRoleToEObjectMapping()){
					roleToEObjectMappingContainer = Roles2eobjectsFactory.eINSTANCE.createRoleToEObjectMappingContainer();
					
					//also create role-to-eobject mapping for roles of collaborations in merged packages:
//					for (Package mergedUMLPackage : uml2EcoreMapping.getMergedUMLPackages()) {
//						Collaboration collaboration2 = getFirstPackageCollaboration(mergedUMLPackage);
//						if (collaboration2 != null){
							for (Property role : umlCollaborationSelectedForObjectSystemCreation.getOwnedAttributes()) {
								if (role.getName() == null || !role.getName().isEmpty()){
									EObject eObjectMappedToRole = roleNameToEObjectMap.get(role.getName());
									RoleToEObjectMapping roleToEObjectMapping = Roles2eobjectsFactory.eINSTANCE.createRoleToEObjectMapping();
									roleToEObjectMapping.setEObject(eObjectMappedToRole);
									roleToEObjectMapping.setRole(role);
									roleToEObjectMappingContainer.getRoleToEObjects().add(roleToEObjectMapping);
								}
							}
//						}
//					}
					
					Resource rolesToEObjectsMappingResource = resourceSet.createResource(URI.createPlatformResourceURI(containerName+"/"+ecoreFileName.replace(".ecore", ".roles2eobjects"), true));
					rolesToEObjectsMappingResource.getContents().add(roleToEObjectMappingContainer);
					try {
						rolesToEObjectsMappingResource.save(options);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
				// optionally create scenario run configuration...
				if(performTransformationUponFinishSelectionWizardPage.isCreateScenarioRunConfiguration()){
					ScenarioRunConfiguration scenarioRunConfiguration = ScenariorunconfigurationFactory.eINSTANCE.createScenarioRunConfiguration();
					scenarioRunConfiguration.setExecutionSemantics(ExecutionSemantics.PLAY_OUT);
					if (roleToEObjectMappingContainer != null)
						scenarioRunConfiguration.setRoleToEObjectMappingContainer(roleToEObjectMappingContainer);
					scenarioRunConfiguration.setUml2EcoreMapping(rootUmlPackage2EPackage);
					if (containerObject != null){
						scenarioRunConfiguration.getSimulationRootObjects().add(containerObject);
					}else{
						scenarioRunConfiguration.getSimulationRootObjects().addAll(eObjectList);
					}
					Resource scenarioRunConfigurationResource = resourceSet.createResource(URI.createPlatformResourceURI(containerName+"/"+ecoreFileName.replace(".ecore", ".scenariorunconfiguration"), true));
					scenarioRunConfigurationResource.getContents().add(scenarioRunConfiguration);
					try {
						scenarioRunConfigurationResource.save(options);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
			}
			
		}
		
		monitor.worked(1);
	}
	
	private Collaboration getFirstPackageCollaboration(Package umlPackage){
		Collaboration foundCollaboration = null;
		for (Element element : umlPackage.getOwnedElements()) {
			if (element instanceof Collaboration){
				Collaboration collaboration = (Collaboration) element;
				foundCollaboration = collaboration;
			}
		}
		// it can be that a package does not define any collaboration or that the 
		// collaboration does not define any roles, but only contains
		// interactions that refer to roles of a collaboration in a merged package.
		if (foundCollaboration == null || foundCollaboration.getOwnedAttributes().isEmpty()){
			// here we iterate recursively over all merged packages and pick the first that
			// has a collaboration with roles.
			// TODO: find the collaboration instead that contains the roles by
			// referenced by the lifelines of contained interactions.
			for (PackageMerge packageMerge : umlPackage.getPackageMerges()) {
				Package mergedPackage = packageMerge.getMergedPackage();
				Collaboration mergedPackageCollaboration = getFirstPackageCollaboration(mergedPackage);
				if (mergedPackageCollaboration != null)
					return mergedPackageCollaboration;
			}
		}else
			return foundCollaboration;

		return null;
	}
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream() {
		String contents =
			"This is the initial file contents for *.interpreterconfiguration file that should be word-sorted in the Preview page of the multi-page editor";
		return new ByteArrayInputStream(contents.getBytes());
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "org.scenariotools.msd.simulation.ui", IStatus.OK, message, null);
		throw new CoreException(status);
	}
	
}