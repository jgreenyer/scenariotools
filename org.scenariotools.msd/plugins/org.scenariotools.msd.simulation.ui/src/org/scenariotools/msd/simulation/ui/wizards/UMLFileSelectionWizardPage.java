package org.scenariotools.msd.simulation.ui.wizards;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.uml2.uml.Package;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (interpreterconfiguration).
 */

public class UMLFileSelectionWizardPage extends WizardPage {

	private Text umlFileURIText;	

	private ISelection selection;
	
	private ResourceSet resourceSet;

	private AbstractScenarioConfigurationWizard scenarioConfigurationWizard;

	/**
	 * Constructor for SampleNewWizardPage.
	 * @param resourceSet 
	 * @param scenarioConfigurationWizard 
	 * 
	 * @param pageName
	 */
	public UMLFileSelectionWizardPage(ISelection selection, ResourceSet resourceSet, AbstractScenarioConfigurationWizard scenarioConfigurationWizard) {
		super("wizardPage");
		this.scenarioConfigurationWizard = scenarioConfigurationWizard;
		setTitle("Select UML model file");
		setDescription("Select a .uml model file from which a new UML-to-ECore transformation configuration will be created.");
		this.selection = selection;
		this.resourceSet = resourceSet;
	}
	
	public AbstractScenarioConfigurationWizard getScenarioConfigurationWizard() {
		return scenarioConfigurationWizard;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		
		
		Label label1 = new Label(container, SWT.NONE);
		label1.setText("UML model:");

		// resource name entry field
		umlFileURIText = new Text(container, SWT.BORDER | SWT.SINGLE);
		umlFileURIText.setText("");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		umlFileURIText.setLayoutData(gd);
		umlFileURIText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
				
			}
		});
		
		Button browseUMLFileURIButton = new Button(container, SWT.PUSH);
		browseUMLFileURIButton.setText("Browse...");

		browseUMLFileURIButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseUMLFile();
			}
		});

		
		initialize();
		dialogChanged();
		setControl(container);
	}


	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IFile) {
				IFile file = (IFile) obj;
				if (file.getFileExtension().equals("uml")){
					umlFileURIText.setText(file.getFullPath().toString());
				}
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {

		IResource umlFile = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getUMLFileURIText()));

		if (getUMLFileURIText().length() == 0) {
			updateStatus("UML file must be specified");
			return;
		}
		if (umlFile == null
				|| umlFile.getType() != IResource.FILE
				|| !umlFile.getFileExtension().equals("uml")) {
			updateStatus("The selected resource is not a UML file");
			return;
		}
		Resource umlResource = resourceSet.getResource(URI.createPlatformResourceURI(umlFile.getFullPath().toString(), true), true);
		try {
			umlResource.load(null);
		} catch (IOException e) {
			updateStatus("Cannot load the UML model: " + e);
			return;
		}
		if (umlResource.getContents().isEmpty()){
			updateStatus("The UML model file does not contain any element");
			return;
		}
		if (!(umlResource.getContents().get(0) instanceof org.eclipse.uml2.uml.Package)){
			updateStatus("The UML model must contain an instance of org.eclipse.uml2.uml.Package or org.eclipse.uml2.uml.Model as its root");
			return;
		}else{
			getScenarioConfigurationWizard().setRootUMLPackage((Package) umlResource.getContents().get(0));
		}

		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	public String getUMLFileURIText() {
		if (umlFileURIText == null) 
			return "";
		else
			return umlFileURIText.getText();
	}

	private void handleBrowseUMLFile()
    {
       ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
             getShell(), new WorkbenchLabelProvider(),
             new BaseWorkbenchContentProvider());
       dialog.setTitle("UML model");
       dialog.setMessage("Select a UML model from the workspace:");
       dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
       ViewerFilter extensionFilter = new ViewerFilter()
       {
          @Override
          public boolean select(Viewer viewer, Object parentElement,
                Object element)
          {
             return !(element instanceof IFile)
                   || !((IFile) element).getFileExtension().isEmpty();
          }
       };
       dialog.addFilter(extensionFilter);

       if (dialog.open() == ContainerSelectionDialog.OK)
       {
          Object[] result = dialog.getResult();
          if (result.length == 1)
          {
        	  IFile umlFile = (IFile) result[0];
        	  umlFileURIText.setText(umlFile.getFullPath().toString());
          }
       }
    }

	public String getUMLFileURI(){
		return umlFileURIText.getText();
	} 
}