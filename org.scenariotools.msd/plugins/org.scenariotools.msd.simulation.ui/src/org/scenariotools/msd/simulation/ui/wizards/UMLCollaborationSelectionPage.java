package org.scenariotools.msd.simulation.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.edit.providers.UMLItemProviderAdapterFactory;
import org.eclipse.uml2.uml.edit.providers.UMLReflectiveItemProviderAdapterFactory;
import org.eclipse.uml2.uml.edit.providers.UMLResourceItemProviderAdapterFactory;

public class UMLCollaborationSelectionPage extends WizardPage {

	private AbstractScenarioConfigurationWizard scenarioConfigurationWizard;

	public UMLCollaborationSelectionPage(AbstractScenarioConfigurationWizard scenarioConfigurationWizard) {
		super("Select collaboration to create object system from");
		setTitle("Select Collaboration");
		setDescription("Select collaboration to create object system from.");
		this.scenarioConfigurationWizard = scenarioConfigurationWizard;
	}
	
	public AbstractScenarioConfigurationWizard getScenarioConfigurationWizard() {
		return scenarioConfigurationWizard;
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if(visible){
			modelViewer.setInput(getScenarioConfigurationWizard().getRootUMLPackage());
		}
	}

	/**
	 * @generated
	 */
	protected EObject selectedModelElement;

	/**
	 * @generated
	 */
	private TreeViewer modelViewer;

	/**
	 * @generated
	 */
	public EObject getModelElement() {
		return selectedModelElement;
	}

	/**
	 * @generated
	 */
	public void setModelElement(EObject modelElement) {
		selectedModelElement = modelElement;
		if (modelViewer != null) {
			if (selectedModelElement != null) {
				modelViewer.setInput(selectedModelElement.eResource());
				modelViewer.setSelection(new StructuredSelection(
						selectedModelElement));
			} else {
				modelViewer.setInput(null);
			}
			setPageComplete(validatePage());
		}
	}

	/**
	 * @generated
	 */
	public void createControl(Composite parent) {
		initializeDialogUnits(parent);

		Composite plate = new Composite(parent, SWT.NONE);
		plate.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		plate.setLayout(layout);
		setControl(plate);

		Label label = new Label(plate, SWT.NONE);
		label.setText(getSelectionTitle());
		label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));

		modelViewer = new TreeViewer(plate, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.BORDER);
		GridData layoutData = new GridData(GridData.FILL_BOTH);
		layoutData.heightHint = 300;
		layoutData.widthHint = 300;
		modelViewer.getTree().setLayoutData(layoutData);
		
		List<AdapterFactory> factories = new ArrayList<AdapterFactory>();
		factories.add(new UMLResourceItemProviderAdapterFactory());
		factories.add(new UMLItemProviderAdapterFactory());
		factories.add(new EcoreItemProviderAdapterFactory());
		factories.add(new UMLReflectiveItemProviderAdapterFactory());

		AdapterFactory adapterFactory = new ComposedAdapterFactory(factories);
		
		modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		modelViewer.setInput(getScenarioConfigurationWizard().getRootUMLPackage());		
//		if (selectedModelElement != null) {
//			modelViewer.setInput(selectedModelElement.eResource());
//			modelViewer.setSelection(new StructuredSelection(
//					selectedModelElement));
//		}
		modelViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						UMLCollaborationSelectionPage.this
								.updateSelection((IStructuredSelection) event
										.getSelection());
					}
				});

		setPageComplete(validatePage());
	}

	/**
	 * Override to provide custom model element description.
	 * 
	 * @generated
	 */
	protected String getSelectionTitle() {
		return "Select collaboration to create object system from";
	}

	/**
	 * @generated
	 */
	protected void updateSelection(IStructuredSelection selection) {
		selectedModelElement = null;
		if (selection.size() == 1) {
			Object selectedElement = selection.getFirstElement();
			if (selectedElement instanceof EObject) {
				selectedModelElement = (EObject) selectedElement;
			}
		}
		setPageComplete(validatePage());
	}

	/**
	 * Override to provide specific validation of the selected model element.
	 * 
	 * @generated
	 */
	public boolean validatePage() {
		if (!getScenarioConfigurationWizard().isCreateObjectSystemFromCollaboration()){
			updateStatus(null);
			return true;
		}
		if (!(selectedModelElement instanceof Collaboration)){
			updateStatus("The selection must be an instance of org.eclipse.uml2.uml.Collaboration.");
			return false;
		}
		if (((Collaboration)selectedModelElement).getOwnedAttributes().isEmpty()){
			updateStatus("The Collaboration must contain roles (properties) upon which an object system can be created.");
			return false;
		} 
		getScenarioConfigurationWizard().setUmlCollaborationSelectedForObjectSystemCreation((Collaboration) selectedModelElement);
		updateStatus(null);
		return true;
	}
	
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

}
