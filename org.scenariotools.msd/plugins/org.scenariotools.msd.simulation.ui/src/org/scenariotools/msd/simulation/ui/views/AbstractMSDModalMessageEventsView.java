package org.scenariotools.msd.simulation.ui.views;


import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.util.EventsUtil;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.simulation.ui.icons.MSDModalMessageEventsIconProvider;
import org.scenariotools.msd.util.RuntimeUtil;

public abstract class AbstractMSDModalMessageEventsView extends ViewPart {

	protected TreeViewer viewer;
	protected DrillDownAdapter drillDownAdapter;
	private Action doubleClickAction;

	private boolean disabled;
	
	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	public class ViewContentProvider implements IStructuredContentProvider,
			ITreeContentProvider {

		private EList<MSDModalMessageEvent> topLevelCondensatedEvents;

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			topLevelCondensatedEvents = null;
		}

		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			return getChildren(parent);
		}

		public Object getParent(Object child) {
			if (child instanceof MSDModalMessageEvent && ((MSDModalMessageEvent) child).getParentSymbolicMSDModalMessageEvent() != null) {
				return ((MSDModalMessageEvent) child).getParentSymbolicMSDModalMessageEvent();
			}
			return null;
		}

		public Object[] getChildren(Object parent) {
			if (parent instanceof Collection<?>) {
				@SuppressWarnings("unchecked")
				Collection<MSDModalMessageEvent> topLevelCondensatedEvents = getTopLevelCondensatedEvents((Collection<Object>) parent);
				return topLevelCondensatedEvents
						.toArray(new MSDModalMessageEvent[topLevelCondensatedEvents
								.size()]);
			}
			if (parent instanceof MSDModalMessageEvent) {
				EList<MSDModalMessageEvent> childCondensatedMessageEvents = ((MSDModalMessageEvent) parent)
						.getConcreteMSDModalMessageEvent();
				return childCondensatedMessageEvents
						.toArray(new MSDModalMessageEvent[childCondensatedMessageEvents
								.size()]);
			}
			return new Object[0];
		}

		public boolean hasChildren(Object parent) {
			if (parent instanceof Collection<?>) {
				return !((Collection<?>) parent).isEmpty();
			}
			if (parent instanceof MSDModalMessageEvent) {
				return !((MSDModalMessageEvent) parent)
						.getConcreteMSDModalMessageEvent().isEmpty();
			}
			return false;
		}

		private Collection<MSDModalMessageEvent> getTopLevelCondensatedEvents(Collection<Object> objects) {
			if (topLevelCondensatedEvents == null) {
				topLevelCondensatedEvents = new BasicEList<MSDModalMessageEvent>();
				for (Object object : objects) {
					if (object instanceof MSDModalMessageEvent){
						MSDModalMessageEvent condensatedMessageEvent = (MSDModalMessageEvent) object;
						if (condensatedMessageEvent
								.getParentSymbolicMSDModalMessageEvent() == null)
							topLevelCondensatedEvents.add(condensatedMessageEvent);
					}
				}
			}
			return topLevelCondensatedEvents;
		}
	}

	public class FirstColumnLabelProvider extends ColumnLabelProvider{

		@Override
		public Image getImage(Object element) {
			if (element instanceof MSDModalMessageEvent) {
			return MSDModalMessageEventsIconProvider
					.getImageForCondensatedMessageEvent((MSDModalMessageEvent) element, isDisabled());
			}else
				return super.getImage(element);
		}
		
		FontRegistry registry = new FontRegistry();

		@Override
		public Font getFont(Object element) {
			if (element instanceof MSDModalMessageEvent){
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) element; 
				if (msdModalMessageEvent.getRequirementsModality().isMandatory()
						&& !msdModalMessageEvent.getEnabledInActiveProcess().isEmpty()) {
					return registry.getBold(Display.getCurrent()
							.getSystemFont().getFontData()[0].getName());
				}
			}
			return super.getFont(element);
		}
		
		@Override
		public Color getForeground(Object element) {
			if (isDisabled())
				return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
			if (element instanceof MSDModalMessageEvent) {
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) element;
				MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();
				if (requirementsModality.isHot()
						&& !requirementsModality.isSafetyViolating()) {
					return Display.getCurrent().getSystemColor(SWT.COLOR_RED);
				}
				if (requirementsModality.isCold()
						&& !requirementsModality.isHot()
						&& !requirementsModality.isSafetyViolating()) {
					return Display.getCurrent().getSystemColor(SWT.COLOR_BLUE);
				}
				if (requirementsModality.isSafetyViolating()) {
					return new Color(null, 255, 200, 200);
					// Display.getCurrent().getSystemColor(
					// SWT.COLOR_GRAY);
				}
			}
			return super.getForeground(element);
		}
		
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
			if (element instanceof MSDModalMessageEvent)
				return (RuntimeUtil.Helper.getMessageEventString(((MSDModalMessageEvent) element).getRepresentedMessageEvent()));
			return "<unknown element>"; //super.getText(element);
		}
		

//		@Override
//		public Color getForeground(Object element, int columnIndex) {
//			if (isDisabled())
//				return Display.getCurrent().getSystemColor(
//						SWT.COLOR_GRAY);
//			switch (columnIndex) {
//			case 0:
//				if (element instanceof CondensatedMessageEvent) {
//					CondensatedMessageEvent condensatedMessageEvent = (CondensatedMessageEvent) element;
//					if (condensatedMessageEvent.isRequirementHot()
//							&& !condensatedMessageEvent
//									.isRequirementSafetyViolating()) {
//						return Display.getCurrent().getSystemColor(
//								SWT.COLOR_RED);
//					}
//					if (condensatedMessageEvent.isRequirementCold()
//							&& !condensatedMessageEvent.isRequirementHot()
//							&& !condensatedMessageEvent
//									.isRequirementSafetyViolating()) {
//						return Display.getCurrent().getSystemColor(
//								SWT.COLOR_BLUE);
//					}
//					if (condensatedMessageEvent.isRequirementSafetyViolating()) {
//						return new Color(null, 255, 200, 200);
////								Display.getCurrent().getSystemColor(
////								SWT.COLOR_GRAY);
//					}
//				}
//
//			}
//			return null;
//		}


		@Override
		public void update(ViewerCell cell) {
			super.update(cell);
		}
		
		@Override
		public String getToolTipText(Object element) {
			if (element instanceof MSDModalMessageEvent){
				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) element;
				
				MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent.getAssumptionsModality();
				MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();

				StringBuilder tooltipText = new StringBuilder();
				tooltipText.append("assumptionHot: " + String.valueOf(assumptionsModality.isHot()).toUpperCase() + "\n"); 
				tooltipText.append("assumptionCold: " + String.valueOf(assumptionsModality.isCold()).toUpperCase() + "\n");
				tooltipText.append("assumptionMonitored: " + String.valueOf(assumptionsModality.isMonitored()).toUpperCase() + "\n");
				tooltipText.append("assumptionExecuted: " + String.valueOf(assumptionsModality.isMandatory()).toUpperCase() + "\n");
				tooltipText.append("assumptionSafetyViolating: " + String.valueOf(assumptionsModality.isSafetyViolating()).toUpperCase() + "\n");
				tooltipText.append("assumptionColdViolating: " + String.valueOf(assumptionsModality.isColdViolating()).toUpperCase() + "\n");
				tooltipText.append("assumptionInitializing: " + String.valueOf(assumptionsModality.isInitializing()).toUpperCase() + "\n");
				tooltipText.append("requirementHot: " + String.valueOf(requirementsModality.isHot()).toUpperCase() + "\n"); 
				tooltipText.append("requirementCold: " + String.valueOf(requirementsModality.isCold()).toUpperCase() + "\n");
				tooltipText.append("requirementMonitored: " + String.valueOf(requirementsModality.isMonitored()).toUpperCase() + "\n");
				tooltipText.append("requirementExecuted: " + String.valueOf(requirementsModality.isMandatory()).toUpperCase() + "\n");
				tooltipText.append("requirementSafetyViolating: " + String.valueOf(requirementsModality.isSafetyViolating()).toUpperCase() + "\n");
				tooltipText.append("requirementColdViolating: " + String.valueOf(requirementsModality.isColdViolating()).toUpperCase() + "\n");
				tooltipText.append("requirementInitializing: " + String.valueOf(requirementsModality.isInitializing()).toUpperCase());
				return tooltipText.toString();	
			}
			return super.getToolTipText(element);
		}
		
		@Override
		public int getToolTipStyle(Object object) {
			return SWT.WRAP;
		}
		
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipShift(java
		 * .lang.Object)
		 */
		public Point getToolTipShift(Object object) {
			return new Point(5, 5);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipDisplayDelayTime
		 * (java.lang.Object)
		 */
		public int getToolTipDisplayDelayTime(Object object) {
			return 500;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipTimeDisplayed
		 * (java.lang.Object)
		 */
		public int getToolTipTimeDisplayed(Object object) {
			return 0;
		}

	}

	public class NameSorter extends ViewerSorter {
	}

	
	protected class MSDColumnLabelProvider extends ColumnLabelProvider {
		protected int column;
		public MSDColumnLabelProvider(int column) {
			super();
			this.column = column;
		}
		
		@Override
		public String getText(Object element) {
			if (element instanceof MSDModalMessageEvent) {
				EObject eObject;
				EList<ActiveProcess> activeProcesses;
				switch(column) {
				case 2:
					eObject = ((MSDModalMessageEvent) element)
							.getRepresentedMessageEvent().getSendingObject();
					return EventsUtil.getName(eObject) + ":"
							+ eObject.eClass().getName();
				case 3:
					eObject = ((MSDModalMessageEvent) element)
							.getRepresentedMessageEvent().getReceivingObject();
					return EventsUtil.getName(eObject) + ":"
							+ eObject.eClass().getName();
				case 4:
					MessageEvent messageEvent = ((MSDModalMessageEvent) element)
							.getRepresentedMessageEvent();
					EOperation operation = messageEvent.getOperation();
					if (!operation.getEParameters().isEmpty()) {
						EParameter parameter = operation.getEParameters()
								.get(0);
						return parameter.getName()
								+ "="
								+ EventsUtil
										.getParameterValueString(messageEvent);
					}
				case 5:
					activeProcesses = ((MSDModalMessageEvent) element)
							.getEnabledInActiveProcess();
					return getActiveProcessesListString(activeProcesses);
				case 6:
					activeProcesses = ((MSDModalMessageEvent) element)
							.getColdViolatingInActiveProcess();
					return getActiveProcessesListString(activeProcesses);
				case 7:
					activeProcesses = ((MSDModalMessageEvent) element)
							.getSafetyViolatingInActiveProcess();
					return getActiveProcessesListString(activeProcesses);
				}
				
			}
			return "<unknown element>";
		}
	}
	
	protected MSDColumnLabelProvider getMSDColumnLabelProvider(int column) {
		return new MSDColumnLabelProvider(column);
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {

		//Tree tree = new Tree(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		viewer = new TreeViewer(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
		viewer.setUseHashlookup(true);
		viewer.setAutoExpandLevel(2);
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);

		TreeViewerColumn column1 = new TreeViewerColumn(viewer, SWT.LEFT);
		column1.setLabelProvider(getFirstColumnLabelProvider());
		column1.getColumn().setWidth(400);
		column1.getColumn().setMoveable(false);
		column1.getColumn().setText("Message");

		TreeViewerColumn column2 = new TreeViewerColumn(viewer, SWT.LEFT);
		column2.setLabelProvider(getMSDColumnLabelProvider(2));
		column2.getColumn().setWidth(200);
		column2.getColumn().setMoveable(false);
		column2.getColumn().setText("Sending Object");

		TreeViewerColumn column3 = new TreeViewerColumn(viewer, SWT.LEFT);
		column3.setLabelProvider(getMSDColumnLabelProvider(3));
		column3.getColumn().setWidth(200);
		column3.getColumn().setMoveable(false);
		column3.getColumn().setText("Receiving Object");

		TreeViewerColumn column4 = new TreeViewerColumn(viewer, SWT.LEFT);
		column4.setLabelProvider(getMSDColumnLabelProvider(4));
		column4.getColumn().setWidth(150);
		column4.getColumn().setMoveable(false);
		column4.getColumn().setText("Parameter");
		
		
		TreeViewerColumn column5 = new TreeViewerColumn(viewer, SWT.LEFT);
		column5.setLabelProvider(getMSDColumnLabelProvider(5));
		column5.getColumn().setWidth(150);
		column5.getColumn().setMoveable(false);
		column5.getColumn().setText("enabled in Active MSD");

		TreeViewerColumn column6 = new TreeViewerColumn(viewer, SWT.LEFT);
		column6.setLabelProvider(getMSDColumnLabelProvider(6));
		column6.getColumn().setWidth(150);
		column6.getColumn().setMoveable(false);
		column6.getColumn().setText("cold-violating in Active MSD");

		TreeViewerColumn column7 = new TreeViewerColumn(viewer, SWT.LEFT);
		column7.setLabelProvider(getMSDColumnLabelProvider(7));
		column7.getColumn().setWidth(150);
		column7.getColumn().setMoveable(false);
		column7.getColumn().setText("safety-violating in Active MSD");

		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(getViewContentProvider());
		viewer.setSorter(new NameSorter());
		
		getSite().setSelectionProvider(viewer);
		
		ColumnViewerToolTipSupport.enableFor(viewer);
		
		// Create the help context id for the viewer's control
		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(viewer.getControl(),
						"org.scenariotools.msd.simulation.msdmodalmessageeventsview.viewer");
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	protected FirstColumnLabelProvider getFirstColumnLabelProvider() {
		return new FirstColumnLabelProvider();
	}

	protected ViewContentProvider getViewContentProvider() {
		return new ViewContentProvider();
	}
	
	protected String getActiveProcessesListString(EList<ActiveProcess> activeProcesses){

			String returnString = "";
			for (ActiveProcess activeProcess : activeProcesses) {
				if(activeProcess instanceof ActiveMSD)
					returnString += ((ActiveMSD)activeProcess).getInteraction().getName() + ", ";
			}
			if (returnString.length() > 1) returnString = returnString.substring(0, returnString.length()-2);
			return returnString;
	}
	
	protected TreeViewer getViewer(){
		return viewer;
	}
	
	protected void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	protected void fillLocalPullDown(IMenuManager manager) {
		// manager.add(resume);
		// manager.add(new Separator());
		// manager.add(pause);
	}

	protected void fillContextMenu(IMenuManager manager) {
		// manager.add(resume);
		// manager.add(pause);
		// manager.add(new Separator());
		//drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	protected void fillLocalToolBar(IToolBarManager manager) {
		// manager.add(resume);
		// manager.add(pause);
		// manager.add(new Separator());
		//drillDownAdapter.addNavigationActions(manager);
	}

	protected void makeActions() {
		
		// resume.setText("resume");
		// resume.setToolTipText("Resume agent execution");
		// resume.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
		// getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		
		// resume = new Action() {
		// public void run() {
		// resumePressed();
		// }
		// };
		// resume.setText("resume");
		// resume.setToolTipText("Resume agent execution");
		// resume.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
		// getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		//
		//
		// pause = new Action() {
		// public void run() {
		// pausePressed();
		// }
		// };
		// pause.setText("pause");
		// pause.setToolTipText("Pause agent execution");
		// pause.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
		// getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		//
		//
		// nextStep = new Action() {
		// public void run() {
		// resumePressed();
		// }
		// };
		// nextStep.setText("next step");
		// nextStep.setToolTipText("Execute next agent step");
		// nextStep.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
		// getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				objectDoubleClicked(obj);
			}
		};
	}
	
	protected void objectDoubleClicked(Object object){
		showMessage("Double-click detected on " + object.toString());
	}
	
	protected void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				"Test Condensated Events View", message);
	}


	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		getViewer().refresh();
	}
	
	public void setInput(Collection<MSDModalMessageEvent> msdModalMessageEvent){
		getViewer().setInput(msdModalMessageEvent);
	}
	
	@SuppressWarnings("unchecked")
	protected EList<MSDModalMessageEvent> getInput(){
		if (getViewer().getInput() instanceof EList<?>)
			return (EList<MSDModalMessageEvent>) getViewer().getInput();
		else return null;
	}
	
}
