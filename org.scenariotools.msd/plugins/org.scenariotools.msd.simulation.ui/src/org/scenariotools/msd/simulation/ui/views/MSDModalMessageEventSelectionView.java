package org.scenariotools.msd.simulation.ui.views;

import java.util.Collection;

import org.eclipse.debug.core.Launch;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ExecutionSemantics;
import org.scenariotools.msd.runtime.MSDModalMessageEvent;
import org.scenariotools.msd.runtime.MSDModality;
import org.scenariotools.msd.simulation.SimulationManager;
import org.scenariotools.msd.simulation.UserInteractingSimulationAgent;
import org.scenariotools.msd.simulation.debug.AbstractScenarioThread;
import org.scenariotools.msd.simulation.debug.ActiveMSDThread;
import org.scenariotools.msd.simulation.debug.ActiveMSSThread;
import org.scenariotools.msd.simulation.debug.ObjectThread;
import org.scenariotools.msd.simulation.debug.ScenarioDebugTarget;
import org.scenariotools.msd.simulation.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.msd.simulation.listener.IMSDModalMessageEventListChangeListener;
import org.scenariotools.msd.simulation.ui.icons.ImageHelper;
import org.scenariotools.msd.util.RuntimeUtil;
import org.scenariotools.runtime.ModalMessageEvent;

public class MSDModalMessageEventSelectionView extends
		AbstractMSDModalMessageEventsView {

	public static String MODAL_MESSAGE_EVENT_SELECTION_VIEW = "org.scenariotools.msd.simulation.ui.views.CondensatedMessageEventSelectionView";

	private SimulationManager currentSimulationManager = null;
	private UserInteractingSimulationAgent currentUserInteractingSimulationAgent = null;

	private Object filterObject; // stores an object (Launch,
									// ScenarioDebugTarget, ActiveMSDThread,
									// ObjectThread) that was selected last

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection)
						.getFirstElement();
				if (firstElement instanceof ScenarioDebugTarget) {
					selectionChanged((ScenarioDebugTarget) firstElement);
				}
			}
		}

		private void selectionChanged(ScenarioDebugTarget scenarioDebugTarget) {
			setFilterObject(scenarioDebugTarget);
			setCurrentSimulationManager(scenarioDebugTarget
					.getSimulationManager());
			setInput((Collection<MSDModalMessageEvent>) (Collection<?>) getCurrentUserInteractingSimulationAgent()
					.getSimulationManager().getCurrentMSDRuntimeState()
					.getMessageEventToModalMessageEventMap().values());
		}

	};

	/**
	 * This listener is registered at a current simulation agent to be notified
	 * about a changing MSD modal message event list.
	 */
	private IMSDModalMessageEventListChangeListener msdModalMessageEventListChangeListener = new IMSDModalMessageEventListChangeListener() {
		@Override
		public void msdModalMessageEventsChanged(
				Collection<MSDModalMessageEvent> msdModalMessageEvents) {
			setInput(msdModalMessageEvents);
			MSDModalMessageEventSelectionView.this.getViewer().refresh();
		}
	};

	/**
	 * this listener is registered at a current simulation manager to be
	 * notified about a changing current simulation agent.
	 */
	private IActiveSimulationAgentChangeListener activeSimulationAgentChangeListener = new IActiveSimulationAgentChangeListener() {

		@Override
		public void activeSimulationAgentChanged(
				UserInteractingSimulationAgent userInteractingSimulationAgent) {
			setCurrentUserInteractingSimulationAgent(userInteractingSimulationAgent);
		}
	};

	/**
	 * This method is called 1. when the simulation manager notifies the view
	 * (through the {@link IActiveSimulationAgentChangeListener}) about a change
	 * of the current simulation agent. 2. if the {@link
	 * setCurrentSimulationManager(SimulationManager)} is called.
	 * 
	 * @param newUserInteractingSimulationAgent
	 */
	protected void setCurrentUserInteractingSimulationAgent(
			UserInteractingSimulationAgent newUserInteractingSimulationAgent) {
		UserInteractingSimulationAgent oldUserInteractingSimulationAgent = currentUserInteractingSimulationAgent;
		if (oldUserInteractingSimulationAgent != null)
			oldUserInteractingSimulationAgent
					.getRegisteredMSDModalEventListChangeListener().remove(
							msdModalMessageEventListChangeListener);

		newUserInteractingSimulationAgent
				.getRegisteredMSDModalEventListChangeListener().add(
						msdModalMessageEventListChangeListener);
		currentUserInteractingSimulationAgent = newUserInteractingSimulationAgent;
		setInput((Collection<MSDModalMessageEvent>) (Collection<?>) currentUserInteractingSimulationAgent
				.getSimulationManager().getCurrentMSDRuntimeState()
				.getMessageEventToModalMessageEventMap().values());
	}

	protected UserInteractingSimulationAgent getCurrentUserInteractingSimulationAgent() {
		return currentUserInteractingSimulationAgent;
	}

	/**
	 * This method is called when the selection in the debug view has changed.
	 * Calling this method results in re-registering the
	 * activeSimulationAgentChangeListener to that simulation manager and
	 * setting the current user-interacting simulation agent (calling {@link
	 * setCurrentUserInteractingSimulationAgent(UserInteractingSimulationAgent
	 * newUserInteractingSimulationAgent)})
	 * 
	 * @param newSimulationManager
	 */
	protected void setCurrentSimulationManager(
			SimulationManager newSimulationManager) {
		SimulationManager oldSimulationManager = this.currentSimulationManager;

		if (oldSimulationManager != null)
			oldSimulationManager
					.getRegisteredActiveSimulationAgentChangeListener().remove(
							activeSimulationAgentChangeListener);

		newSimulationManager.getRegisteredActiveSimulationAgentChangeListener()
				.add(activeSimulationAgentChangeListener);

		currentSimulationManager = newSimulationManager;

		if (newSimulationManager.getActiveSimulationAgent() instanceof UserInteractingSimulationAgent)
			setCurrentUserInteractingSimulationAgent((UserInteractingSimulationAgent) newSimulationManager
					.getActiveSimulationAgent());

		togglePlayOutExecutionModeAction.setChecked(newSimulationManager
				.getCurrentExecutionSemantics() == ExecutionSemantics.PLAY_OUT);
	}

	protected SimulationManager getCurrentSimulationManager() {
		return currentSimulationManager;
	}

	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		hookSelectionListener();
	}

	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}

	protected Object getFilterObject() {
		return filterObject;
	}

	protected void setFilterObject(Object filterObject) {
		this.filterObject = filterObject;
	}

	@Override
	public void setInput(Collection<MSDModalMessageEvent> msdModalMessageEvents) {
		if (getFilterObject() != null) {

			EList<MSDModalMessageEvent> filteredMSDModalMessageEvents = new BasicEList<MSDModalMessageEvent>();

			if (getFilterObject() instanceof Launch) {
				Launch launch = (Launch) getFilterObject();
				if (launch.getDebugTarget() instanceof ScenarioDebugTarget
						&& !launch.isTerminated()) {
					filteredMSDModalMessageEvents.addAll(msdModalMessageEvents);
				}
			} else if (getFilterObject() instanceof ScenarioDebugTarget) {
				if (!((ScenarioDebugTarget) getFilterObject()).isTerminated())
					filteredMSDModalMessageEvents
							.addAll(getRequirementExecutedOrEnvironmentMSDModalMessageEventsFromList(msdModalMessageEvents));
			} else if (getFilterObject() instanceof AbstractScenarioThread
					&& !((AbstractScenarioThread) getFilterObject()).getDebugTarget()
							.isTerminated()) {
				ActiveProcess activeMSD = ((AbstractScenarioThread) getFilterObject())
						.getActiveProcess();
				if(activeMSD !=null){
					for (MSDModalMessageEvent msdMessageEvent : msdModalMessageEvents) {
						for (MessageEvent messageEvent : activeMSD
								.getEnabledEvents().values()) {
							if (RuntimeUtil.Helper.isMessageUnifiable(messageEvent,
									msdMessageEvent.getRepresentedMessageEvent())) {
								filteredMSDModalMessageEvents.add(msdMessageEvent);
							}
						}
						for (MessageEvent messageEvent : activeMSD
								.getViolatingEvents().values()) {
							if (RuntimeUtil.Helper.isMessageUnifiable(messageEvent,
									msdMessageEvent.getRepresentedMessageEvent())) {
								filteredMSDModalMessageEvents.add(msdMessageEvent);
							}
						}
					}
				}
			} else if (getFilterObject() instanceof ObjectThread
					&& !((ObjectThread) getFilterObject()).getDebugTarget()
							.isTerminated()) {
				for (MSDModalMessageEvent msdModalMessageEvent : msdModalMessageEvents) {
					if (msdModalMessageEvent.getRepresentedMessageEvent()
							.getSendingObject() == ((ObjectThread) getFilterObject())
							.getSimulationObject()) {
						filteredMSDModalMessageEvents.add(msdModalMessageEvent);
					}
				}
			}
			super.setInput(filteredMSDModalMessageEvents);
			return;
		}
		// super.setInput(condensatedMessageEvents);
	}

	protected EList<MSDModalMessageEvent> getRequirementExecutedOrEnvironmentMSDModalMessageEventsFromList(
			Collection<MSDModalMessageEvent> msdModalMessageEvents) {
		EList<MSDModalMessageEvent> filteredMSDModalMessageEvents = new UniqueEList<MSDModalMessageEvent>();
		for (MSDModalMessageEvent msdModalMessageEvent : msdModalMessageEvents) {
			//MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent.getAssumptionsModality();
			MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();
			
			if (requirementsModality.isMandatory() || getCurrentUserInteractingSimulationAgent().getSimulationManager().getCurrentMSDRuntimeState().isEnvironmentMessageEvent(msdModalMessageEvent.getRepresentedMessageEvent())){
				filteredMSDModalMessageEvents.add(msdModalMessageEvent);
				if (msdModalMessageEvent
						.getParentSymbolicMSDModalMessageEvent() != null) {
					filteredMSDModalMessageEvents.add(msdModalMessageEvent
							.getParentSymbolicMSDModalMessageEvent());
				}
			}
		}
		return filteredMSDModalMessageEvents;
	}

	@Override
	protected void objectDoubleClicked(
			Object obj) {
		if (obj instanceof MSDModalMessageEvent){
			getCurrentUserInteractingSimulationAgent().setNextEvent(((ModalMessageEvent) obj).getRepresentedMessageEvent());
		}
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				MSDModalMessageEventSelectionView.this
						.getCurrentUserInteractingSimulationAgent()
						.getSimulationManager()
						.performNextStepFromSimulationAgent(
								MSDModalMessageEventSelectionView.this
										.getCurrentUserInteractingSimulationAgent());
			}
		});
	}

	protected void refreshCurrentModelMessageEventsList() {
		if (getCurrentSimulationManager() == null
				|| getCurrentSimulationManager().getCurrentMSDRuntimeState() == null)
			return;

		setInput((Collection<MSDModalMessageEvent>) (Collection<?>) getCurrentSimulationManager()
				.getCurrentMSDRuntimeState()
				.getMessageEventToModalMessageEventMap().values());
		MSDModalMessageEventSelectionView.this.getViewer().refresh();
	}

	protected void toggleExecutionMode() {
		if (getCurrentSimulationManager() == null)
			return;

		if (togglePlayOutExecutionModeAction.isChecked()) {
			getCurrentSimulationManager().setCurrentExecutionSemantics(
					ExecutionSemantics.PLAY_OUT);
			if (getCurrentSimulationManager().getCurrentMSDRuntimeState() != null)
				getCurrentSimulationManager().getCurrentMSDRuntimeState()
						.updateMSDModalMessageEvents(
								ExecutionSemantics.PLAY_OUT);
			refreshCurrentModelMessageEventsList();
		} else {
			getCurrentSimulationManager().setCurrentExecutionSemantics(
					ExecutionSemantics.ALWAYS_ALL_EVENTS);
			if (getCurrentSimulationManager().getCurrentMSDRuntimeState() != null)
				getCurrentSimulationManager().getCurrentMSDRuntimeState()
						.updateMSDModalMessageEvents(
								ExecutionSemantics.ALWAYS_ALL_EVENTS);
			refreshCurrentModelMessageEventsList();
		}
	}

	Action togglePlayOutExecutionModeAction;

	@Override
	protected void makeActions() {
		super.makeActions();

		togglePlayOutExecutionModeAction = new Action(
				"Toggle Play-Out execution mode", Action.AS_CHECK_BOX) {
			public void run() {
				toggleExecutionMode();
			}
		};
		togglePlayOutExecutionModeAction
				.setToolTipText("Toggle between Play-Out and general execution mode (Play-Out: if there are active system events only consider these, otherwise consider all environment events; General: in every state consider all environment and system message events.)");
		togglePlayOutExecutionModeAction.setImageDescriptor(ImageHelper
				.getImageDescriptor("play-out-execution-mode.png"));

	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		// TODO Auto-generated method stub
		super.fillLocalPullDown(manager);

		manager.add(togglePlayOutExecutionModeAction);
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		super.fillLocalToolBar(manager);

		manager.add(togglePlayOutExecutionModeAction);
	}

	@Override
	public void dispose() {
		disposeSelectionListener();
		super.dispose();
	}
}
