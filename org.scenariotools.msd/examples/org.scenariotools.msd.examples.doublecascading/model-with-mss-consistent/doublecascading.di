<?xml version="1.0" encoding="ASCII"?>
<di:SashWindowsMngr xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:di="http://www.eclipse.org/papyrus/0.7.0/sashdi">
  <pageList>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_QkFA8ON_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_cL-RION_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_pCZxAON_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_fGXJMOOAEeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_qD1t8OOBEeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_lwzfkOOOEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_YXiXYOOZEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_rHG04OOZEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_NBeMUOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_d_WCsOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_rvOTEOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_Aq2YcOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_OOh_4OPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_acOEwOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_lw9IYOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_xSLI0OPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#__QvVcOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_L4NzUOPCEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage/>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_dirxoPPMEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_B5hqAPPcEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_PtLxsPPdEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_8PdYQPPdEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_WAyL0PPfEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_Cw72UPPgEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_puWtMPPgEeK1Kt2iVyZU9A"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_M5NgEPSfEeKVqa_XSRry_Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_TSN0EPSgEeKVqa_XSRry_Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_EKFFYPSiEeKVqa_XSRry_Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_z6ol0PSiEeKVqa_XSRry_Q"/>
    </availablePage>
  </pageList>
  <sashModel currentSelection="//@sashModel/@windows.0/@children.0">
    <windows>
      <children xsi:type="di:TabFolder">
        <children>
          <emfPageIdentifier href="doublecascading.notation#_M5NgEPSfEeKVqa_XSRry_Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_TSN0EPSgEeKVqa_XSRry_Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_EKFFYPSiEeKVqa_XSRry_Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_z6ol0PSiEeKVqa_XSRry_Q"/>
        </children>
      </children>
    </windows>
  </sashModel>
</di:SashWindowsMngr>
