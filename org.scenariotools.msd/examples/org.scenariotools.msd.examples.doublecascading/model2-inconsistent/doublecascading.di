<?xml version="1.0" encoding="ASCII"?>
<di:SashWindowsMngr xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:di="http://www.eclipse.org/papyrus/0.7.0/sashdi">
  <pageList>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_QkFA8ON_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_cL-RION_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_pCZxAON_EeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_fGXJMOOAEeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_qD1t8OOBEeGtOpzEbVgyDg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_lwzfkOOOEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_YXiXYOOZEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_rHG04OOZEeGv6uAqVV6h5Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_NBeMUOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_d_WCsOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_rvOTEOPAEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_Aq2YcOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_OOh_4OPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_acOEwOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_lw9IYOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_xSLI0OPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#__QvVcOPBEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_L4NzUOPCEeGCFLRg723uBg"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_EmS68P5HEeGF7K-SKcmk6Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_mjgnUP5HEeGF7K-SKcmk6Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_w7K-sP5HEeGF7K-SKcmk6Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_8DFHQP5HEeGF7K-SKcmk6Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_UB_XcP5IEeGF7K-SKcmk6Q"/>
    </availablePage>
    <availablePage>
      <emfPageIdentifier href="doublecascading.notation#_nOzxoP5IEeGF7K-SKcmk6Q"/>
    </availablePage>
  </pageList>
  <sashModel currentSelection="//@sashModel/@windows.0/@children.0">
    <windows>
      <children xsi:type="di:TabFolder">
        <children>
          <emfPageIdentifier href="doublecascading.notation#_QkFA8ON_EeGtOpzEbVgyDg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_fGXJMOOAEeGtOpzEbVgyDg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_qD1t8OOBEeGtOpzEbVgyDg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_Aq2YcOPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_OOh_4OPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_acOEwOPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_lw9IYOPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_xSLI0OPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#__QvVcOPBEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_L4NzUOPCEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_d_WCsOPAEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_lwzfkOOOEeGv6uAqVV6h5Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_YXiXYOOZEeGv6uAqVV6h5Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_rHG04OOZEeGv6uAqVV6h5Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_NBeMUOPAEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_rvOTEOPAEeGCFLRg723uBg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_cL-RION_EeGtOpzEbVgyDg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_pCZxAON_EeGtOpzEbVgyDg"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_EmS68P5HEeGF7K-SKcmk6Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_mjgnUP5HEeGF7K-SKcmk6Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_w7K-sP5HEeGF7K-SKcmk6Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_8DFHQP5HEeGF7K-SKcmk6Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_UB_XcP5IEeGF7K-SKcmk6Q"/>
        </children>
        <children>
          <emfPageIdentifier href="doublecascading.notation#_nOzxoP5IEeGF7K-SKcmk6Q"/>
        </children>
      </children>
    </windows>
  </sashModel>
</di:SashWindowsMngr>
