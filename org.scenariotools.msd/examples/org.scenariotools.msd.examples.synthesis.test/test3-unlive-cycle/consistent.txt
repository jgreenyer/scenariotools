consistent.
The challenge is that there may be a loop in the state space graph that must be ecluded by the controller extraction.

The loop is LOOP=(m3->m2->m1)^omega
after e1->m1->m2->m1->LOOP

instead, the extracted controller should be
(e1->m1->m2->m1)^omega

