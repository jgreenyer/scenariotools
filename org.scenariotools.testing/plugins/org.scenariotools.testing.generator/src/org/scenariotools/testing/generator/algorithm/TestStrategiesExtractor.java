package org.scenariotools.testing.generator.algorithm;

import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class TestStrategiesExtractor {
	private TestPopulatorAlgorithm testPopulatorAlgorithm;
	private InteractionBasedTestSuite interactionBasedTestSuite;
	

	public TestStrategiesExtractor(
			TestPopulatorAlgorithm testPopulatorAlgorithm,
			InteractionBasedTestSuite interactionBasedTestSuite) {
		this.testPopulatorAlgorithm = testPopulatorAlgorithm;
		this.interactionBasedTestSuite = interactionBasedTestSuite;
	}

	public void extractTestStrategies() {
		for (InteractionBasedTest interactionBasedTest : interactionBasedTestSuite.getInteractionBasedTests()) {
			ExtractionAlgorithm extractionAlgorithm = new ExtractionAlgorithm(
					testPopulatorAlgorithm);
			StateGraph testStrategy = extractionAlgorithm
					.extractTestStrategy(interactionBasedTest);
			interactionBasedTest.setTestGraph(testStrategy);
		}
	}

}
