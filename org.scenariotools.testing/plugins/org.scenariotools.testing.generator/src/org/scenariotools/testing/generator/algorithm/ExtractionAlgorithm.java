package org.scenariotools.testing.generator.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;

public class ExtractionAlgorithm {
	// map states to all associated tests
	private TestPopulatorAlgorithm populatorAlgorithm;
	// set of visited states during forward exploration
	private Set<RuntimeState> visited;
	// set of visited transitions during forward exploration
	private Set<Transition> visitedTransitions;
	// stack of transitions to be explored
	private Stack<Transition> waiting;
	// map of states to remainingsubTest
	Map<State, InteractionBasedTest> mapStatesToRemainingSubTest;
	// map from testController to testStrategy
	Map<RuntimeState, RuntimeState> testControllerToStrategyStatesMap;

	public ExtractionAlgorithm(TestPopulatorAlgorithm populatorAlgorithm) {
		this.populatorAlgorithm = populatorAlgorithm;
		this.visited = new HashSet<RuntimeState>();
		this.visitedTransitions = new HashSet<Transition>();
		this.waiting = new Stack<Transition>();
		this.mapStatesToRemainingSubTest = new HashMap<State, InteractionBasedTest>();
		this.testControllerToStrategyStatesMap = new HashMap<RuntimeState, RuntimeState>();
	}

	public StateGraph extractTestStrategy(InteractionBasedTest testToBeExtracted) {
		RuntimeState startState = populatorAlgorithm
				.getTestGeneratorAlgorithm().getStartState();
		visited.add(startState);
		mapStatesToRemainingSubTest.put(startState, testToBeExtracted);
		addTransitionsToBeExplored(startState);
		while (otherTransitionsNeedToBeExplored()) {			
			Transition t = getTransitionToExplore();
			if(!testToBeExtracted.getGoalStates().contains(t.getSourceState())){
				if (!visitedTransitions.contains(t)){
					visitedTransitions.add(t);
					RuntimeState targetState = (RuntimeState) t.getTargetState();
					forwardExploration(targetState, t);
				}
			}
		}
		return createTestStrategyFromStates(visited);
	}

	private void addTransitionsToBeExplored(RuntimeState sourceState) {
		InteractionBasedTest testToBeExtracted = mapStatesToRemainingSubTest
				.get(sourceState);
		EList<InteractionTuple> activeInteractionsOfSourceState = populatorAlgorithm
				.getStateToActiveInteractionTuples().get(sourceState);
		for (Transition t : sourceState.getOutgoingTransition()) {
			
//			System.out.println("ExtractionAlgorithm transition evaluated = "
//					+ t.getEvent());
			boolean needToExploreOtherTransitions = true;
			State targetState = t.getTargetState();
			if (populatorAlgorithm.getTestGeneratorAlgorithm()
					.getWinAndGoalStates().contains(targetState)) {
				EList<InteractionTuple> activeInteractionsOfTargetState = populatorAlgorithm
						.getStateToActiveInteractionTuples().get(targetState);
				InteractionBasedTest copyOfTestToBeExtracted = EcoreUtil
						.copy(testToBeExtracted);
				EList<InteractionTuple> copyOfActiveInteractionsOfSourceState = new BasicEList<InteractionTuple>(
						EcoreUtil.copyAll(activeInteractionsOfSourceState));

				copyOfTestToBeExtracted
						.removeAllInteractionTuples(copyOfActiveInteractionsOfSourceState);
				EList<InteractionTuple> copyOfActiveInteractionsOfTargetState = new BasicEList<InteractionTuple>(
						EcoreUtil.copyAll(activeInteractionsOfTargetState));
				copyOfTestToBeExtracted
						.addAllInteractionTuples(copyOfActiveInteractionsOfTargetState);
				if (populatorAlgorithm.getTestGeneratorAlgorithm()
						.isControllable(t)) {
					// we need to explore all controllable transitions
					waiting.push(t);
					mapStatesToRemainingSubTest.put(targetState,
							copyOfTestToBeExtracted);
				} else {
					// we need to explore only one of the uncontrollable
					// transition
					Set<InteractionBasedTest> testsOfTargetState = populatorAlgorithm
							.getStateToTestsMap().get(targetState);
					for (InteractionBasedTest testOfTargetState : testsOfTargetState) {
						if (testOfTargetState
								.hasSameInteractionTuples(copyOfTestToBeExtracted)) {
							waiting.push(t);
							needToExploreOtherTransitions = false;
							mapStatesToRemainingSubTest.put(targetState,
									copyOfTestToBeExtracted);
							break;
						}
					}
					if (!needToExploreOtherTransitions)
						break;
				}
			}
		}

	}

	private boolean otherTransitionsNeedToBeExplored() {
		return !waiting.isEmpty();
	}

	private Transition getTransitionToExplore() {
		return waiting.pop();
	}

	private void forwardExploration(RuntimeState targetState, Transition t) {
		visited.add(targetState);
		visitedTransitions.add(t);
		addTransitionsToBeExplored(targetState);
	}

	private StateGraph createTestStrategyFromStates(Set<RuntimeState> states) {

		StateGraph testStrategy = RuntimeFactory.eINSTANCE
				.createRuntimeStateGraph();
		
		for (RuntimeState visitedState : visited) {
			RuntimeState testStrategyState = createTestStrategyState(visitedState);
			testStrategy.getStates().add(testStrategyState);
			if (visitedState == populatorAlgorithm.getTestGeneratorAlgorithm().getStartState())
				testStrategy.setStartState(testStrategyState);
			

			// add new state to map
			testControllerToStrategyStatesMap.put(visitedState,
					testStrategyState);
		}

		for (RuntimeState visitedState : visited) {
			// create corresponding testStrategy transitions for all
			// transitions between visited states
			for (Transition outTransition : visitedState
					.getOutgoingTransition()) {
				if (visitedTransitions.contains(outTransition)) {
					RuntimeState sourceState = testControllerToStrategyStatesMap
							.get(visitedState);
					RuntimeState targetState = testControllerToStrategyStatesMap
							.get(outTransition.getTargetState());
					Transition testStrategyTransition = StategraphFactory.eINSTANCE
							.createTransition();
					testStrategyTransition.setSourceState(sourceState);
					testStrategyTransition.setTargetState(targetState);

					// reference original event
					testStrategyTransition.setEvent(outTransition.getEvent());
				}
			}
		}
		return testStrategy;

	}
	
	private RuntimeState createTestStrategyState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();

		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		if (state.getStringToBooleanAnnotationMap().get("win") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("win",
					state.getStringToBooleanAnnotationMap().get("win"));
		if (state.getStringToBooleanAnnotationMap().get("loseBuechi") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi",
					state.getStringToBooleanAnnotationMap().get("loseBuechi"));
		if (state.getStringToBooleanAnnotationMap().get("goal") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("goal",
					state.getStringToBooleanAnnotationMap().get("goal"));
		if (state.isSafetyViolationOccurredInRequirements())
			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		//setting goal state
		if (populatorAlgorithm.getTestGeneratorAlgorithm().getGoalStates().contains(state))
			newControllerState.getStringToBooleanAnnotationMap().put("goal", true);

		return newControllerState;
	} 

}
