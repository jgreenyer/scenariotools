package org.scenariotools.testing.generator.algorithm;

import java.util.Random;

import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

public class RandomizedTestSuiteGeneratorAlgorithm {
	public InteractionBasedTestSuite generateRandomTestSuite(InteractionBasedTestSuite sourceTestSuite, RuntimeStateGraph stateGraphToExplore){
		InteractionBasedTestSuite randomTestSuite = TestsuiteFactory.eINSTANCE.createInteractionBasedTestSuite();
		for (InteractionBasedTest sourceTest: sourceTestSuite.getInteractionBasedTests()){
			InteractionBasedTest generatedTest = generateRandomTest(sourceTest.getTestGraph().getStates().size(), stateGraphToExplore);
			randomTestSuite.getInteractionBasedTests().add(generatedTest);
		}
		return randomTestSuite;
	}

	private InteractionBasedTest generateRandomTest(int size, RuntimeStateGraph stateGraphToExplore) {
		InteractionBasedTest randomTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTest();
		StateGraph randomTestGraph = RuntimeFactory.eINSTANCE
				.createRuntimeStateGraph();
		RuntimeState currentState = (RuntimeState) stateGraphToExplore.getStartState();
		RuntimeState copyOfCurrentState = createTestStrategyState((RuntimeState)currentState);
		copyOfCurrentState.getStringToBooleanAnnotationMap().put("goal", false);
		randomTestGraph.getStates().add(copyOfCurrentState);		
		int numberOfAddedState = 1;
		while (numberOfAddedState < size){
			if (currentState.getOutgoingTransition().isEmpty())
				break;
			Transition randomTransition = getRandomTransition(currentState);			
			RuntimeState newTargetState = createTestStrategyState((RuntimeState)randomTransition.getTargetState());		
			Transition testStrategyTransition = StategraphFactory.eINSTANCE
					.createTransition();
			// reference original event
			testStrategyTransition.setEvent(randomTransition.getEvent());
			testStrategyTransition.setSourceState(copyOfCurrentState);
			testStrategyTransition.setTargetState(newTargetState);					
			
			randomTestGraph.getStates().add(newTargetState);
			
			numberOfAddedState++;
			copyOfCurrentState = newTargetState;
			currentState = (RuntimeState)randomTransition.getTargetState();
			if(numberOfAddedState != size - 1){
				copyOfCurrentState.getStringToBooleanAnnotationMap().put("goal", false);
			}
			else {
				copyOfCurrentState.getStringToBooleanAnnotationMap().put("goal", true);
			}
			
		}
		randomTestGraph.setStartState(randomTestGraph.getStates().get(0));
		

		
		
		randomTest.setTestGraph(randomTestGraph);
		
		return randomTest;
	}

	private Transition getRandomTransition(State currentState) {
		int sizeOfOutGoingTransition = currentState.getOutgoingTransition().size();
		int randomTransitionIndex = new Random().nextInt(sizeOfOutGoingTransition);
		return currentState.getOutgoingTransition().get(randomTransitionIndex);
	}
	
	private RuntimeState createTestStrategyState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();

		
		newControllerState.setObjectSystem(state.getObjectSystem());


		return newControllerState;
	} 

}
