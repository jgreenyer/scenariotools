package org.scenariotools.testing.generator.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class RandomizedMutantGeneratorAlgorithm {
	private Map<State,RuntimeState> originalStateToNewStateMap;
	private List<Transition> transitionList;
	public RandomizedMutantGeneratorAlgorithm() {
		originalStateToNewStateMap = new HashMap<>();
		transitionList = new ArrayList<>();
		
	}
	public RuntimeStateGraph generateMutant(
			RuntimeStateGraph originalController, int numberOfMutatedStates) {
		RuntimeStateGraph mutant = RuntimeFactory.eINSTANCE
				.createRuntimeStateGraph();
		
		
		for (State originalState : originalController.getStates()){
			RuntimeState newState = createState((RuntimeState)originalState);
			mutant.getStates().add(newState);
			originalStateToNewStateMap.put(originalState, newState);
		}
		for (State originalState : originalController.getStates()){
			for (Transition t : originalState.getOutgoingTransition()){
				createControllerTransition(t);				
			}
		}
		mutant.setStartState(originalStateToNewStateMap.get(originalController.getStartState()));
		
		mutate (mutant,numberOfMutatedStates);
//		mutateStates(mutant, numberOfMutatedStates);
		
		return mutant;
	}
	
	private void mutate(RuntimeStateGraph mutant, int numberOfMutatedStates) {
		int numberOfTransitions = transitionList.size();
		for (int i = 0; i < numberOfMutatedStates; i++){			
			// setting to a random transition an event coming from another random transition
			int randomTransitionIndex = new Random().nextInt(numberOfTransitions);			
			int otherRandomTransitionIndex = new Random().nextInt(numberOfTransitions);
			Transition t = transitionList.get(randomTransitionIndex);					
			Transition otherT = transitionList.get(otherRandomTransitionIndex);
			t.setTargetState(otherT.getTargetState());
		}		
	}
	
	private void mutateStates(RuntimeStateGraph mutant, int numberOfMutatedStates){
		int numberOfStates = mutant.getStates().size();
		List<RuntimeState> listOfStates = new ArrayList<>(originalStateToNewStateMap.values());
		for (int i = 0; i < numberOfMutatedStates; i++){
			int randomStateIndex = new Random().nextInt(numberOfStates);
			
			RuntimeState randomState = listOfStates.get(randomStateIndex);
			while (randomState.getOutgoingTransition().isEmpty()){
				randomStateIndex = new Random().nextInt(numberOfStates);
				
				randomState = listOfStates.get(randomStateIndex);
			}
			randomState.getOutgoingTransition().remove(0);
		}
		
	}
	private RuntimeState createState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();
		newControllerState.setObjectSystem(state.getObjectSystem());
		return newControllerState;
	} 
	
	private void createControllerTransition(Transition transition){
		createControllerTransition(
				originalStateToNewStateMap.get(transition.getSourceState()),
				originalStateToNewStateMap.get(transition.getTargetState()),
				transition.getEvent()
				);
	} 
	
	private void createControllerTransition(State sourceControllerState, State targetControllerState, Event event){
		Transition controllerTransition = StategraphFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
		transitionList.add(controllerTransition);
	}

}
