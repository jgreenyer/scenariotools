package org.scenariotools.testing.generator.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class TestGenerator {
	private RuntimeStateGraph newController;
	protected TestGeneratorAlgorithm testGeneratorAlgorithm;
	private Map<RuntimeState, RuntimeState> specStateToControllerStateMap;
	
	private Set<RuntimeState> excludedStates;
	private Set<Transition> excludedTransitions;
	
	public void initializeTestGeneratorAlgorithm(TestGeneratorAlgorithm testGeneratorAlgorithm) {
		this.testGeneratorAlgorithm = testGeneratorAlgorithm;
	}
	
	public boolean findTest(MSDRuntimeStateGraph msdRuntimeStateGraph) {
		
		boolean testStrategyExists = testGeneratorAlgorithm.testStrategyExists((RuntimeState)msdRuntimeStateGraph.getStartState());

		if (testStrategyExists) {
			postProcessSuccessfulSynthesisResult();
		} else {
			postProcessUnsuccessfulSynthesisResult();
		}
		return testStrategyExists;
	}
	
	protected void postProcessSuccessfulSynthesisResult() {
		Set<RuntimeState> winAndGoalStates = testGeneratorAlgorithm
				.getWinAndGoalStates();
		
		computeExcludedElements(winAndGoalStates);
		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
		newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();

		// add start state first
		RuntimeState newControllerStartState = RuntimeFactory.eINSTANCE
				.createRuntimeState();
		newControllerStartState.setObjectSystem(((RuntimeState) testGeneratorAlgorithm
				.getStartState()).getObjectSystem());
		newController.getStates().add(newControllerStartState);
		specStateToControllerStateMap.put(
				(RuntimeState) testGeneratorAlgorithm.getStartState(),
				newControllerStartState);
		newController.setStartState(newControllerStartState);

		for (RuntimeState winningSpecificationState : winAndGoalStates) {
			if (winningSpecificationState != testGeneratorAlgorithm.getStartState() 
					&& !excludedStates.contains(winningSpecificationState)) {
				RuntimeState newControllerState = RuntimeFactory.eINSTANCE
						.createRuntimeState();
				newControllerState.setObjectSystem(winningSpecificationState
						.getObjectSystem());
				newController.getStates().add(newControllerState);
				// add new state to map
				specStateToControllerStateMap.put(winningSpecificationState,
						newControllerState);
			}
		}

		for (RuntimeState winningSpecificationState : winAndGoalStates) {
			// create corresponding newController transitions for all
			// transitions between winning states
			for (Transition outTransition : winningSpecificationState
					.getOutgoingTransition()) {
				if (!excludedTransitions.contains(
								outTransition)) {
					RuntimeState sourceState = specStateToControllerStateMap
							.get(winningSpecificationState);
					RuntimeState targetState = specStateToControllerStateMap
							.get(outTransition.getTargetState());
					Transition newControllerTransition = StategraphFactory.eINSTANCE
							.createTransition();
					newControllerTransition.setSourceState(sourceState);
					newControllerTransition.setTargetState(targetState);

					// reference original event
					newControllerTransition.setEvent(outTransition
							.getEvent());
				}
			}
		}			
	}
		
	private void computeExcludedElements(Set<RuntimeState> winAndNotLoseBuechiStates){
		excludedStates = new HashSet<RuntimeState>();
		excludedTransitions = new HashSet<Transition>();
		
		//remove transitions with target not in set of winning states	
		for (RuntimeState state : winAndNotLoseBuechiStates) {
			for (Transition outTransition : state.getOutgoingTransition()) {
				if (outTransition.getTargetState() == null
						|| !winAndNotLoseBuechiStates.contains(outTransition
								.getTargetState()))
					excludeTransition(outTransition);					
			}
		}

//		//remove unlive loops
//		for(Transition transition:otfrAlgorithm.getUnliveLoopTransitions()){
//			excludeTransition(transition);
//		}
		
		Set<RuntimeState> reachableStates = computeReachableStates();
		
		//remove unreachable states
		for(RuntimeState state:winAndNotLoseBuechiStates){
			if(!reachableStates.contains(state))
				excludeState(state);
		}
	}
	
	private Set<RuntimeState> computeReachableStates(){
		Set<RuntimeState> result=new HashSet<RuntimeState>();
		List<RuntimeState> open=new LinkedList<RuntimeState>();
		open.add(testGeneratorAlgorithm.getStartState());
		
		while(!open.isEmpty()){
			RuntimeState current=open.remove(0);
			result.add(current);
			for(Transition outTransition:current.getOutgoingTransition()){
				if(!excludedTransitions.contains(outTransition) && !result.contains(outTransition.getTargetState())){
					open.add((RuntimeState)outTransition.getTargetState());
				}
			}
		}
		return result;
	}
	private void excludeTransition(Transition transitionToExclude) {
		if (excludedTransitions.contains(transitionToExclude))
			return;
		RuntimeState sourceState = (RuntimeState) transitionToExclude
				.getSourceState();
		excludedTransitions.add(transitionToExclude);
		
		// exclude states without unmarked outgoing transitions, except if environment assumptions are violated
		if (isDeadlockState(sourceState)
				&& !sourceState.isSafetyViolationOccurredInAssumptions()
				&& !excludedStates.contains(sourceState)) {
			excludeState(sourceState);
		}
	}
	
	private boolean isDeadlockState(RuntimeState state) {
		for(Transition outTransition:state.getOutgoingTransition()){
			if(!excludedTransitions.contains(outTransition)){
				return false;
			}
		}
		return true;
	}

	private void excludeState(RuntimeState stateToExclude) {
		excludedStates.add(stateToExclude);
		for(Transition inTransition: stateToExclude.getIncomingTransition()){
			if(!excludedTransitions.contains(inTransition))
				excludeTransition(inTransition);
		}
		for(Transition outTransition: stateToExclude.getOutgoingTransition()){
			if(!excludedTransitions.contains(outTransition))
				excludeTransition(outTransition);
		}
	}

	protected void postProcessUnsuccessfulSynthesisResult() {
		//TODO		
	}
	public TestGeneratorAlgorithm getTestGeneratorAlgorithm() {
		return testGeneratorAlgorithm;
	}
	public RuntimeStateGraph getNewController() {
		return newController;
	}



}
