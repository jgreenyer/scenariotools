package org.scenariotools.testing.generator.algorithm;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

public class MinimizationGreedyAlgorithm {
	private InteractionBasedTestSuite originalTestSuite;
	private InteractionBasedTestSuite minimizedTestSuite;
	private int sizeOfOriginalTestSuite;
	private int sizeOfMinimizedTestSuite;
	private PriorityQueue<InteractionBasedTest> queue;
	
	public MinimizationGreedyAlgorithm(InteractionBasedTestSuite interactionBasedTestSuite){
		this.originalTestSuite = interactionBasedTestSuite;
	}
	
	public void minimize(){
		sizeOfOriginalTestSuite = computeSizeOfTestSuite(originalTestSuite);
		populatePriorityQueue(originalTestSuite.getInteractionBasedTests());
		originalTestSuite.getOverallCoveredTuples().addAll(originalTestSuite.computeOverallCoveredTuples());
		boolean minimizationOK = minimizeTestSuite(originalTestSuite.getOverallCoveredTuples());
		if (!minimizationOK){
			System.err.println("################ERROR in MinimizationGreedyAlgorithm");
			return;
		}
		sizeOfMinimizedTestSuite = computeSizeOfTestSuite(minimizedTestSuite);
	}

	private int computeSizeOfTestSuite(
			InteractionBasedTestSuite testSuite) {
		int counter = 0;
		for (InteractionBasedTest interactionBasedTest :testSuite.getInteractionBasedTests()){
			if (!interactionBasedTest.getCoveredInteractionTuples().isEmpty())
				counter++;
			else {
				System.out.println("covered tuple empty");
			}
		}
		return counter;
	}
	private void populatePriorityQueue(
			EList<InteractionBasedTest> interactionBasedTests) {
		Comparator<InteractionBasedTest> comparator = new InteractionBasedTestLengthComparator();
		queue = new PriorityQueue<InteractionBasedTest>(getSizeOfOriginalTestSuite(),comparator);
		queue.addAll(interactionBasedTests);	
		
	}
	
	private boolean minimizeTestSuite(
			EList<InteractionTuple> overallCoveredTuples) {
		minimizedTestSuite = TestsuiteFactory.eINSTANCE.createInteractionBasedTestSuite();
		while (!overallCoveredTuples.isEmpty()){			
			InteractionBasedTest nextTest = getQueue().poll();
			if (nextTest == null){
				return false;
			}
			assert(nextTest.getCoveredInteractionTuples().size()!=0);
			System.out.println("nextTest.getCoveredInteractionTuples().size() = " + nextTest.getCoveredInteractionTuples().size());
								
			EList<InteractionTuple> coveredTuplesOfNextTest = nextTest.getCoveredInteractionTuples(); 
			boolean atLeastOneTupleRemoved = removeAllTuples(overallCoveredTuples, coveredTuplesOfNextTest);
			if (atLeastOneTupleRemoved)
				minimizedTestSuite.getInteractionBasedTests().add(EcoreUtil.copy(nextTest));				
				
		}
		
		return true;
	
	
	}
	
	private boolean removeAllTuples(
			EList<InteractionTuple> overallCoveredTuples,
			EList<InteractionTuple> coveredTuplesOfNextTest) {
		Iterator<InteractionTuple> it = overallCoveredTuples.iterator();
		boolean atLeastOneRemoved = false;
		while (it.hasNext()){
			InteractionTuple toBeRemoved = it.next();
			for (InteractionTuple tupleOfNextTest : coveredTuplesOfNextTest){
				if (EcoreUtil.equals(toBeRemoved, tupleOfNextTest)){
					atLeastOneRemoved =true;
					it.remove();
					break;
					
				}
			}
		}
		
		return atLeastOneRemoved;
	}

	private class InteractionBasedTestLengthComparator implements Comparator<InteractionBasedTest>
	{

		@Override
		public int compare(InteractionBasedTest test1, InteractionBasedTest test2) {
			if (test1.getCoveredInteractionTuples().size() > test2.getCoveredInteractionTuples().size())
				return -1;
			if (test1.getCoveredInteractionTuples().size() < test2.getCoveredInteractionTuples().size())
				return 1;
			return 0;
		}
		
	}
	
	public InteractionBasedTestSuite getOriginalTestSuite() {
		return originalTestSuite;
	}

	public InteractionBasedTestSuite getMinimizedTestSuite() {
		return minimizedTestSuite;
	}

	public int getSizeOfOriginalTestSuite() {
		return sizeOfOriginalTestSuite;
	}

	public int getSizeOfMinimizedTestSuite() {
		return sizeOfMinimizedTestSuite;
	}

	protected PriorityQueue<InteractionBasedTest> getQueue() {
		return queue;
	}
}
