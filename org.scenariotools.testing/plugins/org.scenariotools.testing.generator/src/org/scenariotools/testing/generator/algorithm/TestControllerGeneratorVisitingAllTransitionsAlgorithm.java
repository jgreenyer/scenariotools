package org.scenariotools.testing.generator.algorithm;

import java.util.HashSet;
import java.util.Set;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class TestControllerGeneratorVisitingAllTransitionsAlgorithm extends TestGeneratorAlgorithm{

	private Set<RuntimeState> winning;
	private Set<RuntimeState> violating;

	public TestControllerGeneratorVisitingAllTransitionsAlgorithm(){
		super();		
		this.winning = new HashSet<RuntimeState>();
		this.violating = new HashSet<RuntimeState>();
	}
	@Override
	protected boolean otherTransitionsNeedToBeExplored() {		
		return !getWaiting().isEmpty();	
	}
 
	@Override
	protected void forwardExploration(RuntimeState targetState, Transition t) {
		getVisited().add(targetState);
		addDepend(targetState, t);
		if (!violating.contains(targetState))
			addTransitionsForAllOutgoingConcreteMessageEvents(targetState);			
		if (isGoal(targetState)) {
			getWaiting().push(t);
		} 
						
	}


	@Override
	protected boolean isGoal(RuntimeState state) {	
		if (getGoalStates().contains(state))
			return true;
		else {			
			if (!state.isSafetyViolationOccurredInRequirements() && 
					!state.isSafetyViolationOccurredInAssumptions() && 
					!violating.contains(state) &&
					 !stateContainsUnvisitedSuccessors(state)) {
				getGoalStates().add(state);
				return true;
			}
		}
		return false;
	}
	
	private boolean stateContainsUnvisitedSuccessors(RuntimeState state){
		for (Transition t:state.getOutgoingTransition()){
			RuntimeState runtimeState = null;
			try {
				runtimeState = (RuntimeState) t.getTargetState();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			if (!violating.contains(runtimeState) && !isAlreadyVisited(runtimeState))
				return true;
		}
		return false;
	}
	
	@Override
	protected void backwardReEvaluation(RuntimeState sourceState,
			RuntimeState targetState, Transition t) {
		if (violatingStatusChanged(sourceState)) {			
			addDependentTransitionsForReEvaluation(sourceState);
			violating.add(sourceState);
		}
		else if (winningStatusChanged(sourceState)) {
			addDependentTransitionsForReEvaluation(sourceState);
			addStateAnnotation(sourceState, "+w");
		}

		if (!isWinning(targetState) && !violating.contains(targetState))
			addDepend(targetState, t);

		if (!isWinning(targetState) && !isGoal(targetState)
				&& !violating.contains(targetState)) {			
			getUnliveLoopTransitions().add(t);
		}		
	}
	
	private boolean violatingStatusChanged(RuntimeState sourceState) {		
		return !violating.contains(sourceState) && updateViolatingStatus(sourceState);
	}

	/**
	 * mark the source state to violating only if all the outgoing transitions 
	 * are controllable and have as target state a violating state 
	 * @param sourceState
	 * @return true if the source state is now a violating state
	 */
	private boolean updateViolatingStatus(RuntimeState sourceState) {
		if (sourceState.isSafetyViolationOccurredInAssumptions() || sourceState.isSafetyViolationOccurredInRequirements())
			return true;
		if (sourceState.getOutgoingTransition().isEmpty())
			return true;
		for (Transition transition: sourceState.getOutgoingTransition()) {			
			if (transition.getTargetState() == null)				
				return false; //other transitions need to be explored first
			if (!isControllable(transition))
				return false;
			else if (!violating.contains(transition.getTargetState()))
				return false;				
		}
		return true;
	}
	
	private boolean winningStatusChanged(RuntimeState sourceState) {		
		return !isWinning(sourceState) && updateWinningStatus(sourceState);
	}

	private boolean updateWinningStatus(RuntimeState sourceState) {
		boolean stateHasControllableOutgoingTransitions = false;
		boolean hasUnexploredControllableTransitions = false;
		boolean stateHasControllableOutgoingTransitionsLeadingToNonWinningState = false;
		
		for (Transition transition : sourceState.getOutgoingTransition()) {
					
			boolean transitionIsUncontrollable = true;
			if (isControllable(transition)) {
				transitionIsUncontrollable = false;
				stateHasControllableOutgoingTransitions = true;
			}
			RuntimeState targetState = (RuntimeState) transition.getTargetState();
			
			
			if (!transitionIsUncontrollable && targetState == null) {
					hasUnexploredControllableTransitions = true;
			}
			boolean isGoalTargetState = targetState != null	&& 
					isGoal(targetState);
			boolean isViolatingTargetState = targetState!= null && 
					violating.contains(targetState);
			if (transitionIsUncontrollable) {
				if (isGoalTargetState && !isViolatingTargetState) {					
					winning.add(sourceState);					
					return true;
				}
				if(winning.contains(targetState)){
					winning.add(sourceState);
					return true;
				}
			}

			if (!transitionIsUncontrollable) {
				if (!isGoalTargetState && !winning.contains(targetState))
				{
					// target state is already explored but not marked as winning nor goal
					stateHasControllableOutgoingTransitionsLeadingToNonWinningState = true;
				}								
			}			
		}
		
		if (stateHasControllableOutgoingTransitions && 
				!stateHasControllableOutgoingTransitionsLeadingToNonWinningState
				&& !hasUnexploredControllableTransitions){
			winning.add(sourceState);
			return true;
		}
		
		return false;
	}
	

	@Override
	protected boolean isWinning(State state) {		
		return winning.contains(state);
	}

	@Override
	public Set<RuntimeState> getWinAndGoalStates() {
		Set<RuntimeState> winAndGoalStates = new HashSet<RuntimeState>(winning);
		winAndGoalStates.addAll(getGoalStates());
		return winAndGoalStates;
	}
	
	@Override
	protected void setStateStatusFlags(RuntimeStateGraph graph){
		for(State state:graph.getStates()){
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("goal", getGoalStates().contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("loseBuechi", violating.contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("win", isWinning(state));
		}
	}
	@Override
	protected void postSynthesisProcessing() {
		recomputeGoalStates();
		
	}
	
	private void recomputeGoalStates(){
		getGoalStates().clear();
		getWaiting().clear();
		getVisited().clear();
		
		addStartStateToVisited(getStartState());
		addTransitionsForAllOutgoingConcreteMessageEvents(getStartState());
		while (otherTransitionsNeedToBeExplored()) {
			Transition t = getWaiting().pop();			
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if (!isAlreadyVisited(targetState)) {
				forwardExploration(targetState, t);
			} 
		}
	}

}
