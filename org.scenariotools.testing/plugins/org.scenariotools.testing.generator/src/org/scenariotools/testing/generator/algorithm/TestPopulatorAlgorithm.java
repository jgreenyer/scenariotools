package org.scenariotools.testing.generator.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Interaction;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TWiseCoverage;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;



public class TestPopulatorAlgorithm {
	private TestGeneratorAlgorithm testGeneratorAlgorithm;
	// set of visited states during forward exploration
	private Set<RuntimeState> visited;
	//set of visited states during backward reevaluation
	private Set<RuntimeState> backwardEvaluated;
	// stack of transitions to be explored
	private Stack<Transition> waiting;
	// maps states to transitions from predecessors
	private Map<RuntimeState, Set<Transition>> depend;
	// set of visited transitions
	private Set<Transition> visitedTransitions;
	// map states to active interactions
	private Map<RuntimeState, Set<Interaction>> stateToActiveInteractionsMap;
	// map states to active Interaction Tuples
	private Map<RuntimeState, EList<InteractionTuple>> stateToActiveInteractionTuples;
	
	// map states to all associated tests
	private Map<RuntimeState, Set<InteractionBasedTest>> stateToTestsMap;
	
	private InteractionBasedTestSuite interactionBasedTestSuite;
	
	private TWiseCoverage tWiseCoverage;	
	
	public TestPopulatorAlgorithm (TestGeneratorAlgorithm testGeneratorAlgorithm, int tWiseCoverageCriterionIndex) {
		this.testGeneratorAlgorithm = testGeneratorAlgorithm;
		this.visited = new HashSet<RuntimeState>();
		this.backwardEvaluated = new HashSet<RuntimeState>();
		this.waiting = new Stack<Transition>();
		this.depend = new HashMap<RuntimeState, Set<Transition>>();		
		this.visitedTransitions = new HashSet<Transition>();
		this.stateToActiveInteractionsMap = new HashMap<RuntimeState, Set<Interaction>>();
		this.stateToActiveInteractionTuples = new HashMap<RuntimeState, EList<InteractionTuple>>();
		this.stateToTestsMap = new HashMap<RuntimeState, Set<InteractionBasedTest>>();		
		this.interactionBasedTestSuite = TestsuiteFactory.eINSTANCE.createInteractionBasedTestSuite();
		this.settWiseCoverage(TWiseCoverage.get(tWiseCoverageCriterionIndex));
	
	}
	
	public void extractTests() {
		RuntimeState startState = testGeneratorAlgorithm.getStartState();			
		visited.add(startState);
		addTransitionsToBeExplored(startState);
		while (otherTransitionsNeedToBeExplored()) {
			Transition t = getTransitionToExplore();			
			//System.out.println("current transition event " + t.getEvent());
			RuntimeState sourceState = (RuntimeState) t.getSourceState();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if (!isAlreadyVisited(targetState)) {
				forwardExploration(targetState, t);
			} else {
				visitedTransitions.add(t);
				backwardReEvaluation(sourceState, targetState, t);		
			}
		}
		stateToTestsMap.get(startState);
		interactionBasedTestSuite.getInteractionBasedTests().addAll(stateToTestsMap.get(startState));
	}



	private void addTransitionsToBeExplored(RuntimeState sourceState) {
		for (Transition t: sourceState.getOutgoingTransition() ) {
			if (testGeneratorAlgorithm.isWinning(t.getTargetState()) || testGeneratorAlgorithm.getGoalStates().contains(t.getTargetState()))
				waiting.push(t);
		}		
	}

	private Transition getTransitionToExplore() {		
		return waiting.pop();
	}

	private boolean otherTransitionsNeedToBeExplored() {		
		return !waiting.isEmpty();
	}
	
	private boolean isAlreadyVisited(RuntimeState state) {
		return visited.contains(state);
	}
	
	private void forwardExploration(RuntimeState targetState, Transition t) {
		visited.add(targetState);
		addDepend(targetState, t);
		if (testGeneratorAlgorithm.getGoalStates().contains(targetState)) {			
			// first time this goal state is visited
			// create the test for the goal state
			InteractionBasedTest test = createTestForGoalState(targetState);
			addTestToState(test, targetState);
			waiting.push(t);
		} else
		{
			// continue the exploration
			addTransitionsToBeExplored(targetState);
		}		
	}

	private InteractionBasedTest createTestForGoalState(RuntimeState state) {						
		Set<Interaction> activeInteractions = getActiveInteractions((MSDRuntimeState) state);
		System.out.println(gettWiseCoverage());
		InteractionBasedTest test = TestsuiteFactory.eINSTANCE.createInteractionBasedTestFromInteractions(activeInteractions,gettWiseCoverage());
		test.getGoalStates().add(state);
		EList<InteractionTuple> interactionTuplesCoveredByState = test.getCoveredInteractionTuples();		
		stateToActiveInteractionTuples.put(state,interactionTuplesCoveredByState);				
		return test;
	}
	
	private void addTestToState(InteractionBasedTest test, RuntimeState state) {
		Set<InteractionBasedTest> testSet = stateToTestsMap.get(state);
		if (testSet == null){
			testSet = new HashSet<InteractionBasedTest>();
			stateToTestsMap.put(state, testSet);
		}
		if (!isTestAlreadyContainedInSet(testSet, test))
			testSet.add(test);				
	}
	
	private Set<Interaction> getActiveInteractions(MSDRuntimeState state) {		
		Set<Interaction> activeInteractions = stateToActiveInteractionsMap.get(state);
		if (activeInteractions == null){
			activeInteractions = new HashSet<Interaction>();
			for (ActiveProcess activeProcess : state.getActiveProcesses()){
				ActiveMSD activeMSD = (ActiveMSD) activeProcess;
				activeInteractions.add(activeMSD.getInteraction());
			}
			stateToActiveInteractionsMap.put(state, activeInteractions);
		}			
		return activeInteractions;
	}
	
	

	private void backwardReEvaluation(RuntimeState sourceState,
			RuntimeState targetState, Transition t) {
		backwardEvaluated.add(targetState);
		Set<InteractionBasedTest> testsOfSourceState = stateToTestsMap.get(sourceState);
		EList<InteractionTuple> activeInteractionTuplesOfSourceState = getActiveInteractionTuplesOfState(sourceState);		
		Set<InteractionBasedTest> testsOfTargetState = stateToTestsMap.get(targetState);
		if (testsOfTargetState != null){
			//consider only the transitions which does not close a loop
			if (testsOfSourceState == null) {
				// first time we backreavaluate the source state
				// copy the tests of the target state and put them to the source state			
				testsOfSourceState = new HashSet<InteractionBasedTest>();		 
				for (InteractionBasedTest targetStateTest : testsOfTargetState){					 
					InteractionBasedTest copyOfTargetStateTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByCopy(targetStateTest);							
					copyOfTargetStateTest.addAllInteractionTuples(activeInteractionTuplesOfSourceState);	
					if (!isTestAlreadyContainedInSet(testsOfSourceState, copyOfTargetStateTest))
						testsOfSourceState.add(copyOfTargetStateTest);					
				}			
			}
			else {
				if (testGeneratorAlgorithm.isControllable(t)) {
					Set<InteractionBasedTest> updatedTestsOfSourceState = new HashSet<InteractionBasedTest>();
					// intersect the tuples of the target state with the existing in the target state
					for (InteractionBasedTest currentTestOfSourceState : testsOfSourceState) {
						for (InteractionBasedTest testOfTargetState: testsOfTargetState) {
							InteractionBasedTest intersectionTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByIntersection(currentTestOfSourceState, testOfTargetState);						
							intersectionTest.addAllInteractionTuples(activeInteractionTuplesOfSourceState);
							if (!isTestAlreadyContainedInSet(updatedTestsOfSourceState, intersectionTest))
								updatedTestsOfSourceState.add(intersectionTest);						
						}					
					}
					testsOfSourceState = updatedTestsOfSourceState;
				}
				else {
					// add all the tests of the target state to the source state and for each test add the active interaction tuples of the source state
					for (InteractionBasedTest targetStateTest: testsOfTargetState) {
						InteractionBasedTest copyOfTargetStateTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByCopy(targetStateTest);
						copyOfTargetStateTest.addAllInteractionTuples(activeInteractionTuplesOfSourceState);
						if (!isTestAlreadyContainedInSet(testsOfSourceState, copyOfTargetStateTest))
							testsOfSourceState.add(copyOfTargetStateTest);
					}
				}
			}
		
			stateToTestsMap.put(sourceState,testsOfSourceState);
		}
			if (allTargetStatesAreAlreadyBackwardEvaluated(sourceState))
				addDependentTransitionsForReEvaluation(sourceState);
		
	}
	
	private boolean isTestAlreadyContainedInSet(Set<InteractionBasedTest> setOfTests, InteractionBasedTest testToCheck){
		for (InteractionBasedTest testOfSet : setOfTests){
			if (testOfSet.hasSameInteractionTuples(testToCheck))
				return true;
		}
		return false;
	}
	private EList<InteractionTuple> getActiveInteractionTuplesOfState(RuntimeState state) {
		EList<InteractionTuple> activeInteractionTuples = stateToActiveInteractionTuples.get(state);
		if (activeInteractionTuples == null) {					
			Set<Interaction> activeInteractionsOfState = getActiveInteractions((MSDRuntimeState) state);			
			InteractionBasedTest tempTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestFromInteractions(activeInteractionsOfState, gettWiseCoverage());
			activeInteractionTuples = new BasicEList<InteractionTuple>();
			activeInteractionTuples.addAll(tempTest.getCoveredInteractionTuples());
			stateToActiveInteractionTuples.put(state, activeInteractionTuples);
		}
		return activeInteractionTuples;
	}
 
	private boolean allTargetStatesAreAlreadyBackwardEvaluated(
			RuntimeState sourceState) {
		for (Transition outgoingTransition : sourceState.getOutgoingTransition()) {
			RuntimeState targetState = (RuntimeState) outgoingTransition.getTargetState();
			if (testGeneratorAlgorithm.isWinning(targetState) || testGeneratorAlgorithm.getGoalStates().contains(targetState)) {
				if (!backwardEvaluated.contains(targetState))
					return false;
			}
		}
		return true;
	}

	
	protected void addDepend(RuntimeState q, Transition t) {
		Set<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new HashSet<Transition>();
			depend.put(q, dependingTransitions);
		}
		dependingTransitions.add(t);
	}
	
	protected void addDependentTransitionsForReEvaluation(RuntimeState sourceState) {
		if (depend.get(sourceState) != null)
			waiting.addAll(depend.get(sourceState));
		
	}

	public InteractionBasedTestSuite getInteractionBasedTestSuite() {
		
		return interactionBasedTestSuite;
	}
	
	protected TestGeneratorAlgorithm getTestGeneratorAlgorithm() {
		return testGeneratorAlgorithm;
	}

	protected Map<RuntimeState, Set<InteractionBasedTest>> getStateToTestsMap() {
		return stateToTestsMap;
	}

	protected Map<RuntimeState, EList<InteractionTuple>> getStateToActiveInteractionTuples() {
		return stateToActiveInteractionTuples;
	}

	public TWiseCoverage gettWiseCoverage() {
		return tWiseCoverage;
	}

	public void settWiseCoverage(TWiseCoverage tWiseCoverage) {
		this.tWiseCoverage = tWiseCoverage;
	}
	


}
