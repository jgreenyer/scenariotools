package org.scenariotools.testing.generator.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;


public abstract class TestGeneratorAlgorithm {
	
	
	// set of goal states
	private Set<RuntimeState> goal;
	
	private RuntimeState startState;	
	// set of visited states
	private Set<RuntimeState> visited;
	// stack of transitions to be explored
	private Stack<Transition> waiting;
	// maps states to transitions from predecessors
	private Map<RuntimeState, Set<Transition>> depend;
	// set of visited transitions
	private Set<Transition> visitedTransitions;
	
	private Set<Transition> unliveLoopTransitions;

	protected abstract boolean otherTransitionsNeedToBeExplored();
	protected abstract void forwardExploration(RuntimeState state, Transition t);
	protected abstract void backwardReEvaluation (RuntimeState sourceState, RuntimeState targetState, Transition t);
	protected abstract boolean isWinning(State state);
	protected abstract boolean isGoal(RuntimeState state);
	public abstract Set<RuntimeState> getWinAndGoalStates();
	protected abstract void postSynthesisProcessing();
	
	public TestGeneratorAlgorithm() {		
		this.goal = new HashSet<RuntimeState>();
		this.visited = new HashSet<RuntimeState>();
		this.waiting = new Stack<Transition>();
		this.depend = new HashMap<RuntimeState, Set<Transition>>();
		this.unliveLoopTransitions = new HashSet<Transition>();
		this.visitedTransitions = new HashSet<Transition>();
	}
	
	public boolean testStrategyExists(RuntimeState startState){
		return findStrategy(startState);
	}
	
	protected boolean findStrategy(RuntimeState startState) {
		this.startState = startState;		
		
		addStartStateToVisited(startState);
		addTransitionsForAllOutgoingConcreteMessageEvents(startState);
		while (otherTransitionsNeedToBeExplored()) {
			Transition t = getTransitionToExplore();			
			RuntimeState sourceState = (RuntimeState) t.getSourceState();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if (!isAlreadyVisited(targetState)) {
				forwardExploration(targetState, t);
			} else {
				if (!visitedTransitions.contains(t)){
					visitedTransitions.add(t);
					backwardReEvaluation(sourceState, targetState, t);					
				}
			}
		}
		
		postSynthesisProcessing();
		addStateAnnotation(startState, "-OTFR");
		setStateStatusFlags((RuntimeStateGraph) startState.eContainer());
		return isWinning(startState);
	}

	protected void addStartStateToVisited(RuntimeState startState) {
		addStateAnnotation(startState, "+OTFR");
		visited.add(startState);
	}
	
	
	protected void addStateAnnotation(RuntimeState state, String newAnnotation) {
		String stateLog = state.getStringToStringAnnotationMap()
				.get("stateLog");
		String newStateLog;
		if (stateLog != null && !stateLog.isEmpty())
			newStateLog = stateLog + ", " + newAnnotation;
		else
			newStateLog = newAnnotation;
		String[] lines = newStateLog.split("\\n");
		if (lines[lines.length - 1].length() > 35)
			newStateLog += "\n";
		state.getStringToStringAnnotationMap().put("stateLog", newStateLog);
	}
	
	protected boolean addTransitionsForAllOutgoingConcreteMessageEvents(
			RuntimeState runtimeState) {
		Map<Integer, Set<Transition>> prioritySetsMap = new HashMap<Integer, Set<Transition>>();
		int maxPriorityValue = groupTransitionsByPriority(runtimeState, prioritySetsMap);
		
		boolean atLeastOneTransitionWasAdded = !prioritySetsMap.isEmpty();
		for (int i = maxPriorityValue; i >= 0; i--) {
			if (prioritySetsMap.get(i) != null) {
				getWaiting().addAll(prioritySetsMap.get(i));
			}
		}

		return atLeastOneTransitionWasAdded;
	}
	
	private int groupTransitionsByPriority(RuntimeState runtimeState,
			Map<Integer, Set<Transition>> prioritySetsMap) {
		
		int maxPriorityValue = 0;

		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
					.getKey();
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();
			if (messageEvent.isConcrete()) {
				Transition transition = runtimeState.getEventToTransitionMap().get(
						messageEvent);
	
				if (transition == null)
					transition = createTmpTransition(runtimeState, messageEvent);
	
				Integer transitionPriority = getTransitionPriority(transition,
						modalMessageEvent);
				if (maxPriorityValue < transitionPriority)
					maxPriorityValue = transitionPriority;
				if (prioritySetsMap.get(transitionPriority) == null) {
					prioritySetsMap.put(transitionPriority,
							new HashSet<Transition>());
				}	
				prioritySetsMap.get(transitionPriority).add(transition);				
			}
		}
		return maxPriorityValue;
	}

	protected Transition createTmpTransition(RuntimeState runtimeState,
			MessageEvent messageEvent) {
		Transition tmpTransition = StategraphFactory.eINSTANCE
				.createTransition();
		tmpTransition.setEvent(messageEvent);
		tmpTransition.setSourceState(runtimeState);
		return tmpTransition;
	}

	/**
	 * "0" is the highest priority.
	 *  We give highest priority to Uncontrollable events
	 * @param transition
	 * @param modalMessageEvent
	 * @return
	 */
	protected int getTransitionPriority(Transition transition,
			ModalMessageEvent modalMessageEvent) {
		if ((!isControllable(transition) && !modalMessageEvent
				.getRequirementsModality().isSafetyViolating()))
			return 0;
		else
			return 1;
	}
	
	protected boolean isControllable(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(
						((MessageEvent) transition.getEvent())
								.getSendingObject());
	}

	
	private Transition getTransitionToExplore() {
		Transition t = getWaiting().pop();
		if (t.getTargetState() == null)
			t = generateSuccessor(t);
		return t;
	}
	
	private Transition generateSuccessor(Transition t) {
		RuntimeState sourceState = (RuntimeState) t.getSourceState();
		t.setSourceState(null); // must detach temporary transition from source
								// state again!		
		t = ((RuntimeStateGraph) sourceState.getStateGraph())
				.generateSuccessor(sourceState, t.getEvent());

		return t;
	}
	
	protected boolean isAlreadyVisited(RuntimeState state) {
		return visited.contains(state);
	}

	protected abstract void setStateStatusFlags(RuntimeStateGraph graph);
	
	protected void addDepend(RuntimeState q, Transition t) {
		Set<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new HashSet<Transition>();
			depend.put(q, dependingTransitions);
		}
		dependingTransitions.add(t);
	}
	
	protected void addDependentTransitionsForReEvaluation(RuntimeState sourceState) {
		if (depend.get(sourceState) != null)
			getWaiting().addAll(depend.get(sourceState));
		//removeOutgoingTransitionsFromStack(getWaiting(), sourceState);// optimization
	}
	
	/**
	 * Removes the outgoing transitions of the given source state from the given
	 * stack and removes unexplored transitions from the source state, i.e.,
	 * transitions where the target state is null.
	 */
	private void removeOutgoingTransitionsFromStack(Stack<Transition> waiting,
			RuntimeState sourceState) {
		for (Iterator<Transition> transitionsIterator = sourceState
				.getOutgoingTransition().iterator(); transitionsIterator
				.hasNext();) {
			Transition transition = transitionsIterator.next();
			waiting.remove(transition);
			// remove unexplored tmp transitions
			if (transition.getTargetState() == null)
				transitionsIterator.remove();
		}
	}
	
	protected Map<RuntimeState, Set<Transition>> getDepend() {
		return depend;
	}
	
	protected Stack<Transition> getWaiting() {
		return waiting;
	}
	
	public Set<Transition> getUnliveLoopTransitions() {
		return unliveLoopTransitions;
	}
	public RuntimeState getStartState() {
		return startState;
	}
	protected Set<RuntimeState> getVisited() {
		return visited;
	}

	public Set<RuntimeState> getGoalStates() {
		return goal;
	}

}
