package org.scenariotools.testing.generator.strategyexporter;

import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public interface TestStrategiesExporter {

	public void exportStrategies(InteractionBasedTestSuite testSuiteToExport, String outDirectoryPath);
}
