package org.scenariotools.testing.generator.strategyexporter.graphviz;

import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.synthesis.graphviz.RuntimeStateGraphExporter;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.strategyexporter.TestStrategiesExporter;

public class GraphvizStrategiesExporter implements TestStrategiesExporter{

	@Override
	public void exportStrategies(InteractionBasedTestSuite testSuiteToExport,
			String outPathPrefix) {
		int strategyCounter =0;
		for(InteractionBasedTest test: testSuiteToExport.getInteractionBasedTests()){
			strategyCounter++;
			String outPath = outPathPrefix + "_testStrategy_"+strategyCounter+".gv";
			export(test, outPath);
		}
		
	}
	private void export(InteractionBasedTest test, String outPath){
		RuntimeStateGraphExporter runtimeStateGraphExporter = new RuntimeStateGraphExporter();
		runtimeStateGraphExporter.setOutPath(outPath);
		runtimeStateGraphExporter.export((RuntimeStateGraph)test.getTestGraph());
		
	}

}
