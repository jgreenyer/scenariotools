package org.scenariotools.testing.executor.algorithm;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class SingleTestExecutor {
	
	private State testCurrentState;
	private State controllerCurrentState;	
	
	public SingleTestExecutor(RuntimeStateGraph testGraph,
			RuntimeStateGraph controller) {	
		this.testCurrentState = testGraph.getStartState();
		this.controllerCurrentState = controller.getStartState();		
	}
	
	public boolean execute(){
		boolean executionResult = true;
		while (executionResult !=  false && !testCompleted()){
			if (testCurrentState.getOutgoingTransition().isEmpty() )
				return true;
			if (controllerCurrentState.getOutgoingTransition().isEmpty())
				return false;
			if (!isTestTurn()){
				Transition controllerTransition = getNextTransitionToExecute(controllerCurrentState);
				executionResult = updateState(testCurrentState, controllerTransition);
				controllerCurrentState = controllerTransition.getTargetState();				
			}
			else {
				Transition testTransition = getNextTransitionToExecute(testCurrentState);
				executionResult = updateState(controllerCurrentState,testTransition);
				testCurrentState = testTransition.getTargetState();				
			}
		}		
		return executionResult;		
	}
		

	private Transition getNextTransitionToExecute(State currentState) {
		// always returning the single available transition
		return currentState.getOutgoingTransition().get(0);
	}

	private boolean updateState(State stateToUpdate, Transition transitionToExecute) {
		boolean isStateUpdated = false;
		for (Transition currentTransition : stateToUpdate.getOutgoingTransition()){
			if (isValidTransition(currentTransition, transitionToExecute)){
				if (stateToUpdate == testCurrentState){
					testCurrentState = currentTransition.getTargetState();
				} else if (stateToUpdate == controllerCurrentState) {
					controllerCurrentState = currentTransition.getTargetState();
				}				
				isStateUpdated = true;
				break;
			}
		}
		return isStateUpdated;
	}

	private boolean isValidTransition(Transition currentTransition,
			Transition otherTransition) {	
		// currently implements a simple check between transition name. 
		// TODO to extend supporting the same object
		MessageEvent currentEvent = (MessageEvent) currentTransition.getEvent();
		MessageEvent otherEvent = (MessageEvent) otherTransition.getEvent();
		return currentEvent.getMessageName().equals(otherEvent.getMessageName());
	}

	private boolean isTestTurn() {		
		if (testCurrentState.getOutgoingTransition().isEmpty())
			return false;
		return !isControllable(testCurrentState.getOutgoingTransition().get(0));
	}

	private boolean testCompleted(){		
		return isGoalState(testCurrentState);
	}
	
	private boolean isGoalState(State state){
		return state.getStringToBooleanAnnotationMap().get("goal");
	}
	
	private boolean isControllable(Transition transition) {
		return !(((RuntimeState) transition.getSourceState()).getObjectSystem() != null
				&& transition.getEvent() != null
				&& transition.getEvent() instanceof MessageEvent && !((RuntimeState) transition
					.getSourceState()).getObjectSystem().isControllable(
				((MessageEvent) transition.getEvent()).getSendingObject()));
	}
	

}
