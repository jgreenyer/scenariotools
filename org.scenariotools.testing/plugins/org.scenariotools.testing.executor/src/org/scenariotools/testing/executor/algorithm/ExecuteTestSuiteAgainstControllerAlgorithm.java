package org.scenariotools.testing.executor.algorithm;

import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class ExecuteTestSuiteAgainstControllerAlgorithm {
	private InteractionBasedTestSuite testSuite;
	private RuntimeStateGraph controller;
	private int totalNumberOfTests;
	private int numberOfSuccessfulTests;
	
	
	public ExecuteTestSuiteAgainstControllerAlgorithm(InteractionBasedTestSuite testSuite, RuntimeStateGraph controller){
		this.testSuite = testSuite;
		this.controller = controller;
	}
	
	public void executeTestSuite(){
		totalNumberOfTests = testSuite.getInteractionBasedTests().size();
		for (InteractionBasedTest test : testSuite.getInteractionBasedTests()){
			SingleTestExecutor executor = new SingleTestExecutor((RuntimeStateGraph)test.getTestGraph(),controller);
			boolean isSuccessful = executor.execute();
			if (isSuccessful)
				numberOfSuccessfulTests++;
			
		}
	}
	
	public int getTotalNumberOfTests(){
		return totalNumberOfTests;
	}
	
	public int getNumberOfSuccessfulTests(){
		return numberOfSuccessfulTests;
	}
	
	public int getNumberOfFailedTests(){
		return totalNumberOfTests - numberOfSuccessfulTests;
	}
}
