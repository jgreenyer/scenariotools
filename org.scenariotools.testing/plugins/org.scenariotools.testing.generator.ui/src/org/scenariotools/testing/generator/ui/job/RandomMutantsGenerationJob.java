package org.scenariotools.testing.generator.ui.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.testing.generator.algorithm.RandomizedMutantGeneratorAlgorithm;
import org.scenariotools.testing.generator.manager.ResourceManager;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class RandomMutantsGenerationJob extends Job{
	
	
	private ResourceSet resourceSet;
	private RuntimeStateGraph originalController;
	private IFile controllerResourceFile;
	

	
	public RandomMutantsGenerationJob(String name,
			IFile controllerResourceFile, ResourceSet resourceSet,
			RuntimeStateGraph controller) {
		super(name);
		
		this.resourceSet = resourceSet;
		this.originalController = controller;
		this.controllerResourceFile = controllerResourceFile;
	}
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		RandomizedMutantGeneratorAlgorithm randomMutantGeneratorAlgorithm = new RandomizedMutantGeneratorAlgorithm();
		ResourceManager resourceManager = new ResourceManager(controllerResourceFile, resourceSet);
		int numberOfStatesOfOriginalController = originalController.getStates().size();
		int multiplier = 1;
		int numberOfMutatedStates = numberOfStatesOfOriginalController/100;
		numberOfMutatedStates = (numberOfMutatedStates > 0) ? numberOfMutatedStates : 1;
		RuntimeStateGraph mutant =randomMutantGeneratorAlgorithm.generateMutant(originalController,numberOfMutatedStates * multiplier);
		resourceManager.saveTestController(mutant, "_MUTANT_1%_NStates_" + numberOfMutatedStates * multiplier);
		
		randomMutantGeneratorAlgorithm = new RandomizedMutantGeneratorAlgorithm();
		numberOfMutatedStates = numberOfStatesOfOriginalController/20;
		numberOfMutatedStates = (numberOfMutatedStates > 0) ? numberOfMutatedStates : 1;
		mutant =randomMutantGeneratorAlgorithm.generateMutant(originalController,numberOfMutatedStates);
		resourceManager.saveTestController(mutant, "_MUTANT_5%_NStates_" + numberOfMutatedStates);
		
		randomMutantGeneratorAlgorithm = new RandomizedMutantGeneratorAlgorithm();
		numberOfMutatedStates = numberOfStatesOfOriginalController/10;
		numberOfMutatedStates = (numberOfMutatedStates > 0) ? numberOfMutatedStates : 1;
		mutant =randomMutantGeneratorAlgorithm.generateMutant(originalController,numberOfMutatedStates);
		resourceManager.saveTestController(mutant, "_MUTANT_10%_NStates_" + numberOfMutatedStates);
		
		
		postExecutionResult();
		return Status.OK_STATUS;
		
	}
	
	private void postExecutionResult(){
		Display.getDefault().asyncExec(new Runnable() {
			public void run(){
				MessageDialog.openInformation(new Shell(), "Random Mutants Generated", 
						"Random Mutants Generated"
						);
			}
		});
	}

}
