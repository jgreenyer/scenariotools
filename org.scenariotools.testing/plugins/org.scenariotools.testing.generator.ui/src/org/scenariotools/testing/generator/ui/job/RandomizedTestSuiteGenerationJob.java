package org.scenariotools.testing.generator.ui.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.testing.generator.algorithm.RandomizedTestSuiteGeneratorAlgorithm;
import org.scenariotools.testing.generator.manager.ResourceManager;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class RandomizedTestSuiteGenerationJob extends Job{
	
	private InteractionBasedTestSuite sourceTestSuite;
	private ResourceSet resourceSet;
	private RuntimeStateGraph sourceTestGraph;
	private IFile sourceTestSuiteResourceFile;
	

	public RandomizedTestSuiteGenerationJob(String name, IFile sourceTestSuiteResourceFile, InteractionBasedTestSuite testSuite,
			ResourceSet resourceSet, RuntimeStateGraph controller) {
		super(name);
		this.sourceTestSuite = testSuite;
		this.resourceSet = resourceSet;
		this.sourceTestGraph = controller;
		this.sourceTestSuiteResourceFile = sourceTestSuiteResourceFile;
		
		
	}
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		RandomizedTestSuiteGeneratorAlgorithm randomGeneratorAlgorithm = new RandomizedTestSuiteGeneratorAlgorithm();
		
		InteractionBasedTestSuite randomTestSuite = randomGeneratorAlgorithm.generateRandomTestSuite(sourceTestSuite, sourceTestGraph);
		ResourceManager resourceManager = new ResourceManager(sourceTestSuiteResourceFile, resourceSet);
		resourceManager.saveTestSuite(randomTestSuite, "_RANDOM");
		postExecutionResult();
		return Status.OK_STATUS;
		
	}
	
	private void postExecutionResult(){
		Display.getDefault().asyncExec(new Runnable() {
			public void run(){
				MessageDialog.openInformation(new Shell(), "Random Test Suite Generated", 
						"Random Test Suite Generated"
						);
			}
		});
	}

}
