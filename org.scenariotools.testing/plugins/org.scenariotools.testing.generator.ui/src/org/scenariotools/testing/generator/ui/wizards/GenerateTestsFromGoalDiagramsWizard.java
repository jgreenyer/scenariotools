package org.scenariotools.testing.generator.ui.wizards;

import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Interaction;
import org.scenariotools.testing.generator.manager.TestGeneratorManager;
import org.scenariotools.testing.generator.manager.TestGeneratorManagerFromGoalDiagrams;

public class GenerateTestsFromGoalDiagramsWizard extends
		AbstractScenarioTestConfigurationWizard {
	
	private ScenarioRunConfigurationFileSelectionWizardPage scenarioRunConfigurationFileSelectionWizardPage;

	private InteractionSelectionWizardPage interactionSelectionWizardPage;
	
	private Set<Interaction> interactions;

	@Override
	public void addPages() {		
		scenarioRunConfigurationFileSelectionWizardPage = new ScenarioRunConfigurationFileSelectionWizardPage(
				selection, resourceSet, this);
		addPage(scenarioRunConfigurationFileSelectionWizardPage);
		interactionSelectionWizardPage = new InteractionSelectionWizardPage(
				this);
		addPage(interactionSelectionWizardPage);		
		
	}
	
	/**
	 * the wizard can finish if both the interactions and the scenariorunconfiguration are selected 
	 */
	@Override
	public boolean canFinish() {
		return (getInteractions()!=null && !getInteractions().isEmpty() && getScenarioRunConfiguration() != null);
	}
	
	@Override
	protected TestGeneratorManager createTestGeneratorManager() {
		
		return new TestGeneratorManagerFromGoalDiagrams(
				getScenarioRunConfiguration(), getInteractions(),				 
				gettWiseCoverageCriterion());
	}
	
	public Set<Interaction> getInteractions() {
		return interactions;
	}

	public void setInteractions(Set<Interaction> interactions) {
		this.interactions = interactions;
	}


}
