package org.scenariotools.testing.generator.ui.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.wizard.WizardDialog;
import org.scenariotools.testing.generator.ui.wizards.GenerateAllTestsWizard;

public class GenerateAllTestsPopUpAction extends AbstractTestPopUpAction {

	@Override
	public void run(IAction action) {
		GenerateAllTestsWizard generateAllTestsWizard = new GenerateAllTestsWizard();
		generateAllTestsWizard.init(getWorkbench(), getSelection());
		WizardDialog dialog = new WizardDialog(getShell(),generateAllTestsWizard);
		dialog.open();
	}
	
}
