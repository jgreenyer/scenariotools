package org.scenariotools.testing.generator.ui.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.wizard.WizardDialog;
import org.scenariotools.testing.generator.ui.wizards.GenerateTestsFromGoalDiagramsWizard;
/**
 * PopUpAction activating the wizard for msd-based test generation obtained via goal diagrams
 * @author valerio
 *
 */
public class GenerateTestsFromGoalDiagramsPopUpAction extends AbstractTestPopUpAction {

	@Override
	public void run(IAction action) {
		GenerateTestsFromGoalDiagramsWizard generateTestsFromGoalDiagramsWizard = new GenerateTestsFromGoalDiagramsWizard();
		generateTestsFromGoalDiagramsWizard.init(getWorkbench(), getSelection());
		WizardDialog dialog = new WizardDialog(getShell(),generateTestsFromGoalDiagramsWizard);
		dialog.open();
	}
}
