package org.scenariotools.testing.generator.ui.wizards;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;



public class ScenarioRunConfigurationFileSelectionWizardPage extends WizardPage {

	private Text scenarioRunConfigurationURIText;
	private ISelection selection;
	private ResourceSet resourceSet;
	
	private Text containerText;
	
	private int tWiseComboSelectedValue;
	
	private AbstractScenarioTestConfigurationWizard scenarioTestConfigurationWizard;
	
	public ScenarioRunConfigurationFileSelectionWizardPage(
			 ISelection selection,
			ResourceSet resourceSet, AbstractScenarioTestConfigurationWizard abstractScenarioTestConfigurationWizard) {
		super("ScenarioRunConfigurationFileSelectionWizardPage");
		setTitle("Select Scenario Run Configuration file");
		setDescription("Select a .scenariorunconfiguration model file from which a new test case will be generated.");
		this.scenarioTestConfigurationWizard = abstractScenarioTestConfigurationWizard;
		
		this.selection = selection;
		this.resourceSet = resourceSet;
	}
	



	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
				
		Label label1 = new Label(container, SWT.NONE);
		label1.setText("ScenarioRunConfiguration model:");

		// resource name entry field
		scenarioRunConfigurationURIText = new Text(container, SWT.BORDER | SWT.SINGLE);
		scenarioRunConfigurationURIText.setText("");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		scenarioRunConfigurationURIText.setLayoutData(gd);
		scenarioRunConfigurationURIText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
				
			}
		});
		
		Button browseScenarioRunConfigurationFileURIButton = new Button(container, SWT.PUSH);
		browseScenarioRunConfigurationFileURIButton.setText("Browse...");

		browseScenarioRunConfigurationFileURIButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseScenarioRunConfigurationFile();
			}			

		});
		
		Label containerLabel = new Label(container, SWT.NULL);
		containerLabel.setText("Folder for test strategies:");
		
		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		containerText.setText("");
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button browseContainerButton = new Button(container, SWT.PUSH);
		browseContainerButton.setText("Browse...");
		browseContainerButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseContainer();
			}
		});
		
		Label comboLabel = new Label(container, SWT.NULL);
		comboLabel.setText("Select the t-wise coverage criterion:");
		Combo tWiseCombo = new Combo(container, SWT.DROP_DOWN);
		String[] items = {"Single-wise","Pair-wise","Three-wise","Four-wise"};
		tWiseCombo.setItems(items);		
		tWiseComboSelectedValue=1;
		tWiseCombo.select(tWiseComboSelectedValue);
		tWiseCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				tWiseComboSelectedValue = ((Combo)e.widget).getSelectionIndex();				
				dialogChanged();				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				
				
			}
		});

		initialize();
		dialogChanged();
		setControl(container);
		
	}
	
	
	public AbstractScenarioTestConfigurationWizard getScenarioTestConfigurationWizard() {
		return scenarioTestConfigurationWizard;
	}

	
	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */
	private void handleBrowseContainer() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select new file container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerText.setText(((Path) result[0]).toString());
			}
		}
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IFile) {
				IFile file = (IFile) obj;
				if (file.getFileExtension().equals("scenariorunconfiguration")){
					scenarioRunConfigurationURIText.setText(file.getFullPath().toString());					
				}
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {

		IResource scenarioRunConfigurationFile = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getScenarioRunConfigurationURIText()));

		if (getScenarioRunConfigurationURIText().length() == 0) {
			updateStatus("Scenario Run Configuration file must be specified");
			return;
		}
		if (scenarioRunConfigurationFile == null
				|| scenarioRunConfigurationFile.getType() != IResource.FILE
				|| !scenarioRunConfigurationFile.getFileExtension().equals("scenariorunconfiguration")) {
			updateStatus("The selected resource is not a scenariorunconfiguration file");
			return;
		}
		Resource scenarioRunConfigurationResource = resourceSet.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationFile.getFullPath().toString(), true), true);
		try {
			scenarioRunConfigurationResource.load(null);
		} catch (IOException e) {
			updateStatus("Cannot load the Scenario Run Configuration model: " + e);
			return;
		}
		if (scenarioRunConfigurationResource.getContents().isEmpty()){
			updateStatus("The Scenario Run Configuration model file does not contain any element");
			return;
		}
		if (!(scenarioRunConfigurationResource.getContents().get(0) instanceof ScenarioRunConfiguration)){
			updateStatus("The Scenario Run Configuration model must contain an instance of org.eclipse.uml2.uml.Package or org.eclipse.uml2.uml.Model as its root");
			return;
		}else{
			getScenarioTestConfigurationWizard().setScenarioRunConfigurationResourceFile((IFile)scenarioRunConfigurationFile);
			getScenarioTestConfigurationWizard().setScenarioRunConfiguration((ScenarioRunConfiguration) scenarioRunConfigurationResource.getContents().get(0));
		}
		
		
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getContainerName()));


		if (getContainerName().length() == 0) {
			updateStatus("File container must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("File container must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		else{
			getScenarioTestConfigurationWizard().setTestStrategiesFolderResource(container);
		}
		
		getScenarioTestConfigurationWizard().settWiseCoverageCriterion(tWiseComboSelectedValue+1);
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	public String getScenarioRunConfigurationURIText() {
		if (scenarioRunConfigurationURIText == null)
			return "";
		else
			return scenarioRunConfigurationURIText.getText();
	}
	
	private void handleBrowseScenarioRunConfigurationFile() {
       ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
               getShell(), new WorkbenchLabelProvider(),
               new BaseWorkbenchContentProvider());
         dialog.setTitle("Scenario Run Configuration model");
         dialog.setMessage("Select a Scenario Run Configuration model from the workspace:");
         dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
         ViewerFilter extensionFilter = new ViewerFilter()
         {
            @Override
            public boolean select(Viewer viewer, Object parentElement,
                  Object element)
            {
               return !(element instanceof IFile)
                     || !((IFile) element).getFileExtension().isEmpty();
            }
         };
         dialog.addFilter(extensionFilter);

         if (dialog.open() == ContainerSelectionDialog.OK)
         {
            Object[] result = dialog.getResult();
            if (result.length == 1)
            {
          	  IFile scenarioRunConfigurationFile = (IFile) result[0];
          	  scenarioRunConfigurationURIText.setText(scenarioRunConfigurationFile.getFullPath().toString());
            }
         }
      }
	public String getContainerName() {
		if (containerText == null) 
			return "";
		else
			return containerText.getText();
	}

}
