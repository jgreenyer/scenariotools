package org.scenariotools.testing.generator.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.testing.generator.manager.TestGeneratorManager;
/**
 * Abstract Wizard for MSD-based test generation.
 * The wizard allows the user to specify the scenariorunconfiguration file containing the MSD specification 
 * A concrete implementation specifies, the required wizard pages,  the generator manager and the desired output.  
 * @author valerio
 *
 */
public abstract class AbstractScenarioTestConfigurationWizard extends Wizard {
	
	protected ResourceSet resourceSet;
	protected ISelection selection;
	
	private ScenarioRunConfiguration scenarioRunConfiguration;	

	private IFile scenarioRunConfigurationFile;	
	
	private IResource testStrategiesFolderResource;
	
	private int tWiseCoverageCriterion;


	/**
	 * Adds pages to the wizard.
	 */
	public abstract void addPages();
	
	@Override
	public abstract boolean canFinish();	
	
	protected abstract TestGeneratorManager createTestGeneratorManager();
	
	public AbstractScenarioTestConfigurationWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;		
		resourceSet = new ResourceSetImpl();
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		return super.getNextPage(page);
	}

	
	@Override
	public boolean performFinish() {

		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {
				try {
					doFinish();
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} catch (Exception e) {
					e.printStackTrace();
				}
				finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error",
					realException.getMessage());
			return false;
		}
		return true;
	}
	

	protected void doFinish() throws CoreException {
		long currentTimeMillis = System.currentTimeMillis();
		
		TestGeneratorManager testGeneratorManager = createTestGeneratorManager();		
		boolean testsExist = testGeneratorManager.generateTests();

		if (testsExist) {
			testGeneratorManager.saveTestResources(getScenarioRunConfigurationFile(),getResourceSet(),getTestStrategiesFolderResource());
		}

		long currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;		
		postExplorationFinished(testsExist, currentTimeMillisDelta,
				testGeneratorManager);

	}
	
	protected static void postExplorationFinished(final boolean testsExist,
			final long currentTimeMillisDelta,
			final TestGeneratorManager testGeneratorManager) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = testGeneratorManager
						.getNumberOfExploredStates();
				int numberOfTransitions = testGeneratorManager
						.getNumberOfExploredTransitions();
				int numberOfGeneratedTests = testGeneratorManager
						.getNumberOfGeneratedTests();
				int numberOfMinimizedTests = testGeneratorManager
						.getNumberOfMinimizedTests();

				MessageDialog.openInformation(new Shell(),
						"Test strategy exists: "
								+ (testsExist ? "TRUE " : "FALSE "),
						"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of generated tests: "
								+ numberOfGeneratedTests
								+ "\nNumber of tests after minimization: "
								+ numberOfMinimizedTests);
			}
		});
	}
	
	public ScenarioRunConfiguration getScenarioRunConfiguration() {
		return scenarioRunConfiguration;
	}

	public void setScenarioRunConfiguration(
			ScenarioRunConfiguration scenarioRunConfiguration) {
		this.scenarioRunConfiguration = scenarioRunConfiguration;
	}

	public void setScenarioRunConfigurationResourceFile(
			IFile scenarioRunConfigurationFile) {
		this.scenarioRunConfigurationFile = scenarioRunConfigurationFile;		
	}
	
	public IFile getScenarioRunConfigurationFile() {
		return scenarioRunConfigurationFile;
	}

	public ResourceSet getResourceSet() {
		return resourceSet;
	}

	public IResource getTestStrategiesFolderResource() {
		return testStrategiesFolderResource;
	}

	public void setTestStrategiesFolderResource(
			IResource container) {
		this.testStrategiesFolderResource = container;
	}

	public int gettWiseCoverageCriterion() {
		return tWiseCoverageCriterion;
	}

	public void settWiseCoverageCriterion(int tWiseCoverageCriterion) {
		this.tWiseCoverageCriterion = tWiseCoverageCriterion;
	}
	
}
