package org.scenariotools.testing.generator.ui.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
/**
 * Abstract class implementing PopUpAction 
 * @author valerio
 *
 */
public abstract class AbstractTestPopUpAction implements IObjectActionDelegate {

	private Shell shell;
	private IWorkbench workbench;
	private IStructuredSelection selection;
	
	@Override
	public abstract void run(IAction action);

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// Auto-generated method stub		
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell= targetPart.getSite().getShell();
		workbench = targetPart.getSite().getWorkbenchWindow().getWorkbench();
		selection = (IStructuredSelection) targetPart.getSite().getSelectionProvider().getSelection();
	}

	public Shell getShell() {
		return shell;
	}

	public IWorkbench getWorkbench() {
		return workbench;
	}

	public IStructuredSelection getSelection() {
		return selection;
	}




}
