package org.scenariotools.testing.generator.ui.popup.actions;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.testing.executor.ui.job.TestExecutionJob;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.ui.job.RandomMutantsGenerationJob;
import org.scenariotools.testing.generator.ui.job.RandomizedTestSuiteGenerationJob;

public class GenerateRandomizedMutantsPopUpAction implements IObjectActionDelegate {
	private Shell shell;
	
	private IFile controllerResourceFile = null;
	private ResourceSet resourceSet;
	private RuntimeStateGraph controller;
	
	
	public GenerateRandomizedMutantsPopUpAction(){
		super();
	}
	
	@Override
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		List<?> selectionList = structuredSelection.toList();
		
		
		if ("msdruntime".equals(((IFile)selectionList.get(0)).getFileExtension())){			
			controllerResourceFile = ((IFile)selectionList.get(0));		
		}else{
			postProblem("You must select one *.msdruntime (containing the controller to be mutated).");
			return;
		}
		
		
		resourceSet = new ResourceSetImpl();
		
		Resource controllerResource = resourceSet
				.getResource(URI.createPlatformResourceURI(controllerResourceFile.getFullPath()
						.toString(), true), true);
		controller = (RuntimeStateGraph) controllerResource
				.getContents().get(0);


		Job job = createRandomMutantsGenerationJob();
		boolean dialogOk=openDialog(job);
		if (dialogOk){
			job.setUser(true);
			job.schedule();
		}
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
		
	}
	
	protected boolean openDialog(Job job){
		//empty method to override
		return true;
	}
	
	protected void postProblem(final String problem) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"A problem occurred",
						problem);
			}
		});
	}

	protected Job createRandomMutantsGenerationJob(){
		return new RandomMutantsGenerationJob("Randomized Mutant Generation", controllerResourceFile, resourceSet, controller);
	}


}
