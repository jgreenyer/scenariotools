package org.scenariotools.testing.generator.ui.wizards;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.edit.providers.UMLItemProviderAdapterFactory;
import org.eclipse.uml2.uml.edit.providers.UMLReflectiveItemProviderAdapterFactory;
import org.eclipse.uml2.uml.edit.providers.UMLResourceItemProviderAdapterFactory;


public class InteractionSelectionWizardPage extends WizardPage {
	private GenerateTestsFromGoalDiagramsWizard scenarioTestConfigurationWizard;
	private CheckboxTreeViewer modelViewer;
	protected Set<Interaction> selectedModelElements;
	
	protected InteractionSelectionWizardPage(GenerateTestsFromGoalDiagramsWizard abstractScenarioTestConfigurationWizard) {
		super("Select interactions to be considered for test generation");
		setTitle("Select Interactions");
		setDescription("Select interactions to be considered for test generation");
		this.scenarioTestConfigurationWizard =  abstractScenarioTestConfigurationWizard;
		selectedModelElements = new HashSet<Interaction>();
		
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if(visible) {
			modelViewer.setInput(scenarioTestConfigurationWizard.getScenarioRunConfiguration().getUml2EcoreMapping().getUmlPackage());
		}
			
	}
	@Override
	public void createControl(Composite parent) {
		initializeDialogUnits(parent);
		
		Composite plate = new Composite(parent, SWT.NONE);
		plate.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		plate.setLayout(layout);
		setControl(plate);
		
		Label label = new Label(plate, SWT.NONE);
		label.setText(getSelectionTitle());
		label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));
		
		modelViewer = new CheckboxTreeViewer(plate, SWT.CHECK | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		GridData layoutData = new GridData(GridData.FILL_BOTH);
		layoutData.heightHint = 300;
		layoutData.widthHint = 300;
		modelViewer.getTree().setLayoutData(layoutData);
		
	
		List<AdapterFactory> factories = new ArrayList<AdapterFactory>();
		factories.add(new UMLResourceItemProviderAdapterFactory());
		factories.add(new UMLItemProviderAdapterFactory());
		factories.add(new EcoreItemProviderAdapterFactory());
		factories.add(new UMLReflectiveItemProviderAdapterFactory());
		

		AdapterFactory adapterFactory = new ComposedAdapterFactory(factories);
		
		modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		modelViewer.setInput(scenarioTestConfigurationWizard.getScenarioRunConfiguration().getUml2EcoreMapping().getUmlPackage());

		modelViewer.addCheckStateListener(new ICheckStateListener() {
			
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				InteractionSelectionWizardPage.this
				.updateCheck(event);
				
			}
		});

		setPageComplete(validatePage());
		
		
	}
	
	
	protected boolean validatePage() {
		for (Object checkedElements :modelViewer.getCheckedElements()) {
			if (!(checkedElements instanceof Interaction)){
				updateStatus("All the checked elements must be instances of org.eclipse.uml2.uml.Interaction.");
				return false;
			}				
		}
		if (selectedModelElements.size() < 1) {
			updateStatus("Select at least one interaction.");
			return false;			
		}

		
		scenarioTestConfigurationWizard.setInteractions(selectedModelElements);
		updateStatus(null);
		return true;
	}
	
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	protected String getSelectionTitle() {
		return "Select interactions to be considered for test generation";
	}
	
	protected void updateCheck(CheckStateChangedEvent event) {
		if (event.getElement() instanceof Interaction) {
			Interaction selectedInteraction = (Interaction) event.getElement();
			if (event.getChecked()) {
				selectedModelElements.add(selectedInteraction);
			}
			else {
				selectedModelElements.remove(selectedInteraction);
			}			
		}
		setPageComplete(validatePage());
		
	}

}
