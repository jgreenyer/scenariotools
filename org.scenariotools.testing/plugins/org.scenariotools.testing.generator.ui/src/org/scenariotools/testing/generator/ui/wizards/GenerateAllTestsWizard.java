package org.scenariotools.testing.generator.ui.wizards;

import org.scenariotools.testing.generator.manager.TestGeneratorManager;

public class GenerateAllTestsWizard extends
		AbstractScenarioTestConfigurationWizard {
	
	private ScenarioRunConfigurationFileSelectionWizardPage scenarioRunConfigurationFileSelectionWizardPage;

	@Override
	public void addPages() {		
		scenarioRunConfigurationFileSelectionWizardPage = new ScenarioRunConfigurationFileSelectionWizardPage(
				selection, resourceSet, this);
		addPage(scenarioRunConfigurationFileSelectionWizardPage);		
	}
	
	/**
	 * the wizard can finish if scenariorunconfiguration is selected 
	 */
	public boolean canFinish() {
		return getScenarioRunConfiguration() != null;
	}

	@Override
	protected TestGeneratorManager createTestGeneratorManager() {		
		return new TestGeneratorManager(getScenarioRunConfiguration(), gettWiseCoverageCriterion());
	}
	



	



}
