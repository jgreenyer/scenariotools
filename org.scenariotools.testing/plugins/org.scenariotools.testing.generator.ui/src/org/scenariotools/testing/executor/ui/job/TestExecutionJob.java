package org.scenariotools.testing.executor.ui.job;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.testing.executor.algorithm.ExecuteTestSuiteAgainstControllerAlgorithm;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class TestExecutionJob extends Job{
	
	private InteractionBasedTestSuite testSuite;
	private ResourceSet resourceSet;
	private RuntimeStateGraph controller;
	

	public TestExecutionJob(String name, InteractionBasedTestSuite testSuite,
			ResourceSet resourceSet, RuntimeStateGraph controller) {
		super(name);
		this.testSuite = testSuite;
		this.resourceSet = resourceSet;
		this.controller = controller;
		
		
	}
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		ExecuteTestSuiteAgainstControllerAlgorithm testSuiteAlgorithm = new ExecuteTestSuiteAgainstControllerAlgorithm(testSuite, controller);
		long currentTimeMillis = System.currentTimeMillis();
		testSuiteAlgorithm.executeTestSuite();
		long currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;
		postExecutionResult(testSuiteAlgorithm, currentTimeMillisDelta);
		return Status.OK_STATUS;
		
	}
	
	private void postExecutionResult(final ExecuteTestSuiteAgainstControllerAlgorithm executorAlgorithm, final long currentTimeMillisDelta){
		Display.getDefault().asyncExec(new Runnable() {
			public void run(){
				MessageDialog.openInformation(new Shell(), "Test Suite Execution", 
						"Test Suite was executed," + 
						" number of total tests: " + executorAlgorithm.getTotalNumberOfTests() +
						" number of successful tests: "  + executorAlgorithm.getNumberOfSuccessfulTests() +
						" number of failures: "  + executorAlgorithm.getNumberOfFailedTests() +
						", execution time: " + currentTimeMillisDelta +"ms"
						);
			}
		});
	}

}
