/**
 */
package org.scenariotools.testing.generator.model.testsuite.tests;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interaction Based Test Suite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#computeOverallCoveredTuples() <em>Compute Overall Covered Tuples</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class InteractionBasedTestSuiteTest extends TestCase {

	/**
	 * The fixture for this Interaction Based Test Suite test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTestSuite fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InteractionBasedTestSuiteTest.class);
	}

	/**
	 * Constructs a new Interaction Based Test Suite test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionBasedTestSuiteTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interaction Based Test Suite test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InteractionBasedTestSuite fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interaction Based Test Suite test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTestSuite getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TestsuiteFactory.eINSTANCE.createInteractionBasedTestSuite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#computeOverallCoveredTuples() <em>Compute Overall Covered Tuples</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#computeOverallCoveredTuples()	 
	 */
	public void testComputeOverallCoveredTuples() {
		assertTrue(true);
	}
	
	public void testComputeOverallCoveredTuples_TestSuiteWithTwoTestsEachOfThisWithTwoDifferentSingletons_overallCoveredTupleIsFour(){
		InteractionBasedTest test1 = TestSuiteAllTestsFactory.createTestWithTwoSingletons();
		InteractionBasedTest test2 = TestSuiteAllTestsFactory.createTestWithTwoSingletons();
		
		fixture.getInteractionBasedTests().add(test1);
		fixture.getInteractionBasedTests().add(test2);		
		fixture.getOverallCoveredTuples().addAll(fixture.computeOverallCoveredTuples());
		
		assertTrue(fixture.getOverallCoveredTuples().size()==4);
	}
	
} //InteractionBasedTestSuiteTest
