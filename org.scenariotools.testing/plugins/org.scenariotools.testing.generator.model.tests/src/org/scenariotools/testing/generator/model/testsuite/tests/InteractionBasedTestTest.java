/**
 */
package org.scenariotools.testing.generator.model.testsuite.tests;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interaction Based Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#addInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Add Interaction Tuple</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#removeInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Remove Interaction Tuple</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#hasSameInteractionTuples(org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest) <em>Has Same Interaction Tuples</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class InteractionBasedTestTest extends TestCase {

	/**
	 * The fixture for this Interaction Based Test test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTest fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InteractionBasedTestTest.class);
	}

	/**
	 * Constructs a new Interaction Based Test test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionBasedTestTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interaction Based Test test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InteractionBasedTest fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interaction Based Test test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTest getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TestsuiteFactory.eINSTANCE.createInteractionBasedTest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}
	
	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#addInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Add Interaction Tuple</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#addInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 * @generated NOT
	 */
	public void testAddInteractionTuple__InteractionTuple() {
		InteractionTuple tuple = TestSuiteAllTestsFactory.createSingletonTuple();
		fixture.addInteractionTuple(tuple);
		assertTrue(fixture.getCoveredInteractionTuples().contains(tuple));
	}
	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#removeInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Remove Interaction Tuple</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#removeInteractionTuple(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 * @generated
	 */
	public void testRemoveInteractionTuple__InteractionTuple() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#hasSameInteractionTuples(org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest) <em>Has Same Interaction Tuples</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#hasSameInteractionTuples(org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest)
	 * @generated
	 */
	public void testHasSameInteractionTuples__InteractionBasedTest() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	public void testAddInteractionTuple__AddTheSameSingletonToTest__coveredTuplesDoNOTChange(){
		InteractionTuple tuple = TestSuiteAllTestsFactory.createSingletonTuple();
		fixture.addInteractionTuple(tuple);
		InteractionTuple tupleWithSameInteractions = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(tuple.getInteractions().get(0));
		fixture.addInteractionTuple(tupleWithSameInteractions);
		assertTrue(fixture.getCoveredInteractionTuples().size()==1 && fixture.getCoveredInteractionTuples().contains(tuple));
	}
	public void testAddInteractionTuple__AddAPairWithInteractionsInReverseOrder__coveredTuplesDoNOTChange(){		
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		Interaction interaction2 = UMLFactory.eINSTANCE.createInteraction();
		InteractionTuple tuple = TestsuiteFactory.eINSTANCE.createInteractionTuple();
		tuple.getInteractions().add(interaction1);
		tuple.getInteractions().add(interaction2);
		fixture.addInteractionTuple(tuple);
		InteractionTuple reversedTuple = TestsuiteFactory.eINSTANCE.createInteractionTuple();
		reversedTuple.getInteractions().add(interaction2);
		reversedTuple.getInteractions().add(interaction1);
		fixture.addInteractionTuple(reversedTuple);
		assertTrue(fixture.getCoveredInteractionTuples().size()==1 && fixture.getCoveredInteractionTuples().contains(tuple));
	}

	public void testCreateInteractionBasedTestByIntersection_TwoInputTestsHaveTheSameTuple_IntersectionHasSizeOne() {		
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		Set<Interaction> interactions1 = new HashSet<Interaction>();
		interactions1.add(interaction1);
		InteractionTuple tuple1 = TestsuiteFactory.eINSTANCE.createInteractionTupleFromInteractions(interactions1);	
		fixture.getCoveredInteractionTuples().add(tuple1);
		
		InteractionTuple copyOfTuple1 = TestsuiteFactory.eINSTANCE.createInteractionTupleFromInteractions(interactions1);		
		InteractionBasedTest copyOfFixture = TestsuiteFactory.eINSTANCE.createInteractionBasedTest();		
		copyOfFixture.getCoveredInteractionTuples().add(copyOfTuple1);
		
		InteractionBasedTest intersection = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByIntersection(fixture, copyOfFixture);
		assertTrue(intersection.getCoveredInteractionTuples().size()==1);
	}
	
	public void testCreateInteractionBasedTestByIntersection_TwoInputTestsHaveOnlyOneCommonTuple_IntersectionHasSizeOne() {		
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();		
		InteractionTuple tuple1 = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(interaction1);
		Interaction interaction2 = UMLFactory.eINSTANCE.createInteraction();
		InteractionTuple tuple2 = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(interaction2);		
		fixture.getCoveredInteractionTuples().add(tuple1);
		fixture.getCoveredInteractionTuples().add(tuple2);
		
		InteractionTuple copyOfTuple1 = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(interaction1);
		Interaction interaction3 = UMLFactory.eINSTANCE.createInteraction();
		InteractionTuple tuple3 = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(interaction3);
		
		InteractionBasedTest test2 = TestsuiteFactory.eINSTANCE.createInteractionBasedTest();
		test2.getCoveredInteractionTuples().add(copyOfTuple1);
		test2.getCoveredInteractionTuples().add(tuple3);
		InteractionBasedTest intersection = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByIntersection(fixture, test2);
		
		assertTrue(intersection.getCoveredInteractionTuples().size()==1);
	}
	
	public void testCreateInteractionBasedTestByCopy__AddRepeatedTuple__coveredTuplesDoNOTChange(){
		InteractionTuple tuple = TestSuiteAllTestsFactory.createSingletonTuple();
		InteractionTuple copiedTuple = TestSuiteAllTestsFactory.createSingletonTupleWithInteraction(tuple.getInteractions().get(0));
		fixture.addInteractionTuple(tuple);
		InteractionBasedTest copiedTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByCopy(fixture);
		copiedTest.addInteractionTuple(copiedTuple);
		assertTrue(copiedTest.getCoveredInteractionTuples().size()==1);
				
	}
	
	public void testCreateInteractionBasedTestByCopy_CompareOriginalTestWithTheCopy_BothTestsHaveTheSameCoveredTuples() {
		InteractionTuple tuple = TestSuiteAllTestsFactory.createSingletonTuple();
		fixture.addInteractionTuple(tuple);
		InteractionBasedTest copiedTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTestByCopy(fixture);
		assertTrue(copiedTest.getCoveredInteractionTuples().size() == 1 && fixture.getCoveredInteractionTuples().size()==1);
	}

	

} //InteractionBasedTestTest
