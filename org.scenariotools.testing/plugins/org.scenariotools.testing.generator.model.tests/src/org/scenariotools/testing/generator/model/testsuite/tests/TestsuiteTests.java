/**
 */
package org.scenariotools.testing.generator.model.testsuite.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>testsuite</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestsuiteTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new TestsuiteTests("testsuite Tests");
		suite.addTestSuite(InteractionBasedTestSuiteTest.class);
		suite.addTestSuite(InteractionBasedTestTest.class);
		suite.addTestSuite(InteractionTupleTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestsuiteTests(String name) {
		super(name);
	}

} //TestsuiteTests
