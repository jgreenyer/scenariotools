package org.scenariotools.testing.generator.model.testsuite.tests;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

public class TestSuiteAllTestsFactory {
	public static InteractionTuple createSingletonTupleWithInteraction(Interaction interaction){
		Set<Interaction> singletonInteractionSet = new HashSet<Interaction>();
		singletonInteractionSet.add(interaction);
		InteractionTuple singletonTuple = TestsuiteFactory.eINSTANCE.createInteractionTupleFromInteractions(singletonInteractionSet);
		return singletonTuple;
	}
	
	public static InteractionTuple createSingletonTuple() {
		Interaction interaction = UMLFactory.eINSTANCE.createInteraction();
		//to have the interaction structurally different
		interaction.setName((new Integer(interaction.hashCode()).toString()));
		return createSingletonTupleWithInteraction(interaction);
	}
	
	public static InteractionBasedTest createTestWithTwoSingletons(){
		InteractionBasedTest test = TestsuiteFactory.eINSTANCE.createInteractionBasedTest();
		test.addInteractionTuple(createSingletonTuple());
		test.addInteractionTuple(createSingletonTuple());
				
		return test;
	}
	
}
