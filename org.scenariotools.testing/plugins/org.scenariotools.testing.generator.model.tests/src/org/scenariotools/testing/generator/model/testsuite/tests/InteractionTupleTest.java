/**
 */
package org.scenariotools.testing.generator.model.testsuite.tests;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;
import junit.textui.TestRunner;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.UMLFactory;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interaction Tuple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#isEmpty() <em>Is Empty</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#intersect(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Intersect</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#union(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Union</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class InteractionTupleTest extends TestCase {

	/**
	 * The fixture for this Interaction Tuple test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionTuple fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InteractionTupleTest.class);
	}

	/**
	 * Constructs a new Interaction Tuple test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionTupleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interaction Tuple test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InteractionTuple fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interaction Tuple test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionTuple getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TestsuiteFactory.eINSTANCE.createInteractionTuple());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#isEmpty() <em>Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#isEmpty()
	 * @generated NOT
	 */
	public void testIsEmpty() {

		assertTrue(getFixture().isEmpty());
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#intersect(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Intersect</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#intersect(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 */
	public void testIntersect__InteractionTuple() {
		fixture.intersect(EcoreUtil.copy(getFixture()));
		assertTrue(getFixture().isEmpty());
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#union(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Union</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#union(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 */
	public void testUnion__InteractionTuple() {
		fixture.union(EcoreUtil.copy(getFixture()));
		assertTrue(getFixture().isEmpty());

	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#isEmpty() <em>Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#isEmpty()
	 * @generated NOT
	 */
	public void testIsEmpty_TupleJustCreated_true() {		
		assertTrue(fixture.isEmpty());	
	}

	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#intersect(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Intersect</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#intersect(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 * @generated NOT
	 */
	public void testIntersect__TwoTuplesWithDifferentInteractions_intersectionEmptyIsTrue() {		
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		fixture.getInteractions().add(interaction1);				
		fixture.intersect(createSingletonTuple());
		assertTrue(fixture.isEmpty());
	}
	
	public void testIntersect__TwoPairsWithTheSameInteractions_intersectionEqualToBothOfThose() {
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		Interaction interaction2 = UMLFactory.eINSTANCE.createInteraction();
		fixture.getInteractions().add(interaction1);
		fixture.getInteractions().add(interaction2);
		Set<Interaction>interactionsOfSecondTuple = new HashSet<Interaction>();		
		interactionsOfSecondTuple.add(interaction2);
		interactionsOfSecondTuple.add(interaction1);		
		InteractionTuple secondTuple = createTupleWithInteractions(interactionsOfSecondTuple);
		fixture.intersect(secondTuple);
		assertTrue(EcoreUtil.equals(fixture, secondTuple));
	}
	
	public void testIntersect__TwoPairsWithTheSameInteractions_intersectionHaveInteractionsOfSizeTwo() {
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		Interaction interaction2 = UMLFactory.eINSTANCE.createInteraction();
		fixture.getInteractions().add(interaction1);
		fixture.getInteractions().add(interaction2);
		Set<Interaction>interactionsOfSecondTuple = new HashSet<Interaction>();		
		interactionsOfSecondTuple.add(interaction2);
		interactionsOfSecondTuple.add(interaction1);		
		InteractionTuple secondTuple = createTupleWithInteractions(interactionsOfSecondTuple);
		fixture.intersect(secondTuple);
		assertTrue(fixture.getInteractions().size()==2);
	}
	private InteractionTuple createSingletonTuple() {
		
		Interaction interaction = UMLFactory.eINSTANCE.createInteraction();
		Set<Interaction> interactions = new HashSet<Interaction>();
		interactions.add(interaction);
		InteractionTuple tuple = createTupleWithInteractions(interactions);
		return tuple;
	}
	private InteractionTuple createTupleWithInteractions(Collection<Interaction> interactions) {
		InteractionTuple tuple = TestsuiteFactory.eINSTANCE.createInteractionTuple();
		tuple.getInteractions().addAll(interactions);
		return tuple;
	}
	
	/**
	 * Tests the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#union(org.scenariotools.testing.generator.model.testsuite.InteractionTuple) <em>Union</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#union(org.scenariotools.testing.generator.model.testsuite.InteractionTuple)
	 * @generated NOT
	 */
	public void testUnion__OfTwoSingletonTuplesWithDifferentInteraction_getInteractionsOfSize2() {
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		fixture.getInteractions().add(interaction1);
		InteractionTuple other = createSingletonTuple();
		fixture.union(other);
		assertTrue(fixture.getInteractions().size()==2);		
	}
	
	public void testUnion__OfTwoSingletonTuplesWithTheSameInteraction_getInteractionsOfSize1() {
		Interaction interaction1 = UMLFactory.eINSTANCE.createInteraction();
		fixture.getInteractions().add(interaction1);
		Set<Interaction> interactionsOfOther = new HashSet<Interaction>();		
		InteractionTuple other = createTupleWithInteractions(interactionsOfOther);
		fixture.union(other);
		assertTrue(fixture.getInteractions().size()==1);		
	}

} //InteractionTupleTest
