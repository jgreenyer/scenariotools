/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Based Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getCoveredInteractionTuples <em>Covered Interaction Tuples</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getTestGraph <em>Test Graph</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getGoalStates <em>Goal States</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTest()
 * @model
 * @generated
 */
public interface InteractionBasedTest extends EObject {
	/**
	 * Returns the value of the '<em><b>Covered Interaction Tuples</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Covered Interaction Tuples</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Covered Interaction Tuples</em>' containment reference list.
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTest_CoveredInteractionTuples()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<InteractionTuple> getCoveredInteractionTuples();

	/**
	 * Returns the value of the '<em><b>Test Graph</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Graph</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Graph</em>' containment reference.
	 * @see #setTestGraph(StateGraph)
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTest_TestGraph()
	 * @model containment="true"
	 * @generated
	 */
	StateGraph getTestGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getTestGraph <em>Test Graph</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Graph</em>' containment reference.
	 * @see #getTestGraph()
	 * @generated
	 */
	void setTestGraph(StateGraph value);

	/**
	 * Returns the value of the '<em><b>Goal States</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.stategraph.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal States</em>' reference list.
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTest_GoalStates()
	 * @model
	 * @generated
	 */
	EList<State> getGoalStates();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addInteractionTuple(InteractionTuple interactionTuple);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void removeInteractionTuple(InteractionTuple interactionTuple);
	
	void removeAllInteractionTuples(Collection<InteractionTuple> interactionTuples);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.eclipse.uml2.types.Boolean"
	 * @generated
	 */
	boolean hasSameInteractionTuples(InteractionBasedTest otherTest);
	
	

	void addAllInteractionTuples(
			EList<InteractionTuple> interactionTuples);

} // InteractionBasedTest
