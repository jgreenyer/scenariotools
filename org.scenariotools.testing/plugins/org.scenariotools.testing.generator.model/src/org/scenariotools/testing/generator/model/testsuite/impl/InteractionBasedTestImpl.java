/**
 */
package org.scenariotools.testing.generator.model.testsuite.impl;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TestsuitePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Based Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl#getCoveredInteractionTuples <em>Covered Interaction Tuples</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl#getTestGraph <em>Test Graph</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl#getGoalStates <em>Goal States</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InteractionBasedTestImpl extends EObjectImpl implements InteractionBasedTest {
	/**
	 * The cached value of the '{@link #getCoveredInteractionTuples() <em>Covered Interaction Tuples</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoveredInteractionTuples()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionTuple> coveredInteractionTuples;

	/**
	 * The cached value of the '{@link #getTestGraph() <em>Test Graph</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestGraph()
	 * @generated
	 * @ordered
	 */
	protected StateGraph testGraph;

	/**
	 * The cached value of the '{@link #getGoalStates() <em>Goal States</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoalStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> goalStates;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestsuitePackage.Literals.INTERACTION_BASED_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionTuple> getCoveredInteractionTuples() {
		if (coveredInteractionTuples == null) {
			coveredInteractionTuples = new EObjectContainmentEList<InteractionTuple>(InteractionTuple.class, this, TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES);
		}
		return coveredInteractionTuples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateGraph getTestGraph() {
		return testGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestGraph(StateGraph newTestGraph, NotificationChain msgs) {
		StateGraph oldTestGraph = testGraph;
		testGraph = newTestGraph;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH, oldTestGraph, newTestGraph);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestGraph(StateGraph newTestGraph) {
		if (newTestGraph != testGraph) {
			NotificationChain msgs = null;
			if (testGraph != null)
				msgs = ((InternalEObject)testGraph).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH, null, msgs);
			if (newTestGraph != null)
				msgs = ((InternalEObject)newTestGraph).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH, null, msgs);
			msgs = basicSetTestGraph(newTestGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH, newTestGraph, newTestGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getGoalStates() {
		if (goalStates == null) {
			goalStates = new EObjectResolvingEList<State>(State.class, this, TestsuitePackage.INTERACTION_BASED_TEST__GOAL_STATES);
		}
		return goalStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES:
				return ((InternalEList<?>)getCoveredInteractionTuples()).basicRemove(otherEnd, msgs);
			case TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH:
				return basicSetTestGraph(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES:
				return getCoveredInteractionTuples();
			case TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH:
				return getTestGraph();
			case TestsuitePackage.INTERACTION_BASED_TEST__GOAL_STATES:
				return getGoalStates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES:
				getCoveredInteractionTuples().clear();
				getCoveredInteractionTuples().addAll((Collection<? extends InteractionTuple>)newValue);
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH:
				setTestGraph((StateGraph)newValue);
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST__GOAL_STATES:
				getGoalStates().clear();
				getGoalStates().addAll((Collection<? extends State>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES:
				getCoveredInteractionTuples().clear();
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH:
				setTestGraph((StateGraph)null);
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST__GOAL_STATES:
				getGoalStates().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES:
				return coveredInteractionTuples != null && !coveredInteractionTuples.isEmpty();
			case TestsuitePackage.INTERACTION_BASED_TEST__TEST_GRAPH:
				return testGraph != null;
			case TestsuitePackage.INTERACTION_BASED_TEST__GOAL_STATES:
				return goalStates != null && !goalStates.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public void addAllInteractionTuples(
			EList<InteractionTuple> interactionTuples) {
		for (InteractionTuple interactionTuple : interactionTuples){
			addInteractionTuple(interactionTuple);
		}		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addInteractionTuple(InteractionTuple interactionTuple) {
		for (InteractionTuple tuple : getCoveredInteractionTuples()) {
			if (EcoreUtil.equals(tuple,	interactionTuple))
				return;
		}
		getCoveredInteractionTuples().add(EcoreUtil.copy(interactionTuple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeInteractionTuple(InteractionTuple interactionTuple) {
		Iterator<InteractionTuple> it = getCoveredInteractionTuples().iterator();
		while (it.hasNext()){
			InteractionTuple tuple = it.next();
			if (EcoreUtil.equals(tuple, interactionTuple)){
				it.remove();
				break;
			}
		}
	}

	@Override
	public void removeAllInteractionTuples(
			Collection<InteractionTuple> interactionTuples) {
		for (InteractionTuple interactionTuple: interactionTuples){
			removeInteractionTuple(interactionTuple);
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean hasSameInteractionTuples(InteractionBasedTest otherTest) {
		boolean hasSameTuples = true;
		for (InteractionTuple otherTuple : otherTest.getCoveredInteractionTuples()){		
			boolean commonTupleFound = false; 
			for (InteractionTuple tuple: getCoveredInteractionTuples()){	
				if (EcoreUtil.equals(tuple, otherTuple)){
					commonTupleFound = true;
					break;
				}
			}
			if (!commonTupleFound){				
				return !hasSameTuples;
			}				
		}
		return hasSameTuples;
	}

} //InteractionBasedTestImpl
