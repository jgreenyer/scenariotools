/**
 */
package org.scenariotools.testing.generator.model.testsuite.impl;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.testing.generator.model.testsuite.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Interaction;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TWiseCoverage;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;
import org.scenariotools.testing.generator.model.testsuite.TestsuitePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestsuiteFactoryImpl extends EFactoryImpl implements TestsuiteFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TestsuiteFactory init() {
		try {
			TestsuiteFactory theTestsuiteFactory = (TestsuiteFactory)EPackage.Registry.INSTANCE.getEFactory(TestsuitePackage.eNS_URI);
			if (theTestsuiteFactory != null) {
				return theTestsuiteFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TestsuiteFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestsuiteFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE: return createInteractionBasedTestSuite();
			case TestsuitePackage.INTERACTION_BASED_TEST: return createInteractionBasedTest();
			case TestsuitePackage.INTERACTION_TUPLE: return createInteractionTuple();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TestsuitePackage.TWISE_COVERAGE:
				return createTWiseCoverageFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TestsuitePackage.TWISE_COVERAGE:
				return convertTWiseCoverageToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionBasedTestSuite createInteractionBasedTestSuite() {
		InteractionBasedTestSuiteImpl interactionBasedTestSuite = new InteractionBasedTestSuiteImpl();
		return interactionBasedTestSuite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionBasedTest createInteractionBasedTest() {
		InteractionBasedTestImpl interactionBasedTest = new InteractionBasedTestImpl();
		return interactionBasedTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionTuple createInteractionTuple() {
		InteractionTupleImpl interactionTuple = new InteractionTupleImpl();
		return interactionTuple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TWiseCoverage createTWiseCoverageFromString(EDataType eDataType, String initialValue) {
		TWiseCoverage result = TWiseCoverage.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTWiseCoverageToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestsuitePackage getTestsuitePackage() {
		return (TestsuitePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TestsuitePackage getPackage() {
		return TestsuitePackage.eINSTANCE;
	}

	
	public InteractionBasedTest createInteractionBasedTestByCopy(
			InteractionBasedTest testToCopy) {
		InteractionBasedTest resultTest = EcoreUtil.copy(testToCopy);		
		return resultTest;
	}

	
	public InteractionBasedTest createInteractionBasedTestByIntersection(
			InteractionBasedTest firstTest,
			InteractionBasedTest secondTest) {
		InteractionBasedTest resultTest = createInteractionBasedTest();
		for (InteractionTuple interactionTupleOfFirstTest : firstTest.getCoveredInteractionTuples()) {
			for (InteractionTuple interactionTupleOfSecondTest : secondTest.getCoveredInteractionTuples()){
				InteractionTuple intersectionTuple = createInteractionTuple();
				intersectionTuple.union(interactionTupleOfFirstTest);
				intersectionTuple.intersect(interactionTupleOfSecondTest);
				if (!intersectionTuple.isEmpty())
					resultTest.addInteractionTuple(intersectionTuple);									
			}
		}
		return resultTest;

	}

	 
	public InteractionTuple createInteractionTupleFromInteractions(
			Set<Interaction> interactions) {
		InteractionTuple interactionTuple = createInteractionTuple();		
		interactionTuple.getInteractions().addAll(interactions);
		return interactionTuple;
	}



	public Set<InteractionTuple> createSingletonInteractionTuplesFromInteractions(
			Set<Interaction> interactions) {
		Set<InteractionTuple> resultSet = new HashSet<InteractionTuple>();
		for (Interaction interaction : interactions) {
			Set <Interaction> singletonSet = new HashSet<>();
			singletonSet.add(interaction);
			InteractionTuple singletonTuple = createInteractionTupleFromInteractions(singletonSet);
			resultSet.add(singletonTuple);
		}
		return resultSet;		
	}
	
	public Set<InteractionTuple> createPairInteractionTuplesFromInteractions(
			Set<Interaction> interactions) {
		Set<InteractionTuple> resultSet = new HashSet<InteractionTuple>();		
		Interaction[] interactionsArray=interactions.toArray(new Interaction[0]);
		for (int i = 0; i < interactionsArray.length - 1 ; i++){
			for (int j = i + 1 ; j < interactionsArray.length; j++) {
				Set<Interaction> pair = new HashSet<>();
				pair.add(interactionsArray[i]);
				pair.add(interactionsArray[j]);
				InteractionTuple pairInteractionTuple =createInteractionTupleFromInteractions(pair);
				resultSet.add(pairInteractionTuple);				
			}
		}
		return resultSet;
	}
	
	public Set<InteractionTuple> createTripleInteractionTuplesFromInteractions(
			Set<Interaction> interactions){
		Set<InteractionTuple> resultSet = new HashSet<InteractionTuple>();		
		Interaction[] interactionsArray=interactions.toArray(new Interaction[0]);
		for (int i = 0; i < interactionsArray.length - 1 ; i++){
			for (int j = i + 1 ; j < interactionsArray.length; j++) {
				for(int x = j +1; x < interactionsArray.length; x++ ){
					Set<Interaction> triple = new HashSet<>();
					triple.add(interactionsArray[i]);
					triple.add(interactionsArray[j]);
					triple.add(interactionsArray[x]);
					InteractionTuple pairInteractionTuple =createInteractionTupleFromInteractions(triple);
					resultSet.add(pairInteractionTuple);					
				}													
			}
		}
		return resultSet;
		
	}
	
	public Set<InteractionTuple> createQuadrupleInteractionTuplesFromInteractions(
			Set<Interaction> interactions){
		Set<InteractionTuple> resultSet = new HashSet<InteractionTuple>();		
		Interaction[] interactionsArray=interactions.toArray(new Interaction[0]);
		for (int i = 0; i < interactionsArray.length - 1 ; i++){
			for (int j = i + 1 ; j < interactionsArray.length; j++) {
				for(int x = j + 1; x < interactionsArray.length; x++ ){
					for (int y = x + 1; y < interactionsArray.length; y++){
						Set<Interaction> quadruple = new HashSet<>();
						quadruple.add(interactionsArray[i]);
						quadruple.add(interactionsArray[j]);
						quadruple.add(interactionsArray[x]);
						quadruple.add(interactionsArray[y]);
						InteractionTuple pairInteractionTuple =createInteractionTupleFromInteractions(quadruple);
						resultSet.add(pairInteractionTuple);					
					}
				}													
			}
		}
		return resultSet;		
	}

	@Override
	public InteractionBasedTest createInteractionBasedTestFromInteractions(
			Set<Interaction> interactions, TWiseCoverage tWiseCoverage) {
		InteractionBasedTest test = createInteractionBasedTest();
		switch (tWiseCoverage) {
		case SINGLE_WISE:
			test.getCoveredInteractionTuples().addAll(createSingletonInteractionTuplesFromInteractions(interactions));
			break;
		case PAIR_WISE:
			test.getCoveredInteractionTuples().addAll(createSingletonInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createPairInteractionTuplesFromInteractions(interactions));
			break;
		case THREE_WISE:
			test.getCoveredInteractionTuples().addAll(createSingletonInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createPairInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createTripleInteractionTuplesFromInteractions(interactions));
			break;
		case FOUR_WISE:
			test.getCoveredInteractionTuples().addAll(createSingletonInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createPairInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createTripleInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createQuadrupleInteractionTuplesFromInteractions(interactions));
			break;		

		default:
			test.getCoveredInteractionTuples().addAll(createSingletonInteractionTuplesFromInteractions(interactions));
			test.getCoveredInteractionTuples().addAll(createPairInteractionTuplesFromInteractions(interactions));
			break;
		}
		
		return test;
	}

	
} //TestsuiteFactoryImpl
