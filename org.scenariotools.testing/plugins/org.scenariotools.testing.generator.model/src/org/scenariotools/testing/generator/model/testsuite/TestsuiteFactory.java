/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import java.util.Set;

import org.eclipse.emf.ecore.EFactory;
import org.eclipse.uml2.uml.Interaction;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage
 * @generated
 */
public interface TestsuiteFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestsuiteFactory eINSTANCE = org.scenariotools.testing.generator.model.testsuite.impl.TestsuiteFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Interaction Based Test Suite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Based Test Suite</em>'.
	 * @generated
	 */
	InteractionBasedTestSuite createInteractionBasedTestSuite();

	/**
	 * Returns a new object of class '<em>Interaction Based Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Based Test</em>'.
	 * @generated
	 */
	InteractionBasedTest createInteractionBasedTest();

	/**
	 * Returns a new object of class '<em>Interaction Tuple</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Tuple</em>'.
	 * @generated
	 */
	InteractionTuple createInteractionTuple();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TestsuitePackage getTestsuitePackage();

	InteractionBasedTest createInteractionBasedTestByCopy(
			InteractionBasedTest targetStateTest);

	InteractionBasedTest createInteractionBasedTestByIntersection(
			InteractionBasedTest currentTestOfSourceState,
			InteractionBasedTest testOfTargetState);

	InteractionTuple createInteractionTupleFromInteractions(
			Set<Interaction> singletonSet);

	Set<InteractionTuple> createSingletonInteractionTuplesFromInteractions(
			Set<Interaction> interactions);
	
	Set<InteractionTuple> createPairInteractionTuplesFromInteractions(
			Set<Interaction> interactions);
	
	Set<InteractionTuple> createTripleInteractionTuplesFromInteractions(
			Set<Interaction> interactions);
	
	Set<InteractionTuple> createQuadrupleInteractionTuplesFromInteractions(
			Set<Interaction> interactions);
	
	InteractionBasedTest createInteractionBasedTestFromInteractions(
			Set<Interaction> interactions, TWiseCoverage tWiseCoverage);

} //TestsuiteFactory
