/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory
 * @model kind="package"
 * @generated
 */
public interface TestsuitePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "testsuite";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.testing.generator.model.testsuite/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "testsuite";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestsuitePackage eINSTANCE = org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl <em>Interaction Based Test Suite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionBasedTestSuite()
	 * @generated
	 */
	int INTERACTION_BASED_TEST_SUITE = 0;

	/**
	 * The feature id for the '<em><b>Interaction Based Tests</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS = 0;

	/**
	 * The feature id for the '<em><b>Overall Covered Tuples</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES = 1;

	/**
	 * The feature id for the '<em><b>TWise Coverage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE = 2;

	/**
	 * The number of structural features of the '<em>Interaction Based Test Suite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST_SUITE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl <em>Interaction Based Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionBasedTest()
	 * @generated
	 */
	int INTERACTION_BASED_TEST = 1;

	/**
	 * The feature id for the '<em><b>Covered Interaction Tuples</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES = 0;

	/**
	 * The feature id for the '<em><b>Test Graph</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST__TEST_GRAPH = 1;

	/**
	 * The feature id for the '<em><b>Goal States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST__GOAL_STATES = 2;

	/**
	 * The number of structural features of the '<em>Interaction Based Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_BASED_TEST_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionTupleImpl <em>Interaction Tuple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionTupleImpl
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionTuple()
	 * @generated
	 */
	int INTERACTION_TUPLE = 2;

	/**
	 * The feature id for the '<em><b>Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TUPLE__INTERACTIONS = 0;

	/**
	 * The number of structural features of the '<em>Interaction Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_TUPLE_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link org.scenariotools.testing.generator.model.testsuite.TWiseCoverage <em>TWise Coverage</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.testing.generator.model.testsuite.TWiseCoverage
	 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getTWiseCoverage()
	 * @generated
	 */
	int TWISE_COVERAGE = 3;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite <em>Interaction Based Test Suite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Based Test Suite</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite
	 * @generated
	 */
	EClass getInteractionBasedTestSuite();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getInteractionBasedTests <em>Interaction Based Tests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interaction Based Tests</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getInteractionBasedTests()
	 * @see #getInteractionBasedTestSuite()
	 * @generated
	 */
	EReference getInteractionBasedTestSuite_InteractionBasedTests();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getOverallCoveredTuples <em>Overall Covered Tuples</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Overall Covered Tuples</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getOverallCoveredTuples()
	 * @see #getInteractionBasedTestSuite()
	 * @generated
	 */
	EReference getInteractionBasedTestSuite_OverallCoveredTuples();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getTWiseCoverage <em>TWise Coverage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>TWise Coverage</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getTWiseCoverage()
	 * @see #getInteractionBasedTestSuite()
	 * @generated
	 */
	EAttribute getInteractionBasedTestSuite_TWiseCoverage();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest <em>Interaction Based Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Based Test</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest
	 * @generated
	 */
	EClass getInteractionBasedTest();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getCoveredInteractionTuples <em>Covered Interaction Tuples</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Covered Interaction Tuples</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getCoveredInteractionTuples()
	 * @see #getInteractionBasedTest()
	 * @generated
	 */
	EReference getInteractionBasedTest_CoveredInteractionTuples();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getTestGraph <em>Test Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Graph</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getTestGraph()
	 * @see #getInteractionBasedTest()
	 * @generated
	 */
	EReference getInteractionBasedTest_TestGraph();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getGoalStates <em>Goal States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Goal States</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest#getGoalStates()
	 * @see #getInteractionBasedTest()
	 * @generated
	 */
	EReference getInteractionBasedTest_GoalStates();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple <em>Interaction Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Tuple</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple
	 * @generated
	 */
	EClass getInteractionTuple();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#getInteractions <em>Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interactions</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.InteractionTuple#getInteractions()
	 * @see #getInteractionTuple()
	 * @generated
	 */
	EReference getInteractionTuple_Interactions();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.testing.generator.model.testsuite.TWiseCoverage <em>TWise Coverage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>TWise Coverage</em>'.
	 * @see org.scenariotools.testing.generator.model.testsuite.TWiseCoverage
	 * @generated
	 */
	EEnum getTWiseCoverage();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestsuiteFactory getTestsuiteFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl <em>Interaction Based Test Suite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionBasedTestSuite()
		 * @generated
		 */
		EClass INTERACTION_BASED_TEST_SUITE = eINSTANCE.getInteractionBasedTestSuite();

		/**
		 * The meta object literal for the '<em><b>Interaction Based Tests</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS = eINSTANCE.getInteractionBasedTestSuite_InteractionBasedTests();

		/**
		 * The meta object literal for the '<em><b>Overall Covered Tuples</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES = eINSTANCE.getInteractionBasedTestSuite_OverallCoveredTuples();

		/**
		 * The meta object literal for the '<em><b>TWise Coverage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE = eINSTANCE.getInteractionBasedTestSuite_TWiseCoverage();

		/**
		 * The meta object literal for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl <em>Interaction Based Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestImpl
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionBasedTest()
		 * @generated
		 */
		EClass INTERACTION_BASED_TEST = eINSTANCE.getInteractionBasedTest();

		/**
		 * The meta object literal for the '<em><b>Covered Interaction Tuples</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES = eINSTANCE.getInteractionBasedTest_CoveredInteractionTuples();

		/**
		 * The meta object literal for the '<em><b>Test Graph</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_BASED_TEST__TEST_GRAPH = eINSTANCE.getInteractionBasedTest_TestGraph();

		/**
		 * The meta object literal for the '<em><b>Goal States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_BASED_TEST__GOAL_STATES = eINSTANCE.getInteractionBasedTest_GoalStates();

		/**
		 * The meta object literal for the '{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionTupleImpl <em>Interaction Tuple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.InteractionTupleImpl
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getInteractionTuple()
		 * @generated
		 */
		EClass INTERACTION_TUPLE = eINSTANCE.getInteractionTuple();

		/**
		 * The meta object literal for the '<em><b>Interactions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_TUPLE__INTERACTIONS = eINSTANCE.getInteractionTuple_Interactions();

		/**
		 * The meta object literal for the '{@link org.scenariotools.testing.generator.model.testsuite.TWiseCoverage <em>TWise Coverage</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.testing.generator.model.testsuite.TWiseCoverage
		 * @see org.scenariotools.testing.generator.model.testsuite.impl.TestsuitePackageImpl#getTWiseCoverage()
		 * @generated
		 */
		EEnum TWISE_COVERAGE = eINSTANCE.getTWiseCoverage();

	}

} //TestsuitePackage
