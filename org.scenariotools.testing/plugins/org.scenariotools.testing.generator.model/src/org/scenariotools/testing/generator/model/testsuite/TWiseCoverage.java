/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>TWise Coverage</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getTWiseCoverage()
 * @model
 * @generated
 */
public enum TWiseCoverage implements Enumerator {
	/**
	 * The '<em><b>Single Wise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SINGLE_WISE_VALUE
	 * @generated
	 * @ordered
	 */
	SINGLE_WISE(1, "SingleWise", "SingleWise"),

	/**
	 * The '<em><b>Pair Wise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAIR_WISE_VALUE
	 * @generated
	 * @ordered
	 */
	PAIR_WISE(2, "PairWise", "PairWise"),

	/**
	 * The '<em><b>Three Wise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THREE_WISE_VALUE
	 * @generated
	 * @ordered
	 */
	THREE_WISE(3, "ThreeWise", "ThreeWise"),

	/**
	 * The '<em><b>Four Wise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOUR_WISE_VALUE
	 * @generated
	 * @ordered
	 */
	FOUR_WISE(4, "FourWise", "FourWise");

	/**
	 * The '<em><b>Single Wise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Single Wise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SINGLE_WISE
	 * @model name="SingleWise"
	 * @generated
	 * @ordered
	 */
	public static final int SINGLE_WISE_VALUE = 1;

	/**
	 * The '<em><b>Pair Wise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pair Wise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PAIR_WISE
	 * @model name="PairWise"
	 * @generated
	 * @ordered
	 */
	public static final int PAIR_WISE_VALUE = 2;

	/**
	 * The '<em><b>Three Wise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Three Wise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THREE_WISE
	 * @model name="ThreeWise"
	 * @generated
	 * @ordered
	 */
	public static final int THREE_WISE_VALUE = 3;

	/**
	 * The '<em><b>Four Wise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Four Wise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FOUR_WISE
	 * @model name="FourWise"
	 * @generated
	 * @ordered
	 */
	public static final int FOUR_WISE_VALUE = 4;

	/**
	 * An array of all the '<em><b>TWise Coverage</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final TWiseCoverage[] VALUES_ARRAY =
		new TWiseCoverage[] {
			SINGLE_WISE,
			PAIR_WISE,
			THREE_WISE,
			FOUR_WISE,
		};

	/**
	 * A public read-only list of all the '<em><b>TWise Coverage</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<TWiseCoverage> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>TWise Coverage</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TWiseCoverage get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TWiseCoverage result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>TWise Coverage</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TWiseCoverage getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TWiseCoverage result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>TWise Coverage</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TWiseCoverage get(int value) {
		switch (value) {
			case SINGLE_WISE_VALUE: return SINGLE_WISE;
			case PAIR_WISE_VALUE: return PAIR_WISE;
			case THREE_WISE_VALUE: return THREE_WISE;
			case FOUR_WISE_VALUE: return FOUR_WISE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TWiseCoverage(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //TWiseCoverage
