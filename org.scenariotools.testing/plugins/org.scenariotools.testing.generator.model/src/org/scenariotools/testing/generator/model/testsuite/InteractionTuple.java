/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Interaction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#getInteractions <em>Interactions</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionTuple()
 * @model
 * @generated
 */
public interface InteractionTuple extends EObject {
	/**
	 * Returns the value of the '<em><b>Interactions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Interaction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interactions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interactions</em>' reference list.
	 * @see #isSetInteractions()
	 * @see #unsetInteractions()
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionTuple_Interactions()
	 * @model unsettable="true" ordered="false"
	 * @generated
	 */
	EList<Interaction> getInteractions();

	/**
	 * Unsets the value of the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#getInteractions <em>Interactions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInteractions()
	 * @see #getInteractions()
	 * @generated
	 */
	void unsetInteractions();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple#getInteractions <em>Interactions</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Interactions</em>' reference list is set.
	 * @see #unsetInteractions()
	 * @see #getInteractions()
	 * @generated
	 */
	boolean isSetInteractions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isEmpty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void intersect(InteractionTuple other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void union(InteractionTuple other);

} // InteractionTuple
