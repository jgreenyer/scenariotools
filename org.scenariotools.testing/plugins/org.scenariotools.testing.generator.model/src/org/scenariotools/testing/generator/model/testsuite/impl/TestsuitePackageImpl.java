/**
 */
package org.scenariotools.testing.generator.model.testsuite.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

import org.scenariotools.stategraph.StategraphPackage;

import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TWiseCoverage;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;
import org.scenariotools.testing.generator.model.testsuite.TestsuitePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestsuitePackageImpl extends EPackageImpl implements TestsuitePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionBasedTestSuiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionBasedTestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum tWiseCoverageEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TestsuitePackageImpl() {
		super(eNS_URI, TestsuiteFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TestsuitePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TestsuitePackage init() {
		if (isInited) return (TestsuitePackage)EPackage.Registry.INSTANCE.getEPackage(TestsuitePackage.eNS_URI);

		// Obtain or create and register package
		TestsuitePackageImpl theTestsuitePackage = (TestsuitePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TestsuitePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TestsuitePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StategraphPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTestsuitePackage.createPackageContents();

		// Initialize created meta-data
		theTestsuitePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTestsuitePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TestsuitePackage.eNS_URI, theTestsuitePackage);
		return theTestsuitePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionBasedTestSuite() {
		return interactionBasedTestSuiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionBasedTestSuite_InteractionBasedTests() {
		return (EReference)interactionBasedTestSuiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionBasedTestSuite_OverallCoveredTuples() {
		return (EReference)interactionBasedTestSuiteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInteractionBasedTestSuite_TWiseCoverage() {
		return (EAttribute)interactionBasedTestSuiteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionBasedTest() {
		return interactionBasedTestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionBasedTest_CoveredInteractionTuples() {
		return (EReference)interactionBasedTestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionBasedTest_TestGraph() {
		return (EReference)interactionBasedTestEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionBasedTest_GoalStates() {
		return (EReference)interactionBasedTestEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionTuple() {
		return interactionTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionTuple_Interactions() {
		return (EReference)interactionTupleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTWiseCoverage() {
		return tWiseCoverageEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestsuiteFactory getTestsuiteFactory() {
		return (TestsuiteFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		interactionBasedTestSuiteEClass = createEClass(INTERACTION_BASED_TEST_SUITE);
		createEReference(interactionBasedTestSuiteEClass, INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS);
		createEReference(interactionBasedTestSuiteEClass, INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES);
		createEAttribute(interactionBasedTestSuiteEClass, INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE);

		interactionBasedTestEClass = createEClass(INTERACTION_BASED_TEST);
		createEReference(interactionBasedTestEClass, INTERACTION_BASED_TEST__COVERED_INTERACTION_TUPLES);
		createEReference(interactionBasedTestEClass, INTERACTION_BASED_TEST__TEST_GRAPH);
		createEReference(interactionBasedTestEClass, INTERACTION_BASED_TEST__GOAL_STATES);

		interactionTupleEClass = createEClass(INTERACTION_TUPLE);
		createEReference(interactionTupleEClass, INTERACTION_TUPLE__INTERACTIONS);

		// Create enums
		tWiseCoverageEEnum = createEEnum(TWISE_COVERAGE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StategraphPackage theStategraphPackage = (StategraphPackage)EPackage.Registry.INSTANCE.getEPackage(StategraphPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(interactionBasedTestSuiteEClass, InteractionBasedTestSuite.class, "InteractionBasedTestSuite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionBasedTestSuite_InteractionBasedTests(), this.getInteractionBasedTest(), null, "interactionBasedTests", null, 0, -1, InteractionBasedTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getInteractionBasedTestSuite_OverallCoveredTuples(), this.getInteractionTuple(), null, "overallCoveredTuples", null, 0, -1, InteractionBasedTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getInteractionBasedTestSuite_TWiseCoverage(), this.getTWiseCoverage(), "tWiseCoverage", null, 0, 1, InteractionBasedTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(interactionBasedTestSuiteEClass, null, "computeOverallCoveredTuples", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(interactionBasedTestEClass, InteractionBasedTest.class, "InteractionBasedTest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionBasedTest_CoveredInteractionTuples(), this.getInteractionTuple(), null, "coveredInteractionTuples", null, 0, -1, InteractionBasedTest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getInteractionBasedTest_TestGraph(), theStategraphPackage.getStateGraph(), null, "testGraph", null, 0, 1, InteractionBasedTest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionBasedTest_GoalStates(), theStategraphPackage.getState(), null, "goalStates", null, 0, -1, InteractionBasedTest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(interactionBasedTestEClass, null, "addInteractionTuple", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInteractionTuple(), "interactionTuple", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(interactionBasedTestEClass, null, "removeInteractionTuple", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInteractionTuple(), "interactionTuple", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(interactionBasedTestEClass, theTypesPackage.getBoolean(), "hasSameInteractionTuples", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInteractionBasedTest(), "otherTest", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(interactionTupleEClass, InteractionTuple.class, "InteractionTuple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionTuple_Interactions(), theUMLPackage.getInteraction(), null, "interactions", null, 0, -1, InteractionTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		addEOperation(interactionTupleEClass, ecorePackage.getEBoolean(), "isEmpty", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(interactionTupleEClass, null, "intersect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInteractionTuple(), "other", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(interactionTupleEClass, null, "union", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInteractionTuple(), "other", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(tWiseCoverageEEnum, TWiseCoverage.class, "TWiseCoverage");
		addEEnumLiteral(tWiseCoverageEEnum, TWiseCoverage.SINGLE_WISE);
		addEEnumLiteral(tWiseCoverageEEnum, TWiseCoverage.PAIR_WISE);
		addEEnumLiteral(tWiseCoverageEEnum, TWiseCoverage.THREE_WISE);
		addEEnumLiteral(tWiseCoverageEEnum, TWiseCoverage.FOUR_WISE);

		// Create resource
		createResource(eNS_URI);
	}

} //TestsuitePackageImpl
