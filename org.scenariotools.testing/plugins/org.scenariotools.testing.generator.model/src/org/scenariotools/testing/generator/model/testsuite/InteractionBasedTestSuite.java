/**
 */
package org.scenariotools.testing.generator.model.testsuite;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Based Test Suite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getInteractionBasedTests <em>Interaction Based Tests</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getOverallCoveredTuples <em>Overall Covered Tuples</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getTWiseCoverage <em>TWise Coverage</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTestSuite()
 * @model
 * @generated
 */
public interface InteractionBasedTestSuite extends EObject {
	/**
	 * Returns the value of the '<em><b>Interaction Based Tests</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Based Tests</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Based Tests</em>' containment reference list.
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTestSuite_InteractionBasedTests()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<InteractionBasedTest> getInteractionBasedTests();

	/**
	 * Returns the value of the '<em><b>Overall Covered Tuples</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.testing.generator.model.testsuite.InteractionTuple}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overall Covered Tuples</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overall Covered Tuples</em>' reference list.
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTestSuite_OverallCoveredTuples()
	 * @model ordered="false"
	 * @generated
	 */
	EList<InteractionTuple> getOverallCoveredTuples();
	
	/**
	 * Returns the value of the '<em><b>TWise Coverage</b></em>' attribute.
	 * The literals are from the enumeration {@link org.scenariotools.testing.generator.model.testsuite.TWiseCoverage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TWise Coverage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TWise Coverage</em>' attribute.
	 * @see org.scenariotools.testing.generator.model.testsuite.TWiseCoverage
	 * @see #setTWiseCoverage(TWiseCoverage)
	 * @see org.scenariotools.testing.generator.model.testsuite.TestsuitePackage#getInteractionBasedTestSuite_TWiseCoverage()
	 * @model
	 * @generated
	 */
	TWiseCoverage getTWiseCoverage();

	/**
	 * Sets the value of the '{@link org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite#getTWiseCoverage <em>TWise Coverage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TWise Coverage</em>' attribute.
	 * @see org.scenariotools.testing.generator.model.testsuite.TWiseCoverage
	 * @see #getTWiseCoverage()
	 * @generated
	 */
	void setTWiseCoverage(TWiseCoverage value);

	EList<InteractionTuple> computeOverallCoveredTuples();

} // InteractionBasedTestSuite
