/**
 */
package org.scenariotools.testing.generator.model.testsuite.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osgi.internal.resolver.ComputeNodeOrder;

import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTest;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.model.testsuite.InteractionTuple;
import org.scenariotools.testing.generator.model.testsuite.TWiseCoverage;
import org.scenariotools.testing.generator.model.testsuite.TestsuiteFactory;
import org.scenariotools.testing.generator.model.testsuite.TestsuitePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Based Test Suite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl#getInteractionBasedTests <em>Interaction Based Tests</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl#getOverallCoveredTuples <em>Overall Covered Tuples</em>}</li>
 *   <li>{@link org.scenariotools.testing.generator.model.testsuite.impl.InteractionBasedTestSuiteImpl#getTWiseCoverage <em>TWise Coverage</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InteractionBasedTestSuiteImpl extends EObjectImpl implements InteractionBasedTestSuite {
	/**
	 * The cached value of the '{@link #getInteractionBasedTests() <em>Interaction Based Tests</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionBasedTests()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionBasedTest> interactionBasedTests;

	/**
	 * The cached value of the '{@link #getOverallCoveredTuples() <em>Overall Covered Tuples</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverallCoveredTuples()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionTuple> overallCoveredTuples;

	/**
	 * The default value of the '{@link #getTWiseCoverage() <em>TWise Coverage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTWiseCoverage()
	 * @generated
	 * @ordered
	 */
	protected static final TWiseCoverage TWISE_COVERAGE_EDEFAULT = TWiseCoverage.SINGLE_WISE;

	/**
	 * The cached value of the '{@link #getTWiseCoverage() <em>TWise Coverage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTWiseCoverage()
	 * @generated
	 * @ordered
	 */
	protected TWiseCoverage tWiseCoverage = TWISE_COVERAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionBasedTestSuiteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestsuitePackage.Literals.INTERACTION_BASED_TEST_SUITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionBasedTest> getInteractionBasedTests() {
		if (interactionBasedTests == null) {
			interactionBasedTests = new EObjectContainmentEList<InteractionBasedTest>(InteractionBasedTest.class, this, TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS);
		}
		return interactionBasedTests;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionTuple> getOverallCoveredTuples() {
		if (overallCoveredTuples == null) {
			overallCoveredTuples = new EObjectResolvingEList<InteractionTuple>(InteractionTuple.class, this, TestsuitePackage.INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES);
		}
		return overallCoveredTuples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TWiseCoverage getTWiseCoverage() {
		return tWiseCoverage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTWiseCoverage(TWiseCoverage newTWiseCoverage) {
		TWiseCoverage oldTWiseCoverage = tWiseCoverage;
		tWiseCoverage = newTWiseCoverage == null ? TWISE_COVERAGE_EDEFAULT : newTWiseCoverage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestsuitePackage.INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE, oldTWiseCoverage, tWiseCoverage));
	}

	public EList<InteractionTuple> computeOverallCoveredTuples() {
		InteractionBasedTest overallTest = TestsuiteFactory.eINSTANCE.createInteractionBasedTest();
		for (InteractionBasedTest test : getInteractionBasedTests()){
			EList<InteractionTuple> copiedInteractionTuples = new BasicEList<InteractionTuple>(EcoreUtil.copyAll(test.getCoveredInteractionTuples()));
			overallTest.addAllInteractionTuples(copiedInteractionTuples);					
		}
			
		return overallTest.getCoveredInteractionTuples();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS:
				return ((InternalEList<?>)getInteractionBasedTests()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS:
				return getInteractionBasedTests();
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES:
				return getOverallCoveredTuples();
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE:
				return getTWiseCoverage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS:
				getInteractionBasedTests().clear();
				getInteractionBasedTests().addAll((Collection<? extends InteractionBasedTest>)newValue);
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES:
				getOverallCoveredTuples().clear();
				getOverallCoveredTuples().addAll((Collection<? extends InteractionTuple>)newValue);
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE:
				setTWiseCoverage((TWiseCoverage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS:
				getInteractionBasedTests().clear();
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES:
				getOverallCoveredTuples().clear();
				return;
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE:
				setTWiseCoverage(TWISE_COVERAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__INTERACTION_BASED_TESTS:
				return interactionBasedTests != null && !interactionBasedTests.isEmpty();
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__OVERALL_COVERED_TUPLES:
				return overallCoveredTuples != null && !overallCoveredTuples.isEmpty();
			case TestsuitePackage.INTERACTION_BASED_TEST_SUITE__TWISE_COVERAGE:
				return tWiseCoverage != TWISE_COVERAGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tWiseCoverage: ");
		result.append(tWiseCoverage);
		result.append(')');
		return result.toString();
	}

} //InteractionBasedTestSuiteImpl
