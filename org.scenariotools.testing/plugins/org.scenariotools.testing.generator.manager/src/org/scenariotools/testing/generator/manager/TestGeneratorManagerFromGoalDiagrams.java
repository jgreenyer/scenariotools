package org.scenariotools.testing.generator.manager;

import java.util.Set;

import org.eclipse.uml2.uml.Interaction;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.testing.generator.algorithm.TestControllerGeneratorFromGoalDiagramsAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestGeneratorAlgorithm;

public class TestGeneratorManagerFromGoalDiagrams extends TestGeneratorManager{
	private Set<Interaction> goalDiagrams;
	
	public TestGeneratorManagerFromGoalDiagrams(
			ScenarioRunConfiguration scenarioRunConfiguration,
			Set<Interaction> interactions, int tWiseCoverageCriterion) {
		super(scenarioRunConfiguration,tWiseCoverageCriterion);		
		this.goalDiagrams = interactions;
					
	}

	@Override
	protected TestGeneratorAlgorithm createTestGeneratorAlgorithm(){
		return new TestControllerGeneratorFromGoalDiagramsAlgorithm(goalDiagrams);
	}
	
}
