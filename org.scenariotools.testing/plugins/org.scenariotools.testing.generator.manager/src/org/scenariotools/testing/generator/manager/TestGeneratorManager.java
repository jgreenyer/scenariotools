package org.scenariotools.testing.generator.manager;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.msd.runtime.RuntimeFactory;
import org.scenariotools.msd.scenariorunconfiguration.ScenarioRunConfiguration;
import org.scenariotools.stategraph.State;
import org.scenariotools.testing.generator.algorithm.MinimizationGreedyAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestControllerGeneratorVisitingAllTransitionsAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestGenerator;
import org.scenariotools.testing.generator.algorithm.TestGeneratorAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestPopulatorAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestStrategiesExtractor;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;
import org.scenariotools.testing.generator.strategyexporter.TestStrategiesExporter;
import org.scenariotools.testing.generator.strategyexporter.graphviz.GraphvizStrategiesExporter;

public class TestGeneratorManager {
	
	private final boolean DEBUG = false;
	
	private ScenarioRunConfiguration scenarioRunConfiguration;
	private MSDRuntimeStateGraph msdRuntimeStateGraph;
	private TestGenerator testGenerator;
	private TestPopulatorAlgorithm populatorAlgorithm;
	private InteractionBasedTestSuite populatedTestSuite;
	private InteractionBasedTestSuite minimizedTestSuite;

	private int tWiseCoverageCriterion;
	

	public TestGeneratorManager(
			ScenarioRunConfiguration scenarioRunConfiguration,
			int tWiseCoverageCriterion) {
		this.scenarioRunConfiguration = scenarioRunConfiguration;		
		this.tWiseCoverageCriterion = tWiseCoverageCriterion;			
	}


	protected TestGeneratorAlgorithm createTestGeneratorAlgorithm(){
		return new TestControllerGeneratorVisitingAllTransitionsAlgorithm();
	}
	
	public boolean generateTests() {
		
		testGenerator = new TestGenerator();
		
		TestGeneratorAlgorithm testGeneratorAlgorithm = createTestGeneratorAlgorithm();				
		testGenerator.initializeTestGeneratorAlgorithm(testGeneratorAlgorithm);
		
		msdRuntimeStateGraph = createMSDRuntimeStateGraph(scenarioRunConfiguration);		
		boolean testControllerExists = testGenerator.findTest(msdRuntimeStateGraph);
		System.out.println("TestGeneratorManager: Test Controller Generator Completed");		
		if (!DEBUG){
			if (testControllerExists) {
				populatorAlgorithm = new TestPopulatorAlgorithm(testGeneratorAlgorithm,tWiseCoverageCriterion);			
				populatorAlgorithm.extractTests();
				System.out.println("TestGeneratorManager: Test Population Completed");
				populatedTestSuite = populatorAlgorithm
						.getInteractionBasedTestSuite();
				MinimizationGreedyAlgorithm minimizationAlgorithm = new MinimizationGreedyAlgorithm(
						populatedTestSuite);
				minimizationAlgorithm.minimize();
				System.out.println("TestGeneratorManager: Test Minimization Completed");
				minimizedTestSuite = minimizationAlgorithm.getMinimizedTestSuite();
				 
				TestStrategiesExtractor testStrategiesExtractor = new TestStrategiesExtractor(
						populatorAlgorithm, minimizedTestSuite);
				testStrategiesExtractor.extractTestStrategies();
				System.out.println("TestGeneratorManager: Test Extraction Completed");
			}
		}

		return testControllerExists;
	}

	private MSDRuntimeStateGraph createMSDRuntimeStateGraph(
			ScenarioRunConfiguration scenarioRunConfiguration) {

		MSDRuntimeStateGraph msdRuntimeStateGraph = RuntimeFactory.eINSTANCE
				.createMSDRuntimeStateGraph();
		msdRuntimeStateGraph.init(scenarioRunConfiguration);
		return msdRuntimeStateGraph;
	}

	public void saveTestResources(IFile scenarioRunConfigurationFile, ResourceSet resourceSet, IResource testStrategiesFolderResource ) {
		ResourceManager resourceManager = new ResourceManager(scenarioRunConfigurationFile,resourceSet);
		resourceManager.saveSpecificationStateGraph(msdRuntimeStateGraph,"_msdSpecController");
		resourceManager.saveTestController(testGenerator.getNewController(),"_testController");
		
		if (!DEBUG){
			resourceManager.saveTestSuite(populatedTestSuite,"_overallTestSuite");
			resourceManager.saveTestSuite(minimizedTestSuite,"_minimizedTestSuite");
			
			exportStrategies(testStrategiesFolderResource,scenarioRunConfigurationFile);
		}
		
	}

	private void exportStrategies(IResource testStrategiesFolderResource, IFile scenarioRunConfigurationFile){
		TestStrategiesExporter strategiesExporter = new GraphvizStrategiesExporter();
		String outDirectoryPath = testStrategiesFolderResource.getLocation().toOSString()+File.separator;
		int lengthOfFileName = scenarioRunConfigurationFile.getName().length()-scenarioRunConfigurationFile.getFileExtension().length()-1;
		String scenarioRunConfigurationFilename = scenarioRunConfigurationFile.getName().substring(0,lengthOfFileName);
		String strategyNamePrefix = scenarioRunConfigurationFilename;
		String outputPrefix =outDirectoryPath + strategyNamePrefix; 
		strategiesExporter.exportStrategies(minimizedTestSuite,outputPrefix);
	}
	public int getNumberOfExploredStates() {
		return msdRuntimeStateGraph.getStates().size();
	}

	public int getNumberOfExploredTransitions() {
		int numberOfTransitions = 0;
		for (State state : msdRuntimeStateGraph.getStates()) {
			numberOfTransitions += state.getOutgoingTransition().size();
		}
		return numberOfTransitions;
	}

	public int getNumberOfGeneratedTests() {
		if (DEBUG)
			return 0;
		return populatedTestSuite.getInteractionBasedTests().size();
	}

	public int getNumberOfMinimizedTests() {
		if (DEBUG)
			return 0;
		return minimizedTestSuite.getInteractionBasedTests().size();
	}


}
