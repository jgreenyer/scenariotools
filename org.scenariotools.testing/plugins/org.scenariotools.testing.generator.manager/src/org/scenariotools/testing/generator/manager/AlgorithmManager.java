package org.scenariotools.testing.generator.manager;

import org.scenariotools.testing.generator.algorithm.ExtractionAlgorithm;
import org.scenariotools.testing.generator.algorithm.MinimizationGreedyAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestControllerGeneratorFromGoalDiagramsAlgorithm;
import org.scenariotools.testing.generator.algorithm.TestPopulatorAlgorithm;

public class AlgorithmManager {
	private TestControllerGeneratorFromGoalDiagramsAlgorithm testControllerGeneratorAlgorithm;
	private TestPopulatorAlgorithm testPopulatorAlgorithm;
	private MinimizationGreedyAlgorithm minimizationAlgorithm;
	private ExtractionAlgorithm extractionAlgorithm;
	
	public TestControllerGeneratorFromGoalDiagramsAlgorithm getTestControllerGeneratorAlgorithm() {
		return testControllerGeneratorAlgorithm;
	}
	public void setTestControllerGeneratorAlgorithm(
			TestControllerGeneratorFromGoalDiagramsAlgorithm testControllerGeneratorAlgorithm) {
		this.testControllerGeneratorAlgorithm = testControllerGeneratorAlgorithm;
	}
	public TestPopulatorAlgorithm getTestPopulatorAlgorithm() {
		return testPopulatorAlgorithm;
	}
	public void setTestPopulatorAlgorithm(
			TestPopulatorAlgorithm testPopulatorAlgorithm) {
		this.testPopulatorAlgorithm = testPopulatorAlgorithm;
	}
	public MinimizationGreedyAlgorithm getMinimizationAlgorithm() {
		return minimizationAlgorithm;
	}
	public void setMinimizationAlgorithm(
			MinimizationGreedyAlgorithm minimizationAlgorithm) {
		this.minimizationAlgorithm = minimizationAlgorithm;
	}
	public ExtractionAlgorithm getExtractionAlgorithm() {
		return extractionAlgorithm;
	}
	public void setExtractionAlgorithm(ExtractionAlgorithm extractionAlgorithm) {
		this.extractionAlgorithm = extractionAlgorithm;
	}
	
	

}
