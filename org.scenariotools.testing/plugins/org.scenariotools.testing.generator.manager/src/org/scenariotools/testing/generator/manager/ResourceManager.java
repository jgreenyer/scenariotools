package org.scenariotools.testing.generator.manager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.scenariotools.msd.runtime.ActiveMSD;
import org.scenariotools.msd.runtime.ActiveProcess;
import org.scenariotools.msd.runtime.ActiveState;
import org.scenariotools.msd.runtime.MSDObjectSystem;
import org.scenariotools.msd.runtime.MSDRuntimeState;
import org.scenariotools.msd.runtime.MSDRuntimeStateGraph;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.testing.generator.model.testsuite.InteractionBasedTestSuite;

public class ResourceManager {
	private IFile scenarioRunConfigurationResourceFile;
	private ResourceSet resourceSet;
	private Map<String, Boolean> options;

	public ResourceManager(IFile scenarioRunConfigurationFile,
			ResourceSet resourceSet) {
		this.scenarioRunConfigurationResourceFile = scenarioRunConfigurationFile;
		this.resourceSet = resourceSet;
		options = new HashMap<String, Boolean>();
		options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
	}

	public void saveSpecificationStateGraph(
			MSDRuntimeStateGraph msdRuntimeStateGraph, String stringToAppend) {
		removeReferencesToRuntimeUtil(msdRuntimeStateGraph);
		Resource msdSpecificationStateSpaceResource = createStateGraphResource(
				msdRuntimeStateGraph, stringToAppend);
		saveResource(msdSpecificationStateSpaceResource);
	}

	private Resource createStateGraphResource(
			MSDRuntimeStateGraph msdRuntimeStateGraph, String stringToAppend) {
		Resource msdSpecificationStateSpaceResource;
		URI msdSpecificationStateSpaceFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
						.removeFileExtension().toString()
						+ stringToAppend, true).appendFileExtension("msdruntime");
		msdSpecificationStateSpaceResource = resourceSet
				.createResource(msdSpecificationStateSpaceFileURI);
		msdSpecificationStateSpaceResource.getContents().add(
				msdRuntimeStateGraph);
		return msdSpecificationStateSpaceResource;
	}

	private void saveResource(Resource resource) {
		if (resource != null) {
			try {
				resource.save(options);				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void saveTestController(RuntimeStateGraph testController, String stringToAppend) {
		Resource controllerResource = null;
		controllerResource = createControllerResource(testController, stringToAppend);
		saveResource(controllerResource);
	}

	private Resource createControllerResource(RuntimeStateGraph controller, String stringToAppend) {
		URI controllerFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
				.removeFileExtension().toString()
				+ stringToAppend, true).appendFileExtension("msdruntime");
		Resource controllerResource = resourceSet
				.createResource(controllerFileURI);
		controllerResource.getContents().add(controller);
		return controllerResource;

	}

	public void saveTestSuite(InteractionBasedTestSuite testSuite, String stringToAppend) {
		Resource testSuiteResource = null;
		testSuiteResource = createTestSuiteResource(testSuite, stringToAppend);
		saveResource(testSuiteResource);
	}

	private Resource createTestSuiteResource(InteractionBasedTestSuite testSuite, String stringToAppend) {
		Resource testSuiteResource;
		URI testSuiteResourceFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
						.removeFileExtension().toString()
						+ stringToAppend, true).appendFileExtension("testsuite");

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("testsuite", new XMIResourceFactoryImpl());

		testSuiteResource = resourceSet
				.createResource(testSuiteResourceFileURI);
		testSuiteResource.getContents().add(testSuite);
		return testSuiteResource;
	}

	private void removeReferencesToRuntimeUtil(
			MSDRuntimeStateGraph msdRuntimeStateGraph) {
		msdRuntimeStateGraph.setRuntimeUtil(null);
		for (State state : msdRuntimeStateGraph.getStates()) {
			MSDRuntimeState msdRuntimeState = (MSDRuntimeState) state;
			msdRuntimeState.setRuntimeUtil(null);
		}

		for (ActiveProcess activeProcess : msdRuntimeStateGraph
				.getElementContainer().getActiveProcesses()) {
			activeProcess.setRuntimeUtil(null);
			((ActiveMSD) activeProcess).setMsdUtil(null);
		}
		for (ActiveState activeState : msdRuntimeStateGraph
				.getElementContainer().getActiveStates()) {
			activeState.setRuntimeUtil(null);
		}
		for (MSDObjectSystem msdObjectSystem : msdRuntimeStateGraph
				.getElementContainer().getObjectSystems()) {
			msdObjectSystem.setRuntimeUtil(null);
		}
	}

	public void saveTestStrategies(InteractionBasedTestSuite minimizedTestSuite, String string) {
		
		
	}

	// private void exportTestStrategiesToGraphViz(InteractionBasedTestSuite
	// testSuite){
	// int testCounter = 0;
	// for (InteractionBasedTest test :testSuite.getInteractionBasedTests()){
	// testCounter++;
	// RuntimeStateGraph testStrategy = (RuntimeStateGraph)test.getTestGraph();
	// IFile strategyFile =
	// (IFile)((IStructuredSelection)selection).getFirstElement();
	//
	// RuntimeStateGraphExporter exporter = new RuntimeStateGraphExporter();
	// exporter.setInputFile(strategyFile);
	// exporter.export(testStrategy);
	// }
	// }

}
